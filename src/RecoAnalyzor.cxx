#include "RecoAnalyzor.h"
#include <TMath.h>
#include <TH1.h>
#include <TLorentzVector.h>
#include <TStyle.h>
#include <TCanvas.h>
#include <iostream>

using namespace std;

RecoAnalyzor::RecoAnalyzor() {
    m_lg = new Logger("Data Ana");
    m_lg->Info("s", "Hi, this is a data analyzor~");

    string m_savename = "DataHistograms.root";
    string m_saveoption = "recreate";
    string m_sysname = "Nominal";

    m_zmass = 91187.6;

    m_CUT_ph_n = 1;
    m_CUT_ph_pt = 15000.;
    m_CUT_zwindow = 5000.;
    m_CUT_dr_phj = 0.5;
    m_CUT_dr_phl = 0.7;

    m_nom_geq1ph = 0; 
    m_nom_eq1ph = 0;
    m_nom_zmass = 0;
    m_nom_drphj = 0;
    m_nom_drphl = 0;
}

RecoAnalyzor::~RecoAnalyzor()
{
    delete m_lg;
    if (!fChain) return;
    delete fChain->GetCurrentFile();
}

Int_t RecoAnalyzor::GetEntry(Long64_t entry)
{
    if (!fChain) return 0;
    return fChain->GetEntry(entry);
}

Long64_t RecoAnalyzor::LoadTree(Long64_t entry)
{
    if (!fChain) return -5;
    Long64_t centry = fChain->LoadTree(entry);
    if (centry < 0) return centry;
    if (fChain->GetTreeNumber() != fCurrent) {
       fCurrent = fChain->GetTreeNumber();
       Notify();
    }
    return centry;
}

void RecoAnalyzor::Init() {
    m_lg->Info("ss", "Initialize data analyzor with tree -->", m_treename.c_str());

    if (m_file) delete m_file;
    m_file = new TFile(m_filename.c_str());
    if (!m_file) {
	m_lg->Err("ss", "File doesnot exist -->", m_filename.c_str());
	exit(-1);
    }
    TTree* tree = (TTree*)m_file->Get(m_treename.c_str());
    if (!tree) {
	m_lg->Err("ss", "Tree doesnot exist -->", m_treename.c_str());
	exit(-1);
    }

    // Set object pointer
    scaleFactor_BTAG_bTagVarUp = 0;
    scaleFactor_BTAG_bTagVarDown = 0;
    scaleFactor_BTAG_cTagVarUp = 0;
    scaleFactor_BTAG_cTagVarDown = 0;
    scaleFactor_BTAG_misTagVarUp = 0;
    scaleFactor_BTAG_misTagVarDown = 0;
    mc_m = 0;
    mc_pt = 0;
    mc_eta = 0;
    mc_phi = 0;
    mc_status = 0;
    mc_barcode = 0;
    mc_pdgId = 0;
    mc_charge = 0;
    mc_parent_index = 0;
    mc_child_index = 0;
    // Set branch addresses and branch pointers
    if (!tree) return;
    fChain = tree;
    fCurrent = -1;
    fChain->SetMakeClass(1);
    
    fChain->SetBranchAddress("runNumber", &runNumber, &b_runNumber);
    fChain->SetBranchAddress("eventNumber", &eventNumber, &b_eventNumber);
    fChain->SetBranchAddress("channelNumber", &channelNumber, &b_channelNumber);
    fChain->SetBranchAddress("rndRunNumber", &rndRunNumber, &b_rndRunNumber);
    fChain->SetBranchAddress("hfor", &hfor, &b_hfor);
    fChain->SetBranchAddress("mu", &mu, &b_mu);
    fChain->SetBranchAddress("vxp_z", &vxp_z, &b_vxp_z);
    fChain->SetBranchAddress("flag_DL", &flag_DL, &b_flag_DL);
    fChain->SetBranchAddress("channel_DL", &channel_DL, &b_channel_DL);
    fChain->SetBranchAddress("pvxp_n", &pvxp_n, &b_pvxp_n);
    fChain->SetBranchAddress("mcWeight", &mcWeight, &b_mcWeight);
    fChain->SetBranchAddress("lep_pt", &lep_pt, &b_lep_pt);
    fChain->SetBranchAddress("lep_trigMatch", &lep_trigMatch, &b_lep_trigMatch);
    fChain->SetBranchAddress("lep_eta", &lep_eta, &b_lep_eta);
    fChain->SetBranchAddress("lep_phi", &lep_phi, &b_lep_phi);
    fChain->SetBranchAddress("lep_E", &lep_E, &b_lep_E);
    fChain->SetBranchAddress("lep_charge", &lep_charge, &b_lep_charge);
    fChain->SetBranchAddress("jet_n", &jet_n, &b_jet_n);
    fChain->SetBranchAddress("jet_pt", jet_pt, &b_jet_pt);
    fChain->SetBranchAddress("jet_eta", jet_eta, &b_jet_eta);
    fChain->SetBranchAddress("jet_phi", jet_phi, &b_jet_phi);
    fChain->SetBranchAddress("jet_E", jet_E, &b_jet_E);
    fChain->SetBranchAddress("jet_trueflav", jet_trueflav, &b_jet_trueflav);
    fChain->SetBranchAddress("jet_SV0", jet_SV0, &b_jet_SV0);
    fChain->SetBranchAddress("jet_MV1", jet_MV1, &b_jet_MV1);
    fChain->SetBranchAddress("met_et", &met_et, &b_met_et);
    fChain->SetBranchAddress("met_phi", &met_phi, &b_met_phi);
    fChain->SetBranchAddress("ph_n", &ph_n, &b_ph_n);
    fChain->SetBranchAddress("ph_E", ph_E, &b_ph_E);
    fChain->SetBranchAddress("ph_eta", ph_eta, &b_ph_eta);
    fChain->SetBranchAddress("ph_phi", ph_phi, &b_ph_phi);
    fChain->SetBranchAddress("ph_pt", ph_pt, &b_ph_pt);
    fChain->SetBranchAddress("ph_ptcone20", ph_ptcone20, &b_ph_ptcone20);
    fChain->SetBranchAddress("ph_topoEtcone40", ph_topoEtcone40, &b_ph_topoEtcone40);
    fChain->SetBranchAddress("ph_type", ph_type, &b_ph_type);
    fChain->SetBranchAddress("ph_origin", ph_origin, &b_ph_origin);
    fChain->SetBranchAddress("ph_truth_deltaRRecPhoton", ph_truth_deltaRRecPhoton, &b_ph_truth_deltaRRecPhoton);
    fChain->SetBranchAddress("ph_truth_E", ph_truth_E, &b_ph_truth_E);
    fChain->SetBranchAddress("ph_truth_pt", ph_truth_pt, &b_ph_truth_pt);
    fChain->SetBranchAddress("ph_truth_eta", ph_truth_eta, &b_ph_truth_eta);
    fChain->SetBranchAddress("ph_truth_phi", ph_truth_phi, &b_ph_truth_phi);
    fChain->SetBranchAddress("ph_truth_type", ph_truth_type, &b_ph_truth_type);
    fChain->SetBranchAddress("ph_truth_status", ph_truth_status, &b_ph_truth_status);
    fChain->SetBranchAddress("ph_truth_barcode", ph_truth_barcode, &b_ph_truth_barcode);
    fChain->SetBranchAddress("ph_truth_mothertype", ph_truth_mothertype, &b_ph_truth_mothertype);
    fChain->SetBranchAddress("ph_truth_motherbarcode", ph_truth_motherbarcode, &b_ph_truth_motherbarcode);
    fChain->SetBranchAddress("ph_truth_index", ph_truth_index, &b_ph_truth_index);
    fChain->SetBranchAddress("ph_truth_matched", ph_truth_matched, &b_ph_truth_matched);
    fChain->SetBranchAddress("ph_truth_isConv", ph_truth_isConv, &b_ph_truth_isConv);
    fChain->SetBranchAddress("ph_truth_isBrem", ph_truth_isBrem, &b_ph_truth_isBrem);
    fChain->SetBranchAddress("ph_truth_isFromHardProc", ph_truth_isFromHardProc, &b_ph_truth_isFromHardProc);
    fChain->SetBranchAddress("ph_truth_isPhotonFromHardProc", ph_truth_isPhotonFromHardProc, &b_ph_truth_isPhotonFromHardProc);
    fChain->SetBranchAddress("scaleFactor_PILEUP", &scaleFactor_PILEUP, &b_scaleFactor_PILEUP);
    fChain->SetBranchAddress("scaleFactor_ELE", &scaleFactor_ELE, &b_scaleFactor_ELE);
    fChain->SetBranchAddress("scaleFactor_MUON", &scaleFactor_MUON, &b_scaleFactor_MUON);
    if (m_treename == "nominal") fChain->SetBranchAddress("scaleFactor_ELE_RECO_UP", &scaleFactor_ELE_RECO_UP, &b_scaleFactor_ELE_RECO_UP);
    if (m_treename == "nominal") fChain->SetBranchAddress("scaleFactor_ELE_RECO_DOWN", &scaleFactor_ELE_RECO_DOWN, &b_scaleFactor_ELE_RECO_DOWN);
    if (m_treename == "nominal") fChain->SetBranchAddress("scaleFactor_ELE_ID_UP", &scaleFactor_ELE_ID_UP, &b_scaleFactor_ELE_ID_UP);
    if (m_treename == "nominal") fChain->SetBranchAddress("scaleFactor_ELE_ID_DOWN", &scaleFactor_ELE_ID_DOWN, &b_scaleFactor_ELE_ID_DOWN);
    if (m_treename == "nominal") fChain->SetBranchAddress("scaleFactor_MUON_RECO_UP", &scaleFactor_MUON_RECO_UP, &b_scaleFactor_MUON_RECO_UP);
    if (m_treename == "nominal") fChain->SetBranchAddress("scaleFactor_MUON_RECO_DOWN", &scaleFactor_MUON_RECO_DOWN, &b_scaleFactor_MUON_RECO_DOWN);
    if (m_treename == "nominal") fChain->SetBranchAddress("scaleFactor_MUON_ID_UP", &scaleFactor_MUON_ID_UP, &b_scaleFactor_MUON_ID_UP);
    if (m_treename == "nominal") fChain->SetBranchAddress("scaleFactor_MUON_ID_DOWN", &scaleFactor_MUON_ID_DOWN, &b_scaleFactor_MUON_ID_DOWN);
    if (m_treename == "nominal") fChain->SetBranchAddress("scaleFactor_ELE_TRIGGER_UP", &scaleFactor_ELE_TRIGGER_UP, &b_scaleFactor_ELE_TRIGGER_UP);
    if (m_treename == "nominal") fChain->SetBranchAddress("scaleFactor_ELE_TRIGGER_DOWN", &scaleFactor_ELE_TRIGGER_DOWN, &b_scaleFactor_ELE_TRIGGER_DOWN);
    if (m_treename == "nominal") fChain->SetBranchAddress("scaleFactor_MUON_TRIGGER_UP", &scaleFactor_MUON_TRIGGER_UP, &b_scaleFactor_MUON_TRIGGER_UP);
    if (m_treename == "nominal") fChain->SetBranchAddress("scaleFactor_MUON_TRIGGER_DOWN", &scaleFactor_MUON_TRIGGER_DOWN, &b_scaleFactor_MUON_TRIGGER_DOWN);
    if (m_treename == "nominal") fChain->SetBranchAddress("scaleFactor_BTAG_bTagVarUp", &scaleFactor_BTAG_bTagVarUp, &b_scaleFactor_BTAG_bTagVarUp);
    if (m_treename == "nominal") fChain->SetBranchAddress("scaleFactor_BTAG_bTagVarDown", &scaleFactor_BTAG_bTagVarDown, &b_scaleFactor_BTAG_bTagVarDown);
    if (m_treename == "nominal") fChain->SetBranchAddress("scaleFactor_BTAG_cTagVarUp", &scaleFactor_BTAG_cTagVarUp, &b_scaleFactor_BTAG_cTagVarUp);
    if (m_treename == "nominal") fChain->SetBranchAddress("scaleFactor_BTAG_cTagVarDown", &scaleFactor_BTAG_cTagVarDown, &b_scaleFactor_BTAG_cTagVarDown);
    if (m_treename == "nominal") fChain->SetBranchAddress("scaleFactor_BTAG_misTagVarUp", &scaleFactor_BTAG_misTagVarUp, &b_scaleFactor_BTAG_misTagVarUp);
    if (m_treename == "nominal") fChain->SetBranchAddress("scaleFactor_BTAG_misTagVarDown", &scaleFactor_BTAG_misTagVarDown, &b_scaleFactor_BTAG_misTagVarDown);
    fChain->SetBranchAddress("scaleFactor_PHOTON", &scaleFactor_PHOTON, &b_scaleFactor_PHOTON);
    fChain->SetBranchAddress("scaleFactor_BTAG", &scaleFactor_BTAG, &b_scaleFactor_BTAG);
    fChain->SetBranchAddress("scaleFactor_TRIGGER", &scaleFactor_TRIGGER, &b_scaleFactor_TRIGGER);
    fChain->SetBranchAddress("scaleFactor_ZVERTEX", &scaleFactor_ZVERTEX, &b_scaleFactor_ZVERTEX);
    fChain->SetBranchAddress("scaleFactor_ALLPRETAG", &scaleFactor_ALLPRETAG, &b_scaleFactor_ALLPRETAG);
    fChain->SetBranchAddress("scaleFactor_ALLBTAG", &scaleFactor_ALLBTAG, &b_scaleFactor_ALLBTAG);
    if (m_treename == "nominal") fChain->SetBranchAddress("mc_n", &mc_n, &b_mc_n);
    if (m_treename == "nominal") fChain->SetBranchAddress("mc_m", &mc_m, &b_mc_m);
    if (m_treename == "nominal") fChain->SetBranchAddress("mc_pt", &mc_pt, &b_mc_pt);
    if (m_treename == "nominal") fChain->SetBranchAddress("mc_eta", &mc_eta, &b_mc_eta);
    if (m_treename == "nominal") fChain->SetBranchAddress("mc_phi", &mc_phi, &b_mc_phi);
    if (m_treename == "nominal") fChain->SetBranchAddress("mc_status", &mc_status, &b_mc_status);
    if (m_treename == "nominal") fChain->SetBranchAddress("mc_barcode", &mc_barcode, &b_mc_barcode);
    if (m_treename == "nominal") fChain->SetBranchAddress("mc_pdgId", &mc_pdgId, &b_mc_pdgId);
    if (m_treename == "nominal") fChain->SetBranchAddress("mc_charge", &mc_charge, &b_mc_charge);
    if (m_treename == "nominal") fChain->SetBranchAddress("mc_parent_index", &mc_parent_index, &b_mc_parent_index);
    if (m_treename == "nominal") fChain->SetBranchAddress("mc_child_index", &mc_child_index, &b_mc_child_index);
}

Bool_t RecoAnalyzor::Notify()
{
    m_lg->Info("s", "OPEN new tree!");
    return kTRUE;
}

void RecoAnalyzor::Show(Long64_t entry)
{
    m_lg->Info("sd", "SHOW event -->", (int)entry);
    if (!fChain) return;
    fChain->Show(entry);
}

void RecoAnalyzor::SetDataType(int dtype) {
    if (dtype == 0) {
	m_datatype = "Data";
	m_lg->Info("s", "Analysis DATA sample.");
    } else if (dtype == 1) {
        m_datatype = "MC";
	m_lg->Info("s", "Analysis MC sample.");
    } else {
	m_lg->Err("sd", "UNKNOWN data type index -->", dtype);
	exit(-1);
    }
}

void RecoAnalyzor::SetSaveName(string sname) {
    m_savename = sname;
}

void RecoAnalyzor::SetSaveOpt(string sopt) {
    m_saveoption = sopt;
}

void RecoAnalyzor::SetFileName(string fname) {
    m_filename = fname;
}

void RecoAnalyzor::SetTreeName(string tname) {
    m_treename = tname;
}

void RecoAnalyzor::SetSysName(string name) {
    m_sysname = name;
}

void RecoAnalyzor::SetChannel(int ich) {
    m_ich = ich;
    if (ich == 0) {
	m_channel = "El";
	m_lg->Info("s", "Analysis channel --> El");
    } else if (ich == 1) {
	m_channel = "Mu";
	m_lg->Info("s", "Analysis channel --> Mu");
    } else {
	m_lg->Err("sd", "UNKNOWN channel index -->", ich);
	exit(-1);
    }
}

void RecoAnalyzor::Reset() {
    m_nom_geq1ph = 0; 
    m_nom_eq1ph = 0;
    m_nom_zmass = 0;
    m_nom_drphj = 0;
    m_nom_drphl = 0;
    m_sys_geq1ph.clear(); 
    m_sys_eq1ph.clear();
    m_sys_zmass.clear();
    m_sys_drphj.clear();
    m_sys_drphl.clear();
    m_diff_geq1ph.clear(); 
    m_diff_eq1ph.clear();
    m_diff_zmass.clear();
    m_diff_drphj.clear();
    m_diff_drphl.clear();
}

void RecoAnalyzor::Loop()
{
    m_lg->Info("s", "START loop ....");
    if (fChain == 0) return;
    Long64_t nentries = fChain->GetEntriesFast();

    m_cnt_geq1ph = 0; 
    m_cnt_eq1ph = 0;
    m_cnt_zmass = 0;
    m_cnt_drphj = 0;
    m_cnt_drphl = 0;

    m_err_geq1ph = 0; 
    m_err_eq1ph = 0;
    m_err_zmass = 0;
    m_err_drphj = 0;
    m_err_drphl = 0;

    Float_t bins[] = {0, 1, 3, 5, 10, 20};
    Int_t binnum = sizeof(bins)/sizeof(Float_t) - 1;
    TH1F *h_ptcone20 = new TH1F((string("PtCone20")+string("_")+m_channel+string("_")+m_sysname).c_str(), 
			        (string("PtCone20")+string("_")+m_channel+string("_")+m_sysname).c_str(),
				binnum, bins);
    h_ptcone20->Sumw2();

    TH1F* h_cutflow = new TH1F((string("Cutflow")+string("_")+m_channel+string("_")+m_sysname).c_str(),
			       (string("Cutflow")+string("_")+m_channel+string("_")+m_sysname).c_str(),
			       5, 0, 5);

    TH1F* h_el_leppt = new TH1F("el_leppt", "el_leppt", 30, 0, 300);
    TH1F* h_el_lepeta = new TH1F("el_lepeta", "el_lepeta", 30, -3, 3);
    TH1F* h_mu_leppt = new TH1F("mu_leppt", "mu_leppt", 30, 0, 300);
    TH1F* h_mu_lepeta = new TH1F("mu_lepeta", "mu_lepeta", 30, -3, 3);
    TH1F* h_el_jetpt = new TH1F("el_jetpt", "el_jetpt", 50, 0, 500);
    TH1F* h_el_jeteta = new TH1F("el_jeteta", "el_jeteta", 30, -3, 3);
    TH1F* h_mu_jetpt = new TH1F("mu_jetpt", "mu_jetpt", 50, 0, 500);
    TH1F* h_mu_jeteta = new TH1F("mu_jeteta", "mu_jeteta", 30, -3, 3);
    TH1F* h_el_phpt = new TH1F("el_phpt", "el_phpt", 30, 0, 300);
    TH1F* h_el_pheta = new TH1F("el_pheta", "el_pheta", 30, -3, 3);
    TH1F* h_mu_phpt = new TH1F("mu_phpt", "mu_phpt", 30, 0, 300);
    TH1F* h_mu_pheta = new TH1F("mu_pheta", "mu_pheta", 30, -3, 3);
    TH1F* h_el_drphl = new TH1F("el_drphl", "el_drphl", 40, 0, 8);
    TH1F* h_mu_drphl = new TH1F("mu_drphl", "mu_drphl", 40, 0, 8);
    TH1F* h_el_drphj = new TH1F("el_drphj", "el_drphj", 40, 0, 8);
    TH1F* h_mu_drphj = new TH1F("mu_drphj", "mu_drphj", 40, 0, 8);
    TH1F* h_el_mphl = new TH1F("el_mphl", "el_mphl", 30, 0, 300);
    TH1F* h_mu_mphl = new TH1F("mu_mphl", "mu_mphl", 30, 0, 300);
    TH1F* h_el_met = new TH1F("el_met", "el_met", 50, 0, 500);
    TH1F* h_mu_met = new TH1F("mu_met", "mu_met", 50, 0, 500);

    h_el_leppt->Sumw2();
    h_el_lepeta->Sumw2();
    h_mu_leppt->Sumw2();
    h_mu_lepeta->Sumw2();
    h_el_jetpt->Sumw2();
    h_el_jeteta->Sumw2();
    h_mu_jetpt->Sumw2();
    h_mu_jeteta->Sumw2();
    h_el_phpt->Sumw2();
    h_el_pheta->Sumw2();
    h_mu_phpt->Sumw2();
    h_mu_pheta->Sumw2();
    h_el_drphl->Sumw2();
    h_mu_drphl->Sumw2();
    h_el_drphj->Sumw2();
    h_mu_drphj->Sumw2();
    h_el_mphl->Sumw2();
    h_mu_mphl->Sumw2();
    h_el_met->Sumw2();
    h_mu_met->Sumw2();

    Long64_t nbytes = 0, nb = 0;
    int badweight = 0;
    for (Long64_t jentry=0; jentry<nentries;jentry++) {
	Long64_t ientry = LoadTree(jentry);
	if (ientry < 0) break;
    	nb = fChain->GetEntry(jentry);   nbytes += nb;

	float weight;
	if (m_datatype == "Data") weight = 1;
	else {
	    weight = mcWeight * scaleFactor_PILEUP * scaleFactor_PHOTON * scaleFactor_ZVERTEX;
	    
	    if (m_ich == 0) {
	    	if (m_sysname == "ELE_RECO_UP") weight *= scaleFactor_ELE_RECO_UP;
	    	else if (m_sysname == "ELE_RECO_DOWN") weight *= scaleFactor_ELE_RECO_DOWN;
	    	else if (m_sysname == "ELE_ID_UP") weight *= scaleFactor_ELE_ID_UP;
	    	else if (m_sysname == "ELE_ID_DOWN") weight *= scaleFactor_ELE_ID_DOWN;
		else weight *= scaleFactor_ELE;
	    	
	    	if (m_sysname == "ELE_TRIGGER_UP") weight *= scaleFactor_ELE_TRIGGER_UP;
	    	else if (m_sysname == "ELE_TRIGGER_DOWN") weight *= scaleFactor_ELE_TRIGGER_DOWN;
	    	else weight *= scaleFactor_TRIGGER;
	    } else if (m_ich == 1) {
	    	if (m_sysname == "MUON_RECO_UP") weight *= scaleFactor_MUON_RECO_UP;
	    	else if (m_sysname == "MUON_RECO_DOWN") weight *= scaleFactor_MUON_RECO_DOWN;
	    	else if (m_sysname == "MUON_ID_UP") weight *= scaleFactor_MUON_ID_UP;
	    	else if (m_sysname == "MUON_ID_DOWN") weight *= scaleFactor_MUON_ID_DOWN;
		else weight *= scaleFactor_MUON;
	    	
	    	if (m_sysname == "MUON_TRIGGER_UP") weight *= scaleFactor_MUON_TRIGGER_UP;
	    	else if (m_sysname == "MUON_TRIGGER_DOWN") weight *= scaleFactor_MUON_TRIGGER_DOWN;
	    	else weight *= scaleFactor_TRIGGER;
	    }
	    
	    if (m_sysname.find("BTAG_bTagVarUp",0) != string::npos) {
	        float weighttmp = 1;
		string varid = m_sysname.substr(14, m_sysname.size() - 14);
		int varid2 = atoi(varid.c_str());
	        weighttmp *= scaleFactor_BTAG_bTagVarUp->at(varid2-1);
	        weight *= weighttmp;
	    } else if (m_sysname.find("BTAG_bTagVarDown",0) != string::npos) {
	        float weighttmp = 1;
		string varid = m_sysname.substr(16, m_sysname.size() - 16);
		int varid2 = atoi(varid.c_str());
	        weighttmp *= scaleFactor_BTAG_bTagVarDown->at(varid2-1);
	        weight *= weighttmp;
	    } else if (m_sysname.find("BTAG_cTagVarUp",0) != string::npos) {
	        float weighttmp = 1;
		string varid = m_sysname.substr(14, m_sysname.size() - 14);
		int varid2 = atoi(varid.c_str());
		weighttmp *= scaleFactor_BTAG_cTagVarUp->at(varid2-1);
	        weight *= weighttmp;
	    } else if (m_sysname.find("BTAG_cTagVarDown",0) != string::npos) {
	        float weighttmp = 1;
		string varid = m_sysname.substr(16, m_sysname.size() - 16);
		int varid2 = atoi(varid.c_str());
	        weighttmp *= scaleFactor_BTAG_cTagVarDown->at(varid2-1);
	        weight *= weighttmp;
	    } else if (m_sysname.find("BTAG_misTagVarUp",0) != string::npos) {
	        float weighttmp = 1;
		string varid = m_sysname.substr(16, m_sysname.size() - 16);
		int varid2 = atoi(varid.c_str());
	        weighttmp *= scaleFactor_BTAG_misTagVarUp->at(varid2-1);
	        weight *= weighttmp;
	    } else if (m_sysname.find("BTAG_misTagVarDown",0) != string::npos) {
	        float weighttmp = 1;
		string varid = m_sysname.substr(18, m_sysname.size() - 18);
		int varid2 = atoi(varid.c_str());
	        weighttmp *= scaleFactor_BTAG_misTagVarDown->at(varid2-1);
	        weight *= weighttmp;
	    }
	    else weight *= scaleFactor_BTAG;
	}

	if (!(ph_n >= 1)) continue;
	if (weight != weight) {
	    badweight++;
	    weight = 0;
	}
    	m_cnt_geq1ph += weight;
    	m_err_geq1ph += weight*weight;

	if (!(ph_n == m_CUT_ph_n && ph_pt[0] >= m_CUT_ph_pt)) continue;
	m_cnt_eq1ph += weight;
	m_err_eq1ph += weight*weight;

	TLorentzVector tlv_lep_rec;
      	tlv_lep_rec.SetPtEtaPhiE(lep_pt, lep_eta, lep_phi, lep_E);

      	TLorentzVector tlv_ph_rec;
      	tlv_ph_rec.SetPtEtaPhiE(ph_pt[0], ph_eta[0], ph_phi[0], ph_E[0]);

      	double mphl = (tlv_lep_rec+tlv_ph_rec).M();

      	if (m_ich == 0) {
      	    bool passzmass = fabs(mphl - m_zmass) > m_CUT_zwindow;
      	    if (!passzmass) continue;
      	}
      	m_cnt_zmass += weight;
      	m_err_zmass += weight*weight;

	bool passphj = true;
	float drphj = -1;
      	for (int i = 0; i < jet_n; i++) {
	    TLorentzVector tmpj;
      	    tmpj.SetPtEtaPhiE(jet_pt[i], jet_eta[i], jet_phi[i], jet_E[i]);
	    if (i == 0) drphj = tmpj.DeltaR(tlv_ph_rec);
      	    if (tmpj.DeltaR(tlv_ph_rec) < m_CUT_dr_phj) {
		passphj = false;
      	       	break;
      	    }
      	}

      	if (!passphj) continue;
      	m_cnt_drphj += weight;
      	m_err_drphj += weight*weight;

	float drphl = tlv_ph_rec.DeltaR(tlv_lep_rec);
	if (!(tlv_ph_rec.DeltaR(tlv_lep_rec) >= m_CUT_dr_phl)) continue;
	m_cnt_drphl += weight;
	m_err_drphl += weight*weight;

	if (ph_ptcone20[0] >= 20000) h_ptcone20->Fill(15, weight);
      	else h_ptcone20->Fill(ph_ptcone20[0]/1000., weight);

	if (m_channel == "El") {
	    h_el_leppt->Fill(lep_pt/1000., weight);
	    h_el_lepeta->Fill(lep_eta, weight);
	    h_el_phpt->Fill(ph_pt[0]/1000., weight);
	    h_el_pheta->Fill(ph_eta[0], weight);
	    h_el_jetpt->Fill(jet_pt[0]/1000., weight);
	    h_el_jeteta->Fill(jet_eta[0], weight);
	    h_el_drphj->Fill(drphj, weight);
	    h_el_drphl->Fill(drphl, weight);
	    h_el_mphl->Fill(mphl/1000., weight);
	    h_el_met->Fill(met_et/1000., weight);
	} else if (m_channel == "Mu") {
	    h_mu_leppt->Fill(lep_pt/1000., weight);
	    h_mu_lepeta->Fill(lep_eta, weight);
	    h_mu_phpt->Fill(ph_pt[0]/1000., weight);
	    h_mu_pheta->Fill(ph_eta[0], weight);
	    h_mu_jetpt->Fill(jet_pt[0]/1000., weight);
	    h_mu_jeteta->Fill(jet_eta[0], weight);
	    h_mu_drphj->Fill(drphj, weight);
	    h_mu_drphl->Fill(drphl, weight);
	    h_mu_mphl->Fill(mphl/1000., weight);
	    h_mu_met->Fill(met_et/1000., weight);
	}
    }
    m_err_geq1ph = sqrt(m_err_geq1ph);
    m_err_eq1ph = sqrt(m_err_eq1ph);
    m_err_zmass = sqrt(m_err_zmass);
    m_err_drphj = sqrt(m_err_drphj);
    m_err_drphl = sqrt(m_err_drphl);
    h_cutflow->SetBinContent(1, m_cnt_geq1ph);
    h_cutflow->SetBinContent(2, m_cnt_eq1ph);
    h_cutflow->SetBinContent(3, m_cnt_zmass);
    h_cutflow->SetBinContent(4, m_cnt_drphj);
    h_cutflow->SetBinContent(5, m_cnt_drphl);
    h_cutflow->SetBinError(1, m_err_geq1ph);
    h_cutflow->SetBinError(2, m_err_eq1ph);
    h_cutflow->SetBinError(3, m_err_zmass);
    h_cutflow->SetBinError(4, m_err_drphj);
    h_cutflow->SetBinError(5, m_err_drphl);
    if (m_sysname == "Nominal") {
	m_nom_geq1ph = m_cnt_geq1ph;
	m_nom_eq1ph = m_cnt_eq1ph;
	m_nom_zmass = m_cnt_zmass;
	m_nom_drphj = m_cnt_drphj;
	m_nom_drphl = m_cnt_drphl;
    } else {
	m_sys_geq1ph.insert(pair<string,float>(m_sysname, m_cnt_geq1ph));
	m_sys_eq1ph.insert (pair<string,float>(m_sysname, m_cnt_eq1ph ));
	m_sys_zmass.insert (pair<string,float>(m_sysname, m_cnt_zmass ));
	m_sys_drphj.insert (pair<string,float>(m_sysname, m_cnt_drphj ));
	m_sys_drphl.insert (pair<string,float>(m_sysname, m_cnt_drphl ));
	
	if (m_nom_geq1ph != 0) {
	    m_diff_geq1ph.insert(pair<string,float>(m_sysname, 100*(m_cnt_geq1ph - m_nom_geq1ph)/m_nom_geq1ph));
	    m_diff_eq1ph.insert( pair<string,float>(m_sysname,  100*(m_cnt_eq1ph  - m_nom_eq1ph )/m_nom_eq1ph) );
	    m_diff_zmass.insert( pair<string,float>(m_sysname,  100*(m_cnt_zmass  - m_nom_zmass )/m_nom_zmass) );
	    m_diff_drphj.insert( pair<string,float>(m_sysname,  100*(m_cnt_drphj  - m_nom_drphj )/m_nom_drphj) );
	    m_diff_drphl.insert( pair<string,float>(m_sysname,  100*(m_cnt_drphl  - m_nom_drphl )/m_nom_drphl) );
	}
    }

    m_lg->Info("s", "------------- Analysis Result Summary ---------- ");
    m_lg->Info("ss", "Analysis Channel -->", m_channel.c_str());
    m_lg->Info("ss", "Analysis Type    -->", m_datatype.c_str());
    m_lg->Info("ss", "Systematic Type  -->", m_sysname.c_str());
    if (badweight != 0) m_lg->Warn("sd", "Bad weight Number-->", badweight);
    if (m_sysname == "Nominal") 
    m_lg->Info("sfs", "Signal Efficiency-->", 100*m_cnt_drphl/2991482., "%");
    if (m_sysname != "Nominal" && m_nom_geq1ph != 0) {
	m_lg->InfoFixW("dsdfdsdfdsdfds", 30, "CUT >=1 photon   -->", 15, m_cnt_geq1ph, 5, "+/-", 15, m_err_geq1ph, 10, "-->", 15, m_diff_geq1ph[m_sysname], 1, "%");
    	m_lg->InfoFixW("dsdfdsdfdsdfds", 30, "CUT ==1 photon   -->", 15, m_cnt_eq1ph,  5, "+/-", 15, m_err_eq1ph , 10, "-->", 15, m_diff_eq1ph[m_sysname] , 1, "%");
    	m_lg->InfoFixW("dsdfdsdfdsdfds", 30, "CUT off zwindow  -->", 15, m_cnt_zmass,  5, "+/-", 15, m_err_zmass , 10, "-->", 15, m_diff_zmass[m_sysname] , 1, "%");
    	m_lg->InfoFixW("dsdfdsdfdsdfds", 30, "CUT overlap phj  -->", 15, m_cnt_drphj,  5, "+/-", 15, m_err_drphj , 10, "-->", 15, m_diff_drphj[m_sysname] , 1, "%");
    	m_lg->InfoFixW("dsdfdsdfdsdfds", 30, "CUT overlap phl  -->", 15, m_cnt_drphl,  5, "+/-", 15, m_err_drphl , 10, "-->", 15, m_diff_drphl[m_sysname] , 1, "%");
    } else {
	m_lg->InfoFixW("dsdfdsdf", 30, "CUT >=1 photon   -->", 15, m_cnt_geq1ph, 5, "+/-", 15, m_err_geq1ph);
    	m_lg->InfoFixW("dsdfdsdf", 30, "CUT ==1 photon   -->", 15, m_cnt_eq1ph,  5, "+/-", 15, m_err_eq1ph);
    	m_lg->InfoFixW("dsdfdsdf", 30, "CUT off zwindow  -->", 15, m_cnt_zmass,  5, "+/-", 15, m_err_zmass);
    	m_lg->InfoFixW("dsdfdsdf", 30, "CUT overlap phj  -->", 15, m_cnt_drphj,  5, "+/-", 15, m_err_drphj);
    	m_lg->InfoFixW("dsdfdsdf", 30, "CUT overlap phl  -->", 15, m_cnt_drphl,  5, "+/-", 15, m_err_drphl);
    }
    m_lg->Info("s", "------------------------------------------------ ");

    TFile* tmpf = new TFile(m_savename.c_str(), m_saveoption.c_str());

    h_ptcone20->Write();
    h_cutflow->Write();

    if (m_channel == "El") {
	h_el_leppt->Write();
    	h_el_lepeta->Write();
    	h_el_jetpt->Write();
    	h_el_jeteta->Write();
    	h_el_phpt->Write();
    	h_el_pheta->Write();
    	h_el_drphl->Write();
    	h_el_drphj->Write();
    	h_el_mphl->Write();
    	h_el_met->Write();
    } else if (m_channel == "Mu") {
	h_mu_leppt->Write();
    	h_mu_lepeta->Write();
    	h_mu_jetpt->Write();
    	h_mu_jeteta->Write();
    	h_mu_phpt->Write();
    	h_mu_pheta->Write();
    	h_mu_drphl->Write();
    	h_mu_drphj->Write();
    	h_mu_mphl->Write();
    	h_mu_met->Write();
    }

    tmpf->Close();
    delete tmpf;
    delete h_ptcone20;
    delete h_cutflow;
    m_lg->Info("s", "LOOP ended.");
    m_lg->NewLine();
}
