#include "PlotComparor.h"
#include "AtlasStyle.h"
#include "AtlasUtils.h"
#include "AtlasLabels.h"
#include <TLegend.h>
#include <THStack.h>
#include <TLatex.h>
#include <TMarker.h>
#include <TFile.h>
#include <TMath.h>
#include <TColor.h>
#include <TLine.h>
#include <TCanvas.h>

using namespace std;

int FindRemoveMax(vector<double> &input) {
    int pos = 0;
    double max = -1;
    for (int i = 0; i < input.size(); i++) {
	if (input.at(i) > max) {
	    max = input.at(i); 
	    pos = i;
	}
    }
    input.at(pos) = -1;
    return pos;
}

void OrderFVector(vector<double> fv, vector<int> &order) {
    order.clear();
    for (int i = 0; i < fv.size(); i++) {
	order.push_back(FindRemoveMax(fv));
    }
}

void OrderBins(TH1F* h, vector<int> order) {
    TH1F* h_tmp = (TH1F*)h->Clone();
    for (int i = 1; i <= h->GetNbinsX(); i++) {
	h->SetBinContent(i, h_tmp->GetBinContent(order.at(i-1)));
	h->SetBinError(i, h_tmp->GetBinError(order.at(i-1)));
    }
    delete h_tmp;
}

void PlotComparor::SetATLASFlag(int flag) {
    atlas_flag = flag;
}

PlotComparor::PlotComparor() {
    SetAtlasStyle();

    // flag
    atlas_flag = 1;
    m_norm = false;
    m_cutbins = false;
    m_logy = false;
    m_autorange = true;
    m_draw_dhline = false;
    m_overflow = false;
    m_normbinwidth = false;
    m_do_ratio = false;
    m_fix_ratio_range = false;
    m_atlas_logo = true;
    m_is_data = true;
    m_is_simulation = false;
    m_is_13tev = false;
    m_is_14tev = false;
    m_showlumi = true;
    m_stack_reverse = false;
    m_stack_mc_only = false;
    m_save_ratio = false;
    m_x_label_v = false;
    m_datamc_sf = false;
    
    m_normMCtodata = false;
    m_line_width = 0;

    // log
    m_lhc_info = ""; //, 20.2 fb^{-1}";
    m_logy_min = 0.001;
    m_range_y_low = 9999;
    m_range_y_high = 9999;
    m_logo_xshift = 0;
    m_lg_xshift = 0;
    m_lg_xshift2 = 0;
    m_lg_yshift = 0;
    m_logy_scale = 20;

    // canvas
    m_canvas_l = 800;
    m_canvas_h = 600;
    m_c_square = false;
    m_ypos_dhline = 0;
    m_rightmargin = -1;

    // save
    m_savename = "";
    m_savedir = "";

    // legend
    m_no_lg = false;
    m_startcolor = -1;
    m_lg_right = true;
    m_lg_pos[0] = 0.68;
    m_lg_pos[1] = 0.65;
    m_lg_pos[2] = 0.9;
    m_lg_pos[3] = 0.9;
    m_lg_column = 1;
    m_lg_textsize = 0.045;
    m_lg_basename = "nominal";
    m_lg_altername = "alternative";
    m_basecolor = -1;
    m_lg_scale_x = 1.0;
    m_forced_lgpos = false;
    m_lgpos_manual = false;

    // latex
    m_lt_x = 0.2;
    m_lt_y = 0.85;
    m_channel = "Undefined";
    m_type = "";

    // title
    m_xtitle = "";
    m_ytitle = "";
    m_ytitle_offset = -1;
    m_xtitle_size = -999;
    m_ratio_ytitle = "Ratio";

    m_yrange_ratio = 0;

    m_draw_option = "";

    h_base = NULL;
    h_alter = NULL;

    m_max_ratio = 2.;
    m_min_ratio = 0.;

    m_lg = new Logger("PlotComparor");
}

PlotComparor::~PlotComparor() {
};

void PlotComparor::ForcedLGPos(bool forced) {
    m_forced_lgpos = forced;
}

void PlotComparor::Print() {
    for (int i = 0; i < s_cumi.size(); i++) {
	cout << s_cumi.at(i) << endl;
    }
}

void PlotComparor::NoLegend() {
    m_no_lg = true;
};

void PlotComparor::SetRightMargin(double margin) {
    m_rightmargin = margin;
};

void PlotComparor::VerticleXLabel(bool vx) {
    m_x_label_v = vx;
};

void PlotComparor::OrderBin(bool order) {
    m_order_bin = order;
};

void PlotComparor::NormMCtoData(bool norm) {
    m_normMCtodata = norm;
};

double PlotComparor::Separation(TH1F* h2, TH1F* h1, int idx) {
    double s = 0;
    for (int i = 1; i <= h1->GetNbinsX(); i++) {
	if (h1->GetBinContent(i) + h2->GetBinContent(i) != 0)
	    s += 0.5*pow(h1->GetBinContent(i) - h2->GetBinContent(i),2)/(h1->GetBinContent(i) + h2->GetBinContent(i));
    }
    s_cumi.at(idx) += s;
    return s;
}

void PlotComparor::SaveRatio(bool save) {
    m_save_ratio = save;
};

void PlotComparor::SquareCanvas(bool square) {
    m_c_square = square;
};

void PlotComparor::Is13TeV(bool is13tev) {
    m_is_13tev = is13tev;
};

void PlotComparor::Is14TeV(bool is14tev, bool showlumi) {
    m_is_14tev = is14tev;
    m_showlumi = showlumi;
};

void PlotComparor::SetStartColor(int color) {
    m_startcolor = color;
};

void PlotComparor::DoubleYRange(bool dby) {
    m_dby = dby;
}

void PlotComparor::DataLumi(double lumi) {
    char tmp[100];
    if (m_is_13tev) sprintf(tmp, "#sqrt{s}=13TeV, %.1f fb^{-1}", lumi);
    else if (m_is_14tev) sprintf(tmp, "#sqrt{s} = 14 TeV, 3 ab^{-1}", lumi);
    else sprintf(tmp, "#sqrt{s}=8TeV, 20.3 fb^{-1}");
    m_lhc_info = tmp;
};

void PlotComparor::SetLHCInfo(string lhcinfo) {
    m_lhc_info = lhcinfo;
};

void PlotComparor::SetLineWidth(int width) {
    m_line_width = width;
};

TH1F * PlotComparor::AddOverflow(TH1F *h) {  
    UInt_t nbinsx    = h->GetNbinsX();
    double binlo = h->GetBinLowEdge(1);
    double binhi = h->GetBinLowEdge(nbinsx)+h->GetBinWidth(nbinsx);
    double step = (binhi - binlo) / nbinsx;
    string tempName= h->GetName();
    tempName += "OF";

    TH1F *htmp = new TH1F(tempName.c_str(), h->GetTitle(), nbinsx + 2, binlo - step, binhi + step);
    htmp->SetXTitle(h->GetXaxis()->GetTitle());
    htmp->SetYTitle(h->GetYaxis()->GetTitle());

    for (UInt_t i = 1; i <= nbinsx + 2; i++) {
         htmp->SetBinContent(i, h->GetBinContent(i-1));
         htmp->SetBinError(i, h->GetBinError(i-1));
    }  
    htmp->SetEntries(h->GetEntries());
    htmp->SetFillStyle(h->GetFillStyle());
    htmp->SetFillColor(h->GetFillColor());
    return htmp;
}

TH1F * PlotComparor::NormToBinWidth(TH1F *h) {  
    string tempName= h->GetName();
    tempName += "BW";

    TH1F *htmp = (TH1F*)h->Clone(tempName.c_str());
    htmp->SetXTitle(h->GetXaxis()->GetTitle());
    htmp->SetYTitle(h->GetYaxis()->GetTitle());

    for (UInt_t i = 1; i <= h->GetNbinsX(); i++) {
         htmp->SetBinContent(i, h->GetBinContent(i)/h->GetBinWidth(i));
         htmp->SetBinError(i, h->GetBinError(i)/h->GetBinWidth(i));
    }  
    htmp->SetEntries(h->GetEntries());
    htmp->SetFillStyle(h->GetFillStyle());
    htmp->SetFillColor(h->GetFillColor());
    return htmp;
}

TH1F * PlotComparor::Cutbins(TH1F *h) {  
    UInt_t nbinsx    = h->GetNbinsX();
    UInt_t nbins_s = h->FindBin(m_cut_lo) + 1;
    UInt_t nbins_e = h->FindBin(m_cut_hi) - 1;
    double binlo = h->GetBinLowEdge(nbins_s);
    double binhi = h->GetBinLowEdge(nbins_e) + h->GetBinWidth(nbins_e);
    //double binlo = h->GetBinLowEdge(1);
    //double binhi = h->GetBinLowEdge(nbinsx)+h->GetBinWidth(nbinsx);
    double step = (binhi - binlo) / (nbins_e - nbins_s);
    string tempName= h->GetName();
    tempName += "OF";

    TH1F *htmp = new TH1F(tempName.c_str(), h->GetTitle(), nbins_e - nbins_s, binlo, binhi);
    htmp->SetXTitle(h->GetXaxis()->GetTitle());
    htmp->SetYTitle(h->GetYaxis()->GetTitle());

    for (UInt_t i = nbins_s; i <= nbins_e; i++) {
	 htmp->GetXaxis()->SetBinLabel(i, h->GetXaxis()->GetBinLabel(i));
         htmp->SetBinContent(i, h->GetBinContent(i));
         htmp->SetBinError(i, h->GetBinError(i));
    }  
    htmp->SetEntries(h->GetEntries());
    htmp->SetFillStyle(h->GetFillStyle());
    htmp->SetFillColor(h->GetFillColor());
    return htmp;
}

void PlotComparor::LegendPosAutoAdjust(bool debug) {

    if (debug) m_lg->Info("s", "Adjust legend position");
    int total_item = 0;
    int max_name_length = 0;
    if (h_alters.size() != 0) {
	if (m_stack_mc_only) total_item = h_alters.size();
	else total_item = 1 + h_alters.size();
	max_name_length = max_name_length > m_lg_basename.size() ? max_name_length : m_lg_basename.size();
	for (int i = 0; i < h_alters.size(); i++) {
	    if (m_lg_alternames.at(i).size() > max_name_length) max_name_length = m_lg_alternames.at(i).size();
	}
    } else {
	total_item = 2;
	max_name_length = max_name_length > m_lg_basename.size() ? max_name_length : m_lg_basename.size();
	max_name_length = max_name_length > m_lg_altername.size() ? max_name_length : m_lg_altername.size();
    }

    bool right = true;

    if (debug) m_lg->Info("s", "Detemine left/right placing of legend");

    float diff_leftright = 0;
    diff_leftright = fabs(diff_leftright) > fabs(h_base->Integral(1, h_base->GetNbinsX()/2+1) - h_base->Integral(h_base->GetNbinsX()/2+2, h_base->GetNbinsX())) ? diff_leftright : h_base->Integral(1, h_base->GetNbinsX()/2+1) - h_base->Integral(h_base->GetNbinsX()/2+2, h_base->GetNbinsX());
    if (h_alter) diff_leftright = fabs(diff_leftright) > fabs(h_alter->Integral(1, h_alter->GetNbinsX()/2+1) - h_alter->Integral(h_alter->GetNbinsX()/2+2, h_alter->GetNbinsX())) ? diff_leftright : h_alter->Integral(1, h_alter->GetNbinsX()/2+1) - h_alter->Integral(h_alter->GetNbinsX()/2+2, h_alter->GetNbinsX());
    if (h_alters.size() != 0) {
	for (int i = 0; i < h_alters.size(); i++) {
	    diff_leftright = fabs(diff_leftright) > fabs(h_alters.at(i)->Integral(1, h_alters.at(i)->GetNbinsX()/2+1) - h_alters.at(i)->Integral(h_alters.at(i)->GetNbinsX()/2+2, h_alters.at(i)->GetNbinsX())) ? diff_leftright : h_alters.at(i)->Integral(1, h_alters.at(i)->GetNbinsX()/2+1) - h_alters.at(i)->Integral(h_alters.at(i)->GetNbinsX()/2+2, h_alters.at(i)->GetNbinsX());
	}
    }

    if (diff_leftright > 0) {
	if (h_base->GetMaximumBin() == h_base->GetNbinsX()) diff_leftright = -1;
    }

    if (debug) m_lg->Info("s", "Calculate new legend position");

    float lg_dx = 0.9 - 0.68;
    float lg_dy = 0.9 - 0.6;
    if (m_forced_lgpos) diff_leftright = 1;
    if (diff_leftright > 0) {
	m_lg_pos[2] = 0.9;
	m_lg_pos[3] = 0.9;
	if (max_name_length > 15) {
	    m_lg_pos[0] = 0.9 - lg_dx * (max_name_length / 12.5) * m_lg_scale_x;
	    m_lg_scale_x = 1.0;
	} else m_lg_pos[0] = 0.70;
    	if (total_item >= 7) {
	    m_lg_pos[1] = 0.9 - lg_dy * (total_item / 5.5) / m_lg_column;
	    m_lg_textsize *= 0.7;
    	} else if (total_item >= 5) {
	    m_lg_pos[1] = 0.9 - lg_dy * (total_item / 5.6) / m_lg_column;
    	} else if (total_item >= 4) {
	    m_lg_pos[1] = 0.9 - lg_dy * (total_item / 4.) / m_lg_column;
    	} else if (total_item >= 3) {
	    m_lg_pos[1] = 0.9 - lg_dy * (total_item / 4.) / m_lg_column;
    	} else if (total_item >= 2) {
	    m_lg_pos[1] = 0.9 - lg_dy * (total_item / 3.5) / m_lg_column;
	} else {
	    m_lg_pos[1] = 0.9 - lg_dy * (total_item / 3.5)/ m_lg_column;
	}
	m_lt_x = 0.2;
	m_lg_right = true;
    } else {
	m_lg_pos[0] = 0.2;
    	m_lg_pos[2] = 0.2 + lg_dx * ((max_name_length > 15 ? max_name_length : 15) / 12.5) ;
    	//m_lg_pos[1] = m_lg_pos[3] - lg_dy * (total_item / 5.);
    	if (total_item >= 7) {
	    m_lg_pos[1] = 0.9 - lg_dy * (total_item / 5.5) / m_lg_column;
	    m_lg_textsize *= 0.7;
    	} else if (total_item >= 5) {
	    m_lg_pos[1] = 0.9 - lg_dy * (total_item / 5.6) / m_lg_column;
    	} else if (total_item >= 4) {
	    m_lg_pos[1] = 0.9 - lg_dy * (total_item / 4.) / m_lg_column;
    	} else if (total_item >= 3) {
	    m_lg_pos[1] = 0.9 - lg_dy * (total_item / 4.) / m_lg_column;
    	} else if (total_item >= 2) {
	    m_lg_pos[1] = 0.9 - lg_dy * (total_item / 3.5) / m_lg_column;
	} else {
	    m_lg_pos[1] = 0.9 - lg_dy * (total_item / 3.5)/ m_lg_column;
	}

	m_lt_x = 0.54;
	m_lg_right = false;
    }
}

void PlotComparor::SetLegendPos(float x1, float y1, float x2, float y2) {
    m_lgpos_manual = true;
    m_lg_pos[0] = x1;
    m_lg_pos[1] = y1;
    m_lg_pos[2] = x2;
    m_lg_pos[3] = y2;
}

void PlotComparor::DashHLine(float ypos, int color) {
    m_draw_dhline = true;
    m_ypos_dhline = ypos;
    m_color_dhline = color;
}

void PlotComparor::ShiftLegendX(float shift) {
    m_lg_xshift = shift;
}

void PlotComparor::ShiftLegendX2(float shift) {
    m_lg_xshift2 = shift;
}

void PlotComparor::ShiftLogoX(float shift) {
    m_logo_xshift = shift;
}

void PlotComparor::ShiftLegendY(float shift) {
    m_lg_yshift = shift;
}

string PlotComparor::ParseYtitle(string name) {
    string title;
    if (name.find("FR_",0) != string::npos) title = "F.R.";
    return title;
}

string PlotComparor::ParseXtitle(string name) {
    string title;
    if (name.find("LeadLepPt",0) != string::npos) title = "p_{T}^{lep,lead} [GeV]";
    else if (name.find("SubLepPt",0) != string::npos) title = "p_{T}^{sublead-l} [GeV]";
    else if (name.find("LeadPhAbsEta",0) != string::npos) title = "|#eta(#gamma)|";
    else if (name.find("LeadPhPtCone",0) != string::npos) title = "P_{T,#gamma}^{cone20} [GeV]";
    else if (name.find("MPhLep",0) != string::npos && name.find("MPhLepLep",0) == string::npos) title = "m_{l,#gamma} [GeV]";
    else if (name.find("MPhLepLep",0) != string::npos) title = "m_{ll,#gamma} [GeV]";
    else if (name.find("MLepLep",0) != string::npos) title = "m_{ll} [GeV]";
    else if (name.find("LeadLepEta",0) != string::npos) title = "#eta_{lead-l}";
    else if (name.find("Period",0) != string::npos) title = "Period";
    else if (name.find("Period",0) != string::npos) title = "Period";
    else if (name.find("Year2015",0) != string::npos) title = "Is 2015";
    else if (name.find("PileUpMu",0) != string::npos) title = "<#mu>";
    else if (name.find("M2J",0) != string::npos) title = "m_{jj} [GeV]";
    else if (name.find("DrLeadJetLeadLep",0) != string::npos) title = "#DeltaR_{lead-j,lead-l}";
    else if (name.find("DrLeadJetLeadPh",0) != string::npos) title = "#DeltaR_{lead-j,#gamma}";
    else if (name.find("LeadJetPt",0) != string::npos) title = "p_{T}^{jet,lead} [GeV]";
    else if (name.find("SubJetPt",0) != string::npos) title = "p_{T}^{jet,sl} [GeV]";
    else if (name.find("PhPt",0) != string::npos) title = "p_{T}(#gamma) [GeV]";
    else if (name.find("Ptbins",0) != string::npos) title = "p_{T}^{#gamma} [GeV]";
    else if (name.find("LepEta",0) != string::npos && name.find("_ejets",0) != string::npos) title = "#eta_{e}";
    else if (name.find("LepEta",0) != string::npos && name.find("_mujets",0) != string::npos) title = "#eta_{#mu}";
    else if (name.find("LeadJetEta",0) != string::npos) title = "#eta_{jet,l}";
    else if (name.find("SubJetEta",0) != string::npos) title = "#eta_{jet,sl}";
    else if (name.find("PhEta",0) != string::npos) title = "#eta_{#gamma}";
    else if (name.find("Etabins",0) != string::npos) title = "#eta_{#gamma}";
    else if (name.find("LeadPhMinDrPhLep",0) != string::npos) {
	if (m_channel == "ejets" || m_channel == "mujets") {
	    title = "#DeltaR(#gamma,#it{l})";
	} else {
	    title = "#DeltaR(#gamma,#it{l})_{min}";
	}
    }
    else if (name.find("DEtaLepLep",0) != string::npos) title = "|#Delta#eta(#it{l},#it{l})|";
    else if (name.find("DPhiLepLep",0) != string::npos) title = "#Delta#phi(#it{l},#it{l})";
    else if (name.find("LeadPhMinDrPhJet",0) != string::npos) title = "#DeltaR_{#gamma,jet}";
    else if (name.find("LeadLepMinDrPhJet",0) != string::npos && name.find("_ejets",0) != string::npos) title = "#DeltaR_{e,jet}";
    else if (name.find("LeadLepMinDrPhJet",0) != string::npos && name.find("_mujets",0) != string::npos) title = "#DeltaR_{#mu,jet}";
    else if (name.find("LeadPhLeadLepDphi",0) != string::npos && name.find("_ejets",0) != string::npos) title = "#Delta#phi_{#gamma,e}";
    else if (name.find("LeadPhLeadLepDphi",0) != string::npos && name.find("_mujets",0) != string::npos) title = "#Delta#phi_{#gamma,#mu}";
    else if (name.find("MPhLep",0) != string::npos&& name.find("mujets",0) != string::npos) title = "m(#gamma,#mu) [GeV]";
    else if (name.find("CA",0) != string::npos) title = "#Delta|y|";
    else if (name.find("LHBest",0) != string::npos) title = "LH (Best)";
    else if (name.find("TopPoleMass",0) != string::npos) title = "m_{t,pole} [GeV]";
    else if (name.find("HadronTopEta",0) != string::npos) title = "#eta_{t,had}";
    else if (name.find("HadronTopMass",0) != string::npos) title = "m_{t,had} [GeV]";
    else if (name.find("HadronTopPt",0) != string::npos) title = "pT_{t,had} [GeV]";
    else if (name.find("HadronWMass",0) != string::npos) title = "m_{W,had} [GeV]";
    else if (name.find("LeptonTopEta",0) != string::npos) title = "#eta_{t,lep}";
    else if (name.find("LeptonTopMass",0) != string::npos) title = "m_{t,lep} [GeV]";
    else if (name.find("LeptonTopPt",0) != string::npos) title = "pT_{t,lep} [GeV]";
    else if (name.find("LeptonWMass",0) != string::npos) title = "m_{W,lep} [GeV]";
    else if (name.find("dPhiLepMET",0) != string::npos) title = "#Delta#phi(l,E_{T}^{miss})";
    else if (name.find("dPhiPhMET",0) != string::npos) title = "#Delta#phi(#gamma,E_{T}^{miss})";
    else if (name.find("MET",0) != string::npos) title = "E_{T}^{miss} [GeV]";
    else if (name.find("MWT",0) != string::npos) title = "m_{W,T} [GeV]";
    else if (name.find("Njet",0) != string::npos) title = "N_{jet}";
    else if (name.find("Nbjet",0) != string::npos) title = "N_{bjet}";
    else if (name.find("LeadPhCvt",0) != string::npos) title = "Converted #gamma";
    else if (name.find("PhRHad1",0) != string::npos) title = "R_{had1}";
    else if (name.find("PhRHad",0) != string::npos) title = "R_{had}";
    else if (name.find("PhREta",0) != string::npos) title = "R_{#eta}";
    else if (name.find("PhRPhi",0) != string::npos) title = "R_{#phi}";
    else if (name.find("PhWEta2",0) != string::npos) title = "w_{#eta2}";
    else if (name.find("PhWEta",0) != string::npos) title = "w_{#eta1}";
    else if (name.find("PhfSide",0) != string::npos) title = "f_{side}";
    else if (name.find("PPT",0) != string::npos) title = "PPT";
    //if (name.find("Match_LepPt_",0) != string::npos && name.find("_EJ_",0) != string::npos) title = "matched p_{T}^{e} [GeV]";
    //if (name.find("Match_LepPt_",0) != string::npos && name.find("_MJ_",0) != string::npos) title = "matched p_{T}^{#mu} [GeV]";
    //if (name.find("Match_PhPt_",0) != string::npos) title = "matched p_{T}^{#gamma} [GeV]";
    return title;
}

void PlotComparor::NormToUnit(bool norm) {
    m_norm = norm;
}

void PlotComparor::ReverseStackOrder(bool reverse) {
    m_stack_reverse = reverse;
}

void PlotComparor::IsSimulation(bool issimu) {
    m_is_simulation = issimu;
    m_is_data = !m_is_simulation;
}

void PlotComparor::SetLogo(string logo) {
    m_logo = logo;
    m_atlas_logo = false;
}

void PlotComparor::SetYRangeRatio(float ratio) {
    m_yrange_ratio = ratio;
}

void PlotComparor::SetLogyScale(float scale) {
    m_logy_scale = scale;
}

void PlotComparor::HasData(bool hasdata) {
    m_is_data = hasdata;
    m_is_simulation = !m_is_data;
}

void PlotComparor::UseLogy(bool logy, double min, double scale) {
    m_logy = logy;
    m_logy_min = min;
    m_logy_scale = scale;
}

void PlotComparor::LogyMin(double min) {
    m_logy_min = min;
}

void PlotComparor::ShowOverflow(bool of) {
    m_overflow = of;
}

void PlotComparor::NormBinWidth(bool norm) {
    m_normbinwidth = norm;
}

void PlotComparor::DoCutbins(double cut_lo, double cut_hi) {
    m_cutbins = true;
    m_cut_lo = cut_lo;
    m_cut_hi = cut_hi;
}

void PlotComparor::AutoRange(bool range) {
    m_autorange = range;
}

void PlotComparor::SetLegendNC(int nc) {
    m_lg_column = nc;
}

void PlotComparor::SetLegendScaleX(double scale) {
    m_lg_scale_x = scale;
}

void PlotComparor::SetLegendTS(float size) {
    m_lg_textsize = size;
}

void PlotComparor::SetDrawOption(string option) {
    m_draw_option = option;
}

void PlotComparor::SetChannel(string channel) {
    m_channel = channel;
}

void PlotComparor::SetType(string type) {
    m_type = type;
}

void PlotComparor::SetType(int type) {
    if (type == 0) m_type = "Inclusive";
    if (type == 1) m_type = "15 #leq p_T < 25";
    if (type == 2) m_type = "25 #leq p_T < 40";
    if (type == 3) m_type = "40 #leq p_T < 60";
    if (type == 4) m_type = "60 #leq p_T < 100";
    if (type == 5) m_type = "100 #leq p_T < 300";
    if (type == 6) m_type = "0 #leq |#eta| < 0.25";
    if (type == 7) m_type = "0.25 #leq |#eta| < 0.55";
    if (type == 8) m_type = "0.55 #leq |#eta| < 0.90";
    if (type == 9) m_type = "0.90 #leq |#eta| < 1.37";
    if (type == 10) m_type = "1.37 #leq |#eta| < 2.37";
}

void PlotComparor::SetYtitle(string title) {
    m_ytitle = title;
}

void PlotComparor::SetYtitleOffset(double offset) {
    m_ytitle_offset = offset;
}

void PlotComparor::SetRatioYtitle(string title) {
    m_ratio_ytitle = title;
}

void PlotComparor::SetXtitle(string title) {
    m_xtitle = title;
}

void PlotComparor::SetXtitleSize(float titlesize) {
    m_xtitle_size = titlesize;
}

void PlotComparor::SetBaseHName(string name) {
    m_lg_basename = name;
}

void PlotComparor::SetAlterHName(string name) {
    m_lg_altername = name;
}

void PlotComparor::AddAlterHName(string name) {
    m_lg_alternames.push_back(name);
}

void PlotComparor::AddAlterColors(int i) {
    m_altercolors.push_back(i);
}

void PlotComparor::SetBaseH(TH1F*h) {
    h_base = h;
}

void PlotComparor::SetAlterH(TH1F*h) {
    h_alter = h;
}

void PlotComparor::SetBaseColor(int i) {
    m_basecolor = i;
}

void PlotComparor::AddAlterH(TH1F*h) {
    h_alters.push_back(h);
    if (s_cumi.size() < h_alters.size()) {
	s_cumi.resize(h_alters.size());
	s_cumi.at(s_cumi.size()-1) = 0;
    }
}

void PlotComparor::ClearAlterHs() {
    h_base = 0;
    h_alter = 0;
    h_alters.clear();
    //if (h_base) delete h_base;
    //if (h_alter) delete h_alter;
    //for (int i = 0; i < h_alters.size(); i++) {
    //    if (h_alters.at(i)) delete h_alters.at(i);
    //}
    m_lg_alternames.clear();
    ClearAlterColors();
}

void PlotComparor::ClearAlterColors() {
    m_altercolors.clear();
}

void PlotComparor::SetSaveName(string name) {
    m_savename = name;
}

void PlotComparor::SetSaveDir(string dir) {
    m_savedir = dir;
}

void PlotComparor::DrawRatio(bool ratio) {
    m_do_ratio = ratio;
}

void PlotComparor::FixRatioRange(bool fix) {
    m_fix_ratio_range = fix;
}

void PlotComparor::SetMaxRatio(float max) {
    m_fix_ratio_range = true;
    m_max_ratio = max;
}

void PlotComparor::SetMinRatio(float min) {
    m_fix_ratio_range = true;
    m_min_ratio = min;
}

void PlotComparor::SetRangeY(float low, float high) {
    m_autorange = false;
    m_range_y_low = low;
    m_range_y_high = high;
}

void PlotComparor::PrintDataMCSF(bool datamc) {
    m_datamc_sf = datamc;
}

void PlotComparor::DrawStack(bool debug) {
    if (h_alters.size() <= 1) {
	m_lg->Err("s", "Stack mode should have multiple alternative histograms defined!");
	exit(-1);
    }

    if (h_base == NULL) {
	m_stack_mc_only = true;
	m_do_ratio = false;
	m_lg->Warn("s", "Empty data histogram! Will use show the MC stack!");
	for (int i = 0; i < h_alters.size(); i++) {
	    if (i == 0) h_base = (TH1F*)h_alters.at(i)->Clone();
	    else h_base->Add(h_alters.at(i));
	}
    }

    TCanvas *c = NULL;
    if (!m_c_square) {
	c = new TCanvas("", "", m_canvas_l, m_canvas_h);
    } else {
	c = new TCanvas("", "", m_canvas_l, m_canvas_l);
	m_c_square = false;
    }
    c->cd();
    if (m_do_ratio) {
    	pad1 =  new TPad("pad1","pad1name",0.,0.20,1.,1.);   
    	pad2 =  new TPad("pad2","pad1name",0.,0.05,1.,0.30);    
    	pad1->Draw();
    	pad2->Draw();
    	pad2->SetGridy();
	pad2->SetBottomMargin(0.42);
	//pad2->SetTopMargin(0.08);
    } else {
    	pad1 =  new TPad("pad1","pad1name",0., 0., 1., 1.);   
    	pad1->Draw();
	//pad1->SetBottomMargin(0.2);
	if (m_rightmargin > 0) {
	    pad1->SetRightMargin(m_rightmargin);
	    m_rightmargin = -1;
	}
    }
    pad1->cd();
    if (m_logy) pad1->SetLogy(true);

    if (m_overflow) {
	if (debug) m_lg->Info("s", "Add overflow");
	h_base = AddOverflow(h_base);
	for (int i = 0; i < h_alters.size(); i++) {
	    h_alters.at(i) = AddOverflow(h_alters.at(i));
	}
    }

    if (m_normbinwidth) {
	if (debug) m_lg->Info("s", "Norm to bin width");
	h_base = NormToBinWidth(h_base);
	for (int i = 0; i < h_alters.size(); i++) {
	    h_alters.at(i) = NormToBinWidth(h_alters.at(i));
	}
    }

    float ymax = -999;
    ymax = 1.2*h_base->GetMaximum();
    float ymin = 0;

    if (m_autorange) {
	if (debug) m_lg->Info("s", "Adjust y axis range");
	if (ymax > 0) {
	//if (ymin >= 0 && ymax > 0) {
	    if (!m_logy) h_base->GetYaxis()->SetRangeUser(0, 1.7*ymax);
	    else h_base->GetYaxis()->SetRangeUser(m_logy_min, m_logy_scale*ymax);
	//} else if (ymin < 0 && ymax > 0) {
	//    if (!m_logy) h_base->GetYaxis()->SetRangeUser(1.2*ymin, 2.5*ymax);
	//    else {
	//	cout << "Minimum of y is negative, cannot use log scale!" << endl;
	//	exit(-1);
	//    }
	} else {
	    if (!m_logy) h_base->GetYaxis()->SetRangeUser(1.2*ymin, ymax + 4*fabs(ymax));
	    else {
		cout << "y is negative, cannot use log scale!" << endl;
		exit(-1);
	    }
	}
    } else {
	h_base->GetYaxis()->SetRangeUser(m_range_y_low, m_range_y_high);
	m_autorange = true;
    }

    if (m_yrange_ratio != 0) {
	h_base->GetYaxis()->SetRangeUser(0, m_yrange_ratio*ymax);
	m_yrange_ratio = 0;
    }

    if (debug) m_lg->Info("s", "Tweak titles");

    if (m_ytitle == "") {
	string ytitle = ParseYtitle(h_base->GetName());
	string ytitle_font = "";
	ytitle_font += ytitle;
	h_base->GetYaxis()->SetTitle(ytitle_font.c_str());
	m_ytitle = "";
    }


    //h_base->GetXaxis()->SetLabelSize(0.06);
    if (m_xtitle_size != -999) {h_base->GetXaxis()->SetTitleSize(m_xtitle_size); m_xtitle_size = -999;}
    if (m_norm) h_base->GetYaxis()->SetTitle("A.U.");
    else {
	if (m_normbinwidth) {
	    if (string(h_base->GetName()).find("LeadPhPt", 0) != string::npos) 
	       h_base->GetYaxis()->SetTitle("Events / GeV");
            else
	       h_base->GetYaxis()->SetTitle("Events / Bin width");
	} else {
	    h_base->GetYaxis()->SetTitle("Events");
	}
    }
    if (m_ytitle != "") {
	h_base->GetYaxis()->SetTitle(m_ytitle.c_str());
	m_ytitle = "";
    }
    if (m_ytitle_offset > 0) {
	h_base->GetYaxis()->SetTitleOffset(m_ytitle_offset);
	m_ytitle_offset = -1;
    }

    if (m_xtitle == "") {
	string xtitle = ParseXtitle(h_base->GetName());
	string xtitle_font = "";
	xtitle_font += xtitle;
	h_base->GetXaxis()->SetTitle(xtitle_font.c_str());
	m_xtitle = "";
    } else {
	string xtitle_font = "";
	xtitle_font += m_xtitle;
	h_base->GetXaxis()->SetTitle(xtitle_font.c_str());
	m_xtitle = "";
    }

    if (debug) m_lg->Info("s", "Set color and marker");

    if (m_order_bin) {
	vector<double> soverbs;
    	for (int i = 1; i <= h_base->GetNbinsX(); i++) {
    	    double mc_sig = h_alters.at(0)->GetBinContent(i);
    	    double mc_bkg = 0;
    	    for (int j = 1; j < h_alters.size(); j++) {
    	        double mc = h_alters.at(j)->GetBinContent(i);
    	        mc_bkg += mc;
    	    }
    	    double soverb = 0;
    	    if (mc_bkg != 0) {
    	        soverb = mc_sig / mc_bkg;
    	    }
    	}
    	vector<int> binorder;
    	OrderFVector(soverbs, binorder);

    	OrderBins(h_base, binorder);
    	for (int i = 0; i < h_alters.size(); i++) {
    	    OrderBins(h_alters.at(i), binorder);
    	}
    }

    double all_data = 0;
    double all_data_err = 0;
    for (int i = 1; i <= h_base->GetNbinsX()+1; i++) {
	all_data += h_base->GetBinContent(i);
	all_data_err = sqrt(pow(all_data_err,2) + pow(h_base->GetBinError(i),2));
    }
    double all_mc = 0;
    double all_mc_err = 0;
    for (int i = 0; i < h_alters.size(); i++) {
	for (int j = 1; j < h_alters.at(i)->GetNbinsX()+1; j++) {
	    all_mc += h_alters.at(i)->GetBinContent(j);
	    all_mc_err = sqrt(pow(all_mc_err,2) + pow(h_alters.at(i)->GetBinError(j),2));
	}
    }

    double sf = all_data/all_mc;
    double sf_err = sf * sqrt(pow(all_data_err/all_data,2) + pow(all_mc_err/all_mc,2));
    cout << "Data: " << all_data << " +/- " << all_data_err << " MC: " << all_mc << " +/- " << all_mc_err << " SF: " << sf << " +/- " << sf_err << endl;

    if (m_normMCtodata) {
	for (int i = 0; i < h_alters.size(); i++) {
	    h_alters.at(i)->Scale(sf);
	}
    }

    int icolor = (m_basecolor == -1) ? 1 : m_basecolor;
    h_base->SetLineColor(icolor);
    h_base->SetMarkerColor(icolor);
    h_base->SetMarkerSize(1);
    h_base->SetMarkerStyle(20);
    int tmpcolor = icolor;
    for (int i = 0; i < h_alters.size(); i++) {
        tmpcolor++;
        if (tmpcolor%5 == 0) tmpcolor++;
	if (m_altercolors.size() != 0) tmpcolor = m_altercolors.at(i); 
        h_alters.at(i)->SetLineColor(tmpcolor);
        h_alters.at(i)->SetFillColor(tmpcolor);
        h_alters.at(i)->SetMarkerColor(tmpcolor);
        //h_alters.at(i)->SetMarkerSize(1);
    }

    if (debug) m_lg->Info("s", "DRAW");

    if (m_normbinwidth) {
        if (string(h_base->GetName()).find("LeadPhPt", 0) != string::npos) 
	   h_base->GetYaxis()->SetTitle("Events / GeV");
        else
	   h_base->GetYaxis()->SetTitle("Events / Bin width");
    }

    if (m_do_ratio) h_base->GetXaxis()->SetLabelSize(0.);
    if (m_stack_mc_only) {
	h_base->Draw("hist");
    } else {
	h_base->Draw("PE hist");
    }
    THStack* hs = new THStack();
    TH1F* h_sum = NULL;
    if (m_stack_reverse) {
	for (int i = h_alters.size() - 1; i >= 0; i--) {
    	    if (i == h_alters.size() - 1) h_sum = (TH1F*)h_alters.at(i)->Clone();
    	    else h_sum->Add(h_alters.at(i));
	    h_alters.at(i)->SetLineWidth(0);
    	    hs->Add(h_alters.at(i));
    	}
    } else {
	for (int i = 0; i < h_alters.size(); i++) {
    	    if (i == 0) h_sum = (TH1F*)h_alters.at(i)->Clone();
    	    else h_sum->Add(h_alters.at(i));
	    h_alters.at(i)->SetLineWidth(0);
    	    hs->Add(h_alters.at(i));
    	}
    }
    hs->Draw("same hist");
    if (!m_stack_mc_only) h_base->Draw("same PE hist");
    gStyle->SetHatchesLineWidth(2.5);
    h_sum->SetFillStyle(3354);
    h_sum->SetMarkerSize(0.000001);
    h_sum->SetFillColor(kGray+1);
    h_sum->SetLineWidth(2);
    h_sum->Draw("E2 same");
    if (!m_stack_mc_only) h_base->Draw("same PE hist");

    if (debug) m_lg->Info("s", "Adjust legend position");

    if (!m_lgpos_manual) LegendPosAutoAdjust(debug);

    if (debug) m_lg->Info("s", "Add legend");
    TLegend *lg = new TLegend(m_lg_pos[0] + m_lg_xshift, m_lg_pos[1]+m_lg_yshift, m_lg_pos[2] + m_lg_xshift2, m_lg_pos[3]+m_lg_yshift);
    m_lg_xshift = 0;
    m_lg_xshift2 = 0;
    m_lg_yshift = 0;
    lg->SetNColumns(m_lg_column);
    m_lg_column = 1;
    lg->SetTextSize(m_do_ratio ? m_lg_textsize : m_lg_textsize);
    lg->SetTextSize(0.04);
    lg->SetBorderSize(0);
    lg->SetFillColor(0);
    if (!m_stack_mc_only) lg->AddEntry(h_base, m_lg_basename.c_str(), "P");
    if (m_lg_alternames.size() != h_alters.size()) {
        cout << "Miss legend names!" << endl;
        return;
    }
    for (int i = 0; i < h_alters.size(); i++) {
        lg->AddEntry(h_alters.at(i), m_lg_alternames.at(i).c_str(), "f");
    }
    if (!m_no_lg) lg->Draw();

    if (debug) m_lg->Info("s", "Add latex information");

    if (m_channel == "Undefined") {
	if (string(h_base->GetName()).find("_EJ_",0) != string::npos) m_channel = "EJ Channel";
	if (string(h_base->GetName()).find("_MJ_",0) != string::npos) m_channel = "MJ Channel";
    }
    if (m_type == "") {
	if (string(h_base->GetName()).find("_Data_",0) != string::npos) m_type = "Data";
	if (string(h_base->GetName()).find("_Reco_",0) != string::npos) m_type = "Simulation";
	if (string(h_base->GetName()).find("_Truth_",0) != string::npos) m_type = "Truth";
    }
    TLatex lt;
    lt.SetNDC();
    //lt.SetTextFont(72);
    //lt.SetTextColor(1);

    float xstep = 0;
    float ystep = 0;
    xstep = 0.21 * (m_channel.size() / 10.);
    ystep = -0.08;

    float lt_y = m_lt_y;

    if (m_atlas_logo) {
	if (m_is_simulation) {
	    lt.SetTextFont(72);
	    lt.DrawLatex(m_lt_x+m_logo_xshift, lt_y, "ATLAS");
	    lt.SetTextFont(42);
	    double delx = 0.115*696*pad1->GetWh()/(472*pad1->GetWw());
	    string atlasflag_str;
	    if (atlas_flag == 1) atlasflag_str = "Simulation Preliminary";
	    if (atlas_flag == 2) atlasflag_str = "Internal Simulation";
	    lt.DrawLatex(m_lt_x+m_logo_xshift+delx, lt_y, atlasflag_str.c_str());
	    lt_y += ystep;
	    string cms;
	    if (m_is_13tev) cms = "#sqrt{s}=13TeV";
	    else if (m_is_14tev) {
		if (m_lhc_info == "") {
		    if (m_showlumi) {
			cms = "#sqrt{s} = 14 TeV, 3 ab^{-1}";
		    } else {
			cms = "#sqrt{s} = 14 TeV";
		    }
		} else {
		    cms = m_lhc_info;
		}
	    }
	    else cms = "#sqrt{s}=8TeV";
	    lt.DrawLatex(m_lt_x+m_logo_xshift, lt_y, cms.c_str());
	    lt_y += ystep;
	} 
	if (m_is_data) {
	    lt.SetTextFont(72);
	    lt.DrawLatex(m_lt_x+m_logo_xshift, lt_y, "ATLAS");
	    lt.SetTextFont(42);
	    double delx = 0;
	    if (m_do_ratio) {
		delx = 0.115*580*pad1->GetWh()/(472*pad1->GetWw());
	    } else {
		delx = 0.115*700*pad1->GetWh()/(472*pad1->GetWw());
	    }
	    lt.DrawLatex(m_lt_x+m_logo_xshift+delx, lt_y, "Internal");
	    lt_y += ystep;
	    lt.DrawLatex(m_lt_x+m_logo_xshift, lt_y, m_lhc_info.c_str());
	    lt_y += ystep;
	}
    }

    if (m_logo != "") {
	lt.SetTextFont(72);
	lt.DrawLatex(m_lt_x+m_logo_xshift, lt_y, m_logo.c_str());
	lt.SetTextFont(42);
	lt_y += ystep;
	m_logo = "";
    }

    if (m_channel != "") lt.DrawLatex(m_lt_x+m_logo_xshift, lt_y, m_channel.c_str());

    m_logo_xshift = 0;

    //if (m_lt_x + xstep + m_type.size()*0.21/10. > (m_lg_right ? m_lg_pos[0] : 0.9)) {
    //    lt.DrawLatex(m_lt_x, m_lt_y + ystep - 0.16, m_type.c_str());// shift -0.05 from the original
    //} else lt.DrawLatex(m_lt_x + xstep, m_lt_y - 0.16, m_type.c_str());// shift -0.05 from the original
    pad1->RedrawAxis();

    if (m_do_ratio && !m_stack_mc_only) {
	pad2->cd();

	int bin_number = h_base->GetNbinsX();
	float bin_low = h_base->GetXaxis()->GetXmin();
	float bin_high = h_base->GetXaxis()->GetXmax();
	Double_t x1 = h_base->GetBinLowEdge(1);
	Double_t y1 = 1.;
	Double_t x2 = h_base->GetBinLowEdge(bin_number) + h_base->GetBinWidth(bin_number);
	Double_t y2 = 1.;
	TLine* central_line = new TLine(x1, y1, x2, y2);
	central_line->SetLineColor(kBlack);

	Bool_t ratio_first = true;
	TH1F *h_sum_ratio = (TH1F*)h_base->Clone();
	for (int i = 0; i < h_sum_ratio->GetNbinsX(); i++) {
	    if (h_sum->GetBinContent(i+1) != 0) {
		h_sum_ratio->SetBinContent(i+1,h_sum_ratio->GetBinContent(i+1)/h_sum->GetBinContent(i+1));
		h_sum_ratio->SetBinError(i+1,h_sum_ratio->GetBinError(i+1)/h_sum->GetBinContent(i+1));
	    } else {
		h_sum_ratio->SetBinContent(i+1,0);
		h_sum_ratio->SetBinError(i+1,0);
	    }
	}
	TH1F *h_sum_ratio2 = (TH1F*)h_sum->Clone();
	for (int i = 0; i < h_sum_ratio2->GetNbinsX(); i++) {
	    if (h_sum->GetBinContent(i+1) != 0) {
		h_sum_ratio2->SetBinContent(i+1,h_sum_ratio2->GetBinContent(i+1)/h_sum->GetBinContent(i+1));
		h_sum_ratio2->SetBinError(i+1,h_sum_ratio2->GetBinError(i+1)/h_sum->GetBinContent(i+1));
	    } else {
		h_sum_ratio2->SetBinContent(i+1,0);
		h_sum_ratio2->SetBinError(i+1,0);
	    }
	}
	//h_sum_ratio->Divide(h_sum);
	Double_t enlarge = 3;
	h_sum_ratio->GetXaxis()->SetLabelSize(0.05*enlarge);
	h_sum_ratio->GetXaxis()->SetTitleSize(0.05*enlarge);
	h_sum_ratio->GetXaxis()->SetTitleOffset(1.2);
	h_sum_ratio->GetYaxis()->SetLabelSize(0.05*enlarge);
	h_sum_ratio->GetYaxis()->SetTitle(m_ratio_ytitle.c_str());
	h_sum_ratio->GetYaxis()->SetTitleSize(0.05*enlarge);
	h_sum_ratio->GetYaxis()->SetTitleOffset(1.2/enlarge);
	h_sum_ratio->GetYaxis()->SetNdivisions(505);
	Double_t max = h_sum_ratio->GetMaximum();
	Double_t min = h_sum_ratio->GetMinimum();
	Double_t deviation_up = max > 1 ? (max - 1) : 1;
	Double_t deviation_down = min < 1 ? (1 - min) : 1;
	Double_t max_deviation = deviation_up > deviation_down ? deviation_up : deviation_down;
	h_sum_ratio->SetMaximum(1 + max_deviation * 1.2 <= m_max_ratio ? 1 + max_deviation * 1.2 : m_max_ratio);
	h_sum_ratio->SetMinimum(1 - max_deviation * 1.2 >= m_min_ratio ? 1 - max_deviation * 1.2 : m_min_ratio);
	if (m_fix_ratio_range) {
	    h_sum_ratio->SetMaximum(m_max_ratio);
	    h_sum_ratio->SetMinimum(m_min_ratio);
	}
	h_sum_ratio2->SetFillStyle(3354);
	h_sum_ratio2->SetFillColor(kGray+1);
	h_sum_ratio2->SetLineWidth(2);
	if (m_x_label_v) h_sum_ratio->GetXaxis()->LabelsOption("v");
	h_sum_ratio->Draw();
	h_sum_ratio2->Draw("E2 same");
	h_sum_ratio->Draw("same");
	
	central_line->Draw("same");

	if (m_save_ratio) {
	    string f_savename = m_savedir + m_savename + string(".root");
	    m_lg->Info("ss", "Save ratio plot to file ->", f_savename.c_str());
	    TFile *fout = new TFile(f_savename.c_str(), "recreate");
	    h_sum_ratio->Write("Ratio");
	    fout->Close();
	    delete fout;
	}
    }

    if (debug) m_lg->Info("s", "Save plot");
    if (m_savename == "") m_savename = h_base->GetName();
    c->SaveAs((m_savedir + m_savename + string(".png")).c_str());
    c->SaveAs((m_savedir + m_savename + string(".pdf")).c_str());
    //c->Print((m_savedir + m_savename + string(".C")).c_str());

    delete c;
    delete lg;
}

void PlotComparor::Compare(bool debug) {
    TCanvas *c = NULL;
    if (!m_c_square) {
	c = new TCanvas("", "", m_canvas_l, m_canvas_h);
    } else {
	c = new TCanvas("", "", m_canvas_l, m_canvas_l);
	m_c_square = false;
    }
    c->cd();
    if (m_do_ratio) {
    	pad1 =  new TPad("pad1","pad1name",0.,0.20,1.,1.);   
    	pad2 =  new TPad("pad2","pad1name",0.,0.05,1.,0.30);    
    	pad1->Draw();
    	pad2->Draw();
    	pad2->SetGridy();
	pad2->SetBottomMargin(0.42);
	//pad2->SetTopMargin(0.08);
    } else {
    	pad1 =  new TPad("pad1","pad1name",0., 0., 1., 1.);   
    	pad1->Draw();
	//pad1->SetBottomMargin(0.2);
	if (m_rightmargin > 0) {
	    pad1->SetRightMargin(m_rightmargin);
	    m_rightmargin = -1;
	}
    }
    pad1->cd();
    if (m_logy) pad1->SetLogy(true);

    if (debug) m_lg->Info("s", "Start to compare");

    if (debug) {
	m_lg->Info("s", "show base integral");
	cout << h_base << endl;
	cout << h_base->Integral() << endl;
    }
    if (h_base->Integral() == 0) {
	m_lg->Err("s", "Empty base histogram!");
	exit(-1);
    }

    if (debug) m_lg->Info("s", "overflow");

    if (m_overflow) {
	if (debug) m_lg->Info("s", "Add overflow");
	h_base = AddOverflow(h_base);
	if (h_alter) h_alter = AddOverflow(h_alter);
	else if (h_alters.size() != 0) {
	    for (int i = 0; i < h_alters.size(); i++) {
		h_alters.at(i) = AddOverflow(h_alters.at(i));
	    }
	}
    }

    if (m_normbinwidth) {
	if (debug) m_lg->Info("s", "Add overflow");
	h_base = NormToBinWidth(h_base);
	if (h_alter) h_alter = NormToBinWidth(h_alter);
	else if (h_alters.size() != 0) {
	    for (int i = 0; i < h_alters.size(); i++) {
		h_alters.at(i) = NormToBinWidth(h_alters.at(i));
	    }
	}
    }

    if (debug) m_lg->Info("s", "cutbin");
    if (m_cutbins) {
	if (debug) m_lg->Info("s", "Add overflow");
	h_base = Cutbins(h_base);
	if (h_alter) h_alter = Cutbins(h_alter);
	else if (h_alters.size() != 0) {
	    for (int i = 0; i < h_alters.size(); i++) {
		h_alters.at(i) = Cutbins(h_alters.at(i));
	    }
	}
	m_cutbins = false;
    }

    if (debug) m_lg->Info("s", "norm to 1");
    if (m_norm) {
	if (debug) m_lg->Info("s", "Normalize to 1");
	h_base->Scale(1./h_base->Integral());
	if (h_alter) {
	    if (h_alter->Integral() != 0) {
		h_alter->Scale(1./h_alter->Integral());
	    }
	}
	else if (h_alters.size() != 0) {
	    for (int i = 0; i < h_alters.size(); i++) {
		if (h_alters.at(i)->Integral() != 0) {
		    h_alters.at(i)->Scale(1./h_alters.at(i)->Integral());
		}
	    }
	}
    }

    if (debug) m_lg->Info("s", "set line width");
    if (m_line_width != 0) {
	h_base->SetLineWidth(m_line_width);
	if (h_alter) {
	    h_alter->SetLineWidth(m_line_width);
	}
	else if (h_alters.size() != 0) {
	    for (int i = 0; i < h_alters.size(); i++) {
		h_alters.at(i)->SetLineWidth(m_line_width);
	    }
	}
    }

    if (debug) m_lg->Info("s", "set range user");
    float ymax = -999;
    ymax = h_base->GetMaximum() > ymax ? h_base->GetMaximum() : ymax;
    if (h_alter) ymax = h_alter->GetMaximum() > ymax ? h_alter->GetMaximum() : ymax;
    else if (h_alters.size() != 0) {
	for (int i = 0; i < h_alters.size(); i++) {
	    ymax = h_alters.at(i)->GetMaximum() > ymax ? h_alters.at(i)->GetMaximum() : ymax;
	}
    }
    float ymin = 999;
    ymin = h_base->GetMinimum() < ymin ? h_base->GetMinimum() : ymin;
    if (h_alter) ymin = h_alter->GetMinimum() < ymin ? h_alter->GetMinimum() : ymin;
    else if (h_alters.size() != 0) {
	for (int i = 0; i < h_alters.size(); i++) {
	    ymin = h_alters.at(i)->GetMinimum() < ymin ? h_alters.at(i)->GetMinimum() : ymin;
	}
    }
    if (m_autorange) {
	if (debug) m_lg->Info("s", "Adjust y axis range");
	if (ymax > 0) {
	//if (ymin >= 0 && ymax > 0) {
	    if (!m_logy) h_base->GetYaxis()->SetRangeUser(0, (m_dby ? 3 : 1.8)*ymax);
	    else h_base->GetYaxis()->SetRangeUser(m_logy_min, m_logy_scale*ymax);
	//} else if (ymin < 0 && ymax > 0) {
	//    if (!m_logy) h_base->GetYaxis()->SetRangeUser(1.2*ymin, 2.0*ymax);
	//    else {
	//	cout << "Minimum of y is negative, cannot use log scale!" << endl;
	//	exit(-1);
	//    }
	} else {
	    if (!m_logy) h_base->GetYaxis()->SetRangeUser(1.2*ymin, ymax + 4*fabs(ymax));
	    else {
		cout << "y is negative, cannot use log scale!" << endl;
		exit(-1);
	    }
	}
    } else {
	h_base->GetYaxis()->SetRangeUser(m_range_y_low, m_range_y_high);
	m_autorange = true;
    }

    if (m_yrange_ratio != 0) {
	h_base->GetYaxis()->SetRangeUser(0, m_yrange_ratio*ymax);
	m_yrange_ratio = 0;
    }

    if (debug) m_lg->Info("s", "Tweak titles");

    //h_base->GetXaxis()->SetLabelSize(0.06);
    if (m_xtitle_size != -999) {h_base->GetXaxis()->SetTitleSize(m_xtitle_size); m_xtitle_size = -999;}
    if (m_norm) h_base->GetYaxis()->SetTitle("A.U.");
    else {
	if (m_normbinwidth) {
	    if (string(h_base->GetName()).find("LeadPhPt", 0) != string::npos) 
	       h_base->GetYaxis()->SetTitle("Events / GeV");
            else
	       h_base->GetYaxis()->SetTitle("Events / Bin width");
	} else {
	    h_base->GetYaxis()->SetTitle("Events");
	}
    }
    if (m_ytitle != "") {
	h_base->GetYaxis()->SetTitle(m_ytitle.c_str());
	m_ytitle = "";
    }
    if (m_ytitle_offset > 0) {
	h_base->GetYaxis()->SetTitleOffset(m_ytitle_offset);
	m_ytitle_offset = -1;
    }

    if (m_xtitle == "") {
	string xtitle = ParseXtitle(h_base->GetName());
	string xtitle_font = "";
	xtitle_font += xtitle;
	h_base->GetXaxis()->SetTitle(xtitle_font.c_str());
    } else {
	string xtitle_font = "";
	xtitle_font += m_xtitle;
	h_base->GetXaxis()->SetTitle(xtitle_font.c_str());
    }

    if (debug) m_lg->Info("s", "Set color and marker");

    int icolor = 1;
    h_base->SetLineColor(icolor);
    h_base->SetMarkerColor(icolor);
    //h_base->SetMarkerSize(0.0001);
    if (h_alter) {
	h_alter->SetLineColor(icolor+1);
	h_alter->SetMarkerColor(icolor+1);
	//h_alter->SetMarkerSize(0.0001);
    } else if (h_alters.size() != 0) {
	int tmpcolor = icolor;
	for (int i = 0; i < h_alters.size(); i++) {
	    tmpcolor++;
	    if (tmpcolor == 5) tmpcolor++;
	    if (m_altercolors.size() != 0) tmpcolor = m_altercolors.at(i); 
	    h_alters.at(i)->SetLineColor(tmpcolor);
	    h_alters.at(i)->SetMarkerColor(tmpcolor);
	    //h_alters.at(i)->SetMarkerSize(0.0001);
	}
    }

    if (debug) m_lg->Info("s", "Set line style");

    //h_base->SetLineWidth(3);
    //if (h_alter) h_alter->SetLineWidth(3);
    //else if (h_alters.size() != 0) {
    //    for (int i = 0; i < h_alters.size(); i++) {
    //        h_alters.at(i)->SetLineWidth(3);
    //    }
    //}

    if (debug) m_lg->Info("s", "DRAW");

    string option = m_draw_option;
    if (m_do_ratio) h_base->GetXaxis()->SetLabelSize(0.);
    h_base->Draw(option.c_str());
    option = m_draw_option;
    option += " same";
    if (h_alter) h_alter->Draw(option.c_str());
    else if (h_alters.size() != 0) {
	for (int i = 0; i < h_alters.size(); i++) {
	    h_alters.at(i)->Draw(option.c_str());
	    cout << h_alters.at(i)->GetName() << ": " << Separation(h_alters.at(i), h_base, i) << endl;
	}
    }
    if (m_draw_option.find("hist",0) != string::npos) {
	h_base->Draw("hist same");
	if (h_alter) h_alter->Draw("hist same");
    	else if (h_alters.size() != 0) {
    	    for (int i = 0; i < h_alters.size(); i++) {
    	        h_alters.at(i)->Draw("hist same");
		cout << h_alters.at(i)->GetName() << ": " << Separation(h_alters.at(i), h_base, i) << endl;
    	    }
    	}
    }

    if (m_draw_dhline) {
	if (debug) m_lg->Info("s", "Draw line notification");
	m_draw_dhline = false;
	UInt_t nbinsx    = h_base->GetNbinsX();
	double binlo = h_base->GetBinLowEdge(1);
    	double binhi = h_base->GetBinLowEdge(nbinsx)+h_base->GetBinWidth(nbinsx);
	//TLine l(binlo, m_ypos_dhline, binhi, m_ypos_dhline);
	TLine l(c->GetUxmin(),m_ypos_dhline,c->GetUxmax(),m_ypos_dhline);
	l.SetLineWidth(2);
	l.SetLineStyle(2);
	l.SetLineColor(kBlack);
	l.Draw("same");
    }

    if (debug) m_lg->Info("s", "Adjust legend position");

    if (!m_lgpos_manual) LegendPosAutoAdjust(debug);

    if (debug) m_lg->Info("s", "Add legend");
    TLegend *lg = new TLegend(m_lg_pos[0] + m_lg_xshift, m_lg_pos[1]+m_lg_yshift, m_lg_pos[2] + m_lg_xshift2, m_lg_pos[3]+m_lg_yshift);
    m_lg_xshift = 0;
    m_lg_xshift2 = 0;
    m_lg_yshift = 0;
    lg->SetNColumns(m_lg_column);
    m_lg_column = 1;
    //lg->SetTextSize(m_do_ratio ? m_lg_textsize*1.2 : m_lg_textsize);
    lg->SetTextSize(0.05);
    lg->SetTextFont(42);
    lg->SetBorderSize(0);
    lg->SetFillColor(0);
    string lg_option = "l";
    if (m_draw_option.find("e",0) != string::npos) lg_option += "e";
    if (m_draw_option.find("p",0) != string::npos) lg_option += "p";
    //lg_option = "P";
    lg->AddEntry(h_base, m_lg_basename.c_str(), lg_option.c_str());
    if (h_alter) lg->AddEntry(h_alter, m_lg_altername.c_str(), lg_option.c_str());
    else if (h_alters.size() != 0) {
	if (m_lg_alternames.size() != h_alters.size()) {
	    cout << "Miss legend names!" << endl;
	    return;
	}
	for (int i = 0; i < h_alters.size(); i++) {
	    lg->AddEntry(h_alters.at(i), m_lg_alternames.at(i).c_str(), lg_option.c_str());
	}
    }
    if (!m_no_lg) lg->Draw();

    if (debug) m_lg->Info("s", "Add latex information");

    if (m_channel == "Undefined") {
	if (string(h_base->GetName()).find("_EJ_",0) != string::npos) m_channel = "EJ Channel";
	if (string(h_base->GetName()).find("_MJ_",0) != string::npos) m_channel = "MJ Channel";
    }
    if (m_type == "") {
	if (string(h_base->GetName()).find("_Data_",0) != string::npos) m_type = "Data";
	if (string(h_base->GetName()).find("_Reco_",0) != string::npos) m_type = "Simulation";
	if (string(h_base->GetName()).find("_Truth_",0) != string::npos) m_type = "Truth";
    }
    TLatex lt;
    lt.SetNDC();
    //lt.SetTextFont(72);
    //lt.SetTextColor(1);

    float xstep = 0;
    float ystep = 0;
    xstep = 0.21 * (m_channel.size() / 10.);
    ystep = -0.08;

    float lt_y = m_lt_y;

    if (m_atlas_logo) {
	if (m_is_simulation) {
	    lt.SetTextFont(72);
	    lt.DrawLatex(m_lt_x+m_logo_xshift, lt_y, "ATLAS");
	    lt.SetTextFont(42);
	    double delx = 0.115*696*pad1->GetWh()/(472*pad1->GetWw());
	    string atlasflag_str;
	    if (atlas_flag == 1) atlasflag_str = "Simulation Preliminary";
	    if (atlas_flag == 2) atlasflag_str = "Internal Simulation";
	    lt.DrawLatex(m_lt_x+m_logo_xshift+delx, lt_y, atlasflag_str.c_str());
	    lt_y += ystep;
	    string cms;
	    if (m_is_13tev) cms = "#sqrt{s}=13TeV";
	    else if (m_is_14tev) {
		if (m_lhc_info == "") {
		    if (m_showlumi) {
			cms = "#sqrt{s} = 14 TeV, 3 ab^{-1}";
		    } else {
			cms = "#sqrt{s} = 14 TeV";
		    }
		} else {
		    cms = m_lhc_info;
		}
	    }
	    else cms = "#sqrt{s}=8TeV";
	    lt.DrawLatex(m_lt_x+m_logo_xshift, lt_y, cms.c_str());
	    lt_y += ystep;
	} 
	if (m_is_data) {
	    lt.SetTextFont(72);
	    lt.DrawLatex(m_lt_x+m_logo_xshift, lt_y, "ATLAS");
	    lt.SetTextFont(42);
	    double delx = 0.115*696*pad1->GetWh()/(472*pad1->GetWw());
	    lt.DrawLatex(m_lt_x+m_logo_xshift+delx, lt_y, "Internal");
	    lt_y += ystep;
	    lt.DrawLatex(m_lt_x+m_logo_xshift, lt_y, m_lhc_info.c_str());
	    lt_y += ystep;
	}
    }

    if (m_logo != "") {
	lt.SetTextFont(72);
	lt.DrawLatex(m_lt_x+m_logo_xshift, lt_y, m_logo.c_str());
	lt.SetTextFont(42);
	lt_y += ystep;
	m_logo = "";
    }

    if (m_channel != "") lt.DrawLatex(m_lt_x+m_logo_xshift, lt_y, m_channel.c_str());

    m_logo_xshift = 0;

    //if (m_lt_x + xstep + m_type.size()*0.21/10. > (m_lg_right ? m_lg_pos[0] : 0.9)) {
    //    lt.DrawLatex(m_lt_x, m_lt_y + ystep - 0.16, m_type.c_str());// shift -0.05 from the original
    //} else lt.DrawLatex(m_lt_x + xstep, m_lt_y - 0.16, m_type.c_str());// shift -0.05 from the original

    if (m_do_ratio) {
	pad2->cd();

	int bin_number = h_base->GetNbinsX();
	float bin_low = h_base->GetXaxis()->GetXmin();
	float bin_high = h_base->GetXaxis()->GetXmax();
	Double_t x1 = h_base->GetBinLowEdge(1);
	Double_t y1 = 1.;
	Double_t x2 = h_base->GetBinLowEdge(bin_number) + h_base->GetBinWidth(bin_number);
	Double_t y2 = 1.;
	TLine* central_line = new TLine(x1, y1, x2, y2);
	central_line->SetLineColor(kBlack);

	Bool_t ratio_first = true;
	for (int i = 0; i < h_alters.size(); i++) {
	    TH1F *h_tmp = (TH1F*)h_alters.at(i)->Clone();
	    h_tmp->Sumw2();
	    h_tmp->Divide(h_base);
	    if (ratio_first) {
	    	ratio_first = false;
	    	Double_t enlarge = 3;
	    	h_tmp->GetXaxis()->SetTitle(m_xtitle.c_str());
	    	h_tmp->GetXaxis()->SetLabelSize(0.05*enlarge);
	    	h_tmp->GetXaxis()->SetTitleSize(0.05*enlarge);
	    	h_tmp->GetXaxis()->SetTitleOffset(1.2);
	    	h_tmp->GetYaxis()->SetLabelSize(0.05*enlarge);
	    	h_tmp->GetYaxis()->SetTitle(m_ratio_ytitle.c_str());
	    	h_tmp->GetYaxis()->SetTitleSize(0.05*enlarge);
	    	h_tmp->GetYaxis()->SetTitleOffset(1.2/enlarge);
	    	h_tmp->GetYaxis()->SetNdivisions(505);
	    	Double_t max = h_tmp->GetMaximum();
	    	Double_t min = h_tmp->GetMinimum();
	    	//Double_t deviation_up = max > 1 ? (max - 1) : 1;
	    	//Double_t deviation_down = min < 1 ? (1 - min) : 1;
	    	//Double_t max_deviation = deviation_up > deviation_down ? deviation_up : deviation_down;
	    	h_tmp->SetMaximum((max) <= m_max_ratio ? (max) : m_max_ratio);
	    	h_tmp->SetMinimum((min) >= m_min_ratio ? (min) : m_min_ratio);
		if (m_fix_ratio_range) {
		    h_tmp->SetMaximum(m_max_ratio);
		    h_tmp->SetMinimum(m_min_ratio);
		}
	    }
	    string draw_option_ratio;
	    //draw_option_ratio = m_draw_option;
	    draw_option_ratio = " same e";
	    //h_tmp->SetMarkerSize(0.0001);
	    h_tmp->Draw(draw_option_ratio.c_str());

	}
	central_line->Draw("same");
    }

    if (debug) m_lg->Info("s", "Save plot");
    if (m_savename == "") m_savename = h_base->GetName();
    c->SaveAs((m_savedir + m_savename + string(".png")).c_str());
    c->SaveAs((m_savedir + m_savename + string(".pdf")).c_str());
    //c->Print((m_savedir + m_savename + string(".C")).c_str());

    delete c;
    delete lg;
}
