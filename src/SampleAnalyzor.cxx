#include "SampleAnalyzor.h"
#include <TMath.h>
#include <TH1.h>
#include <TStyle.h>
#include <TCanvas.h>
#include <iostream>
#include <map>
#include <fstream>
#include <algorithm>
#include <stdio.h>
#include <stdlib.h>
#include <list>
#include <TKey.h>
#include <TIterator.h>
#include <TDirectory.h>
#include <TRandom3.h>
#include "StringPlayer.h"

void AddOverflow(TH1F*h) {
    double of = h->GetBinContent(h->GetNbinsX()+1);
    double of_err = h->GetBinError(h->GetNbinsX()+1);
    h->SetBinContent(h->GetNbinsX(), h->GetBinContent(h->GetNbinsX()) + of);
    h->SetBinError(h->GetNbinsX(), sqrt(pow(h->GetBinError(h->GetNbinsX()),2) + pow(of_err,2)));
    h->SetBinContent(h->GetNbinsX()+1, 0);
    h->SetBinError(h->GetNbinsX()+1, 0);
}

std::pair<bool, bool> isPromptElectron(int type, int origin, int egMotherType, int egMotherOrigin, int egMotherPdgId, int RecoCharge){
    // 43 is "diboson" origin, but is needed due to buggy origin flags in Sherpa ttbar
    bool isprompt = (type == 2 || 
	(type == 4 && origin == 5 && fabs(egMotherPdgId) == 11) ||
	// bkg electrons from ElMagDecay with origin top, W or Z, higgs, diBoson
	(type == 4 && origin == 7 && egMotherType == 2 && (egMotherOrigin == 10 || egMotherOrigin == 12 || egMotherOrigin == 13 || egMotherOrigin == 14 || egMotherOrigin == 43) && fabs(egMotherPdgId) == 11) ||
	// unknown electrons from multi-boson (sherpa 222, di-boson)
	(type == 1 && egMotherType == 2 && egMotherOrigin == 47 && fabs(egMotherPdgId) == 11) );

    bool isChargeFl = false;
    if (egMotherPdgId*RecoCharge>0) isChargeFl = true;
    if (isprompt) return std::make_pair(true,isChargeFl); //charge flipped electrons are also considered prompt

    // bkg photons from photon conv from FSR (must check!!)
    if (type == 4 && origin == 5 && egMotherOrigin == 40) return std::make_pair(true,false);  
    // non-iso photons from FSR for the moment but we must check!! (must check!!)
    if (type == 15 && origin == 40) return std::make_pair(true,false);  
    // mainly in Sherpa Zee, but some also in Zmumu
    if (type == 4 && origin == 7 && egMotherType == 15 && egMotherOrigin == 40) return std::make_pair(true,false); 

    return std::make_pair(false,false);
}
 bool isPromptMuon(int type, int origin){
    // 43 is "diboson" origin, but is needed due to buggy origin flags in Sherpa ttbar
    bool prompt = (type == 6 &&
	   (origin == 10 || origin == 12 || origin == 13 || origin == 14 || origin == 15 || origin == 22 || origin == 43) ); 

    return prompt;
  }



using namespace std;

enum FinalState {ej = 1, mj, tj, ee, em, et, mm, mt, tt};

SampleAnalyzor::SampleAnalyzor() {
    m_lg = new Logger("SP Analyzor");
    m_lg->Info("s", "Hi, this is a sample analyzor~");

    // general control
    m_debug = false;
    m_test = false;
    m_1pct = false;
    m_10pct = false;
    m_firstevent = true;
    m_cali_egamma = false;
    m_cali_egamma_type = "1";
    m_nevt = -1;
    m_nfile = -1;
    m_random = NULL;
    m_phj_or = true;
    m_large_eta = false;

    // btag
    m_cont_btag = true;
    weight_btag = 1.0;

    // KLFitter
    m_do_klfitter = false;
    //m_kl_bveto = false;
    //m_kl_fixtop = false;
    //m_klfitter = NULL;
    //m_klfitter_lt = NULL;
    //m_klfitter_ht = NULL;
    //m_klfitter_lW = NULL;
    //m_klfitter_hW = NULL;
    //m_detector = NULL;
    //m_likelihood = NULL;
    //m_likelihood_lt = NULL;
    //m_likelihood_ht = NULL;
    //m_likelihood_lW = NULL;
    //m_likelihood_hW = NULL;
    //m_particles = NULL;
    //m_particles2 = NULL;
    //m_ph_klfitter = NULL;
    //m_el_klfitter = NULL;
    //m_mu_klfitter = NULL;

    // database
    m_xsectionlist = "";
    m_histogramlist = "/afs/cern.ch/work/y/yili/private/Analysis/Siegen13TeVWork/MainCode/config/Histograms.txt";
    m_2Dhistogramlist = "/afs/cern.ch/work/y/yili/private/Analysis/Siegen13TeVWork/MainCode/config/2DHistograms.txt";

    // meta info
    m_cme = "13TeV";
    m_simulation = "FULL";

    // region
    m_region = "";
    m_subregion = "";

    // sample
    m_type = "";
    m_process = "";

    // egamma fake
    m_efake_dr = 0.05;
    m_use_efake_eta_SF = false;
    m_efake_eta_weight = 1;
    m_reweight_egamma = false;

    m_reweight_hfake = false;
    m_reweight_hfake_025 = false;
    m_reweight_hfake_05 = false;
    m_reweight_hfake_075 = false;
    m_reweight_zgamma = false;

    // selection
    n_cuts_start = 1;
    n_cuts_end = 0;
    m_selection = "";
    m_ttsg = false;
    m_notau = false;
    m_ttdi = false;
    m_ttee = false;
    m_ttem = false;
    m_ttet = false;

    // truth photon match
    m_do_phmatch = false;
    m_phmatch_type = "";
    m_do_lpmatch = true;
    m_lpmatch_type = "";
    
    // jet match
    m_do_jetmatch = false;

    // weight
    m_use_weight = false;
    m_use_lumi_weight = false;
    m_use_kfac = true;
    m_pt_kfac = true;
    m_kfac_file = "";
    m_kfac_hist = "";
    m_do_bootstrap = false;

    // for QCD
    m_PSQCD = false;
    m_QCD_para = "";
    m_QCD_para_idx = -1;
    n_QCD_para = 63;
    m_QCD_1para = false;

    // histogram
    m_do_hists = true;
    m_show_of = true;
    m_do_egammahists = false;
    m_do_egammatf1 = false;
    m_do_hists_sys = false;

    m_egammaptbins_lo.push_back(20.);
    m_egammaptbins_lo.push_back(25.);
    m_egammaptbins_lo.push_back(30.);
    m_egammaptbins_lo.push_back(35.);
    m_egammaptbins_lo.push_back(40.);
    m_egammaptbins_lo.push_back(45.);
    m_egammaptbins_lo.push_back(50.);
    m_egammaptbins_lo.push_back(55.);
    m_egammaptbins_lo.push_back(60.);
    m_egammaptbins_lo.push_back(65.);
    m_egammaptbins_lo.push_back(70.);
    m_egammaptbins_hi.push_back(25.);
    m_egammaptbins_hi.push_back(30.);
    m_egammaptbins_hi.push_back(35.);
    m_egammaptbins_hi.push_back(40.);
    m_egammaptbins_hi.push_back(45.);
    m_egammaptbins_hi.push_back(50.);
    m_egammaptbins_hi.push_back(55.);
    m_egammaptbins_hi.push_back(60.);
    m_egammaptbins_hi.push_back(65.);
    m_egammaptbins_hi.push_back(70.);
    m_egammaptbins_hi.push_back(99999.);

    m_egammaetabins_lo.push_back(0.);
    m_egammaetabins_lo.push_back(0.2);
    m_egammaetabins_lo.push_back(0.4);
    m_egammaetabins_lo.push_back(0.6);
    m_egammaetabins_lo.push_back(0.8);
    m_egammaetabins_lo.push_back(1.0);
    m_egammaetabins_lo.push_back(1.2);
    m_egammaetabins_lo.push_back(1.37);
    m_egammaetabins_lo.push_back(1.52);
    m_egammaetabins_lo.push_back(1.7);
    m_egammaetabins_lo.push_back(1.9);
    m_egammaetabins_lo.push_back(2.1);
    m_egammaetabins_hi.push_back(0.2);
    m_egammaetabins_hi.push_back(0.4);
    m_egammaetabins_hi.push_back(0.6);
    m_egammaetabins_hi.push_back(0.8);
    m_egammaetabins_hi.push_back(1.0);
    m_egammaetabins_hi.push_back(1.2);
    m_egammaetabins_hi.push_back(1.37);
    m_egammaetabins_hi.push_back(1.52);
    m_egammaetabins_hi.push_back(1.7);
    m_egammaetabins_hi.push_back(1.9);
    m_egammaetabins_hi.push_back(2.1);
    m_egammaetabins_hi.push_back(2.37);

    m_egammapt2dbins_lo.push_back(25.);
    m_egammapt2dbins_lo.push_back(35.);
    m_egammapt2dbins_lo.push_back(45.);
    m_egammapt2dbins_lo.push_back(60.);
    m_egammapt2dbins_hi.push_back(35.);
    m_egammapt2dbins_hi.push_back(45.);
    m_egammapt2dbins_hi.push_back(60.);
    m_egammapt2dbins_hi.push_back(99999.);

    m_egammaeta2dbins_lo.push_back(0.);
    m_egammaeta2dbins_lo.push_back(0.5);
    m_egammaeta2dbins_lo.push_back(1);
    m_egammaeta2dbins_lo.push_back(1.37);
    m_egammaeta2dbins_lo.push_back(1.52);
    m_egammaeta2dbins_lo.push_back(2);
    m_egammaeta2dbins_hi.push_back(0.5);
    m_egammaeta2dbins_hi.push_back(1);
    m_egammaeta2dbins_hi.push_back(1.37);
    m_egammaeta2dbins_hi.push_back(1.52);
    m_egammaeta2dbins_hi.push_back(2);
    m_egammaeta2dbins_hi.push_back(2.37);

    // print
    m_notify = false;

    // save
    m_saveopt = "recreate";
    m_saveto = "";
    m_savekey = "";

    // constants
    m_zmass = 91187.6;
    m_cut_ph_pt = 15000.;
    m_cut_zveto = 5000.;
    m_cut_drphj = 0.5;
    m_cut_drphl = 0.7;
}

SampleAnalyzor::~SampleAnalyzor()
{
    delete m_lg;
}

void SampleAnalyzor::AddFile(vector<string> filename) {
    m_filenames = filename;
}

void SampleAnalyzor::AddTreeName(string treename) {
    m_treenames.push_back(treename);
}

void SampleAnalyzor::AddVariation(string variation) {
    m_variations.push_back(variation);
}

void SampleAnalyzor::NoPhJOR() {
    m_phj_or = false;
}

void SampleAnalyzor::LargeEta() {
    m_large_eta = true;
}

void SampleAnalyzor::UseEFakeEtaSF(bool etasf) {
    m_use_efake_eta_SF = etasf;
}

void SampleAnalyzor::DoEFakeDr02(bool dr02) {
    if (dr02) m_efake_dr = 0.2;
}

void SampleAnalyzor::DoEFakeDr05(bool dr05) {
    if (dr05) m_efake_dr = 0.5;
}

void SampleAnalyzor::RunNEvt(int nevt) {
    m_nevt = nevt;
}

void SampleAnalyzor::RunNFile(int nfile) {
    m_nfile = nfile;
}

void SampleAnalyzor::ShowOverflow(bool of) {
    m_show_of = of;
}

void SampleAnalyzor::SetSimulation(string simulation) {
    m_simulation = simulation;
}

void SampleAnalyzor::SetProcess(string process) {
    m_process = process;
}

void SampleAnalyzor::SetType(string type) {
    m_type = type;
}

void SampleAnalyzor::SetCME(string cme) {
    m_cme = cme;
}

void SampleAnalyzor::UsePSTrigger(bool PS) {
    m_PSQCD = PS;
}

void SampleAnalyzor::NoContBtag(bool nocont) {
    m_cont_btag = !nocont;
}

void SampleAnalyzor::SetQCDPara(string para) {
    m_QCD_para = para;
    m_QCD_para_idx = -1;
    vector<TString> paras;
    paras.push_back("pt");
    paras.push_back("eta");
    paras.push_back("dR");
    paras.push_back("jetpt");
    paras.push_back("dPhi");
    paras.push_back("nbtag");
    paras.push_back("mtw");
    paras.push_back("pt:eta");
    paras.push_back("pt:dR");
    paras.push_back("pt:jetpt");
    paras.push_back("pt:dPhi");
    paras.push_back("pt:nbtag");
    paras.push_back("pt:mtw");
    paras.push_back("eta:dR");
    paras.push_back("eta:jetpt");
    paras.push_back("eta:dPhi");
    paras.push_back("eta:nbtag");
    paras.push_back("eta:mtw");
    paras.push_back("dR:jetpt");
    paras.push_back("dR:dPhi");
    paras.push_back("dR:nbtag");
    paras.push_back("dR:mtw");
    paras.push_back("jetpt:dPhi");
    paras.push_back("jetpt:nbtag");
    paras.push_back("jetpt:mtw");
    paras.push_back("dPhi:nbtag");
    paras.push_back("dPhi:mtw");
    paras.push_back("nbtag:mtw");
    paras.push_back("pt:eta:dR");
    paras.push_back("pt:eta:jetpt");
    paras.push_back("pt:eta:dPhi");
    paras.push_back("pt:eta:nbtag");
    paras.push_back("pt:eta:mtw");
    paras.push_back("pt:dR:jetpt");
    paras.push_back("pt:dR:dPhi");
    paras.push_back("pt:dR:nbtag");
    paras.push_back("pt:dR:mtw");
    paras.push_back("pt:jetpt:dPhi");
    paras.push_back("pt:jetpt:nbtag");
    paras.push_back("pt:jetpt:mtw");
    paras.push_back("pt:dPhi:nbtag");
    paras.push_back("pt:dPhi:mtw");
    paras.push_back("pt:nbtag:mtw");
    paras.push_back("eta:dR:jetpt");
    paras.push_back("eta:dR:dPhi");
    paras.push_back("eta:dR:nbtag");
    paras.push_back("eta:dR:mtw");
    paras.push_back("eta:jetpt:dPhi");
    paras.push_back("eta:jetpt:nbtag");
    paras.push_back("eta:jetpt:mtw");
    paras.push_back("eta:dPhi:nbtag");
    paras.push_back("eta:dPhi:mtw");
    paras.push_back("eta:nbtag:mtw");
    paras.push_back("dR:jetpt:dPhi");
    paras.push_back("dR:jetpt:nbtag");
    paras.push_back("dR:jetpt:mtw");
    paras.push_back("dR:dPhi:nbtag");
    paras.push_back("dR:dPhi:mtw");
    paras.push_back("dR:nbtag:mtw");
    paras.push_back("jetpt:dPhi:nbtag");
    paras.push_back("jetpt:dPhi:mtw");
    paras.push_back("jetpt:nbtag:mtw");
    paras.push_back("dPhi:nbtag:mtw");
    bool found = false;
    for (int i = 0; i < paras.size(); i++) {
	if (paras.at(i) == m_QCD_para) {
	    m_QCD_para_idx = i;
	    found = true;
	    break;
	}
    }
    if (!found) {
	m_lg->Err("ss", "Unsupported QCD para option ->", m_QCD_para.c_str());
	exit(-1);
    }
    m_QCD_1para = true;
}

void SampleAnalyzor::Selection(string selection) {
    m_selection = selection;
}

void SampleAnalyzor::SelectTTBarSG(bool sg) {
    m_ttsg = sg;
}

void SampleAnalyzor::SelectTTBarNoTau(bool notau) {
    m_notau = notau;
}

void SampleAnalyzor::SelectTTBarDI(bool di, bool ee, bool em, bool et) {
    m_ttdi = di;
    m_ttee = ee;
    m_ttem = em;
    m_ttet = et;
}

void SampleAnalyzor::Quiet(bool quiet) {
    m_quiet = quiet;
}

void SampleAnalyzor::TestMode(bool test) {
    m_test = test;
}

void SampleAnalyzor::RunOnePct(bool pct) {
    m_1pct = pct;
}

void SampleAnalyzor::RunTenPct(bool pct) {
    m_10pct = pct;
}

void SampleAnalyzor::Debug(bool debug) {
    m_debug = debug;
}

void SampleAnalyzor::ReweightEGamma(bool reweight) {
    m_reweight_egamma = reweight;
}

void SampleAnalyzor::ReweightHFake(bool reweight) {
    m_reweight_hfake = reweight;
}

void SampleAnalyzor::ReweightHFake025(bool reweight025) {
    m_reweight_hfake_025 = reweight025;
}

void SampleAnalyzor::ReweightHFake05(bool reweight05) {
    m_reweight_hfake_05 = reweight05;
}

void SampleAnalyzor::ReweightHFake075(bool reweight075) {
    m_reweight_hfake_075 = reweight075;
}

void SampleAnalyzor::ReweightZGamma(bool reweight) {
    m_reweight_zgamma = reweight;
}

void SampleAnalyzor::CaliEGamma(bool cali, string type) {
    m_cali_egamma = cali;
    m_cali_egamma_type = type;
}

void SampleAnalyzor::UseXsectionList(string xsection) {
    m_xsectionlist = xsection;
}

void SampleAnalyzor::UseHistogramList(string histogramlist, string histogramlist_2D) {
    m_histogramlist = histogramlist;
    m_2Dhistogramlist = histogramlist_2D;
}

void SampleAnalyzor::DoHist(bool fill, bool fillsys) {
    m_do_hists = fill;
    m_do_hists_sys = fillsys;
}

void SampleAnalyzor::DoEGammaHist(bool fill) {
    m_do_egammahists = fill;
}

void SampleAnalyzor::DoEGammaTF1(bool fill) {
    m_do_egammatf1 = fill;
}

void SampleAnalyzor::DoPhMatch(bool match, string type) {
    m_do_phmatch = match;
    m_phmatch_type = type;
}

void SampleAnalyzor::DoLpMatch(bool match, string type) {
    m_do_lpmatch = match;
    m_lpmatch_type = type;
}

void SampleAnalyzor::DoJetMatch(bool match) {
    m_do_jetmatch = match;
}

void SampleAnalyzor::SetRegion(string region) {
    m_lumi = 3212.96 + 33257.2;
    m_region = region;
}

void SampleAnalyzor::SetSubRegion(string subregion) {
    m_subregion = subregion;
}

void SampleAnalyzor::InitEvent() {
    if (m_debug) m_lg->Info("s", "Initialize event");

    // test
    //if (selph_index1 >= 0) {
    //    cout << int(ph_pt->at(selph_index1))/1000 << " " << fabs(ph_eta->at(selph_index1)) << " " << efake_sf_Nominal->at(selph_index1) << endl;
    //}

    // KLFitter
    if (m_do_klfitter) {
	//m_vjet_klfitter.clear();
	//m_ph_klfitter = NULL;
	//m_el_klfitter = NULL;
	//m_mu_klfitter = NULL;
    }

    if (m_type == "Data" || m_type == "QCD") mu /= 1.09;

    // event level
    evtnum = -1;
    m_is2015 = false;

    m_period = 0;
    if (m_type == "Data" || m_type == "QCD") {
	if (runNumber == 276262 || runNumber == 276329 || runNumber == 276336 || runNumber == 276416 || runNumber == 276511 || runNumber == 276689 || runNumber == 276778 || runNumber == 276790 || runNumber == 276952 || runNumber == 276954) m_period = 1;
    	else if (runNumber == 278880 || runNumber == 278912 || runNumber == 278968 || runNumber == 279169 || runNumber == 279259 || runNumber == 279279 || runNumber == 279284 || runNumber == 279345 || runNumber == 279515 || runNumber == 279598 || runNumber == 279685 || runNumber == 279813 || runNumber == 279867 || runNumber == 279928) m_period = 2;
    	else if (runNumber == 279932 || runNumber == 279984 || runNumber == 280231 || runNumber == 280273 || runNumber == 280319 || runNumber == 280368) m_period = 3;
    	else if (runNumber == 280423 || runNumber == 280464 || runNumber == 280500 || runNumber == 280520 || runNumber == 280614 || runNumber == 280673 || runNumber == 280753 || runNumber == 280853 || runNumber == 280950 || runNumber == 280977 || runNumber == 281070 || runNumber == 281074 || runNumber == 281075) m_period = 4;
    	else if (runNumber == 281317 || runNumber == 281385 || runNumber == 281411) m_period = 5;
    	else if (runNumber == 282625 || runNumber == 282631 || runNumber == 282712 || runNumber == 282784 || runNumber == 282992 || runNumber == 283074 || runNumber == 283155 || runNumber == 283270 || runNumber == 283429 || runNumber == 283608 || runNumber == 283780 || runNumber == 284006 || runNumber == 284154 || runNumber == 284213 || runNumber == 284285 || runNumber == 284420 || runNumber == 284427 || runNumber == 284484) m_period = 6;
    	else if (runNumber == 297730 || runNumber == 298595 || runNumber == 298609 || runNumber == 298633 || runNumber == 298687 || runNumber == 298690 || runNumber == 298771 || runNumber == 298773 || runNumber == 298862 || runNumber == 298967 || runNumber == 299055 || runNumber == 299144 || runNumber == 299147 || runNumber == 299184 || runNumber == 299243 || runNumber == 299584 || runNumber == 300279) m_period = 7;
    	else if (runNumber == 300345 || runNumber == 300415 || runNumber == 300418 || runNumber == 300487 || runNumber == 300540 || runNumber == 300571 || runNumber == 300600 || runNumber == 300655 || runNumber == 300687 || runNumber == 300784 || runNumber == 300800 || runNumber == 300863 || runNumber == 300908) m_period = 8;
    	else if (runNumber == 301912 || runNumber == 301918 || runNumber == 301932 || runNumber == 301973 || runNumber == 302053 || runNumber == 302137 || runNumber == 302265 || runNumber == 302269 || runNumber == 302300 || runNumber == 302347 || runNumber == 302380 || runNumber == 302391 || runNumber == 302393) m_period = 9;
    	else if (runNumber == 302737 || runNumber == 302831 || runNumber == 302872 || runNumber == 302919 || runNumber == 302925 || runNumber == 302956 || runNumber == 303007 || runNumber == 303079 || runNumber == 303201 || runNumber == 303208 || runNumber == 303264 || runNumber == 303266 || runNumber == 303291 || runNumber == 303304 || runNumber == 303338 || runNumber == 303421 || runNumber == 303499 || runNumber == 303560) m_period = 10;
    	else if (runNumber == 303638 || runNumber == 303832 || runNumber == 303846 || runNumber == 303892) m_period = 11;
    	else if (runNumber == 303943 || runNumber == 304006 || runNumber == 304008 || runNumber == 304128 || runNumber == 304178 || runNumber == 304198 || runNumber == 304211 || runNumber == 304243 || runNumber == 304308 || runNumber == 304337 || runNumber == 304409 || runNumber == 304431 || runNumber == 304494) m_period = 12;
    	else if (runNumber == 305380 || runNumber == 305543 || runNumber == 305571 || runNumber == 305618 || runNumber == 305671 || runNumber == 305674 || runNumber == 305723 || runNumber == 305727 || runNumber == 305735 || runNumber == 305777 || runNumber == 305811 || runNumber == 305920 || runNumber == 306269 || runNumber == 306278 || runNumber == 306310 || runNumber == 306384 || runNumber == 306419 || runNumber == 306442 || runNumber == 306448 || runNumber == 306451) m_period = 13;
    	else if (runNumber == 307126 || runNumber == 307195 || runNumber == 307259 || runNumber == 307306 || runNumber == 307354 || runNumber == 307358 || runNumber == 307394 || runNumber == 307454 || runNumber == 307514 || runNumber == 307539 || runNumber == 307569 || runNumber == 307601 || runNumber == 307619 || runNumber == 307656 || runNumber == 307710 || runNumber == 307716 || runNumber == 307732 || runNumber == 307861 || runNumber == 307935 || runNumber == 308047 || runNumber == 308084) m_period = 14;
    	else if (runNumber == 309375 || runNumber == 309390 || runNumber == 309440 || runNumber == 309516 || runNumber == 309640 || runNumber == 309674 || runNumber == 309759) m_period = 15;
    	else if (runNumber == 310015 || runNumber == 310247 || runNumber == 310249 || runNumber == 310341 || runNumber == 310370 || runNumber == 310405 || runNumber == 310468 || runNumber == 310473 || runNumber == 310634 || runNumber == 310691 || runNumber == 310738 || runNumber == 310809 || runNumber == 310863 || runNumber == 310872 || runNumber == 310969 || runNumber == 311071 || runNumber == 311170 || runNumber == 311244 || runNumber == 311287 || runNumber == 311321 || runNumber == 311365 || runNumber == 311402 || runNumber == 311473) m_period = 16;
    	else {
    	    m_period = 17;
    	}
    } else {
	if (randomRunNumber == 276262 || randomRunNumber == 276329 || randomRunNumber == 276336 || randomRunNumber == 276416 || randomRunNumber == 276511 || randomRunNumber == 276689 || randomRunNumber == 276778 || randomRunNumber == 276790 || randomRunNumber == 276952 || randomRunNumber == 276954) m_period = 1;
    	else if (randomRunNumber == 278880 || randomRunNumber == 278912 || randomRunNumber == 278968 || randomRunNumber == 279169 || randomRunNumber == 279259 || randomRunNumber == 279279 || randomRunNumber == 279284 || randomRunNumber == 279345 || randomRunNumber == 279515 || randomRunNumber == 279598 || randomRunNumber == 279685 || randomRunNumber == 279813 || randomRunNumber == 279867 || randomRunNumber == 279928) m_period = 2;
    	else if (randomRunNumber == 279932 || randomRunNumber == 279984 || randomRunNumber == 280231 || randomRunNumber == 280273 || randomRunNumber == 280319 || randomRunNumber == 280368) m_period = 3;
    	else if (randomRunNumber == 280423 || randomRunNumber == 280464 || randomRunNumber == 280500 || randomRunNumber == 280520 || randomRunNumber == 280614 || randomRunNumber == 280673 || randomRunNumber == 280753 || randomRunNumber == 280853 || randomRunNumber == 280950 || randomRunNumber == 280977 || randomRunNumber == 281070 || randomRunNumber == 281074 || randomRunNumber == 281075) m_period = 4;
    	else if (randomRunNumber == 281317 || randomRunNumber == 281385 || randomRunNumber == 281411) m_period = 5;
    	else if (randomRunNumber == 282625 || randomRunNumber == 282631 || randomRunNumber == 282712 || randomRunNumber == 282784 || randomRunNumber == 282992 || randomRunNumber == 283074 || randomRunNumber == 283155 || randomRunNumber == 283270 || randomRunNumber == 283429 || randomRunNumber == 283608 || randomRunNumber == 283780 || randomRunNumber == 284006 || randomRunNumber == 284154 || randomRunNumber == 284213 || randomRunNumber == 284285 || randomRunNumber == 284420 || randomRunNumber == 284427 || randomRunNumber == 284484) m_period = 6;
    	else if (randomRunNumber == 297730 || randomRunNumber == 298595 || randomRunNumber == 298609 || randomRunNumber == 298633 || randomRunNumber == 298687 || randomRunNumber == 298690 || randomRunNumber == 298771 || randomRunNumber == 298773 || randomRunNumber == 298862 || randomRunNumber == 298967 || randomRunNumber == 299055 || randomRunNumber == 299144 || randomRunNumber == 299147 || randomRunNumber == 299184 || randomRunNumber == 299243 || randomRunNumber == 299584 || randomRunNumber == 300279) m_period = 7;
    	else if (randomRunNumber == 300345 || randomRunNumber == 300415 || randomRunNumber == 300418 || randomRunNumber == 300487 || randomRunNumber == 300540 || randomRunNumber == 300571 || randomRunNumber == 300600 || randomRunNumber == 300655 || randomRunNumber == 300687 || randomRunNumber == 300784 || randomRunNumber == 300800 || randomRunNumber == 300863 || randomRunNumber == 300908) m_period = 8;
    	else if (randomRunNumber == 301912 || randomRunNumber == 301918 || randomRunNumber == 301932 || randomRunNumber == 301973 || randomRunNumber == 302053 || randomRunNumber == 302137 || randomRunNumber == 302265 || randomRunNumber == 302269 || randomRunNumber == 302300 || randomRunNumber == 302347 || randomRunNumber == 302380 || randomRunNumber == 302391 || randomRunNumber == 302393) m_period = 9;
    	else if (randomRunNumber == 302737 || randomRunNumber == 302831 || randomRunNumber == 302872 || randomRunNumber == 302919 || randomRunNumber == 302925 || randomRunNumber == 302956 || randomRunNumber == 303007 || randomRunNumber == 303079 || randomRunNumber == 303201 || randomRunNumber == 303208 || randomRunNumber == 303264 || randomRunNumber == 303266 || randomRunNumber == 303291 || randomRunNumber == 303304 || randomRunNumber == 303338 || randomRunNumber == 303421 || randomRunNumber == 303499 || randomRunNumber == 303560) m_period = 10;
    	else if (randomRunNumber == 303638 || randomRunNumber == 303832 || randomRunNumber == 303846 || randomRunNumber == 303892) m_period = 11;
    	else if (randomRunNumber == 303943 || randomRunNumber == 304006 || randomRunNumber == 304008 || randomRunNumber == 304128 || randomRunNumber == 304178 || randomRunNumber == 304198 || randomRunNumber == 304211 || randomRunNumber == 304243 || randomRunNumber == 304308 || randomRunNumber == 304337 || randomRunNumber == 304409 || randomRunNumber == 304431 || randomRunNumber == 304494) m_period = 12;
    	else if (randomRunNumber == 305380 || randomRunNumber == 305543 || randomRunNumber == 305571 || randomRunNumber == 305618 || randomRunNumber == 305671 || randomRunNumber == 305674 || randomRunNumber == 305723 || randomRunNumber == 305727 || randomRunNumber == 305735 || randomRunNumber == 305777 || randomRunNumber == 305811 || randomRunNumber == 305920 || randomRunNumber == 306269 || randomRunNumber == 306278 || randomRunNumber == 306310 || randomRunNumber == 306384 || randomRunNumber == 306419 || randomRunNumber == 306442 || randomRunNumber == 306448 || randomRunNumber == 306451) m_period = 13;
    	else if (randomRunNumber == 307126 || randomRunNumber == 307195 || randomRunNumber == 307259 || randomRunNumber == 307306 || randomRunNumber == 307354 || randomRunNumber == 307358 || randomRunNumber == 307394 || randomRunNumber == 307454 || randomRunNumber == 307514 || randomRunNumber == 307539 || randomRunNumber == 307569 || randomRunNumber == 307601 || randomRunNumber == 307619 || randomRunNumber == 307656 || randomRunNumber == 307710 || randomRunNumber == 307716 || randomRunNumber == 307732 || randomRunNumber == 307861 || randomRunNumber == 307935 || randomRunNumber == 308047 || randomRunNumber == 308084) m_period = 14;
    	else if (randomRunNumber == 309375 || randomRunNumber == 309390 || randomRunNumber == 309440 || randomRunNumber == 309516 || randomRunNumber == 309640 || randomRunNumber == 309674 || randomRunNumber == 309759) m_period = 15;
    	else if (randomRunNumber == 310015 || randomRunNumber == 310247 || randomRunNumber == 310249 || randomRunNumber == 310341 || randomRunNumber == 310370 || randomRunNumber == 310405 || randomRunNumber == 310468 || randomRunNumber == 310473 || randomRunNumber == 310634 || randomRunNumber == 310691 || randomRunNumber == 310738 || randomRunNumber == 310809 || randomRunNumber == 310863 || randomRunNumber == 310872 || randomRunNumber == 310969 || randomRunNumber == 311071 || randomRunNumber == 311170 || randomRunNumber == 311244 || randomRunNumber == 311287 || randomRunNumber == 311321 || randomRunNumber == 311365 || randomRunNumber == 311402 || randomRunNumber == 311473) m_period = 16;
    	else {
    	    m_period = 17;
    	}
    }

    if (m_debug) m_lg->Info("s", "Period calculated");
    if (ejets_2015 || mujets_2015 || ee_2015 || mumu_2015 || emu_2015 || ttel_ee_2015 || ttel_mue_2015 || zee_2015 || zeg_2015 || eeSS_2015 || mumuSS_2015 || emuSS_2015) m_is2015 = true;

    // jet
    n_jet = 0;
    n_bjet = 0;
    n_bjet77 = 0;
    n_bjet70 = 0;
    TLorentzVector tlv_leadjet;
    leadjet_pt = -1;
    leadjet_phi = 999;
    leadjet_eta = 999;
    leadjet_asm = 999;
    subjet_pt = -1;
    subjet_eta = 999;
    drleadjet_leadph = 999;
    drleadjet_leadlep = 999;
    m_12jet = -1;
    m_123jet = -1;

    // photon
    n_ph = 0;
    n_ph_good = 0;
    n_ph_hfake = 0;
    n_ph_iso = 0;
    n_ph_noid_noiso = 0;
    n_ph_good_index.clear();
    n_ph_hfake_index.clear();
    n_ph_iso_index.clear();
    n_ph_noid_noiso_index.clear();
    leadph_pt = -1;
    leadph_cvt = -1;
    leadph_bincvt = -1;
    leadph_HFT_MVA = -1;
    leadph_mc_pt = -1;
    leadph_ptreco_pttrue = -1;
    leadph_drrecotrue = -1;
    leadph_mc_eta = -1;
    leadph_mc_phi = -1;
    leadph_mc_e = -1;
    leadph_mc_dr = -1;
    leadph_mc_pid = 0;
    leadph_faketype = -1;
    leadph_mc_barcode = 0;
    leadph_mc_el_pt = -1;
    leadph_mc_el_eta = -1;
    leadph_mc_el_phi = -1;
    leadph_mc_el_dr = -1;
    leadph_eta = 999;
    leadph_phi = 999;
    leadph_m = -1;
    leadph_e = -1;
    leadph_ptcone = -1;
    leadph_etcone = -1;
    //leadph_tight = '';
    leadphleadlep_dphi = 999;
    leadph_mindrphl = 999;
    detaleplep = -1;
    dphileplep = -1;
    leadph_mindrphj = 999;
    leadph_ancestor = 0;
    leadph_origin = 999;
    leadph_type = 999;
    leadph_rhad1 = 999;
    leadph_rhad = 999;
    leadph_reta = 999;
    leadph_weta2 = 999;
    leadph_rphi = 999;
    leadph_ws3 = 999;
    leadph_wstot = 999;
    leadph_fracm = 999;
    leadph_deltaE = 999;
    leadph_eratio = 999;
    leadph_emaxs1 = 999;
    leadph_f1 = 999;
    leadph_e277 = 999;

    // lepton
    n_lep = 0;
    n_el = 0;
    n_el_good_index.clear();
    n_mu = 0;
    n_mu_good_index.clear();
    leadlep_pt = -1;
    leadlep_eta = 999;
    leadlep_phi = 999;
    leadlep_m = -1;
    leadlep_e = -1;
    leadlep_mc_pt = -1;
    leadlep_mc_eta = -1;
    leadlep_mc_phi = -1;
    leadlep_mc_e = -1;
    leadlep_mc_dr = -1;
    //leadlep_HLT_e24_lhmedium_L1EM20VH = '';
    //leadlep_HLT_e60_lhmedium = '';
    //leadlep_HLT_e120_lhloose = '';
    //leadlep_HLT_e26_lhtight_nod0_ivarloose = '';
    //leadlep_HLT_e60_lhmedium_nod0 = '';
    //leadlep_HLT_e140_lhloose_nod0 = '';
    leadlep_origin = 999;
    leadlep_type = 999;
    sublep_pt = -1;
    sublep_eta = 999;
    sublep_phi = 999;
    sublep_mc_pt = -1;
    sublep_mc_eta = -1;
    sublep_mc_phi = -1;
    sublep_mc_e = -1;
    sublep_mc_dr = -1;
    sublep_m = -1;
    sublep_e = -1;
    sublep_ptcone = -1;
    sublep_etcone = -1;
    sublep_origin = 999;
    sublep_type = 999;
    //sublep_HLT_e24_lhmedium_L1EM20VH = '';
    //sublep_HLT_e60_lhmedium = '';
    //sublep_HLT_e120_lhloose = '';
    //sublep_HLT_e26_lhtight_nod0_ivarloose = '';
    //sublep_HLT_e60_lhmedium_nod0 = '';
    //sublep_HLT_e140_lhloose_nod0 = '';
    leadlepsublep_dphi = 999;
    leadlepsublep_deta = 999;
    sublepleadlep_dphi = 999;
    leadlep_mindrphl = 999;
    leadlep_mindrphj = 999;
    sublep_mindrphl = 999;
    sublep_mindrphj = 999;
    drleplep = -999;

    // met
    met = -1;
    met_x = -1;
    met_y = -1;

    // W mass
    mwt = -1;
    mwt2 = -1;
    dphilepmet = -1;
    dphiphmet = -1;
    mt_lphmet = -1;

    // invariant mass
    mphl = -1;
    mmcphl = -1;
    mmcelphl = -1;
    mmcll = -1;
    mlmcl = -1;
    mmclmcl = -1;
    mll = -1;
    mllg = -1;

    // weight
    weight = 1;
    weights_QCD.clear();
    weights_bootstrap.clear();

    kl_best_CA = -99;
    kl_best_top_pole_mass = -1;
    kl_best_reco_hW_mass = -1;
    kl_best_reco_lW_mass = -1;
    kl_best_reco_htop_mass = -1;
    kl_best_reco_htop_pt = -1;
    kl_best_reco_htop_eta = 9999;
    kl_best_reco_ltop_mass = -1;
    kl_best_reco_ltop_pt = -1;
    kl_best_reco_ltop_eta = 9999;

    kl_CA = -99;
    kl_top_pole_mass = -1;
    kl_reco_hW_mass = -1;
    kl_reco_lW_mass = -1;
    kl_reco_htop_mass = -1;
    kl_reco_htop_pt = -1;
    kl_reco_htop_eta = 9999;
    kl_reco_ltop_mass = -1;
    kl_reco_ltop_pt = -1;
    kl_reco_ltop_eta = 9999;

    kl_lt_CA = -99;
    kl_lt_top_pole_mass = -1;
    kl_lt_reco_hW_mass = -1;
    kl_lt_reco_lW_mass = -1;
    kl_lt_reco_htop_mass = -1;
    kl_lt_reco_htop_pt = -1;
    kl_lt_reco_htop_eta = 9999;
    kl_lt_reco_ltop_mass = -1;
    kl_lt_reco_ltop_pt = -1;
    kl_lt_reco_ltop_eta = 9999;

    kl_ht_CA = -99;
    kl_ht_top_pole_mass = -1;
    kl_ht_reco_hW_mass = -1;
    kl_ht_reco_lW_mass = -1;
    kl_ht_reco_htop_mass = -1;
    kl_ht_reco_htop_pt = -1;
    kl_ht_reco_htop_eta = 9999;
    kl_ht_reco_ltop_mass = -1;
    kl_ht_reco_ltop_pt = -1;
    kl_ht_reco_ltop_eta = 9999;

    kl_lW_CA = -99;
    kl_lW_top_pole_mass = -1;
    kl_lW_reco_hW_mass = -1;
    kl_lW_reco_lW_mass = -1;
    kl_lW_reco_htop_mass = -1;
    kl_lW_reco_htop_pt = -1;
    kl_lW_reco_htop_eta = 9999;
    kl_lW_reco_ltop_mass = -1;
    kl_lW_reco_ltop_pt = -1;
    kl_lW_reco_ltop_eta = 9999;

    kl_hW_CA = -99;
    kl_hW_top_pole_mass = -1;
    kl_hW_reco_hW_mass = -1;
    kl_hW_reco_lW_mass = -1;
    kl_hW_reco_htop_mass = -1;
    kl_hW_reco_htop_pt = -1;
    kl_hW_reco_htop_eta = 9999;
    kl_hW_reco_ltop_mass = -1;
    kl_hW_reco_ltop_pt = -1;
    kl_hW_reco_ltop_eta = 9999;

    tlv_jets.clear();
    tlv_leps.clear();
    tlv_phs.clear();
    if (m_debug) m_lg->Info("s", "Ready to go");
    if (m_type == "Upgrade") {
	if (m_debug) m_lg->Info("s", "Initialize upgrade");
        evtnum = eventNumber;
    
	if (m_debug) m_lg->Info("s", "Initialize el");

	for (int i = 0; i < el_pt->size(); i++) {
	    if (m_debug) m_lg->Info("s", "Initialize el 1");
	    if (m_do_lpmatch) {
		if (m_lpmatch_type == "TrueLp") {
		    if (el_faketype->at(i) != 0) continue;
		} else if (m_lpmatch_type == "HFake") {
		    if (el_faketype->at(i) != 2) continue;
		}
	    }
	    bool isprompt = isPromptElectron(el_true_type->at(i),el_true_origin->at(i),el_true_type->at(i),el_true_origin->at(i),11,el_charge->at(i)).first;
	    if (m_debug) m_lg->Info("s", "Initialize el 2");
	    double etacut = 2.47;
	    if (m_large_eta) etacut = 4.0;
	    if (el_pt->at(i) >= 25000. && fabs(el_eta->at(i)) < etacut && (fabs(el_eta->at(i)) < 1.37 || fabs(el_eta->at(i)) > 1.52) && isprompt) {
	        n_el_good_index.push_back(i);
	    }
	    if (m_debug) m_lg->Info("s", "Initialize el 3");
	}
	if (m_debug) m_lg->Info("s", "Initialize mu");
	for (int i = 0; i < mu_pt->size(); i++) {
	    if (m_debug) m_lg->Info("s", "Initialize mu 1");
	    if (m_do_lpmatch) {
		if (m_lpmatch_type == "TrueLp") {
		} else if (m_lpmatch_type == "HFake") {
		    continue;
		}
	    }
	    double etacut = 2.5;
	    if (m_large_eta) etacut = 4.0;
	    if (mu_pt->at(i) >= 25000. && fabs(mu_eta->at(i)) < etacut && mu_true_isPrompt->at(i)) {
	        n_mu_good_index.push_back(i);
	    }
	   if (m_debug) m_lg->Info("s", "Initialize mu 2");
	}
        n_el = n_el_good_index.size();
        n_mu = n_mu_good_index.size();

        n_lep = n_el + n_mu;
        n_ph = ph_pt->size();	
	if (m_debug) m_lg->Info("s", "Initialize photon");
	for (int i = 0; i < n_ph; i++) {
	    if (m_do_phmatch) {
		if (m_phmatch_type == "TruePh") {
	    	    if (ph_faketype->at(i) != 0) continue;
	    	    if ((ph_mc_Origin->at(i)==23 || ph_mc_Origin->at(i)==24 || ph_mc_Origin->at(i)==25 || ph_mc_Origin->at(i)==26 || ph_mc_Origin->at(i)==27 || ph_mc_Origin->at(i)==28 || ph_mc_Origin->at(i)==29 || ph_mc_Origin->at(i)==30 || ph_mc_Origin->at(i)==31 || ph_mc_Origin->at(i)==32 || ph_mc_Origin->at(i)==33 || ph_mc_Origin->at(i)==34 || ph_mc_Origin->at(i)==35 || ph_mc_Origin->at(i)==42) && ph_mc_Type->at(i) == 16) continue;
	    	} else if (m_phmatch_type == "HFake") {
	    	    if (ph_faketype->at(i) != 2) continue;
	    	} else if (m_phmatch_type == "EFake") {
	    	    if (ph_faketype->at(i) != 1) continue;
	    	}
	    }
	    double etacut = 2.37;
	    if (m_large_eta) etacut = 4.0;
	    if (ph_pt->at(i) >= 20000. && fabs(ph_eta->at(i)) < etacut && (fabs(ph_eta->at(i)) < 1.37 || fabs(ph_eta->at(i)) > 1.52)) {
	        n_ph_good_index.push_back(i);
		TLorentzVector tmp_tlv_ph; tmp_tlv_ph.SetPtEtaPhiE(ph_pt->at(i), ph_eta->at(i), ph_phi->at(i), ph_e->at(i));
		tlv_phs.push_back(tmp_tlv_ph);
	    }
	}
	if (m_debug) m_lg->Info("s", "Initialize n good photon");
	n_ph_good = n_ph_good_index.size();

	event_ngoodphotons = n_ph_good;
	if (n_ph_good >= 1) selph_index1 = n_ph_good_index.at(0);
	else selph_index1 = -1;

        if (n_ph_good >= 1) {
	    leadph_pt = ph_pt->at(selph_index1);
    	    leadph_eta = ph_eta->at(selph_index1);
    	    leadph_phi = ph_phi->at(selph_index1);
    	    leadph_e = ph_e->at(selph_index1);
	    tlv_leadph.SetPtEtaPhiE(leadph_pt, leadph_eta, leadph_phi, leadph_e);
	    leadph_m = tlv_leadph.M();
    	    leadph_type = ph_mc_Type->at(selph_index1);
    	    leadph_origin = ph_mc_Origin->at(selph_index1);
	    leadph_faketype = ph_faketype->at(selph_index1);
        }
    
        if (n_el >= 1 && n_mu == 0) {
	    leadlep_pt  = el_pt->at(n_el_good_index.at(0));
	    leadmu_pt = 0;
	    leadel_pt  = el_pt->at(n_el_good_index.at(0));
	    leadlep_eta = el_eta->at(n_el_good_index.at(0));
	    leadlep_phi = el_phi->at(n_el_good_index.at(0));
	    leadlep_e   = el_e->at(n_el_good_index.at(0));
	    tlv_leadlep.SetPtEtaPhiE(leadlep_pt, leadlep_eta, leadlep_phi, leadlep_e);
	    leadlep_m = tlv_leadlep.M();
	    tlv_leps.push_back(tlv_leadlep);
	    if (n_el >= 2) { 
	        sublep_pt  = el_pt->at(n_el_good_index.at(1));
	        sublep_eta = el_eta->at(n_el_good_index.at(1));
	        sublep_phi = el_phi->at(n_el_good_index.at(1));
	        sublep_e   = el_e->at(n_el_good_index.at(1));
	        tlv_sublep.SetPtEtaPhiE(sublep_pt, sublep_eta, sublep_phi, sublep_e);
	        sublep_m = tlv_sublep.M();
		tlv_leps.push_back(tlv_sublep);
	    }
        } else if (n_mu >= 1 && n_el == 0) {
	    leadlep_pt  = mu_pt->at(n_mu_good_index.at(0));
	    leadel_pt = 0;
	    leadmu_pt  = mu_pt->at(n_mu_good_index.at(0));
	    leadlep_eta = mu_eta->at(n_mu_good_index.at(0));
	    leadlep_phi = mu_phi->at(n_mu_good_index.at(0));
	    leadlep_e   = mu_e->at(n_mu_good_index.at(0));
	    tlv_leadlep.SetPtEtaPhiE(leadlep_pt, leadlep_eta, leadlep_phi, leadlep_e);
	    leadlep_m = tlv_leadlep.M();
	    tlv_leps.push_back(tlv_leadlep);
	    if (n_mu >= 2) {
	        sublep_pt  = mu_pt->at(n_mu_good_index.at(1));
	        sublep_eta = mu_eta->at(n_mu_good_index.at(1));
	        sublep_phi = mu_phi->at(n_mu_good_index.at(1));
	        sublep_e   = mu_e->at(n_mu_good_index.at(1));
	        tlv_sublep.SetPtEtaPhiE(sublep_pt, sublep_eta, sublep_phi, sublep_e);
	        sublep_m = tlv_sublep.M();
		tlv_leps.push_back(tlv_sublep);
	    }
        } else if (n_el >= 1 && n_mu >= 1) {
	    leadel_pt = el_pt->at(n_el_good_index.at(0));
	    leadmu_pt = mu_pt->at(n_mu_good_index.at(0));
    	    if (leadel_pt > leadmu_pt) {
	        leadlep_pt  = el_pt->at(n_el_good_index.at(0));
	        leadlep_eta = el_eta->at(n_el_good_index.at(0));
	        leadlep_phi = el_phi->at(n_el_good_index.at(0));
	        leadlep_e   = el_e->at(n_el_good_index.at(0));
	        tlv_leadlep.SetPtEtaPhiE(leadlep_pt, leadlep_eta, leadlep_phi, leadlep_e);
	        leadlep_m = tlv_leadlep.M();
	        sublep_pt  = mu_pt->at(n_mu_good_index.at(0));
		sublep_eta = mu_eta->at(n_mu_good_index.at(0));
		sublep_phi = mu_phi->at(n_mu_good_index.at(0));
		sublep_e   = mu_e->at(n_mu_good_index.at(0));
		tlv_sublep.SetPtEtaPhiE(sublep_pt, sublep_eta, sublep_phi, sublep_e);
		sublep_m = tlv_sublep.M();
    	    } else {
		leadlep_pt  = mu_pt->at(n_mu_good_index.at(0));
            	leadlep_eta = mu_eta->at(n_mu_good_index.at(0));
            	leadlep_phi = mu_phi->at(n_mu_good_index.at(0));
            	leadlep_e   = mu_e->at(n_mu_good_index.at(0));
		tlv_leadlep.SetPtEtaPhiE(leadlep_pt, leadlep_eta, leadlep_phi, leadlep_e);
		leadlep_m = tlv_leadlep.M();
		sublep_pt  = el_pt->at(n_el_good_index.at(0));
            	sublep_eta = el_eta->at(n_el_good_index.at(0));
            	sublep_phi = el_phi->at(n_el_good_index.at(0));
            	sublep_e   = el_e->at(n_el_good_index.at(0));
		tlv_sublep.SetPtEtaPhiE(sublep_pt, sublep_eta, sublep_phi, sublep_e);
		sublep_m = tlv_sublep.M();
    	    }
	    tlv_leps.push_back(tlv_leadlep);
	    tlv_leps.push_back(tlv_sublep);
        }

	if (m_debug) m_lg->Info("s", "Initialize met");
	TLorentzVector met_tlv;
	met_tlv.SetPtEtaPhiM(met_met, met_phi, 0, 0);
	met_x = met_tlv.Px();
	met_y = met_tlv.Py();
    
	if (m_debug) m_lg->Info("s", "Initialize jet");
        for (int i = 0; i < jet_pt->size(); i++) {
	    double etacut = 2.5;
	    if (m_large_eta) etacut = 4.0;
	    if (jet_pt->at(i) > 25000 && fabs(jet_eta->at(i)) < etacut) {
		TLorentzVector tmp_jet;
		tmp_jet.SetPtEtaPhiE(jet_pt->at(i), jet_eta->at(i), jet_phi->at(i), jet_e->at(i));

		bool overlaped = false;
		for (int j = 0; j < tlv_leps.size(); j++) {
		    if (tmp_jet.DeltaR(tlv_leps.at(j)) < 0.4) {
			overlaped = true;
			break;
		    }
		}
		if (m_phj_or) {
		    for (int j = 0; j < tlv_phs.size(); j++) {
		        if (tmp_jet.DeltaR(tlv_phs.at(j)) < 0.4) {
		    	overlaped = true;
		    	break;
		        }
		    }
		}
		if (overlaped) continue;

		tlv_jets.push_back(tmp_jet);
		n_jet++;

		if (m_random->Rndm() < jet_mv1eff->at(i)) {
		    n_bjet++;
		}

		if (n_jet == 1) {
	    	    leadjet_pt = jet_pt->at(i);
	    	    leadjet_phi = jet_phi->at(i);
	    	    leadjet_eta = jet_eta->at(i);
	    	    if (jet_eta->at(i) > 0) leadjet_asm = 0.5;
	    	    else leadjet_asm = -0.5;
	    	    tlv_leadjet.SetPtEtaPhiE(jet_pt->at(i), jet_eta->at(i), jet_phi->at(i), jet_e->at(i));
	    	}
	    	if (n_jet == 2) {
	    	    subjet_pt = jet_pt->at(i);
	    	    subjet_eta = jet_eta->at(i);
	    	}
		if (m_do_klfitter) {
	    	    //TLorentzVector * vJet = new TLorentzVector(tmp_jet.Px()/1000., tmp_jet.Py()/1000., tmp_jet.Pz()/1000., tmp_jet.E()/1000.);
	    	    //m_vjet_klfitter.push_back(vJet);
	    	}
	    }
        }

	double pt_12jet = 0;
	double j1_id = -1;
	double j2_id = -1;
	if (tlv_jets.size() >= 2) {
	    for (int i = 0; i < tlv_jets.size(); i++) {
		TLorentzVector j1 = tlv_jets.at(i);
	        for (int j = i+1; j < tlv_jets.size(); j++) {
		    TLorentzVector j2 = tlv_jets.at(j);
		    double pt_12jet_tmp = (j1+j2).Pt();
		    if (pt_12jet_tmp > pt_12jet) {
			pt_12jet = pt_12jet_tmp;
			j1_id = i;
			j2_id = j;
		    }
	        }
	    }
	}
	if (j1_id != -1 && j2_id != -1) {
	    m_12jet = (tlv_jets.at(j1_id) + tlv_jets.at(j2_id)).M();
	}

        met = met_met;
	dphilepmet = fabs(met_phi - leadlep_phi);
	dphiphmet = fabs(met_phi - leadph_phi);
	mwt = event_mwt;
        mwt2 = sqrt(2. * leadjet_pt * met * (1. - cos(leadjet_phi - met_phi)));
	TLorentzVector tlv_phll = tlv_leadlep+tlv_leadph;
	double mt_lphmetl = sqrt(2. * tlv_phll.Pt() * met * (1. - cos(tlv_phll.Phi() - met_phi)));
	TLorentzVector tlv_phls = tlv_sublep+tlv_leadph;
	double mt_lphmets = sqrt(2. * tlv_phls.Pt() * met * (1. - cos(tlv_phls.Phi() - met_phi)));
	mt_lphmet = mt_lphmetl > mt_lphmets ? mt_lphmetl : mt_lphmets;

	if (m_do_klfitter) {
	    //if (m_subregion.find("ejets", 0) != string::npos) {
	    //    m_el_klfitter = new TLorentzVector(tlv_leadlep.Px()/1000., tlv_leadlep.Py()/1000., tlv_leadlep.Pz()/1000., tlv_leadlep.E()/1000.);
	    //} else if (m_subregion.find("mujets", 0) != string::npos) {
	    //    m_mu_klfitter = new TLorentzVector(tlv_leadlep.Px()/1000., tlv_leadlep.Py()/1000., tlv_leadlep.Pz()/1000., tlv_leadlep.E()/1000.);
	    //}
	    //m_ph_klfitter = new TLorentzVector(tlv_leadph.Px()/1000., tlv_leadph.Py()/1000., tlv_leadph.Pz()/1000., tlv_leadph.E()/1000.);
	}
    } else if (m_type == "Data" || m_type == "Reco" || m_type == "QCD") {
	if (m_debug) m_lg->Info("s", "Not upgrade");
    
        evtnum = eventNumber;
    
	if (m_debug) m_lg->Info("s", "el_pt");
        n_el = el_pt->size();
	if (m_debug) m_lg->Info("s", "mu_pt");
        n_mu = mu_pt->size();

	if (m_debug) m_lg->Info("s", "ph_pt");
        n_ph = ph_pt->size();	
	if (m_debug) m_lg->Info("s", "Initialize photon");
	for (int i = 0; i < n_ph; i++) {
	    if (ph_pt->at(i) >= 20000. && fabs(ph_eta->at(i)) < 2.37 && (fabs(ph_eta->at(i)) < 1.37 || fabs(ph_eta->at(i)) > 1.52) && ph_isoFCT->at(i)) {
	        n_ph_iso_index.push_back(i);
	    }
	    if (ph_pt->at(i) >= 20000. && fabs(ph_eta->at(i)) < 2.37 && (fabs(ph_eta->at(i)) < 1.37 || fabs(ph_eta->at(i)) > 1.52)) {
	        n_ph_noid_noiso_index.push_back(i);
	    }
	    if (ph_isTight->at(i) && ph_pt->at(i) >= 20000. && fabs(ph_eta->at(i)) < 2.37 && (fabs(ph_eta->at(i)) < 1.37 || fabs(ph_eta->at(i)) > 1.52) && ph_isoFCT->at(i)) {
	        n_ph_good_index.push_back(i);
	    }
	    if (ph_pt->at(i) >= 20000. && fabs(ph_eta->at(i)) < 2.37 && (fabs(ph_eta->at(i)) < 1.37 || fabs(ph_eta->at(i)) > 1.52) && 
		( ph_isHadronFakeFailedDeltaE->at(i) && ph_isHadronFakeFailedERatio->at(i) )
		||( ph_isHadronFakeFailedDeltaE->at(i) && ph_isHadronFakeFailedFside->at(i) )
		||( ph_isHadronFakeFailedDeltaE->at(i) && ph_isHadronFakeFailedWs3->at(i) )
		||( ph_isHadronFakeFailedERatio->at(i) && ph_isHadronFakeFailedFside->at(i) )
		||( ph_isHadronFakeFailedERatio->at(i) && ph_isHadronFakeFailedWs3->at(i) )
		||( ph_isHadronFakeFailedFside->at(i)  && ph_isHadronFakeFailedWs3->at(i) )
		) {
	        n_ph_hfake_index.push_back(i);
	    }
	}
	n_ph_good = n_ph_good_index.size();
	n_ph_iso = n_ph_iso_index.size();
	n_ph_noid_noiso = n_ph_noid_noiso_index.size();
	n_ph_hfake = n_ph_hfake_index.size();

	event_ngoodphotons = n_ph_good;
	if (n_ph_good >= 1) selph_index1 = n_ph_good_index.at(0);
	else selph_index1 = -1;
	if (m_selection == "CutSRNoIDNoIso") {
	    event_ngoodphotons = n_ph_noid_noiso;
	    if (n_ph_noid_noiso >= 1) selph_index1 = n_ph_noid_noiso_index.at(0);
	    else selph_index1 = -1;
	}
	if (m_selection == "CutSRNoID" || m_selection == "CutSRNoIDCvt" || m_selection == "CutSRNoIDUnCvt") {
	    event_ngoodphotons = n_ph_iso;
	    if (n_ph_iso >= 1) selph_index1 = n_ph_iso_index.at(0);
	    else selph_index1 = -1;
	}
	if (m_selection == "CutCRHFake" || m_selection == "CutCRHFakeCvt" || m_selection == "CutCRHFakeUnCvt") {
	    event_ngoodphotons = n_ph_hfake;
	    if (n_ph_hfake >= 1) selph_index1 = n_ph_hfake_index.at(0);
	    else selph_index1 = -1;
	}

	if (m_debug) m_lg->Info("s", "Initialize jet");
        n_jet = jet_pt->size();
	n_bjet = event_nbjets77;
	n_bjet77 = event_nbjets77;
	n_bjet70 = event_nbjets70;
        for (int i = 0; i < n_jet; i++) {
	    TLorentzVector tmp_jet;
	    tmp_jet.SetPtEtaPhiE(jet_pt->at(i), jet_eta->at(i), jet_phi->at(i), jet_e->at(i));
	    if (fabs(jet_eta->at(i)) > 2.5) {
		m_lg->Err("s", "Found jet with eta > 2.5!");
		exit(-1);
	    }
	    tlv_jets.push_back(tmp_jet);
	    if (i == 0) {
		leadjet_pt = jet_pt->at(i);
		leadjet_phi = jet_phi->at(i);
		leadjet_eta = jet_eta->at(i);
		if (jet_eta->at(i) > 0) leadjet_asm = 0.5;
		else leadjet_asm = -0.5;
		tlv_leadjet.SetPtEtaPhiE(jet_pt->at(i), jet_eta->at(i), jet_phi->at(i), jet_e->at(i));
	    }
	    if (i == 1) {
		subjet_pt = jet_pt->at(i);
		subjet_eta = jet_eta->at(i);
	    }

	    if (m_do_klfitter) {
		//TLorentzVector * vJet = new TLorentzVector(tmp_jet.Px()/1000., tmp_jet.Py()/1000., tmp_jet.Pz()/1000., tmp_jet.E()/1000.);
		//m_vjet_klfitter.push_back(vJet);
	    }
        }
        n_lep = n_el + n_mu;

	double pt_12jet = 0;
	double j1_id = -1;
	double j2_id = -1;
	if (tlv_jets.size() >= 2) {
	    for (int i = 0; i < tlv_jets.size(); i++) {
		TLorentzVector j1 = tlv_jets.at(i);
	        for (int j = i+1; j < tlv_jets.size(); j++) {
		    TLorentzVector j2 = tlv_jets.at(j);
		    double pt_12jet_tmp = (j1+j2).Pt();
		    if (pt_12jet_tmp > pt_12jet) {
			pt_12jet = pt_12jet_tmp;
			j1_id = i;
			j2_id = j;
		    }
	        }
	    }
	}
	if (j1_id != -1 && j2_id != -1) {
	    m_12jet = (tlv_jets.at(j1_id) + tlv_jets.at(j2_id)).M();
	}

	//double pt_123jet = 0;
	//j1_id = -1;
	//j2_id = -1;
	//double j3_id = -1;
	//if (tlv_jets.size() >= 3) {
	//    for (int i = 0; i < tlv_jets.size(); i++) {
	//	TLorentzVector j1 = tlv_jets.at(i);
	//        for (int j = i+1; j < tlv_jets.size(); j++) {
	//	    TLoretzVector j2 = tlv_jets.at(j);
	//	    double pt_12jet_tmp = (j1+j2).Pt();
	//	    if (pt_12jet_tmp > pt_12jet) {
	//		pt_12jet = pt_12jet_tmp;
	//		j1_id = i;
	//		j2_id = j;
	//	    }
	//        }
	//    }
	//}
	//if (j1_id != -1 && j2_id != -1) {
	//    m_12jet = (tlv_jets.at(j1_id) + tlv_jets.at(j2_id)).M();
	//}
    
	if (m_debug) m_lg->Info("s", "Filling photon");
        if (selph_index1 >= 0) {
	    leadph_pt = ph_pt->at(selph_index1);
	    leadph_cvt = ph_conversionType->at(selph_index1);
	    leadph_bincvt = (leadph_cvt == 0) ? 0 : 1;
	    if (m_debug) m_lg->Info("s", "Filling photon 1");
	    if (ph_HFT_MVA!=NULL) leadph_HFT_MVA = ph_HFT_MVA->at(selph_index1);
	    if (m_type != "Data" && m_type != "QCD") {
		leadph_mc_pt = ph_mc_pt->at(selph_index1);
	    	leadph_mc_eta = ph_mc_eta->at(selph_index1);
	    	leadph_mc_phi = ph_mc_phi->at(selph_index1);
	    	leadph_mc_pid = ph_mc_pid->at(selph_index1);
	    	leadph_mc_barcode = ph_mc_barcode->at(selph_index1);
	    	leadph_mc_el_pt = ph_mcel_pt->at(selph_index1);
	    	leadph_mc_el_dr = ph_mcel_dr->at(selph_index1);
	    	leadph_mc_el_eta = ph_mcel_eta->at(selph_index1);
	    	leadph_mc_el_phi = ph_mcel_phi->at(selph_index1);
	    	if (m_phmatch_type == "EFake") {
	    	    leadph_ptreco_pttrue = leadph_pt/leadph_mc_el_pt;
	    	} else {
	    	    leadph_ptreco_pttrue = leadph_pt/leadph_mc_pt;
	    	}
	    }
	    if (m_debug) m_lg->Info("s", "Filling photon 2");
    	    leadph_eta = ph_eta->at(selph_index1);
    	    leadph_phi = ph_phi->at(selph_index1);
    	    leadph_e = ph_e->at(selph_index1);
	    tlv_leadph.SetPtEtaPhiE(leadph_pt, leadph_eta, leadph_phi, leadph_e);
	    leadph_m = tlv_leadph.M();
	    if (m_debug) m_lg->Info("s", "Filling photon 3");
	    if (m_type != "Data" && m_type != "QCD") {
		tlv_leadph_mc.SetPtEtaPhiM(leadph_mc_pt, leadph_mc_eta, leadph_mc_phi, 0);
	    	leadph_mc_e = tlv_leadph_mc.E();
	    	leadph_mc_dr = tlv_leadph_mc.DeltaR(tlv_leadph);
	    	tlv_leadph_mc_el.SetPtEtaPhiM(leadph_mc_el_pt, leadph_mc_el_eta, leadph_mc_el_phi, 0);
	    	if (m_phmatch_type == "EFake") {
	    	    leadph_drrecotrue = tlv_leadph.DeltaR(tlv_leadph_mc_el);
	    	} else {
	    	    leadph_drrecotrue = tlv_leadph.DeltaR(tlv_leadph_mc);
	    	}
		//cout << leadph_drrecotrue << " " << leadph_ptreco_pttrue << endl;
	    }
    	    leadph_ptcone = ph_ptcone20->at(selph_index1);
    	    leadph_etcone = ph_topoetcone20->at(selph_index1);
    	    leadph_tight = ph_isTight->at(selph_index1);
    	    leadph_ancestor = ph_truthAncestor->at(selph_index1);
	    if (m_debug) m_lg->Info("s", "Filling photon 4");
	    if (leadph_ancestor > 100) leadph_ancestor = 100;
    	    leadph_origin = ph_truthOrigin->at(selph_index1);
    	    leadph_type = ph_truthType->at(selph_index1);
	    leadph_rhad1 =	ph_rhad1->at(selph_index1);
    	    leadph_rhad =	ph_rhad->at(selph_index1);
    	    leadph_reta =	ph_reta->at(selph_index1);
    	    leadph_weta2 =	ph_weta2->at(selph_index1);
    	    leadph_rphi =	ph_rphi->at(selph_index1);
    	    leadph_ws3 =	ph_ws3->at(selph_index1);
    	    leadph_wstot =	ph_wstot->at(selph_index1);
    	    leadph_fracm =	ph_fracm->at(selph_index1);
    	    leadph_deltaE =	ph_deltaE->at(selph_index1);
	    if (m_debug) m_lg->Info("s", "Filling photon 5");
    	    leadph_eratio =	ph_eratio->at(selph_index1);
    	    leadph_emaxs1 =	ph_emaxs1->at(selph_index1);
    	    leadph_f1 =		ph_f1->at(selph_index1);
    	    leadph_e277 =	ph_e277->at(selph_index1);

	    if (m_cali_egamma) {
		double tmppt = leadph_pt/1000.;
		if (tmppt >= 75.) tmppt = 73;
		int bin = h_egamma_alpha->FindBin(tmppt);
		double alpha = h_egamma_alpha->GetBinContent(bin);
		leadph_pt *= (1 - alpha);
	    }
        }
    
	if (m_debug) m_lg->Info("s", "Filling lepton");
        if (ejets_2015 || ejets_2016 || zeg_2015 || zeg_2016 || ee_2015 || ee_2016 || zee_2015 || zee_2016 || ttel_ee_2015 || ttel_ee_2016 || eeSS_2015 || eeSS_2016) {
	    leadlep_pt  = el_pt->at(0);
	    leadmu_pt = 0;
	    leadel_pt  = el_pt->at(0);
	    leadlep_eta = el_eta->at(0);
	    leadlep_phi = el_phi->at(0);
	    leadlep_e   = el_e->at(0);
	    tlv_leadlep.SetPtEtaPhiE(leadlep_pt, leadlep_eta, leadlep_phi, leadlep_e);
	    leadlep_m = tlv_leadlep.M();
	    if (m_debug) m_lg->Info("s", "Filling lepton truth");
	    if (m_type != "Data" && m_type != "QCD") {
	    if (m_region != "EF1") {
		leadlep_mc_pt = el_mc_pt->at(0);
	    	leadlep_mc_eta = el_mc_eta->at(0);
	    	leadlep_mc_phi = el_mc_phi->at(0);
	    	tlv_leadlep_mc.SetPtEtaPhiM(leadlep_mc_pt, leadlep_mc_eta, leadlep_mc_phi, 0);
	    	leadlep_mc_e = tlv_leadlep_mc.E();
	    	leadlep_mc_dr = tlv_leadlep_mc.DeltaR(tlv_leadlep);
		leadlep_origin = el_true_origin->at(0);
		leadlep_type = el_true_type->at(0);
	    }
	    }
	    if (m_debug) m_lg->Info("s", "Filling lepton trigger");
	    if (zeg_2015 || zee_2015 || ttel_ee_2015) {
		if (m_debug) m_lg->Info("s", "Filling 2015 lep trigger1");
	        leadlep_HLT_e24_lhmedium_L1EM20VH = el_trigMatch_HLT_e24_lhmedium_L1EM20VH->at(0);
		if (m_debug) m_lg->Info("s", "Filling 2015 lep trigger2");
	        leadlep_HLT_e60_lhmedium = el_trigMatch_HLT_e60_lhmedium->at(0);
		if (m_debug) m_lg->Info("s", "Filling 2015 lep trigger3");
	        leadlep_HLT_e120_lhloose = el_trigMatch_HLT_e120_lhloose->at(0);
	    } else if (zeg_2016 || zee_2016 || ttel_ee_2016) {
		if (m_debug) m_lg->Info("s", "Filling 2016 lep trigger1");
	        leadlep_HLT_e26_lhtight_nod0_ivarloose = el_trigMatch_HLT_e26_lhtight_nod0_ivarloose->at(0);
		if (m_debug) m_lg->Info("s", "Filling 2016 lep trigger2");
	        leadlep_HLT_e60_lhmedium_nod0 = el_trigMatch_HLT_e60_lhmedium_nod0->at(0);
		if (m_debug) m_lg->Info("s", "Filling 2016 lep trigger3");
	        leadlep_HLT_e140_lhloose_nod0 = el_trigMatch_HLT_e140_lhloose_nod0->at(0);
	    }
	    if (ee_2015 || ee_2016 || zee_2015 || zee_2016 || ttel_ee_2015 || ttel_ee_2016 || eeSS_2015 || eeSS_2016) {
		if (m_debug) m_lg->Info("s", "Filling second lepton");
	        sublep_pt  = el_pt->at(1);
	        sublep_eta = el_eta->at(1);
	        sublep_phi = el_phi->at(1);
	        sublep_e   = el_e->at(1);
	        tlv_sublep.SetPtEtaPhiE(sublep_pt, sublep_eta, sublep_phi, sublep_e);
	        sublep_m = tlv_sublep.M();
		if (m_type != "Data" && m_type != "QCD" && m_region != "EF1") {
		sublep_mc_pt = el_mc_pt->at(1);
	    	sublep_mc_eta = el_mc_eta->at(1);
	    	sublep_mc_phi = el_mc_phi->at(1);
		tlv_sublep_mc.SetPtEtaPhiM(sublep_mc_pt, sublep_mc_eta, sublep_mc_phi, 0);
		sublep_mc_e = tlv_sublep_mc.E();
		sublep_mc_dr = tlv_sublep_mc.DeltaR(tlv_sublep);
		sublep_origin = el_true_origin->at(1);
    	    	sublep_type = el_true_type->at(1);
		}
		sublep_ptcone = el_ptvarcone20->at(1);
    	    	sublep_etcone = el_topoetcone20->at(1);
	        if (zee_2015 || ttel_ee_2015 || eeSS_2015) {
	            sublep_HLT_e24_lhmedium_L1EM20VH = el_trigMatch_HLT_e24_lhmedium_L1EM20VH->at(1);
	            sublep_HLT_e60_lhmedium = el_trigMatch_HLT_e60_lhmedium->at(1);
	            sublep_HLT_e120_lhloose = el_trigMatch_HLT_e120_lhloose->at(1);
	        } else if (zee_2016 || ttel_ee_2016 || eeSS_2016) {
	            sublep_HLT_e26_lhtight_nod0_ivarloose = el_trigMatch_HLT_e26_lhtight_nod0_ivarloose->at(1);
	            sublep_HLT_e60_lhmedium_nod0 = el_trigMatch_HLT_e60_lhmedium_nod0->at(1);
	            sublep_HLT_e140_lhloose_nod0 = el_trigMatch_HLT_e140_lhloose_nod0->at(1);
	        }
	    }
        } else if (mujets_2015 || mujets_2016 || mumu_2015 || mumu_2016 || mumuSS_2015 || mumuSS_2016) {
	    if (m_debug) m_lg->Info("s", "Filling mu leptons");
	    leadlep_pt  = mu_pt->at(0);
	    leadel_pt = 0;
	    leadmu_pt  = mu_pt->at(0);
	    leadlep_eta = mu_eta->at(0);
	    leadlep_phi = mu_phi->at(0);
	    leadlep_e   = mu_e->at(0);
	    tlv_leadlep.SetPtEtaPhiE(leadlep_pt, leadlep_eta, leadlep_phi, leadlep_e);
	    leadlep_m = tlv_leadlep.M();
	    if (m_type != "Data" && m_type != "QCD") {
		leadlep_origin = mu_true_origin->at(0);
		leadlep_type = mu_true_type->at(0);
	    }
	    if (mumu_2015 || mumu_2016 || mumuSS_2015 || mumuSS_2016) {
	        sublep_pt  = mu_pt->at(1);
	        sublep_eta = mu_eta->at(1);
	        sublep_phi = mu_phi->at(1);
	        sublep_e   = mu_e->at(1);
	        tlv_sublep.SetPtEtaPhiE(sublep_pt, sublep_eta, sublep_phi, sublep_e);
	        sublep_m = tlv_sublep.M();
		if (m_type != "Data" && m_type != "QCD") {
		sublep_origin = mu_true_origin->at(1);
		sublep_type = mu_true_type->at(1);
		}
		sublep_ptcone = mu_ptvarcone30->at(1);
    	    	sublep_etcone = mu_topoetcone20->at(1);
	    }
        } else if (emu_2015 || emu_2016 || ttel_mue_2015 || ttel_mue_2016 || emuSS_2015 || emuSS_2016) {
	    if (m_debug) m_lg->Info("s", "Filling emu leptons");
	    if (n_mu > 0 && n_el > 0) {
		leadel_pt = el_pt->at(0);
		leadmu_pt = mu_pt->at(0);
    	        if (el_pt->at(0) > mu_pt->at(0)) {
		    leadlep_pt  = el_pt->at(0);
		    leadlep_eta = el_eta->at(0);
		    leadlep_phi = el_phi->at(0);
		    leadlep_e   = el_e->at(0);
		    tlv_leadlep.SetPtEtaPhiE(leadlep_pt, leadlep_eta, leadlep_phi, leadlep_e);
		    leadlep_m = tlv_leadlep.M();
		    if (m_type != "Data" && m_type != "QCD" && m_region != "EF1") {
		    leadlep_mc_pt = el_mc_pt->at(0);
	    	    leadlep_mc_eta = el_mc_eta->at(0);
	    	    leadlep_mc_phi = el_mc_phi->at(0);
		    tlv_leadlep_mc.SetPtEtaPhiM(leadlep_mc_pt, leadlep_mc_eta, leadlep_mc_phi, 0);
		    leadlep_mc_e = tlv_leadlep_mc.E();
		    leadlep_mc_dr = tlv_leadlep_mc.DeltaR(tlv_leadlep);
		    leadlep_origin = el_true_origin->at(0);
		    leadlep_type = el_true_type->at(0);
		    }
		    sublep_pt  = mu_pt->at(0);
		    sublep_eta = mu_eta->at(0);
		    sublep_phi = mu_phi->at(0);
		    sublep_e   = mu_e->at(0);
		    tlv_sublep.SetPtEtaPhiE(sublep_pt, sublep_eta, sublep_phi, sublep_e);
		    sublep_m = tlv_sublep.M();
		    if (m_type != "Data" && m_type != "QCD" && m_region != "EF1") {
		    sublep_origin = mu_true_origin->at(0);
		    sublep_type = mu_true_type->at(0);
		    }
		    sublep_ptcone = mu_ptvarcone30->at(0);
		    sublep_etcone = mu_topoetcone20->at(0);
    	        } else {
		    leadlep_pt  = mu_pt->at(0);
            	    leadlep_eta = mu_eta->at(0);
            	    leadlep_phi = mu_phi->at(0);
            	    leadlep_e   = mu_e->at(0);
		    tlv_leadlep.SetPtEtaPhiE(leadlep_pt, leadlep_eta, leadlep_phi, leadlep_e);
		    leadlep_m = tlv_leadlep.M();
		    if (m_type != "Data" && m_type != "QCD" && m_region != "EF1") {
		    leadlep_origin = mu_true_origin->at(0);
		    leadlep_type = mu_true_type->at(0);
		    }
		    sublep_pt  = el_pt->at(0);
            	    sublep_eta = el_eta->at(0);
            	    sublep_phi = el_phi->at(0);
            	    sublep_e   = el_e->at(0);
		    tlv_sublep.SetPtEtaPhiE(sublep_pt, sublep_eta, sublep_phi, sublep_e);
		    sublep_m = tlv_sublep.M();
		    if (m_type != "Data" && m_type != "QCD" && m_region != "EF1") {
		    sublep_mc_pt = el_mc_pt->at(0);
	    	    sublep_mc_eta = el_mc_eta->at(0);
	    	    sublep_mc_phi = el_mc_phi->at(0);
		    tlv_sublep_mc.SetPtEtaPhiM(sublep_mc_pt, sublep_mc_eta, sublep_mc_phi, 0);
		    sublep_mc_e = tlv_sublep_mc.E();
		    sublep_mc_dr = tlv_sublep_mc.DeltaR(tlv_sublep);
		    sublep_origin = el_true_origin->at(0);
		    sublep_type = el_true_type->at(0);
		    }
		    sublep_ptcone = el_ptvarcone20->at(0);
		    sublep_etcone = el_topoetcone20->at(0);
    	        }
    	    }
        }

	if (m_debug) m_lg->Info("s", "Filling MET");
	TLorentzVector met_tlv;
	met_tlv.SetPtEtaPhiM(met_met, met_phi, 0, 0);
	met_x = met_tlv.Px();
	met_y = met_tlv.Py();

	if (m_do_klfitter) {
	    //if (m_subregion == "ejets") {
	    //    m_el_klfitter = new TLorentzVector(tlv_leadlep.Px()/1000., tlv_leadlep.Py()/1000., tlv_leadlep.Pz()/1000., tlv_leadlep.E()/1000.);
	    //} else if (m_subregion == "mujets") {
	    //    m_mu_klfitter = new TLorentzVector(tlv_leadlep.Px()/1000., tlv_leadlep.Py()/1000., tlv_leadlep.Pz()/1000., tlv_leadlep.E()/1000.);
	    //}
	    //m_ph_klfitter = new TLorentzVector(tlv_leadph.Px()/1000., tlv_leadph.Py()/1000., tlv_leadph.Pz()/1000., tlv_leadph.E()/1000.);
	}
    
        met = met_met;
	dphilepmet = fabs(met_phi - leadlep_phi);
	dphiphmet = fabs(met_phi - leadph_phi);
	mwt = event_mwt;
        mwt2 = sqrt(2. * leadjet_pt * met * (1. - cos(leadjet_phi - met_phi)));
        //mwt = sqrt(2. * leadlep_pt * met * (1. - cos(leadlep_phi - met_phi)));
	TLorentzVector tlv_phll = tlv_leadlep+tlv_leadph;
	double mt_lphmetl = sqrt(2. * tlv_phll.Pt() * met * (1. - cos(tlv_phll.Phi() - met_phi)));
	TLorentzVector tlv_phls = tlv_sublep+tlv_leadph;
	double mt_lphmets = sqrt(2. * tlv_phls.Pt() * met * (1. - cos(tlv_phls.Phi() - met_phi)));
	mt_lphmet = mt_lphmetl > mt_lphmets ? mt_lphmetl : mt_lphmets;
    }

    if (m_debug) m_lg->Info("s", "Initialize others");
    mphl = (tlv_leadlep + tlv_leadph).M();
    mljet = (tlv_leadlep + tlv_leadjet).M();
    mmcphl = (tlv_leadlep + tlv_leadph_mc).M();
    mmcelphl = (tlv_leadlep + tlv_leadph_mc_el).M();
    mmcll = (tlv_leadlep + tlv_sublep_mc).M();
    mlmcl = (tlv_leadlep_mc + tlv_sublep).M();
    mmclmcl = (tlv_leadlep_mc + tlv_sublep_mc).M();
    mll = (tlv_leadlep + tlv_sublep).M();
    mllg = (tlv_leadlep + tlv_sublep + tlv_leadph).M();

    if (leadph_ptcone >= 20000. && leadph_ptcone < 1000000.) leadph_ptcone = 15000.;

    for (int i = 0; i < tlv_jets.size(); i++) {
	float tmp_drphj = tlv_jets.at(i).DeltaR(tlv_leadph);
	if (leadph_mindrphj > tmp_drphj) leadph_mindrphj = tmp_drphj;
    }
    for (int i = 0; i < tlv_jets.size(); i++) {
	float tmp_drphj = tlv_jets.at(i).DeltaR(tlv_sublep);
	if (sublep_mindrphj > tmp_drphj) sublep_mindrphj = tmp_drphj;
    }
    for (int i = 0; i < tlv_jets.size(); i++) {
	float tmp_drphj = tlv_jets.at(i).DeltaR(tlv_leadlep);
	if (leadlep_mindrphj > tmp_drphj) leadlep_mindrphj = tmp_drphj;
    }

    if (n_lep >= 1) leadphleadlep_dphi = tlv_leadlep.DeltaPhi(tlv_leadph);
    if (n_lep >= 2) {
	leadlepsublep_dphi = fabs(tlv_leadlep.DeltaPhi(tlv_sublep));
	leadlepsublep_deta = fabs(tlv_leadlep.Eta() - tlv_sublep.Eta());
	sublepleadlep_dphi = tlv_leadlep.DeltaPhi(tlv_sublep);
	drleplep = tlv_leadlep.DeltaR(tlv_sublep);
    }

    if (n_lep == 1) {
	leadph_mindrphl = tlv_leadlep.DeltaR(tlv_leadph);
    } else if (n_lep == 2) {
	leadph_mindrphl = leadph_mindrphl > tlv_leadlep.DeltaR(tlv_leadph) ? tlv_leadlep.DeltaR(tlv_leadph) : leadph_mindrphl;
	leadph_mindrphl = leadph_mindrphl > tlv_sublep.DeltaR(tlv_leadph) ? tlv_sublep.DeltaR(tlv_leadph) : leadph_mindrphl;
	sublep_mindrphl = tlv_sublep.DeltaR(tlv_leadlep);
	leadlep_mindrphl = tlv_leadlep.DeltaR(tlv_sublep);
	detaleplep = fabs(tlv_leadlep.Eta() - tlv_sublep.Eta());
	dphileplep = fabs(tlv_leadlep.DeltaPhi(tlv_sublep));
    }
    drleadjet_leadlep = tlv_leadjet.DeltaR(tlv_leadlep);
    drleadjet_leadph = tlv_leadjet.DeltaR(tlv_leadph);

    if (m_debug) m_lg->Info("s", "Initialize weight");
    if (!m_use_weight) weight = 1.0;
    else {
	if (m_type == "Upgrade") {
	    weight = weight_mc;
	    if (m_use_lumi_weight) {
		weight *= m_lumi_weight;
	    }
	    if (m_reweight_hfake) {
		int ibin = h_hfake_weight->FindBin(leadph_pt/1000.);
		if (ibin > h_hfake_weight->GetNbinsX()) ibin = h_hfake_weight->GetNbinsX();
		
		weight *= h_hfake_weight->GetBinContent(ibin);
	    }
	    if (m_reweight_hfake_025) {
		int ibin = h_hfake_weight->FindBin(leadph_pt/1000.);
		if (ibin > h_hfake_weight->GetNbinsX()) ibin = h_hfake_weight->GetNbinsX();
		
		weight *= 1 - (1 - h_hfake_weight->GetBinContent(ibin))*0.25;
	    }
	    if (m_reweight_hfake_05) {
		int ibin = h_hfake_weight->FindBin(leadph_pt/1000.);
		if (ibin > h_hfake_weight->GetNbinsX()) ibin = h_hfake_weight->GetNbinsX();
		
		weight *= 1 - (1 - h_hfake_weight->GetBinContent(ibin))*0.5;
	    }
	    if (m_reweight_hfake_075) {
		int ibin = h_hfake_weight->FindBin(leadph_pt/1000.);
		if (ibin > h_hfake_weight->GetNbinsX()) ibin = h_hfake_weight->GetNbinsX();
		
		weight *= 1 - (1 - h_hfake_weight->GetBinContent(ibin))*0.75;
	    }
	} else if (m_type == "Data") {
	    weight = 1.;
	    if (m_reweight_egamma) {
		int ibin = h_egamma_weight->FindBin(mphl/1000.);
		if (ibin < 1 || ibin > h_egamma_weight->GetNbinsX()) weight = 0;
		else weight *= h_egamma_weight->GetBinContent(ibin);
	    }
	    if (m_do_bootstrap) { 
		for (int i = 0; i < 1000; i++) {
		   weights_bootstrap.push_back(weight_poisson->at(i));
		}
	    }
	} else if (m_type == "QCD") {
	    if (m_QCD_1para) {
	    	int weightid;
	    	if (!m_PSQCD) weightid = m_QCD_para_idx;
	    	else weightid = n_QCD_para + m_QCD_para_idx;
		if (m_subregion == "ejets") weight = weights_mm_ejets->at(weightid);
	    	else if (m_subregion == "mujets") weight = weights_mm_mujets->at(weightid);
	    } else {
		for (int i = 0; i < n_QCD_para; i++) {
	    	    int weightid;
	    	    if (!m_PSQCD) weightid = i;
	    	    else weightid = n_QCD_para + i;
		    if (m_subregion == "ejets") weights_QCD.push_back(weights_mm_ejets->at(weightid));
	    	    else if (m_subregion == "mujets") weights_QCD.push_back(weights_mm_mujets->at(weightid));
		}
	    }
	    if (m_reweight_zgamma) {
		int ibin = h_zgamma_weight->FindBin(leadph_pt/1000.);
		if (ibin > h_zgamma_weight->GetNbinsX()) ibin = h_zgamma_weight->GetNbinsX();
		
		weight *= h_zgamma_weight->GetBinContent(ibin);
	    }
	} else if (m_type == "Reco") {

	    double weight_pu = weight_pileup;
	    double weight_lSF = weight_leptonSF;
	    double weight_JVT = weight_jvt;
	    double weight_ph_eff = 1; if (selph_index1 >= 0) weight_ph_eff == ph_SF_eff->at(selph_index1);
	    double weight_ph_iso = 1; if (selph_index1 >= 0) weight_ph_iso == ph_SF_iso->at(selph_index1);
	    double weight_MC = weight_mc;
	    if (m_cont_btag) weight_btag = weight_bTagSF_Continuous;
	    else weight_btag = weight_bTagSF_77;

	    if (m_treename == "nominal" && m_variation != "Nominal") {
		if (m_variation == "PUUp") weight_pu = weight_pileup_UP;
	    	else if (m_variation == "PUDn") weight_pu = weight_pileup_DOWN;
	    	else if (m_variation == "BTagCExtUp") weight_btag = weight_bTagSF_Continuous_extrapolation_from_charm_up;
	    	else if (m_variation == "BTagCExtDn") weight_btag = weight_bTagSF_Continuous_extrapolation_from_charm_down;
	    	else if (m_variation == "BTagB0Up") weight_btag = weight_bTagSF_Continuous_eigenvars_B_up->at(0);
	    	else if (m_variation == "BTagB0Dn") weight_btag = weight_bTagSF_Continuous_eigenvars_B_down->at(0);
	    	else if (m_variation == "BTagB1Up") weight_btag = weight_bTagSF_Continuous_eigenvars_B_up->at(1);
	    	else if (m_variation == "BTagB1Dn") weight_btag = weight_bTagSF_Continuous_eigenvars_B_down->at(1);
	    	else if (m_variation == "BTagB2Up") weight_btag = weight_bTagSF_Continuous_eigenvars_B_up->at(2);
	    	else if (m_variation == "BTagB2Dn") weight_btag = weight_bTagSF_Continuous_eigenvars_B_down->at(2);
	    	else if (m_variation == "BTagB3Up") weight_btag = weight_bTagSF_Continuous_eigenvars_B_up->at(3);
	    	else if (m_variation == "BTagB3Dn") weight_btag = weight_bTagSF_Continuous_eigenvars_B_down->at(3);
	    	else if (m_variation == "BTagB4Up") weight_btag = weight_bTagSF_Continuous_eigenvars_B_up->at(4);
	    	else if (m_variation == "BTagB4Dn") weight_btag = weight_bTagSF_Continuous_eigenvars_B_down->at(4);
	    	else if (m_variation == "BTagB5Up") weight_btag = weight_bTagSF_Continuous_eigenvars_B_up->at(5);
	    	else if (m_variation == "BTagB5Dn") weight_btag = weight_bTagSF_Continuous_eigenvars_B_down->at(5);
	    	else if (m_variation == "BTagB6Up") weight_btag = weight_bTagSF_Continuous_eigenvars_B_up->at(6);
	    	else if (m_variation == "BTagB6Dn") weight_btag = weight_bTagSF_Continuous_eigenvars_B_down->at(6);
	    	else if (m_variation == "BTagB7Up") weight_btag = weight_bTagSF_Continuous_eigenvars_B_up->at(7);
	    	else if (m_variation == "BTagB7Dn") weight_btag = weight_bTagSF_Continuous_eigenvars_B_down->at(7);
	    	else if (m_variation == "BTagB8Up") weight_btag = weight_bTagSF_Continuous_eigenvars_B_up->at(8);
	    	else if (m_variation == "BTagB8Dn") weight_btag = weight_bTagSF_Continuous_eigenvars_B_down->at(8);
	    	else if (m_variation == "BTagB9Up") weight_btag = weight_bTagSF_Continuous_eigenvars_B_up->at(9);
	    	else if (m_variation == "BTagB9Dn") weight_btag = weight_bTagSF_Continuous_eigenvars_B_down->at(9);
	    	else if (m_variation == "BTagB10Up") weight_btag = weight_bTagSF_Continuous_eigenvars_B_up->at(10);
	    	else if (m_variation == "BTagB10Dn") weight_btag = weight_bTagSF_Continuous_eigenvars_B_down->at(10);
	    	else if (m_variation == "BTagB11Up") weight_btag = weight_bTagSF_Continuous_eigenvars_B_up->at(11);
	    	else if (m_variation == "BTagB11Dn") weight_btag = weight_bTagSF_Continuous_eigenvars_B_down->at(11);
	    	else if (m_variation == "BTagB12Up") weight_btag = weight_bTagSF_Continuous_eigenvars_B_up->at(12);
	    	else if (m_variation == "BTagB12Dn") weight_btag = weight_bTagSF_Continuous_eigenvars_B_down->at(12);
	    	else if (m_variation == "BTagB13Up") weight_btag = weight_bTagSF_Continuous_eigenvars_B_up->at(13);
	    	else if (m_variation == "BTagB13Dn") weight_btag = weight_bTagSF_Continuous_eigenvars_B_down->at(13);
	    	else if (m_variation == "BTagB14Up") weight_btag = weight_bTagSF_Continuous_eigenvars_B_up->at(14);
	    	else if (m_variation == "BTagB14Dn") weight_btag = weight_bTagSF_Continuous_eigenvars_B_down->at(14);
	    	else if (m_variation == "BTagB15Up") weight_btag = weight_bTagSF_Continuous_eigenvars_B_up->at(15);
	    	else if (m_variation == "BTagB15Dn") weight_btag = weight_bTagSF_Continuous_eigenvars_B_down->at(15);
	    	else if (m_variation == "BTagB16Up") weight_btag = weight_bTagSF_Continuous_eigenvars_B_up->at(16);
	    	else if (m_variation == "BTagB16Dn") weight_btag = weight_bTagSF_Continuous_eigenvars_B_down->at(16);
	    	else if (m_variation == "BTagB17Up") weight_btag = weight_bTagSF_Continuous_eigenvars_B_up->at(17);
	    	else if (m_variation == "BTagB17Dn") weight_btag = weight_bTagSF_Continuous_eigenvars_B_down->at(17);
	    	else if (m_variation == "BTagB18Up") weight_btag = weight_bTagSF_Continuous_eigenvars_B_up->at(18);
	    	else if (m_variation == "BTagB18Dn") weight_btag = weight_bTagSF_Continuous_eigenvars_B_down->at(18);
	    	else if (m_variation == "BTagB19Up") weight_btag = weight_bTagSF_Continuous_eigenvars_B_up->at(19);
	    	else if (m_variation == "BTagB19Dn") weight_btag = weight_bTagSF_Continuous_eigenvars_B_down->at(19);
	    	else if (m_variation == "BTagB20Up") weight_btag = weight_bTagSF_Continuous_eigenvars_B_up->at(20);
	    	else if (m_variation == "BTagB20Dn") weight_btag = weight_bTagSF_Continuous_eigenvars_B_down->at(20);
	    	else if (m_variation == "BTagB21Up") weight_btag = weight_bTagSF_Continuous_eigenvars_B_up->at(21);
	    	else if (m_variation == "BTagB21Dn") weight_btag = weight_bTagSF_Continuous_eigenvars_B_down->at(21);
	    	else if (m_variation == "BTagB22Up") weight_btag = weight_bTagSF_Continuous_eigenvars_B_up->at(22);
	    	else if (m_variation == "BTagB22Dn") weight_btag = weight_bTagSF_Continuous_eigenvars_B_down->at(22);
	    	else if (m_variation == "BTagB23Up") weight_btag = weight_bTagSF_Continuous_eigenvars_B_up->at(23);
	    	else if (m_variation == "BTagB23Dn") weight_btag = weight_bTagSF_Continuous_eigenvars_B_down->at(23);
	    	else if (m_variation == "BTagB24Up") weight_btag = weight_bTagSF_Continuous_eigenvars_B_up->at(24);
	    	else if (m_variation == "BTagB24Dn") weight_btag = weight_bTagSF_Continuous_eigenvars_B_down->at(24);
	    	else if (m_variation == "BTagB25Up") weight_btag = weight_bTagSF_Continuous_eigenvars_B_up->at(25);
	    	else if (m_variation == "BTagB25Dn") weight_btag = weight_bTagSF_Continuous_eigenvars_B_down->at(25);
	    	else if (m_variation == "BTagB26Up") weight_btag = weight_bTagSF_Continuous_eigenvars_B_up->at(26);
	    	else if (m_variation == "BTagB26Dn") weight_btag = weight_bTagSF_Continuous_eigenvars_B_down->at(26);
	    	else if (m_variation == "BTagB27Up") weight_btag = weight_bTagSF_Continuous_eigenvars_B_up->at(27);
	    	else if (m_variation == "BTagB27Dn") weight_btag = weight_bTagSF_Continuous_eigenvars_B_down->at(27);
	    	else if (m_variation == "BTagB28Up") weight_btag = weight_bTagSF_Continuous_eigenvars_B_up->at(28);
	    	else if (m_variation == "BTagB28Dn") weight_btag = weight_bTagSF_Continuous_eigenvars_B_down->at(28);
	    	else if (m_variation == "BTagB29Up") weight_btag = weight_bTagSF_Continuous_eigenvars_B_up->at(29);
	    	else if (m_variation == "BTagB29Dn") weight_btag = weight_bTagSF_Continuous_eigenvars_B_down->at(29);
	    	else if (m_variation == "BTagC0Up") weight_btag = weight_bTagSF_Continuous_eigenvars_C_up->at(0);
	    	else if (m_variation == "BTagC0Dn") weight_btag = weight_bTagSF_Continuous_eigenvars_C_down->at(0);
	    	else if (m_variation == "BTagC1Up") weight_btag = weight_bTagSF_Continuous_eigenvars_C_up->at(1);
	    	else if (m_variation == "BTagC1Dn") weight_btag = weight_bTagSF_Continuous_eigenvars_C_down->at(1);
	    	else if (m_variation == "BTagC2Up") weight_btag = weight_bTagSF_Continuous_eigenvars_C_up->at(2);
	    	else if (m_variation == "BTagC2Dn") weight_btag = weight_bTagSF_Continuous_eigenvars_C_down->at(2);
	    	else if (m_variation == "BTagC3Up") weight_btag = weight_bTagSF_Continuous_eigenvars_C_up->at(3);
	    	else if (m_variation == "BTagC3Dn") weight_btag = weight_bTagSF_Continuous_eigenvars_C_down->at(3);
	    	else if (m_variation == "BTagC4Up") weight_btag = weight_bTagSF_Continuous_eigenvars_C_up->at(4);
	    	else if (m_variation == "BTagC4Dn") weight_btag = weight_bTagSF_Continuous_eigenvars_C_down->at(4);
	    	else if (m_variation == "BTagC5Up") weight_btag = weight_bTagSF_Continuous_eigenvars_C_up->at(5);
	    	else if (m_variation == "BTagC5Dn") weight_btag = weight_bTagSF_Continuous_eigenvars_C_down->at(5);
	    	else if (m_variation == "BTagC6Up") weight_btag = weight_bTagSF_Continuous_eigenvars_C_up->at(6);
	    	else if (m_variation == "BTagC6Dn") weight_btag = weight_bTagSF_Continuous_eigenvars_C_down->at(6);
	    	else if (m_variation == "BTagC7Up") weight_btag = weight_bTagSF_Continuous_eigenvars_C_up->at(7);
	    	else if (m_variation == "BTagC7Dn") weight_btag = weight_bTagSF_Continuous_eigenvars_C_down->at(7);
	    	else if (m_variation == "BTagC8Up") weight_btag = weight_bTagSF_Continuous_eigenvars_C_up->at(8);
	    	else if (m_variation == "BTagC8Dn") weight_btag = weight_bTagSF_Continuous_eigenvars_C_down->at(8);
	    	else if (m_variation == "BTagC9Up") weight_btag = weight_bTagSF_Continuous_eigenvars_C_up->at(9);
	    	else if (m_variation == "BTagC9Dn") weight_btag = weight_bTagSF_Continuous_eigenvars_C_down->at(9);
	    	else if (m_variation == "BTagC10Up") weight_btag = weight_bTagSF_Continuous_eigenvars_C_up->at(10);
	    	else if (m_variation == "BTagC10Dn") weight_btag = weight_bTagSF_Continuous_eigenvars_C_down->at(10);
	    	else if (m_variation == "BTagC11Up") weight_btag = weight_bTagSF_Continuous_eigenvars_C_up->at(11);
	    	else if (m_variation == "BTagC11Dn") weight_btag = weight_bTagSF_Continuous_eigenvars_C_down->at(11);
	    	else if (m_variation == "BTagC12Up") weight_btag = weight_bTagSF_Continuous_eigenvars_C_up->at(12);
	    	else if (m_variation == "BTagC12Dn") weight_btag = weight_bTagSF_Continuous_eigenvars_C_down->at(12);
	    	else if (m_variation == "BTagC13Up") weight_btag = weight_bTagSF_Continuous_eigenvars_C_up->at(13);
	    	else if (m_variation == "BTagC13Dn") weight_btag = weight_bTagSF_Continuous_eigenvars_C_down->at(13);
	    	else if (m_variation == "BTagC14Up") weight_btag = weight_bTagSF_Continuous_eigenvars_C_up->at(14);
	    	else if (m_variation == "BTagC14Dn") weight_btag = weight_bTagSF_Continuous_eigenvars_C_down->at(14);
	    	else if (m_variation == "BTagL0Up") weight_btag = weight_bTagSF_Continuous_eigenvars_Light_up->at(0);
	    	else if (m_variation == "BTagL0Dn") weight_btag = weight_bTagSF_Continuous_eigenvars_Light_down->at(0);
	    	else if (m_variation == "BTagL1Up") weight_btag = weight_bTagSF_Continuous_eigenvars_Light_up->at(1);
	    	else if (m_variation == "BTagL1Dn") weight_btag = weight_bTagSF_Continuous_eigenvars_Light_down->at(1);
	    	else if (m_variation == "BTagL2Up") weight_btag = weight_bTagSF_Continuous_eigenvars_Light_up->at(2);
	    	else if (m_variation == "BTagL2Dn") weight_btag = weight_bTagSF_Continuous_eigenvars_Light_down->at(2);
	    	else if (m_variation == "BTagL3Up") weight_btag = weight_bTagSF_Continuous_eigenvars_Light_up->at(3);
	    	else if (m_variation == "BTagL3Dn") weight_btag = weight_bTagSF_Continuous_eigenvars_Light_down->at(3);
	    	else if (m_variation == "BTagL4Up") weight_btag = weight_bTagSF_Continuous_eigenvars_Light_up->at(4);
	    	else if (m_variation == "BTagL4Dn") weight_btag = weight_bTagSF_Continuous_eigenvars_Light_down->at(4);
	    	else if (m_variation == "BTagL5Up") weight_btag = weight_bTagSF_Continuous_eigenvars_Light_up->at(5);
	    	else if (m_variation == "BTagL5Dn") weight_btag = weight_bTagSF_Continuous_eigenvars_Light_down->at(5);
	    	else if (m_variation == "BTagL6Up") weight_btag = weight_bTagSF_Continuous_eigenvars_Light_up->at(6);
	    	else if (m_variation == "BTagL6Dn") weight_btag = weight_bTagSF_Continuous_eigenvars_Light_down->at(6);
	    	else if (m_variation == "BTagL7Up") weight_btag = weight_bTagSF_Continuous_eigenvars_Light_up->at(7);
	    	else if (m_variation == "BTagL7Dn") weight_btag = weight_bTagSF_Continuous_eigenvars_Light_down->at(7);
	    	else if (m_variation == "BTagL8Up") weight_btag = weight_bTagSF_Continuous_eigenvars_Light_up->at(8);
	    	else if (m_variation == "BTagL8Dn") weight_btag = weight_bTagSF_Continuous_eigenvars_Light_down->at(8);
	    	else if (m_variation == "BTagL9Up") weight_btag = weight_bTagSF_Continuous_eigenvars_Light_up->at(9);
	    	else if (m_variation == "BTagL9Dn") weight_btag = weight_bTagSF_Continuous_eigenvars_Light_down->at(9);
	    	else if (m_variation == "BTagL10Up") weight_btag = weight_bTagSF_Continuous_eigenvars_Light_up->at(10);
	    	else if (m_variation == "BTagL10Dn") weight_btag = weight_bTagSF_Continuous_eigenvars_Light_down->at(10);
	    	else if (m_variation == "BTagL11Up") weight_btag = weight_bTagSF_Continuous_eigenvars_Light_up->at(11);
	    	else if (m_variation == "BTagL11Dn") weight_btag = weight_bTagSF_Continuous_eigenvars_Light_down->at(11);
	    	else if (m_variation == "BTagL12Up") weight_btag = weight_bTagSF_Continuous_eigenvars_Light_up->at(12);
	    	else if (m_variation == "BTagL12Dn") weight_btag = weight_bTagSF_Continuous_eigenvars_Light_down->at(12);
	    	else if (m_variation == "BTagL13Up") weight_btag = weight_bTagSF_Continuous_eigenvars_Light_up->at(13);
	    	else if (m_variation == "BTagL13Dn") weight_btag = weight_bTagSF_Continuous_eigenvars_Light_down->at(13);
	    	else if (m_variation == "BTagL14Up") weight_btag = weight_bTagSF_Continuous_eigenvars_Light_up->at(14);
	    	else if (m_variation == "BTagL14Dn") weight_btag = weight_bTagSF_Continuous_eigenvars_Light_down->at(14);
	    	else if (m_variation == "BTagL15Up") weight_btag = weight_bTagSF_Continuous_eigenvars_Light_up->at(15);
	    	else if (m_variation == "BTagL15Dn") weight_btag = weight_bTagSF_Continuous_eigenvars_Light_down->at(15);
	    	else if (m_variation == "BTagL16Up") weight_btag = weight_bTagSF_Continuous_eigenvars_Light_up->at(16);
	    	else if (m_variation == "BTagL16Dn") weight_btag = weight_bTagSF_Continuous_eigenvars_Light_down->at(16);
	    	else if (m_variation == "BTagL17Up") weight_btag = weight_bTagSF_Continuous_eigenvars_Light_up->at(17);
	    	else if (m_variation == "BTagL17Dn") weight_btag = weight_bTagSF_Continuous_eigenvars_Light_down->at(17);
	    	else if (m_variation == "BTagL18Up") weight_btag = weight_bTagSF_Continuous_eigenvars_Light_up->at(18);
	    	else if (m_variation == "BTagL18Dn") weight_btag = weight_bTagSF_Continuous_eigenvars_Light_down->at(18);
	    	else if (m_variation == "BTagL19Up") weight_btag = weight_bTagSF_Continuous_eigenvars_Light_up->at(19);
	    	else if (m_variation == "BTagL19Dn") weight_btag = weight_bTagSF_Continuous_eigenvars_Light_down->at(19);
	    	else if (m_variation == "BTagL20Up") weight_btag = weight_bTagSF_Continuous_eigenvars_Light_up->at(20);
	    	else if (m_variation == "BTagL20Dn") weight_btag = weight_bTagSF_Continuous_eigenvars_Light_down->at(20);
	    	else if (m_variation == "BTagL21Up") weight_btag = weight_bTagSF_Continuous_eigenvars_Light_up->at(21);
	    	else if (m_variation == "BTagL21Dn") weight_btag = weight_bTagSF_Continuous_eigenvars_Light_down->at(21);
	    	else if (m_variation == "BTagL22Up") weight_btag = weight_bTagSF_Continuous_eigenvars_Light_up->at(22);
	    	else if (m_variation == "BTagL22Dn") weight_btag = weight_bTagSF_Continuous_eigenvars_Light_down->at(22);
	    	else if (m_variation == "BTagL23Up") weight_btag = weight_bTagSF_Continuous_eigenvars_Light_up->at(23);
	    	else if (m_variation == "BTagL23Dn") weight_btag = weight_bTagSF_Continuous_eigenvars_Light_down->at(23);
	    	else if (m_variation == "BTagL24Up") weight_btag = weight_bTagSF_Continuous_eigenvars_Light_up->at(24);
	    	else if (m_variation == "BTagL24Dn") weight_btag = weight_bTagSF_Continuous_eigenvars_Light_down->at(24);
	    	else if (m_variation == "BTagL25Up") weight_btag = weight_bTagSF_Continuous_eigenvars_Light_up->at(25);
	    	else if (m_variation == "BTagL25Dn") weight_btag = weight_bTagSF_Continuous_eigenvars_Light_down->at(25);
	    	else if (m_variation == "BTagL26Up") weight_btag = weight_bTagSF_Continuous_eigenvars_Light_up->at(26);
	    	else if (m_variation == "BTagL26Dn") weight_btag = weight_bTagSF_Continuous_eigenvars_Light_down->at(26);
	    	else if (m_variation == "BTagL27Up") weight_btag = weight_bTagSF_Continuous_eigenvars_Light_up->at(27);
	    	else if (m_variation == "BTagL27Dn") weight_btag = weight_bTagSF_Continuous_eigenvars_Light_down->at(27);
	    	else if (m_variation == "BTagL28Up") weight_btag = weight_bTagSF_Continuous_eigenvars_Light_up->at(28);
	    	else if (m_variation == "BTagL28Dn") weight_btag = weight_bTagSF_Continuous_eigenvars_Light_down->at(28);
	    	else if (m_variation == "BTagL29Up") weight_btag = weight_bTagSF_Continuous_eigenvars_Light_up->at(29);
	    	else if (m_variation == "BTagL29Dn") weight_btag = weight_bTagSF_Continuous_eigenvars_Light_down->at(29);
	    	else if (m_variation == "EL_Trigger_Up") weight_lSF = weight_leptonSF_EL_SF_Trigger_UP;
	    	else if (m_variation == "EL_Trigger_Dn") weight_lSF = weight_leptonSF_EL_SF_Trigger_DOWN;
	    	else if (m_variation == "EL_Reco_Up") weight_lSF = weight_leptonSF_EL_SF_Reco_UP;
	    	else if (m_variation == "EL_Reco_Dn") weight_lSF = weight_leptonSF_EL_SF_Reco_DOWN;
	    	else if (m_variation == "EL_ID_Up") weight_lSF = weight_leptonSF_EL_SF_ID_UP;
	    	else if (m_variation == "EL_ID_Dn") weight_lSF = weight_leptonSF_EL_SF_ID_DOWN;
	    	else if (m_variation == "EL_Isol_Up") weight_lSF = weight_leptonSF_EL_SF_Isol_UP;
	    	else if (m_variation == "EL_Isol_Dn") weight_lSF = weight_leptonSF_EL_SF_Isol_DOWN;
	    	else if (m_variation == "MU_Trigger_STAT_Up") weight_lSF = weight_leptonSF_MU_SF_Trigger_STAT_UP;
	    	else if (m_variation == "MU_Trigger_STAT_Dn") weight_lSF = weight_leptonSF_MU_SF_Trigger_STAT_DOWN;
	    	else if (m_variation == "MU_Trigger_SYST_Up") weight_lSF = weight_leptonSF_MU_SF_Trigger_SYST_UP;
	    	else if (m_variation == "MU_Trigger_SYST_Dn") weight_lSF = weight_leptonSF_MU_SF_Trigger_SYST_DOWN;
	    	else if (m_variation == "MU_ID_STAT_Up") weight_lSF = weight_leptonSF_MU_SF_ID_STAT_UP;
	    	else if (m_variation == "MU_ID_STAT_Dn") weight_lSF = weight_leptonSF_MU_SF_ID_STAT_DOWN;
	    	else if (m_variation == "MU_ID_SYST_Up") weight_lSF = weight_leptonSF_MU_SF_ID_SYST_UP;
	    	else if (m_variation == "MU_ID_SYST_Dn") weight_lSF = weight_leptonSF_MU_SF_ID_SYST_DOWN;
	    	else if (m_variation == "MU_ID_STAT_LOWPT_Up") weight_lSF = weight_leptonSF_MU_SF_ID_STAT_LOWPT_UP;
	    	else if (m_variation == "MU_ID_STAT_LOWPT_Dn") weight_lSF = weight_leptonSF_MU_SF_ID_STAT_LOWPT_DOWN;
	    	else if (m_variation == "MU_ID_SYST_LOWPT_Up") weight_lSF = weight_leptonSF_MU_SF_ID_SYST_LOWPT_UP;
	    	else if (m_variation == "MU_ID_SYST_LOWPT_Dn") weight_lSF = weight_leptonSF_MU_SF_ID_SYST_LOWPT_DOWN;
	    	else if (m_variation == "MU_Isol_STAT_Up") weight_lSF = weight_leptonSF_MU_SF_Isol_STAT_UP;
	    	else if (m_variation == "MU_Isol_STAT_Dn") weight_lSF = weight_leptonSF_MU_SF_Isol_STAT_DOWN;
	    	else if (m_variation == "MU_Isol_SYST_Up") weight_lSF = weight_leptonSF_MU_SF_Isol_SYST_UP;
	    	else if (m_variation == "MU_Isol_SYST_Dn") weight_lSF = weight_leptonSF_MU_SF_Isol_SYST_DOWN;
	    	else if (m_variation == "MU_TTVA_STAT_Up") weight_lSF = weight_leptonSF_MU_SF_TTVA_STAT_UP;
	    	else if (m_variation == "MU_TTVA_STAT_Dn") weight_lSF = weight_leptonSF_MU_SF_TTVA_STAT_DOWN;
	    	else if (m_variation == "MU_TTVA_SYST_Up") weight_lSF = weight_leptonSF_MU_SF_TTVA_SYST_UP;
	    	else if (m_variation == "MU_TTVA_SYST_Dn") weight_lSF = weight_leptonSF_MU_SF_TTVA_SYST_DOWN;
	    	else if (m_variation == "JVT_Up") weight_JVT = weight_jvt_UP;
	    	else if (m_variation == "JVT_Dn") weight_JVT = weight_jvt_DOWN;
	    	else if (m_variation == "PH_ID_Up") {if (selph_index1 >= 0) weight_ph_eff = ph_SF_effUP->at(selph_index1);}
	    	else if (m_variation == "PH_ID_Dn") {if (selph_index1 >= 0) weight_ph_eff = ph_SF_effDO->at(selph_index1);}
	    	else if (m_variation == "PH_Isol_Up") {if (selph_index1 >= 0) weight_ph_iso = ph_SF_lowisoUP->at(selph_index1);}
	    	else if (m_variation == "PH_Isol_Dn") {if (selph_index1 >= 0) weight_ph_iso = ph_SF_lowisoDO->at(selph_index1);}
	    	else if (m_variation == "PH_TrkIsol_Up") {if (selph_index1 >= 0) weight_ph_iso = ph_SF_trkisoUP->at(selph_index1);}
	    	else if (m_variation == "PH_TrkIsol_Dn") {if (selph_index1 >= 0) weight_ph_iso = ph_SF_trkisoDO->at(selph_index1);}
		else if (m_variation == "R1F2") weight_MC = mc_generator_weights->at(77);
		else if (m_variation == "R2F1") weight_MC = mc_generator_weights->at(33);
		else if (m_variation == "R2F05") weight_MC = mc_generator_weights->at(66);
		else if (m_variation == "R05F2") weight_MC = mc_generator_weights->at(88);
		else if (m_variation == "R05F1") weight_MC = mc_generator_weights->at(22);
		else if (m_variation == "R05F05") weight_MC = mc_generator_weights->at(55);
		else if (m_variation == "R1F05") weight_MC = mc_generator_weights->at(44);
		else if (m_variation == "R2F2") weight_MC = mc_generator_weights->at(99);
		else if (m_variation == "PDF25") weight_MC = mc_generator_weights->at(39);
 		else if (m_variation == "PDF24") weight_MC = mc_generator_weights->at(38);
 		else if (m_variation == "PDF27") weight_MC = mc_generator_weights->at(41);
 		else if (m_variation == "PDF26") weight_MC = mc_generator_weights->at(40);
 		else if (m_variation == "PDF21") weight_MC = mc_generator_weights->at(35);
 		else if (m_variation == "PDF20") weight_MC = mc_generator_weights->at(34);
 		else if (m_variation == "PDF23") weight_MC = mc_generator_weights->at(37);
 		else if (m_variation == "PDF22") weight_MC = mc_generator_weights->at(36);
 		else if (m_variation == "PDF29") weight_MC = mc_generator_weights->at(43);
 		else if (m_variation == "PDF28") weight_MC = mc_generator_weights->at(42);
 		else if (m_variation == "PDF36") weight_MC = mc_generator_weights->at(51);
 		else if (m_variation == "PDF34") weight_MC = mc_generator_weights->at(49);
 		else if (m_variation == "PDF35") weight_MC = mc_generator_weights->at(50);
 		else if (m_variation == "PDF32") weight_MC = mc_generator_weights->at(47);
 		else if (m_variation == "PDF33") weight_MC = mc_generator_weights->at(48);
 		else if (m_variation == "PDF30") weight_MC = mc_generator_weights->at(45);
 		else if (m_variation == "PDF31") weight_MC = mc_generator_weights->at(46);
 		else if (m_variation == "PDF38") weight_MC = mc_generator_weights->at(53);
 		else if (m_variation == "PDF39") weight_MC = mc_generator_weights->at(54);
 		else if (m_variation == "PDF83") weight_MC = mc_generator_weights->at(103);
 		else if (m_variation == "PDF82") weight_MC = mc_generator_weights->at(102);
 		else if (m_variation == "PDF81") weight_MC = mc_generator_weights->at(101);
 		else if (m_variation == "PDF80") weight_MC = mc_generator_weights->at(100);
 		else if (m_variation == "PDF87") weight_MC = mc_generator_weights->at(107);
 		else if (m_variation == "PDF100") weight_MC = mc_generator_weights->at(13);
 		else if (m_variation == "PDF85") weight_MC = mc_generator_weights->at(105);
 		else if (m_variation == "PDF84") weight_MC = mc_generator_weights->at(104);
 		else if (m_variation == "PDF89") weight_MC = mc_generator_weights->at(109);
 		else if (m_variation == "PDF88") weight_MC = mc_generator_weights->at(108);
 		else if (m_variation == "PDF69") weight_MC = mc_generator_weights->at(87);
 		else if (m_variation == "PDF68") weight_MC = mc_generator_weights->at(86);
 		else if (m_variation == "PDF61") weight_MC = mc_generator_weights->at(79);
 		else if (m_variation == "PDF60") weight_MC = mc_generator_weights->at(78);
 		else if (m_variation == "PDF63") weight_MC = mc_generator_weights->at(81);
 		else if (m_variation == "PDF62") weight_MC = mc_generator_weights->at(80);
 		else if (m_variation == "PDF65") weight_MC = mc_generator_weights->at(83);
 		else if (m_variation == "PDF64") weight_MC = mc_generator_weights->at(82);
 		else if (m_variation == "PDF67") weight_MC = mc_generator_weights->at(85);
 		else if (m_variation == "PDF66") weight_MC = mc_generator_weights->at(84);
 		else if (m_variation == "PDF8") weight_MC = mc_generator_weights->at(20);
 		else if (m_variation == "PDF9") weight_MC = mc_generator_weights->at(21);
 		else if (m_variation == "PDF6") weight_MC = mc_generator_weights->at(18);
 		else if (m_variation == "PDF7") weight_MC = mc_generator_weights->at(19);
 		else if (m_variation == "PDF4") weight_MC = mc_generator_weights->at(16);
 		else if (m_variation == "PDF5") weight_MC = mc_generator_weights->at(17);
 		else if (m_variation == "PDF2") weight_MC = mc_generator_weights->at(14);
 		else if (m_variation == "PDF3") weight_MC = mc_generator_weights->at(15);
 		else if (m_variation == "PDF1") weight_MC = mc_generator_weights->at(12);
 		else if (m_variation == "PDF94") weight_MC = mc_generator_weights->at(6);
 		else if (m_variation == "PDF95") weight_MC = mc_generator_weights->at(7);
 		else if (m_variation == "PDF96") weight_MC = mc_generator_weights->at(8);
 		else if (m_variation == "PDF97") weight_MC = mc_generator_weights->at(9);
 		else if (m_variation == "PDF90") weight_MC = mc_generator_weights->at(2);
 		else if (m_variation == "PDF91") weight_MC = mc_generator_weights->at(3);
 		else if (m_variation == "PDF92") weight_MC = mc_generator_weights->at(4);
 		else if (m_variation == "PDF93") weight_MC = mc_generator_weights->at(5);
 		else if (m_variation == "PDF98") weight_MC = mc_generator_weights->at(10);
 		else if (m_variation == "PDF99") weight_MC = mc_generator_weights->at(11);
 		else if (m_variation == "PDF18") weight_MC = mc_generator_weights->at(31);
 		else if (m_variation == "PDF19") weight_MC = mc_generator_weights->at(32);
 		else if (m_variation == "PDF14") weight_MC = mc_generator_weights->at(27);
 		else if (m_variation == "PDF15") weight_MC = mc_generator_weights->at(28);
 		else if (m_variation == "PDF16") weight_MC = mc_generator_weights->at(29);
 		else if (m_variation == "PDF17") weight_MC = mc_generator_weights->at(30);
 		else if (m_variation == "PDF10") weight_MC = mc_generator_weights->at(23);
 		else if (m_variation == "PDF11") weight_MC = mc_generator_weights->at(24);
 		else if (m_variation == "PDF12") weight_MC = mc_generator_weights->at(25);
 		else if (m_variation == "PDF13") weight_MC = mc_generator_weights->at(26);
 		else if (m_variation == "PDF78") weight_MC = mc_generator_weights->at(97);
 		else if (m_variation == "PDF79") weight_MC = mc_generator_weights->at(98);
 		else if (m_variation == "PDF72") weight_MC = mc_generator_weights->at(91);
 		else if (m_variation == "PDF73") weight_MC = mc_generator_weights->at(92);
 		else if (m_variation == "PDF70") weight_MC = mc_generator_weights->at(89);
 		else if (m_variation == "PDF71") weight_MC = mc_generator_weights->at(90);
 		else if (m_variation == "PDF76") weight_MC = mc_generator_weights->at(95);
 		else if (m_variation == "PDF77") weight_MC = mc_generator_weights->at(96);
 		else if (m_variation == "PDF74") weight_MC = mc_generator_weights->at(93);
 		else if (m_variation == "PDF75") weight_MC = mc_generator_weights->at(94);
 		else if (m_variation == "PDF86") weight_MC = mc_generator_weights->at(106);
 		else if (m_variation == "PDF37") weight_MC = mc_generator_weights->at(52);
 		else if (m_variation == "PDF49") weight_MC = mc_generator_weights->at(65);
 		else if (m_variation == "PDF48") weight_MC = mc_generator_weights->at(64);
 		else if (m_variation == "PDF47") weight_MC = mc_generator_weights->at(63);
 		else if (m_variation == "PDF46") weight_MC = mc_generator_weights->at(62);
 		else if (m_variation == "PDF45") weight_MC = mc_generator_weights->at(61);
 		else if (m_variation == "PDF44") weight_MC = mc_generator_weights->at(60);
 		else if (m_variation == "PDF43") weight_MC = mc_generator_weights->at(59);
 		else if (m_variation == "PDF42") weight_MC = mc_generator_weights->at(58);
 		else if (m_variation == "PDF41") weight_MC = mc_generator_weights->at(57);
 		else if (m_variation == "PDF40") weight_MC = mc_generator_weights->at(56);
 		else if (m_variation == "PDF50") weight_MC = mc_generator_weights->at(67);
 		else if (m_variation == "PDF51") weight_MC = mc_generator_weights->at(68);
 		else if (m_variation == "PDF52") weight_MC = mc_generator_weights->at(69);
 		else if (m_variation == "PDF53") weight_MC = mc_generator_weights->at(70);
 		else if (m_variation == "PDF54") weight_MC = mc_generator_weights->at(71);
 		else if (m_variation == "PDF55") weight_MC = mc_generator_weights->at(72);
 		else if (m_variation == "PDF56") weight_MC = mc_generator_weights->at(73);
 		else if (m_variation == "PDF57") weight_MC = mc_generator_weights->at(74);
 		else if (m_variation == "PDF58") weight_MC = mc_generator_weights->at(75);
 		else if (m_variation == "PDF59") weight_MC = mc_generator_weights->at(76);
	    	else {
	    	    m_lg->Err("ss", "Unknown variation ->", m_variation.c_str());
	    	    exit(-1);
	    	}
	    }

	    if (m_selection == "CutCRHFake" || m_selection == "CutCRHFakeCvt" || m_selection == "CutCRHFakeUnCvt") {
		weight_ph_eff = 1.0;
		weight_ph_iso = 1.0;
	    }
	    weight = weight_MC * weight_pu * weight_lSF * weight_JVT * weight_ph_eff * weight_ph_iso;
	    if (m_use_lumi_weight) {
		if (m_process == "Signal") {
		    weight *= event_lumi * event_norm2;
		} else {
		    weight *= event_lumi * event_norm;
		}
		//cout << event_lumi << " " << event_norm << " " << event_norm2 << endl;
		if (m_use_kfac && m_process == "Signal") {
		    if (selph_index1 >= 0) {
		       if (!ph_kfactor_correct) {
		           weight *= 1.6;
		       } else {
		           weight *= ph_kfactor_correct->at(selph_index1);
		       }
		    }
		}
	    }
	    if (m_reweight_egamma) {
		int ibin = h_egamma_weight->FindBin(mphl/1000.);
		if (ibin < 1 || ibin > h_egamma_weight->GetNbinsX()) weight = 0;
	    }
	    if (m_reweight_zgamma) {
		int ibin = h_zgamma_weight->FindBin(leadph_pt/1000.);
		if (ibin > h_zgamma_weight->GetNbinsX()) ibin = h_zgamma_weight->GetNbinsX();
		
		weight *= h_zgamma_weight->GetBinContent(ibin);
	    }
	}
    }
}

void SampleAnalyzor::Clear() {
    m_treenames.clear();
    m_variations.clear();
    m_filenames.clear();
}

void SampleAnalyzor::UseKfactor(bool use) {
    m_use_kfac = use;
}

void SampleAnalyzor::DoBootStrap(bool use) {
    m_do_bootstrap = use;
}

void SampleAnalyzor::UseKfactor(bool use, string file, string hist) {
    m_use_kfac = use;
    m_kfac_file = file;
    if (hist.find("pt", 0) != string::npos) m_pt_kfac = true;
    else m_pt_kfac = false;
    m_kfac_hist = hist;
}

void SampleAnalyzor::UseEvtWeight(bool use) {
    m_use_weight = use;
}

void SampleAnalyzor::UseLumiWeight(bool use) {
    m_use_lumi_weight = use;
}

void SampleAnalyzor::SaveKey(string savekey) {
    m_savekey = savekey;
}

void SampleAnalyzor::SaveTo(string saveto) {
    m_saveto = saveto;
}

void SampleAnalyzor::SaveOption(string opt) {
    m_saveopt = opt;
}

void SampleAnalyzor::Notify(bool notify) {
    m_notify = notify;
}

void SampleAnalyzor::Check() {
    if (m_treenames.size() != m_variations.size()) {
	m_lg->Err("s", "Number of treenames NOT equal to number of variations!");
	exit(-1);
    }
}

void SampleAnalyzor::InitHistogram() {
    f_kfac = NULL;
    h_kfac = NULL;

    // cutflow
    h_Cutflow = NULL;
    h_Cutflow_QCD.clear();
    h_LeadLepPt_QCD.clear();
    h_LeadJetPt_QCD.clear();
    h_LeadPhPt_QCD.clear();
    h_MWT_QCD.clear();
    h_MET_QCD.clear();
    h_dPhiLepMET_QCD.clear();
    m_cf_inherit = false;
    h_bootstrap_LeadPhPt.clear();
    h_bootstrap_LeadPhEta.clear();
    h_bootstrap_LeadPhMinDrPhLep.clear();
    h_bootstrap_DEtaLepLep.clear();
    h_bootstrap_DPhiLepLep.clear();
	
    // histograms
    m_hists.clear();
    m_2Dhists.clear();
    m_2Dhists_vars.clear();
    h_best_CA = NULL;
    h_best_lh_best = NULL;
    h_best_ph_lh_best = NULL;
    h_best_top_pole_mass = NULL;
    h_best_reco_hW_mass = NULL;
    h_best_reco_lW_mass = NULL;
    h_best_reco_htop_mass = NULL;
    h_best_reco_htop_pt = NULL;
    h_best_reco_htop_eta = NULL;
    h_best_reco_ltop_mass = NULL;
    h_best_reco_ltop_pt = NULL;
    h_best_reco_ltop_eta = NULL;

    h_CA = NULL;
    h_lh_best = NULL;
    h_top_pole_mass = NULL;
    h_reco_hW_mass = NULL;
    h_reco_lW_mass = NULL;
    h_reco_htop_mass = NULL;
    h_reco_htop_pt = NULL;
    h_reco_htop_eta = NULL;
    h_reco_ltop_mass = NULL;
    h_reco_ltop_pt = NULL;
    h_reco_ltop_eta = NULL;

    h_lt_CA = NULL;
    h_lt_lh_best = NULL;
    h_lt_top_pole_mass = NULL;
    h_lt_reco_hW_mass = NULL;
    h_lt_reco_lW_mass = NULL;
    h_lt_reco_htop_mass = NULL;
    h_lt_reco_htop_pt = NULL;
    h_lt_reco_htop_eta = NULL;
    h_lt_reco_ltop_mass = NULL;
    h_lt_reco_ltop_pt = NULL;
    h_lt_reco_ltop_eta = NULL;

    h_ht_CA = NULL;
    h_ht_lh_best = NULL;
    h_ht_top_pole_mass = NULL;
    h_ht_reco_hW_mass = NULL;
    h_ht_reco_lW_mass = NULL;
    h_ht_reco_htop_mass = NULL;
    h_ht_reco_htop_pt = NULL;
    h_ht_reco_htop_eta = NULL;
    h_ht_reco_ltop_mass = NULL;
    h_ht_reco_ltop_pt = NULL;
    h_ht_reco_ltop_eta = NULL;

    h_lW_CA = NULL;
    h_lW_lh_best = NULL;
    h_lW_top_pole_mass = NULL;
    h_lW_reco_hW_mass = NULL;
    h_lW_reco_lW_mass = NULL;
    h_lW_reco_htop_mass = NULL;
    h_lW_reco_htop_pt = NULL;
    h_lW_reco_htop_eta = NULL;
    h_lW_reco_ltop_mass = NULL;
    h_lW_reco_ltop_pt = NULL;
    h_lW_reco_ltop_eta = NULL;

    h_hW_CA = NULL;
    h_hW_lh_best = NULL;
    h_hW_top_pole_mass = NULL;
    h_hW_reco_hW_mass = NULL;
    h_hW_reco_lW_mass = NULL;
    h_hW_reco_htop_mass = NULL;
    h_hW_reco_htop_pt = NULL;
    h_hW_reco_htop_eta = NULL;
    h_hW_reco_ltop_mass = NULL;
    h_hW_reco_ltop_pt = NULL;
    h_hW_reco_ltop_eta = NULL;

    h2_CA = NULL;
    h2_lh_best = NULL;
    h2_top_pole_mass = NULL;
    h2_reco_hW_mass = NULL;
    h2_reco_lW_mass = NULL;
    h2_reco_htop_mass = NULL;
    h2_reco_htop_pt = NULL;
    h2_reco_htop_eta = NULL;
    h2_reco_ltop_mass = NULL;
    h2_reco_ltop_pt = NULL;
    h2_reco_ltop_eta = NULL;

    h2_lh_ph_lh_best = NULL;

    // init kfactor
    //if (m_use_kfac && m_kfac_file != "") {
    //    f_kfac = new TFile(m_kfac_file.c_str());
    //    h_kfac = (TH1F*)f_kfac->Get(m_kfac_hist.c_str());
    //}

    if (m_use_efake_eta_SF) {
        TFile *f = new TFile("/afs/cern.ch/work/y/yili/private/Analysis/Siegen13TeVWork/MainCode/run/results/EGammaSF.root");
        h_efake_eta_weight = (TH1F*)f->Get("SF_Etabins_");
    }

    // cutflow
    h_Cutflow = new TH1F(AppendInfo("Cutflow").c_str(), AppendInfo("Cutflow").c_str(), 50, 0.5, 50.5); h_Cutflow->Sumw2();

    if (m_type == "QCD" && !m_QCD_1para) {
	m_lg->Warn("s", "Creating multiple cutflows for QCD sample!");
	for (int i = 0; i < n_QCD_para; i++) {
	    char tmp[100];
	    sprintf(tmp, "Cutflow_Para%d", i+1); TH1F* tmp_Cutflow = new TH1F(AppendInfo(tmp).c_str(), AppendInfo(tmp).c_str(), 50, 0.5, 50.5); tmp_Cutflow->Sumw2(); h_Cutflow_QCD.push_back(tmp_Cutflow);
	    sprintf(tmp, "LeadLepPt_Para%d", i+1); TH1F* tmp_LeadLepPt = new TH1F(AppendInfo(tmp).c_str(), AppendInfo(tmp).c_str(), 10, 0, 200); tmp_LeadLepPt->Sumw2(); h_LeadLepPt_QCD.push_back(tmp_LeadLepPt);
	    sprintf(tmp, "LeadJetPt_Para%d", i+1); TH1F* tmp_LeadJetPt = new TH1F(AppendInfo(tmp).c_str(), AppendInfo(tmp).c_str(), 10, 0, 200); tmp_LeadJetPt->Sumw2(); h_LeadJetPt_QCD.push_back(tmp_LeadJetPt);
	    sprintf(tmp, "LeadPhPt_Para%d", i+1); TH1F* tmp_LeadPhPt = new TH1F(AppendInfo(tmp).c_str(), AppendInfo(tmp).c_str(), 10, 0, 200); tmp_LeadPhPt->Sumw2(); h_LeadPhPt_QCD.push_back(tmp_LeadPhPt);
	    sprintf(tmp, "MWT_Para%d", i+1); TH1F* tmp_MWT = new TH1F(AppendInfo(tmp).c_str(), AppendInfo(tmp).c_str(), 10, 0, 200); tmp_MWT->Sumw2(); h_MWT_QCD.push_back(tmp_MWT);
	    sprintf(tmp, "MET_Para%d", i+1); TH1F* tmp_MET = new TH1F(AppendInfo(tmp).c_str(), AppendInfo(tmp).c_str(), 10, 0, 200); tmp_MET->Sumw2(); h_MET_QCD.push_back(tmp_MET);
	    sprintf(tmp, "dPhiLepMET_Para%d", i+1); TH1F* tmp_dPhiLepMET = new TH1F(AppendInfo(tmp).c_str(), AppendInfo(tmp).c_str(), 14, 0, 7); tmp_dPhiLepMET->Sumw2(); h_dPhiLepMET_QCD.push_back(tmp_dPhiLepMET);
	}
    }

    if (m_type == "Data" && m_do_bootstrap) {
	m_lg->Warn("s", "Creating multiple historams for bootstrap!");
	for (int i = 0; i < 1000; i++) {
	    char tmp[100];
	    sprintf(tmp, "LeadPhPt_BS%d", i+1); 
	    Double_t bins_LeadPhPt[] = {20,35,50,65,80, 95 ,110,140, 180,300}; 
	    int binnum_LeadPhPt = sizeof(bins_LeadPhPt)/sizeof(Double_t) - 1; 
	    TH1F* tmp_LeadPhPt = new TH1F(AppendInfo(tmp).c_str(), AppendInfo(tmp).c_str(), binnum_LeadPhPt, bins_LeadPhPt); 
	    tmp_LeadPhPt->Sumw2(); 
	    h_bootstrap_LeadPhPt.push_back(tmp_LeadPhPt);
	    sprintf(tmp, "LeadPhEta_BS%d", i+1); 
	    Double_t bins_LeadPhEta[] = {0, 0.2, 0.4, 0.6, 0.8, 1.0, 1.2, 1.7, 2.37};
	    int binnum_LeadPhEta = sizeof(bins_LeadPhEta)/sizeof(Double_t) - 1; 
	    TH1F* tmp_LeadPhEta = new TH1F(AppendInfo(tmp).c_str(), AppendInfo(tmp).c_str(), binnum_LeadPhEta, bins_LeadPhEta); 
	    tmp_LeadPhEta->Sumw2(); 
	    h_bootstrap_LeadPhEta.push_back(tmp_LeadPhEta);
	    sprintf(tmp, "LeadPhMinDrPhLep_BS%d", i+1); 
	    Double_t bins_LeadPhMinDrPhLep[] = {1.0,1.2,1.4,1.6,1.8,2.0,2.2,2.4,2.6,6};
	    int binnum_LeadPhMinDrPhLep = sizeof(bins_LeadPhMinDrPhLep)/sizeof(Double_t) - 1; 
	    TH1F* tmp_LeadPhMinDrPhLep = new TH1F(AppendInfo(tmp).c_str(), AppendInfo(tmp).c_str(), binnum_LeadPhMinDrPhLep, bins_LeadPhMinDrPhLep); 
	    tmp_LeadPhMinDrPhLep->Sumw2(); 
	    h_bootstrap_LeadPhMinDrPhLep.push_back(tmp_LeadPhMinDrPhLep);
	    sprintf(tmp, "DEtaLepLep_BS%d", i+1); 
	    Double_t bins_DEtaLepLep[] = {0, 0.2, 0.4, 0.6, 0.8, 1.0, 1.2, 1.7, 2.5};
	    int binnum_DEtaLepLep = sizeof(bins_DEtaLepLep)/sizeof(Double_t) - 1; 
	    TH1F* tmp_DEtaLepLep = new TH1F(AppendInfo(tmp).c_str(), AppendInfo(tmp).c_str(), binnum_DEtaLepLep, bins_DEtaLepLep); 
	    tmp_DEtaLepLep->Sumw2(); 
	    h_bootstrap_DEtaLepLep.push_back(tmp_DEtaLepLep);
	    sprintf(tmp, "DPhiLepLep_BS%d", i+1); 
	    Double_t bins_DPhiLepLep[] = {0, 0.35, 0.70, 1.05, 1.40, 1.75, 2.10, 2.45, 2.8, 3.14};
	    int binnum_DPhiLepLep = sizeof(bins_DPhiLepLep)/sizeof(Double_t) - 1; 
	    TH1F* tmp_DPhiLepLep = new TH1F(AppendInfo(tmp).c_str(), AppendInfo(tmp).c_str(), binnum_DPhiLepLep, bins_DPhiLepLep); 
	    tmp_DPhiLepLep->Sumw2(); 
	    h_bootstrap_DPhiLepLep.push_back(tmp_DPhiLepLep);
	}
    }

    if (m_cali_egamma) {
	TFile* f_egamma_alpha = new TFile("results/EGammaCalibration.root");
	if (m_cali_egamma_type == "1") {
	    if (m_selection.find("Reverse",0) == string::npos) {
		h_egamma_alpha = (TH1F*)f_egamma_alpha->Get("Postfit_CaliHist_TypeApB_Ptbin_Alpha");
	    } else {
		h_egamma_alpha = (TH1F*)f_egamma_alpha->Get("Postfit_CaliHist_TypeApB_Ptbin_Alpha_Reverse");
	    }
	} else if (m_cali_egamma_type == "2") {
	    h_egamma_alpha = (TH1F*)f_egamma_alpha->Get("Postfit_CaliHist_TypeApB_NpR_Ptbin_Alpha");
	} else if (m_cali_egamma_type == "3") {
	    if (m_selection.find("Reverse",0) == string::npos) {
		h_egamma_alpha = (TH1F*)f_egamma_alpha->Get("Prefit_CaliHist_TypeApB_Ptbin_Alpha");
	    } else {
		h_egamma_alpha = (TH1F*)f_egamma_alpha->Get("Prefit_CaliHist_TypeApB_Ptbin_Alpha_Reverse");
	    }
	} else if (m_cali_egamma_type == "4") {
	    h_egamma_alpha = (TH1F*)f_egamma_alpha->Get("Prefit_CaliHist_TypeApB_NpR_Ptbin_Alpha");
	}
    }

    if (m_reweight_egamma) {
	TFile* f_egamma_weight = new TFile("/afs/cern.ch/work/y/yili/private/Analysis/Siegen13TeVWork/MainCode/run/results/Nominal/EFakeWeights.root");
	if (!f_egamma_weight) {
	    m_lg->Err("s", "Can't open -> //afs/cern.ch/work/y/yili/private/Analysis/Siegen13TeVWork/MainCode/run/scripts/Nominal/EFakeWeights.root");
	    exit(-1);
	}
	h_egamma_weight = (TH1F*)f_egamma_weight->Get("Weight");
    }

    if (m_reweight_hfake || m_reweight_hfake_025 || m_reweight_hfake_05 || m_reweight_hfake_075) {
	TFile* f_hfake_weight = new TFile("/afs/cern.ch/work/y/yili/private/Analysis/Siegen13TeVWork/MainCode/run/results/Upgrade/HFakeCorr.root");
	if (!f_hfake_weight) {
	    m_lg->Err("s", "Can't open -> /afs/cern.ch/work/y/yili/private/Analysis/Siegen13TeVWork/MainCode/run/results/Upgrade/HFakeCorr.root");
	    exit(-1);
	}
	if (m_subregion == "ejets" || m_subregion == "mujets") {
	    h_hfake_weight = (TH1F*)f_hfake_weight->Get("HFakeCorr_ljets");
	} else {
	    h_hfake_weight = (TH1F*)f_hfake_weight->Get("HFakeCorr_ll");
	}
    }

    if (m_reweight_zgamma) {
	TFile* f_zgamma_weight = new TFile("/afs/cern.ch/work/y/yili/private/Analysis/Siegen13TeVWork/MainCode/run/results/Upgrade/ZGammaCorr.root");
	if (!f_zgamma_weight) {
	    m_lg->Err("s", "Can't open -> /afs/cern.ch/work/y/yili/private/Analysis/Siegen13TeVWork/MainCode/run/results/Upgrade/ZGammaCorr.root");
	    exit(-1);
	}
	if (m_subregion == "ejets" || m_subregion == "mujets") {
	    h_zgamma_weight = (TH1F*)f_zgamma_weight->Get("ZGammaCorr_ljets");
	} else {
	    h_zgamma_weight = (TH1F*)f_zgamma_weight->Get("ZGammaCorr_ll");
	}
    }

    if (m_do_klfitter) {
	//h_best_CA = new TH1F(AppendInfo("Best_CA").c_str(), "Best Likelihood", 2, -1, 1); h_best_CA->Sumw2();
	//h_best_lh_best = new TH1F(AppendInfo("Best_LHBest").c_str(), "Best Likelihood", 140, -150, -10); h_best_lh_best->Sumw2();
	//h_best_ph_lh_best = new TH1F(AppendInfo("Best_Ph_LHBest").c_str(), "Best Likelihood", 140, -150, -10); h_best_ph_lh_best->Sumw2();
	//h_best_top_pole_mass = new TH1F(AppendInfo("Best_TopPoleMass").c_str(), "Top Pole Mass", 50, 80, 580); h_best_top_pole_mass->Sumw2();
	//h_best_reco_hW_mass = new TH1F(AppendInfo("Best_HadronWMass").c_str(), "Best Likelihood", 40, 0, 200); h_best_reco_hW_mass->Sumw2();
	//h_best_reco_lW_mass = new TH1F(AppendInfo("Best_LeptonWMass").c_str(), "Best Likelihood", 40, 0, 200); h_best_reco_lW_mass->Sumw2();
	//h_best_reco_htop_mass = new TH1F(AppendInfo("Best_HadronTopMass").c_str(), "Best Likelihood", 50, 80, 580); h_best_reco_htop_mass->Sumw2();
	//h_best_reco_ltop_mass = new TH1F(AppendInfo("Best_LeptonTopMass").c_str(), "Best Likelihood", 50, 80, 580); h_best_reco_ltop_mass->Sumw2();
	//h_best_reco_htop_pt = new TH1F(AppendInfo("Best_HadronTopPt").c_str(), "Best Likelihood", 50, 0, 500); h_best_reco_htop_pt->Sumw2();
	//h_best_reco_htop_eta = new TH1F(AppendInfo("Best_HadronTopEta").c_str(), "Best Likelihood", 30, -3, 3); h_best_reco_htop_eta->Sumw2();
	//h_best_reco_ltop_pt = new TH1F(AppendInfo("Best_LeptonTopPt").c_str(), "Best Likelihood", 50, 0, 500); h_best_reco_ltop_pt->Sumw2();
	//h_best_reco_ltop_eta = new TH1F(AppendInfo("Best_LeptonTopEta").c_str(), "Best Likelihood", 30, -3, 3); h_best_reco_ltop_eta->Sumw2();

	//h_CA = new TH1F(AppendInfo("CA").c_str(), "Best Likelihood", 2, -1, 1); h_CA->Sumw2();
	//h_lh_best = new TH1F(AppendInfo("LHBest").c_str(), "Best Likelihood", 140, -150, -10); h_lh_best->Sumw2();
	//h_top_pole_mass = new TH1F(AppendInfo("TopPoleMass").c_str(), "Top Pole Mass", 50, 80, 580); h_top_pole_mass->Sumw2();
	//h_reco_hW_mass = new TH1F(AppendInfo("HadronWMass").c_str(), "Best Likelihood", 40, 0, 200); h_reco_hW_mass->Sumw2();
	//h_reco_lW_mass = new TH1F(AppendInfo("LeptonWMass").c_str(), "Best Likelihood", 40, 0, 200); h_reco_lW_mass->Sumw2();
	//h_reco_htop_mass = new TH1F(AppendInfo("HadronTopMass").c_str(), "Best Likelihood", 50, 80, 580); h_reco_htop_mass->Sumw2();
	//h_reco_ltop_mass = new TH1F(AppendInfo("LeptonTopMass").c_str(), "Best Likelihood", 50, 80, 580); h_reco_ltop_mass->Sumw2();
	//h_reco_htop_pt = new TH1F(AppendInfo("HadronTopPt").c_str(), "Best Likelihood", 50, 0, 500); h_reco_htop_pt->Sumw2();
	//h_reco_htop_eta = new TH1F(AppendInfo("HadronTopEta").c_str(), "Best Likelihood", 30, -3, 3); h_reco_htop_eta->Sumw2();
	//h_reco_ltop_pt = new TH1F(AppendInfo("LeptonTopPt").c_str(), "Best Likelihood", 50, 0, 500); h_reco_ltop_pt->Sumw2();
	//h_reco_ltop_eta = new TH1F(AppendInfo("LeptonTopEta").c_str(), "Best Likelihood", 30, -3, 3); h_reco_ltop_eta->Sumw2();

	//h_lt_CA = new TH1F(AppendInfo("LeptonTop_CA").c_str(), "Best Likelihood", 2, -1, 1); h_lt_CA->Sumw2();
	//h_lt_lh_best = new TH1F(AppendInfo("LeptonTop_LHBest").c_str(), "Best Likelihood", 140, -150, -10); h_lt_lh_best->Sumw2();
	//h_lt_top_pole_mass = new TH1F(AppendInfo("LeptonTop_TopPoleMass").c_str(), "Top Pole Mass", 50, 80, 580); h_lt_top_pole_mass->Sumw2();
	//h_lt_reco_hW_mass = new TH1F(AppendInfo("LeptonTop_HadronWMass").c_str(), "Best Likelihood", 40, 0, 200); h_lt_reco_hW_mass->Sumw2();
	//h_lt_reco_lW_mass = new TH1F(AppendInfo("LeptonTop_LeptonWMass").c_str(), "Best Likelihood", 40, 0, 200); h_lt_reco_lW_mass->Sumw2();
	//h_lt_reco_htop_mass = new TH1F(AppendInfo("LeptonTop_HadronTopMass").c_str(), "Best Likelihood", 50, 80, 580); h_lt_reco_htop_mass->Sumw2();
	//h_lt_reco_ltop_mass = new TH1F(AppendInfo("LeptonTop_LeptonTopMass").c_str(), "Best Likelihood", 50, 80, 580); h_lt_reco_ltop_mass->Sumw2();
	//h_lt_reco_htop_pt = new TH1F(AppendInfo("LeptonTop_HadronTopPt").c_str(), "Best Likelihood", 50, 0, 500); h_lt_reco_htop_pt->Sumw2();
	//h_lt_reco_htop_eta = new TH1F(AppendInfo("LeptonTop_HadronTopEta").c_str(), "Best Likelihood", 30, -3, 3); h_lt_reco_htop_eta->Sumw2();
	//h_lt_reco_ltop_pt = new TH1F(AppendInfo("LeptonTop_LeptonTopPt").c_str(), "Best Likelihood", 50, 0, 500); h_lt_reco_ltop_pt->Sumw2();
	//h_lt_reco_ltop_eta = new TH1F(AppendInfo("LeptonTop_LeptonTopEta").c_str(), "Best Likelihood", 30, -3, 3); h_lt_reco_ltop_eta->Sumw2();

	//h_ht_CA = new TH1F(AppendInfo("HadronTop_CA").c_str(), "Best Likelihood", 2, -1, 1); h_ht_CA->Sumw2();
	//h_ht_lh_best = new TH1F(AppendInfo("HadronTop_LHBest").c_str(), "Best Likelihood", 140, -150, -10); h_ht_lh_best->Sumw2();
	//h_ht_top_pole_mass = new TH1F(AppendInfo("HadronTop_TopPoleMass").c_str(), "Top Pole Mass", 50, 80, 580); h_ht_top_pole_mass->Sumw2();
	//h_ht_reco_hW_mass = new TH1F(AppendInfo("HadronTop_HadronWMass").c_str(), "Best Likelihood", 40, 0, 200); h_ht_reco_hW_mass->Sumw2();
	//h_ht_reco_lW_mass = new TH1F(AppendInfo("HadronTop_LeptonWMass").c_str(), "Best Likelihood", 40, 0, 200); h_ht_reco_lW_mass->Sumw2();
	//h_ht_reco_htop_mass = new TH1F(AppendInfo("HadronTop_HadronTopMass").c_str(), "Best Likelihood", 50, 80, 580); h_ht_reco_htop_mass->Sumw2();
	//h_ht_reco_ltop_mass = new TH1F(AppendInfo("HadronTop_LeptonTopMass").c_str(), "Best Likelihood", 50, 80, 580); h_ht_reco_ltop_mass->Sumw2();
	//h_ht_reco_htop_pt = new TH1F(AppendInfo("HadronTop_HadronTopPt").c_str(), "Best Likelihood", 50, 0, 500); h_ht_reco_htop_pt->Sumw2();
	//h_ht_reco_htop_eta = new TH1F(AppendInfo("HadronTop_HadronTopEta").c_str(), "Best Likelihood", 30, -3, 3); h_ht_reco_htop_eta->Sumw2();
	//h_ht_reco_ltop_pt = new TH1F(AppendInfo("HadronTop_LeptonTopPt").c_str(), "Best Likelihood", 50, 0, 500); h_ht_reco_ltop_pt->Sumw2();
	//h_ht_reco_ltop_eta = new TH1F(AppendInfo("HadronTop_LeptonTopEta").c_str(), "Best Likelihood", 30, -3, 3); h_ht_reco_ltop_eta->Sumw2();

	//h_lW_CA = new TH1F(AppendInfo("LeptonW_CA").c_str(), "Best Likelihood", 2, -1, 1); h_lW_CA->Sumw2();
	//h_lW_lh_best = new TH1F(AppendInfo("LeptonW_LHBest").c_str(), "Best Likelihood", 140, -150, -10); h_lW_lh_best->Sumw2();
	//h_lW_top_pole_mass = new TH1F(AppendInfo("LeptonW_TopPoleMass").c_str(), "Top Pole Mass", 50, 80, 580); h_lW_top_pole_mass->Sumw2();
	//h_lW_reco_hW_mass = new TH1F(AppendInfo("LeptonW_HadronWMass").c_str(), "Best Likelihood", 40, 0, 200); h_lW_reco_hW_mass->Sumw2();
	//h_lW_reco_lW_mass = new TH1F(AppendInfo("LeptonW_LeptonWMass").c_str(), "Best Likelihood", 40, 0, 200); h_lW_reco_lW_mass->Sumw2();
	//h_lW_reco_htop_mass = new TH1F(AppendInfo("LeptonW_HadronTopMass").c_str(), "Best Likelihood", 50, 80, 580); h_lW_reco_htop_mass->Sumw2();
	//h_lW_reco_ltop_mass = new TH1F(AppendInfo("LeptonW_LeptonTopMass").c_str(), "Best Likelihood", 50, 80, 580); h_lW_reco_ltop_mass->Sumw2();
	//h_lW_reco_htop_pt = new TH1F(AppendInfo("LeptonW_HadronTopPt").c_str(), "Best Likelihood", 50, 0, 500); h_lW_reco_htop_pt->Sumw2();
	//h_lW_reco_htop_eta = new TH1F(AppendInfo("LeptonW_HadronTopEta").c_str(), "Best Likelihood", 30, -3, 3); h_lW_reco_htop_eta->Sumw2();
	//h_lW_reco_ltop_pt = new TH1F(AppendInfo("LeptonW_LeptonTopPt").c_str(), "Best Likelihood", 50, 0, 500); h_lW_reco_ltop_pt->Sumw2();
	//h_lW_reco_ltop_eta = new TH1F(AppendInfo("LeptonW_LeptonTopEta").c_str(), "Best Likelihood", 30, -3, 3); h_lW_reco_ltop_eta->Sumw2();

	//h_hW_CA = new TH1F(AppendInfo("HadronW_CA").c_str(), "Best Likelihood", 2, -1, 1); h_hW_CA->Sumw2();
	//h_hW_lh_best = new TH1F(AppendInfo("HadronW_LHBest").c_str(), "Best Likelihood", 140, -150, -10); h_hW_lh_best->Sumw2();
	//h_hW_top_pole_mass = new TH1F(AppendInfo("HadronW_TopPoleMass").c_str(), "Top Pole Mass", 50, 80, 580); h_hW_top_pole_mass->Sumw2();
	//h_hW_reco_hW_mass = new TH1F(AppendInfo("HadronW_HadronWMass").c_str(), "Best Likelihood", 40, 0, 200); h_hW_reco_hW_mass->Sumw2();
	//h_hW_reco_lW_mass = new TH1F(AppendInfo("HadronW_LeptonWMass").c_str(), "Best Likelihood", 40, 0, 200); h_hW_reco_lW_mass->Sumw2();
	//h_hW_reco_htop_mass = new TH1F(AppendInfo("HadronW_HadronTopMass").c_str(), "Best Likelihood", 50, 80, 580); h_hW_reco_htop_mass->Sumw2();
	//h_hW_reco_ltop_mass = new TH1F(AppendInfo("HadronW_LeptonTopMass").c_str(), "Best Likelihood", 50, 80, 580); h_hW_reco_ltop_mass->Sumw2();
	//h_hW_reco_htop_pt = new TH1F(AppendInfo("HadronW_HadronTopPt").c_str(), "Best Likelihood", 50, 0, 500); h_hW_reco_htop_pt->Sumw2();
	//h_hW_reco_htop_eta = new TH1F(AppendInfo("HadronW_HadronTopEta").c_str(), "Best Likelihood", 30, -3, 3); h_hW_reco_htop_eta->Sumw2();
	//h_hW_reco_ltop_pt = new TH1F(AppendInfo("HadronW_LeptonTopPt").c_str(), "Best Likelihood", 50, 0, 500); h_hW_reco_ltop_pt->Sumw2();
	//h_hW_reco_ltop_eta = new TH1F(AppendInfo("HadronW_LeptonTopEta").c_str(), "Best Likelihood", 30, -3, 3); h_hW_reco_ltop_eta->Sumw2();

	//h2_CA = new TH2F(AppendInfo("LeptonHadronTop_CA").c_str(), "Best Likelihood", 2, -1, 1, 2, -1, 1); h2_CA->Sumw2();
	//h2_lh_best = new TH2F(AppendInfo("LeptonHadronTop_LHBest").c_str(), "Best Likelihood", 140, -150, -10, 140, -150, -10); h2_lh_best->Sumw2();
	//h2_top_pole_mass = new TH2F(AppendInfo("LeptonHadronTop_TopPoleMass").c_str(), "Top Pole Mass", 50, 80, 580, 50, 80, 580); h2_top_pole_mass->Sumw2();
	//h2_reco_hW_mass = new TH2F(AppendInfo("LeptonHadronTop_HadronWMass").c_str(), "Best Likelihood", 40, 0, 200, 40, 0, 200); h2_reco_hW_mass->Sumw2();
	//h2_reco_lW_mass = new TH2F(AppendInfo("LeptonHadronTop_LeptonWMass").c_str(), "Best Likelihood", 40, 0, 200, 40, 0, 200); h2_reco_lW_mass->Sumw2();
	//h2_reco_htop_mass = new TH2F(AppendInfo("LeptonHadronTop_HadronTopMass").c_str(), "Best Likelihood", 50, 80, 580, 50, 80, 580); h2_reco_htop_mass->Sumw2();
	//h2_reco_ltop_mass = new TH2F(AppendInfo("LeptonHadronTop_LeptonTopMass").c_str(), "Best Likelihood", 50, 80, 580, 50, 80, 580); h2_reco_ltop_mass->Sumw2();
	//h2_reco_htop_pt = new TH2F(AppendInfo("LeptonHadronTop_HadronTopPt").c_str(), "Best Likelihood", 50, 0, 500, 50, 0, 500); h2_reco_htop_pt->Sumw2();
	//h2_reco_htop_eta = new TH2F(AppendInfo("LeptonHadronTop_HadronTopEta").c_str(), "Best Likelihood", 30, -3, 3, 30, -3, 3); h2_reco_htop_eta->Sumw2();
	//h2_reco_ltop_pt = new TH2F(AppendInfo("LeptonHadronTop_LeptonTopPt").c_str(), "Best Likelihood", 50, 0, 500, 50, 0, 500); h2_reco_ltop_pt->Sumw2();
	//h2_reco_ltop_eta = new TH2F(AppendInfo("LeptonHadronTop_LeptonTopEta").c_str(), "Best Likelihood", 30, -3, 3, 30, -3, 3); h2_reco_ltop_eta->Sumw2();

	//h2_lh_ph_lh_best = new TH2F(AppendInfo("LHBest_Ph_LHBest").c_str(), "Best Likelihood", 140, -150, -10, 140, -150, -10); h2_lh_ph_lh_best->Sumw2();
    }
	
    // histograms
    if (m_do_hists) {
	if (m_debug) m_lg->Info("s", "Initialize histograms");

    	ifstream if_hists;
    	if_hists.open(m_histogramlist.c_str());
	if (!if_hists.is_open()) {
    	    m_lg->Warn("s", "No histogram configuration file found!");
    	} else {	
	    string line;
    	    while (getline(if_hists,line)) {
	        if (line == "") continue;
	        if (line.at(0) == ' ') continue;
    	        if (line.find("#",0) != string::npos) continue;
	        istringstream iss(line);
	        vector<string> words;
	        for (string word; iss >> word;) {
		    words.push_back(word);
	        }
	        if (words.size() == 2) {
		    string varname = removeSpaces(words.at(0));
	    	    vector<string> tmp_bins;
		    split(words.at(1).c_str(), tmp_bins, ',' );
		    Double_t bins[tmp_bins.size()];
		    for (int i = 0; i < tmp_bins.size(); i++) { 
		        bins[i] = atof(tmp_bins.at(i).c_str());
		    }
		    int binnum = sizeof(bins)/sizeof(Double_t) - 1;
		    TH1F* h_tmp = new TH1F(AppendInfo(varname).c_str(), varname.c_str(), binnum, bins);
		    h_tmp->Sumw2();
		    m_hists.push_back(h_tmp);
	        } else if (words.size() == 4) {
		    string varname = removeSpaces(words.at(0));
	    	    int varnb = atoi(removeSpaces(words.at(1)).c_str());
		    double varlo = atof(removeSpaces(words.at(2)).c_str());
		    double varhi = atof(removeSpaces(words.at(3)).c_str());
		    TH1F* h_tmp = new TH1F(AppendInfo(varname).c_str(), varname.c_str(), varnb, varlo, varhi);
		    h_tmp->Sumw2();
		    m_hists.push_back(h_tmp);
	        }
    	    }
	}

    	ifstream if_2Dhists;
    	if_2Dhists.open(m_2Dhistogramlist.c_str());
	if (!if_2Dhists.is_open()) {
    	    m_lg->Warn("s", "No 2D histogram configuration file found!");
    	} else {	
	    string line;
	    while (getline(if_2Dhists,line)) {
	        if (line == "") continue;
	        if (line.at(0) == ' ') continue;
    	        if (line.find("#",0) != string::npos) continue;
	        istringstream iss(line);
	        vector<string> words;
	        for (string word; iss >> word;) {
		    words.push_back(word);
	        }
	        if (words.size() == 4) {
		    string varname1 = removeSpaces(words.at(0));
	    	    string varname2 = removeSpaces(words.at(1));
	    	    vector<string> tmp_bins1;
	    	    vector<string> tmp_bins2;
		    split(words.at(2).c_str(), tmp_bins1, ',' );
		    split(words.at(3).c_str(), tmp_bins2, ',' );
		    Double_t bins1[tmp_bins1.size()];
		    Double_t bins2[tmp_bins2.size()];
		    for (int i = 0; i < tmp_bins1.size(); i++) { 
		        bins1[i] = atof(tmp_bins1.at(i).c_str());
		    }
		    for (int i = 0; i < tmp_bins2.size(); i++) { 
		        bins2[i] = atof(tmp_bins2.at(i).c_str());
		    }
		    int binnum1 = sizeof(bins1)/sizeof(Double_t) - 1;
		    int binnum2 = sizeof(bins2)/sizeof(Double_t) - 1;
		    string varname = varname1; varname += "_"; varname += varname2;
		    TH2F* h_tmp = new TH2F(AppendInfo(varname).c_str(), varname.c_str(), binnum1, bins1, binnum2, bins2);
		    h_tmp->Sumw2();
		    m_2Dhists_vars.push_back(pair<string,string>(varname1,varname2));
		    m_2Dhists.push_back(h_tmp);
	        } else if (words.size() == 8) {
		    string varname1 = removeSpaces(words.at(0));
	    	    string varname2 = removeSpaces(words.at(1));
	    	    int varnb1 = atoi(removeSpaces(words.at(2)).c_str());
		    double varlo1 = atof(removeSpaces(words.at(3)).c_str());
		    double varhi1 = atof(removeSpaces(words.at(4)).c_str());
		    int varnb2 = atoi(removeSpaces(words.at(5)).c_str());
		    double varlo2 = atof(removeSpaces(words.at(6)).c_str());
		    double varhi2 = atof(removeSpaces(words.at(7)).c_str());
		    string varname = varname1; varname += "_"; varname += varname2;
		    TH2F* h_tmp = new TH2F(AppendInfo(varname).c_str(), varname.c_str(), varnb1, varlo1, varhi1, varnb2, varlo2, varhi2);
		    h_tmp->Sumw2();
		    m_2Dhists_vars.push_back(pair<string,string>(varname1,varname2));
    	            m_2Dhists.push_back(h_tmp);
	        } else if (words.size() == 6) {
		    string varname1 = removeSpaces(words.at(0));
	    	    string varname2 = removeSpaces(words.at(1));
		    string varname = varname1; varname += "_"; varname += varname2;
		    m_2Dhists_vars.push_back(pair<string,string>(varname1,varname2));
	    	    vector<string> tmp_bins1;
		    split(words.at(2).c_str(), tmp_bins1, ',' );
		    if (tmp_bins1.size() > 1) {
			Double_t bins1[tmp_bins1.size()];
		    	for (int i = 0; i < tmp_bins1.size(); i++) { 
		    	    bins1[i] = atof(tmp_bins1.at(i).c_str());
		    	}
		    	int binnum1 = sizeof(bins1)/sizeof(Double_t) - 1;
		    	int varnb2 = atoi(removeSpaces(words.at(3)).c_str());
		    	double varlo2 = atof(removeSpaces(words.at(4)).c_str());
		    	double varhi2 = atof(removeSpaces(words.at(5)).c_str());
		    	TH2F* h_tmp = new TH2F(AppendInfo(varname).c_str(), varname.c_str(), binnum1, bins1, varnb2, varlo2, varhi2);
		    	h_tmp->Sumw2();
		    	m_2Dhists.push_back(h_tmp);
		    } else {
	    	    	int varnb1 = atoi(removeSpaces(words.at(2)).c_str());
		    	double varlo1 = atof(removeSpaces(words.at(3)).c_str());
		    	double varhi1 = atof(removeSpaces(words.at(4)).c_str());
	    	    	vector<string> tmp_bins2;
		    	split(words.at(5).c_str(), tmp_bins2, ',' );
		    	Double_t bins2[tmp_bins2.size()];
		    	for (int i = 0; i < tmp_bins2.size(); i++) { 
		    	    bins2[i] = atof(tmp_bins2.at(i).c_str());
		    	}
		    	int binnum2 = sizeof(bins2)/sizeof(Double_t) - 1;
		    	TH2F* h_tmp = new TH2F(AppendInfo(varname).c_str(), varname.c_str(), varnb1, varlo1, varhi1, binnum2, bins2);
		    	h_tmp->Sumw2();
		    	m_2Dhists.push_back(h_tmp);
		    }
	        }
    	    }
	}
    }
    if (m_do_egammahists) {
	for (int i = 0; i < m_egammaptbins_lo.size(); i++) {
	    char tmp[100];
	    sprintf(tmp, "InvariantMass_PtBin%d", i+1);
	    TH1F* h_tmp = new TH1F(AppendInfo(tmp).c_str(), tmp, 90, 50, 140);
	    h_tmp->Sumw2();
	    m_egammahists_pt.push_back(h_tmp);
	}
	for (int i = 0; i < m_egammaetabins_lo.size(); i++) {
	    char tmp[100];
	    sprintf(tmp, "InvariantMass_EtaBin%d", i+1);
	    TH1F* h_tmp = new TH1F(AppendInfo(tmp).c_str(), tmp, 90, 50, 140);
	    h_tmp->Sumw2();
	    m_egammahists_eta.push_back(h_tmp);
	}
	for (int i = 0; i < m_egammapt2dbins_lo.size(); i++) {
	    vector<TH1F*> tmp_hists;
	    for (int j = 0; j < m_egammaeta2dbins_lo.size(); j++) {
		char tmp[100];
		sprintf(tmp, "InvariantMass_PtBin%d_EtaBin%d", i+1, j+1);
		TH1F* h_tmp = new TH1F(AppendInfo(tmp).c_str(), tmp, 90, 50, 140);
		h_tmp->Sumw2();
		tmp_hists.push_back(h_tmp);
	    }
	    m_egammahists_pteta.push_back(tmp_hists);
	}
	if (m_do_egammatf1) {
	    m_tkde_data.clear();
	    m_tkde_data_w.clear();
	    m_tkde_data_pteta.clear();
	    m_tkde_data_pteta_w.clear();
	    m_tkde_data_pteta.resize(m_egammapt2dbins_lo.size());
	    m_tkde_data_pteta_w.resize(m_egammapt2dbins_lo.size());
	    for (int i = 0; i < m_egammapt2dbins_lo.size(); i++) {
		vector<vector<double> > tmp_tkde_data_pteta;
		vector<vector<double> > tmp_tkde_data_pteta_w;
		tmp_tkde_data_pteta.clear();
		tmp_tkde_data_pteta_w.clear();
		tmp_tkde_data_pteta.resize(m_egammaeta2dbins_lo.size());
		tmp_tkde_data_pteta_w.resize(m_egammaeta2dbins_lo.size());
	        for (int j = 0; j < m_egammaeta2dbins_lo.size(); j++) {
		    vector<double> tmp_tmp_tkde_data_pteta;
		    vector<double> tmp_tmp_tkde_data_pteta_w;
		    tmp_tmp_tkde_data_pteta.clear();
		    tmp_tmp_tkde_data_pteta_w.clear();
		    tmp_tkde_data_pteta.at(j) = tmp_tmp_tkde_data_pteta;
		    tmp_tkde_data_pteta_w.at(j) = tmp_tmp_tkde_data_pteta_w;
		}
		m_tkde_data_pteta.at(i) = tmp_tkde_data_pteta;
		m_tkde_data_pteta_w.at(i) = tmp_tkde_data_pteta_w;
	    }
	}
    }
}

int SampleAnalyzor::InitFile(bool oneevt) {

    if (m_debug) m_lg->Info("s", "Initialize file");

    m_file = NULL;
    ejets_2015 = false;
    mujets_2015 = false;
    ejets_2016 = false;
    mujets_2016 = false;
    emu_2015 = false;
    ee_2015 = false;
    mumu_2015 = false;
    emu_2016 = false;
    ee_2016 = false;
    mumu_2016 = false;
    emuSS_2015 = false;
    eeSS_2015 = false;
    mumuSS_2015 = false;
    emuSS_2016 = false;
    eeSS_2016 = false;
    mumuSS_2016 = false;

    zee_2015 = false;
    zeg_2015 = false;
    zee_2016 = false;
    zeg_2016 = false;

    ejets_gamma_basic = false;
    mujets_gamma_basic = false;
    ee_gamma_basic = false;
    emu_gamma_basic = false;
    mumu_gamma_basic = false;

    ttel_ee_2015 = false;
    ttel_ee_2016 = false;
    ttel_mue_2015 = false;
    ttel_mue_2016 = false;

    // init tree
    m_file = new TFile(m_filename.c_str());
    if (!m_file->IsOpen()) {
	m_lg->Err("ss", "Cannot open file ->", m_filename.c_str());
	exit(-1);
    }

    m_ifile++;

    //totalmc.clear();
    //xsection.clear();
    //filter.clear();
    //kfactor.clear();
    //mcid.clear();
    //if (m_use_lumi_weight) {

    //    string drname;
    //    if (m_region == "EF1") {
    //        if (m_subregion == "zeg") {
    //    	drname = "zeg_";
    //        } else if (m_subregion == "zee") {
    //    	drname = "zee_";
    //        } else if (m_subregion == "ttel_ee") {
    //    	drname = "ttel_ee_";
    //        } else if (m_subregion == "ttel_mue") {
    //    	drname = "ttel_mue_";
    //        }
    //    } else if (m_region == "CR1" || m_region == "VR") {
    //        if (m_subregion == "ejets") {
    //    	drname = "ejets_";
    //        } else if (m_subregion == "mujets") {
    //    	drname = "mujets_";
    //        } else if (m_subregion == "emu") {
    //    	drname = "emu_";
    //        } else if (m_subregion == "ee") {
    //    	drname = "ee_";
    //        } else if (m_subregion == "mumu") {
    //    	drname = "mumu_";
    //        }
    //    } else if (m_region == "SS") {
    //        if (m_subregion == "emu") {
    //    	drname = "emuSS_";
    //        } else if (m_subregion == "ee") {
    //    	drname = "eeSS_";
    //        } else if (m_subregion == "mumu") {
    //    	drname = "mumuSS_";
    //        }
    //    }

    //    string drname2015 = drname; drname2015 += "2015";
    //    TDirectoryFile *dr2015 = NULL;
    //    if (!(m_process == "ZjetsElEl" && m_region == "CR1") && !(m_process == "TTBar" && m_region == "CR1")) dr2015 = (TDirectoryFile*)m_file->Get(drname2015.c_str());
    //    if (dr2015) { 
    //        string hname;
    //        if (m_type == "Data" || m_type == "QCD") hname = "cutflow";
    //        else hname = "cutflow_mc_pu_zvtx";
    //        TH1D* h_tmp = (TH1D*)dr2015->Get(hname.c_str());
    //        if (!h_tmp) {
    //    	m_lg->Warn("ssss", "Cannot find cutflow/cutflow_mc_pu_zvtx in dir", drname2015.c_str(), "in file", m_filename.c_str());
    //        } else {
    //    	if (m_ifile == 1) {
    //    	    m_cf_inherit = true;
    //        	    n_cuts_start = h_tmp->GetNbinsX()+1;
    //    	}
    //    	if (m_cf_inherit) {
    //    	    for (int i = 0; i < h_tmp->GetNbinsX(); i++) {
    //        	        string onlinecut = h_tmp->GetXaxis()->GetBinLabel(i+1);
    //        	        h_Cutflow->GetXaxis()->SetBinLabel(i+1, onlinecut.c_str());
    //        	        if (onlinecut.find("TRIGDEC",0) != string::npos) {
    //        	            h_Cutflow->GetXaxis()->SetBinLabel(i+1, "TRIGDEC");
    //        	        }
    //        	        if (onlinecut.find("RUN_NUMBER",0) != string::npos) {
    //        	            h_Cutflow->GetXaxis()->SetBinLabel(i+1, "RUN_NUMBER");
    //        	        }
    //    	        if (m_type != "Data" && m_type != "QCD") {
    //        	        h_Cutflow->SetBinContent(i+1, h_tmp->GetBinContent(i+1)*m_lumi + h_Cutflow->GetBinContent(i+1));
    //        	        h_Cutflow->SetBinError(i+1, sqrt(pow(h_tmp->GetBinError(i+1)*m_lumi,2) + pow(h_Cutflow->GetBinError(i+1),2)));
    //    	        } else {
    //        	        h_Cutflow->SetBinContent(i+1, h_tmp->GetBinContent(i+1) + h_Cutflow->GetBinContent(i+1));
    //        	        h_Cutflow->SetBinError(i+1, sqrt(pow(h_tmp->GetBinError(i+1),2) + pow(h_Cutflow->GetBinError(i+1),2)));
    //    	        }
    //        	    }
    //    	}

    //        	TIter next(dr2015->GetListOfKeys());
    //        	TKey *key;
    //        	while ((key = (TKey*)next())) {
    //        	    TH1D* htmp = (TH1D*)key->ReadObj();
    //        	    string hname = htmp->GetName();
    //        	    if (hname.at(0) != 'd') continue;
    //        	    if (hname.find("mc_pu_zvtx") == string::npos) continue;
    //        	    string hid = hname.substr(1,6);
    //        	    
    //        	    mcid.push_back(atoi(hid.c_str()));
    //        	    totalmc.push_back(htmp->GetBinContent(1));

    //        	    ifstream ifile;
    //        	    ifile.open(m_xsectionlist.c_str());
    //        	    string line;
    //        	    bool found = false;
    //        	    while (getline(ifile,line)) {
    //        	        vector<string> pieces;
    //    		split(line.c_str(), pieces, '|' );
    //    		if (pieces.size() < 2) continue;
    //    		if (removeSpaces(pieces.at(1)) == hid) {
    //    		    found = true;
    //    		    xsection.push_back(atof(removeSpaces(pieces.at(7)).c_str()));
    //    		    kfactor.push_back(atof(removeSpaces(pieces.at(8)).c_str()));
    //    		    filter.push_back(atof(removeSpaces(pieces.at(9)).c_str()));
    //    		    break;
    //    		}
    //        	    }
    //        	    if (!found) {
    //        	       m_lg->Warn("ss", "Cannot find xsection information for -->", hid.c_str());
    //        	       //exit(-1);
    //        	    } else {
    //    		m_lg->Info("ss", "DSID:    ", hid.c_str());
    //        	    	m_lg->Info("sf", "Xsection:", xsection.back());
    //        	    	m_lg->Info("sf", "k-factor:", kfactor.back());
    //        	    	m_lg->Info("sf", "filter:  ", filter.back());
    //        	    	m_lg->Info("sf", "total mc:", totalmc.back());
    //    	    }
    //        	}
    //        }
    //    } else {
    //        m_lg->Warn("ssss", "Cannot open dir", drname2015.c_str(), "in file", m_filename.c_str());
    //    }

    //    string drname2016 = drname; drname2016 += "2016";
    //    TDirectoryFile *dr2016 = (TDirectoryFile*)m_file->Get(drname2016.c_str());
    //    if (dr2016) { 
    //        string hname;
    //        if (m_type == "Data" || m_type == "QCD") hname = "cutflow";
    //        else hname = "cutflow_mc_pu_zvtx";
    //        TH1D* h_tmp = (TH1D*)dr2016->Get(hname.c_str());
    //        if (!h_tmp) {
    //    	m_lg->Warn("ssss", "Cannot find cutflow/cutflow_mc_pu_zvtx in dir", drname2016.c_str(), "in file", m_filename.c_str());
    //        } else {
    //    	if (m_cf_inherit) {
    //    	    for (int i = 0; i < h_tmp->GetNbinsX()+1; i++) {
    //        	        if (i < 6) continue;
    //    	        if (m_type != "Data" && m_type != "QCD") {
    //        	        h_Cutflow->SetBinContent(i+1, h_tmp->GetBinContent(i)*m_lumi + h_Cutflow->GetBinContent(i+1));
    //        	        h_Cutflow->SetBinError(i+1, sqrt(pow(h_tmp->GetBinError(i)*m_lumi,2) + pow(h_Cutflow->GetBinError(i+1),2)));
    //    	        } else {
    //        	        h_Cutflow->SetBinContent(i+1, h_tmp->GetBinContent(i) + h_Cutflow->GetBinContent(i+1));
    //        	        h_Cutflow->SetBinError(i+1, sqrt(pow(h_tmp->GetBinError(i),2) + pow(h_Cutflow->GetBinError(i+1),2)));
    //    	        }
    //        	    }
    //    	}
    //        }
    //    } else {
    //        m_lg->Warn("ssss", "Cannot open dir", drname2016.c_str(), "in file", m_filename.c_str());
    //    }
    //}

    if (m_type == "QCD") m_treename = "nominal_Loose";

    m_tree = (TTree*)m_file->Get(m_treename.c_str());
    //cout << m_treename << endl;
    //m_tree->Print();
    //cout << m_tree << endl;
    if (!m_tree) {
        m_lg->Warn("sss", "Cannot open tree ->", m_treename.c_str(), "SKIP!");
	return 0;
    }
    
    if (m_type == "Data" || m_type == "Reco" || m_type == "QCD" || m_type == "Upgrade") {
        // Set object pointer
	mc_generator_weights = 0;
	weight_poisson = 0;
        weight_bTagSF_77_eigenvars_B_up = 0;
        weight_bTagSF_77_eigenvars_C_up = 0;
        weight_bTagSF_77_eigenvars_Light_up = 0;
        weight_bTagSF_77_eigenvars_B_down = 0;
        weight_bTagSF_77_eigenvars_C_down = 0;
        weight_bTagSF_77_eigenvars_Light_down = 0;
        el_pt = 0;
        el_eta = 0;
        el_cl_eta = 0;
        el_phi = 0;
        el_e = 0;
        el_charge = 0;
        el_faketype = 0;
        el_topoetcone20 = 0;
        el_ptvarcone20 = 0;
        el_d0sig = 0;
        el_delta_z0_sintheta = 0;
        el_true_type = 0;
        el_true_origin = 0;
        el_true_typebkg = 0;
        el_true_originbkg = 0;
	el_truthAncestor = 0;
	el_mc_pid = 0;
	el_mc_pt = 0;
	el_mc_eta = 0;
	el_mc_phi = 0;
        mu_pt = 0;
        mu_eta = 0;
        mu_phi = 0;
        mu_e = 0;
        mu_charge = 0;
        mu_faketype = 0;
        mu_topoetcone20 = 0;
        mu_ptvarcone30 = 0;
        mu_d0sig = 0;
        mu_delta_z0_sintheta = 0;
        mu_true_type = 0;
        mu_true_origin = 0;
        mu_true_isPrompt = 0;
        ph_pt = 0;
        ph_eta = 0;
        ph_phi = 0;
        ph_e = 0;
        ph_iso = 0;
        jet_pt = 0;
        jet_eta = 0;
        jet_phi = 0;
        jet_e = 0;
        jet_mv1eff = 0;
        jet_isPileup = 0;
        jet_nGhosts_cHadron = 0;
        jet_nGhosts_bHadron = 0;
        ljet_pt = 0;
        ljet_eta = 0;
        ljet_phi = 0;
        ljet_e = 0;
        ljet_nGhosts_cHadron = 0;
        ljet_nGhosts_bHadron = 0;
        jet_mv2c00 = 0;
        jet_mv2c10 = 0;
        jet_mv2c20 = 0;
        jet_ip3dsv1 = 0;
        jet_jvt = 0;
        jet_truthflav = 0;
        jet_isbtagged_77 = 0;
        el_trigMatch_HLT_e60_lhmedium_nod0 = 0;
        el_trigMatch_HLT_e26_lhtight_nod0_ivarloose = 0;
        el_trigMatch_HLT_e140_lhloose_nod0 = 0;
        el_trigMatch_HLT_e60_lhmedium = 0;
        el_trigMatch_HLT_e24_lhmedium_L1EM20VH = 0;
        el_trigMatch_HLT_e120_lhloose = 0;
        mu_trigMatch_HLT_mu26_ivarmedium = 0;
        mu_trigMatch_HLT_mu50 = 0;
        mu_trigMatch_HLT_mu20_iloose_L1MU15 = 0;
        ph_topoetcone20 = 0;
        ph_topoetcone30 = 0;
        ph_topoetcone40 = 0;
        ph_ptcone20 = 0;
        ph_ptcone30 = 0;
        ph_faketype = 0;
        ph_mc_barcode = 0;
        ph_mc_Type = 0;
        ph_mc_Origin = 0;
        ph_ptcone40 = 0;
        ph_ptvarcone20 = 0;
        ph_ptvarcone30 = 0;
        ph_ptvarcone40 = 0;
        ph_isoFCTCO = 0;
        ph_isoFCT = 0;
        ph_isoFCL = 0;
        ph_isTight = 0;
        ph_isLoose = 0;
	ph_isHadronFakeFailedDeltaE = 0;
    	ph_isHadronFakeFailedFside = 0;
    	ph_isHadronFakeFailedWs3 = 0;
    	ph_isHadronFakeFailedERatio = 0;
        ph_truthType = 0;
        ph_truthOrigin = 0;
        ph_truthAncestor = 0;
        ph_HFT_MVA = 0;
        ph_mc_pid = 0;
        ph_mc_pt = 0;
        ph_mc_eta = 0;
        ph_mc_phi = 0;
        ph_mc_barcode = 0;
        ph_OQ = 0;
	ph_rhad1 = 0;
	ph_rhad = 0;
	ph_reta = 0;
	ph_weta2 = 0;
	ph_rphi = 0;
	ph_ws3 = 0;
	ph_wstot = 0;
	ph_fracm = 0;
	ph_deltaE = 0;
	ph_eratio = 0;
	ph_emaxs1 = 0;
	ph_f1 = 0;
	ph_e277 = 0;
        ph_author = 0;
        ph_conversionType = 0;
        ph_caloEta = 0;
        ph_drlept = 0;
        ph_mgammalept = 0;
	ph_kfactor_correct = 0;
	//efake_sf_Nominal = 0;
	ph_SF_eff = 0;
    	ph_SF_effUP = 0;
    	ph_SF_effDO = 0;
    	ph_SF_iso = 0;
    	ph_SF_lowisoUP = 0;
    	ph_SF_lowisoDO = 0;
    	ph_SF_trkisoUP = 0;
    	ph_SF_trkisoDO = 0;
        ph_mgammaleptlept = 0;
        ph_mcel_dr = 0;
        ph_mcel_pt = 0;
        ph_mcel_eta = 0;
        ph_mcel_phi = 0;
        ph_dralljet = 0;
        lepton_type = 0;
        el_isoGradient = 0;
        mcph_pt = 0;
        mcph_eta = 0;
        mcph_phi = 0;
        mcph_ancestor = 0;
	weights_mm_ejets = 0;
	weights_mm_mujets = 0;
    
        m_tree->SetMakeClass(1);
	if (m_type == "Upgrade") {
	    m_tree->SetBranchAddress("ejets_gamma_basic", &ejets_gamma_basic);
	    m_tree->SetBranchAddress("mujets_gamma_basic", &mujets_gamma_basic);
	    m_tree->SetBranchAddress("ee_gamma_basic", &ee_gamma_basic);
	    m_tree->SetBranchAddress("emu_gamma_basic", &emu_gamma_basic);
	    m_tree->SetBranchAddress("mumu_gamma_basic", &mumu_gamma_basic);
	    m_tree->SetBranchAddress("weight_mc", &weight_mc);
	    m_tree->SetBranchAddress("eventNumber", &eventNumber);
            m_tree->SetBranchAddress("runNumber", &runNumber);
            m_tree->SetBranchAddress("mcChannelNumber", &mcChannelNumber);
            m_tree->SetBranchAddress("mu", &mu);
	    m_tree->SetBranchAddress("el_pt", &el_pt);
            m_tree->SetBranchAddress("el_eta", &el_eta);
            m_tree->SetBranchAddress("el_phi", &el_phi);
            m_tree->SetBranchAddress("el_e", &el_e);
            m_tree->SetBranchAddress("el_charge", &el_charge);
            m_tree->SetBranchAddress("el_true_type", &el_true_type);
            m_tree->SetBranchAddress("el_true_origin", &el_true_origin);
            m_tree->SetBranchAddress("el_faketype", &el_faketype);
	    m_tree->SetBranchAddress("mu_pt", &mu_pt);
            m_tree->SetBranchAddress("mu_eta", &mu_eta);
            m_tree->SetBranchAddress("mu_phi", &mu_phi);
            m_tree->SetBranchAddress("mu_e", &mu_e);
            m_tree->SetBranchAddress("mu_charge", &mu_charge);
            m_tree->SetBranchAddress("mu_true_type", &mu_true_type);
            m_tree->SetBranchAddress("mu_true_origin", &mu_true_origin);
            m_tree->SetBranchAddress("mu_true_isPrompt", &mu_true_isPrompt);
	    m_tree->SetBranchAddress("jet_pt",  &jet_pt);
            m_tree->SetBranchAddress("jet_eta", &jet_eta);
            m_tree->SetBranchAddress("jet_phi", &jet_phi);
            m_tree->SetBranchAddress("jet_e", &jet_e);
            m_tree->SetBranchAddress("jet_mv1eff", &jet_mv1eff);
            m_tree->SetBranchAddress("jet_isPileup", &jet_isPileup);
            m_tree->SetBranchAddress("jet_nGhosts_bHadron", &jet_nGhosts_bHadron);
            m_tree->SetBranchAddress("jet_nGhosts_cHadron", &jet_nGhosts_cHadron);
	    //m_tree->SetBranchAddress("ljet_pt",  &ljet_pt);
            //m_tree->SetBranchAddress("ljet_eta", &ljet_eta);
            //m_tree->SetBranchAddress("ljet_phi", &ljet_phi);
            //m_tree->SetBranchAddress("ljet_e", &ljet_e);
            //m_tree->SetBranchAddress("ljet_nGhosts_bHadron", &ljet_nGhosts_bHadron);
            //m_tree->SetBranchAddress("ljet_nGhosts_cHadron", &ljet_nGhosts_cHadron);
            m_tree->SetBranchAddress("ph_pt", &ph_pt);
            m_tree->SetBranchAddress("ph_eta", &ph_eta);
            m_tree->SetBranchAddress("ph_phi", &ph_phi);
            m_tree->SetBranchAddress("ph_e", &ph_e);
            m_tree->SetBranchAddress("ph_faketype", &ph_faketype);
            m_tree->SetBranchAddress("ph_true_type", &ph_mc_Type);
            m_tree->SetBranchAddress("ph_true_origin", &ph_mc_Origin);
            m_tree->SetBranchAddress("met_met", &met_met);
            m_tree->SetBranchAddress("met_phi", &met_phi);
	} else {
	    if (m_type == "Data" && m_do_bootstrap) {
	        m_tree->SetBranchAddress("weight_poisson", &weight_poisson);
	    }

	    if (m_type != "Data" && m_type != "QCD") {
	        if (m_process == "Signal") m_tree->SetBranchAddress("ph_kfactor_correct", &ph_kfactor_correct);
	        //m_tree->SetBranchAddress("efake_sf_Nominal", &efake_sf_Nominal);
	        m_tree->SetBranchAddress("weight_mc", &weight_mc);
	        m_tree->SetBranchAddress("weight_pileup", &weight_pileup);
	        m_tree->SetBranchAddress("weight_leptonSF", &weight_leptonSF);
	        m_tree->SetBranchAddress("weight_photonSF", &weight_photonSF);
	        m_tree->SetBranchAddress("weight_bTagSF_77", &weight_bTagSF_77);
	        m_tree->SetBranchAddress("weight_bTagSF_Continuous", &weight_bTagSF_Continuous);
	        m_tree->SetBranchAddress("weight_jvt", &weight_jvt);
	        //if (m_process.find("Wjets", 0) != string::npos) m_tree->SetBranchAddress("weight_sherpa_22_vjets", &weight_sherpa_22_vjets);
	        if (m_treename == "nominal") {
		    if (m_region != "EF1") {
			m_tree->SetBranchAddress("mc_generator_weights", &mc_generator_weights);
			m_tree->SetBranchAddress("weight_pileup_UP", &weight_pileup_UP);
		    	m_tree->SetBranchAddress("weight_pileup_DOWN", &weight_pileup_DOWN);
		    	m_tree->SetBranchAddress("weight_leptonSF_EL_SF_Trigger_UP", &weight_leptonSF_EL_SF_Trigger_UP);
		    	m_tree->SetBranchAddress("weight_leptonSF_EL_SF_Trigger_DOWN", &weight_leptonSF_EL_SF_Trigger_DOWN);
		    	m_tree->SetBranchAddress("weight_leptonSF_EL_SF_Reco_UP", &weight_leptonSF_EL_SF_Reco_UP);
		    	m_tree->SetBranchAddress("weight_leptonSF_EL_SF_Reco_DOWN", &weight_leptonSF_EL_SF_Reco_DOWN);
		    	m_tree->SetBranchAddress("weight_leptonSF_EL_SF_ID_UP", &weight_leptonSF_EL_SF_ID_UP);
		    	m_tree->SetBranchAddress("weight_leptonSF_EL_SF_ID_DOWN", &weight_leptonSF_EL_SF_ID_DOWN);
		    	m_tree->SetBranchAddress("weight_leptonSF_EL_SF_Isol_UP", &weight_leptonSF_EL_SF_Isol_UP);
		    	m_tree->SetBranchAddress("weight_leptonSF_EL_SF_Isol_DOWN", &weight_leptonSF_EL_SF_Isol_DOWN);
		    	m_tree->SetBranchAddress("weight_leptonSF_MU_SF_Trigger_STAT_UP", &weight_leptonSF_MU_SF_Trigger_STAT_UP);
		    	m_tree->SetBranchAddress("weight_leptonSF_MU_SF_Trigger_STAT_DOWN", &weight_leptonSF_MU_SF_Trigger_STAT_DOWN);
		    	m_tree->SetBranchAddress("weight_leptonSF_MU_SF_Trigger_SYST_UP", &weight_leptonSF_MU_SF_Trigger_SYST_UP);
		    	m_tree->SetBranchAddress("weight_leptonSF_MU_SF_Trigger_SYST_DOWN", &weight_leptonSF_MU_SF_Trigger_SYST_DOWN);
		    	m_tree->SetBranchAddress("weight_leptonSF_MU_SF_ID_STAT_UP", &weight_leptonSF_MU_SF_ID_STAT_UP);
		    	m_tree->SetBranchAddress("weight_leptonSF_MU_SF_ID_STAT_DOWN", &weight_leptonSF_MU_SF_ID_STAT_DOWN);
		    	m_tree->SetBranchAddress("weight_leptonSF_MU_SF_ID_SYST_UP", &weight_leptonSF_MU_SF_ID_SYST_UP);
		    	m_tree->SetBranchAddress("weight_leptonSF_MU_SF_ID_SYST_DOWN", &weight_leptonSF_MU_SF_ID_SYST_DOWN);
		    	m_tree->SetBranchAddress("weight_leptonSF_MU_SF_ID_STAT_LOWPT_UP", &weight_leptonSF_MU_SF_ID_STAT_LOWPT_UP);
		    	m_tree->SetBranchAddress("weight_leptonSF_MU_SF_ID_STAT_LOWPT_DOWN", &weight_leptonSF_MU_SF_ID_STAT_LOWPT_DOWN);
		    	m_tree->SetBranchAddress("weight_leptonSF_MU_SF_ID_SYST_LOWPT_UP", &weight_leptonSF_MU_SF_ID_SYST_LOWPT_UP);
		    	m_tree->SetBranchAddress("weight_leptonSF_MU_SF_ID_SYST_LOWPT_DOWN", &weight_leptonSF_MU_SF_ID_SYST_LOWPT_DOWN);
		    	m_tree->SetBranchAddress("weight_leptonSF_MU_SF_Isol_STAT_UP", &weight_leptonSF_MU_SF_Isol_STAT_UP);
		    	m_tree->SetBranchAddress("weight_leptonSF_MU_SF_Isol_STAT_DOWN", &weight_leptonSF_MU_SF_Isol_STAT_DOWN);
		    	m_tree->SetBranchAddress("weight_leptonSF_MU_SF_Isol_SYST_UP", &weight_leptonSF_MU_SF_Isol_SYST_UP);
		    	m_tree->SetBranchAddress("weight_leptonSF_MU_SF_Isol_SYST_DOWN", &weight_leptonSF_MU_SF_Isol_SYST_DOWN);
		    	m_tree->SetBranchAddress("weight_leptonSF_MU_SF_TTVA_STAT_UP", &weight_leptonSF_MU_SF_TTVA_STAT_UP);
		    	m_tree->SetBranchAddress("weight_leptonSF_MU_SF_TTVA_STAT_DOWN", &weight_leptonSF_MU_SF_TTVA_STAT_DOWN);
		    	m_tree->SetBranchAddress("weight_leptonSF_MU_SF_TTVA_SYST_UP", &weight_leptonSF_MU_SF_TTVA_SYST_UP);
		    	m_tree->SetBranchAddress("weight_leptonSF_MU_SF_TTVA_SYST_DOWN", &weight_leptonSF_MU_SF_TTVA_SYST_DOWN);
		    	m_tree->SetBranchAddress("weight_indiv_SF_EL_Trigger", &weight_indiv_SF_EL_Trigger);
		    	m_tree->SetBranchAddress("weight_indiv_SF_EL_Trigger_UP", &weight_indiv_SF_EL_Trigger_UP);
		    	m_tree->SetBranchAddress("weight_indiv_SF_EL_Trigger_DOWN", &weight_indiv_SF_EL_Trigger_DOWN);
		    	m_tree->SetBranchAddress("weight_indiv_SF_EL_Reco", &weight_indiv_SF_EL_Reco);
		    	m_tree->SetBranchAddress("weight_indiv_SF_EL_Reco_UP", &weight_indiv_SF_EL_Reco_UP);
		    	m_tree->SetBranchAddress("weight_indiv_SF_EL_Reco_DOWN", &weight_indiv_SF_EL_Reco_DOWN);
		    	m_tree->SetBranchAddress("weight_indiv_SF_EL_ID", &weight_indiv_SF_EL_ID);
		    	m_tree->SetBranchAddress("weight_indiv_SF_EL_ID_UP", &weight_indiv_SF_EL_ID_UP);
		    	m_tree->SetBranchAddress("weight_indiv_SF_EL_ID_DOWN", &weight_indiv_SF_EL_ID_DOWN);
		    	m_tree->SetBranchAddress("weight_indiv_SF_EL_Isol", &weight_indiv_SF_EL_Isol);
		    	m_tree->SetBranchAddress("weight_indiv_SF_EL_Isol_UP", &weight_indiv_SF_EL_Isol_UP);
		    	m_tree->SetBranchAddress("weight_indiv_SF_EL_Isol_DOWN", &weight_indiv_SF_EL_Isol_DOWN);
		    	m_tree->SetBranchAddress("weight_indiv_SF_MU_Trigger", &weight_indiv_SF_MU_Trigger);
		    	m_tree->SetBranchAddress("weight_indiv_SF_MU_Trigger_STAT_UP", &weight_indiv_SF_MU_Trigger_STAT_UP);
		    	m_tree->SetBranchAddress("weight_indiv_SF_MU_Trigger_STAT_DOWN", &weight_indiv_SF_MU_Trigger_STAT_DOWN);
		    	m_tree->SetBranchAddress("weight_indiv_SF_MU_Trigger_SYST_UP", &weight_indiv_SF_MU_Trigger_SYST_UP);
		    	m_tree->SetBranchAddress("weight_indiv_SF_MU_Trigger_SYST_DOWN", &weight_indiv_SF_MU_Trigger_SYST_DOWN);
		    	m_tree->SetBranchAddress("weight_indiv_SF_MU_ID", &weight_indiv_SF_MU_ID);
		    	m_tree->SetBranchAddress("weight_indiv_SF_MU_ID_STAT_UP", &weight_indiv_SF_MU_ID_STAT_UP);
		    	m_tree->SetBranchAddress("weight_indiv_SF_MU_ID_STAT_DOWN", &weight_indiv_SF_MU_ID_STAT_DOWN);
		    	m_tree->SetBranchAddress("weight_indiv_SF_MU_ID_SYST_UP", &weight_indiv_SF_MU_ID_SYST_UP);
		    	m_tree->SetBranchAddress("weight_indiv_SF_MU_ID_SYST_DOWN", &weight_indiv_SF_MU_ID_SYST_DOWN);
		    	m_tree->SetBranchAddress("weight_indiv_SF_MU_ID_STAT_LOWPT_UP", &weight_indiv_SF_MU_ID_STAT_LOWPT_UP);
		    	m_tree->SetBranchAddress("weight_indiv_SF_MU_ID_STAT_LOWPT_DOWN", &weight_indiv_SF_MU_ID_STAT_LOWPT_DOWN);
		    	m_tree->SetBranchAddress("weight_indiv_SF_MU_ID_SYST_LOWPT_UP", &weight_indiv_SF_MU_ID_SYST_LOWPT_UP);
		    	m_tree->SetBranchAddress("weight_indiv_SF_MU_ID_SYST_LOWPT_DOWN", &weight_indiv_SF_MU_ID_SYST_LOWPT_DOWN);
		    	m_tree->SetBranchAddress("weight_indiv_SF_MU_Isol", &weight_indiv_SF_MU_Isol);
		    	m_tree->SetBranchAddress("weight_indiv_SF_MU_Isol_STAT_UP", &weight_indiv_SF_MU_Isol_STAT_UP);
		    	m_tree->SetBranchAddress("weight_indiv_SF_MU_Isol_STAT_DOWN", &weight_indiv_SF_MU_Isol_STAT_DOWN);
		    	m_tree->SetBranchAddress("weight_indiv_SF_MU_Isol_SYST_UP", &weight_indiv_SF_MU_Isol_SYST_UP);
		    	m_tree->SetBranchAddress("weight_indiv_SF_MU_Isol_SYST_DOWN", &weight_indiv_SF_MU_Isol_SYST_DOWN);
		    	m_tree->SetBranchAddress("weight_indiv_SF_MU_TTVA", &weight_indiv_SF_MU_TTVA);
		    	m_tree->SetBranchAddress("weight_indiv_SF_MU_TTVA_STAT_UP", &weight_indiv_SF_MU_TTVA_STAT_UP);
		    	m_tree->SetBranchAddress("weight_indiv_SF_MU_TTVA_STAT_DOWN", &weight_indiv_SF_MU_TTVA_STAT_DOWN);
		    	m_tree->SetBranchAddress("weight_indiv_SF_MU_TTVA_SYST_UP", &weight_indiv_SF_MU_TTVA_SYST_UP);
		    	m_tree->SetBranchAddress("weight_indiv_SF_MU_TTVA_SYST_DOWN", &weight_indiv_SF_MU_TTVA_SYST_DOWN);
		    	m_tree->SetBranchAddress("weight_photonSF_ID_UP", &weight_photonSF_ID_UP);
		    	m_tree->SetBranchAddress("weight_photonSF_ID_DOWN", &weight_photonSF_ID_DOWN);
		    	m_tree->SetBranchAddress("weight_jvt_UP", &weight_jvt_UP);
		    	m_tree->SetBranchAddress("weight_jvt_DOWN", &weight_jvt_DOWN);
		    	m_tree->SetBranchAddress("weight_bTagSF_77_eigenvars_B_up", &weight_bTagSF_77_eigenvars_B_up);
		    	m_tree->SetBranchAddress("weight_bTagSF_77_eigenvars_C_up", &weight_bTagSF_77_eigenvars_C_up);
		    	m_tree->SetBranchAddress("weight_bTagSF_77_eigenvars_Light_up", &weight_bTagSF_77_eigenvars_Light_up);
		    	m_tree->SetBranchAddress("weight_bTagSF_77_eigenvars_B_down", &weight_bTagSF_77_eigenvars_B_down);
		    	m_tree->SetBranchAddress("weight_bTagSF_77_eigenvars_C_down", &weight_bTagSF_77_eigenvars_C_down);
		    	m_tree->SetBranchAddress("weight_bTagSF_77_eigenvars_Light_down", &weight_bTagSF_77_eigenvars_Light_down);
		    	m_tree->SetBranchAddress("weight_bTagSF_77_extrapolation_up", &weight_bTagSF_77_extrapolation_up);
		    	m_tree->SetBranchAddress("weight_bTagSF_77_extrapolation_down", &weight_bTagSF_77_extrapolation_down);
		    	m_tree->SetBranchAddress("weight_bTagSF_77_extrapolation_from_charm_up", &weight_bTagSF_77_extrapolation_from_charm_up);
		    	m_tree->SetBranchAddress("weight_bTagSF_77_extrapolation_from_charm_down", &weight_bTagSF_77_extrapolation_from_charm_down);
		    	m_tree->SetBranchAddress("weight_bTagSF_Continuous_eigenvars_B_up", &weight_bTagSF_Continuous_eigenvars_B_up);
		    	m_tree->SetBranchAddress("weight_bTagSF_Continuous_eigenvars_C_up", &weight_bTagSF_Continuous_eigenvars_C_up);
		    	m_tree->SetBranchAddress("weight_bTagSF_Continuous_eigenvars_Light_up", &weight_bTagSF_Continuous_eigenvars_Light_up);
		    	m_tree->SetBranchAddress("weight_bTagSF_Continuous_eigenvars_B_down", &weight_bTagSF_Continuous_eigenvars_B_down);
		    	m_tree->SetBranchAddress("weight_bTagSF_Continuous_eigenvars_C_down", &weight_bTagSF_Continuous_eigenvars_C_down);
		    	m_tree->SetBranchAddress("weight_bTagSF_Continuous_eigenvars_Light_down", &weight_bTagSF_Continuous_eigenvars_Light_down);
		    	m_tree->SetBranchAddress("weight_bTagSF_Continuous_extrapolation_from_charm_up", &weight_bTagSF_Continuous_extrapolation_from_charm_up);
		    	m_tree->SetBranchAddress("weight_bTagSF_Continuous_extrapolation_from_charm_down", &weight_bTagSF_Continuous_extrapolation_from_charm_down);
		    	m_tree->SetBranchAddress("randomRunNumber", &randomRunNumber);
		    	m_tree->SetBranchAddress("el_true_type", &el_true_type);
		    	m_tree->SetBranchAddress("el_true_origin", &el_true_origin);
		    	m_tree->SetBranchAddress("el_true_typebkg", &el_true_typebkg);
		    	m_tree->SetBranchAddress("el_true_originbkg", &el_true_originbkg);
		    	m_tree->SetBranchAddress("el_truthAncestor", &el_truthAncestor);
		    	m_tree->SetBranchAddress("el_mc_pid", &el_mc_pid);
		    	m_tree->SetBranchAddress("el_mc_pt", &el_mc_pt);
		    	m_tree->SetBranchAddress("el_mc_eta", &el_mc_eta);
		    	m_tree->SetBranchAddress("el_mc_phi", &el_mc_phi);
		    	m_tree->SetBranchAddress("mu_true_type", &mu_true_type);
		    	m_tree->SetBranchAddress("mu_true_origin", &mu_true_origin);
			m_tree->SetBranchAddress("event_norm2", &event_norm2);
		    }
		    m_tree->SetBranchAddress("event_lumi", &event_lumi);
		    m_tree->SetBranchAddress("event_norm", &event_norm);
		    m_tree->SetBranchAddress("jet_truthflav", &jet_truthflav);
		}
	    }
	    m_tree->SetBranchAddress("eventNumber", &eventNumber);
	    m_tree->SetBranchAddress("runNumber", &runNumber);
	    m_tree->SetBranchAddress("mcChannelNumber", &mcChannelNumber);
	    m_tree->SetBranchAddress("mu", &mu);
	    m_tree->SetBranchAddress("el_pt", &el_pt);
	    m_tree->SetBranchAddress("el_eta", &el_eta);
	    m_tree->SetBranchAddress("el_cl_eta", &el_cl_eta);
	    m_tree->SetBranchAddress("el_phi", &el_phi);
	    m_tree->SetBranchAddress("el_e", &el_e);
	    m_tree->SetBranchAddress("el_charge", &el_charge);
	    m_tree->SetBranchAddress("el_topoetcone20", &el_topoetcone20);
	    m_tree->SetBranchAddress("el_ptvarcone20", &el_ptvarcone20);
	    m_tree->SetBranchAddress("el_d0sig", &el_d0sig);
	    m_tree->SetBranchAddress("el_delta_z0_sintheta", &el_delta_z0_sintheta);
	    m_tree->SetBranchAddress("mu_pt", &mu_pt);
	    m_tree->SetBranchAddress("mu_eta", &mu_eta);
	    m_tree->SetBranchAddress("mu_phi", &mu_phi);
	    m_tree->SetBranchAddress("mu_e", &mu_e);
	    m_tree->SetBranchAddress("mu_charge", &mu_charge);
	    m_tree->SetBranchAddress("mu_topoetcone20", &mu_topoetcone20);
	    m_tree->SetBranchAddress("mu_ptvarcone30", &mu_ptvarcone30);
	    m_tree->SetBranchAddress("mu_d0sig", &mu_d0sig);
	    m_tree->SetBranchAddress("mu_delta_z0_sintheta", &mu_delta_z0_sintheta);
	    m_tree->SetBranchAddress("ph_pt", &ph_pt);
	    m_tree->SetBranchAddress("ph_eta", &ph_eta);
	    m_tree->SetBranchAddress("ph_phi", &ph_phi);
	    m_tree->SetBranchAddress("ph_e", &ph_e);
	    m_tree->SetBranchAddress("ph_iso", &ph_iso);
	    m_tree->SetBranchAddress("selph_index1", &selph_index1);
	    m_tree->SetBranchAddress("jet_pt",  &jet_pt);
	    m_tree->SetBranchAddress("jet_eta", &jet_eta);
	    m_tree->SetBranchAddress("jet_phi", &jet_phi);
	    m_tree->SetBranchAddress("jet_e", &jet_e);
	    m_tree->SetBranchAddress("jet_mv2c00", &jet_mv2c00);
	    m_tree->SetBranchAddress("jet_mv2c10", &jet_mv2c10);
	    m_tree->SetBranchAddress("jet_mv2c20", &jet_mv2c20);
	    m_tree->SetBranchAddress("jet_ip3dsv1", &jet_ip3dsv1);
	    m_tree->SetBranchAddress("jet_jvt", &jet_jvt);
	    m_tree->SetBranchAddress("jet_isbtagged_77", &jet_isbtagged_77);
	    m_tree->SetBranchAddress("event_mwt", &event_mwt);
	    m_tree->SetBranchAddress("met_met", &met_met);
	    m_tree->SetBranchAddress("met_phi", &met_phi);
	    if (m_region == "EF1") {
		m_tree->SetBranchAddress("el_trigMatch_HLT_e60_lhmedium", &el_trigMatch_HLT_e60_lhmedium);
		m_tree->SetBranchAddress("el_trigMatch_HLT_e60_lhmedium_nod0", &el_trigMatch_HLT_e60_lhmedium_nod0);
		m_tree->SetBranchAddress("el_trigMatch_HLT_e24_lhmedium_L1EM20VH", &el_trigMatch_HLT_e24_lhmedium_L1EM20VH);
		m_tree->SetBranchAddress("el_trigMatch_HLT_e26_lhtight_nod0_ivarloose", &el_trigMatch_HLT_e26_lhtight_nod0_ivarloose);
		m_tree->SetBranchAddress("el_trigMatch_HLT_e120_lhloose", &el_trigMatch_HLT_e120_lhloose);
		m_tree->SetBranchAddress("el_trigMatch_HLT_e140_lhloose_nod0", &el_trigMatch_HLT_e140_lhloose_nod0);
		//m_tree->SetBranchAddress("mu_trigMatch_HLT_mu50", &mu_trigMatch_HLT_mu50);
		//m_tree->SetBranchAddress("mu_trigMatch_HLT_mu20_iloose_L1MU15", &mu_trigMatch_HLT_mu20_iloose_L1MU15);
	        if (m_subregion == "zee") {
	    	m_tree->SetBranchAddress("zee_2015", &zee_2015);
	        	m_tree->SetBranchAddress("zee_2016", &zee_2016);
	        } else if (m_subregion == "zeg") { 
	    	m_tree->SetBranchAddress("zeg_2015", &zeg_2015);
	        	m_tree->SetBranchAddress("zeg_2016", &zeg_2016);
	        } else if (m_subregion == "ttel_ee") {
	    	m_tree->SetBranchAddress("ttel_ee_2015", &ttel_ee_2015);
	        	m_tree->SetBranchAddress("ttel_ee_2016", &ttel_ee_2016);
	        } else if (m_subregion == "ttel_mue") {
	    	m_tree->SetBranchAddress("ttel_mue_2015", &ttel_mue_2015);
	        	m_tree->SetBranchAddress("ttel_mue_2016", &ttel_mue_2016);
	        }
	    }
            if (m_region == "CR1" || m_region == "VR" || m_region == "SR1") {
    	        m_tree->SetBranchAddress("ejets_2015", &ejets_2015);
    	        m_tree->SetBranchAddress("mujets_2015", &mujets_2015);
    	        m_tree->SetBranchAddress("ee_2015", &ee_2015);
                m_tree->SetBranchAddress("mumu_2015", &mumu_2015);
                m_tree->SetBranchAddress("emu_2015", &emu_2015);
    	        m_tree->SetBranchAddress("ejets_2016", &ejets_2016);
    	        m_tree->SetBranchAddress("mujets_2016", &mujets_2016);
    	        m_tree->SetBranchAddress("ee_2016", &ee_2016);
                m_tree->SetBranchAddress("mumu_2016", &mumu_2016);
                m_tree->SetBranchAddress("emu_2016", &emu_2016);
            }
            if (m_region == "SS") {
    	        m_tree->SetBranchAddress("eeSS_2015", &eeSS_2015);
                m_tree->SetBranchAddress("mumuSS_2015", &mumuSS_2015);
                m_tree->SetBranchAddress("emuSS_2015", &emuSS_2015);
    	        m_tree->SetBranchAddress("eeSS_2016", &eeSS_2016);
                m_tree->SetBranchAddress("mumuSS_2016", &mumuSS_2016);
                m_tree->SetBranchAddress("emuSS_2016", &emuSS_2016);
            }
	    if (m_type == "QCD") {
	        m_tree->SetBranchAddress("HLT_mu26_ivarmedium", &HLT_mu26_ivarmedium);
   	        m_tree->SetBranchAddress("HLT_mu20_iloose_L1MU15", &HLT_mu20_iloose_L1MU15);
   	        m_tree->SetBranchAddress("HLT_e60_lhmedium_nod0", &HLT_e60_lhmedium_nod0);
   	        m_tree->SetBranchAddress("HLT_mu50", &HLT_mu50);
   	        m_tree->SetBranchAddress("HLT_e26_lhvloose_nod0_L1EM20VH_6j15noL1", &HLT_e26_lhvloose_nod0_L1EM20VH_6j15noL1);
   	        m_tree->SetBranchAddress("HLT_e24_lhmedium_L1EM20VH", &HLT_e24_lhmedium_L1EM20VH);
   	        m_tree->SetBranchAddress("HLT_e140_lhloose_nod0", &HLT_e140_lhloose_nod0);
   	        m_tree->SetBranchAddress("HLT_e60_lhmedium", &HLT_e60_lhmedium);
   	        m_tree->SetBranchAddress("HLT_e26_lhtight_nod0_ivarloose", &HLT_e26_lhtight_nod0_ivarloose);
   	        m_tree->SetBranchAddress("HLT_e120_lhloose", &HLT_e120_lhloose);
   	        m_tree->SetBranchAddress("HLT_e26_lhvloose_nod0_L1EM20VH_5j15noL1", &HLT_e26_lhvloose_nod0_L1EM20VH_5j15noL1);
   	        m_tree->SetBranchAddress("HLT_e26_lhvloose_nod0_L1EM20VH_4j20noL1", &HLT_e26_lhvloose_nod0_L1EM20VH_4j20noL1);
   	        m_tree->SetBranchAddress("HLT_e24_lhmedium_nod0_L1EM18VH", &HLT_e24_lhmedium_nod0_L1EM18VH);
   	        m_tree->SetBranchAddress("HLT_e26_lhvloose_nod0_L1EM20VH", &HLT_e26_lhvloose_nod0_L1EM20VH);
   	        m_tree->SetBranchAddress("HLT_mu24", &HLT_mu24);
   	        m_tree->SetBranchAddress("HLT_e26_lhvloose_nod0_L1EM20VH_3j20noL1", &HLT_e26_lhvloose_nod0_L1EM20VH_3j20noL1);
	    }

	    if (m_type == "QCD") {
	        m_tree->SetBranchAddress("weight_mm_ejets", &weight_mm_ejets);
	        m_tree->SetBranchAddress("weight_mm_mujets", &weight_mm_mujets);
	        m_tree->SetBranchAddress("weights_mm_ejets", &weights_mm_ejets);
	        m_tree->SetBranchAddress("weights_mm_mujets", &weights_mm_mujets);
	    }

            m_tree->SetBranchAddress("ph_topoetcone20", &ph_topoetcone20);
            m_tree->SetBranchAddress("ph_topoetcone30", &ph_topoetcone30);
            m_tree->SetBranchAddress("ph_topoetcone40", &ph_topoetcone40);
            m_tree->SetBranchAddress("ph_ptcone20", &ph_ptcone20);
            m_tree->SetBranchAddress("ph_ptcone30", &ph_ptcone30);
            m_tree->SetBranchAddress("ph_ptcone40", &ph_ptcone40);
            m_tree->SetBranchAddress("ph_ptvarcone20", &ph_ptvarcone20);
            m_tree->SetBranchAddress("ph_ptvarcone30", &ph_ptvarcone30);
            m_tree->SetBranchAddress("ph_ptvarcone40", &ph_ptvarcone40);
            m_tree->SetBranchAddress("ph_isoFCTCO", &ph_isoFCTCO);
            m_tree->SetBranchAddress("ph_isoFCT", &ph_isoFCT);
            m_tree->SetBranchAddress("ph_isoFCL", &ph_isoFCL);
            m_tree->SetBranchAddress("ph_isTight", &ph_isTight);
            m_tree->SetBranchAddress("ph_isLoose", &ph_isLoose);
	    m_tree->SetBranchAddress("ph_isHadronFakeFailedDeltaE", &ph_isHadronFakeFailedDeltaE);
   	    m_tree->SetBranchAddress("ph_isHadronFakeFailedFside", &ph_isHadronFakeFailedFside);
   	    m_tree->SetBranchAddress("ph_isHadronFakeFailedWs3", &ph_isHadronFakeFailedWs3);
   	    m_tree->SetBranchAddress("ph_isHadronFakeFailedERatio", &ph_isHadronFakeFailedERatio);
            m_tree->SetBranchAddress("ph_truthType", &ph_truthType);
            m_tree->SetBranchAddress("ph_truthOrigin", &ph_truthOrigin);
            m_tree->SetBranchAddress("ph_truthAncestor", &ph_truthAncestor);
            m_tree->SetBranchAddress("ph_HFT_MVA", &ph_HFT_MVA);
            m_tree->SetBranchAddress("ph_mc_pid", &ph_mc_pid);
            m_tree->SetBranchAddress("ph_mc_pt", &ph_mc_pt);
            m_tree->SetBranchAddress("ph_mc_eta", &ph_mc_eta);
            m_tree->SetBranchAddress("ph_mc_phi", &ph_mc_phi);
            m_tree->SetBranchAddress("ph_mc_barcode", &ph_mc_barcode);
	    m_tree->SetBranchAddress("ph_rhad1", &ph_rhad1);
	    m_tree->SetBranchAddress("ph_rhad", &ph_rhad);
	    m_tree->SetBranchAddress("ph_reta", &ph_reta);
	    m_tree->SetBranchAddress("ph_weta2", &ph_weta2);
	    m_tree->SetBranchAddress("ph_rphi", &ph_rphi);
	    m_tree->SetBranchAddress("ph_ws3", &ph_ws3);
	    m_tree->SetBranchAddress("ph_wstot", &ph_wstot);
	    m_tree->SetBranchAddress("ph_fracm", &ph_fracm);
	    m_tree->SetBranchAddress("ph_deltaE", &ph_deltaE);
	    m_tree->SetBranchAddress("ph_eratio", &ph_eratio);
	    m_tree->SetBranchAddress("ph_emaxs1", &ph_emaxs1);
	    m_tree->SetBranchAddress("ph_f1", &ph_f1);
	    m_tree->SetBranchAddress("ph_e277", &ph_e277);
            m_tree->SetBranchAddress("ph_OQ", &ph_OQ);
            m_tree->SetBranchAddress("ph_author", &ph_author);
            m_tree->SetBranchAddress("ph_conversionType", &ph_conversionType);
            m_tree->SetBranchAddress("ph_caloEta", &ph_caloEta);
            m_tree->SetBranchAddress("ph_drlept", &ph_drlept);
            m_tree->SetBranchAddress("ph_mgammalept", &ph_mgammalept);
	    m_tree->SetBranchAddress("ph_SF_eff", &ph_SF_eff);
	    m_tree->SetBranchAddress("ph_SF_effUP", &ph_SF_effUP);
   	    m_tree->SetBranchAddress("ph_SF_effDO", &ph_SF_effDO);
   	    m_tree->SetBranchAddress("ph_SF_iso", &ph_SF_iso);
   	    m_tree->SetBranchAddress("ph_SF_lowisoUP", &ph_SF_lowisoUP);
   	    m_tree->SetBranchAddress("ph_SF_lowisoDO", &ph_SF_lowisoDO);
   	    m_tree->SetBranchAddress("ph_SF_trkisoUP", &ph_SF_trkisoUP);
   	    m_tree->SetBranchAddress("ph_SF_trkisoDO", &ph_SF_trkisoDO);
            m_tree->SetBranchAddress("ph_mgammaleptlept", &ph_mgammaleptlept);
            m_tree->SetBranchAddress("ph_mcel_dr", &ph_mcel_dr);
            m_tree->SetBranchAddress("ph_mcel_pt", &ph_mcel_pt);
            m_tree->SetBranchAddress("ph_mcel_eta", &ph_mcel_eta);
            m_tree->SetBranchAddress("ph_mcel_phi", &ph_mcel_phi);
            m_tree->SetBranchAddress("ph_dralljet", &ph_dralljet);
            m_tree->SetBranchAddress("lepton_type", &lepton_type);
            if (m_region == "EF1") m_tree->SetBranchAddress("el_isoGradient", &el_isoGradient);
            m_tree->SetBranchAddress("event_HT", &event_HT);
            m_tree->SetBranchAddress("event_mll", &event_mll);
            m_tree->SetBranchAddress("mcph_pt", &mcph_pt);
            m_tree->SetBranchAddress("mcph_eta", &mcph_eta);
            m_tree->SetBranchAddress("mcph_phi", &mcph_phi);
            m_tree->SetBranchAddress("mcph_ancestor", &mcph_ancestor);
            m_tree->SetBranchAddress("event_njets", &event_njets);
            m_tree->SetBranchAddress("event_nbjets77", &event_nbjets77);
            m_tree->SetBranchAddress("event_nbjets70", &event_nbjets70);
            m_tree->SetBranchAddress("event_ngoodphotons", &event_ngoodphotons);
            m_tree->SetBranchAddress("event_photonorigin", &event_photonorigin);
            m_tree->SetBranchAddress("event_photonoriginTA", &event_photonoriginTA);
	}
    }

    if (m_type == "Upgrade" && m_use_lumi_weight) {
	if (oneevt) {
	    string dsid = "";
    	    if (m_filename.find("user.finelli.mc15_14TeV.",0) != string::npos) {
    	        int n1 = m_filename.find("user.finelli.",0);
    	        n1 += 24;
    	        dsid = m_filename.substr(n1, 6);
    	    } else if (m_filename.find("user.yili.",0) != string::npos) {
    	        int n1 = m_filename.find("user.yili.",0);
    	        n1 += 10;
    	        dsid = m_filename.substr(n1, 6);
    	    } else if (m_filename.find("user.finelli.",0) != string::npos) {
    	        int n1 = m_filename.find("user.finelli.",0);
    	        n1 += 13;
    	        dsid = m_filename.substr(n1, 6);
    	    } else if (m_filename.find("user.cescobar.",0) != string::npos) {
    	        int n1 = m_filename.find("user.cescobar.",0);
    	        n1 += 14;
    	        dsid = m_filename.substr(n1, 6);
    	    } else if (m_filename.find("user.adurglis.",0) != string::npos) {
    	        int n1 = m_filename.find("user.adurglis.",0);
    	        n1 += 14;
    	        dsid = m_filename.substr(n1, 6);
    	    }
	    bool registered = false;
	    int idindex = -1;
	    for (int i = 0; i < m_ids.size(); i++) {
		if (m_ids.at(i) == dsid) {
		    registered = true;
		    idindex = i;
		    break;
		}
	    }
	    if (!registered) {
		m_ids.push_back(dsid);
		idindex = m_ids.size();
	    }

    	    m_tree_sumWeight = (TTree*)m_file->Get("sumWeights");
    	    if (!m_tree_sumWeight) {
    		m_lg->Warn("s", "Cannot open sumWeight tree!");
    		return 0;
    	    }
    	    m_totalEventsWeighted = 0;
	    m_totalEventsWeighted_mc_generator_weights = 0;
    	    m_tree_sumWeight->SetMakeClass(1);
	    string sumname;
	    if (m_process == "Wjets") {
		sumname = "totalEventsWeighted_mc_generator_weights";
		m_tree_sumWeight->SetBranchAddress(sumname.c_str(), &m_totalEventsWeighted_mc_generator_weights);
	    }
	    else {
		sumname = "totalEventsWeighted";
		m_tree_sumWeight->SetBranchAddress(sumname.c_str(), &m_totalEventsWeighted);
	    }
    	    int totentry = m_tree_sumWeight->GetEntriesFast();
	    double totevt = 0;
	    for (int ientry = 0; ientry < totentry; ientry++) {
		m_tree_sumWeight->GetEntry(ientry);
		if (m_process == "Wjets") {
		  totevt += m_totalEventsWeighted_mc_generator_weights->at(0);
		} else {
		  totevt += m_totalEventsWeighted;
		}
	    }
	
	    if (!registered) {
		m_sumws.push_back(totevt);
	    } else {
		m_sumws.at(idindex) += totevt;
	    }

	    double xsec = 0;
    	    ifstream ifile_xsec;
    	    ifile_xsec.open("/afs/cern.ch/work/y/yili/private/Analysis/AnalysisTop/TopUpgradeGrid/XSection-MC15-14TeV.data");
    	    string line;
    	    bool found_xsec = false;
    	    while (getline(ifile_xsec,line)) {
    	        if (line == "") continue;
    	        if (line.at(0) == ' ') continue;
    		if (line.find("#",0) != string::npos) continue;
    	        istringstream iss(line);
    	        vector<string> words;
    	        for (string word; iss >> word;) {
    	            words.push_back(word);
    	        }
    	        if (words.at(0) == dsid) {
		if (m_process == "TTBar") {
		    xsec = atof(words.at(1).c_str()) * 1.139;
		} else {
		    xsec = atof(words.at(1).c_str()) * atof(words.at(2).c_str());
		}
    	    	found_xsec = true;
    	    	//m_lg->Info("sss", words.at(0).c_str(), words.at(1).c_str(), words.at(2).c_str());
    	    	break;
    	        }
    	    }
    	    
    	    if (!found_xsec){
    	        m_lg->Err("sd", "can't find xsection info for ->", dsid.c_str());
    	        exit(-1);
    	    }

    	    if (m_process == "Signal") {
    	        if (m_subregion == "ejets" || m_subregion == "mujets") xsec *= 1.30;
    	        if (m_subregion == "ee" || m_subregion == "emu" || m_subregion == "mumu") xsec *= 1.44;
    	    }

	    if (!registered) {
		xsec *= 1000.;
		m_xsecs.push_back(xsec);
	    }
	} else {
	    string dsid = "";
    	    if (m_filename.find("user.finelli.mc15_14TeV.",0) != string::npos) {
    	        int n1 = m_filename.find("user.finelli.",0);
    	        n1 += 24;
    	        dsid = m_filename.substr(n1, 6);
    	    } else if (m_filename.find("user.finelli.",0) != string::npos) {
    	        int n1 = m_filename.find("user.finelli.",0);
    	        n1 += 13;
    	        dsid = m_filename.substr(n1, 6);
    	    } else if (m_filename.find("user.yili.",0) != string::npos) {
    	        int n1 = m_filename.find("user.yili.",0);
    	        n1 += 10;
    	        dsid = m_filename.substr(n1, 6);
    	    } else if (m_filename.find("user.cescobar.",0) != string::npos) {
    	        int n1 = m_filename.find("user.cescobar.",0);
    	        n1 += 14;
    	        dsid = m_filename.substr(n1, 6);
    	    } else if (m_filename.find("user.adurglis.",0) != string::npos) {
    	        int n1 = m_filename.find("user.adurglis.",0);
    	        n1 += 14;
    	        dsid = m_filename.substr(n1, 6);
    	    }
	    m_current_idindex = -1;
	    for (int i = 0; i < m_ids.size(); i++) {
		if (m_ids.at(i) == dsid) {
		    m_current_idindex = i;
		    break;
		}
	    }
	    if (m_current_idindex == -1) {
		m_lg->Err("ss", "can't find ->", dsid.c_str());
		exit(-1);
	    }

	    double totevt = m_sumws.at(m_current_idindex);
	    double xsec = m_xsecs.at(m_current_idindex);
    	    double mc_lumi = totevt/xsec;

    	    double data_lumi = 3000.;
    	    if (mc_lumi != 0) m_lumi_weight = data_lumi/mc_lumi;
    	    else m_lumi_weight = 0;
	}
    }

    m_lg->Info("s", "File initialized.");
}

void SampleAnalyzor::Loop(bool oneevt) {
    m_lg->NewLine();

    m_lg->Info("s", "START loop.");
    m_lg->Info("sd", "Number of systematics to run ->", m_variations.size());

    m_random = new TRandom3();

    if (m_do_klfitter) {
	//m_klfitter = new KLFitter::Fitter();
	//m_klfitter_lt = new KLFitter::Fitter();
	//m_klfitter_ht = new KLFitter::Fitter();
	//m_klfitter_lW = new KLFitter::Fitter();
	//m_klfitter_hW = new KLFitter::Fitter();

	//m_detector = new KLFitter::DetectorAtlas_8TeV("/afs/cern.ch/work/y/yili/private/MySVN/Code/KLFitter/data/transferfunctions/8TeV/ttbar/mc12_LCJets_v1/"); 
	//if (!m_klfitter->SetDetector(m_detector)) {
	//    m_lg->Err("s", "Can't set detector for KLFitter!");
	//    exit(-1);
	//}
	//if (!m_klfitter_lt->SetDetector(m_detector)) {
	//    m_lg->Err("s", "Can't set detector for KLFitter!");
	//    exit(-1);
	//}
	//if (!m_klfitter_ht->SetDetector(m_detector)) {
	//    m_lg->Err("s", "Can't set detector for KLFitter!");
	//    exit(-1);
	//}
	//if (!m_klfitter_lW->SetDetector(m_detector)) {
	//    m_lg->Err("s", "Can't set detector for KLFitter!");
	//    exit(-1);
	//}
	//if (!m_klfitter_hW->SetDetector(m_detector)) {
	//    m_lg->Err("s", "Can't set detector for KLFitter!");
	//    exit(-1);
	//}

	//m_likelihood = new KLFitter::LikelihoodTopLeptonJets(); 
	//m_likelihood_lt = new KLFitter::LikelihoodTopLeptonLPhotonJets(); 
	//m_likelihood_ht = new KLFitter::LikelihoodTopLeptonHPhotonJets(); 
	//m_likelihood_lW = new KLFitter::LikelihoodTopLeptonLWPhotonJets(); 
	//m_likelihood_hW = new KLFitter::LikelihoodTopLeptonHWPhotonJets(); 
	//if (m_subregion == "ejets") {
	//    m_likelihood->SetLeptonType(KLFitter::LikelihoodTopLeptonJets::kElectron);
	//    m_likelihood_lt->SetLeptonType(KLFitter::LikelihoodTopLeptonLPhotonJets::kElectron);
	//    m_likelihood_ht->SetLeptonType(KLFitter::LikelihoodTopLeptonHPhotonJets::kElectron);
	//    m_likelihood_lW->SetLeptonType(KLFitter::LikelihoodTopLeptonLWPhotonJets::kElectron);
	//    m_likelihood_hW->SetLeptonType(KLFitter::LikelihoodTopLeptonHWPhotonJets::kElectron);
	//} else if (m_subregion == "mujets") {
	//    m_likelihood->SetLeptonType(KLFitter::LikelihoodTopLeptonJets::kMuon);
	//    m_likelihood_lt->SetLeptonType(KLFitter::LikelihoodTopLeptonLPhotonJets::kMuon);
	//    m_likelihood_ht->SetLeptonType(KLFitter::LikelihoodTopLeptonHPhotonJets::kMuon);
	//    m_likelihood_lW->SetLeptonType(KLFitter::LikelihoodTopLeptonLWPhotonJets::kMuon);
	//    m_likelihood_hW->SetLeptonType(KLFitter::LikelihoodTopLeptonHWPhotonJets::kMuon);
	//} else {
	//    m_lg->Err("s", "Channels other than e/mu+j are not supported for KLFitter yet!");
	//    exit(-1);
	//}

	//m_likelihood->SetFlagTopMassFixed(m_kl_fixtop); m_likelihood->PhysicsConstants()->SetMassTop(172.5);
	//m_likelihood_lt->SetFlagTopMassFixed(m_kl_fixtop); m_likelihood_lt->PhysicsConstants()->SetMassTop(172.5);
	//m_likelihood_ht->SetFlagTopMassFixed(m_kl_fixtop); m_likelihood_ht->PhysicsConstants()->SetMassTop(172.5);
	//m_likelihood_lW->SetFlagTopMassFixed(m_kl_fixtop); m_likelihood_lW->PhysicsConstants()->SetMassTop(172.5);
	//m_likelihood_hW->SetFlagTopMassFixed(m_kl_fixtop); m_likelihood_hW->PhysicsConstants()->SetMassTop(172.5);
	//if (!m_kl_bveto) {
	//    m_likelihood->SetBTagging(KLFitter::LikelihoodBase::kNotag);
	//    m_likelihood_lt->SetBTagging(KLFitter::LikelihoodBase::kNotag);
	//    m_likelihood_ht->SetBTagging(KLFitter::LikelihoodBase::kNotag);
	//    m_likelihood_lW->SetBTagging(KLFitter::LikelihoodBase::kNotag);
	//    m_likelihood_hW->SetBTagging(KLFitter::LikelihoodBase::kNotag);
	//} else {
	//    m_likelihood->SetBTagging(KLFitter::LikelihoodBase::kVetoNoFit);
	//    m_likelihood_lt->SetBTagging(KLFitter::LikelihoodBase::kVetoNoFit);
	//    m_likelihood_ht->SetBTagging(KLFitter::LikelihoodBase::kVetoNoFit);
	//    m_likelihood_lW->SetBTagging(KLFitter::LikelihoodBase::kVetoNoFit);
	//    m_likelihood_hW->SetBTagging(KLFitter::LikelihoodBase::kVetoNoFit);
	//}
	//m_klfitter->SetLikelihood(m_likelihood);
	//m_klfitter_lt->SetLikelihood(m_likelihood_lt);
	//m_klfitter_ht->SetLikelihood(m_likelihood_ht);
	//m_klfitter_lW->SetLikelihood(m_likelihood_lW);
	//m_klfitter_hW->SetLikelihood(m_likelihood_hW);
    }
    
    for (int itree = 0; itree < m_variations.size(); itree++) {
        m_variation = m_variations.at(itree);
        m_treename = m_treenames.at(itree);
        
        m_lg->NewLine();
        m_lg->AddShift();
        m_lg->Info("sdss", "Do systematics", itree+1, "->", m_variation.c_str());
        InitHistogram();
    
        int badweight = 0;
        n_cuts_end = 0;
        m_lg->Info("sd", "Number of files to run ->", m_filenames.size());
	m_ifile = 0;
        for (int ifile = 0; ifile < m_filenames.size(); ifile++) {
            m_filename = m_filenames.at(ifile);
            m_lg->NewLine();
	    m_lg->AddShift();
            m_lg->Info("sdss", "Use file", ifile+1, "->", m_filename.c_str());
    
            if (!InitFile(oneevt)) {
		m_lg->ReduShift();
		continue;
	    }
	    if (m_debug) m_lg->Info("s", "GetEntriesFast");
            Long64_t nentries = m_tree->GetEntriesFast();
	    if (m_debug) m_lg->Info("s", "GotEntriesFast");
            if (m_test) nentries = 500;
	    if (m_1pct) nentries /= 100.;
	    if (m_10pct) nentries /= 10.;
	    if (m_nevt != -1) {nentries = m_nevt < nentries ? m_nevt : nentries;}
	    if (m_nfile != -1) {
		if (ifile+1 > m_nfile) {
		    break;
		}
	    }
	    if (oneevt) nentries = 0;
	    for (Long64_t jentry=0; jentry<nentries; jentry++) {
		if (m_notify) if (jentry % 10000 == 0) m_lg->Info("sds", "This is the", jentry, "events.");
		
		if (m_debug) m_lg->Info("s", "GetEntry");
		m_tree->GetEntry(jentry);
		if (m_debug) m_lg->Info("s", "GetEntryAfter");
		InitEvent();
		
		if (weight != weight) {badweight++; continue;}
		
		int cut_cnt = n_cuts_start;
		FillCutflow(cut_cnt, "Region");

		if (m_debug) m_lg->Info("s", "Region selection");
		if (m_region == "CR1" || m_region == "VR" || m_region == "SR1") {
		    if (m_subregion == "ejets") {
		        if (!(ejets_2015 || ejets_2016)) continue;
		    } else if (m_subregion == "mujets") {
		        if (!(mujets_2015 || mujets_2016)) continue;
		    } else if (m_subregion == "emu") {
		        if (!(emu_2015 || emu_2016)) continue;
		    } else if (m_subregion == "ee") {
		        if (!(ee_2015 || ee_2016)) continue;
		    } else if (m_subregion == "mumu") {
		        if (!(mumu_2015 || mumu_2016)) continue;
		    }
		} else if (m_region == "SS") {
		    if (m_subregion == "emu") {
		        if (!(emuSS_2015 || emuSS_2016)) continue;
		    } else if (m_subregion == "ee") {
		        if (!(eeSS_2015 || eeSS_2016)) continue;
		    } else if (m_subregion == "mumu") {
		        if (!(mumuSS_2015 || mumuSS_2016)) continue;
		    }
		} else if (m_region == "EF1") {
		    if (m_subregion == "zeg") {
		        if (!(zeg_2015 || zeg_2016)) continue;
		    } else if (m_subregion == "zee") {
		        if (!(zee_2015 || zee_2016)) continue;
		    } else if (m_subregion == "ttel_ee") {
		        if (!(ttel_ee_2015 || ttel_ee_2016)) continue;
		    } else if (m_subregion == "ttel_mue") {
		        if (!(ttel_mue_2015 || ttel_mue_2016)) continue;
		    }
		} else if (m_region == "UR") {
		    if (m_subregion == "ejets") {
		        if (!(ejets_gamma_basic && n_el == 1 && n_mu == 0)) continue;
		    } else if (m_subregion == "mujets") {
		        if (!(mujets_gamma_basic && n_mu == 1 && n_el == 0)) continue;
		    } else if (m_subregion == "ee") {
		        if (!(ee_gamma_basic && n_el == 2 && n_mu == 0)) continue;
		    } else if (m_subregion == "emu") {
		        if (!(emu_gamma_basic && n_el == 1 && n_mu == 1)) continue;
		    } else if (m_subregion == "mumu") {
		        if (!(mumu_gamma_basic && n_mu == 2 && n_el == 0)) continue;
		    }
		}
		
		if (m_ttsg) { // SG
		    if (!((lepton_type->at(0) == 0 && lepton_type->at(1) != 0) || (lepton_type->at(0) != 0 && lepton_type->at(1) == 0))) continue;
		}
		if (m_ttdi) { // dilepton
		    if (m_ttee) {
		        if (!(abs(lepton_type->at(0)) * abs(lepton_type->at(1)) == 121)) continue;
		    } else if (m_ttem) {
		        if (!(abs(lepton_type->at(0)) * abs(lepton_type->at(1)) == 143)) continue;
		    } else if (m_ttet) {
		        if (!(abs(lepton_type->at(0)) * abs(lepton_type->at(1)) == 165)) continue;
		    } else {
		        if (!(lepton_type->at(0) * lepton_type->at(1) != 0)) continue;
		    }
		}
		if (m_notau) { // dilepton
		    if (fabs(lepton_type->at(0)) == 15 || fabs(lepton_type->at(1)) == 15) continue;
		}

		if (m_type == "QCD") {
		    if (m_subregion == "ejets") {
			if (ejets_2015) {
			    if (!(HLT_e24_lhmedium_L1EM20VH || HLT_e60_lhmedium || HLT_e120_lhloose)) continue;
			} else if (ejets_2016) {
			    if (m_PSQCD) {
				if (!((HLT_e24_lhmedium_nod0_L1EM18VH && leadlep_pt < 61000.) || ((HLT_e60_lhmedium_nod0  || HLT_e140_lhloose_nod0) && leadlep_pt > 61000.))) continue;
			    } else {
				if (!((HLT_e26_lhtight_nod0_ivarloose && leadlep_pt < 61000.) || ((HLT_e60_lhmedium_nod0  || HLT_e140_lhloose_nod0) && leadlep_pt > 61000.))) continue;
			    }
			}
		    } else if (m_subregion == "mujets") {
			if (mujets_2015) {
			    if (!(HLT_mu20_iloose_L1MU15 || HLT_mu50)) continue;
			} else if (mujets_2016) {
			    if (m_PSQCD) {
				if (!((HLT_mu24 && leadlep_pt < 51000.) || (HLT_mu50 && leadlep_pt > 51000.))) continue;
			    } else {
				if (!((HLT_mu26_ivarmedium && leadlep_pt < 51000.)|| (HLT_mu50 && leadlep_pt > 51000.))) continue;
			    }
			}
		    }
		}
		
		cut_cnt++;
		FillCutflow(cut_cnt, "SubRegion");

		//if (m_process.find("CNp",0) != string::npos && m_process.find("CCNp",0) == string::npos) {
		//    if (hfor != 2) continue;
		//} else if (m_process.find("CCNp",0) != string::npos) {
		//    if (hfor != 1) continue;
		//} else if (m_process.find("BNp",0) != string::npos) {
		//    if (hfor != 0) continue;
		//} else {
		//    if (hfor != 3) continue;
		//}
		    //cut_cnt++;
		    //FillCutflow(cut_cnt, "HFOR");
		if (m_debug) m_lg->Info("s", "Main selection");
		
		if (m_selection == "CutTest") {
		    if (m_do_klfitter) KLFit();
		} else if (m_selection == "CutVR") {
		    if (!(n_jet >= 4)) continue;
		    cut_cnt++;
		    FillCutflow(cut_cnt, "NjetGeq4");

		    if (!(n_bjet >= 1)) continue;
		    cut_cnt++;
		    FillCutflow(cut_cnt, "NbjetGeq1");

		    if (!(n_bjet >= 2)) continue;
		    cut_cnt++;
		    FillCutflow(cut_cnt, "NbjetGeq2");

		} else if (m_selection == "CutQCDCR") {
		    if (!(n_jet >= 1)) continue;
		    cut_cnt++;
		    FillCutflow(cut_cnt, "Njet>=1");

		    if (!(n_jet <= 3)) continue;
		    cut_cnt++;
		    FillCutflow(cut_cnt, "Njet<=3");

		    if (!(mwt < 20000.)) continue;
		    cut_cnt++;
		    FillCutflow(cut_cnt, "MWT<20");

		    if (!(mwt + met < 60000.)) continue;
		    cut_cnt++;
		    FillCutflow(cut_cnt, "MWT+MET<20");

		    if (!(event_ngoodphotons == 1)) continue;
		    cut_cnt++;
		    FillCutflow(cut_cnt, "OneGoodPh");

		    if (m_do_phmatch) {
			if (!PhMatch()) continue;
		    }
		    cut_cnt++;
		    FillCutflow(cut_cnt, "PhMatch");
		} else if (m_selection == "CutTest1") {
		    if (!(event_ngoodphotons == 1)) continue;
		    cut_cnt++;
		    FillCutflow(cut_cnt, "OneGoodPh");
		    
		    if (m_do_phmatch) {
		        if (!PhMatch()) continue;
		    }
		    cut_cnt++;
		    FillCutflow(cut_cnt, "PhMatch");
			
		    if (!(n_bjet == 1)) continue;
		    cut_cnt++;
		    if (m_type != "Data" && m_type != "QCD") weight *= weight_btag;
		    FillCutflow(cut_cnt, "Nbjet1");
			
		    if (!(ph_drlept->at(selph_index1) > 1.0)) continue;
		    cut_cnt++;
		    FillCutflow(cut_cnt, "DrPhLep1.0");
		} else if (m_selection == "CutTest2") {
		    if (!(event_ngoodphotons == 1)) continue;
		    cut_cnt++;
		    FillCutflow(cut_cnt, "OneGoodPh");
		    
		    if (m_do_phmatch) {
		        if (!PhMatch()) continue;
		    }
		    cut_cnt++;
		    FillCutflow(cut_cnt, "PhMatch");
			
		    if (!(n_bjet == 0)) continue;
		    cut_cnt++;
		    if (m_type != "Data" && m_type != "QCD") weight *= weight_btag;
		    FillCutflow(cut_cnt, "Nbjet1");
			
		    if (!(ph_drlept->at(selph_index1) > 1.0)) continue;
		    cut_cnt++;
		    FillCutflow(cut_cnt, "DrPhLep1.0");
		} else if (m_selection == "CutTest3") {
		    if (m_subregion == "ejets" || m_subregion == "mujets") {
			if (!(n_jet >= 4)) continue;
			cut_cnt++;
			FillCutflow(cut_cnt, "NjetGeq4");
		    } else if (m_subregion == "ee" || m_subregion == "emu" || m_subregion == "mumu") {
			if (!(n_jet >= 2)) continue;
			cut_cnt++;
			FillCutflow(cut_cnt, "NjetGeq2");
		    }

		    if (!(n_bjet >= 1)) continue;
		    cut_cnt++;
		    if (m_type != "Data" && m_type != "QCD") weight *= weight_btag;
		    FillCutflow(cut_cnt, "Nbjet1");
			
		    if (!(event_ngoodphotons == 1)) continue;
		    cut_cnt++;
		    FillCutflow(cut_cnt, "OneGoodPh");
		    
		    if (m_do_phmatch) {
		        if (!PhMatch()) continue;
		    }
		    cut_cnt++;
		    FillCutflow(cut_cnt, "PhMatch");
		} else if (m_selection == "CutSR2015") {
		    if (!m_is2015) continue;
		    cut_cnt++;
		    FillCutflow(cut_cnt, "2015");

		    if (m_subregion == "ejets" || m_subregion == "mujets") {
			if (!(n_jet >= 4)) continue;
			cut_cnt++;
			FillCutflow(cut_cnt, "NjetGeq4");
		    } else if (m_subregion == "ee" || m_subregion == "emu" || m_subregion == "mumu") {
			if (!(n_jet >= 2)) continue;
			cut_cnt++;
			FillCutflow(cut_cnt, "NjetGeq2");
		    }
			
		    if (!(event_ngoodphotons == 1)) continue;
		    cut_cnt++;
		    FillCutflow(cut_cnt, "OneGoodPh");
		    
		    if (m_do_phmatch) {
		        if (!PhMatch()) continue;
		    }
		    cut_cnt++;
		    FillCutflow(cut_cnt, "PhMatch");
			
		    if (!(n_bjet >= 1)) continue;
		    cut_cnt++;
		    if (m_type != "Data" && m_type != "QCD") weight *= weight_btag;
		    FillCutflow(cut_cnt, "NbjetGeq1");
			
		    if (m_subregion == "ejets") {
		        if (!(fabs(ph_mgammalept->at(selph_index1) - m_zmass) > 5000.)) continue;
		        cut_cnt++;
		        FillCutflow(cut_cnt, "ZVeto");
		    }

		    if (m_subregion == "ee" || m_subregion == "mumu") {
		        if (!(met_met > 30000.)) continue;
		        cut_cnt++;
		        FillCutflow(cut_cnt, "MET>30");

		        if (!(event_mll < 85000 || event_mll > 95000)) continue;
		        cut_cnt++;
		        FillCutflow(cut_cnt, "MllZveto");
			
		        if (!(ph_mgammaleptlept->at(selph_index1) < 85000 || ph_mgammaleptlept->at(selph_index1) > 95000)) continue;
		        cut_cnt++;
		        FillCutflow(cut_cnt, "MllyZveto");
		    }
		    
		    if (!(ph_drlept->at(selph_index1) > 1.0)) continue;
		    cut_cnt++;
		    FillCutflow(cut_cnt, "DrPhLep1.0");

		    if (m_do_klfitter) KLFit();
		} else if (m_selection == "CutSR2016") {
		    if (m_is2015) continue;
		    cut_cnt++;
		    FillCutflow(cut_cnt, "2016");

		    if (m_subregion == "ejets" || m_subregion == "mujets") {
			if (!(n_jet >= 4)) continue;
			cut_cnt++;
			FillCutflow(cut_cnt, "NjetGeq4");
		    } else if (m_subregion == "ee" || m_subregion == "emu" || m_subregion == "mumu") {
			if (!(n_jet >= 2)) continue;
			cut_cnt++;
			FillCutflow(cut_cnt, "NjetGeq2");
		    }
			
		    if (!(event_ngoodphotons == 1)) continue;
		    cut_cnt++;
		    FillCutflow(cut_cnt, "OneGoodPh");
		    
		    if (m_do_phmatch) {
		        if (!PhMatch()) continue;
		    }
		    cut_cnt++;
		    FillCutflow(cut_cnt, "PhMatch");
			
		    if (!(n_bjet >= 1)) continue;
		    cut_cnt++;
		    if (m_type != "Data" && m_type != "QCD") weight *= weight_btag;
		    FillCutflow(cut_cnt, "NbjetGeq1");
			
		    if (m_subregion == "ejets") {
		        if (!(fabs(ph_mgammalept->at(selph_index1) - m_zmass) > 5000.)) continue;
		        cut_cnt++;
		        FillCutflow(cut_cnt, "ZVeto");
		    }

		    if (m_subregion == "ee" || m_subregion == "mumu") {
		        if (!(met_met > 30000.)) continue;
		        cut_cnt++;
		        FillCutflow(cut_cnt, "MET>30");

		        if (!(event_mll < 85000 || event_mll > 95000)) continue;
		        cut_cnt++;
		        FillCutflow(cut_cnt, "MllZveto");
			
		        if (!(ph_mgammaleptlept->at(selph_index1) < 85000 || ph_mgammaleptlept->at(selph_index1) > 95000)) continue;
		        cut_cnt++;
		        FillCutflow(cut_cnt, "MllyZveto");
		    }
		    
		    if (!(ph_drlept->at(selph_index1) > 1.0)) continue;
		    cut_cnt++;
		    FillCutflow(cut_cnt, "DrPhLep1.0");

		    if (m_do_klfitter) KLFit();
		} else if (m_selection == "CutSRNph") {
		    if (m_subregion == "ejets" || m_subregion == "mujets") {
			if (!(n_jet >= 4)) continue;
			cut_cnt++;
			FillCutflow(cut_cnt, "NjetGeq4");
		    } else if (m_subregion == "ee" || m_subregion == "emu" || m_subregion == "mumu") {
			if (!(n_jet >= 2)) continue;
			cut_cnt++;
			FillCutflow(cut_cnt, "NjetGeq2");
		    }

		    if (!(n_bjet >= 1)) continue;
		    cut_cnt++;
		    if (m_type != "Data" && m_type != "QCD") weight *= weight_btag;
		    FillCutflow(cut_cnt, "NbjetGeq1");
			
		    if (!(event_ngoodphotons >= 1)) continue;
		    cut_cnt++;
		    FillCutflow(cut_cnt, "GeqOneGoodPh");
		    
		    if (m_do_phmatch) {
		        if (!PhMatch()) continue;
		    }
		    cut_cnt++;
		    FillCutflow(cut_cnt, "PhMatch");
			
		    if (m_subregion == "ejets") {
		        if (!(fabs(ph_mgammalept->at(selph_index1) - m_zmass) > 5000.)) continue;
		        cut_cnt++;
		        FillCutflow(cut_cnt, "ZVeto");
		    }

		    if (m_subregion == "ee" || m_subregion == "mumu") {
		        if (!(met_met > 30000.)) continue;
		        cut_cnt++;
		        FillCutflow(cut_cnt, "MET>30");

		        if (!(event_mll < 85000 || event_mll > 95000)) continue;
		        cut_cnt++;
		        FillCutflow(cut_cnt, "MllZveto");
			
		        if (!(ph_mgammaleptlept->at(selph_index1) < 85000 || ph_mgammaleptlept->at(selph_index1) > 95000)) continue;
		        cut_cnt++;
		        FillCutflow(cut_cnt, "MllyZveto");
		    }
		    
		    if (!(ph_drlept->at(selph_index1) > 1.0)) continue;
		    cut_cnt++;
		    FillCutflow(cut_cnt, "DrPhLep1.0");

		    if (m_do_klfitter) KLFit();
		} else if (m_selection == "CutSRNphCvt") {
		    if (m_subregion == "mujets" && !m_is2015) {
			if (!(leadlep_pt > 27500)) continue;
		    }
		    cut_cnt++;
		    FillCutflow(cut_cnt, "MuPt27.5");

		    if (!(event_ngoodphotons >= 1)) continue;
		    cut_cnt++;
		    FillCutflow(cut_cnt, "OneGoodPh");

		    if (!(ph_conversionType->at(selph_index1) != 0)) continue;
		    cut_cnt++;
		    FillCutflow(cut_cnt, "PhCvt");
		    
		    if (m_do_phmatch) {
		        if (!PhMatch()) continue;
		    }
		    cut_cnt++;
		    FillCutflow(cut_cnt, "PhMatch");
			
		    if (m_subregion == "ejets" || m_subregion == "mujets") {
			if (!(n_jet >= 4)) continue;
			cut_cnt++;
			FillCutflow(cut_cnt, "NjetGeq4");
		    } else if (m_subregion == "ee" || m_subregion == "emu" || m_subregion == "mumu") {
			if (!(n_jet >= 2)) continue;
			cut_cnt++;
			FillCutflow(cut_cnt, "NjetGeq2");
		    }
			
		    if (!(n_bjet >= 1)) continue;
		    cut_cnt++;
		    if (m_type != "Data" && m_type != "QCD" && m_use_weight) weight *= weight_btag;
		    FillCutflow(cut_cnt, "NbjetGeq1");
			
		    if (m_subregion == "ejets") {
		        if (!(fabs(ph_mgammalept->at(selph_index1) - m_zmass) > 5000.)) continue;
		    }
		    cut_cnt++;
		    FillCutflow(cut_cnt, "ZVeto");

		    if (m_subregion == "ee" || m_subregion == "mumu") {
		        if (!(event_mll < 85000 || event_mll > 95000)) continue;
		    }
		    cut_cnt++;
		    FillCutflow(cut_cnt, "MllZveto");
			
		    if (m_subregion == "ee" || m_subregion == "mumu") {
		        if (!(ph_mgammaleptlept->at(selph_index1) < 85000 || ph_mgammaleptlept->at(selph_index1) > 95000)) continue;
		    }
		    cut_cnt++;
		    FillCutflow(cut_cnt, "MllyZveto");

		    if (m_subregion == "ee" || m_subregion == "mumu") {
		        if (!(met_met > 30000.)) continue;
		    }
		    cut_cnt++;
		    FillCutflow(cut_cnt, "MET>30");

		    if (!(ph_drlept->at(selph_index1) > 1.0)) continue;
		    cut_cnt++;
		    FillCutflow(cut_cnt, "DrPhLep1.0");

		    if (m_do_klfitter) KLFit();
		} else if (m_selection == "CutSRUnCvt") {
		    if (m_subregion == "mujets" && !m_is2015) {
			if (!(leadlep_pt > 27500)) continue;
		    }
		    cut_cnt++;
		    FillCutflow(cut_cnt, "MuPt27.5");

		    if (!(event_ngoodphotons == 1)) continue;
		    cut_cnt++;
		    FillCutflow(cut_cnt, "OneGoodPh");
		    
		    if (!(ph_conversionType->at(selph_index1) == 0)) continue;
		    cut_cnt++;
		    FillCutflow(cut_cnt, "PhUnCvt");
		    
		    if (m_do_phmatch) {
		        if (!PhMatch()) continue;
		    }
		    cut_cnt++;
		    FillCutflow(cut_cnt, "PhMatch");
			
		    if (m_subregion == "ejets" || m_subregion == "mujets") {
			if (!(n_jet >= 4)) continue;
			cut_cnt++;
			FillCutflow(cut_cnt, "NjetGeq4");
		    } else if (m_subregion == "ee" || m_subregion == "emu" || m_subregion == "mumu") {
			if (!(n_jet >= 2)) continue;
			cut_cnt++;
			FillCutflow(cut_cnt, "NjetGeq2");
		    }
			
		    if (!(n_bjet >= 1)) continue;
		    cut_cnt++;
		    if (m_type != "Data" && m_type != "QCD" && m_use_weight) weight *= weight_btag;
		    FillCutflow(cut_cnt, "NbjetGeq1");
			
		    if (m_subregion == "ejets") {
		        if (!(fabs(ph_mgammalept->at(selph_index1) - m_zmass) > 5000.)) continue;
		    }
		    cut_cnt++;
		    FillCutflow(cut_cnt, "ZVeto");

		    if (m_subregion == "ee" || m_subregion == "mumu") {
		        if (!(event_mll < 85000 || event_mll > 95000)) continue;
		    }
		    cut_cnt++;
		    FillCutflow(cut_cnt, "MllZveto");
			
		    if (m_subregion == "ee" || m_subregion == "mumu") {
		        if (!(ph_mgammaleptlept->at(selph_index1) < 85000 || ph_mgammaleptlept->at(selph_index1) > 95000)) continue;
		    }
		    cut_cnt++;
		    FillCutflow(cut_cnt, "MllyZveto");

		    if (m_subregion == "ee" || m_subregion == "mumu") {
		        if (!(met_met > 30000.)) continue;
		    }
		    cut_cnt++;
		    FillCutflow(cut_cnt, "MET>30");

		    if (!(ph_drlept->at(selph_index1) > 1.0)) continue;
		    cut_cnt++;
		    FillCutflow(cut_cnt, "DrPhLep1.0");

		    if (m_do_klfitter) KLFit();
		} else if (m_selection == "CutZGammaCR") {
		    if (!(event_ngoodphotons == 1)) continue;
		    cut_cnt++;
		    FillCutflow(cut_cnt, "OneGoodPh");
		    
		    if (m_do_phmatch) {
		        if (!PhMatch()) continue;
		    }
		    cut_cnt++;
		    FillCutflow(cut_cnt, "PhMatch");

		    if (!(ph_mgammaleptlept->at(selph_index1) > 80000 && ph_mgammaleptlept->at(selph_index1) < 100000)) continue;
		    cut_cnt++;
		    FillCutflow(cut_cnt, "MllyZmass");
		} else if (m_selection == "CutZGammaVRNoIso") {
		    if (!(event_ngoodphotons == 1)) continue;
		    cut_cnt++;
		    FillCutflow(cut_cnt, "OneGoodPh");

		    weight /= ph_SF_iso->at(selph_index1);

		    if (!(ph_mgammaleptlept->at(selph_index1) > 85000 && ph_mgammaleptlept->at(selph_index1) < 95000)) continue;
		    cut_cnt++;
		    FillCutflow(cut_cnt, "Zwindow");
		    
		    if (m_do_phmatch) {
		        if (!PhMatch()) continue;
		    }
		    cut_cnt++;
		    FillCutflow(cut_cnt, "PhMatch");
		} else if (m_selection == "CutZGammaVR") {
		    if (!(event_ngoodphotons == 1)) continue;
		    cut_cnt++;
		    FillCutflow(cut_cnt, "OneGoodPh");
		    
		    if (!(ph_mgammaleptlept->at(selph_index1) > 85000 && ph_mgammaleptlept->at(selph_index1) < 95000)) continue;
		    cut_cnt++;
		    FillCutflow(cut_cnt, "Zwindow");
		    
		    if (m_do_phmatch) {
		        if (!PhMatch()) continue;
		    }
		    cut_cnt++;
		    FillCutflow(cut_cnt, "PhMatch");
		} else if (m_selection == "CutZGammaVR2") {
		    if (!(event_mll > 80000 && event_mll < 100000)) continue;
		    cut_cnt++;
		    FillCutflow(cut_cnt, "Zwindow");

		    if (!(n_jet >= 2)) continue;
		    cut_cnt++;
		    FillCutflow(cut_cnt, "NjetGeq2");
			
		    if (!(event_ngoodphotons == 1)) continue;
		    cut_cnt++;
		    FillCutflow(cut_cnt, "OneGoodPh");
		    
		    if (m_do_phmatch) {
		        if (!PhMatch()) continue;
		    }
		    cut_cnt++;
		    FillCutflow(cut_cnt, "PhMatch");
		} else if (m_selection == "CutZGammaVR3") {
		    if (!(event_mll > 80000 && event_mll < 100000)) continue;
		    cut_cnt++;
		    FillCutflow(cut_cnt, "Zwindow");

		    if (!(n_jet >= 2)) continue;
		    cut_cnt++;
		    FillCutflow(cut_cnt, "NjetGeq2");

		    if (!(n_bjet == 1)) continue;
		    cut_cnt++;
		    if (m_type != "Data" && m_type != "QCD" && m_use_weight) weight *= weight_btag;
		    FillCutflow(cut_cnt, "NbjetGeq1");
			
		    if (!(event_ngoodphotons == 1)) continue;
		    cut_cnt++;
		    FillCutflow(cut_cnt, "OneGoodPh");
		    
		    if (m_do_phmatch) {
		        if (!PhMatch()) continue;
		    }
		    cut_cnt++;
		    FillCutflow(cut_cnt, "PhMatch");
		} else if (m_selection == "CutZGammaVR32015") {
		    if (!m_is2015) continue;
		    cut_cnt++;
		    FillCutflow(cut_cnt, "2015");
		    
		    if (!(event_mll > 80000 && event_mll < 100000)) continue;
		    cut_cnt++;
		    FillCutflow(cut_cnt, "Zwindow");

		    if (!(n_jet >= 2)) continue;
		    cut_cnt++;
		    FillCutflow(cut_cnt, "NjetGeq2");

		    if (!(n_bjet == 1)) continue;
		    cut_cnt++;
		    if (m_type != "Data" && m_type != "QCD" && m_use_weight) weight *= weight_btag;
		    FillCutflow(cut_cnt, "NbjetGeq1");
			
		    if (!(event_ngoodphotons == 1)) continue;
		    cut_cnt++;
		    FillCutflow(cut_cnt, "OneGoodPh");
		    
		    if (m_do_phmatch) {
		        if (!PhMatch()) continue;
		    }
		    cut_cnt++;
		    FillCutflow(cut_cnt, "PhMatch");
		} else if (m_selection == "CutZGammaVR32016") {
		    if (m_is2015) continue;
		    cut_cnt++;
		    FillCutflow(cut_cnt, "2016");
		    
		    if (!(event_mll > 80000 && event_mll < 100000)) continue;
		    cut_cnt++;
		    FillCutflow(cut_cnt, "Zwindow");

		    if (!(n_jet >= 2)) continue;
		    cut_cnt++;
		    FillCutflow(cut_cnt, "NjetGeq2");

		    if (!(n_bjet == 1)) continue;
		    cut_cnt++;
		    if (m_type != "Data" && m_type != "QCD" && m_use_weight) weight *= weight_btag;
		    FillCutflow(cut_cnt, "NbjetGeq1");
			
		    if (!(event_ngoodphotons == 1)) continue;
		    cut_cnt++;
		    FillCutflow(cut_cnt, "OneGoodPh");
		    
		    if (m_do_phmatch) {
		        if (!PhMatch()) continue;
		    }
		    cut_cnt++;
		    FillCutflow(cut_cnt, "PhMatch");
		} else if (m_selection == "CutSRRawNoIso") {
		    if (!(event_ngoodphotons == 1)) continue;
		    cut_cnt++;
		    FillCutflow(cut_cnt, "OneGoodPh");
		    
		    weight /= ph_SF_iso->at(selph_index1);
		    
		    if (m_do_phmatch) {
		        if (!PhMatch()) continue;
		    }
		    cut_cnt++;
		    FillCutflow(cut_cnt, "PhMatch");
		} else if (m_selection == "CutSRRawNoIsoPt2030") {
		    if (!(event_ngoodphotons == 1)) continue;
		    cut_cnt++;
		    FillCutflow(cut_cnt, "OneGoodPh");
		    
		    weight /= ph_SF_iso->at(selph_index1);

		    if (!(ph_pt->at(selph_index1) > 20000. && ph_pt->at(selph_index1) < 30000.)) continue;
		    cut_cnt++;
		    FillCutflow(cut_cnt, "PhPt2030");
		    
		    if (m_do_phmatch) {
		        if (!PhMatch()) continue;
		    }
		    cut_cnt++;
		    FillCutflow(cut_cnt, "PhMatch");
		} else if (m_selection == "CutSRRaw") {
		    if (!(event_ngoodphotons == 1)) continue;
		    cut_cnt++;
		    FillCutflow(cut_cnt, "OneGoodPh");
		    
		    if (m_do_phmatch) {
		        if (!PhMatch()) continue;
		    }
		    cut_cnt++;
		    FillCutflow(cut_cnt, "PhMatch");
		} else if (m_selection == "CutSRHT") {
		    if (!(event_HT > 120000)) continue;
		    cut_cnt++;
		    FillCutflow(cut_cnt, "HT120");

		    if (m_subregion == "ejets" || m_subregion == "mujets") {
			if (!(n_jet >= 4)) continue;
			cut_cnt++;
			FillCutflow(cut_cnt, "NjetGeq4");
		    } else if (m_subregion == "ee" || m_subregion == "emu" || m_subregion == "mumu") {
			if (!(n_jet >= 2)) continue;
			cut_cnt++;
			FillCutflow(cut_cnt, "NjetGeq2");
		    }
			
		    if (!(n_bjet >= 1)) continue;
		    cut_cnt++;
		    if (m_type != "Data" && m_type != "QCD" && m_use_weight) weight *= weight_btag;
		    FillCutflow(cut_cnt, "NbjetGeq1");
			
		    if (!(event_ngoodphotons == 1)) continue;
		    cut_cnt++;
		    FillCutflow(cut_cnt, "OneGoodPh");
		    
		    if (m_do_phmatch) {
		        if (!PhMatch()) continue;
		    }
		    cut_cnt++;
		    FillCutflow(cut_cnt, "PhMatch");
			
		    if (m_subregion == "ejets") {
		        if (!(fabs(ph_mgammalept->at(selph_index1) - m_zmass) > 5000.)) continue;
		        cut_cnt++;
		        FillCutflow(cut_cnt, "ZVeto");
		    }

		    if (m_subregion == "ee" || m_subregion == "mumu") {
		        if (!(met_met > 30000.)) continue;
		        cut_cnt++;
		        FillCutflow(cut_cnt, "MET>30");

		        if (!(event_mll < 85000 || event_mll > 95000)) continue;
		        cut_cnt++;
		        FillCutflow(cut_cnt, "MllZveto");
			
		        if (!(ph_mgammaleptlept->at(selph_index1) < 85000 || ph_mgammaleptlept->at(selph_index1) > 95000)) continue;
		        cut_cnt++;
		        FillCutflow(cut_cnt, "MllyZveto");
		    }
		    
		    if (!(ph_drlept->at(selph_index1) > 1.0)) continue;
		    cut_cnt++;
		    FillCutflow(cut_cnt, "DrPhLep1.0");

		    if (m_do_klfitter) KLFit();
		} else if (m_selection == "CutSRNoZvetoNoDrPhLNoIso") {
		    if (m_subregion == "ejets" || m_subregion == "mujets") {
			if (!(n_jet >= 4)) continue;
			cut_cnt++;
			FillCutflow(cut_cnt, "NjetGeq4");
		    } else if (m_subregion == "ee" || m_subregion == "emu" || m_subregion == "mumu") {
			if (!(n_jet >= 2)) continue;
			cut_cnt++;
			FillCutflow(cut_cnt, "NjetGeq2");
		    }
			
		    if (!(n_bjet >= 1)) continue;
		    cut_cnt++;
		    if (m_type != "Data" && m_type != "QCD" && m_use_weight) weight *= weight_btag;
		    FillCutflow(cut_cnt, "NbjetGeq1");
			
		    if (!(event_ngoodphotons == 1)) continue;
		    cut_cnt++;
		    FillCutflow(cut_cnt, "OneGoodPh");
		    
		    weight /= ph_SF_iso->at(selph_index1);
		    
		    if (m_do_phmatch) {
		        if (!PhMatch()) continue;
		    }
		    cut_cnt++;
		    FillCutflow(cut_cnt, "PhMatch");

		    if (m_do_klfitter) KLFit();
		} else if (m_selection == "CutSRNoDrPhLNoIso") {
		    if (m_subregion == "ejets" || m_subregion == "mujets") {
			if (!(n_jet >= 4)) continue;
			cut_cnt++;
			FillCutflow(cut_cnt, "NjetGeq4");
		    } else if (m_subregion == "ee" || m_subregion == "emu" || m_subregion == "mumu") {
			if (!(n_jet >= 2)) continue;
			cut_cnt++;
			FillCutflow(cut_cnt, "NjetGeq2");
		    }
			
		    if (!(n_bjet >= 1)) continue;
		    cut_cnt++;
		    if (m_type != "Data" && m_type != "QCD" && m_use_weight) weight *= weight_btag;
		    FillCutflow(cut_cnt, "NbjetGeq1");
			
		    if (!(event_ngoodphotons == 1)) continue;
		    cut_cnt++;
		    FillCutflow(cut_cnt, "OneGoodPh");
		    
		    weight /= ph_SF_iso->at(selph_index1);
		    
		    if (m_do_phmatch) {
		        if (!PhMatch()) continue;
		    }
		    cut_cnt++;
		    FillCutflow(cut_cnt, "PhMatch");
			
		    if (m_subregion == "ejets") {
		        if (!(fabs(ph_mgammalept->at(selph_index1) - m_zmass) > 5000.)) continue;
		        cut_cnt++;
		        FillCutflow(cut_cnt, "ZVeto");
		    }

		    if (m_subregion == "ee" || m_subregion == "mumu") {
		        if (!(met_met > 30000.)) continue;
		        cut_cnt++;
		        FillCutflow(cut_cnt, "MET>30");

		        if (!(event_mll < 85000 || event_mll > 95000)) continue;
		        cut_cnt++;
		        FillCutflow(cut_cnt, "MllZveto");
			
		        if (!(ph_mgammaleptlept->at(selph_index1) < 85000 || ph_mgammaleptlept->at(selph_index1) > 95000)) continue;
		        cut_cnt++;
		        FillCutflow(cut_cnt, "MllyZveto");
		    }

		    if (m_do_klfitter) KLFit();
		} else if (m_selection == "CutSRNoDrPhL") {
		    if (m_subregion == "ejets" || m_subregion == "mujets") {
			if (!(n_jet >= 4)) continue;
			cut_cnt++;
			FillCutflow(cut_cnt, "NjetGeq4");
		    } else if (m_subregion == "ee" || m_subregion == "emu" || m_subregion == "mumu") {
			if (!(n_jet >= 2)) continue;
			cut_cnt++;
			FillCutflow(cut_cnt, "NjetGeq2");
		    }
			
		    if (!(n_bjet >= 1)) continue;
		    cut_cnt++;
		    if (m_type != "Data" && m_type != "QCD" && m_use_weight) weight *= weight_btag;
		    FillCutflow(cut_cnt, "NbjetGeq1");
			
		    if (!(event_ngoodphotons == 1)) continue;
		    cut_cnt++;
		    FillCutflow(cut_cnt, "OneGoodPh");
		    
		    if (m_do_phmatch) {
		        if (!PhMatch()) continue;
		    }
		    cut_cnt++;
		    FillCutflow(cut_cnt, "PhMatch");
			
		    if (m_subregion == "ejets") {
		        if (!(fabs(ph_mgammalept->at(selph_index1) - m_zmass) > 5000.)) continue;
		        cut_cnt++;
		        FillCutflow(cut_cnt, "ZVeto");
		    }

		    if (m_subregion == "ee" || m_subregion == "mumu") {
		        if (!(met_met > 30000.)) continue;
		        cut_cnt++;
		        FillCutflow(cut_cnt, "MET>30");

		        if (!(event_mll < 85000 || event_mll > 95000)) continue;
		        cut_cnt++;
		        FillCutflow(cut_cnt, "MllZveto");
			
		        if (!(ph_mgammaleptlept->at(selph_index1) < 85000 || ph_mgammaleptlept->at(selph_index1) > 95000)) continue;
		        cut_cnt++;
		        FillCutflow(cut_cnt, "MllyZveto");
		    }

		    if (m_do_klfitter) KLFit();
		} else if (m_selection == "CutNone") {
		} else if (m_selection == "CutSRUpgradeUptoNph") {
		    if (!(event_ngoodphotons >= 1)) continue;
		    cut_cnt++;
		    FillCutflow(cut_cnt, "OnePh");
		} else if (m_selection == "CutSRUpgradePh65") {
		    if (!(event_ngoodphotons == 1)) continue;
		    cut_cnt++;
		    FillCutflow(cut_cnt, "OnePh");

		    if (!(leadph_pt >= 65000)) continue;
		    cut_cnt++;
		    FillCutflow(cut_cnt, "PhPt");
			
		    if (m_subregion == "ejets" || m_subregion == "mujets") {
			if (!(n_jet >= 4)) continue;
			cut_cnt++;
			FillCutflow(cut_cnt, "NjetGeq4");
		    } else if (m_subregion == "ee" || m_subregion == "emu" || m_subregion == "mumu") {
			if (!(n_jet >= 2)) continue;
			cut_cnt++;
			FillCutflow(cut_cnt, "NjetGeq2");
		    }
			
		    if (!(n_bjet >= 1)) continue;
		    cut_cnt++;
		    FillCutflow(cut_cnt, "NbjetGeq1");
			
		    if (m_subregion == "ejets") {
			if (!(mphl < 85000 || mphl > 95000)) continue;
		    }
		    cut_cnt++;
		    FillCutflow(cut_cnt, "ZVeto");

		    if (m_subregion == "ee" || m_subregion == "mumu") {
			if (!(mll < 85000 || mll > 95000)) continue;
		    }
		    cut_cnt++;
		    FillCutflow(cut_cnt, "MllZveto");
			
		    if (m_subregion == "ee" || m_subregion == "mumu") {
			if (!(mllg < 85000 || mllg > 95000)) continue;
		    }
		    cut_cnt++;
		    FillCutflow(cut_cnt, "MllyZveto");

		    if (m_subregion == "ee" || m_subregion == "mumu") {
		        if (!(met_met > 30000.)) continue;
		    }
		    cut_cnt++;
		    FillCutflow(cut_cnt, "MET>30");

		    if (!(leadph_mindrphl > 1.0)) continue;
		    cut_cnt++;
		    FillCutflow(cut_cnt, "DrPhLep1.0");
		} else if (m_selection == "CutSRUpgradePh50") {
		    if (!(event_ngoodphotons == 1)) continue;
		    cut_cnt++;
		    FillCutflow(cut_cnt, "OnePh");

		    if (!(leadph_pt >= 50000)) continue;
		    cut_cnt++;
		    FillCutflow(cut_cnt, "PhPt");
			
		    if (m_subregion == "ejets" || m_subregion == "mujets") {
			if (!(n_jet >= 4)) continue;
			cut_cnt++;
			FillCutflow(cut_cnt, "NjetGeq4");
		    } else if (m_subregion == "ee" || m_subregion == "emu" || m_subregion == "mumu") {
			if (!(n_jet >= 2)) continue;
			cut_cnt++;
			FillCutflow(cut_cnt, "NjetGeq2");
		    }
			
		    if (!(n_bjet >= 1)) continue;
		    cut_cnt++;
		    FillCutflow(cut_cnt, "NbjetGeq1");
			
		    if (m_subregion == "ejets") {
			if (!(mphl < 85000 || mphl > 95000)) continue;
		    }
		    cut_cnt++;
		    FillCutflow(cut_cnt, "ZVeto");

		    if (m_subregion == "ee" || m_subregion == "mumu") {
			if (!(mll < 85000 || mll > 95000)) continue;
		    }
		    cut_cnt++;
		    FillCutflow(cut_cnt, "MllZveto");
			
		    if (m_subregion == "ee" || m_subregion == "mumu") {
			if (!(mllg < 85000 || mllg > 95000)) continue;
		    }
		    cut_cnt++;
		    FillCutflow(cut_cnt, "MllyZveto");

		    if (m_subregion == "ee" || m_subregion == "mumu") {
		        if (!(met_met > 30000.)) continue;
		    }
		    cut_cnt++;
		    FillCutflow(cut_cnt, "MET>30");

		    if (!(leadph_mindrphl > 1.0)) continue;
		    cut_cnt++;
		    FillCutflow(cut_cnt, "DrPhLep1.0");
		} else if (m_selection == "CutSRUpgradePh35") {
		    if (!(event_ngoodphotons == 1)) continue;
		    cut_cnt++;
		    FillCutflow(cut_cnt, "OnePh");

		    if (!(leadph_pt >= 35000)) continue;
		    cut_cnt++;
		    FillCutflow(cut_cnt, "PhPt");
			
		    if (m_subregion == "ejets" || m_subregion == "mujets") {
			if (!(n_jet >= 4)) continue;
			cut_cnt++;
			FillCutflow(cut_cnt, "NjetGeq4");
		    } else if (m_subregion == "ee" || m_subregion == "emu" || m_subregion == "mumu") {
			if (!(n_jet >= 2)) continue;
			cut_cnt++;
			FillCutflow(cut_cnt, "NjetGeq2");
		    }
			
		    if (!(n_bjet >= 1)) continue;
		    cut_cnt++;
		    FillCutflow(cut_cnt, "NbjetGeq1");
			
		    if (m_subregion == "ejets") {
			if (!(mphl < 85000 || mphl > 95000)) continue;
		    }
		    cut_cnt++;
		    FillCutflow(cut_cnt, "ZVeto");

		    if (m_subregion == "ee" || m_subregion == "mumu") {
			if (!(mll < 85000 || mll > 95000)) continue;
		    }
		    cut_cnt++;
		    FillCutflow(cut_cnt, "MllZveto");
			
		    if (m_subregion == "ee" || m_subregion == "mumu") {
			if (!(mllg < 85000 || mllg > 95000)) continue;
		    }
		    cut_cnt++;
		    FillCutflow(cut_cnt, "MllyZveto");

		    if (m_subregion == "ee" || m_subregion == "mumu") {
		        if (!(met_met > 30000.)) continue;
		    }
		    cut_cnt++;
		    FillCutflow(cut_cnt, "MET>30");

		    if (!(leadph_mindrphl > 1.0)) continue;
		    cut_cnt++;
		    FillCutflow(cut_cnt, "DrPhLep1.0");
		} else if (m_selection == "CutSRUpgrade") {
		    if (!(event_ngoodphotons == 1)) continue;
		    cut_cnt++;
		    FillCutflow(cut_cnt, "OnePh");
			
		    if (m_subregion == "ejets" || m_subregion == "mujets") {
			if (!(n_jet >= 4)) continue;
			cut_cnt++;
			FillCutflow(cut_cnt, "NjetGeq4");
		    } else if (m_subregion == "ee" || m_subregion == "emu" || m_subregion == "mumu") {
			if (!(n_jet >= 2)) continue;
			cut_cnt++;
			FillCutflow(cut_cnt, "NjetGeq2");
		    }
			
		    if (!(n_bjet >= 1)) continue;
		    cut_cnt++;
		    FillCutflow(cut_cnt, "NbjetGeq1");
			
		    if (m_subregion == "ejets") {
			if (!(mphl < 85000 || mphl > 95000)) continue;
		    }
		    cut_cnt++;
		    FillCutflow(cut_cnt, "ZVeto");

		    if (m_subregion == "ee" || m_subregion == "mumu") {
			if (!(mll < 85000 || mll > 95000)) continue;
		    }
		    cut_cnt++;
		    FillCutflow(cut_cnt, "MllZveto");
			
		    if (m_subregion == "ee" || m_subregion == "mumu") {
			if (!(mllg < 85000 || mllg > 95000)) continue;
		    }
		    cut_cnt++;
		    FillCutflow(cut_cnt, "MllyZveto");

		    if (m_subregion == "ee" || m_subregion == "mumu") {
		        if (!(met_met > 30000.)) continue;
		    }
		    cut_cnt++;
		    FillCutflow(cut_cnt, "MET>30");

		    if (!(leadph_mindrphl > 1.0)) continue;
		    cut_cnt++;
		    FillCutflow(cut_cnt, "DrPhLep1.0");
		} else if (m_selection == "CutSRUptoNph") {
		    if (m_subregion == "mujets" && !m_is2015) {
			if (!(leadlep_pt > 27500)) continue;
		    }
		    cut_cnt++;
		    FillCutflow(cut_cnt, "MuPt27.5");

		    if (!(event_ngoodphotons >= 1)) continue;
		    cut_cnt++;
		    FillCutflow(cut_cnt, "OneGoodPh");
		} else if (m_selection == "CutSRPh65") {
		    if (m_subregion == "mujets" && !m_is2015) {
			if (!(leadlep_pt > 27500)) continue;
		    }
		    cut_cnt++;
		    FillCutflow(cut_cnt, "MuPt27.5");

		    if (!(event_ngoodphotons == 1)) continue;
		    cut_cnt++;
		    FillCutflow(cut_cnt, "OneGoodPh");

		    if (!(leadph_pt >=  65000)) continue;
		    cut_cnt++;
		    FillCutflow(cut_cnt, "PhPt");
		    
		    if (m_do_phmatch) {
		        if (!PhMatch()) continue;
		    }
		    cut_cnt++;
		    FillCutflow(cut_cnt, "PhMatch");

		    //cout << leadph_pt << " " << leadph_mc_el_pt << " " << leadph_mc_pt << " " << leadph_ptreco_pttrue << endl;
			
		    if (m_subregion == "ejets" || m_subregion == "mujets") {
			if (!(n_jet >= 4)) continue;
			cut_cnt++;
			FillCutflow(cut_cnt, "NjetGeq4");
		    } else if (m_subregion == "ee" || m_subregion == "emu" || m_subregion == "mumu") {
			if (!(n_jet >= 2)) continue;
			cut_cnt++;
			FillCutflow(cut_cnt, "NjetGeq2");
		    }
			
		    if (!(n_bjet >= 1)) continue;
		    cut_cnt++;
		    if (m_type != "Data" && m_type != "QCD" && m_use_weight) weight *= weight_btag;
		    FillCutflow(cut_cnt, "NbjetGeq1");
			
		    if (m_subregion == "ejets") {
			if (!(fabs(ph_mgammalept->at(selph_index1) - m_zmass) > 5000.)) continue;
		    }
		    cut_cnt++;
		    FillCutflow(cut_cnt, "ZVeto");

		    if (m_subregion == "ee" || m_subregion == "mumu") {
			if (!(event_mll < 85000 || event_mll > 95000)) continue;
		    }
		    cut_cnt++;
		    FillCutflow(cut_cnt, "MllZveto");
			
		    if (m_subregion == "ee" || m_subregion == "mumu") {
			if (!(ph_mgammaleptlept->at(selph_index1) < 85000 || ph_mgammaleptlept->at(selph_index1) > 95000)) continue;
		    }
		    cut_cnt++;
		    FillCutflow(cut_cnt, "MllyZveto");

		    if (m_subregion == "ee" || m_subregion == "mumu") {
		        if (!(met_met > 30000.)) continue;
		    }
		    cut_cnt++;
		    FillCutflow(cut_cnt, "MET>30");

		    if (!(ph_drlept->at(selph_index1) > 1.0)) continue;
		    cut_cnt++;
		    FillCutflow(cut_cnt, "DrPhLep1.0");

		    if (m_do_klfitter) KLFit();
		} else if (m_selection == "CutSRPh50") {
		    if (m_subregion == "mujets" && !m_is2015) {
			if (!(leadlep_pt > 27500)) continue;
		    }
		    cut_cnt++;
		    FillCutflow(cut_cnt, "MuPt27.5");

		    if (!(event_ngoodphotons == 1)) continue;
		    cut_cnt++;
		    FillCutflow(cut_cnt, "OneGoodPh");

		    if (!(leadph_pt >=  50000)) continue;
		    cut_cnt++;
		    FillCutflow(cut_cnt, "PhPt");
		    
		    if (m_do_phmatch) {
		        if (!PhMatch()) continue;
		    }
		    cut_cnt++;
		    FillCutflow(cut_cnt, "PhMatch");

		    //cout << leadph_pt << " " << leadph_mc_el_pt << " " << leadph_mc_pt << " " << leadph_ptreco_pttrue << endl;
			
		    if (m_subregion == "ejets" || m_subregion == "mujets") {
			if (!(n_jet >= 4)) continue;
			cut_cnt++;
			FillCutflow(cut_cnt, "NjetGeq4");
		    } else if (m_subregion == "ee" || m_subregion == "emu" || m_subregion == "mumu") {
			if (!(n_jet >= 2)) continue;
			cut_cnt++;
			FillCutflow(cut_cnt, "NjetGeq2");
		    }
			
		    if (!(n_bjet >= 1)) continue;
		    cut_cnt++;
		    if (m_type != "Data" && m_type != "QCD" && m_use_weight) weight *= weight_btag;
		    FillCutflow(cut_cnt, "NbjetGeq1");
			
		    if (m_subregion == "ejets") {
			if (!(fabs(ph_mgammalept->at(selph_index1) - m_zmass) > 5000.)) continue;
		    }
		    cut_cnt++;
		    FillCutflow(cut_cnt, "ZVeto");

		    if (m_subregion == "ee" || m_subregion == "mumu") {
			if (!(event_mll < 85000 || event_mll > 95000)) continue;
		    }
		    cut_cnt++;
		    FillCutflow(cut_cnt, "MllZveto");
			
		    if (m_subregion == "ee" || m_subregion == "mumu") {
			if (!(ph_mgammaleptlept->at(selph_index1) < 85000 || ph_mgammaleptlept->at(selph_index1) > 95000)) continue;
		    }
		    cut_cnt++;
		    FillCutflow(cut_cnt, "MllyZveto");

		    if (m_subregion == "ee" || m_subregion == "mumu") {
		        if (!(met_met > 30000.)) continue;
		    }
		    cut_cnt++;
		    FillCutflow(cut_cnt, "MET>30");

		    if (!(ph_drlept->at(selph_index1) > 1.0)) continue;
		    cut_cnt++;
		    FillCutflow(cut_cnt, "DrPhLep1.0");

		    if (m_do_klfitter) KLFit();
		} else if (m_selection == "CutSRPh35") {
		    if (m_subregion == "mujets" && !m_is2015) {
			if (!(leadlep_pt > 27500)) continue;
		    }
		    cut_cnt++;
		    FillCutflow(cut_cnt, "MuPt27.5");

		    if (!(event_ngoodphotons == 1)) continue;
		    cut_cnt++;
		    FillCutflow(cut_cnt, "OneGoodPh");

		    if (!(leadph_pt >=  35000)) continue;
		    cut_cnt++;
		    FillCutflow(cut_cnt, "PhPt");
		    
		    if (m_do_phmatch) {
		        if (!PhMatch()) continue;
		    }
		    cut_cnt++;
		    FillCutflow(cut_cnt, "PhMatch");

		    //cout << leadph_pt << " " << leadph_mc_el_pt << " " << leadph_mc_pt << " " << leadph_ptreco_pttrue << endl;
			
		    if (m_subregion == "ejets" || m_subregion == "mujets") {
			if (!(n_jet >= 4)) continue;
			cut_cnt++;
			FillCutflow(cut_cnt, "NjetGeq4");
		    } else if (m_subregion == "ee" || m_subregion == "emu" || m_subregion == "mumu") {
			if (!(n_jet >= 2)) continue;
			cut_cnt++;
			FillCutflow(cut_cnt, "NjetGeq2");
		    }
			
		    if (!(n_bjet >= 1)) continue;
		    cut_cnt++;
		    if (m_type != "Data" && m_type != "QCD" && m_use_weight) weight *= weight_btag;
		    FillCutflow(cut_cnt, "NbjetGeq1");
			
		    if (m_subregion == "ejets") {
			if (!(fabs(ph_mgammalept->at(selph_index1) - m_zmass) > 5000.)) continue;
		    }
		    cut_cnt++;
		    FillCutflow(cut_cnt, "ZVeto");

		    if (m_subregion == "ee" || m_subregion == "mumu") {
			if (!(event_mll < 85000 || event_mll > 95000)) continue;
		    }
		    cut_cnt++;
		    FillCutflow(cut_cnt, "MllZveto");
			
		    if (m_subregion == "ee" || m_subregion == "mumu") {
			if (!(ph_mgammaleptlept->at(selph_index1) < 85000 || ph_mgammaleptlept->at(selph_index1) > 95000)) continue;
		    }
		    cut_cnt++;
		    FillCutflow(cut_cnt, "MllyZveto");

		    if (m_subregion == "ee" || m_subregion == "mumu") {
		        if (!(met_met > 30000.)) continue;
		    }
		    cut_cnt++;
		    FillCutflow(cut_cnt, "MET>30");

		    if (!(ph_drlept->at(selph_index1) > 1.0)) continue;
		    cut_cnt++;
		    FillCutflow(cut_cnt, "DrPhLep1.0");

		    if (m_do_klfitter) KLFit();
		} else if (m_selection == "CutSR2b") {
		    if (m_subregion == "mujets" && !m_is2015) {
			if (!(leadlep_pt > 27500)) continue;
		    }
		    cut_cnt++;
		    FillCutflow(cut_cnt, "MuPt27.5");

		    if (!(event_ngoodphotons == 1)) continue;
		    cut_cnt++;
		    FillCutflow(cut_cnt, "OneGoodPh");
		    
		    if (m_do_phmatch) {
		        if (!PhMatch()) continue;
		    }
		    cut_cnt++;
		    FillCutflow(cut_cnt, "PhMatch");

		    //cout << leadph_pt << " " << leadph_mc_el_pt << " " << leadph_mc_pt << " " << leadph_ptreco_pttrue << endl;
			
		    if (m_subregion == "ejets" || m_subregion == "mujets") {
			if (!(n_jet >= 2)) continue;
			cut_cnt++;
			FillCutflow(cut_cnt, "NjetGeq4");
		    } else if (m_subregion == "ee" || m_subregion == "emu" || m_subregion == "mumu") {
			if (!(n_jet >= 2)) continue;
			cut_cnt++;
			FillCutflow(cut_cnt, "NjetGeq2");
		    }
			
		    if (!(n_bjet >= 2)) continue;
		    cut_cnt++;
		    if (m_type != "Data" && m_type != "QCD" && m_use_weight) weight *= weight_btag;
		    FillCutflow(cut_cnt, "NbjetGeq1");
			
		    if (m_subregion == "ejets") {
			if (!(fabs(ph_mgammalept->at(selph_index1) - m_zmass) > 5000.)) continue;
		    }
		    cut_cnt++;
		    FillCutflow(cut_cnt, "ZVeto");

		    if (m_subregion == "ee" || m_subregion == "mumu") {
			if (!(event_mll < 85000 || event_mll > 95000)) continue;
		    }
		    cut_cnt++;
		    FillCutflow(cut_cnt, "MllZveto");
			
		    if (m_subregion == "ee" || m_subregion == "mumu") {
			if (!(ph_mgammaleptlept->at(selph_index1) < 85000 || ph_mgammaleptlept->at(selph_index1) > 95000)) continue;
		    }
		    cut_cnt++;
		    FillCutflow(cut_cnt, "MllyZveto");

		    if (m_subregion == "ee" || m_subregion == "mumu") {
		        if (!(met_met > 30000.)) continue;
		    }
		    cut_cnt++;
		    FillCutflow(cut_cnt, "MET>30");

		    if (!(ph_drlept->at(selph_index1) > 1.0)) continue;
		    cut_cnt++;
		    FillCutflow(cut_cnt, "DrPhLep1.0");

		    if (m_do_klfitter) KLFit();
		} else if (m_selection == "CutSR") {
		    if (m_subregion == "mujets" && !m_is2015) {
			if (!(leadlep_pt > 27500)) continue;
		    }
		    cut_cnt++;
		    FillCutflow(cut_cnt, "MuPt27.5");

		    if (!(event_ngoodphotons == 1)) continue;
		    cut_cnt++;
		    FillCutflow(cut_cnt, "OneGoodPh");
		    
		    if (m_do_phmatch) {
		        if (!PhMatch()) continue;
		    }
		    cut_cnt++;
		    FillCutflow(cut_cnt, "PhMatch");

		    //cout << leadph_pt << " " << leadph_mc_el_pt << " " << leadph_mc_pt << " " << leadph_ptreco_pttrue << endl;
			
		    if (m_subregion == "ejets" || m_subregion == "mujets") {
			if (!(n_jet >= 4)) continue;
			cut_cnt++;
			FillCutflow(cut_cnt, "NjetGeq4");
		    } else if (m_subregion == "ee" || m_subregion == "emu" || m_subregion == "mumu") {
			if (!(n_jet >= 2)) continue;
			cut_cnt++;
			FillCutflow(cut_cnt, "NjetGeq2");
		    }
			
		    if (!(n_bjet >= 1)) continue;
		    cut_cnt++;
		    if (m_type != "Data" && m_type != "QCD" && m_use_weight) weight *= weight_btag;
		    FillCutflow(cut_cnt, "NbjetGeq1");
			
		    if (m_subregion == "ejets") {
			if (!(fabs(ph_mgammalept->at(selph_index1) - m_zmass) > 5000.)) continue;
		    }
		    cut_cnt++;
		    FillCutflow(cut_cnt, "ZVeto");

		    if (m_subregion == "ee" || m_subregion == "mumu") {
			if (!(event_mll < 85000 || event_mll > 95000)) continue;
		    }
		    cut_cnt++;
		    FillCutflow(cut_cnt, "MllZveto");
			
		    if (m_subregion == "ee" || m_subregion == "mumu") {
			if (!(ph_mgammaleptlept->at(selph_index1) < 85000 || ph_mgammaleptlept->at(selph_index1) > 95000)) continue;
		    }
		    cut_cnt++;
		    FillCutflow(cut_cnt, "MllyZveto");

		    if (m_subregion == "ee" || m_subregion == "mumu") {
		        if (!(met_met > 30000.)) continue;
		    }
		    cut_cnt++;
		    FillCutflow(cut_cnt, "MET>30");

		    if (!(ph_drlept->at(selph_index1) > 1.0)) continue;
		    cut_cnt++;
		    FillCutflow(cut_cnt, "DrPhLep1.0");

		    if (m_do_klfitter) KLFit();
		} else if (m_selection == "CutCRHFake") {
		    if (!(event_ngoodphotons >= 1)) continue;
		    cut_cnt++;
		    FillCutflow(cut_cnt, "OneHFakePh");
		    
		    if (m_do_phmatch) {
		        if (!PhMatch()) continue;
		    }
		    cut_cnt++;
		    FillCutflow(cut_cnt, "PhMatch");
		} else if (m_selection == "CutCRHFakeCvt") {
		    if (!(event_ngoodphotons >= 1)) continue;
		    cut_cnt++;
		    FillCutflow(cut_cnt, "OneHFakePh");

		    if (!(ph_conversionType->at(selph_index1) != 0)) continue;
		    cut_cnt++;
		    FillCutflow(cut_cnt, "PhCvt");
		    
		    if (m_do_phmatch) {
		        if (!PhMatch()) continue;
		    }
		    cut_cnt++;
		    FillCutflow(cut_cnt, "PhMatch");
		} else if (m_selection == "CutCRHFakeUnCvt") {
		    if (!(event_ngoodphotons >= 1)) continue;
		    cut_cnt++;
		    FillCutflow(cut_cnt, "One#HFakePh");

		    if (!(ph_conversionType->at(selph_index1) == 0)) continue;
		    cut_cnt++;
		    FillCutflow(cut_cnt, "PhUnCvt");
		    
		    if (m_do_phmatch) {
		        if (!PhMatch()) continue;
		    }
		    cut_cnt++;
		    FillCutflow(cut_cnt, "PhMatch");
		} else if (m_selection == "CutSRNoIDNoIso") {
		    if (m_subregion == "mujets" && !m_is2015) {
			if (!(leadlep_pt > 27500)) continue;
		    }
		    cut_cnt++;
		    FillCutflow(cut_cnt, "MuPt27.5");

		    if (!(event_ngoodphotons == 1)) continue;
		    cut_cnt++;
		    FillCutflow(cut_cnt, "OneIsoPh");
		    
		    if (m_do_phmatch) {
		        if (!PhMatch()) continue;
		    }
		    cut_cnt++;
		    FillCutflow(cut_cnt, "PhMatch");
			
		    if (m_subregion == "ejets" || m_subregion == "mujets") {
			if (!(n_jet >= 4)) continue;
			cut_cnt++;
			FillCutflow(cut_cnt, "NjetGeq4");
		    } else if (m_subregion == "ee" || m_subregion == "emu" || m_subregion == "mumu") {
			if (!(n_jet >= 2)) continue;
			cut_cnt++;
			FillCutflow(cut_cnt, "NjetGeq2");
		    }
			
		    if (!(n_bjet >= 1)) continue;
		    cut_cnt++;
		    if (m_type != "Data" && m_type != "QCD" && m_use_weight) weight *= weight_btag;
		    FillCutflow(cut_cnt, "NbjetGeq1");
			
		    if (m_subregion == "ejets") {
		        if (!(fabs(ph_mgammalept->at(selph_index1) - m_zmass) > 5000.)) continue;
		    }
		    cut_cnt++;
		    FillCutflow(cut_cnt, "ZVeto");

		    if (m_subregion == "ee" || m_subregion == "mumu") {
		        if (!(event_mll < 85000 || event_mll > 95000)) continue;
		    }
		    cut_cnt++;
		    FillCutflow(cut_cnt, "MllZveto");
			
		    if (m_subregion == "ee" || m_subregion == "mumu") {
		        if (!(ph_mgammaleptlept->at(selph_index1) < 85000 || ph_mgammaleptlept->at(selph_index1) > 95000)) continue;
		    }
		    cut_cnt++;
		    FillCutflow(cut_cnt, "MllyZveto");

		    if (m_subregion == "ee" || m_subregion == "mumu") {
		        if (!(met_met > 30000.)) continue;
		    }
		    cut_cnt++;
		    FillCutflow(cut_cnt, "MET>30");

		    if (!(ph_drlept->at(selph_index1) > 1.0)) continue;
		    cut_cnt++;
		    FillCutflow(cut_cnt, "DrPhLep1.0");

		    if (m_do_klfitter) KLFit();
		} else if (m_selection == "CutSRNoID") {
		    if (m_subregion == "mujets" && !m_is2015) {
			if (!(leadlep_pt > 27500)) continue;
		    }
		    cut_cnt++;
		    FillCutflow(cut_cnt, "MuPt27.5");

		    if (!(event_ngoodphotons == 1)) continue;
		    cut_cnt++;
		    FillCutflow(cut_cnt, "OneIsoPh");
		    
		    if (m_do_phmatch) {
		        if (!PhMatch()) continue;
		    }
		    cut_cnt++;
		    FillCutflow(cut_cnt, "PhMatch");
			
		    if (m_subregion == "ejets" || m_subregion == "mujets") {
			if (!(n_jet >= 4)) continue;
			cut_cnt++;
			FillCutflow(cut_cnt, "NjetGeq4");
		    } else if (m_subregion == "ee" || m_subregion == "emu" || m_subregion == "mumu") {
			if (!(n_jet >= 2)) continue;
			cut_cnt++;
			FillCutflow(cut_cnt, "NjetGeq2");
		    }
			
		    if (!(n_bjet >= 1)) continue;
		    cut_cnt++;
		    if (m_type != "Data" && m_type != "QCD" && m_use_weight) weight *= weight_btag;
		    FillCutflow(cut_cnt, "NbjetGeq1");
			
		    if (m_subregion == "ejets") {
		        if (!(fabs(ph_mgammalept->at(selph_index1) - m_zmass) > 5000.)) continue;
		    }
		    cut_cnt++;
		    FillCutflow(cut_cnt, "ZVeto");

		    if (m_subregion == "ee" || m_subregion == "mumu") {
		        if (!(event_mll < 85000 || event_mll > 95000)) continue;
		    }
		    cut_cnt++;
		    FillCutflow(cut_cnt, "MllZveto");
			
		    if (m_subregion == "ee" || m_subregion == "mumu") {
		        if (!(ph_mgammaleptlept->at(selph_index1) < 85000 || ph_mgammaleptlept->at(selph_index1) > 95000)) continue;
		    }
		    cut_cnt++;
		    FillCutflow(cut_cnt, "MllyZveto");

		    if (m_subregion == "ee" || m_subregion == "mumu") {
		        if (!(met_met > 30000.)) continue;
		    }
		    cut_cnt++;
		    FillCutflow(cut_cnt, "MET>30");

		    if (!(ph_drlept->at(selph_index1) > 1.0)) continue;
		    cut_cnt++;
		    FillCutflow(cut_cnt, "DrPhLep1.0");

		    if (m_do_klfitter) KLFit();
		} else if (m_selection == "CutSRNoIDCvt") {
		    if (m_subregion == "mujets" && !m_is2015) {
			if (!(leadlep_pt > 27500)) continue;
		    }
		    cut_cnt++;
		    FillCutflow(cut_cnt, "MuPt27.5");

		    if (!(event_ngoodphotons == 1)) continue;
		    cut_cnt++;
		    FillCutflow(cut_cnt, "OneIsoPh");

		    if (!(ph_conversionType->at(selph_index1) != 0)) continue;
		    cut_cnt++;
		    FillCutflow(cut_cnt, "PhCvt");
		    
		    if (m_do_phmatch) {
		        if (!PhMatch()) continue;
		    }
		    cut_cnt++;
		    FillCutflow(cut_cnt, "PhMatch");
			
		    if (m_subregion == "ejets" || m_subregion == "mujets") {
			if (!(n_jet >= 4)) continue;
			cut_cnt++;
			FillCutflow(cut_cnt, "NjetGeq4");
		    } else if (m_subregion == "ee" || m_subregion == "emu" || m_subregion == "mumu") {
			if (!(n_jet >= 2)) continue;
			cut_cnt++;
			FillCutflow(cut_cnt, "NjetGeq2");
		    }
			
		    if (!(n_bjet >= 1)) continue;
		    cut_cnt++;
		    if (m_type != "Data" && m_type != "QCD" && m_use_weight) weight *= weight_btag;
		    FillCutflow(cut_cnt, "NbjetGeq1");
			
		    if (m_subregion == "ejets") {
		        if (!(fabs(ph_mgammalept->at(selph_index1) - m_zmass) > 5000.)) continue;
		    }
		    cut_cnt++;
		    FillCutflow(cut_cnt, "ZVeto");

		    if (m_subregion == "ee" || m_subregion == "mumu") {
		        if (!(event_mll < 85000 || event_mll > 95000)) continue;
		    }
		    cut_cnt++;
		    FillCutflow(cut_cnt, "MllZveto");
			
		    if (m_subregion == "ee" || m_subregion == "mumu") {
		        if (!(ph_mgammaleptlept->at(selph_index1) < 85000 || ph_mgammaleptlept->at(selph_index1) > 95000)) continue;
		    }
		    cut_cnt++;
		    FillCutflow(cut_cnt, "MllyZveto");

		    if (m_subregion == "ee" || m_subregion == "mumu") {
		        if (!(met_met > 30000.)) continue;
		    }
		    cut_cnt++;
		    FillCutflow(cut_cnt, "MET>30");

		    if (!(ph_drlept->at(selph_index1) > 1.0)) continue;
		    cut_cnt++;
		    FillCutflow(cut_cnt, "DrPhLep1.0");

		    if (m_do_klfitter) KLFit();
		} else if (m_selection == "CutSRNoIDUnCvt") {
		    if (m_subregion == "mujets" && !m_is2015) {
			if (!(leadlep_pt > 27500)) continue;
		    }
		    cut_cnt++;
		    FillCutflow(cut_cnt, "MuPt27.5");

		    if (!(event_ngoodphotons == 1)) continue;
		    cut_cnt++;
		    FillCutflow(cut_cnt, "OneIsoPh");

		    if (!(ph_conversionType->at(selph_index1) == 0)) continue;
		    cut_cnt++;
		    FillCutflow(cut_cnt, "PhUnCvt");
		    
		    if (m_do_phmatch) {
		        if (!PhMatch()) continue;
		    }
		    cut_cnt++;
		    FillCutflow(cut_cnt, "PhMatch");
			
		    if (m_subregion == "ejets" || m_subregion == "mujets") {
			if (!(n_jet >= 4)) continue;
			cut_cnt++;
			FillCutflow(cut_cnt, "NjetGeq4");
		    } else if (m_subregion == "ee" || m_subregion == "emu" || m_subregion == "mumu") {
			if (!(n_jet >= 2)) continue;
			cut_cnt++;
			FillCutflow(cut_cnt, "NjetGeq2");
		    }
			
		    if (!(n_bjet >= 1)) continue;
		    cut_cnt++;
		    if (m_type != "Data" && m_type != "QCD" && m_use_weight) weight *= weight_btag;
		    FillCutflow(cut_cnt, "NbjetGeq1");
			
		    if (m_subregion == "ejets") {
		        if (!(fabs(ph_mgammalept->at(selph_index1) - m_zmass) > 5000.)) continue;
		    }
		    cut_cnt++;
		    FillCutflow(cut_cnt, "ZVeto");

		    if (m_subregion == "ee" || m_subregion == "mumu") {
		        if (!(event_mll < 85000 || event_mll > 95000)) continue;
		    }
		    cut_cnt++;
		    FillCutflow(cut_cnt, "MllZveto");
			
		    if (m_subregion == "ee" || m_subregion == "mumu") {
		        if (!(ph_mgammaleptlept->at(selph_index1) < 85000 || ph_mgammaleptlept->at(selph_index1) > 95000)) continue;
		    }
		    cut_cnt++;
		    FillCutflow(cut_cnt, "MllyZveto");

		    if (m_subregion == "ee" || m_subregion == "mumu") {
		        if (!(met_met > 30000.)) continue;
		    }
		    cut_cnt++;
		    FillCutflow(cut_cnt, "MET>30");

		    if (!(ph_drlept->at(selph_index1) > 1.0)) continue;
		    cut_cnt++;
		    FillCutflow(cut_cnt, "DrPhLep1.0");

		    if (m_do_klfitter) KLFit();
		} else if (m_selection == "CutSR2Ph") {
		    if (m_subregion == "mujets" && !m_is2015) {
			if (!(leadlep_pt > 27500)) continue;
		    }
		    cut_cnt++;
		    FillCutflow(cut_cnt, "MuPt27.5");

		    if (!(event_ngoodphotons >= 1)) continue;
		    cut_cnt++;
		    FillCutflow(cut_cnt, "OneGoodPh");
		    
		    if (m_do_phmatch) {
		        if (!PhMatch()) continue;
		    }
		    cut_cnt++;
		    FillCutflow(cut_cnt, "PhMatch");
			
		    if (m_subregion == "ejets" || m_subregion == "mujets") {
			if (!(n_jet >= 4)) continue;
			cut_cnt++;
			FillCutflow(cut_cnt, "NjetGeq4");
		    } else if (m_subregion == "ee" || m_subregion == "emu" || m_subregion == "mumu") {
			if (!(n_jet >= 2)) continue;
			cut_cnt++;
			FillCutflow(cut_cnt, "NjetGeq2");
		    }
			
		    if (!(n_bjet >= 1)) continue;
		    cut_cnt++;
		    if (m_type != "Data" && m_type != "QCD" && m_use_weight) weight *= weight_btag;
		    FillCutflow(cut_cnt, "NbjetGeq1");
			
		    if (m_subregion == "ejets") {
		        if (!(fabs(ph_mgammalept->at(selph_index1) - m_zmass) > 5000.)) continue;
		    }
		    cut_cnt++;
		    FillCutflow(cut_cnt, "ZVeto");

		    if (m_subregion == "ee" || m_subregion == "mumu") {
		        if (!(event_mll < 85000 || event_mll > 95000)) continue;
		    }
		    cut_cnt++;
		    FillCutflow(cut_cnt, "MllZveto");
			
		    if (m_subregion == "ee" || m_subregion == "mumu") {
		        if (!(ph_mgammaleptlept->at(selph_index1) < 85000 || ph_mgammaleptlept->at(selph_index1) > 95000)) continue;
		    }
		    cut_cnt++;
		    FillCutflow(cut_cnt, "MllyZveto");

		    if (m_subregion == "ee" || m_subregion == "mumu") {
		        if (!(met_met > 30000.)) continue;
		    }
		    cut_cnt++;
		    FillCutflow(cut_cnt, "MET>30");

		    if (!(ph_drlept->at(selph_index1) > 1.0)) continue;
		    cut_cnt++;
		    FillCutflow(cut_cnt, "DrPhLep1.0");

		    if (m_do_klfitter) KLFit();
		} else if (m_selection == "CutSRDLMPhLepLep170") {
		    if (m_subregion == "mujets" && !m_is2015) {
			if (!(leadlep_pt > 27500)) continue;
		    }
		    cut_cnt++;
		    FillCutflow(cut_cnt, "MuPt27.5");

		    if (!(event_ngoodphotons == 1)) continue;
		    cut_cnt++;
		    FillCutflow(cut_cnt, "OneGoodPh");
		    
		    if (m_do_phmatch) {
		        if (!PhMatch()) continue;
		    }
		    cut_cnt++;
		    FillCutflow(cut_cnt, "PhMatch");
			
		    if (m_subregion == "ejets" || m_subregion == "mujets") {
			if (!(n_jet >= 4)) continue;
			cut_cnt++;
			FillCutflow(cut_cnt, "NjetGeq4");
		    } else if (m_subregion == "ee" || m_subregion == "emu" || m_subregion == "mumu") {
			if (!(n_jet >= 2)) continue;
			cut_cnt++;
			FillCutflow(cut_cnt, "NjetGeq2");
		    }
			
		    if (!(n_bjet >= 1)) continue;
		    cut_cnt++;
		    if (m_type != "Data" && m_type != "QCD" && m_use_weight) weight *= weight_btag;
		    FillCutflow(cut_cnt, "NbjetGeq1");
			
		    if (m_subregion == "ejets") {
		        if (!(fabs(ph_mgammalept->at(selph_index1) - m_zmass) > 5000.)) continue;
		    }
		    cut_cnt++;
		    FillCutflow(cut_cnt, "ZVeto");

		    if (m_subregion == "ee" || m_subregion == "mumu") {
		        if (!(event_mll < 85000 || event_mll > 95000)) continue;
		    }
		    cut_cnt++;
		    FillCutflow(cut_cnt, "MllZveto");
			
		    if (m_subregion == "ee" || m_subregion == "mumu") {
		        if (!(ph_mgammaleptlept->at(selph_index1) < 85000 || ph_mgammaleptlept->at(selph_index1) > 95000)) continue;
		    }
		    cut_cnt++;
		    FillCutflow(cut_cnt, "MllyZveto");

		    if (m_subregion == "ee" || m_subregion == "mumu") {
		        if (!(met_met > 30000.)) continue;
		    }
		    cut_cnt++;
		    FillCutflow(cut_cnt, "MET>30");

		    if (!(ph_drlept->at(selph_index1) > 1.0)) continue;
		    cut_cnt++;
		    FillCutflow(cut_cnt, "DrPhLep1.0");

		    if (!(ph_mgammaleptlept->at(selph_index1) > 150000. && ph_mgammaleptlept->at(selph_index1) < 200000.)) continue;
		    cut_cnt++;
		    FillCutflow(cut_cnt, "MPhLepLep150200");

		    if (m_do_klfitter) KLFit();
		} else if (m_selection == "CutSRDLMPhLepLep170Cvt") {
		    if (m_subregion == "mujets" && !m_is2015) {
			if (!(leadlep_pt > 27500)) continue;
		    }
		    cut_cnt++;
		    FillCutflow(cut_cnt, "MuPt27.5");

		    if (!(event_ngoodphotons == 1)) continue;
		    cut_cnt++;
		    FillCutflow(cut_cnt, "OneGoodPh");
		    
		    if (!(ph_conversionType->at(selph_index1) != 0)) continue;
		    cut_cnt++;
		    FillCutflow(cut_cnt, "PhCvt");
		    
		    if (m_do_phmatch) {
		        if (!PhMatch()) continue;
		    }
		    cut_cnt++;
		    FillCutflow(cut_cnt, "PhMatch");
			
		    if (m_subregion == "ejets" || m_subregion == "mujets") {
			if (!(n_jet >= 4)) continue;
			cut_cnt++;
			FillCutflow(cut_cnt, "NjetGeq4");
		    } else if (m_subregion == "ee" || m_subregion == "emu" || m_subregion == "mumu") {
			if (!(n_jet >= 2)) continue;
			cut_cnt++;
			FillCutflow(cut_cnt, "NjetGeq2");
		    }
			
		    if (!(n_bjet >= 1)) continue;
		    cut_cnt++;
		    if (m_type != "Data" && m_type != "QCD" && m_use_weight) weight *= weight_btag;
		    FillCutflow(cut_cnt, "NbjetGeq1");
			
		    if (m_subregion == "ejets") {
		        if (!(fabs(ph_mgammalept->at(selph_index1) - m_zmass) > 5000.)) continue;
		    }
		    cut_cnt++;
		    FillCutflow(cut_cnt, "ZVeto");

		    if (m_subregion == "ee" || m_subregion == "mumu") {
		        if (!(event_mll < 85000 || event_mll > 95000)) continue;
		    }
		    cut_cnt++;
		    FillCutflow(cut_cnt, "MllZveto");
			
		    if (m_subregion == "ee" || m_subregion == "mumu") {
		        if (!(ph_mgammaleptlept->at(selph_index1) < 85000 || ph_mgammaleptlept->at(selph_index1) > 95000)) continue;
		    }
		    cut_cnt++;
		    FillCutflow(cut_cnt, "MllyZveto");

		    if (m_subregion == "ee" || m_subregion == "mumu") {
		        if (!(met_met > 30000.)) continue;
		    }
		    cut_cnt++;
		    FillCutflow(cut_cnt, "MET>30");

		    if (!(ph_drlept->at(selph_index1) > 1.0)) continue;
		    cut_cnt++;
		    FillCutflow(cut_cnt, "DrPhLep1.0");

		    if (!(ph_mgammaleptlept->at(selph_index1) > 150000. && ph_mgammaleptlept->at(selph_index1) < 200000.)) continue;
		    cut_cnt++;
		    FillCutflow(cut_cnt, "MPhLepLep150200");

		    if (m_do_klfitter) KLFit();
		} else if (m_selection == "CutSRDLMPhLepLep170UnCvt") {
		    if (m_subregion == "mujets" && !m_is2015) {
			if (!(leadlep_pt > 27500)) continue;
		    }
		    cut_cnt++;
		    FillCutflow(cut_cnt, "MuPt27.5");

		    if (!(event_ngoodphotons == 1)) continue;
		    cut_cnt++;
		    FillCutflow(cut_cnt, "OneGoodPh");
		    
		    if (!(ph_conversionType->at(selph_index1) == 0)) continue;
		    cut_cnt++;
		    FillCutflow(cut_cnt, "PhUnCvt");
		    
		    if (m_do_phmatch) {
		        if (!PhMatch()) continue;
		    }
		    cut_cnt++;
		    FillCutflow(cut_cnt, "PhMatch");
			
		    if (m_subregion == "ejets" || m_subregion == "mujets") {
			if (!(n_jet >= 4)) continue;
			cut_cnt++;
			FillCutflow(cut_cnt, "NjetGeq4");
		    } else if (m_subregion == "ee" || m_subregion == "emu" || m_subregion == "mumu") {
			if (!(n_jet >= 2)) continue;
			cut_cnt++;
			FillCutflow(cut_cnt, "NjetGeq2");
		    }
			
		    if (!(n_bjet >= 1)) continue;
		    cut_cnt++;
		    if (m_type != "Data" && m_type != "QCD" && m_use_weight) weight *= weight_btag;
		    FillCutflow(cut_cnt, "NbjetGeq1");
			
		    if (m_subregion == "ejets") {
		        if (!(fabs(ph_mgammalept->at(selph_index1) - m_zmass) > 5000.)) continue;
		    }
		    cut_cnt++;
		    FillCutflow(cut_cnt, "ZVeto");

		    if (m_subregion == "ee" || m_subregion == "mumu") {
		        if (!(event_mll < 85000 || event_mll > 95000)) continue;
		    }
		    cut_cnt++;
		    FillCutflow(cut_cnt, "MllZveto");
			
		    if (m_subregion == "ee" || m_subregion == "mumu") {
		        if (!(ph_mgammaleptlept->at(selph_index1) < 85000 || ph_mgammaleptlept->at(selph_index1) > 95000)) continue;
		    }
		    cut_cnt++;
		    FillCutflow(cut_cnt, "MllyZveto");

		    if (m_subregion == "ee" || m_subregion == "mumu") {
		        if (!(met_met > 30000.)) continue;
		    }
		    cut_cnt++;
		    FillCutflow(cut_cnt, "MET>30");

		    if (!(ph_drlept->at(selph_index1) > 1.0)) continue;
		    cut_cnt++;
		    FillCutflow(cut_cnt, "DrPhLep1.0");

		    if (!(ph_mgammaleptlept->at(selph_index1) > 150000. && ph_mgammaleptlept->at(selph_index1) < 200000.)) continue;
		    cut_cnt++;
		    FillCutflow(cut_cnt, "MPhLepLep150200");

		    if (m_do_klfitter) KLFit();
		} else if (m_selection == "CutSRCvt") {
		    if (m_subregion == "mujets" && !m_is2015) {
			if (!(leadlep_pt > 27500)) continue;
		    }
		    cut_cnt++;
		    FillCutflow(cut_cnt, "MuPt27.5");

		    if (!(event_ngoodphotons == 1)) continue;
		    cut_cnt++;
		    FillCutflow(cut_cnt, "OneGoodPh");
		    
		    if (!(ph_conversionType->at(selph_index1) != 0)) continue;
		    cut_cnt++;
		    FillCutflow(cut_cnt, "PhCvt");
		    
		    if (m_do_phmatch) {
		        if (!PhMatch()) continue;
		    }
		    cut_cnt++;
		    FillCutflow(cut_cnt, "PhMatch");
			
		    if (m_subregion == "ejets" || m_subregion == "mujets") {
			if (!(n_jet >= 4)) continue;
			cut_cnt++;
			FillCutflow(cut_cnt, "NjetGeq4");
		    } else if (m_subregion == "ee" || m_subregion == "emu" || m_subregion == "mumu") {
			if (!(n_jet >= 2)) continue;
			cut_cnt++;
			FillCutflow(cut_cnt, "NjetGeq2");
		    }
			
		    if (!(n_bjet >= 1)) continue;
		    cut_cnt++;
		    if (m_type != "Data" && m_type != "QCD" && m_use_weight) weight *= weight_btag;
		    FillCutflow(cut_cnt, "NbjetGeq1");
			
		    if (m_subregion == "ejets") {
		        if (!(fabs(ph_mgammalept->at(selph_index1) - m_zmass) > 5000.)) continue;
		    }
		    cut_cnt++;
		    FillCutflow(cut_cnt, "ZVeto");

		    if (m_subregion == "ee" || m_subregion == "mumu") {
		        if (!(event_mll < 85000 || event_mll > 95000)) continue;
		    }
		    cut_cnt++;
		    FillCutflow(cut_cnt, "MllZveto");
			
		    if (m_subregion == "ee" || m_subregion == "mumu") {
		        if (!(ph_mgammaleptlept->at(selph_index1) < 85000 || ph_mgammaleptlept->at(selph_index1) > 95000)) continue;
		    }
		    cut_cnt++;
		    FillCutflow(cut_cnt, "MllyZveto");

		    if (m_subregion == "ee" || m_subregion == "mumu") {
		        if (!(met_met > 30000.)) continue;
		    }
		    cut_cnt++;
		    FillCutflow(cut_cnt, "MET>30");

		    if (!(ph_drlept->at(selph_index1) > 1.0)) continue;
		    cut_cnt++;
		    FillCutflow(cut_cnt, "DrPhLep1.0");

		    if (m_do_klfitter) KLFit();
		} else if (m_selection == "CutSRUnCvt") {
		    if (m_subregion == "mujets" && !m_is2015) {
			if (!(leadlep_pt > 27500)) continue;
		    }
		    cut_cnt++;
		    FillCutflow(cut_cnt, "MuPt27.5");

		    if (!(event_ngoodphotons == 1)) continue;
		    cut_cnt++;
		    FillCutflow(cut_cnt, "OneGoodPh");
		    
		    if (!(ph_conversionType->at(selph_index1) == 0)) continue;
		    cut_cnt++;
		    FillCutflow(cut_cnt, "PhUnCvt");
		    
		    if (m_do_phmatch) {
		        if (!PhMatch()) continue;
		    }
		    cut_cnt++;
		    FillCutflow(cut_cnt, "PhMatch");
			
		    if (m_subregion == "ejets" || m_subregion == "mujets") {
			if (!(n_jet >= 4)) continue;
			cut_cnt++;
			FillCutflow(cut_cnt, "NjetGeq4");
		    } else if (m_subregion == "ee" || m_subregion == "emu" || m_subregion == "mumu") {
			if (!(n_jet >= 2)) continue;
			cut_cnt++;
			FillCutflow(cut_cnt, "NjetGeq2");
		    }
			
		    if (!(n_bjet >= 1)) continue;
		    cut_cnt++;
		    if (m_type != "Data" && m_type != "QCD" && m_use_weight) weight *= weight_btag;
		    FillCutflow(cut_cnt, "NbjetGeq1");
			
		    if (m_subregion == "ejets") {
		        if (!(fabs(ph_mgammalept->at(selph_index1) - m_zmass) > 5000.)) continue;
		    }
		    cut_cnt++;
		    FillCutflow(cut_cnt, "ZVeto");

		    if (m_subregion == "ee" || m_subregion == "mumu") {
		        if (!(event_mll < 85000 || event_mll > 95000)) continue;
		    }
		    cut_cnt++;
		    FillCutflow(cut_cnt, "MllZveto");
			
		    if (m_subregion == "ee" || m_subregion == "mumu") {
		        if (!(ph_mgammaleptlept->at(selph_index1) < 85000 || ph_mgammaleptlept->at(selph_index1) > 95000)) continue;
		    }
		    cut_cnt++;
		    FillCutflow(cut_cnt, "MllyZveto");

		    if (m_subregion == "ee" || m_subregion == "mumu") {
		        if (!(met_met > 30000.)) continue;
		    }
		    cut_cnt++;
		    FillCutflow(cut_cnt, "MET>30");

		    if (!(ph_drlept->at(selph_index1) > 1.0)) continue;
		    cut_cnt++;
		    FillCutflow(cut_cnt, "DrPhLep1.0");

		    if (m_do_klfitter) KLFit();
		} else if (m_selection == "CutSRNoIso") {
		    if (m_subregion == "mujets" && !m_is2015) {
			if (!(leadlep_pt > 27500)) continue;
		    }
		    cut_cnt++;
		    FillCutflow(cut_cnt, "MuPt27.5");

		    if (!(event_ngoodphotons == 1)) continue;
		    cut_cnt++;
		    FillCutflow(cut_cnt, "OneGoodPh");
		    
		    if (m_do_phmatch) {
		        if (!PhMatch()) continue;
		    }
		    cut_cnt++;
		    FillCutflow(cut_cnt, "PhMatch");
			
		    if (m_subregion == "ejets" || m_subregion == "mujets") {
			if (!(n_jet >= 4)) continue;
			cut_cnt++;
			FillCutflow(cut_cnt, "NjetGeq4");
		    } else if (m_subregion == "ee" || m_subregion == "emu" || m_subregion == "mumu") {
			if (!(n_jet >= 2)) continue;
			cut_cnt++;
			FillCutflow(cut_cnt, "NjetGeq2");
		    }
			
		    if (!(n_bjet >= 1)) continue;
		    cut_cnt++;
		    if (m_type != "Data" && m_type != "QCD" && m_use_weight) weight *= weight_btag;
		    FillCutflow(cut_cnt, "NbjetGeq1");
			
		    if (m_subregion == "ejets") {
		        if (!(fabs(ph_mgammalept->at(selph_index1) - m_zmass) > 5000.)) continue;
		    }
		    cut_cnt++;
		    FillCutflow(cut_cnt, "ZVeto");

		    if (m_subregion == "ee" || m_subregion == "mumu") {
		        if (!(event_mll < 85000 || event_mll > 95000)) continue;
		    }
		    cut_cnt++;
		    FillCutflow(cut_cnt, "MllZveto");
			
		    if (m_subregion == "ee" || m_subregion == "mumu") {
		        if (!(ph_mgammaleptlept->at(selph_index1) < 85000 || ph_mgammaleptlept->at(selph_index1) > 95000)) continue;
		    }
		    cut_cnt++;
		    FillCutflow(cut_cnt, "MllyZveto");

		    if (m_subregion == "ee" || m_subregion == "mumu") {
		        if (!(met_met > 30000.)) continue;
		    }
		    cut_cnt++;
		    FillCutflow(cut_cnt, "MET>30");

		    if (!(ph_drlept->at(selph_index1) > 1.0)) continue;
		    cut_cnt++;
		    FillCutflow(cut_cnt, "DrPhLep1.0");

		    if (m_do_klfitter) KLFit();
		} else if (m_selection == "CutSRNoIsoCvt") {
		    if (m_subregion == "mujets" && !m_is2015) {
			if (!(leadlep_pt > 27500)) continue;
		    }
		    cut_cnt++;
		    FillCutflow(cut_cnt, "MuPt27.5");

		    if (!(event_ngoodphotons == 1)) continue;
		    cut_cnt++;
		    FillCutflow(cut_cnt, "OneGoodPh");

		    if (!(ph_conversionType->at(selph_index1) != 0)) continue;
		    cut_cnt++;
		    FillCutflow(cut_cnt, "PhCvt");
		    
		    if (m_do_phmatch) {
		        if (!PhMatch()) continue;
		    }
		    cut_cnt++;
		    FillCutflow(cut_cnt, "PhMatch");
			
		    if (m_subregion == "ejets" || m_subregion == "mujets") {
			if (!(n_jet >= 4)) continue;
			cut_cnt++;
			FillCutflow(cut_cnt, "NjetGeq4");
		    } else if (m_subregion == "ee" || m_subregion == "emu" || m_subregion == "mumu") {
			if (!(n_jet >= 2)) continue;
			cut_cnt++;
			FillCutflow(cut_cnt, "NjetGeq2");
		    }
			
		    if (!(n_bjet >= 1)) continue;
		    cut_cnt++;
		    if (m_type != "Data" && m_type != "QCD" && m_use_weight) weight *= weight_btag;
		    FillCutflow(cut_cnt, "NbjetGeq1");
			
		    if (m_subregion == "ejets") {
		        if (!(fabs(ph_mgammalept->at(selph_index1) - m_zmass) > 5000.)) continue;
		    }
		    cut_cnt++;
		    FillCutflow(cut_cnt, "ZVeto");

		    if (m_subregion == "ee" || m_subregion == "mumu") {
		        if (!(event_mll < 85000 || event_mll > 95000)) continue;
		    }
		    cut_cnt++;
		    FillCutflow(cut_cnt, "MllZveto");
			
		    if (m_subregion == "ee" || m_subregion == "mumu") {
		        if (!(ph_mgammaleptlept->at(selph_index1) < 85000 || ph_mgammaleptlept->at(selph_index1) > 95000)) continue;
		    }
		    cut_cnt++;
		    FillCutflow(cut_cnt, "MllyZveto");

		    if (m_subregion == "ee" || m_subregion == "mumu") {
		        if (!(met_met > 30000.)) continue;
		    }
		    cut_cnt++;
		    FillCutflow(cut_cnt, "MET>30");

		    if (!(ph_drlept->at(selph_index1) > 1.0)) continue;
		    cut_cnt++;
		    FillCutflow(cut_cnt, "DrPhLep1.0");

		    if (m_do_klfitter) KLFit();
		} else if (m_selection == "CutSRNoIsoUnCvt") {
		    if (m_subregion == "mujets" && !m_is2015) {
			if (!(leadlep_pt > 27500)) continue;
		    }
		    cut_cnt++;
		    FillCutflow(cut_cnt, "MuPt27.5");

		    if (!(event_ngoodphotons == 1)) continue;
		    cut_cnt++;
		    FillCutflow(cut_cnt, "OneGoodPh");

		    if (!(ph_conversionType->at(selph_index1) == 0)) continue;
		    cut_cnt++;
		    FillCutflow(cut_cnt, "PhUnCvt");
		    
		    if (m_do_phmatch) {
		        if (!PhMatch()) continue;
		    }
		    cut_cnt++;
		    FillCutflow(cut_cnt, "PhMatch");
			
		    if (m_subregion == "ejets" || m_subregion == "mujets") {
			if (!(n_jet >= 4)) continue;
			cut_cnt++;
			FillCutflow(cut_cnt, "NjetGeq4");
		    } else if (m_subregion == "ee" || m_subregion == "emu" || m_subregion == "mumu") {
			if (!(n_jet >= 2)) continue;
			cut_cnt++;
			FillCutflow(cut_cnt, "NjetGeq2");
		    }
			
		    if (!(n_bjet >= 1)) continue;
		    cut_cnt++;
		    if (m_type != "Data" && m_type != "QCD" && m_use_weight) weight *= weight_btag;
		    FillCutflow(cut_cnt, "NbjetGeq1");
			
		    if (m_subregion == "ejets") {
		        if (!(fabs(ph_mgammalept->at(selph_index1) - m_zmass) > 5000.)) continue;
		    }
		    cut_cnt++;
		    FillCutflow(cut_cnt, "ZVeto");

		    if (m_subregion == "ee" || m_subregion == "mumu") {
		        if (!(event_mll < 85000 || event_mll > 95000)) continue;
		    }
		    cut_cnt++;
		    FillCutflow(cut_cnt, "MllZveto");
			
		    if (m_subregion == "ee" || m_subregion == "mumu") {
		        if (!(ph_mgammaleptlept->at(selph_index1) < 85000 || ph_mgammaleptlept->at(selph_index1) > 95000)) continue;
		    }
		    cut_cnt++;
		    FillCutflow(cut_cnt, "MllyZveto");

		    if (m_subregion == "ee" || m_subregion == "mumu") {
		        if (!(met_met > 30000.)) continue;
		    }
		    cut_cnt++;
		    FillCutflow(cut_cnt, "MET>30");

		    if (!(ph_drlept->at(selph_index1) > 1.0)) continue;
		    cut_cnt++;
		    FillCutflow(cut_cnt, "DrPhLep1.0");

		    if (m_do_klfitter) KLFit();
		} else if (m_selection == "CutSRStage0") {
		    if (m_subregion == "mujets" && !m_is2015) {
			if (!(leadlep_pt > 27500)) continue;
			cut_cnt++;
			FillCutflow(cut_cnt, "MuPt27.5");
		    }

		    if (!(event_ngoodphotons == 1)) continue;
		    cut_cnt++;
		    FillCutflow(cut_cnt, "OneGoodPh");
		    
		    if (m_do_phmatch) {
		        if (!PhMatch()) continue;
		    }
		    cut_cnt++;
		    FillCutflow(cut_cnt, "PhMatch");
		} else if (m_selection == "CutSRStage1") {
		    if (m_subregion == "mujets" && !m_is2015) {
			if (!(leadlep_pt > 27500)) continue;
			cut_cnt++;
			FillCutflow(cut_cnt, "MuPt27.5");
		    }

		    if (!(event_ngoodphotons == 1)) continue;
		    cut_cnt++;
		    FillCutflow(cut_cnt, "OneGoodPh");
		    
		    if (m_do_phmatch) {
		        if (!PhMatch()) continue;
		    }
		    cut_cnt++;
		    FillCutflow(cut_cnt, "PhMatch");
		} else if (m_selection == "CutSRStage2") {
		    if (m_subregion == "mujets" && !m_is2015) {
			if (!(leadlep_pt > 27500)) continue;
			cut_cnt++;
			FillCutflow(cut_cnt, "MuPt27.5");
		    }

		    if (!(event_ngoodphotons == 1)) continue;
		    cut_cnt++;
		    FillCutflow(cut_cnt, "OneGoodPh");
		    
		    if (m_do_phmatch) {
		        if (!PhMatch()) continue;
		    }
		    cut_cnt++;
		    FillCutflow(cut_cnt, "PhMatch");
			
		    if (m_subregion == "ejets" || m_subregion == "mujets") {
			if (!(n_jet >= 4)) continue;
			cut_cnt++;
			FillCutflow(cut_cnt, "NjetGeq4");
		    } else if (m_subregion == "ee" || m_subregion == "emu" || m_subregion == "mumu") {
			if (!(n_jet >= 2)) continue;
			cut_cnt++;
			FillCutflow(cut_cnt, "NjetGeq2");
		    }
		} else if (m_selection == "CutSRStage3") {
		    if (m_subregion == "mujets" && !m_is2015) {
			if (!(leadlep_pt > 27500)) continue;
			cut_cnt++;
			FillCutflow(cut_cnt, "MuPt27.5");
		    }

		    if (!(event_ngoodphotons == 1)) continue;
		    cut_cnt++;
		    FillCutflow(cut_cnt, "OneGoodPh");
		    
		    if (m_do_phmatch) {
		        if (!PhMatch()) continue;
		    }
		    cut_cnt++;
		    FillCutflow(cut_cnt, "PhMatch");
			
		    if (m_subregion == "ejets" || m_subregion == "mujets") {
			if (!(n_jet >= 4)) continue;
			cut_cnt++;
			FillCutflow(cut_cnt, "NjetGeq4");
		    } else if (m_subregion == "ee" || m_subregion == "emu" || m_subregion == "mumu") {
			if (!(n_jet >= 2)) continue;
			cut_cnt++;
			FillCutflow(cut_cnt, "NjetGeq2");
		    }
			
		    if (!(n_bjet >= 1)) continue;
		    cut_cnt++;
		    if (m_type != "Data" && m_type != "QCD" && m_use_weight) weight *= weight_btag;
		    FillCutflow(cut_cnt, "NbjetGeq1");
		} else if (m_selection == "CutSRStage4") {
		    if (m_subregion == "mujets" && !m_is2015) {
			if (!(leadlep_pt > 27500)) continue;
			cut_cnt++;
			FillCutflow(cut_cnt, "MuPt27.5");
		    }

		    if (!(event_ngoodphotons == 1)) continue;
		    cut_cnt++;
		    FillCutflow(cut_cnt, "OneGoodPh");
		    
		    if (m_do_phmatch) {
		        if (!PhMatch()) continue;
		    }
		    cut_cnt++;
		    FillCutflow(cut_cnt, "PhMatch");
			
		    if (m_subregion == "ejets" || m_subregion == "mujets") {
			if (!(n_jet >= 4)) continue;
			cut_cnt++;
			FillCutflow(cut_cnt, "NjetGeq4");
		    } else if (m_subregion == "ee" || m_subregion == "emu" || m_subregion == "mumu") {
			if (!(n_jet >= 2)) continue;
			cut_cnt++;
			FillCutflow(cut_cnt, "NjetGeq2");
		    }
			
		    if (!(n_bjet >= 1)) continue;
		    cut_cnt++;
		    if (m_type != "Data" && m_type != "QCD" && m_use_weight) weight *= weight_btag;
		    FillCutflow(cut_cnt, "NbjetGeq1");
			
		    if (m_subregion == "ejets") {
		        if (!(fabs(ph_mgammalept->at(selph_index1) - m_zmass) > 5000.)) continue;
		        cut_cnt++;
		        FillCutflow(cut_cnt, "ZVeto");
		    }
		} else if (m_selection == "CutSRStage5") {
		    if (m_subregion == "mujets" && !m_is2015) {
			if (!(leadlep_pt > 27500)) continue;
			cut_cnt++;
			FillCutflow(cut_cnt, "MuPt27.5");
		    }

		    if (!(event_ngoodphotons == 1)) continue;
		    cut_cnt++;
		    FillCutflow(cut_cnt, "OneGoodPh");
		    
		    if (m_do_phmatch) {
		        if (!PhMatch()) continue;
		    }
		    cut_cnt++;
		    FillCutflow(cut_cnt, "PhMatch");
			
		    if (m_subregion == "ejets" || m_subregion == "mujets") {
			if (!(n_jet >= 4)) continue;
			cut_cnt++;
			FillCutflow(cut_cnt, "NjetGeq4");
		    } else if (m_subregion == "ee" || m_subregion == "emu" || m_subregion == "mumu") {
			if (!(n_jet >= 2)) continue;
			cut_cnt++;
			FillCutflow(cut_cnt, "NjetGeq2");
		    }
			
		    if (!(n_bjet >= 1)) continue;
		    cut_cnt++;
		    if (m_type != "Data" && m_type != "QCD" && m_use_weight) weight *= weight_btag;
		    FillCutflow(cut_cnt, "NbjetGeq1");
			
		    if (m_subregion == "ejets") {
		        if (!(fabs(ph_mgammalept->at(selph_index1) - m_zmass) > 5000.)) continue;
		        cut_cnt++;
		        FillCutflow(cut_cnt, "ZVeto");
		    }

		    if (m_subregion == "ee" || m_subregion == "mumu") {
		        if (!(event_mll < 85000 || event_mll > 95000)) continue;
		        cut_cnt++;
		        FillCutflow(cut_cnt, "MllZveto");
		    }
		} else if (m_selection == "CutSRStage6") {
		    if (m_subregion == "mujets" && !m_is2015) {
			if (!(leadlep_pt > 27500)) continue;
			cut_cnt++;
			FillCutflow(cut_cnt, "MuPt27.5");
		    }

		    if (!(event_ngoodphotons == 1)) continue;
		    cut_cnt++;
		    FillCutflow(cut_cnt, "OneGoodPh");
		    
		    if (m_do_phmatch) {
		        if (!PhMatch()) continue;
		    }
		    cut_cnt++;
		    FillCutflow(cut_cnt, "PhMatch");
			
		    if (m_subregion == "ejets" || m_subregion == "mujets") {
			if (!(n_jet >= 4)) continue;
			cut_cnt++;
			FillCutflow(cut_cnt, "NjetGeq4");
		    } else if (m_subregion == "ee" || m_subregion == "emu" || m_subregion == "mumu") {
			if (!(n_jet >= 2)) continue;
			cut_cnt++;
			FillCutflow(cut_cnt, "NjetGeq2");
		    }
			
		    if (!(n_bjet >= 1)) continue;
		    cut_cnt++;
		    if (m_type != "Data" && m_type != "QCD" && m_use_weight) weight *= weight_btag;
		    FillCutflow(cut_cnt, "NbjetGeq1");
			
		    if (m_subregion == "ejets") {
		        if (!(fabs(ph_mgammalept->at(selph_index1) - m_zmass) > 5000.)) continue;
		        cut_cnt++;
		        FillCutflow(cut_cnt, "ZVeto");
		    }

		    if (m_subregion == "ee" || m_subregion == "mumu") {
		        if (!(event_mll < 85000 || event_mll > 95000)) continue;
		        cut_cnt++;
		        FillCutflow(cut_cnt, "MllZveto");
			
		        if (!(ph_mgammaleptlept->at(selph_index1) < 85000 || ph_mgammaleptlept->at(selph_index1) > 95000)) continue;
		        cut_cnt++;
		        FillCutflow(cut_cnt, "MllyZveto");
		    }
		} else if (m_selection == "CutSRStage7") {
		    if (m_subregion == "mujets" && !m_is2015) {
			if (!(leadlep_pt > 27500)) continue;
			cut_cnt++;
			FillCutflow(cut_cnt, "MuPt27.5");
		    }

		    if (!(event_ngoodphotons == 1)) continue;
		    cut_cnt++;
		    FillCutflow(cut_cnt, "OneGoodPh");
		    
		    if (m_do_phmatch) {
		        if (!PhMatch()) continue;
		    }
		    cut_cnt++;
		    FillCutflow(cut_cnt, "PhMatch");
			
		    if (m_subregion == "ejets" || m_subregion == "mujets") {
			if (!(n_jet >= 4)) continue;
			cut_cnt++;
			FillCutflow(cut_cnt, "NjetGeq4");
		    } else if (m_subregion == "ee" || m_subregion == "emu" || m_subregion == "mumu") {
			if (!(n_jet >= 2)) continue;
			cut_cnt++;
			FillCutflow(cut_cnt, "NjetGeq2");
		    }
			
		    if (!(n_bjet >= 1)) continue;
		    cut_cnt++;
		    if (m_type != "Data" && m_type != "QCD" && m_use_weight) weight *= weight_btag;
		    FillCutflow(cut_cnt, "NbjetGeq1");
			
		    if (m_subregion == "ejets") {
		        if (!(fabs(ph_mgammalept->at(selph_index1) - m_zmass) > 5000.)) continue;
		        cut_cnt++;
		        FillCutflow(cut_cnt, "ZVeto");
		    }

		    if (m_subregion == "ee" || m_subregion == "mumu") {
		        if (!(event_mll < 85000 || event_mll > 95000)) continue;
		        cut_cnt++;
		        FillCutflow(cut_cnt, "MllZveto");
			
		        if (!(ph_mgammaleptlept->at(selph_index1) < 85000 || ph_mgammaleptlept->at(selph_index1) > 95000)) continue;
		        cut_cnt++;
		        FillCutflow(cut_cnt, "MllyZveto");

		        if (!(met_met > 30000.)) continue;
		        cut_cnt++;
		        FillCutflow(cut_cnt, "MET>30");

		    }
		} else if (m_selection == "CutSRStage8") {
		    if (m_subregion == "mujets" && !m_is2015) {
			if (!(leadlep_pt > 27500)) continue;
			cut_cnt++;
			FillCutflow(cut_cnt, "MuPt27.5");
		    }

		    if (!(event_ngoodphotons == 1)) continue;
		    cut_cnt++;
		    FillCutflow(cut_cnt, "OneGoodPh");
		    
		    if (m_do_phmatch) {
		        if (!PhMatch()) continue;
		    }
		    cut_cnt++;
		    FillCutflow(cut_cnt, "PhMatch");
			
		    if (m_subregion == "ejets" || m_subregion == "mujets") {
			if (!(n_jet >= 4)) continue;
			cut_cnt++;
			FillCutflow(cut_cnt, "NjetGeq4");
		    } else if (m_subregion == "ee" || m_subregion == "emu" || m_subregion == "mumu") {
			if (!(n_jet >= 2)) continue;
			cut_cnt++;
			FillCutflow(cut_cnt, "NjetGeq2");
		    }
			
		    if (!(n_bjet >= 1)) continue;
		    cut_cnt++;
		    if (m_type != "Data" && m_type != "QCD" && m_use_weight) weight *= weight_btag;
		    FillCutflow(cut_cnt, "NbjetGeq1");
			
		    if (m_subregion == "ejets") {
		        if (!(fabs(ph_mgammalept->at(selph_index1) - m_zmass) > 5000.)) continue;
		        cut_cnt++;
		        FillCutflow(cut_cnt, "ZVeto");
		    }

		    if (m_subregion == "ee" || m_subregion == "mumu") {
		        if (!(event_mll < 85000 || event_mll > 95000)) continue;
		        cut_cnt++;
		        FillCutflow(cut_cnt, "MllZveto");
			
		        if (!(ph_mgammaleptlept->at(selph_index1) < 85000 || ph_mgammaleptlept->at(selph_index1) > 95000)) continue;
		        cut_cnt++;
		        FillCutflow(cut_cnt, "MllyZveto");

		        if (!(met_met > 30000.)) continue;
		        cut_cnt++;
		        FillCutflow(cut_cnt, "MET>30");

		    }
		    
		    if (!(ph_drlept->at(selph_index1) > 1.0)) continue;
		    cut_cnt++;
		    FillCutflow(cut_cnt, "DrPhLep1.0");
		} else if (m_selection == "CutWGammaVR") {
		    if (!(n_jet <= 3 && n_jet >= 1)) continue;
		    cut_cnt++;
		    FillCutflow(cut_cnt, "Njet1to3");
			
		    if (!(event_ngoodphotons == 1)) continue;
		    cut_cnt++;
		    FillCutflow(cut_cnt, "OneGoodPh");
		    
		    if (m_do_phmatch) {
		        if (!PhMatch()) continue;
		    }
		    cut_cnt++;
		    FillCutflow(cut_cnt, "PhMatch");
			
		    if (!(n_bjet == 0)) continue;
		    cut_cnt++;
		    if (m_type != "Data" && m_type != "QCD") weight *= weight_btag;
		    FillCutflow(cut_cnt, "Nbjet0");
			
		    if (m_subregion == "ejets") {
		        if (!(fabs(ph_mgammalept->at(selph_index1) - m_zmass) > 15000.)) continue;
		        cut_cnt++;
		        FillCutflow(cut_cnt, "ZVeto");
		    }
		} else if (m_selection == "CutWGammaVR2") {
		    if (!(n_jet <= 3 && n_jet >= 1)) continue;
		    cut_cnt++;
		    FillCutflow(cut_cnt, "Njet1to3");
			
		    if (!(event_ngoodphotons == 1)) continue;
		    cut_cnt++;
		    FillCutflow(cut_cnt, "OneGoodPh");
		    
		    if (m_do_phmatch) {
		        if (!PhMatch()) continue;
		    }
		    cut_cnt++;
		    FillCutflow(cut_cnt, "PhMatch");
			
		    if (!(n_bjet == 0)) continue;
		    cut_cnt++;
		    if (m_type != "Data" && m_type != "QCD") weight *= weight_btag;
		    FillCutflow(cut_cnt, "Nbjet0");
			
		    if (!(ph_mgammalept->at(selph_index1) < 50000. || ph_mgammalept->at(selph_index1) > 100000.)) continue;
		    cut_cnt++;
		    FillCutflow(cut_cnt, "ZVeto");
		} else if (m_selection == "CutWGammaVR3") {
		    if (!(n_jet <= 3 && n_jet >= 1)) continue;
		    cut_cnt++;
		    FillCutflow(cut_cnt, "Njet1to3");
			
		    if (!(event_ngoodphotons == 1)) continue;
		    cut_cnt++;
		    FillCutflow(cut_cnt, "OneGoodPh");
		    
		    if (m_do_phmatch) {
		        if (!PhMatch()) continue;
		    }
		    cut_cnt++;
		    FillCutflow(cut_cnt, "PhMatch");
			
		    if (!(n_bjet == 0)) continue;
		    cut_cnt++;
		    if (m_type != "Data" && m_type != "QCD") weight *= weight_btag;
		    FillCutflow(cut_cnt, "Nbjet0");
			
		    if (m_subregion == "ejets") {
			if (!(ph_mgammalept->at(selph_index1) < 20000. || ph_mgammalept->at(selph_index1) > 150000.)) continue;
		    } else {
			if (!(ph_mgammalept->at(selph_index1) < 50000. || ph_mgammalept->at(selph_index1) > 100000.)) continue;
		    }
		    cut_cnt++;
		    FillCutflow(cut_cnt, "ZVeto");

		    if (!(mwt > 30000.)) continue;
		    cut_cnt++;
		    FillCutflow(cut_cnt, "MWT");
		} else if (m_selection == "CutEGammaVR") {
		    if (!(n_jet >= 4)) continue;
		    cut_cnt++;
		    FillCutflow(cut_cnt, "NjetGeq4");

		    if (!(event_ngoodphotons == 1)) continue;
		    cut_cnt++;
		    FillCutflow(cut_cnt, "OneGoodPh");

		    if (m_do_phmatch) {
			if (!PhMatch()) continue;
		    }
		    cut_cnt++;
		    FillCutflow(cut_cnt, "PhMatch");
		    
		    if (m_subregion == "ejets") {
			if (fabs(mphl - m_zmass) > 5000.) continue;
			cut_cnt++;
			FillCutflow(cut_cnt, "ZVeto");
		    }
		} else if (m_selection == "CutCR") {
		    if (!(event_ngoodphotons == 1)) continue;
		    cut_cnt++;
		    FillCutflow(cut_cnt, "OneGoodPh");

		    if (m_do_phmatch) {
			if (!PhMatch()) continue;
		    }
		    cut_cnt++;
		    FillCutflow(cut_cnt, "PhMatch");

		    if (m_subregion == "ejets") {
			if (!(fabs(mphl - m_zmass) > 20000.)) continue;
		    }
		    cut_cnt++;
		    FillCutflow(cut_cnt, "ZVeto");

		    if (!(mwt < 50000.)) continue;
		    cut_cnt++;
		    FillCutflow(cut_cnt, "MWT<50");
		} else if (m_selection == "CutCR2") {
		    if (!(n_jet >= 2)) continue;
		    cut_cnt++;
		    FillCutflow(cut_cnt, "Njet>=2");

		    if (!(event_ngoodphotons == 1)) continue;
		    cut_cnt++;
		    FillCutflow(cut_cnt, "OneGoodPh");

		    if (m_do_phmatch) {
			if (!PhMatch()) continue;
		    }
		    cut_cnt++;
		    FillCutflow(cut_cnt, "PhMatch");
		} else if (m_selection == "CutQCD") {
		    if (!(event_ngoodphotons == 1)) continue;
		    cut_cnt++;
		    FillCutflow(cut_cnt, "OneGoodPh");

		    if (m_do_phmatch) {
			if (!PhMatch()) continue;
		    }
		    cut_cnt++;
		    FillCutflow(cut_cnt, "PhMatch");

		    if (!(n_jet >= 1 && n_jet <= 3)) continue;
		    cut_cnt++;
		    FillCutflow(cut_cnt, "Njet1to3");

		    if (!(n_bjet == 1)) continue;
		    cut_cnt++;
		    FillCutflow(cut_cnt, "NbjetEQ1");

		    if (m_subregion == "ejets") {
			if (!(fabs(mphl - m_zmass) > 10000.)) continue;
		    }
		    cut_cnt++;
		    FillCutflow(cut_cnt, "ZVeto");

		    if (m_subregion == "mujets") {
			if (!(mwt < 40000.)) continue;
		    }
		    cut_cnt++;
		    FillCutflow(cut_cnt, "MWT<40");
		} else if (m_selection == "CutQCD2") {
		    if (!(event_ngoodphotons == 1)) continue;
		    cut_cnt++;
		    FillCutflow(cut_cnt, "OneGoodPh");

		    if (m_do_phmatch) {
			if (!PhMatch()) continue;
		    }
		    cut_cnt++;
		    FillCutflow(cut_cnt, "PhMatch");

		    if (!(n_jet >= 1 && n_jet <= 3)) continue;
		    cut_cnt++;
		    FillCutflow(cut_cnt, "Njet1to3");

		    if (!(n_bjet == 1)) continue;
		    cut_cnt++;
		    FillCutflow(cut_cnt, "NbjetEQ1");

		    if (m_subregion == "ejets") {
			if (!(fabs(mphl - m_zmass) > 10000.)) continue;
		    }
		    cut_cnt++;
		    FillCutflow(cut_cnt, "ZVeto");

		    //if (!(mwt > 40000.)) continue;
		    //cut_cnt++;
		    //FillCutflow(cut_cnt, "MWT40");
		    
		    //if (!(met > 40000.)) continue;
		    //cut_cnt++;
		    //FillCutflow(cut_cnt, "MET40");

		    //if (!(leadph_mindrphl > 1.0)) continue;
		    //cut_cnt++;
		    //FillCutflow(cut_cnt, "DrPhLep1.0");
		} else if (m_selection == "CutQCD3") {
		    if (!(event_ngoodphotons == 1)) continue;
		    cut_cnt++;
		    FillCutflow(cut_cnt, "OneGoodPh");

		    if (m_do_phmatch) {
			if (!PhMatch()) continue;
		    }
		    cut_cnt++;
		    FillCutflow(cut_cnt, "PhMatch");

		    if (!(n_jet >= 1 && n_jet <= 3)) continue;
		    cut_cnt++;
		    FillCutflow(cut_cnt, "Njet1to3");

		    if (!(n_bjet == 0)) continue;
		    cut_cnt++;
		    FillCutflow(cut_cnt, "NbjetEQ1");

		    if (m_subregion == "ejets") {
			if (!(fabs(mphl - m_zmass) > 10000.)) continue;
		    }
		    cut_cnt++;
		    FillCutflow(cut_cnt, "ZVeto");
		} else if (m_selection == "CutQCD4") {
		    if (!(event_ngoodphotons == 1)) continue;
		    cut_cnt++;
		    FillCutflow(cut_cnt, "OneGoodPh");

		    if (m_do_phmatch) {
			if (!PhMatch()) continue;
		    }
		    cut_cnt++;
		    FillCutflow(cut_cnt, "PhMatch");

		    if (!(n_jet == 3)) continue;
		    cut_cnt++;
		    FillCutflow(cut_cnt, "Njet1to3");

		    if (!(n_bjet == 0)) continue;
		    cut_cnt++;
		    FillCutflow(cut_cnt, "NbjetEQ1");

		    if (!(mphl > 110000.)) continue;
		    cut_cnt++;
		    FillCutflow(cut_cnt, "ZVeto");

		    if (!(leadph_mindrphl < 2.5)) continue;
		    cut_cnt++;
		    FillCutflow(cut_cnt, "dRPhL<2.5");
		} else if (m_selection == "CutQCD5") {
		    if (!(n_jet >= 4)) continue;
		    cut_cnt++;
		    FillCutflow(cut_cnt, "NjetGeq4");

		    if (!(event_ngoodphotons == 1)) continue;
		    cut_cnt++;
		    FillCutflow(cut_cnt, "OneGoodPh");

		    if (m_do_phmatch) {
			if (!PhMatch()) continue;
		    }
		    cut_cnt++;
		    FillCutflow(cut_cnt, "PhMatch");

		    if (!(n_bjet == 0)) continue;
		    cut_cnt++;
		    FillCutflow(cut_cnt, "Nbjet0");
		    
		    if (!(mphl < 60000. || mphl > 100000)) continue;
		    FillCutflow(cut_cnt, "ZVetoPlus");

		    if (!(leadph_mindrphl > 1.0)) continue;
		    cut_cnt++;
		    FillCutflow(cut_cnt, "DrPhLep1.0");
		} else if (m_selection == "CutQCD6") {
		    if (!(n_jet == 3)) continue;
		    cut_cnt++;
		    FillCutflow(cut_cnt, "NjetEQ3");

		    if (!(event_ngoodphotons == 1)) continue;
		    cut_cnt++;
		    FillCutflow(cut_cnt, "OneGoodPh");

		    if (m_do_phmatch) {
			if (!PhMatch()) continue;
		    }
		    cut_cnt++;
		    FillCutflow(cut_cnt, "PhMatch");

		    if (!(n_bjet >= 1)) continue;
		    cut_cnt++;
		    FillCutflow(cut_cnt, "NbjetGEQ1");
		    
		    if (!(mphl < 60000. || mphl > 100000)) continue;
		    FillCutflow(cut_cnt, "ZVetoPlus");

		    if (!(leadph_mindrphl > 1.0)) continue;
		    cut_cnt++;
		    FillCutflow(cut_cnt, "DrPhLep1.0");
		} else if (m_selection == "CutWg") {
		    if (!(event_ngoodphotons == 1)) continue;
		    cut_cnt++;
		    FillCutflow(cut_cnt, "OneGoodPh");

		    if (m_do_phmatch) {
			if (!PhMatch()) continue;
		    }
		    cut_cnt++;
		    FillCutflow(cut_cnt, "PhMatch");

		    if (!(n_jet >= 1 && n_jet <= 3)) continue;
		    cut_cnt++;
		    FillCutflow(cut_cnt, "Njet1to3");

		    if (!(n_bjet == 1)) continue;
		    cut_cnt++;
		    FillCutflow(cut_cnt, "NbjetEQ1");

		    if (m_subregion == "ejets") {
			if (!(fabs(mphl - m_zmass) > 10000.)) continue;
		    }
		    cut_cnt++;
		    FillCutflow(cut_cnt, "ZVeto");

		    if (m_subregion == "mujets") {
			if (!(mwt < 40000.)) continue;
		    }
		    cut_cnt++;
		    FillCutflow(cut_cnt, "MWT<40");
		} else if (m_selection == "Cut2") {
		    if (!(event_ngoodphotons == 1)) continue;
		    cut_cnt++;
		    FillCutflow(cut_cnt, "OneGoodPh");
		    
		    if (!(n_bjet77 >= 1)) continue;
		    cut_cnt++;
		    FillCutflow(cut_cnt, "Nbjet77GEQ1");

		    if (!(met > 30000.)) continue;
		    cut_cnt++;
		    FillCutflow(cut_cnt, "MET30");

		    if (m_subregion == "ee" || m_subregion == "mumu") {
			if (!(fabs(mll - m_zmass) > 5000.)) continue;
			if (!(fabs(mllg - m_zmass) > 5000.)) continue;
		    }
		    cut_cnt++;
		    FillCutflow(cut_cnt, "ZVeto");
		} else if (m_selection == "Cut201") {
		    if (!(event_ngoodphotons == 1)) continue;
		    cut_cnt++;
		    FillCutflow(cut_cnt, "OneGoodPh");
		    
		    if (!(n_bjet77 >= 1)) continue;
		    cut_cnt++;
		    FillCutflow(cut_cnt, "Nbjet77GEQ1");

		    if (!(met > 30000.)) continue;
		    cut_cnt++;
		    FillCutflow(cut_cnt, "MET30");

		} else if (m_selection == "Cut202") {
		    if (!(event_ngoodphotons == 1)) continue;
		    cut_cnt++;
		    FillCutflow(cut_cnt, "OneGoodPh");
		    
		    if (!(n_bjet77 >= 1)) continue;
		    cut_cnt++;
		    FillCutflow(cut_cnt, "Nbjet77GEQ1");

		    if (!(met > 30000.)) continue;
		    cut_cnt++;
		    FillCutflow(cut_cnt, "MET30");

		    if (m_subregion == "ee" || m_subregion == "mumu") {
			if (!(fabs(mll - m_zmass) > 5000.)) continue;
		    }
		    cut_cnt++;
		    FillCutflow(cut_cnt, "ZVeto");
		} else if (m_selection == "CutZee") {
		    if (zee_2015) {
		        if ((!leadlep_HLT_e24_lhmedium_L1EM20VH || leadlep_HLT_e60_lhmedium || leadlep_HLT_e120_lhloose)) continue;
		    } else if (zee_2016) {
		        if (!(leadlep_HLT_e26_lhtight_nod0_ivarloose || leadlep_HLT_e60_lhmedium_nod0 || leadlep_HLT_e140_lhloose_nod0)) continue;
		    }
		    cut_cnt++;
		    FillCutflow(cut_cnt, "TriggerMatch");

		    if (!el_isoGradient->at(0) && el_isoGradient->at(1)) continue;
		    cut_cnt++;
		    FillCutflow(cut_cnt, "ElIso");

		    if (!(fabs(sublep_eta) < 2.37 && (fabs(sublep_eta) < 1.37 || fabs(sublep_eta) > 1.52))) continue; 
		    cut_cnt++;
		    FillCutflow(cut_cnt, "SubLepEtaAsPh");
		} else if (m_selection == "CutZeey") {
		    if (zee_2015) {
		        if ((!leadlep_HLT_e24_lhmedium_L1EM20VH || leadlep_HLT_e60_lhmedium || leadlep_HLT_e120_lhloose)) continue;
		    } else if (zee_2016) {
		        if (!(leadlep_HLT_e26_lhtight_nod0_ivarloose || leadlep_HLT_e60_lhmedium_nod0 || leadlep_HLT_e140_lhloose_nod0)) continue;
		    }
		    cut_cnt++;
		    FillCutflow(cut_cnt, "TriggerMatch");

		    if (!el_isoGradient->at(0) && el_isoGradient->at(1)) continue;
		    cut_cnt++;
		    FillCutflow(cut_cnt, "ElIso");

		    if (!(event_ngoodphotons >= 1)) continue;
		    cut_cnt++;
		    FillCutflow(cut_cnt, "GoodPh>=1");
		} else if (m_selection == "CutZeeReverse") {
		    if (zee_2015) {
		        if ((!sublep_HLT_e24_lhmedium_L1EM20VH || sublep_HLT_e60_lhmedium || sublep_HLT_e120_lhloose)) continue;
		    } else if (zee_2016) {
		        if (!(sublep_HLT_e26_lhtight_nod0_ivarloose || sublep_HLT_e60_lhmedium_nod0 || sublep_HLT_e140_lhloose_nod0)) continue;
		    }
		    cut_cnt++;
		    FillCutflow(cut_cnt, "TriggerMatch");

		    if (!el_isoGradient->at(0) && el_isoGradient->at(1)) continue;
		    cut_cnt++;
		    FillCutflow(cut_cnt, "ElIso");

		    if (!(fabs(leadlep_eta) < 2.37 && (fabs(leadlep_eta) < 1.37 || fabs(leadlep_eta) > 1.52))) continue; 
		    cut_cnt++;
		    FillCutflow(cut_cnt, "LeadLepEtaAsPh");

		    if (zee_2015) {
			if (!(sublep_pt > 25000.)) continue;
		    } else if (zee_2016) {
			if (!(sublep_pt > 27000.)) continue;
		    }
		    cut_cnt++;
		    FillCutflow(cut_cnt, "SubLepPt27");
		} else if (m_selection == "CutZeg") {
		    if (!el_isoGradient->at(0)) continue;
		    cut_cnt++;
		    FillCutflow(cut_cnt, "ElIso");

		    if (!(event_ngoodphotons >= 1)) continue;
		    cut_cnt++;
		    FillCutflow(cut_cnt, "GoodPh>=1");

		    if (!(leadph_pt > 25000.)) continue;
		    cut_cnt++;
		    FillCutflow(cut_cnt, "PhPt>25");
		    
		    if (!(leadlep_pt > leadph_pt)) continue;
		    cut_cnt++;
		    FillCutflow(cut_cnt, "ElPt>PhPt");

		    if (m_do_phmatch) {
			if (!PhMatch()) continue;
		    }
		    cut_cnt++;
		    FillCutflow(cut_cnt, "PhMatch");
		} else if (m_selection == "CutZegReverse") {
		    if (!el_isoGradient->at(0)) continue;
		    cut_cnt++;
		    FillCutflow(cut_cnt, "ElIso");

		    if (!(event_ngoodphotons >= 1)) continue;
		    cut_cnt++;
		    FillCutflow(cut_cnt, "GoodPh>=1");

		    if (!(leadph_pt > 25000.)) continue;
		    cut_cnt++;
		    FillCutflow(cut_cnt, "PhPt>25");
		    
		    if (!(leadlep_pt <= leadph_pt)) continue;
		    cut_cnt++;
		    FillCutflow(cut_cnt, "ElPt<PhPt");

		    if (m_do_phmatch) {
			if (!PhMatch()) continue;
		    }
		    cut_cnt++;
		    FillCutflow(cut_cnt, "PhMatch");
		} else if (m_selection == "CutZegCvt") {
		    if (!el_isoGradient->at(0)) continue;
		    cut_cnt++;
		    FillCutflow(cut_cnt, "ElIso");

		    if (!(event_ngoodphotons >= 1)) continue;
		    cut_cnt++;
		    FillCutflow(cut_cnt, "GoodPh>=1");

		    if (!(leadph_cvt != 0)) continue;
		    cut_cnt++;
		    FillCutflow(cut_cnt, "PhIsCvt");

		    if (!(leadph_pt > 25000.)) continue;
		    cut_cnt++;
		    FillCutflow(cut_cnt, "PhPt>25");
		    
		    if (!(leadlep_pt > leadph_pt)) continue;
		    cut_cnt++;
		    FillCutflow(cut_cnt, "ElPt>PhPt");

		    if (m_do_phmatch) {
			if (!PhMatch()) continue;
		    }
		    cut_cnt++;
		    FillCutflow(cut_cnt, "PhMatch");
		} else if (m_selection == "CutZegCvtReverse") {
		    if (!el_isoGradient->at(0)) continue;
		    cut_cnt++;
		    FillCutflow(cut_cnt, "ElIso");

		    if (!(event_ngoodphotons >= 1)) continue;
		    cut_cnt++;
		    FillCutflow(cut_cnt, "GoodPh>=1");

		    if (!(leadph_cvt != 0)) continue;
		    cut_cnt++;
		    FillCutflow(cut_cnt, "PhIsCvt");

		    if (!(leadph_pt > 25000.)) continue;
		    cut_cnt++;
		    FillCutflow(cut_cnt, "PhPt>25");
		    
		    if (!(leadlep_pt <= leadph_pt)) continue;
		    cut_cnt++;
		    FillCutflow(cut_cnt, "ElPt<PhPt");

		    if (m_do_phmatch) {
			if (!PhMatch()) continue;
		    }
		    cut_cnt++;
		    FillCutflow(cut_cnt, "PhMatch");
		} else if (m_selection == "CutZegUnCvt") {
		    if (!el_isoGradient->at(0)) continue;
		    cut_cnt++;
		    FillCutflow(cut_cnt, "ElIso");

		    if (!(event_ngoodphotons >= 1)) continue;
		    cut_cnt++;
		    FillCutflow(cut_cnt, "GoodPh>=1");

		    if (!(leadph_cvt == 0)) continue;
		    cut_cnt++;
		    FillCutflow(cut_cnt, "PhIsCvt");

		    if (!(leadph_pt > 25000.)) continue;
		    cut_cnt++;
		    FillCutflow(cut_cnt, "PhPt>25");
		    
		    if (!(leadlep_pt > leadph_pt)) continue;
		    cut_cnt++;
		    FillCutflow(cut_cnt, "ElPt>PhPt");

		    if (m_do_phmatch) {
			if (!PhMatch()) continue;
		    }
		    cut_cnt++;
		    FillCutflow(cut_cnt, "PhMatch");
		} else if (m_selection == "CutZegUnCvtReverse") {
		    if (!el_isoGradient->at(0)) continue;
		    cut_cnt++;
		    FillCutflow(cut_cnt, "ElIso");

		    if (!(event_ngoodphotons >= 1)) continue;
		    cut_cnt++;
		    FillCutflow(cut_cnt, "GoodPh>=1");

		    if (!(leadph_cvt == 0)) continue;
		    cut_cnt++;
		    FillCutflow(cut_cnt, "PhIsCvt");

		    if (!(leadph_pt > 25000.)) continue;
		    cut_cnt++;
		    FillCutflow(cut_cnt, "PhPt>25");
		    
		    if (!(leadlep_pt <= leadph_pt)) continue;
		    cut_cnt++;
		    FillCutflow(cut_cnt, "ElPt<PhPt");

		    if (m_do_phmatch) {
			if (!PhMatch()) continue;
		    }
		    cut_cnt++;
		    FillCutflow(cut_cnt, "PhMatch");
		} else if (m_selection == "CutZegNoTight") {
		    if (!(leadlep_pt > leadph_pt)) continue;
		    cut_cnt++;
		    FillCutflow(cut_cnt, "ElPtOverPhPt");

		    if (m_do_phmatch) {
			if (!PhMatch()) continue;
		    }
		    cut_cnt++;
		    FillCutflow(cut_cnt, "PhMatch");
		} else if (m_selection == "CutZegNoPtOrder") {
		    if (!leadph_tight) continue;
		    cut_cnt++;
		    FillCutflow(cut_cnt, "PhTight");

		    if (m_do_phmatch) {
			if (!PhMatch()) continue;
		    }
		    cut_cnt++;
		    FillCutflow(cut_cnt, "PhMatch");
		} else if (m_selection == "CutTTee") {
		    if (ttel_ee_2015) {
		        if ((!leadlep_HLT_e24_lhmedium_L1EM20VH || leadlep_HLT_e60_lhmedium || leadlep_HLT_e120_lhloose)) continue;
		    } else if (ttel_ee_2016) {
		        if (!(leadlep_HLT_e26_lhtight_nod0_ivarloose || leadlep_HLT_e60_lhmedium_nod0 || leadlep_HLT_e140_lhloose_nod0)) continue;
		    }
		    cut_cnt++;
		    FillCutflow(cut_cnt, "TriggerMatch");

		    if (!el_isoGradient->at(0) && el_isoGradient->at(1)) continue;
		    cut_cnt++;
		    FillCutflow(cut_cnt, "ElIso");

		    if (!(fabs(sublep_eta) < 2.37 && (fabs(sublep_eta) < 1.37 || fabs(sublep_eta) > 1.52))) continue; 
		    cut_cnt++;
		    FillCutflow(cut_cnt, "SubLepEtaAsPh");

		    if (!(n_jet >= 4)) continue;
		    cut_cnt++;
		    FillCutflow(cut_cnt, "NjetGeq4");
			
		    if (!(n_bjet >= 1)) continue;
		    cut_cnt++;
		    if (m_type != "Data" && m_type != "QCD") weight *= weight_btag;
		    FillCutflow(cut_cnt, "NbjetGeq1");
			
		    if (!(fabs(event_mll - m_zmass) > 5000.)) continue;
		    cut_cnt++;
		    FillCutflow(cut_cnt, "ZVeto");

		    if (!(drleplep > 1.0)) continue;
		    cut_cnt++;
		    FillCutflow(cut_cnt, "DrLepLep1.0");
		} else if (m_selection == "CutTTeeReverse") {
		    if (ttel_ee_2015) {
		        if ((!sublep_HLT_e24_lhmedium_L1EM20VH || sublep_HLT_e60_lhmedium || sublep_HLT_e120_lhloose)) continue;
		    } else if (ttel_ee_2016) {
		        if (!(sublep_HLT_e26_lhtight_nod0_ivarloose || sublep_HLT_e60_lhmedium_nod0 || sublep_HLT_e140_lhloose_nod0)) continue;
		    }
		    cut_cnt++;
		    FillCutflow(cut_cnt, "TriggerMatch");

		    if (!el_isoGradient->at(0) && el_isoGradient->at(1)) continue;
		    cut_cnt++;
		    FillCutflow(cut_cnt, "ElIso");

		    if (zee_2015) {
			if (!(sublep_pt > 25000.)) continue;
		    } else if (zee_2016) {
			if (!(sublep_pt > 27000.)) continue;
		    }
		    cut_cnt++;
		    FillCutflow(cut_cnt, "SubLepPt27");

		    if (!(fabs(leadlep_eta) < 2.37 && (fabs(leadlep_eta) < 1.37 || fabs(leadlep_eta) > 1.52))) continue; 
		    cut_cnt++;
		    FillCutflow(cut_cnt, "LeadLepEtaAsPh");

		    if (!(n_jet >= 4)) continue;
		    cut_cnt++;
		    FillCutflow(cut_cnt, "NjetGeq4");
			
		    if (!(n_bjet >= 1)) continue;
		    cut_cnt++;
		    if (m_type != "Data" && m_type != "QCD") weight *= weight_btag;
		    FillCutflow(cut_cnt, "NbjetGeq1");
			
		    if (!(fabs(event_mll - m_zmass) > 5000.)) continue;
		    cut_cnt++;
		    FillCutflow(cut_cnt, "ZVeto");

		    if (!(drleplep > 1.0)) continue;
		    cut_cnt++;
		    FillCutflow(cut_cnt, "DrLepLep1.0");
		} else if (m_selection == "CutTTmue") {
		    if (!el_isoGradient->at(0)) continue;
		    cut_cnt++;
		    FillCutflow(cut_cnt, "ElIso");

		    if (!(fabs(sublep_eta) < 2.37 && (fabs(sublep_eta) < 1.37 || fabs(sublep_eta) > 1.52))) continue; 
		    cut_cnt++;
		    FillCutflow(cut_cnt, "SubLepEtaAsPh");

		    if (!(leadmu_pt > leadel_pt)) continue;
		    cut_cnt++;
		    FillCutflow(cut_cnt, "MuPt>ElPt");

		    if (!(n_jet >= 4)) continue;
		    cut_cnt++;
		    FillCutflow(cut_cnt, "NjetGeq4");
			
		    if (!(n_bjet >= 1)) continue;
		    cut_cnt++;
		    if (m_type != "Data" && m_type != "QCD") weight *= weight_btag;
		    FillCutflow(cut_cnt, "NbjetGeq1");
			
		    if (!(drleplep > 1.0)) continue;
		    cut_cnt++;
		    FillCutflow(cut_cnt, "DrLepLep1.0");
		} else if (m_selection == "CutTTmueReverse") {
		    if (!el_isoGradient->at(0)) continue;
		    cut_cnt++;
		    FillCutflow(cut_cnt, "ElIso");

		    if (!(fabs(leadlep_eta) < 2.37 && (fabs(leadlep_eta) < 1.37 || fabs(leadlep_eta) > 1.52))) continue; 
		    cut_cnt++;
		    FillCutflow(cut_cnt, "LeadLepEtaAsPh");

		    if (!(leadmu_pt <= leadel_pt)) continue;
		    cut_cnt++;
		    FillCutflow(cut_cnt, "ElPt>MuPt");

		    if (!(n_jet >= 4)) continue;
		    cut_cnt++;
		    FillCutflow(cut_cnt, "NjetGeq4");
			
		    if (!(n_bjet >= 1)) continue;
		    cut_cnt++;
		    if (m_type != "Data" && m_type != "QCD") weight *= weight_btag;
		    FillCutflow(cut_cnt, "NbjetGeq1");
			
		    if (!(drleplep > 1.0)) continue;
		    cut_cnt++;
		    FillCutflow(cut_cnt, "DrLepLep1.0");
		} else if (m_selection == "CutTTeg") {
		    if (!(event_ngoodphotons >= 1)) continue;
		    cut_cnt++;
		    FillCutflow(cut_cnt, "GoodPh>=1");

		    if (!(leadph_pt > 25000.)) continue;
		    cut_cnt++;
		    FillCutflow(cut_cnt, "PhPt>25");

		    if (!(leadlep_pt > leadph_pt)) continue;
		    cut_cnt++;
		    FillCutflow(cut_cnt, "ElPt>PhPt");

		    if (!(n_jet >= 4)) continue;
		    cut_cnt++;
		    FillCutflow(cut_cnt, "NjetGeq4");
			
		    if (!(n_bjet >= 1)) continue;
		    cut_cnt++;
		    if (m_type != "Data" && m_type != "QCD") weight *= weight_btag;
		    FillCutflow(cut_cnt, "NbjetGeq1");
			
		    if (!(fabs(ph_mgammalept->at(selph_index1) - m_zmass) > 5000.)) continue;
		    cut_cnt++;
		    FillCutflow(cut_cnt, "ZVeto");

		    if (!(ph_drlept->at(selph_index1) > 1.0)) continue;
		    cut_cnt++;
		    FillCutflow(cut_cnt, "DrPhLep1.0");

		    if (m_do_phmatch) {
			if (!PhMatch()) continue;
		    }
		    cut_cnt++;
		    FillCutflow(cut_cnt, "PhMatch");
		} else if (m_selection == "CutTTegReverse") {
		    if (!(event_ngoodphotons >= 1)) continue;
		    cut_cnt++;
		    FillCutflow(cut_cnt, "GoodPh>=1");

		    if (ejets_2015) {
			if (!(leadph_pt > 25000.)) continue;
		    } else if (ejets_2016) {
			if (!(leadph_pt > 27000.)) continue;
		    }
		    cut_cnt++;
		    FillCutflow(cut_cnt, "SubLepPt27");

		    if (!(leadlep_pt < leadph_pt)) continue;
		    cut_cnt++;
		    FillCutflow(cut_cnt, "ElPt<PhPt");

		    if (!(n_jet >= 4)) continue;
		    cut_cnt++;
		    FillCutflow(cut_cnt, "NjetGeq4");
			
		    if (!(n_bjet >= 1)) continue;
		    cut_cnt++;
		    if (m_type != "Data" && m_type != "QCD") weight *= weight_btag;
		    FillCutflow(cut_cnt, "NbjetGeq1");
			
		    if (!(fabs(ph_mgammalept->at(selph_index1) - m_zmass) > 5000.)) continue;
		    cut_cnt++;
		    FillCutflow(cut_cnt, "ZVeto");

		    if (!(ph_drlept->at(selph_index1) > 1.0)) continue;
		    cut_cnt++;
		    FillCutflow(cut_cnt, "DrPhLep1.0");

		    if (m_do_phmatch) {
			if (!PhMatch()) continue;
		    }
		    cut_cnt++;
		    FillCutflow(cut_cnt, "PhMatch");
		} else if (m_selection == "CutTTmug") {
		    if (!(event_ngoodphotons >= 1)) continue;
		    cut_cnt++;
		    FillCutflow(cut_cnt, "GoodPh>=1");

		    if (!(leadph_pt > 25000.)) continue;
		    cut_cnt++;
		    FillCutflow(cut_cnt, "PhPt>25");

		    if (!(leadlep_pt > leadph_pt)) continue;
		    cut_cnt++;
		    FillCutflow(cut_cnt, "ElPt>PhPt");

		    if (!(n_jet >= 4)) continue;
		    cut_cnt++;
		    FillCutflow(cut_cnt, "NjetGeq4");
			
		    if (!(n_bjet >= 1)) continue;
		    cut_cnt++;
		    if (m_type != "Data" && m_type != "QCD") weight *= weight_btag;
		    FillCutflow(cut_cnt, "NbjetGeq1");
			
		    if (!(ph_drlept->at(selph_index1) > 1.0)) continue;
		    cut_cnt++;
		    FillCutflow(cut_cnt, "DrPhLep1.0");

		    if (m_do_phmatch) {
			if (!PhMatch()) continue;
		    }
		    cut_cnt++;
		    FillCutflow(cut_cnt, "PhMatch");
		} else if (m_selection == "CutTTmugReverse") {
		    if (!(event_ngoodphotons >= 1)) continue;
		    cut_cnt++;
		    FillCutflow(cut_cnt, "GoodPh>=1");

		    if (!(leadph_pt > 25000.)) continue;
		    cut_cnt++;
		    FillCutflow(cut_cnt, "SubLepPt25");

		    if (!(leadlep_pt < leadph_pt)) continue;
		    cut_cnt++;
		    FillCutflow(cut_cnt, "MuPt>PhPt");

		    if (!(n_jet >= 4)) continue;
		    cut_cnt++;
		    FillCutflow(cut_cnt, "NjetGeq4");
			
		    if (!(n_bjet >= 1)) continue;
		    cut_cnt++;
		    if (m_type != "Data" && m_type != "QCD") weight *= weight_btag;
		    FillCutflow(cut_cnt, "NbjetGeq1");
			
		    if (!(ph_drlept->at(selph_index1) > 1.0)) continue;
		    cut_cnt++;
		    FillCutflow(cut_cnt, "DrPhLep1.0");

		    if (m_do_phmatch) {
			if (!PhMatch()) continue;
		    }
		    cut_cnt++;
		    FillCutflow(cut_cnt, "PhMatch");
		} else {
		    m_lg->Err("ss", "Undefined cut ->", m_selection.c_str());
		   exit(-1);
    	    	}
            	
    	    	// fill histograms
    	    	FillHistogram();

		ReleaseEventMemory();
            }
	    m_lg->ReduShift();
	
	    if (m_file) {
    	        m_file->Close();
    	        delete m_file;
    	    }
        }
    
        cout << fixed << setprecision(1);
	if (!oneevt) {
	    m_lg->NewLine();
	    m_lg->Info("s", "END loop.");
            m_lg->Info("s", "------------- Summary ---------- ");
            m_lg->Info("ss", "Region ->", m_region.c_str());
            m_lg->Info("ss", "SubRegion ->", m_subregion.c_str());
            m_lg->Info("ss", "Variation ->", m_variation.c_str());
            m_lg->Info("ss", "Selection ->", m_selection.c_str());
            if (m_type != "Data" && m_type != "QCD") m_lg->Info("ss", "Process ->", m_process.c_str());
            m_lg->Info("ss", "Type ->", m_type.c_str());
            if (badweight != 0) m_lg->Warn("sd", "Number of bad event weight ->", badweight);
            m_lg->Info("s", "-------------------------------- ");
            m_lg->Info("s", "------------- Cutflow ---------- ");
            PrintCutflow(h_Cutflow);
            h_Cutflow->GetXaxis()->SetBinLabel(n_cuts_end+1, "STOP");
	    for (int i = 0; i < h_Cutflow_QCD.size(); i++) {
	        h_Cutflow_QCD.at(i)->GetXaxis()->SetBinLabel(n_cuts_end+1, "STOP");
	    }
            m_lg->Info("s", "-------------------------------- ");
	}
    
        SaveResults();
    
        ReleaseMemory();
        m_lg->ReduShift();
    }

    m_lg->NewLine();
    m_lg->Info("s", "Loop ended.");

    if (oneevt) {
	m_lg->Info("s", "xsec summary");
	double totxsec = 0;
	for (int i = 0; i < m_ids.size(); i++) {
	    totxsec += m_xsecs.at(i);
	    m_lg->Info("sff", m_ids.at(i).c_str(), m_xsecs.at(i), m_sumws.at(i));
	}
	m_lg->Info("sf", "Total xsec", totxsec);
    }
}

void SampleAnalyzor::ReleaseEventMemory() {
    // KLFitter
    if (m_do_klfitter) {
	//for (int i = 0; i < m_vjet_klfitter.size(); i++) {
	//    delete m_vjet_klfitter.at(i);
	//}
	//if (m_ph_klfitter) delete m_ph_klfitter;
	//if (m_el_klfitter) delete m_el_klfitter;
	//if (m_mu_klfitter) delete m_mu_klfitter;
    }
}

void SampleAnalyzor::ReleaseMemory() {
    // cutflow
    if (h_Cutflow) delete h_Cutflow;
    for (int i = 0; i < h_Cutflow_QCD.size(); i++) {
	if (h_Cutflow_QCD.at(i)) delete h_Cutflow_QCD.at(i);
	if (h_LeadLepPt_QCD.at(i)) delete h_LeadLepPt_QCD.at(i);
	if (h_LeadJetPt_QCD.at(i)) delete h_LeadJetPt_QCD.at(i);
	if (h_LeadPhPt_QCD.at(i)) delete h_LeadPhPt_QCD.at(i);
	if (h_MWT_QCD.at(i)) delete h_MWT_QCD.at(i);
	if (h_MET_QCD.at(i)) delete h_MET_QCD.at(i);
	if (h_dPhiLepMET_QCD.at(i)) delete h_dPhiLepMET_QCD.at(i);
    }
    if (m_type == "Data" && m_do_bootstrap) {
	for (int i = 0; i < 1000; i++) {
	    delete h_bootstrap_LeadPhPt.at(i);
	    delete h_bootstrap_LeadPhEta.at(i);
	    delete h_bootstrap_LeadPhMinDrPhLep.at(i);
	    delete h_bootstrap_DEtaLepLep.at(i);
	    delete h_bootstrap_DPhiLepLep.at(i);
	}
    }

//    if (m_do_klfitter) {
//	delete h_best_CA;
//	delete h_best_lh_best;
//	delete h_best_ph_lh_best;
//	delete h_best_top_pole_mass;
//	delete h_best_reco_hW_mass;
//	delete h_best_reco_lW_mass;
//	delete h_best_reco_htop_mass;
//	delete h_best_reco_htop_pt;
//	delete h_best_reco_htop_eta;
//	delete h_best_reco_ltop_mass;
//	delete h_best_reco_ltop_pt;
//	delete h_best_reco_ltop_eta;
//
//	delete h_CA;
//	delete h_lh_best;
//	delete h_top_pole_mass;
//	delete h_reco_hW_mass;
//	delete h_reco_lW_mass;
//	delete h_reco_htop_mass;
//	delete h_reco_htop_pt;
//	delete h_reco_htop_eta;
//	delete h_reco_ltop_mass;
//	delete h_reco_ltop_pt;
//	delete h_reco_ltop_eta;
//
//	delete h_lt_CA;
//	delete h_lt_lh_best;
//	delete h_lt_top_pole_mass;
//	delete h_lt_reco_hW_mass;
//	delete h_lt_reco_lW_mass;
//	delete h_lt_reco_htop_mass;
//	delete h_lt_reco_htop_pt;
//	delete h_lt_reco_htop_eta;
//	delete h_lt_reco_ltop_mass;
//	delete h_lt_reco_ltop_pt;
//	delete h_lt_reco_ltop_eta;
//
//	delete h_ht_CA;
//	delete h_ht_lh_best;
//	delete h_ht_top_pole_mass;
//	delete h_ht_reco_hW_mass;
//	delete h_ht_reco_lW_mass;
//	delete h_ht_reco_htop_mass;
//	delete h_ht_reco_htop_pt;
//	delete h_ht_reco_htop_eta;
//	delete h_ht_reco_ltop_mass;
//	delete h_ht_reco_ltop_pt;
//	delete h_ht_reco_ltop_eta;
//
//	delete h_lW_CA;
//	delete h_lW_lh_best;
//	delete h_lW_top_pole_mass;
//	delete h_lW_reco_hW_mass;
//	delete h_lW_reco_lW_mass;
//	delete h_lW_reco_htop_mass;
//	delete h_lW_reco_htop_pt;
//	delete h_lW_reco_htop_eta;
//	delete h_lW_reco_ltop_mass;
//	delete h_lW_reco_ltop_pt;
//	delete h_lW_reco_ltop_eta;
//
//	delete h_hW_CA;
//	delete h_hW_lh_best;
//	delete h_hW_top_pole_mass;
//	delete h_hW_reco_hW_mass;
//	delete h_hW_reco_lW_mass;
//	delete h_hW_reco_htop_mass;
//	delete h_hW_reco_htop_pt;
//	delete h_hW_reco_htop_eta;
//	delete h_hW_reco_ltop_mass;
//	delete h_hW_reco_ltop_pt;
//	delete h_hW_reco_ltop_eta;
//
//	delete h2_CA;
//	delete h2_lh_best;
//	delete h2_top_pole_mass;
//	delete h2_reco_hW_mass;
//	delete h2_reco_lW_mass;
//	delete h2_reco_htop_mass;
//	delete h2_reco_htop_pt;
//	delete h2_reco_htop_eta;
//	delete h2_reco_ltop_mass;
//	delete h2_reco_ltop_pt;
//	delete h2_reco_ltop_eta;
//
//	delete h2_lh_ph_lh_best;
//
//	delete m_klfitter;
//	delete m_klfitter_lt;
//	delete m_klfitter_ht;
//	delete m_detector;
//	delete m_likelihood;
//	delete m_likelihood_lt;
//	delete m_likelihood_ht;
//	delete m_particles;
//	delete m_particles2;
//    }

    // histograms
    m_lg->Info("s", "Release memory.");
    for (int i = 0; i < m_hists.size(); i++) {
	if (m_hists.at(i)) delete m_hists.at(i);
    }
    for (int i = 0; i < m_2Dhists.size(); i++) {
	if (m_2Dhists.at(i)) delete m_2Dhists.at(i);
    }

    m_lg->Info("s", "Memory released.");
}

void SampleAnalyzor::SaveResults() {
    m_lg->Info("ss", "Save results to -->", m_saveto.c_str());
    m_lg->Info("ss", "Save option -->", m_saveopt.c_str());

    TFile f(m_saveto.c_str(), m_saveopt.c_str());
    m_saveopt = "update";
    //if (m_saveopt == "recreate") m_saveopt = "update";

    // cutflow
    h_Cutflow->Write();

    for (int i = 0; i < h_Cutflow_QCD.size(); i++) {
	h_Cutflow_QCD.at(i)->Write();
	h_LeadLepPt_QCD.at(i)->Write();
	h_LeadJetPt_QCD.at(i)->Write();
	h_LeadPhPt_QCD.at(i)->Write();
	h_MWT_QCD.at(i)->Write();
	h_MET_QCD.at(i)->Write();
	h_dPhiLepMET_QCD.at(i)->Write();
    }

    if (m_type == "Data" && m_do_bootstrap) {
	for (int i = 0; i < 1000; i++) {
	    h_bootstrap_LeadPhPt.at(i)->Write();
	    h_bootstrap_LeadPhEta.at(i)->Write();
	    h_bootstrap_LeadPhMinDrPhLep.at(i)->Write();
	    h_bootstrap_DEtaLepLep.at(i)->Write();
	    h_bootstrap_DPhiLepLep.at(i)->Write();
	}
    }

    if (m_do_klfitter) {

	//if (m_show_of) AddOverflow(h_best_CA);
	//if (m_show_of) AddOverflow(h_best_lh_best);
	//if (m_show_of) AddOverflow(h_best_ph_lh_best);
	//if (m_show_of) AddOverflow(h_best_top_pole_mass);
	//if (m_show_of) AddOverflow(h_best_reco_hW_mass);
	//if (m_show_of) AddOverflow(h_best_reco_lW_mass);
	//if (m_show_of) AddOverflow(h_best_reco_htop_mass);
	//if (m_show_of) AddOverflow(h_best_reco_htop_pt);
	//if (m_show_of) AddOverflow(h_best_reco_htop_eta);
	//if (m_show_of) AddOverflow(h_best_reco_ltop_mass);
	//if (m_show_of) AddOverflow(h_best_reco_ltop_pt);
	//if (m_show_of) AddOverflow(h_best_reco_ltop_eta);

	//if (m_show_of) AddOverflow(h_CA);
	//if (m_show_of) AddOverflow(h_lh_best);
	//if (m_show_of) AddOverflow(h_top_pole_mass);
	//if (m_show_of) AddOverflow(h_reco_hW_mass);
	//if (m_show_of) AddOverflow(h_reco_lW_mass);
	//if (m_show_of) AddOverflow(h_reco_htop_mass);
	//if (m_show_of) AddOverflow(h_reco_htop_pt);
	//if (m_show_of) AddOverflow(h_reco_htop_eta);
	//if (m_show_of) AddOverflow(h_reco_ltop_mass);
	//if (m_show_of) AddOverflow(h_reco_ltop_pt);
	//if (m_show_of) AddOverflow(h_reco_ltop_eta);

	//if (m_show_of) AddOverflow(h_lt_CA);
	//if (m_show_of) AddOverflow(h_lt_lh_best);
	//if (m_show_of) AddOverflow(h_lt_top_pole_mass);
	//if (m_show_of) AddOverflow(h_lt_reco_hW_mass);
	//if (m_show_of) AddOverflow(h_lt_reco_lW_mass);
	//if (m_show_of) AddOverflow(h_lt_reco_htop_mass);
	//if (m_show_of) AddOverflow(h_lt_reco_htop_pt);
	//if (m_show_of) AddOverflow(h_lt_reco_htop_eta);
	//if (m_show_of) AddOverflow(h_lt_reco_ltop_mass);
	//if (m_show_of) AddOverflow(h_lt_reco_ltop_pt);
	//if (m_show_of) AddOverflow(h_lt_reco_ltop_eta);

	//if (m_show_of) AddOverflow(h_ht_CA);
	//if (m_show_of) AddOverflow(h_ht_lh_best);
	//if (m_show_of) AddOverflow(h_ht_top_pole_mass);
	//if (m_show_of) AddOverflow(h_ht_reco_hW_mass);
	//if (m_show_of) AddOverflow(h_ht_reco_lW_mass);
	//if (m_show_of) AddOverflow(h_ht_reco_htop_mass);
	//if (m_show_of) AddOverflow(h_ht_reco_htop_pt);
	//if (m_show_of) AddOverflow(h_ht_reco_htop_eta);
	//if (m_show_of) AddOverflow(h_ht_reco_ltop_mass);
	//if (m_show_of) AddOverflow(h_ht_reco_ltop_pt);
	//if (m_show_of) AddOverflow(h_ht_reco_ltop_eta);

	//if (m_show_of) AddOverflow(h_lW_CA);
	//if (m_show_of) AddOverflow(h_lW_lh_best);
	//if (m_show_of) AddOverflow(h_lW_top_pole_mass);
	//if (m_show_of) AddOverflow(h_lW_reco_hW_mass);
	//if (m_show_of) AddOverflow(h_lW_reco_lW_mass);
	//if (m_show_of) AddOverflow(h_lW_reco_htop_mass);
	//if (m_show_of) AddOverflow(h_lW_reco_htop_pt);
	//if (m_show_of) AddOverflow(h_lW_reco_htop_eta);
	//if (m_show_of) AddOverflow(h_lW_reco_ltop_mass);
	//if (m_show_of) AddOverflow(h_lW_reco_ltop_pt);
	//if (m_show_of) AddOverflow(h_lW_reco_ltop_eta);

	//if (m_show_of) AddOverflow(h_hW_CA);
	//if (m_show_of) AddOverflow(h_hW_lh_best);
	//if (m_show_of) AddOverflow(h_hW_top_pole_mass);
	//if (m_show_of) AddOverflow(h_hW_reco_hW_mass);
	//if (m_show_of) AddOverflow(h_hW_reco_lW_mass);
	//if (m_show_of) AddOverflow(h_hW_reco_htop_mass);
	//if (m_show_of) AddOverflow(h_hW_reco_htop_pt);
	//if (m_show_of) AddOverflow(h_hW_reco_htop_eta);
	//if (m_show_of) AddOverflow(h_hW_reco_ltop_mass);
	//if (m_show_of) AddOverflow(h_hW_reco_ltop_pt);
	//if (m_show_of) AddOverflow(h_hW_reco_ltop_eta);

//	//if (m_show_of) AddOverflow(h2_CA);
//	//if (m_show_of) AddOverflow(h2_lh_best);
//	//if (m_show_of) AddOverflow(h2_top_pole_mass);
//	//if (m_show_of) AddOverflow(h2_reco_hW_mass);
//	//if (m_show_of) AddOverflow(h2_reco_lW_mass);
//	//if (m_show_of) AddOverflow(h2_reco_htop_mass);
//	//if (m_show_of) AddOverflow(h2_reco_htop_pt);
//	//if (m_show_of) AddOverflow(h2_reco_htop_eta);
//	//if (m_show_of) AddOverflow(h2_reco_ltop_mass);
//	//if (m_show_of) AddOverflow(h2_reco_ltop_pt);
//	//if (m_show_of) AddOverflow(h2_reco_ltop_eta);
//
//	//if (m_show_of) AddOverflow(h2_lh_ph_lh_best);

	//h_best_CA->Write();
	//h_best_lh_best->Write();
	//h_best_ph_lh_best->Write();
	//h_best_top_pole_mass->Write();
	//h_best_reco_hW_mass->Write();
	//h_best_reco_lW_mass->Write();
	//h_best_reco_htop_mass->Write();
	//h_best_reco_htop_pt->Write();
	//h_best_reco_htop_eta->Write();
	//h_best_reco_ltop_mass->Write();
	//h_best_reco_ltop_pt->Write();
	//h_best_reco_ltop_eta->Write();

	//h_CA->Write();
	//h_lh_best->Write();
	//h_top_pole_mass->Write();
	//h_reco_hW_mass->Write();
	//h_reco_lW_mass->Write();
	//h_reco_htop_mass->Write();
	//h_reco_htop_pt->Write();
	//h_reco_htop_eta->Write();
	//h_reco_ltop_mass->Write();
	//h_reco_ltop_pt->Write();
	//h_reco_ltop_eta->Write();

	//h_lt_CA->Write();
	//h_lt_lh_best->Write();
	//h_lt_top_pole_mass->Write();
	//h_lt_reco_hW_mass->Write();
	//h_lt_reco_lW_mass->Write();
	//h_lt_reco_htop_mass->Write();
	//h_lt_reco_htop_pt->Write();
	//h_lt_reco_htop_eta->Write();
	//h_lt_reco_ltop_mass->Write();
	//h_lt_reco_ltop_pt->Write();
	//h_lt_reco_ltop_eta->Write();

	//h_ht_CA->Write();
	//h_ht_lh_best->Write();
	//h_ht_top_pole_mass->Write();
	//h_ht_reco_hW_mass->Write();
	//h_ht_reco_lW_mass->Write();
	//h_ht_reco_htop_mass->Write();
	//h_ht_reco_htop_pt->Write();
	//h_ht_reco_htop_eta->Write();
	//h_ht_reco_ltop_mass->Write();
	//h_ht_reco_ltop_pt->Write();
	//h_ht_reco_ltop_eta->Write();

	//h_lW_CA->Write();
	//h_lW_lh_best->Write();
	//h_lW_top_pole_mass->Write();
	//h_lW_reco_hW_mass->Write();
	//h_lW_reco_lW_mass->Write();
	//h_lW_reco_htop_mass->Write();
	//h_lW_reco_htop_pt->Write();
	//h_lW_reco_htop_eta->Write();
	//h_lW_reco_ltop_mass->Write();
	//h_lW_reco_ltop_pt->Write();
	//h_lW_reco_ltop_eta->Write();

	//h_hW_CA->Write();
	//h_hW_lh_best->Write();
	//h_hW_top_pole_mass->Write();
	//h_hW_reco_hW_mass->Write();
	//h_hW_reco_lW_mass->Write();
	//h_hW_reco_htop_mass->Write();
	//h_hW_reco_htop_pt->Write();
	//h_hW_reco_htop_eta->Write();
	//h_hW_reco_ltop_mass->Write();
	//h_hW_reco_ltop_pt->Write();
	//h_hW_reco_ltop_eta->Write();

	//h2_CA->Write();
	//h2_lh_best->Write();
	//h2_top_pole_mass->Write();
	//h2_reco_hW_mass->Write();
	//h2_reco_lW_mass->Write();
	//h2_reco_htop_mass->Write();
	//h2_reco_htop_pt->Write();
	//h2_reco_htop_eta->Write();
	//h2_reco_ltop_mass->Write();
	//h2_reco_ltop_pt->Write();
	//h2_reco_ltop_eta->Write();

	//h2_lh_ph_lh_best->Write();
    }

    // histograms
    for (int i = 0; i < m_hists.size(); i++) {
	if (m_show_of) AddOverflow(m_hists.at(i));
	m_hists.at(i)->Write();
    }
    for (int i = 0; i < m_2Dhists.size(); i++) {
	m_2Dhists.at(i)->Write();
    }
    if (m_do_egammahists) {
	for (int i = 0; i < m_egammahists_pt.size(); i++) {
    	    TH1F* tmp_h = m_egammahists_pt.at(i);
    	    tmp_h->Write();
    	}
    	for (int i = 0; i < m_egammahists_eta.size(); i++) {
    	    TH1F* tmp_h = m_egammahists_eta.at(i);
    	    tmp_h->Write();
    	}
    	for (int i = 0; i < m_egammahists_pteta.size(); i++) {
    	    vector<TH1F*> tmp_hists = m_egammahists_pteta.at(i);
    	    for (int j = 0; j < tmp_hists.size(); j++) {
    	        TH1F* tmp_h = tmp_hists.at(j);
    	        tmp_h->Write();
    	    }
    	}
	if (m_do_egammatf1) {
	    TKDE * kde = new TKDE(m_tkde_data.size(), &m_tkde_data[0],  50, 140, "", 1);
	    TF1 * m_egammatf1 = (TF1*) kde->GetFunction(500);
	    char tmp[100];
	    sprintf(tmp, "InvariantMassTF1");
	    m_egammatf1->Write(AppendInfo(tmp).c_str());
	    for (int i = 0; i < m_tkde_data_pteta.size(); i++) {
    	        vector<vector<double> > tmp_tkde_data = m_tkde_data_pteta.at(i);
    	        vector<vector<double> > tmp_tkde_data_w = m_tkde_data_pteta_w.at(i);
    	        for (int j = 0; j < tmp_tkde_data.size(); j++) {
    	            vector<double> tmp_tmp_tkde_data = tmp_tkde_data.at(j);
    	            vector<double> tmp_tmp_tkde_data_w = tmp_tkde_data_w.at(j);
		    if (tmp_tmp_tkde_data.size() == 0) continue;
		    //cout << "print pt eta bin content" << endl;
		    //for (int k = 0; k < tmp_tmp_tkde_data.size(); k++) {
		    //    cout << "pt " << i << " eta " << j << " val " << tmp_tmp_tkde_data.at(k) << endl;
		    //}
		    //cout << "Print finished" << endl;
	            TKDE * kde = new TKDE(tmp_tmp_tkde_data.size(), &tmp_tmp_tkde_data[0], 50, 140, "", 1);
	    	    TF1* tmp_egammatf1 = (TF1*) kde->GetFunction(500);
	    	    sprintf(tmp, "InvariantMassTF1_PtBin%d_EtaBin%d", i+1, j+1);
	    	    tmp_egammatf1->Write(AppendInfo(tmp).c_str());
    	        }
    	    }
	}
    }

    f.Close();
}

void SampleAnalyzor::FillCutflow(int cnt, string cutname) {
    n_cuts_end = n_cuts_end < cnt ? cnt : n_cuts_end;
    h_Cutflow->Fill(cnt, weight);
    for (int i = 0; i < h_Cutflow_QCD.size(); i++) {
	h_Cutflow_QCD.at(i)->Fill(cnt, weights_QCD.at(i));
    }
    string tmpcutname = "Cut:"; tmpcutname += cutname;
    h_Cutflow->GetXaxis()->SetBinLabel(cnt, tmpcutname.c_str());
    for (int i = 0; i < h_Cutflow_QCD.size(); i++) {
	h_Cutflow_QCD.at(i)->GetXaxis()->SetBinLabel(cnt, tmpcutname.c_str());
    }
}

void SampleAnalyzor::PrintCutflow(TH1F* h) {
    m_lg->InfoFixW("dsdsdsdsds", 4, "Nr", 30, "Cut", 15, "Value",		       10, "+/-", 15, "Err"       ); 
    for (int i = 1; i <= n_cuts_end; i++) {
        m_lg->InfoFixW("dddsdfdsdf", 4, i, 30, h->GetXaxis()->GetBinLabel(i), 15, h->GetBinContent(i), 10, "+/-", 15, h->GetBinError(i));
    }
}

void SampleAnalyzor::PrintCutflow(TH1F* h1, TH1F* h2) {
    m_lg->InfoFixW("dsdsdsdsdsdsds", 30, "CUTS                       -->", 15, "Value",		   10, "+/-", 15, "Err"       ,          15 ,"Value", 10, "+/-", 15, "Err"); 
    int cut_cnt = 1;
    m_lg->InfoFixW("dsdfdsdfdfdsdf", 30, "CUT all events             -->", 15, h1->GetBinContent(cut_cnt), 10, "+/-", 15, h1->GetBinError(cut_cnt), 15, h2->GetBinContent(cut_cnt), 10, "+/-", 15, h2->GetBinContent(cut_cnt)); cut_cnt++;
    m_lg->InfoFixW("dsdfdsdfdfdsdf", 30, "CUT single lepton          -->", 15, h1->GetBinContent(cut_cnt), 10, "+/-", 15, h1->GetBinError(cut_cnt), 15, h2->GetBinContent(cut_cnt), 10, "+/-", 15, h2->GetBinContent(cut_cnt)); cut_cnt++;
    m_lg->InfoFixW("dsdfdsdfdfdsdf", 30, "CUT single lepton ptbin 1  -->", 15, h1->GetBinContent(cut_cnt), 10, "+/-", 15, h1->GetBinError(cut_cnt), 15, h2->GetBinContent(cut_cnt), 10, "+/-", 15, h2->GetBinContent(cut_cnt)); cut_cnt++;
    m_lg->InfoFixW("dsdfdsdfdfdsdf", 30, "CUT single lepton ptbin 2  -->", 15, h1->GetBinContent(cut_cnt), 10, "+/-", 15, h1->GetBinError(cut_cnt), 15, h2->GetBinContent(cut_cnt), 10, "+/-", 15, h2->GetBinContent(cut_cnt)); cut_cnt++;
    m_lg->InfoFixW("dsdfdsdfdfdsdf", 30, "CUT single lepton ptbin 3  -->", 15, h1->GetBinContent(cut_cnt), 10, "+/-", 15, h1->GetBinError(cut_cnt), 15, h2->GetBinContent(cut_cnt), 10, "+/-", 15, h2->GetBinContent(cut_cnt)); cut_cnt++;
    m_lg->InfoFixW("dsdfdsdfdfdsdf", 30, "CUT single lepton ptbin 4  -->", 15, h1->GetBinContent(cut_cnt), 10, "+/-", 15, h1->GetBinError(cut_cnt), 15, h2->GetBinContent(cut_cnt), 10, "+/-", 15, h2->GetBinContent(cut_cnt)); cut_cnt++;
    m_lg->InfoFixW("dsdfdsdfdfdsdf", 30, "CUT single lepton ptbin 5  -->", 15, h1->GetBinContent(cut_cnt), 10, "+/-", 15, h1->GetBinError(cut_cnt), 15, h2->GetBinContent(cut_cnt), 10, "+/-", 15, h2->GetBinContent(cut_cnt)); cut_cnt++;
    m_lg->InfoFixW("dsdfdsdfdfdsdf", 30, "CUT single lepton etabin 1 -->", 15, h1->GetBinContent(cut_cnt), 10, "+/-", 15, h1->GetBinError(cut_cnt), 15, h2->GetBinContent(cut_cnt), 10, "+/-", 15, h2->GetBinContent(cut_cnt)); cut_cnt++;
    m_lg->InfoFixW("dsdfdsdfdfdsdf", 30, "CUT single lepton etabin 2 -->", 15, h1->GetBinContent(cut_cnt), 10, "+/-", 15, h1->GetBinError(cut_cnt), 15, h2->GetBinContent(cut_cnt), 10, "+/-", 15, h2->GetBinContent(cut_cnt)); cut_cnt++;
    m_lg->InfoFixW("dsdfdsdfdfdsdf", 30, "CUT single lepton etabin 3 -->", 15, h1->GetBinContent(cut_cnt), 10, "+/-", 15, h1->GetBinError(cut_cnt), 15, h2->GetBinContent(cut_cnt), 10, "+/-", 15, h2->GetBinContent(cut_cnt)); cut_cnt++;
    m_lg->InfoFixW("dsdfdsdfdfdsdf", 30, "CUT single lepton etabin 4 -->", 15, h1->GetBinContent(cut_cnt), 10, "+/-", 15, h1->GetBinError(cut_cnt), 15, h2->GetBinContent(cut_cnt), 10, "+/-", 15, h2->GetBinContent(cut_cnt)); cut_cnt++;
    m_lg->InfoFixW("dsdfdsdfdfdsdf", 30, "CUT single lepton etabin 5 -->", 15, h1->GetBinContent(cut_cnt), 10, "+/-", 15, h1->GetBinError(cut_cnt), 15, h2->GetBinContent(cut_cnt), 10, "+/-", 15, h2->GetBinContent(cut_cnt)); cut_cnt++;
    m_lg->InfoFixW("dsdfdsdfdfdsdf", 30, "CUT ==1 lepton             -->", 15, h1->GetBinContent(cut_cnt), 10, "+/-", 15, h1->GetBinError(cut_cnt), 15, h2->GetBinContent(cut_cnt), 10, "+/-", 15, h2->GetBinContent(cut_cnt)); cut_cnt++;
    m_lg->InfoFixW("dsdfdsdfdfdsdf", 30, "CUT >=4 jets               -->", 15, h1->GetBinContent(cut_cnt), 10, "+/-", 15, h1->GetBinError(cut_cnt), 15, h2->GetBinContent(cut_cnt), 10, "+/-", 15, h2->GetBinContent(cut_cnt)); cut_cnt++;
    m_lg->InfoFixW("dsdfdsdfdfdsdf", 30, "CUT >=1 bjet               -->", 15, h1->GetBinContent(cut_cnt), 10, "+/-", 15, h1->GetBinError(cut_cnt), 15, h2->GetBinContent(cut_cnt), 10, "+/-", 15, h2->GetBinContent(cut_cnt)); cut_cnt++;
    m_lg->InfoFixW("dsdfdsdfdfdsdf", 30, "CUT ==1 photon             -->", 15, h1->GetBinContent(cut_cnt), 10, "+/-", 15, h1->GetBinError(cut_cnt), 15, h2->GetBinContent(cut_cnt), 10, "+/-", 15, h2->GetBinContent(cut_cnt)); cut_cnt++;
    m_lg->InfoFixW("dsdfdsdfdfdsdf", 30, "CUT off zwindow            -->", 15, h1->GetBinContent(cut_cnt), 10, "+/-", 15, h1->GetBinError(cut_cnt), 15, h2->GetBinContent(cut_cnt), 10, "+/-", 15, h2->GetBinContent(cut_cnt)); cut_cnt++;
    m_lg->InfoFixW("dsdfdsdfdfdsdf", 30, "CUT dr(ph,jet)             -->", 15, h1->GetBinContent(cut_cnt), 10, "+/-", 15, h1->GetBinError(cut_cnt), 15, h2->GetBinContent(cut_cnt), 10, "+/-", 15, h2->GetBinContent(cut_cnt)); cut_cnt++;
    m_lg->InfoFixW("dsdfdsdfdfdsdf", 30, "CUT dr(ph,lep)             -->", 15, h1->GetBinContent(cut_cnt), 10, "+/-", 15, h1->GetBinError(cut_cnt), 15, h2->GetBinContent(cut_cnt), 10, "+/-", 15, h2->GetBinContent(cut_cnt)); cut_cnt++;
    m_lg->InfoFixW("dsdfdsdfdfdsdf", 30, "Photon match               -->", 15, h1->GetBinContent(cut_cnt), 10, "+/-", 15, h1->GetBinError(cut_cnt), 15, h2->GetBinContent(cut_cnt), 10, "+/-", 15, h2->GetBinContent(cut_cnt)); cut_cnt++;
    m_lg->InfoFixW("dsdfdsdfdfdsdf", 30, "Photon match ptbin 1       -->", 15, h1->GetBinContent(cut_cnt), 10, "+/-", 15, h1->GetBinError(cut_cnt), 15, h2->GetBinContent(cut_cnt), 10, "+/-", 15, h2->GetBinContent(cut_cnt)); cut_cnt++;
    m_lg->InfoFixW("dsdfdsdfdfdsdf", 30, "Photon match ptbin 2       -->", 15, h1->GetBinContent(cut_cnt), 10, "+/-", 15, h1->GetBinError(cut_cnt), 15, h2->GetBinContent(cut_cnt), 10, "+/-", 15, h2->GetBinContent(cut_cnt)); cut_cnt++;
    m_lg->InfoFixW("dsdfdsdfdfdsdf", 30, "Photon match ptbin 3       -->", 15, h1->GetBinContent(cut_cnt), 10, "+/-", 15, h1->GetBinError(cut_cnt), 15, h2->GetBinContent(cut_cnt), 10, "+/-", 15, h2->GetBinContent(cut_cnt)); cut_cnt++;
    m_lg->InfoFixW("dsdfdsdfdfdsdf", 30, "Photon match ptbin 4       -->", 15, h1->GetBinContent(cut_cnt), 10, "+/-", 15, h1->GetBinError(cut_cnt), 15, h2->GetBinContent(cut_cnt), 10, "+/-", 15, h2->GetBinContent(cut_cnt)); cut_cnt++;
    m_lg->InfoFixW("dsdfdsdfdfdsdf", 30, "Photon match ptbin 5       -->", 15, h1->GetBinContent(cut_cnt), 10, "+/-", 15, h1->GetBinError(cut_cnt), 15, h2->GetBinContent(cut_cnt), 10, "+/-", 15, h2->GetBinContent(cut_cnt)); cut_cnt++;
    m_lg->InfoFixW("dsdfdsdfdfdsdf", 30, "Photon match etabin 1      -->", 15, h1->GetBinContent(cut_cnt), 10, "+/-", 15, h1->GetBinError(cut_cnt), 15, h2->GetBinContent(cut_cnt), 10, "+/-", 15, h2->GetBinContent(cut_cnt)); cut_cnt++;
    m_lg->InfoFixW("dsdfdsdfdfdsdf", 30, "Photon match etabin 2      -->", 15, h1->GetBinContent(cut_cnt), 10, "+/-", 15, h1->GetBinError(cut_cnt), 15, h2->GetBinContent(cut_cnt), 10, "+/-", 15, h2->GetBinContent(cut_cnt)); cut_cnt++;
    m_lg->InfoFixW("dsdfdsdfdfdsdf", 30, "Photon match etabin 3      -->", 15, h1->GetBinContent(cut_cnt), 10, "+/-", 15, h1->GetBinError(cut_cnt), 15, h2->GetBinContent(cut_cnt), 10, "+/-", 15, h2->GetBinContent(cut_cnt)); cut_cnt++;
    m_lg->InfoFixW("dsdfdsdfdfdsdf", 30, "Photon match etabin 4      -->", 15, h1->GetBinContent(cut_cnt), 10, "+/-", 15, h1->GetBinError(cut_cnt), 15, h2->GetBinContent(cut_cnt), 10, "+/-", 15, h2->GetBinContent(cut_cnt)); cut_cnt++;
    m_lg->InfoFixW("dsdfdsdfdfdsdf", 30, "Photon match etabin 5      -->", 15, h1->GetBinContent(cut_cnt), 10, "+/-", 15, h1->GetBinError(cut_cnt), 15, h2->GetBinContent(cut_cnt), 10, "+/-", 15, h2->GetBinContent(cut_cnt)); cut_cnt++;
}

void SampleAnalyzor::FillHistogram() {
    if (m_do_klfitter) {
	//h_best_CA->Fill(kl_best_CA, weight);
	//h_best_lh_best->Fill(kl_best_lh_best, weight);
	//h_best_ph_lh_best->Fill(kl_best_ph_lh_best, weight);
	//h_best_top_pole_mass->Fill(kl_best_top_pole_mass, weight);
	//h_best_reco_hW_mass->Fill(kl_best_reco_hW_mass, weight);
	//h_best_reco_lW_mass->Fill(kl_best_reco_lW_mass, weight);
	//h_best_reco_ltop_mass->Fill(kl_best_reco_ltop_mass, weight);
	//h_best_reco_htop_mass->Fill(kl_best_reco_htop_mass, weight);
	//h_best_reco_htop_pt->Fill(kl_best_reco_htop_pt, weight);
	//h_best_reco_ltop_pt->Fill(kl_best_reco_ltop_pt, weight);
	//h_best_reco_htop_eta->Fill(kl_best_reco_htop_eta, weight);
	//h_best_reco_ltop_eta->Fill(kl_best_reco_ltop_eta, weight);

	//h_CA->Fill(kl_CA, weight);
	//h_lh_best->Fill(kl_lh_best, weight);
	//h_top_pole_mass->Fill(kl_top_pole_mass, weight);
	//h_reco_hW_mass->Fill(kl_reco_hW_mass, weight);
	//h_reco_lW_mass->Fill(kl_reco_lW_mass, weight);
	//h_reco_htop_mass->Fill(kl_reco_htop_mass, weight);
	//h_reco_ltop_mass->Fill(kl_reco_ltop_mass, weight);
	//h_reco_htop_pt->Fill(kl_reco_htop_pt, weight);
	//h_reco_ltop_pt->Fill(kl_reco_ltop_pt, weight);
	//h_reco_htop_eta->Fill(kl_reco_htop_eta, weight);
	//h_reco_ltop_eta->Fill(kl_reco_ltop_eta, weight);

	//h_lt_CA->Fill(kl_lt_CA, weight);
	//h_lt_lh_best->Fill(kl_lt_lh_best, weight);
	//h_lt_top_pole_mass->Fill(kl_lt_top_pole_mass, weight);
	//h_lt_reco_hW_mass->Fill(kl_lt_reco_hW_mass, weight);
	//h_lt_reco_lW_mass->Fill(kl_lt_reco_lW_mass, weight);
	//h_lt_reco_ltop_mass->Fill(kl_lt_reco_ltop_mass, weight);
	//h_lt_reco_htop_mass->Fill(kl_lt_reco_htop_mass, weight);
	//h_lt_reco_htop_pt->Fill(kl_lt_reco_htop_pt, weight);
	//h_lt_reco_ltop_pt->Fill(kl_lt_reco_ltop_pt, weight);
	//h_lt_reco_htop_eta->Fill(kl_lt_reco_htop_eta, weight);
	//h_lt_reco_ltop_eta->Fill(kl_lt_reco_ltop_eta, weight);

	//h_ht_CA->Fill(kl_ht_CA, weight);
	//h_ht_lh_best->Fill(kl_ht_lh_best, weight);
	//h_ht_top_pole_mass->Fill(kl_ht_top_pole_mass, weight);
	//h_ht_reco_hW_mass->Fill(kl_ht_reco_hW_mass, weight);
	//h_ht_reco_lW_mass->Fill(kl_ht_reco_lW_mass, weight);
	//h_ht_reco_ltop_mass->Fill(kl_ht_reco_ltop_mass, weight);
	//h_ht_reco_htop_mass->Fill(kl_ht_reco_htop_mass, weight);
	//h_ht_reco_htop_pt->Fill(kl_ht_reco_htop_pt, weight);
	//h_ht_reco_ltop_pt->Fill(kl_ht_reco_ltop_pt, weight);
	//h_ht_reco_htop_eta->Fill(kl_ht_reco_htop_eta, weight);
	//h_ht_reco_ltop_eta->Fill(kl_ht_reco_ltop_eta, weight);

	//h_lW_CA->Fill(kl_lW_CA, weight);
	//h_lW_lh_best->Fill(kl_lW_lh_best, weight);
	//h_lW_top_pole_mass->Fill(kl_lW_top_pole_mass, weight);
	//h_lW_reco_hW_mass->Fill(kl_lW_reco_hW_mass, weight);
	//h_lW_reco_lW_mass->Fill(kl_lW_reco_lW_mass, weight);
	//h_lW_reco_ltop_mass->Fill(kl_lW_reco_ltop_mass, weight);
	//h_lW_reco_htop_mass->Fill(kl_lW_reco_htop_mass, weight);
	//h_lW_reco_htop_pt->Fill(kl_lW_reco_htop_pt, weight);
	//h_lW_reco_ltop_pt->Fill(kl_lW_reco_ltop_pt, weight);
	//h_lW_reco_htop_eta->Fill(kl_lW_reco_htop_eta, weight);
	//h_lW_reco_ltop_eta->Fill(kl_lW_reco_ltop_eta, weight);

	//h_hW_CA->Fill(kl_hW_CA, weight);
	//h_hW_lh_best->Fill(kl_hW_lh_best, weight);
	//h_hW_top_pole_mass->Fill(kl_hW_top_pole_mass, weight);
	//h_hW_reco_hW_mass->Fill(kl_hW_reco_hW_mass, weight);
	//h_hW_reco_lW_mass->Fill(kl_hW_reco_lW_mass, weight);
	//h_hW_reco_ltop_mass->Fill(kl_hW_reco_ltop_mass, weight);
	//h_hW_reco_htop_mass->Fill(kl_hW_reco_htop_mass, weight);
	//h_hW_reco_htop_pt->Fill(kl_hW_reco_htop_pt, weight);
	//h_hW_reco_ltop_pt->Fill(kl_hW_reco_ltop_pt, weight);
	//h_hW_reco_htop_eta->Fill(kl_hW_reco_htop_eta, weight);
	//h_hW_reco_ltop_eta->Fill(kl_hW_reco_ltop_eta, weight);

	//h2_CA->Fill(kl_lt_CA, kl_ht_CA, weight);
	//h2_lh_best->Fill(kl_lt_lh_best, kl_ht_lh_best, weight);
	//h2_top_pole_mass->Fill(kl_lt_top_pole_mass, kl_ht_top_pole_mass, weight);
	//h2_reco_hW_mass->Fill(kl_lt_reco_hW_mass, kl_ht_reco_hW_mass, weight);
	//h2_reco_lW_mass->Fill(kl_lt_reco_lW_mass, kl_ht_reco_lW_mass, weight);
	//h2_reco_ltop_mass->Fill(kl_lt_reco_ltop_mass, kl_ht_reco_ltop_mass, weight);
	//h2_reco_htop_mass->Fill(kl_lt_reco_htop_mass, kl_ht_reco_htop_mass, weight);
	//h2_reco_htop_pt->Fill(kl_lt_reco_htop_pt, kl_ht_reco_htop_pt, weight);
	//h2_reco_ltop_pt->Fill(kl_lt_reco_ltop_pt, kl_ht_reco_ltop_pt, weight);
	//h2_reco_htop_eta->Fill(kl_lt_reco_htop_eta, kl_ht_reco_htop_eta, weight);
	//h2_reco_ltop_eta->Fill(kl_lt_reco_ltop_eta, kl_ht_reco_ltop_eta, weight);

	//h2_lh_ph_lh_best->Fill(kl_lh_best, kl_best_ph_lh_best, weight);
    }
    for (int i = 0; i < m_hists.size(); i++) {
	TH1F* tmp_h = m_hists.at(i);
	string tmp_name = tmp_h->GetTitle();
	double tmp_val;
	if (tmp_name.find("LeadPhPtRecoPtTrue",0) != string::npos) tmp_val = leadph_ptreco_pttrue;
	else if (tmp_name.find("LeadPhPt",0) != string::npos && tmp_name.find("LeadPhPtCone",0) == string::npos) tmp_val = leadph_pt/1000.;
	else if (tmp_name.find("LeadPhMPtCone",0) != string::npos) tmp_val = (leadph_ptcone- (leadph_mc_el_pt - leadph_mc_pt))/1000.;
	else if (tmp_name.find("LeadPhM2PtCone",0) != string::npos) tmp_val = (leadph_ptcone- (leadph_mc_el_pt - leadph_pt))/1000.;
	else if (tmp_name.find("LeadPhMCPt",0) != string::npos) tmp_val = leadph_mc_pt/1000.;
	else if (tmp_name.find("LeadPhDrRecoTrue",0) != string::npos) tmp_val = leadph_drrecotrue;
	else if (tmp_name.find("LeadPhMCBarcode",0) != string::npos) tmp_val = leadph_mc_barcode;
	else if (tmp_name.find("LeadLepMCPt",0) != string::npos) tmp_val = leadlep_mc_pt/1000.;
	else if (tmp_name.find("SubLepMCPt",0) != string::npos) tmp_val = sublep_mc_pt/1000.;
	else if (tmp_name.find("LeadPhEta",0) != string::npos) tmp_val = leadph_eta;
	else if (tmp_name.find("LeadPhAbsEta",0) != string::npos) tmp_val = fabs(leadph_eta);
	else if (tmp_name.find("LeadLepPt",0) != string::npos) tmp_val = leadlep_pt/1000.;
	else if (tmp_name.find("NGoodPh",0) != string::npos) tmp_val = event_ngoodphotons;
	else if (tmp_name.find("SubLepPt",0) != string::npos) tmp_val = sublep_pt/1000.;
	else if (tmp_name.find("LeadLepEta",0) != string::npos) tmp_val = leadlep_eta;
	else if (tmp_name.find("SubLepEta",0) != string::npos) tmp_val = leadlep_eta;
	else if (tmp_name.find("LeadLepAbsEta",0) != string::npos) tmp_val = fabs(leadlep_eta);
	else if (tmp_name.find("SubLepAbsEta",0) != string::npos) tmp_val = fabs(leadlep_eta);
	else if (tmp_name.find("MU",0) != string::npos) tmp_val = mu;
	else if (tmp_name.find("MMCPhLep",0) != string::npos) tmp_val = mmcphl/1000.;
	else if (tmp_name.find("MMCElPhLep",0) != string::npos) tmp_val = mmcelphl/1000.;
	else if (tmp_name.find("MMCLepLep",0) != string::npos) tmp_val = mmcll/1000.;
	else if (tmp_name.find("MLepMCLep",0) != string::npos) tmp_val = mlmcl/1000.;
	else if (tmp_name.find("MMCLepMCLep",0) != string::npos) tmp_val = mmclmcl/1000.;
	else if (tmp_name.find("LeadPhMCMMCPt",0) != string::npos) tmp_val = (leadph_mc_el_pt-leadph_mc_pt)/leadph_mc_pt; 	
	else if (tmp_name.find("LeadPhMCMMCEta",0) != string::npos) tmp_val = (leadph_mc_el_eta-leadph_mc_eta)/leadph_mc_eta; 	
	else if (tmp_name.find("LeadPhMMCElPt",0) != string::npos) tmp_val = (leadph_pt-leadph_mc_el_pt)/leadph_mc_el_pt;
	else if (tmp_name.find("LeadPhMMCElEta",0) != string::npos) tmp_val = (leadph_eta-leadph_mc_el_eta)/leadph_mc_el_eta;
	else if (tmp_name.find("LeadPhMCElPt",0) != string::npos) tmp_val = leadph_mc_el_pt/1000.;
	else if (tmp_name.find("LeadPhMCElDr",0) != string::npos) tmp_val = leadph_mc_el_dr;
	else if (tmp_name.find("LeadJetEta",0) != string::npos) tmp_val = leadjet_eta;
	else if (tmp_name.find("LeadJetAsm",0) != string::npos) tmp_val = leadjet_asm;
	else if (tmp_name.find("SubJetEta",0) != string::npos) tmp_val = subjet_eta;
	else if (tmp_name.find("LeadJetPt",0) != string::npos) tmp_val = leadjet_pt/1000.;
	else if (tmp_name.find("SubJetPt",0) != string::npos) tmp_val = subjet_pt/1000.;
	else if (tmp_name.find("dPhiLepMET",0) != string::npos) tmp_val = dphilepmet;
	else if (tmp_name.find("dPhiPhMET",0) != string::npos) tmp_val = dphiphmet;
	else if (tmp_name.find("MTLPhMET",0) != string::npos) tmp_val = mt_lphmet/1000.;
	else if (tmp_name.find("MET",0) != string::npos) tmp_val = met/1000.;
	else if (tmp_name.find("MPhLep",0) != string::npos && tmp_name.find("MPhLepLep",0) == string::npos) tmp_val = mphl/1000.;
	else if (tmp_name.find("MPhLepLep",0) != string::npos) tmp_val = mllg/1000.;
	else if (tmp_name.find("MLepLep",0) != string::npos) tmp_val = mll/1000.;
	else if (tmp_name.find("DrLepLep",0) != string::npos) tmp_val = drleplep;
	else if (tmp_name.find("LeadPhMMCPt",0) != string::npos) tmp_val = (leadph_pt-leadph_mc_pt)/leadph_mc_pt; 	
	else if (tmp_name.find("LeadPhMMCEta",0) != string::npos) tmp_val = (leadph_eta-leadph_mc_eta)/leadph_mc_eta; 	
	else if (tmp_name.find("LeadPhPtCone",0) != string::npos) tmp_val = leadph_ptcone/1000.;
	else if (tmp_name.find("Year2015",0) != string::npos) tmp_val = m_is2015;
	else if (tmp_name.find("Period",0) != string::npos) tmp_val = m_period;
	else if (tmp_name.find("PileUpMu",0) != string::npos) tmp_val = mu;
	else if (tmp_name.find("LeadPhEtCone",0) != string::npos) tmp_val = leadph_etcone/1000.;
	else if (tmp_name.find("RHad1",0) != string::npos) tmp_val = leadph_rhad1;
	else if (tmp_name.find("RHad",0) != string::npos) tmp_val = leadph_rhad;
	else if (tmp_name.find("REta",0) != string::npos) tmp_val = leadph_reta;
	else if (tmp_name.find("RPhi",0) != string::npos) tmp_val = leadph_rphi;
	else if (tmp_name.find("WEta2",0) != string::npos) tmp_val = leadph_weta2;
	else if (tmp_name.find("WEta",0) != string::npos) tmp_val = leadph_ws3;
	else if (tmp_name.find("fSide",0) != string::npos) tmp_val = leadph_fracm;
	else if (tmp_name.find("ELD",0) != string::npos) tmp_val = event_ELD_MVA_correct;
	else if (tmp_name == "LeadPhCvt") tmp_val = leadph_cvt;
	else if (tmp_name == "LeadPhBinCvt") tmp_val = leadph_bincvt;
	else if (tmp_name == "LeadPhMCEta") tmp_val = leadph_mc_eta;
	else if (tmp_name == "LeadPhMCPhi") tmp_val = leadph_mc_phi;
	else if (tmp_name == "LeadPhMCE") tmp_val = leadph_mc_e/1000.;
	else if (tmp_name == "LeadLepMMCPt") tmp_val = (leadlep_pt-leadlep_mc_pt)/leadlep_mc_pt;
	else if (tmp_name == "SubLepMMCPt") tmp_val = (sublep_pt-sublep_mc_pt)/sublep_mc_pt;
	else if (tmp_name == "LeadPhMCDr") tmp_val = leadph_mc_dr;
	else if (tmp_name == "LeadLepMCDr") tmp_val = leadlep_mc_dr;
	else if (tmp_name == "SubLepMCDr") tmp_val = sublep_mc_dr;
	else if (tmp_name == "LeadPhMMCE") tmp_val = (leadph_e-leadph_mc_e)/leadph_mc_e;
	else if (tmp_name == "LeadLepMMCE") tmp_val = (leadlep_e-leadlep_mc_e)/leadlep_mc_e;
	else if (tmp_name == "SubLepMMCE") tmp_val = (sublep_e-sublep_mc_e)/sublep_mc_e;
	else if (tmp_name == "LeadPhEta") tmp_val = leadph_eta;
	else if (tmp_name == "LeadPhEtaCoarse") tmp_val = leadph_eta;
	else if (tmp_name == "LeadPhAbsEtaForFR8TeV") tmp_val = fabs(leadph_eta);
	else if (tmp_name.find("LeadPhMinDrPhLep",0) != string::npos) tmp_val = leadph_mindrphl;
	else if (tmp_name.find("DrLeadJetLeadPh",0) != string::npos) tmp_val = drleadjet_leadph;
	else if (tmp_name.find("DrLeadJetLeadLep",0) != string::npos) tmp_val = drleadjet_leadlep;
	else if (tmp_name.find("LeadPhMinDrPhJet",0) != string::npos) tmp_val = leadph_mindrphj;
	else if (tmp_name.find("M2J",0) != string::npos) tmp_val = m_12jet/1000.;
	else if (tmp_name.find("DEtaLepLep",0) != string::npos) tmp_val = detaleplep;
	else if (tmp_name.find("DPhiLepLep",0) != string::npos) tmp_val = dphileplep;
	else if (tmp_name == "SubLepMinDrPhLep") tmp_val = sublep_mindrphl;
	else if (tmp_name == "SubLepMinDrPhJet") tmp_val = sublep_mindrphj;
	else if (tmp_name == "SubLepMinDrPhLepCoarse") tmp_val = sublep_mindrphl;
	else if (tmp_name == "SubLepMinDrPhJetCoarse") tmp_val = sublep_mindrphj;
	else if (tmp_name.find("LeadLepMinDrPhLep",0) != string::npos) tmp_val = leadlep_mindrphl;
	else if (tmp_name.find("LeadLepMinDrPhJet",0) != string::npos) tmp_val = leadlep_mindrphj;
	else if (tmp_name == "LeadPhMCPID") tmp_val = abs(leadph_mc_pid);
	else if (tmp_name == "LeadPhType") tmp_val = leadph_type;
	else if (tmp_name == "LeadPhOrigin") tmp_val = leadph_origin;
	else if (tmp_name == "LeadLepType") tmp_val = leadlep_type;
	else if (tmp_name == "LeadLepOrigin") tmp_val = leadlep_origin;
	else if (tmp_name == "SubLepType") tmp_val = sublep_type;
	else if (tmp_name == "SubLepOrigin") tmp_val = sublep_origin;
	else if (tmp_name == "LeadPhAncestor") tmp_val = leadph_ancestor;
	else if (tmp_name == "Njet") tmp_val = n_jet;
	else if (tmp_name == "Nbjet") tmp_val = n_bjet;
	else if (tmp_name == "Nbjet77") tmp_val = n_bjet77;
	else if (tmp_name == "SubLepPtCone") tmp_val = sublep_ptcone/1000.;
	else if (tmp_name == "SubLepEtCone") tmp_val = sublep_etcone/1000.;
	else if (tmp_name == "SubLepEta") tmp_val = sublep_eta;
	else if (tmp_name == "SubLepEtaCoarse") tmp_val = sublep_eta;
	else if (tmp_name == "SubLepAbsEtaForFR8TeV") tmp_val = sublep_eta;
	else if (tmp_name.find("LeadPhLeadLepDphi",0) != string::npos) tmp_val = leadphleadlep_dphi;
	else if (tmp_name == "LeadLepSubLepDphi") tmp_val = leadlepsublep_dphi;
	else if (tmp_name == "LeadLepSubLepDphiCoarse") tmp_val = leadlepsublep_dphi;
	else if (tmp_name == "SubLepLeadLepDphi") tmp_val = leadlepsublep_dphi;
	else if (tmp_name == "SubLepLeadLepDphiCoarse") tmp_val = leadlepsublep_dphi;
	else if (tmp_name.find("MWT",0) != string::npos) tmp_val = mwt/1000.;
	else if (tmp_name.find("MWT2",0) != string::npos) tmp_val = mwt2/1000.;
	else if (tmp_name.find("PPT",0) != string::npos) tmp_val = leadph_HFT_MVA;
	else {
	    m_lg->Err("ss", "Can't resolve histogram name ->", tmp_name.c_str());
	    exit(-1);
	}
	tmp_h->Fill(tmp_val, weight);
    }
    for (int i = 0; i < m_2Dhists.size(); i++) {
	TH2F* tmp_h = m_2Dhists.at(i);
	string tmp_name1 = m_2Dhists_vars.at(i).first;
	string tmp_name2 = m_2Dhists_vars.at(i).second;
	double tmp_val1;
	double tmp_val2;
	if (tmp_name1.find("LeadPhPtRecoPtTrue",0) != string::npos) tmp_val1 = leadph_ptreco_pttrue;
	else if (tmp_name1.find("LeadPhPt",0) != string::npos && tmp_name1.find("LeadPhPtCone",0) == string::npos) tmp_val1 = leadph_pt/1000.;
	else if (tmp_name1.find("LeadPhMPtCone",0) != string::npos) tmp_val1 = (leadph_ptcone- (leadph_mc_el_pt - leadph_mc_pt))/1000.;
	else if (tmp_name1.find("LeadPhM2PtCone",0) != string::npos) tmp_val1 = (leadph_ptcone- (leadph_mc_el_pt - leadph_pt))/1000.;
	else if (tmp_name1.find("LeadPhMCPt",0) != string::npos) tmp_val1 = leadph_mc_pt/1000.;
	else if (tmp_name1.find("LeadPhDrRecoTrue",0) != string::npos) tmp_val1 = leadph_drrecotrue;
	else if (tmp_name1.find("LeadPhMCBarcode",0) != string::npos) tmp_val1 = leadph_mc_barcode;
	else if (tmp_name1.find("LeadLepMCPt",0) != string::npos) tmp_val1 = leadlep_mc_pt/1000.;
	else if (tmp_name1.find("SubLepMCPt",0) != string::npos) tmp_val1 = sublep_mc_pt/1000.;
	else if (tmp_name1.find("LeadPhEta",0) != string::npos) tmp_val1 = leadph_eta;
	else if (tmp_name1.find("LeadPhAbsEta",0) != string::npos) tmp_val1 = fabs(leadph_eta);
	else if (tmp_name1.find("LeadLepPt",0) != string::npos) tmp_val1 = leadlep_pt/1000.;
	else if (tmp_name1.find("NGoodPh",0) != string::npos) tmp_val1 = event_ngoodphotons;
	else if (tmp_name1.find("SubLepPt",0) != string::npos) tmp_val1 = sublep_pt/1000.;
	else if (tmp_name1.find("LeadLepEta",0) != string::npos) tmp_val1 = leadlep_eta;
	else if (tmp_name1.find("SubLepEta",0) != string::npos) tmp_val1 = leadlep_eta;
	else if (tmp_name1.find("LeadLepAbsEta",0) != string::npos) tmp_val1 = fabs(leadlep_eta);
	else if (tmp_name1.find("SubLepAbsEta",0) != string::npos) tmp_val1 = fabs(leadlep_eta);
	else if (tmp_name1.find("MU",0) != string::npos) tmp_val1 = mu;
	else if (tmp_name1.find("MMCPhLep",0) != string::npos) tmp_val1 = mmcphl/1000.;
	else if (tmp_name1.find("MMCLepLep",0) != string::npos) tmp_val1 = mmcll/1000.;
	else if (tmp_name1.find("MLepMCLep",0) != string::npos) tmp_val1 = mlmcl/1000.;
	else if (tmp_name1.find("MMCLepMCLep",0) != string::npos) tmp_val1 = mmclmcl/1000.;
	else if (tmp_name1.find("LeadPhMCMMCPt",0) != string::npos) tmp_val1 = (leadph_mc_el_pt-leadph_mc_pt)/leadph_mc_pt; 	
	else if (tmp_name1.find("LeadPhMCElPt",0) != string::npos) tmp_val1 = leadph_mc_el_pt/1000.;
	else if (tmp_name1.find("LeadPhMCElDr",0) != string::npos) tmp_val1 = leadph_mc_el_dr;
	else if (tmp_name1.find("LeadJetEta",0) != string::npos) tmp_val1 = leadjet_eta;
	else if (tmp_name1.find("LeadJetAsm",0) != string::npos) tmp_val1 = leadjet_asm;
	else if (tmp_name1.find("SubJetEta",0) != string::npos) tmp_val1 = subjet_eta;
	else if (tmp_name1.find("LeadJetPt",0) != string::npos) tmp_val1 = leadjet_pt/1000.;
	else if (tmp_name1.find("SubJetPt",0) != string::npos) tmp_val1 = subjet_pt/1000.;
	else if (tmp_name1.find("dPhiLepMET",0) != string::npos) tmp_val1 = dphilepmet;
	else if (tmp_name1.find("dPhiPhMET",0) != string::npos) tmp_val1 = dphiphmet;
	else if (tmp_name1.find("MTLPhMET",0) != string::npos) tmp_val1 = mt_lphmet/1000.;
	else if (tmp_name1.find("MET",0) != string::npos) tmp_val1 = met/1000.;
	else if (tmp_name1.find("MPhLep",0) != string::npos && tmp_name1.find("MPhLepLep",0) == string::npos) tmp_val1 = mphl/1000.;
	else if (tmp_name1.find("MPhLepLep",0) != string::npos) tmp_val1 = mllg/1000.;
	else if (tmp_name1.find("MLepLep",0) != string::npos) tmp_val1 = mll/1000.;
	else if (tmp_name1.find("DrLepLep",0) != string::npos) tmp_val1 = drleplep;
	else if (tmp_name1.find("LeadPhPtCone",0) != string::npos) tmp_val1 = leadph_ptcone/1000.;
	else if (tmp_name1.find("Year2015",0) != string::npos) tmp_val1 = m_is2015;
	else if (tmp_name1.find("Period",0) != string::npos) tmp_val1 = m_period;
	else if (tmp_name1.find("PileUpMu",0) != string::npos) tmp_val1 = mu;
	else if (tmp_name1.find("LeadPhEtCone",0) != string::npos) tmp_val1 = leadph_etcone/1000.;
	else if (tmp_name1.find("ELD",0) != string::npos) tmp_val1 = event_ELD_MVA_correct;
	else if (tmp_name1 == "LeadPhMMCElPt") tmp_val1 = (leadph_pt-leadph_mc_el_pt)/leadph_mc_el_pt;
	else if (tmp_name1 == "LeadPhCvt") tmp_val1 = leadph_cvt;
	else if (tmp_name1 == "LeadPhBinCvt") tmp_val1 = leadph_bincvt;
	else if (tmp_name1 == "LeadPhMCEta") tmp_val1 = leadph_mc_eta/1000.;
	else if (tmp_name1 == "LeadPhMCPhi") tmp_val1 = leadph_mc_phi/1000.;
	else if (tmp_name1 == "LeadPhMCE") tmp_val1 = leadph_mc_e/1000.;
	else if (tmp_name1 == "LeadPhMMCPt") tmp_val1 = (leadph_pt-leadph_mc_pt)/leadph_mc_pt;
	else if (tmp_name1 == "LeadLepMMCPt") tmp_val1 = (leadlep_pt-leadlep_mc_pt)/leadlep_mc_pt;
	else if (tmp_name1 == "SubLepMMCPt") tmp_val1 = (sublep_pt-sublep_mc_pt)/sublep_mc_pt;
	else if (tmp_name1 == "LeadPhMCDr") tmp_val1 = leadph_mc_dr;
	else if (tmp_name1 == "LeadLepMCDr") tmp_val1 = leadlep_mc_dr;
	else if (tmp_name1 == "SubLepMCDr") tmp_val1 = sublep_mc_dr;
	else if (tmp_name1 == "LeadPhMMCE") tmp_val1 = (leadph_e-leadph_mc_e)/leadph_mc_e;
	else if (tmp_name1 == "LeadLepMMCE") tmp_val1 = (leadlep_e-leadlep_mc_e)/leadlep_mc_e;
	else if (tmp_name1 == "SubLepMMCE") tmp_val1 = (sublep_e-sublep_mc_e)/sublep_mc_e;
	else if (tmp_name1 == "LeadPhEta") tmp_val1 = leadph_eta;
	else if (tmp_name1 == "LeadPhEtaCoarse") tmp_val1 = leadph_eta;
	else if (tmp_name1 == "LeadPhAbsEtaForFR8TeV") tmp_val1 = fabs(leadph_eta);
	else if (tmp_name1.find("LeadPhMinDrPhLep",0) != string::npos) tmp_val1 = leadph_mindrphl;
	else if (tmp_name1.find("DrLeadJetLeadPh",0) != string::npos) tmp_val1 = drleadjet_leadph;
	else if (tmp_name1.find("DrLeadJetLeadLep",0) != string::npos) tmp_val1 = drleadjet_leadlep;
	else if (tmp_name1.find("M2J",0) != string::npos) tmp_val1 = m_12jet;
	else if (tmp_name1.find("LeadPhMinDrPhJet",0) != string::npos) tmp_val1 = leadph_mindrphj;
	else if (tmp_name1.find("DEtaLepLep",0) != string::npos) tmp_val1 = detaleplep;
	else if (tmp_name1.find("DPhiLepLep",0) != string::npos) tmp_val1 = dphileplep;
	else if (tmp_name1 == "SubLepMinDrPhLep") tmp_val1 = sublep_mindrphl;
	else if (tmp_name1 == "SubLepMinDrPhJet") tmp_val1 = sublep_mindrphj;
	else if (tmp_name1 == "SubLepMinDrPhLepCoarse") tmp_val1 = sublep_mindrphl;
	else if (tmp_name1 == "SubLepMinDrPhJetCoarse") tmp_val1 = sublep_mindrphj;
	else if (tmp_name1.find("LeadLepMinDrPhLep",0) != string::npos) tmp_val1 = leadlep_mindrphl;
	else if (tmp_name1.find("LeadLepMinDrPhJet",0) != string::npos) tmp_val1 = leadlep_mindrphj;
	else if (tmp_name1 == "LeadPhMCPID") tmp_val1 = leadph_mc_pid;
	else if (tmp_name1 == "LeadPhType") tmp_val1 = leadph_type;
	else if (tmp_name1 == "LeadPhOrigin") tmp_val1 = leadph_origin;
	else if (tmp_name1 == "LeadLepType") tmp_val1 = leadlep_type;
	else if (tmp_name1 == "LeadLepOrigin") tmp_val1 = leadlep_origin;
	else if (tmp_name1 == "SubLepType") tmp_val1 = sublep_type;
	else if (tmp_name1 == "SubLepOrigin") tmp_val1 = sublep_origin;
	else if (tmp_name1 == "LeadPhAncestor") tmp_val1 = leadph_ancestor;
	else if (tmp_name1 == "Njet") tmp_val1 = n_jet;
	else if (tmp_name1 == "Nbjet") tmp_val1 = n_bjet;
	else if (tmp_name1 == "Nbjet77") tmp_val1 = n_bjet77;
	else if (tmp_name1 == "SubLepPtCone") tmp_val1 = sublep_ptcone/1000.;
	else if (tmp_name1 == "SubLepEtCone") tmp_val1 = sublep_etcone/1000.;
	else if (tmp_name1 == "SubLepEta") tmp_val1 = sublep_eta;
	else if (tmp_name1 == "SubLepEtaCoarse") tmp_val1 = sublep_eta;
	else if (tmp_name1 == "SubLepAbsEtaForFR8TeV") tmp_val1 = sublep_eta;
	else if (tmp_name1.find("LeadPhLeadLepDphi",0) != string::npos) tmp_val1 = leadphleadlep_dphi;
	else if (tmp_name1 == "LeadLepSubLepDphi") tmp_val1 = leadlepsublep_dphi;
	else if (tmp_name1 == "LeadLepSubLepDphiCoarse") tmp_val1 = leadlepsublep_dphi;
	else if (tmp_name1 == "SubLepLeadLepDphi") tmp_val1 = sublepleadlep_dphi;
	else if (tmp_name1 == "SubLepLeadLepDphiCoarse") tmp_val1 = sublepleadlep_dphi;
	else if (tmp_name1 == "MU") tmp_val1 = mu;
	else if (tmp_name1.find("MWT",0) != string::npos) tmp_val1 = mwt/1000.;
	else if (tmp_name1.find("MWT2",0) != string::npos) tmp_val1 = mwt2/1000.;
	else {
	    m_lg->Err("ss", "Can't resolve histogram name ->", tmp_name1.c_str());
	    exit(-1);
	}
	if (tmp_name2.find("LeadPhPtRecoPtTrue",0) != string::npos) tmp_val2 = leadph_ptreco_pttrue;
	else if (tmp_name2.find("LeadPhPt",0) != string::npos && tmp_name2.find("LeadPhPtCone",0) == string::npos) tmp_val2 = leadph_pt/1000.;
	else if (tmp_name2.find("LeadPhMPtCone",0) != string::npos) tmp_val2 = (leadph_ptcone- (leadph_mc_el_pt - leadph_mc_pt))/1000.;
	else if (tmp_name2.find("LeadPhM2PtCone",0) != string::npos) tmp_val2 = (leadph_ptcone- (leadph_mc_pt - leadph_pt))/1000.;
	else if (tmp_name2.find("LeadPhMCPt",0) != string::npos) tmp_val2 = leadph_mc_pt/1000.;
	else if (tmp_name2.find("LeadPhDrRecoTrue",0) != string::npos) tmp_val2 = leadph_drrecotrue;
	else if (tmp_name2.find("LeadPhMCBarcode",0) != string::npos) tmp_val2 = leadph_mc_barcode;
	else if (tmp_name2.find("LeadLepMCPt",0) != string::npos) tmp_val2 = leadlep_mc_pt/1000.;
	else if (tmp_name2.find("SubLepMCPt",0) != string::npos) tmp_val2 = sublep_mc_pt/1000.;
	else if (tmp_name2.find("LeadPhEta",0) != string::npos) tmp_val2 = leadph_eta;
	else if (tmp_name2.find("LeadPhAbsEta",0) != string::npos) tmp_val2 = fabs(leadph_eta);
	else if (tmp_name2.find("LeadLepPt",0) != string::npos) tmp_val2 = leadlep_pt/1000.;
	else if (tmp_name2.find("NGoodPh",0) != string::npos) tmp_val2 = event_ngoodphotons;
	else if (tmp_name2.find("SubLepPt",0) != string::npos) tmp_val2 = sublep_pt/1000.;
	else if (tmp_name2.find("LeadLepEta",0) != string::npos) tmp_val2 = leadlep_eta;
	else if (tmp_name2.find("SubLepEta",0) != string::npos) tmp_val2 = leadlep_eta;
	else if (tmp_name2.find("LeadLepAbsEta",0) != string::npos) tmp_val2 = fabs(leadlep_eta);
	else if (tmp_name2.find("SubLepAbsEta",0) != string::npos) tmp_val2 = fabs(leadlep_eta);
	else if (tmp_name2.find("MU",0) != string::npos) tmp_val2 = mu;
	else if (tmp_name2.find("MMCPhLep",0) != string::npos) tmp_val2 = mmcphl/1000.;
	else if (tmp_name2.find("MMCLepLep",0) != string::npos) tmp_val2 = mmcll/1000.;
	else if (tmp_name2.find("MLepMCLep",0) != string::npos) tmp_val2 = mlmcl/1000.;
	else if (tmp_name2.find("MMCLepMCLep",0) != string::npos) tmp_val2 = mmclmcl/1000.;
	else if (tmp_name2.find("LeadPhMCMMCPt",0) != string::npos) tmp_val2 = (leadph_mc_el_pt-leadph_mc_pt)/leadph_mc_pt; 	
	else if (tmp_name2.find("LeadPhMCElPt",0) != string::npos) tmp_val2 = leadph_mc_el_pt/1000.;
	else if (tmp_name2.find("LeadPhMCElDr",0) != string::npos) tmp_val2 = leadph_mc_el_dr;
	else if (tmp_name2.find("LeadJetEta",0) != string::npos) tmp_val2 = leadjet_eta;
	else if (tmp_name2.find("LeadJetAsm",0) != string::npos) tmp_val2 = leadjet_asm;
	else if (tmp_name2.find("SubJetEta",0) != string::npos) tmp_val2 = subjet_eta;
	else if (tmp_name2.find("LeadJetPt",0) != string::npos) tmp_val2 = leadjet_pt/1000.;
	else if (tmp_name2.find("SubJetPt",0) != string::npos) tmp_val2 = subjet_pt/1000.;
	else if (tmp_name2.find("dPhiLepMET",0) != string::npos) tmp_val2 = dphilepmet;
	else if (tmp_name2.find("dPhiPhMET",0) != string::npos) tmp_val2 = dphiphmet;
	else if (tmp_name2.find("MTLPhMET",0) != string::npos) tmp_val2 = mt_lphmet/1000.;
	else if (tmp_name2.find("MET",0) != string::npos) tmp_val2 = met/1000.;
	else if (tmp_name2.find("MPhLep",0) != string::npos && tmp_name2.find("MPhLepLep",0) == string::npos) tmp_val2 = mphl/1000.;
	else if (tmp_name2.find("MPhLepLep",0) != string::npos) tmp_val2 = mllg/1000.;
	else if (tmp_name2.find("MLepLep",0) != string::npos) tmp_val2 = mll/1000.;
	else if (tmp_name2.find("DrLepLep",0) != string::npos) tmp_val2 = drleplep;
	else if (tmp_name2.find("LeadPhPtCone",0) != string::npos) tmp_val2 = leadph_ptcone/1000.;
	else if (tmp_name2.find("Year2015",0) != string::npos) tmp_val2 = m_is2015;
	else if (tmp_name2.find("Period",0) != string::npos) tmp_val2 = m_period;
	else if (tmp_name2.find("PileUpMu",0) != string::npos) tmp_val2 = mu;
	else if (tmp_name2.find("LeadPhEtCone",0) != string::npos) tmp_val2 = leadph_etcone/1000.;
	else if (tmp_name2.find("ELD",0) != string::npos) tmp_val2 = event_ELD_MVA_correct;
	else if (tmp_name2 == "LeadPhMMCElPt") tmp_val2 = (leadph_pt-leadph_mc_el_pt)/leadph_mc_el_pt;
	else if (tmp_name2 == "LeadPhCvt") tmp_val2 = leadph_cvt;
	else if (tmp_name2 == "LeadPhBinCvt") tmp_val2 = leadph_bincvt;
	else if (tmp_name2 == "LeadPhMCEta") tmp_val2 = leadph_mc_eta/1000.;
	else if (tmp_name2 == "LeadPhMCPhi") tmp_val2 = leadph_mc_phi/1000.;
	else if (tmp_name2 == "LeadPhMCE") tmp_val2 = leadph_mc_e/1000.;
	else if (tmp_name2 == "LeadPhMMCPt") tmp_val2 = (leadph_pt-leadph_mc_pt)/leadph_mc_pt;
	else if (tmp_name2 == "LeadLepMMCPt") tmp_val2 = (leadlep_pt-leadlep_mc_pt)/leadlep_mc_pt;
	else if (tmp_name2 == "SubLepMMCPt") tmp_val2 = (sublep_pt-sublep_mc_pt)/sublep_mc_pt;
	else if (tmp_name2 == "LeadPhMCDr") tmp_val2 = leadph_mc_dr;
	else if (tmp_name2 == "LeadLepMCDr") tmp_val2 = leadlep_mc_dr;
	else if (tmp_name2 == "SubLepMCDr") tmp_val2 = sublep_mc_dr;
	else if (tmp_name2 == "LeadPhMMCE") tmp_val2 = (leadph_e-leadph_mc_e)/leadph_mc_e;
	else if (tmp_name2 == "LeadLepMMCE") tmp_val2 = (leadlep_e-leadlep_mc_e)/leadlep_mc_e;
	else if (tmp_name2 == "SubLepMMCE") tmp_val2 = (sublep_e-sublep_mc_e)/sublep_mc_e;
	else if (tmp_name2 == "LeadPhEta") tmp_val2 = leadph_eta;
	else if (tmp_name2 == "LeadPhEtaCoarse") tmp_val2 = leadph_eta;
	else if (tmp_name2 == "LeadPhAbsEtaForFR8TeV") tmp_val2 = fabs(leadph_eta);
	else if (tmp_name2.find("LeadPhMinDrPhLep",0) != string::npos) tmp_val2 = leadph_mindrphl;
	else if (tmp_name2.find("DrLeadJetLeadPh",0) != string::npos) tmp_val2 = drleadjet_leadph;
	else if (tmp_name2.find("DrLeadJetLeadLep",0) != string::npos) tmp_val2 = drleadjet_leadlep;
	else if (tmp_name2.find("M2J",0) != string::npos) tmp_val2 = m_12jet;
	else if (tmp_name2.find("LeadPhMinDrPhJet",0) != string::npos) tmp_val2 = leadph_mindrphj;
	else if (tmp_name2.find("DEtaLepLep",0) != string::npos) tmp_val2 = detaleplep;
	else if (tmp_name2.find("DPhiLepLep",0) != string::npos) tmp_val2 = dphileplep;
	else if (tmp_name2 == "SubLepMinDrPhLep") tmp_val2 = sublep_mindrphl;
	else if (tmp_name2 == "SubLepMinDrPhJet") tmp_val2 = sublep_mindrphj;
	else if (tmp_name2 == "SubLepMinDrPhLepCoarse") tmp_val2 = sublep_mindrphl;
	else if (tmp_name2 == "SubLepMinDrPhJetCoarse") tmp_val2 = sublep_mindrphj;
	else if (tmp_name2.find("LeadLepMinDrPhLep",0) != string::npos) tmp_val2 = leadlep_mindrphl;
	else if (tmp_name2.find("LeadLepMinDrPhJet",0) != string::npos) tmp_val2 = leadlep_mindrphj;
	else if (tmp_name2 == "LeadPhMCPID") tmp_val2 = leadph_mc_pid;
	else if (tmp_name2 == "LeadPhType") tmp_val2 = leadph_type;
	else if (tmp_name2 == "LeadPhOrigin") tmp_val2 = leadph_origin;
	else if (tmp_name2 == "LeadLepType") tmp_val2 = leadlep_type;
	else if (tmp_name2 == "LeadLepOrigin") tmp_val2 = leadlep_origin;
	else if (tmp_name2 == "SubLepType") tmp_val2 = sublep_type;
	else if (tmp_name2 == "SubLepOrigin") tmp_val2 = sublep_origin;
	else if (tmp_name2 == "LeadPhAncestor") tmp_val2 = leadph_ancestor;
	else if (tmp_name2 == "Njet") tmp_val2 = n_jet;
	else if (tmp_name2 == "Nbjet") tmp_val2 = n_bjet;
	else if (tmp_name2 == "Nbjet77") tmp_val2 = n_bjet77;
	else if (tmp_name2 == "SubLepPtCone") tmp_val2 = sublep_ptcone/1000.;
	else if (tmp_name2 == "SubLepEtCone") tmp_val2 = sublep_etcone/1000.;
	else if (tmp_name2 == "SubLepEta") tmp_val2 = sublep_eta;
	else if (tmp_name2 == "SubLepEtaCoarse") tmp_val2 = sublep_eta;
	else if (tmp_name2 == "SubLepAbsEtaForFR8TeV") tmp_val2 = sublep_eta;
	else if (tmp_name2.find("LeadPhLeadLepDphi",0) != string::npos) tmp_val2 = leadphleadlep_dphi;
	else if (tmp_name2 == "LeadLepSubLepDphi") tmp_val2 = leadlepsublep_dphi;
	else if (tmp_name2 == "LeadLepSubLepDphiCoarse") tmp_val2 = leadlepsublep_dphi;
	else if (tmp_name2 == "SubLepLeadLepDphi") tmp_val2 = sublepleadlep_dphi;
	else if (tmp_name2 == "SubLepLeadLepDphiCoarse") tmp_val2 = sublepleadlep_dphi;
	else if (tmp_name2 == "MU") tmp_val2 = mu;
	else if (tmp_name2.find("MWT",0) != string::npos) tmp_val2 = mwt/1000.;
	else if (tmp_name2.find("MWT2",0) != string::npos) tmp_val2 = mwt2/1000.;
	else {
	    m_lg->Err("ss", "Can't resolve histogram name ->", tmp_name2.c_str());
	    exit(-1);
	}
	
	//if (tmp_name2.find("LeadPhPtRecoPtTrue",0) != string::npos) cout << tmp_val2 << endl;
	tmp_h->Fill(tmp_val1, tmp_val2, weight);
    }

    if (m_do_egammahists) {
	double val_mass = -1;
	double val_pt = -1;
	double val_eta = 999;
	if (m_subregion == "zee") {
	    val_mass = mll/1000.;
	    if (m_selection.find("Reverse",0) == string::npos) {
		val_pt = sublep_pt/1000.;
		val_eta = fabs(sublep_eta);
	    } else {
		val_pt = leadlep_pt/1000.;
		val_eta = fabs(leadlep_eta);
	    }
	} else if (m_subregion == "zeg") {
	    val_mass = mphl/1000.;
	    val_pt = leadph_pt/1000.;
	    val_eta = fabs(leadph_eta);
	}
	for (int i = 0; i < m_egammahists_pt.size(); i++) {
	    TH1F* tmp_h = m_egammahists_pt.at(i);
	    if (val_pt >= m_egammaptbins_lo.at(i) && val_pt < m_egammaptbins_hi.at(i)) tmp_h->Fill(val_mass, weight);
	}
	for (int i = 0; i < m_egammahists_eta.size(); i++) {
	    TH1F* tmp_h = m_egammahists_eta.at(i);
	    if (val_eta >= m_egammaetabins_lo.at(i) && val_eta < m_egammaetabins_hi.at(i)) tmp_h->Fill(val_mass, weight);
	}
	for (int i = 0; i < m_egammahists_pteta.size(); i++) {
	    vector<TH1F*> tmp_hists = m_egammahists_pteta.at(i);
	    for (int j = 0; j < tmp_hists.size(); j++) {
	        TH1F* tmp_h = tmp_hists.at(j);
	        if (val_pt >= m_egammapt2dbins_lo.at(i) && val_pt < m_egammapt2dbins_hi.at(i) && val_eta >= m_egammaeta2dbins_lo.at(j) && val_eta < m_egammaeta2dbins_hi.at(j)) tmp_h->Fill(val_mass, weight);
	    }
	}
	if (m_do_egammatf1) {
    	    m_tkde_data.push_back(val_mass);
    	    m_tkde_data_w.push_back(weight);
	    for (int i = 0; i < m_tkde_data_pteta.size(); i++) {
	        for (int j = 0; j < m_tkde_data_pteta.at(i).size(); j++) {
	            if (val_pt >= m_egammapt2dbins_lo.at(i) && val_pt < m_egammapt2dbins_hi.at(i) && val_eta >= m_egammaeta2dbins_lo.at(j) && val_eta < m_egammaeta2dbins_hi.at(j)) {
			m_tkde_data_pteta.at(i).at(j).push_back(val_mass);
			m_tkde_data_pteta_w.at(i).at(j).push_back(weight);
		    }
	        }
	    }
    	}
    }

    for (int i = 0; i < h_Cutflow_QCD.size(); i++) {
	h_LeadLepPt_QCD.at(i)->Fill(leadlep_pt/1000., weights_QCD.at(i));
	h_LeadJetPt_QCD.at(i)->Fill(leadjet_pt/1000., weights_QCD.at(i));
	h_LeadPhPt_QCD.at(i)->Fill(leadph_pt/1000., weights_QCD.at(i));
	h_MWT_QCD.at(i)->Fill(mwt/1000., weights_QCD.at(i));
	h_MET_QCD.at(i)->Fill(met/1000., weights_QCD.at(i));
	h_dPhiLepMET_QCD.at(i)->Fill(dphilepmet, weights_QCD.at(i));
    }

    if (m_type == "Data" && m_do_bootstrap) {
	for (int i = 0; i < 1000; i++) {
	    h_bootstrap_LeadPhPt.at(i)->Fill(leadph_pt/1000., weights_bootstrap.at(i));
	    h_bootstrap_LeadPhEta.at(i)->Fill(fabs(leadph_eta), weights_bootstrap.at(i));
	    h_bootstrap_LeadPhMinDrPhLep.at(i)->Fill(leadph_mindrphl, weights_bootstrap.at(i));
	    h_bootstrap_DEtaLepLep.at(i)->Fill(leadlepsublep_deta, weights_bootstrap.at(i));
	    h_bootstrap_DPhiLepLep.at(i)->Fill(leadlepsublep_dphi, weights_bootstrap.at(i));
	}
    }

}

void SampleAnalyzor::DoKLFit(bool fit, bool veto, bool fixtop) {
    m_do_klfitter = fit;
    //m_kl_bveto = veto;
    //m_kl_fixtop = fixtop;
}

void SampleAnalyzor::KLFit() {
//    m_particles = new KLFitter::Particles();
//    m_particles2 = new KLFitter::Particles();
//    for (int i = 0; i < m_vjet_klfitter.size(); i++) {
//	if (i < 4) {
//	    bool isbjet = false;
//	    if (jet_mv2c10->at(i) > 0.645925) isbjet = true;
//	    m_particles->AddParticle(m_vjet_klfitter.at(i), m_vjet_klfitter.at(i)->Eta(), KLFitter::Particles::kParton, "", i, m_kl_bveto&&isbjet);
//	    m_particles2->AddParticle(m_vjet_klfitter.at(i), m_vjet_klfitter.at(i)->Eta(), KLFitter::Particles::kParton, "", i, m_kl_bveto&&isbjet);
//	}
//    }
//    if (m_subregion == "ejets") m_particles->AddParticle(m_el_klfitter, tlv_leadlep.Eta(), KLFitter::Particles::kElectron, "", 0);
//    else m_particles->AddParticle(m_mu_klfitter, tlv_leadlep.Eta(), KLFitter::Particles::kMuon, "", 0);
//    if (m_subregion == "ejets") m_particles2->AddParticle(m_el_klfitter, tlv_leadlep.Eta(), KLFitter::Particles::kElectron, "", 0);
//    else m_particles2->AddParticle(m_mu_klfitter, tlv_leadlep.Eta(), KLFitter::Particles::kMuon, "", 0);
//    m_particles->AddParticle(m_ph_klfitter, tlv_leadph.Eta(), KLFitter::Particles::kPhoton, "", 0);
//    if (m_particles->NPartons() < 4) {
//        m_lg->Err("s", "Less than 4 jets!");
//        exit(-1);
//    } 
//    if (m_particles2->NPartons() < 4) {
//        m_lg->Err("s", "Less than 4 jets!");
//        exit(-1);
//    } 
//    if (!((m_particles->NElectrons() != 1 && m_particles->NMuons() != 0) || (m_particles->NElectrons() != 0 && m_particles->NMuons() != 1))) {
//        m_lg->Err("s", "Wrong lepton number!");
//        exit(-1);
//    } 
//    if (!((m_particles2->NElectrons() != 1 && m_particles2->NMuons() != 0) || (m_particles2->NElectrons() != 0 && m_particles2->NMuons() != 1))) {
//        m_lg->Err("s", "Wrong lepton number!");
//        exit(-1);
//    } 
//    if (!m_klfitter->SetParticles(m_particles2)) {
//        m_lg->Err("s", "Fail to add particles to KLFitter!");
//        exit(-1);
//    }
//    if (!m_klfitter_lt->SetParticles(m_particles)) {
//        m_lg->Err("s", "Fail to add particles to KLFitter!");
//        exit(-1);
//    }
//    if (!m_klfitter_ht->SetParticles(m_particles)) {
//        m_lg->Err("s", "Fail to add particles to KLFitter!");
//        exit(-1);
//    }
//    if (!m_klfitter_lW->SetParticles(m_particles)) {
//        m_lg->Err("s", "Fail to add particles to KLFitter!");
//        exit(-1);
//    }
//    if (!m_klfitter_hW->SetParticles(m_particles)) {
//        m_lg->Err("s", "Fail to add particles to KLFitter!");
//        exit(-1);
//    }
//    if (!m_klfitter->SetET_miss_XY_SumET(met_x/1000., met_y/1000., met_met/1000.)) {
//        m_lg->Err("s", "Fail to set MET to KLFitter!");
//        exit(-1);
//    }
//    if (!m_klfitter_lt->SetET_miss_XY_SumET(met_x/1000., met_y/1000., met_met/1000.)) {
//        m_lg->Err("s", "Fail to set MET to KLFitter!");
//        exit(-1);
//    }
//    if (!m_klfitter_ht->SetET_miss_XY_SumET(met_x/1000., met_y/1000., met_met/1000.)) {
//        m_lg->Err("s", "Fail to set MET to KLFitter!");
//        exit(-1);
//    }
//    if (!m_klfitter_lW->SetET_miss_XY_SumET(met_x/1000., met_y/1000., met_met/1000.)) {
//        m_lg->Err("s", "Fail to set MET to KLFitter!");
//        exit(-1);
//    }
//    if (!m_klfitter_hW->SetET_miss_XY_SumET(met_x/1000., met_y/1000., met_met/1000.)) {
//        m_lg->Err("s", "Fail to set MET to KLFitter!");
//        exit(-1);
//    }
//
//    kl_lh_best = -99999999;
//    for (int iPerm = 0, nPerms(m_klfitter->Permutations()->NPermutations()); iPerm < nPerms; ++iPerm) {
//        m_klfitter->Fit(iPerm); 
//        
//        int ConvergenceStatusBitWord = m_klfitter->ConvergenceStatus();
//	if (ConvergenceStatusBitWord != 0) continue;
//        double LogLikelihood = m_klfitter->Likelihood()->LogLikelihood(m_klfitter->Likelihood()->GetBestFitParameters());
//        double EventProbability = exp(m_klfitter->Likelihood()->LogEventProbability());
//        KLFitter::Particles * OutputParticles = m_klfitter->Likelihood()->ParticlesModel();
//        KLFitter::Particles * InputParticles = *m_klfitter->Likelihood()->PParticlesPermuted();
//        std::vector<double> Par = m_klfitter->Likelihood()->GetBestFitParameters();
//	double top_pole_mass = Par[KLFitter::LikelihoodTopLeptonJets::parTopM];
//	double nu_pz = Par[KLFitter::LikelihoodTopLeptonJets::parNuPz];
//
//	TLorentzVector* hb = InputParticles->Parton(0);
//	TLorentzVector* hq1 = InputParticles->Parton(2);
//	TLorentzVector* hq2 = InputParticles->Parton(3);
//	TLorentzVector hW = (*hq1) + (*hq2);
//	TLorentzVector htop = hW + (*hb);
//
//	TLorentzVector* lb = InputParticles->Parton(1);
//	TLorentzVector* lep; if (m_subregion == "ejets") lep = InputParticles->Electron(0); else lep = InputParticles->Muon(0);
//	TLorentzVector nu; nu.SetXYZM(met_x/1000., met_y/1000., nu_pz, 0); 
//	TLorentzVector lW = (*lep) + nu;
//	TLorentzVector ltop = lW + (*lb);
//
//	double CA = el_charge->at(0) > 0 ? fabs(ltop.Rapidity()) - fabs(htop.Rapidity()) : fabs(htop.Rapidity()) - fabs(ltop.Rapidity());
//
//	double hW_mass = hW.M();
//	double lW_mass = lW.M();
//	double htop_mass = htop.M();
//	double ltop_mass = ltop.M();
//	double htop_pt = htop.Pt();
//	double ltop_pt = ltop.Pt();
//	double htop_eta = htop.Eta();
//	double ltop_eta = ltop.Eta();
//
//	if (LogLikelihood > kl_lh_best) {
//	    kl_CA = CA;
//	    kl_lh_best = LogLikelihood;
//	    kl_top_pole_mass = top_pole_mass;
//	    kl_reco_hW_mass = hW_mass;
//	    kl_reco_lW_mass = lW_mass;
//	    kl_reco_htop_mass = htop_mass;
//	    kl_reco_ltop_mass = ltop_mass;
//	    kl_reco_htop_pt = htop_pt;
//	    kl_reco_ltop_pt = ltop_pt;
//	    kl_reco_htop_eta = htop_eta;
//	    kl_reco_ltop_eta = ltop_eta;
//	}
//    }
//
//    kl_lt_lh_best = -99999999;
//    for (int iPerm = 0, nPerms(m_klfitter_lt->Permutations()->NPermutations()); iPerm < nPerms; ++iPerm) {
//        m_klfitter_lt->Fit(iPerm); 
//        
//        int ConvergenceStatusBitWord = m_klfitter_lt->ConvergenceStatus();
//	if (ConvergenceStatusBitWord != 0) continue;
//        double LogLikelihood = m_klfitter_lt->Likelihood()->LogLikelihood(m_klfitter_lt->Likelihood()->GetBestFitParameters());
//        double EventProbability = exp(m_klfitter_lt->Likelihood()->LogEventProbability());
//        KLFitter::Particles * OutputParticles = m_klfitter_lt->Likelihood()->ParticlesModel();
//        KLFitter::Particles * InputParticles = *m_klfitter_lt->Likelihood()->PParticlesPermuted();
//        std::vector<double> Par = m_klfitter_lt->Likelihood()->GetBestFitParameters();
//	double top_pole_mass = Par[KLFitter::LikelihoodTopLeptonLPhotonJets::parTopM];
//	double nu_pz = Par[KLFitter::LikelihoodTopLeptonLPhotonJets::parNuPz];
//
//	TLorentzVector* hb = InputParticles->Parton(0);
//	TLorentzVector* hq1 = InputParticles->Parton(2);
//	TLorentzVector* hq2 = InputParticles->Parton(3);
//	TLorentzVector hW = (*hq1) + (*hq2);
//	TLorentzVector htop = hW + (*hb);
//
//	TLorentzVector* lb = InputParticles->Parton(1);
//	TLorentzVector* lep; if (m_subregion == "ejets") lep = InputParticles->Electron(0); else lep = InputParticles->Muon(0);
//	TLorentzVector nu; nu.SetXYZM(met_x/1000., met_y/1000., nu_pz, 0); 
//	TLorentzVector lW = (*lep) + nu;
//	TLorentzVector* ph = InputParticles->Photon(0);
//	TLorentzVector ltop = lW + (*lb) + (*ph);
//
//	double CA = el_charge->at(0) > 0 ? fabs(ltop.Rapidity()) - fabs(htop.Rapidity()) : fabs(htop.Rapidity()) - fabs(ltop.Rapidity());
//
//	double hW_mass = hW.M();
//	double lW_mass = lW.M();
//	double htop_mass = htop.M();
//	double ltop_mass = ltop.M();
//	double htop_pt = htop.Pt();
//	double ltop_pt = ltop.Pt();
//	double htop_eta = htop.Eta();
//	double ltop_eta = ltop.Eta();
//
//	if (LogLikelihood > kl_lt_lh_best) {
//	    kl_lt_CA = CA;
//	    kl_lt_lh_best = LogLikelihood;
//	    kl_lt_top_pole_mass = top_pole_mass;
//	    kl_lt_reco_hW_mass = hW_mass;
//	    kl_lt_reco_lW_mass = lW_mass;
//	    kl_lt_reco_htop_mass = htop_mass;
//	    kl_lt_reco_ltop_mass = ltop_mass;
//	    kl_lt_reco_htop_pt = htop_pt;
//	    kl_lt_reco_ltop_pt = ltop_pt;
//	    kl_lt_reco_htop_eta = htop_eta;
//	    kl_lt_reco_ltop_eta = ltop_eta;
//	}
//    }
//
//    kl_ht_lh_best = -99999999;
//    for (int iPerm = 0, nPerms(m_klfitter_ht->Permutations()->NPermutations()); iPerm < nPerms; ++iPerm) {
//        m_klfitter_ht->Fit(iPerm); 
//        
//        int ConvergenceStatusBitWord = m_klfitter_ht->ConvergenceStatus();
//	if (ConvergenceStatusBitWord != 0) continue;
//        double LogLikelihood = m_klfitter_ht->Likelihood()->LogLikelihood(m_klfitter_ht->Likelihood()->GetBestFitParameters());
//        double EventProbability = exp(m_klfitter_ht->Likelihood()->LogEventProbability());
//        KLFitter::Particles * OutputParticles = m_klfitter_ht->Likelihood()->ParticlesModel();
//        KLFitter::Particles * InputParticles = *m_klfitter_ht->Likelihood()->PParticlesPermuted();
//        std::vector<double> Par = m_klfitter_ht->Likelihood()->GetBestFitParameters();
//	double top_pole_mass = Par[KLFitter::LikelihoodTopLeptonHPhotonJets::parTopM];
//	double nu_pz = Par[KLFitter::LikelihoodTopLeptonHPhotonJets::parNuPz];
//
//	TLorentzVector* hb = InputParticles->Parton(0);
//	TLorentzVector* hq1 = InputParticles->Parton(2);
//	TLorentzVector* hq2 = InputParticles->Parton(3);
//	TLorentzVector hW = (*hq1) + (*hq2);
//	TLorentzVector* ph = InputParticles->Photon(0);
//	TLorentzVector htop = hW + (*hb) + (*ph);
//
//	TLorentzVector* lb = InputParticles->Parton(1);
//	TLorentzVector* lep; if (m_subregion == "ejets") lep = InputParticles->Electron(0); else lep = InputParticles->Muon(0);
//	TLorentzVector  nu; nu.SetXYZM(met_x/1000., met_y/1000., nu_pz, 0); 
//	TLorentzVector lW = (*lep) + nu;
//	TLorentzVector ltop = lW + (*lb);
//
//	double CA = (el_charge->at(0) > 0.) ? (fabs(ltop.Rapidity()) - fabs(htop.Rapidity())) : (fabs(htop.Rapidity()) - fabs(ltop.Rapidity()));
//
//	double hW_mass = hW.M();
//	double lW_mass = lW.M();
//	double htop_mass = htop.M();
//	double ltop_mass = ltop.M();
//	double htop_pt = htop.Pt();
//	double ltop_pt = ltop.Pt();
//	double htop_eta = htop.Eta();
//	double ltop_eta = ltop.Eta();
//
//	if (LogLikelihood > kl_ht_lh_best) {
//	    kl_ht_CA = CA;
//	    kl_ht_lh_best = LogLikelihood;
//	    kl_ht_top_pole_mass = top_pole_mass;
//	    kl_ht_reco_hW_mass = hW_mass;
//	    kl_ht_reco_lW_mass = lW_mass;
//	    kl_ht_reco_htop_mass = htop_mass;
//	    kl_ht_reco_ltop_mass = ltop_mass;
//	    kl_ht_reco_htop_pt = htop_pt;
//	    kl_ht_reco_ltop_pt = ltop_pt;
//	    kl_ht_reco_htop_eta = htop_eta;
//	    kl_ht_reco_ltop_eta = ltop_eta;
//	}
//    }
//
//    kl_lW_lh_best = -99999999;
//    for (int iPerm = 0, nPerms(m_klfitter_lW->Permutations()->NPermutations()); iPerm < nPerms; ++iPerm) {
//        m_klfitter_lW->Fit(iPerm); 
//        
//        int ConvergenceStatusBitWord = m_klfitter_lW->ConvergenceStatus();
//	if (ConvergenceStatusBitWord != 0) continue;
//        double LogLikelihood = m_klfitter_lW->Likelihood()->LogLikelihood(m_klfitter_lW->Likelihood()->GetBestFitParameters());
//        double EventProbability = exp(m_klfitter_lW->Likelihood()->LogEventProbability());
//        KLFitter::Particles * OutputParticles = m_klfitter_lW->Likelihood()->ParticlesModel();
//        KLFitter::Particles * InputParticles = *m_klfitter_lW->Likelihood()->PParticlesPermuted();
//        std::vector<double> Par = m_klfitter_lW->Likelihood()->GetBestFitParameters();
//	double top_pole_mass = Par[KLFitter::LikelihoodTopLeptonLWPhotonJets::parTopM];
//	double nu_pz = Par[KLFitter::LikelihoodTopLeptonLWPhotonJets::parNuPz];
//
//	TLorentzVector* hb = InputParticles->Parton(0);
//	TLorentzVector* hq1 = InputParticles->Parton(2);
//	TLorentzVector* hq2 = InputParticles->Parton(3);
//	TLorentzVector hW = (*hq1) + (*hq2);
//	TLorentzVector htop = hW + (*hb);
//
//	TLorentzVector* lb = InputParticles->Parton(1);
//	TLorentzVector* lep; if (m_subregion == "ejets") lep = InputParticles->Electron(0); else lep = InputParticles->Muon(0);
//	TLorentzVector nu; nu.SetXYZM(met_x/1000., met_y/1000., nu_pz, 0); 
//	TLorentzVector* ph = InputParticles->Photon(0);
//	TLorentzVector lW = (*lep) + nu + (*ph);
//	TLorentzVector ltop = lW + (*lb);
//
//	double CA = el_charge->at(0) > 0 ? fabs(ltop.Rapidity()) - fabs(htop.Rapidity()) : fabs(htop.Rapidity()) - fabs(ltop.Rapidity());
//
//	double hW_mass = hW.M();
//	double lW_mass = lW.M();
//	double htop_mass = htop.M();
//	double ltop_mass = ltop.M();
//	double htop_pt = htop.Pt();
//	double ltop_pt = ltop.Pt();
//	double htop_eta = htop.Eta();
//	double ltop_eta = ltop.Eta();
//
//	if (LogLikelihood > kl_lW_lh_best) {
//	    kl_lW_CA = CA;
//	    kl_lW_lh_best = LogLikelihood;
//	    kl_lW_top_pole_mass = top_pole_mass;
//	    kl_lW_reco_hW_mass = hW_mass;
//	    kl_lW_reco_lW_mass = lW_mass;
//	    kl_lW_reco_htop_mass = htop_mass;
//	    kl_lW_reco_ltop_mass = ltop_mass;
//	    kl_lW_reco_htop_pt = htop_pt;
//	    kl_lW_reco_ltop_pt = ltop_pt;
//	    kl_lW_reco_htop_eta = htop_eta;
//	    kl_lW_reco_ltop_eta = ltop_eta;
//	}
//    }
//
//    kl_hW_lh_best = -99999999;
//    for (int iPerm = 0, nPerms(m_klfitter_hW->Permutations()->NPermutations()); iPerm < nPerms; ++iPerm) {
//        m_klfitter_hW->Fit(iPerm); 
//        
//        int ConvergenceStatusBitWord = m_klfitter_hW->ConvergenceStatus();
//	if (ConvergenceStatusBitWord != 0) continue;
//        double LogLikelihood = m_klfitter_hW->Likelihood()->LogLikelihood(m_klfitter_hW->Likelihood()->GetBestFitParameters());
//        double EventProbability = exp(m_klfitter_hW->Likelihood()->LogEventProbability());
//        KLFitter::Particles * OutputParticles = m_klfitter_hW->Likelihood()->ParticlesModel();
//        KLFitter::Particles * InputParticles = *m_klfitter_hW->Likelihood()->PParticlesPermuted();
//        std::vector<double> Par = m_klfitter_hW->Likelihood()->GetBestFitParameters();
//	double top_pole_mass = Par[KLFitter::LikelihoodTopLeptonHWPhotonJets::parTopM];
//	double nu_pz = Par[KLFitter::LikelihoodTopLeptonHWPhotonJets::parNuPz];
//
//	TLorentzVector* hb = InputParticles->Parton(0);
//	TLorentzVector* hq1 = InputParticles->Parton(2);
//	TLorentzVector* hq2 = InputParticles->Parton(3);
//	TLorentzVector* ph = InputParticles->Photon(0);
//	TLorentzVector hW = (*hq1) + (*hq2) + (*ph);
//	TLorentzVector htop = hW + (*hb);
//
//	TLorentzVector* lb = InputParticles->Parton(1);
//	TLorentzVector* lep; if (m_subregion == "ejets") lep = InputParticles->Electron(0); else lep = InputParticles->Muon(0);
//	TLorentzVector  nu; nu.SetXYZM(met_x/1000., met_y/1000., nu_pz, 0); 
//	TLorentzVector lW = (*lep) + nu;
//	TLorentzVector ltop = lW + (*lb);
//
//	double CA = (el_charge->at(0) > 0.) ? (fabs(ltop.Rapidity()) - fabs(htop.Rapidity())) : (fabs(htop.Rapidity()) - fabs(ltop.Rapidity()));
//
//	double hW_mass = hW.M();
//	double lW_mass = lW.M();
//	double htop_mass = htop.M();
//	double ltop_mass = ltop.M();
//	double htop_pt = htop.Pt();
//	double ltop_pt = ltop.Pt();
//	double htop_eta = htop.Eta();
//	double ltop_eta = ltop.Eta();
//
//	if (LogLikelihood > kl_hW_lh_best) {
//	    kl_hW_CA = CA;
//	    kl_hW_lh_best = LogLikelihood;
//	    kl_hW_top_pole_mass = top_pole_mass;
//	    kl_hW_reco_hW_mass = hW_mass;
//	    kl_hW_reco_lW_mass = lW_mass;
//	    kl_hW_reco_htop_mass = htop_mass;
//	    kl_hW_reco_ltop_mass = ltop_mass;
//	    kl_hW_reco_htop_pt = htop_pt;
//	    kl_hW_reco_ltop_pt = ltop_pt;
//	    kl_hW_reco_htop_eta = htop_eta;
//	    kl_hW_reco_ltop_eta = ltop_eta;
//	}
//    }
//
//    kl_best_lh_best = -99999999;
//    kl_best_ph_lh_best = -99999999;
//    if (kl_best_lh_best < kl_lh_best) {
//	kl_best_CA = kl_CA;
//	kl_best_lh_best = kl_lh_best;
//	kl_best_top_pole_mass = kl_top_pole_mass;
//	kl_best_reco_hW_mass = kl_reco_hW_mass;
//	kl_best_reco_lW_mass = kl_reco_lW_mass;
//	kl_best_reco_htop_mass = kl_reco_htop_mass;
//	kl_best_reco_ltop_mass = kl_reco_ltop_mass;
//	kl_best_reco_htop_pt = kl_reco_htop_pt;
//	kl_best_reco_ltop_pt = kl_reco_ltop_pt;
//	kl_best_reco_htop_eta = kl_reco_htop_eta;
//	kl_best_reco_ltop_eta = kl_reco_ltop_eta;
//    }
//    if (kl_best_lh_best < kl_lt_lh_best) {
//	kl_best_CA = kl_lt_CA;
//	kl_best_lh_best = kl_lt_lh_best;
//	kl_best_top_pole_mass = kl_lt_top_pole_mass;
//	kl_best_reco_hW_mass = kl_lt_reco_hW_mass;
//	kl_best_reco_lW_mass = kl_lt_reco_lW_mass;
//	kl_best_reco_htop_mass = kl_lt_reco_htop_mass;
//	kl_best_reco_ltop_mass = kl_lt_reco_ltop_mass;
//	kl_best_reco_htop_pt = kl_lt_reco_htop_pt;
//	kl_best_reco_ltop_pt = kl_lt_reco_ltop_pt;
//	kl_best_reco_htop_eta = kl_lt_reco_htop_eta;
//	kl_best_reco_ltop_eta = kl_lt_reco_ltop_eta;
//    }
//    if (kl_best_lh_best < kl_ht_lh_best) {
//	kl_best_CA = kl_ht_CA;
//	kl_best_lh_best = kl_ht_lh_best;
//	kl_best_top_pole_mass = kl_ht_top_pole_mass;
//	kl_best_reco_hW_mass = kl_ht_reco_hW_mass;
//	kl_best_reco_lW_mass = kl_ht_reco_lW_mass;
//	kl_best_reco_htop_mass = kl_ht_reco_htop_mass;
//	kl_best_reco_ltop_mass = kl_ht_reco_ltop_mass;
//	kl_best_reco_htop_pt = kl_ht_reco_htop_pt;
//	kl_best_reco_ltop_pt = kl_ht_reco_ltop_pt;
//	kl_best_reco_htop_eta = kl_ht_reco_htop_eta;
//	kl_best_reco_ltop_eta = kl_ht_reco_ltop_eta;
//    }
//    if (kl_best_lh_best < kl_lW_lh_best) {
//	kl_best_CA = kl_lW_CA;
//	kl_best_lh_best = kl_lW_lh_best;
//	kl_best_top_pole_mass = kl_lW_top_pole_mass;
//	kl_best_reco_hW_mass = kl_lW_reco_hW_mass;
//	kl_best_reco_lW_mass = kl_lW_reco_lW_mass;
//	kl_best_reco_htop_mass = kl_lW_reco_htop_mass;
//	kl_best_reco_ltop_mass = kl_lW_reco_ltop_mass;
//	kl_best_reco_htop_pt = kl_lW_reco_htop_pt;
//	kl_best_reco_ltop_pt = kl_lW_reco_ltop_pt;
//	kl_best_reco_htop_eta = kl_lW_reco_htop_eta;
//	kl_best_reco_ltop_eta = kl_lW_reco_ltop_eta;
//    }
//    if (kl_best_lh_best < kl_hW_lh_best) {
//	kl_best_CA = kl_hW_CA;
//	kl_best_lh_best = kl_hW_lh_best;
//	kl_best_top_pole_mass = kl_hW_top_pole_mass;
//	kl_best_reco_hW_mass = kl_hW_reco_hW_mass;
//	kl_best_reco_lW_mass = kl_hW_reco_lW_mass;
//	kl_best_reco_htop_mass = kl_hW_reco_htop_mass;
//	kl_best_reco_ltop_mass = kl_hW_reco_ltop_mass;
//	kl_best_reco_htop_pt = kl_hW_reco_htop_pt;
//	kl_best_reco_ltop_pt = kl_hW_reco_ltop_pt;
//	kl_best_reco_htop_eta = kl_hW_reco_htop_eta;
//	kl_best_reco_ltop_eta = kl_hW_reco_ltop_eta;
//    }
//    if (kl_best_ph_lh_best < kl_lt_lh_best) {
//	kl_best_ph_lh_best = kl_lt_lh_best;
//    }
//    if (kl_best_ph_lh_best < kl_ht_lh_best) {
//	kl_best_ph_lh_best = kl_ht_lh_best;
//    }
//    if (kl_best_ph_lh_best < kl_lW_lh_best) {
//	kl_best_ph_lh_best = kl_lW_lh_best;
//    }
//    if (kl_best_ph_lh_best < kl_hW_lh_best) {
//	kl_best_ph_lh_best = kl_hW_lh_best;
//    }
}
//    
//        //if (m_firstevent) {
//	//    m_firstevent = false;
//        //    printf("----------------------------------------------------------------------------------------------\n");
//        //    printf("----------------------------------------Permutation %2i----------------------------------------\n",iPerm);
//        //    printf("----------------------------------------------------------------------------------------------\n");
//        //    printf("                  | hadronic b quark | leptonic b quark  |  light quark 1   |  light quark 2  |\n");
//        //    printf("Input Energies    | %16.2f | %17.2f | %16.2f | %15.2f |\n",
//        //    InputParticles->Parton(0)->E(), InputParticles->Parton(1)->E(),
//        //    InputParticles->Parton(2)->E(), InputParticles->Parton(3)->E() );
//        //    printf("Output Energies   | %16.2f | %17.2f | %16.2f | %15.2f |\n",
//        //    OutputParticles->Parton(0)->E(), OutputParticles->Parton(1)->E(),
//        //    OutputParticles->Parton(2)->E(), OutputParticles->Parton(3)->E() );
//        //    printf("----------------------------------------------------------------------------------------------\n");
//        //    printf("                  |  Log(Likelihood) | Event Probability |   Top Pole Mass  |   Neutrino pz   |\n");
//        //    printf("Fitting Variables | %16.2f | %17.2E | %6.2f +- %6.2f | %6.2f +- %5.2f |\n",
//        //    LogLikelihood, EventProbability, top_pole_mass,ParErrors[KLFitter::LikelihoodTopLeptonLPhotonJets::parTopM], nu_pz,ParErrors[KLFitter::LikelihoodTopLeptonLPhotonJets::parNuPz]);
//        //    printf("----------------------------------------------------------------------------------------------\n");
//        //    printf("                  | Minuit Not Conv. | Fit Aborted: NaN  | >=1 Par at Limit | Invalid TF@Conv.|\n");
//        //    printf("Status Code       | %16i | %17i | %16i | %15i |\n",
//        //    bool((ConvergenceStatusBitWord & KLFitter::Fitter::MinuitDidNotConvergeMask) != 0),
//        //    bool((ConvergenceStatusBitWord & KLFitter::Fitter::FitAbortedDueToNaNMask) != 0),
//        //    bool((ConvergenceStatusBitWord & KLFitter::Fitter::AtLeastOneFitParameterAtItsLimitMask) != 0),
//        //    bool((ConvergenceStatusBitWord & KLFitter::Fitter::InvalidTransferFunctionAtConvergenceMask) != 0));
//        //}

bool SampleAnalyzor::PhMatch() {
    if (m_type == "Reco") {
    	if (m_phmatch_type == "") { // true photon
	    return true;
    	} else if (m_phmatch_type == "TruePh") { // true photon
    	    //if (event_photonorigin != 20 && event_photonorigin != 10) return true;
	    if (!((leadph_origin==23 || leadph_origin==24 || leadph_origin==25 || leadph_origin==26 || leadph_origin==27 || leadph_origin==28 || leadph_origin==29 || leadph_origin==30 || leadph_origin==31 || leadph_origin==32 || leadph_origin==33 || leadph_origin==34 || leadph_origin==35 || leadph_origin==42) && leadph_type == 16 ) && !( abs(leadph_mc_pid)==11 || ( leadph_mc_el_dr< m_efake_dr && leadph_mc_el_dr>=0 ))) return true;
	    else return false;
    	} else if (m_phmatch_type == "TruePhTest") { // true photon
    	    //if (event_photonorigin != 20 && event_photonorigin != 10) return true;
	    if (leadph_origin==12 || leadph_origin==13 || leadph_origin==3 || leadph_origin==14 || leadph_origin==15 || leadph_origin==37 || leadph_origin==38 || leadph_origin==40) return true;
	    else return false;
    	} else if (m_phmatch_type == "HFake") { // hadron fake
    	    //if (event_photonorigin == 10) return true;
	    if ((leadph_origin==23 || leadph_origin==24 || leadph_origin==25 || leadph_origin==26 || leadph_origin==27 || leadph_origin==28 || leadph_origin==29 || leadph_origin==30 || leadph_origin==31 || leadph_origin==32 || leadph_origin==33 || leadph_origin==34 || leadph_origin==35 || leadph_origin==42) && leadph_type == 16  && !( abs(leadph_mc_pid)==11 || ( leadph_mc_el_dr< m_efake_dr && leadph_mc_el_dr>=0 ))) return true;
	    else return false;
    	} else if (m_phmatch_type == "HFake38") { // hadron fake
    	    //if (event_photonorigin == 10) return true;
	    if (leadph_origin==38) return true;
	    else return false;
    	} else if (m_phmatch_type == "EFake") { // egamma fake
    	    //if (event_photonorigin == 20) return true;
	    if (abs(leadph_mc_pid)==11 || ( leadph_mc_el_dr< m_efake_dr && leadph_mc_el_dr>=0 )) return true;
	    else return false;
    	} else if (m_phmatch_type == "HorEFake") { // non-true photon
    	    if (event_photonorigin == 20 || event_photonorigin == 10) return true;
	    else return false;
    	} else if (m_phmatch_type == "EFakeTypeA") { //  (a)
    	    if (fabs(leadph_mc_pid) == 11) return true;
	    else return false;
    	} else if (m_phmatch_type == "EFakeTypeB") { //  (b)
    	    if (leadph_mc_pid == 22 && (leadph_mc_el_dr > 0 && leadph_mc_el_dr < m_efake_dr) && ((leadph_pt-leadph_mc_pt)/leadph_mc_pt) > 0.1) return true;
	    else return false;
    	} else if (m_phmatch_type == "EFakeTypeC") { //  (c)
    	    if (leadph_mc_pid == 22 && !(leadph_mc_el_dr > 0 && leadph_mc_el_dr < m_efake_dr)) return true;
	    else return false;
    	} else if (m_phmatch_type == "EFakeTypeD") { //  (d)
    	    if (leadph_mc_pid == 22 && (leadph_mc_el_dr > 0 && leadph_mc_el_dr < m_efake_dr) && ((leadph_pt-leadph_mc_pt)/leadph_mc_pt) < 0.1) return true;
	    else return false;
    	} else if (m_phmatch_type == "EFake2TypeA") { //  (a)
    	    if (fabs(leadph_mc_pid) == 11 && leadph_ancestor < 100) return true;
	    else return false;
    	} else if (m_phmatch_type == "EFake2TypeB") { //  (c)
    	    if (leadph_mc_pid == 22 && (leadph_mc_el_dr > 0 && leadph_mc_el_dr < m_efake_dr) && ((leadph_pt-leadph_mc_pt)/leadph_mc_pt) > 0.1 && leadph_ancestor < 100) return true;
	    else return false;
    	} else if (m_phmatch_type == "EFake2TypeC") { //  (c)
    	    if (leadph_mc_pid == 22 && !(leadph_mc_el_dr > 0 && leadph_mc_el_dr < m_efake_dr) && leadph_ancestor < 100) return true;
	    else return false;
    	} else if (m_phmatch_type == "EFake2TypeD") { //  (c)
    	    if (leadph_mc_pid == 22 && (leadph_mc_el_dr > 0 && leadph_mc_el_dr < m_efake_dr) && ((leadph_pt-leadph_mc_pt)/leadph_mc_pt) < 0.1 && leadph_ancestor < 100) return true;
	    else return false;
    	} else if (m_phmatch_type == "Ancestor1") { //  (ancestor1)
    	    if (leadph_ancestor == 1) return true;
	    else return false;
    	} else if (m_phmatch_type == "Ancestor2") { //  (ancestor2)
    	    if (leadph_ancestor == 2) return true;
	    else return false;
    	} else if (m_phmatch_type == "Ancestor3") { //  (ancestor3)
    	    if (leadph_ancestor == 3) return true;
	    else return false;
    	} else {
	    return false;
	}
    } else if (m_type == "Upgrade") {
    	if (m_phmatch_type == "TruePh") { //  (ancestor3)
	    if (leadph_faketype == 0) return true;
	    else return false;
    	} else if (m_phmatch_type == "EFake") { //  (ancestor3)
	    if (leadph_faketype == 1) return true;
	    else return false;
    	} else if (m_phmatch_type == "HFake") { //  (ancestor3)
	    if (leadph_faketype == 2) return true;
	    else return false;
	}
	return true;
    }
}

bool SampleAnalyzor::JetMatch() {
    bool tB_matched = false;
    bool tbB_matched = false;
    bool tW1_matched = false;
    bool tW2_matched = false;
    bool tbW1_matched = false;
    bool tbW2_matched = false;

    if (n_jet < 4) {
	m_lg->Err("Jet match should be done for events with jet >= 4!");
	exit(-1);
    } 

    for (int i = 0; i < 4; i++) {
	//if (jet_mcdr_tB->at(i) < 0.3) tB_matched = true;
	//if (jet_mcdr_tbB-at(i) < 0.3) tbB_matched = true;
	//if (jet_mcdr_tW1-at(i) < 0.3) tW1_matched = true;
	//if (jet_mcdr_tW2-at(i) < 0.3) tW2_matched = true;
	//if (jet_mcdr_tbW1-at(i) < 0.3) tbW1_matched = true;
	//if (jet_mcdr_tbW2-at(i) < 0.3) tbW2_matched = true;
    }

    if (tB_matched && tbB_matched && ((tW1_matched && tW2_matched && !tbW1_matched && !tbW2_matched) || (!tW1_matched && !tW2_matched && tbW1_matched && tbW2_matched))) return true;
    else return false;
}

string SampleAnalyzor::AppendInfo(string raw) {
    raw += "_"; raw += m_savekey; raw += m_variation;
    return raw;
}

bool SampleAnalyzor::isBHadron(int id) {
    int pdg = id;
    int q1=(abs(pdg)/1000)%10;
    int q2=(abs(pdg)/100)%10;
    int q3=(abs(pdg)/10)%10;
    if (q1 == 0 && q2 == 5 && q3 == 5)            return true;
    else if( q1 == 0 && q3<5 && q3>0 && q2 == 5 )  return true;
    else if( q1 == 5 )                             return true;
    else return false;
}

bool SampleAnalyzor::isHadron(int pdg) {
	int q1=(abs(pdg)/1000)%10;
	int q2=(abs(pdg)/100)%10;
	int q3=(abs(pdg)/10)%10;
	if (q1 == 0 && q2 == 5 && q3 == 5)            return true; 
	else if( q1 == 0 && q3<5 && q3>0 && q2 == 5 )  return true;
	else if( q1 == 0 && q3 == 4 && q2 == 4 )       return true;
	else if(q1==0&&q3<4&&q3>0&&q2==4)              return true;
	else if( q1 == 5 )                             return true;
	else if( q1 == 4 )                             return true;
	else if( q1 == 3 )                             return true;
	else if( q1 == 2 || q1 == 1 )                  return true;
	else if((q1==0&&q2==3&&q3<3&&q3>0)||abs(pdg)==130)  return true;
	else if( (q1==0&&(q3==1||q3==2) && (q2==1||q2==2)) || (q1==0&&q3==3&&q2==3)) return  true;
	else return false;
}
