#include "Logger.h"

using namespace std;

Logger::Logger(string name, int name_width, int mark_number, string mark_type) {
    m_name = name;
    m_name_width = name_width;
    m_mark_number = mark_number;
    m_mark_type = mark_type;
    
    m_shift = 0;
    m_shift_step = 2;
    m_simple_mode = false;
    
    m_cout_dire = "right";

    m_cnt_warn = 0;

    m_shutup = false;
};
    
Logger::~Logger(){
    Summary();
};

void Logger::Summary() {
    if (m_cnt_warn != 0) {
	Warn("sd", "number of WARNINGs -->", m_cnt_warn);
    }
};

void Logger::ShutUp() {
    m_shutup = true;
};
    
void Logger::SetModeSimple(bool simple) {
    m_simple_mode = simple;
};

void Logger::SetName(string name) {
    m_name = name;
};

void Logger::SetCoutDire(string dire) {
    if (dire == "LEFT" || dire == "left" || dire == "Left") {
        m_cout_dire = "left";
    } else if (dire == "RIGHT" || dire == "right" || dire == "Right") {
        m_cout_dire = "right";
    } else {
        Err("Unknow parameter for cout direction", dire.c_str());
    }
};

void Logger::SetShift(int shift) {
    m_shift = shift;
};

void Logger::SetShiftStep(int shift_step) {
    m_shift_step = shift_step;
};

void Logger::ResetShift() {
    m_shift = 0;
};

void Logger::AddShift() {
    m_shift += m_shift_step;
};

void Logger::ReduShift() {
    m_shift -= m_shift_step;
};

void Logger::PrintBase(string tag, int color) {
    if (m_cout_dire == "right") {
        cout << right;
    } else cout << left;

    cout << setw(m_name_width) << m_name << " ";

    for (int i = 0; i < m_mark_number; i++) {
        cout << m_mark_type;
    }

    char tmpchar[100];
    sprintf(tmpchar, "\033[1;%dm %s:\033[0m ", color, tag.c_str());
    cout << tmpchar;

    for (int i = 0; i < m_shift; i++) {
	cout << " ";
    }
}

void Logger::NewLine() {
    if (m_shutup) return;
    cout << endl;
}

void Logger::SetPrecision(int precision) {
    cout << setprecision(precision);
}

void Logger::Info() {
    if (m_shutup) return;
    if (!m_simple_mode) PrintBase("INFO", LOGGER::Green);
}

void Logger::Info(const char* fmt...) {
    if (m_shutup) return;
    std::cout.setf(std::ios::boolalpha);
    if (!m_simple_mode) PrintBase("INFO", LOGGER::Green);
    va_list args;
    va_start(args, fmt);

    while (*fmt != '\0') {
        if (*fmt == 'd') {
    	int i = va_arg(args, int);
    	cout << i << " ";
        } else if (*fmt == 'c') {
    	int c = va_arg(args, int);
    	cout << static_cast<char>(c) << " ";
        } else if (*fmt == 'f') {
    	double d = va_arg(args, double);
    	cout << d << " ";
        } else if (*fmt == 's') {
    	char* s = va_arg(args, char*);
    	cout << s << " ";
        }
        ++fmt;
    }

    va_end(args);
    cout << endl;
};

void Logger::InfoFixW(const char* fmt...) {
    if (m_shutup) return;
    std::cout.setf(std::ios::boolalpha);
    if (!m_simple_mode) PrintBase("INFO", LOGGER::Green);
    va_list args;
    va_start(args, fmt);

    int index = 0;
    int width = 0;
    while (*fmt != '\0') {
        if (*fmt == 'd') {
    	int i = va_arg(args, int);
    	if (index%2 == 0) width = i;
    	else cout << setw(width) << i << " ";
        } else if (*fmt == 'c') {
    	int c = va_arg(args, int);
    	cout << setw(width) << static_cast<char>(c) << " ";
        } else if (*fmt == 'f') {
    	double d = va_arg(args, double);
    	cout << setw(width) << d << " ";
        } else if (*fmt == 's') {
    	char* s = va_arg(args, char*);
    	cout << setw(width) << s << " ";
        }
        ++fmt;
        ++index;
    }

    va_end(args);
    cout << endl;
}

void Logger::Warn(const char* fmt...) {
    m_cnt_warn++;

    std::cout.setf(std::ios::boolalpha);
    if (!m_simple_mode) PrintBase("WARN", LOGGER::Purple);
    va_list args;
    va_start(args, fmt);

    while (*fmt != '\0') {
        if (*fmt == 'd') {
    	int i = va_arg(args, int);
    	cout << i << " ";
        } else if (*fmt == 'c') {
    	int c = va_arg(args, int);
    	cout << static_cast<char>(c) << " ";
        } else if (*fmt == 'f') {
    	double d = va_arg(args, double);
    	cout << d << " ";
        } else if (*fmt == 's') {
    	char* s = va_arg(args, char*);
    	cout << s << " ";
        }
        ++fmt;
    }

    va_end(args);
    cout << endl;
};

void Logger::Err(const char* fmt...) {
    std::cout.setf(std::ios::boolalpha);
    if (!m_simple_mode) PrintBase("ERROR", LOGGER::Red);
    va_list args;
    va_start(args, fmt);

    while (*fmt != '\0') {
        if (*fmt == 'd') {
    	int i = va_arg(args, int);
    	cout << i << " ";
        } else if (*fmt == 'c') {
    	int c = va_arg(args, int);
    	cout << static_cast<char>(c) << " ";
        } else if (*fmt == 'f') {
    	double d = va_arg(args, double);
    	cout << d << " ";
        } else if (*fmt == 's') {
    	char* s = va_arg(args, char*);
    	cout << s << " ";
        }
        ++fmt;
    }

    va_end(args);
    cout << endl;
};
