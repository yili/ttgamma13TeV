#include<TH1F.h>
#include "RooNLLVar.h"
#include <RooKeysPdf.h>
#include "RooRealSumPdf.h"
#include <RooFitResult.h>
#include <RooBernstein.h>
#include "RooTFnBinding.h" 
#include<TLatex.h>
#include<TLegend.h>
#include<TH2F.h>
#include<TString.h>
#include<TPaveStats.h>
#include<TFile.h>
#include<TROOT.h>
#include<TStyle.h>
#include<RooRealVar.h>
#include<RooPlot.h>
#include<TF1.h>
#include<TCanvas.h>
#include <RooFit.h>
#include <RooRealVar.h>
#include <RooGaussian.h>
#include <RooCBShape.h>
#include <RooWorkspace.h>
#include <RooDataSet.h>
#include "RooPolynomial.h"
#include "RooChebychev.h"
#include <RooRealVar.h>
#include <RooArgSet.h>
#include <RooWorkspace.h>
#include <RooDataHist.h>
#include <RooHist.h>
#include <RooHistPdf.h>
#include <RooProduct.h>
#include <RooCurve.h>
#include <RooAddPdf.h>
#include <RooAbsPdf.h>
#include <RooAddition.h>
#include <RooProdPdf.h>
#include <RooGaussModel.h>
#include <RooBinning.h>
#include <RooGenericPdf.h>
#include <RooExtendPdf.h>
#include <RooPoisson.h>
#include <RooLognormal.h>
#include <RooCategory.h>
#include <RooSimultaneous.h>

#ifndef __CINT__
#include "RooGlobalFunc.h"
#else
// Refer to a class implemented in libRooFit to force its loading
// via the autoloader.
class Roo2DKeysPdf;
#endif
#include "RooRealVar.h"
#include "RooDataSet.h"
#include "RooGaussian.h"
#include "TCanvas.h"
#include "TAxis.h"
#include "RooPlot.h"
#include "TMath.h"
#include "TF1.h"
#include "Math/DistFunc.h"
#ifndef __CINT__
#include "RooCFunction1Binding.h"
#include "RooCFunction3Binding.h"
#endif
#include "RooTFnBinding.h"


#include <RooStats/RooStatsUtils.h>
#include <RooStats/ModelConfig.h>
#include <RooStats/MCMCInterval.h>
#include <RooStats/MCMCIntervalPlot.h>

#include"Logger.h"
#include"PlotComparor.h"
#include "StringPlayer.h"
#include "Logger.h"
#include "AtlasStyle.h"
#include "ConfigReader.h"

using namespace std;
using namespace RooFit;
using namespace RooStats;

string inputdir = "results/Nominal/";
string tag = "Final03";
int precision = 2;

double fnc_dscb(double*xx,double*pp);
pair<double,double> FitMassPeak(TH1F*h, int ptbin, int etabin, TH1F* h_mc = NULL, TF1*tf_mc = NULL);
struct Results {
    double fr_data;
    double fr_data_err;
    double fr_MC;
    double fr_MC_err;
    double sf;
    double sf_err;
};
Results CalcuSF(int ptbin, int etabin);
double Try1PBkg (double val1, double fitrange_lo, double fitrange_hi, TH1F*h) ;
double Try2PBkg (double val1, double val2, double fitrange_lo, double fitrange_hi, TH1F*h) ;
double Try3PBkg (double val1, double val2, double val3, double fitrange_lo, double fitrange_hi, TH1F*h) ;
double Try4PBkg (double val1, double val2, double val3, double val4, double fitrange_lo, double fitrange_hi, TH1F*h) ;
double Try5PBkg (double val1, double val2, double val3, double val4, double val5, double fitrange_lo, double fitrange_hi, TH1F*h) ;

bool RangeDown = false;
bool GausBkg = false;
bool ShowChi2 = false;
bool MCTemp = false;
bool Smooth = false;
bool Auto = false;
bool SSCB = false;
bool NLOZy = false;
bool Cvt = false;
bool UnCvt = false;
bool Test = false;
bool Nominal = true;

int main(int argc, char * argv[]) {

    for (int i = 0; i < argc; i++) {
	if (!strcmp(argv[i],"--RDown")) {
	    RangeDown = true;
	    Nominal = false;
   	}
	if (!strcmp(argv[i],"--Gaus")) {
	    GausBkg = true;
	    Nominal = false;
   	}
	if (!strcmp(argv[i],"--Chi2")) {
	    ShowChi2 = true;
   	}
	if (!strcmp(argv[i],"--MCTemp")) {
	    MCTemp = true;
	    Nominal = false;
   	}
	if (!strcmp(argv[i],"--Smooth")) {
	    Smooth = true;
	    Nominal = false;
   	}
	if (!strcmp(argv[i],"--Auto")) {
	    Auto = true;
	    Nominal = false;
   	}
	if (!strcmp(argv[i],"--Zy")) {
	    NLOZy = true;
	    Nominal = false;
   	}
	if (!strcmp(argv[i],"--Cvt")) {
	    Cvt = true;
   	}
	if (!strcmp(argv[i],"--UnCvt")) {
	    UnCvt = true;
   	}
	if (!strcmp(argv[i],"--Test")) {
	    Test = true;
	    Nominal = false;
   	}
    }

    SetAtlasStyle();

    Results r_overall = CalcuSF(-1,-1);

    if (Test) return 0;

    TH2F* h_sf = new TH2F("FR_SF", "FR_SF", 6, 0, 6, 4, 0, 4);
    for (int i = 1; i <= 4; i++) {
        for (int j = 1; j <= 6; j++) {
            if (j == 4) continue;
            Results r = CalcuSF(i,j);
            h_sf->SetBinContent(j,i,r.sf); 
            h_sf->SetBinError(j,i,r.sf_err); 
        }
    }

    //Double_t bins_phpt[] = {20,25,30,35,40,45,50,55,60,65,70,99999};
    //int binnum_phpt = sizeof(bins_phpt)/sizeof(Double_t) - 1;
    //TH1F* h_sf_pt = new TH1F("FR_SF_Pt", "FR_SF_Pt", binnum_phpt, bins_phpt);
    //for (int i = 1; i <= h_sf_pt->GetNbinsX(); i++) {
    //    Results r = CalcuSF(i,-1);
    //    h_sf_pt->SetBinContent(i,r.sf); 
    //    h_sf_pt->SetBinError(i,r.sf_err); 
    //}
    //Double_t bins_pheta[] = {0,0.2,0.4,0.6,0.8,1.0,1.2,1.37,1.52,1.7,1.9,2.1,2.37};
    //int binnum_pheta = sizeof(bins_pheta)/sizeof(Double_t) - 1;
    //TH1F* h_sf_eta = new TH1F("FR_SF_Eta", "FR_SF_Eta", binnum_pheta, bins_pheta);
    //for (int i = 1; i <= h_sf_eta->GetNbinsX(); i++) {
    //    Results r = CalcuSF(-1, i);
    //    h_sf_eta->SetBinContent(i,r.sf); 
    //    h_sf_eta->SetBinError(i,r.sf_err); 
    //}

    cout << "Overall SF: " << r_overall.sf << " +/- " << r_overall.sf_err << endl;

    string savename = "results/Nominal/EFakeSFs";
    if (RangeDown) savename += "_RDown";
    if (GausBkg) savename += "_Gaus";
    if (SSCB) savename += "_SSCB";
    if (NLOZy) savename += "_Zy";
    if (MCTemp) savename += "_MCTemp";
    if (Smooth) savename += "_TKDE";
    if (Auto) savename += "_Auto";
    if (Cvt) savename += "_Cvt";
    if (UnCvt) savename += "_UnCvt";
    savename += ".root";

    cout << "Save pt-eta SFs to " << savename << endl;

    TFile *f = new TFile(savename.c_str(), "recreate");

    h_sf->Write();
    //h_sf_pt->Write();
    //h_sf_eta->Write();

    TH1F* h_overall = new TH1F("Overall_SF", "Oveall_SF", 1, 0, 1);
    h_overall->SetBinContent(1, r_overall.sf);
    h_overall->SetBinError(1, r_overall.sf_err);
    h_overall->Write();

    f->Close();
    delete f;

    return 0;
}

pair<double,double> FitMassPeak(TH1F*h, int ptbin, int etabin, TH1F* h_mc, TF1* tf_mc) {
    pair<double,double> results;
    bool isData = string(h->GetName()).find("Data",0) != string::npos;

    double nZ = 0;
    double eZ = 0;
    if (h->Integral() == 0) {
        cout << "Warn: hist integral to 0!" << endl;
    } else { 
	bool isZeg = false;
	if (string(h->GetName()).find("zeg",0) != string::npos) isZeg = true;
	else isZeg = false;

        double fitrange_lo = 60;
    	double fitrange_hi = 130;
	if (ptbin == 2) {
	    fitrange_lo = 70;
	} else if (ptbin >= 3) {
	    fitrange_lo = 80;
	}
	if (RangeDown) {
	    if (ptbin >= 3) {
		fitrange_lo += 5;
	    } else {
		fitrange_lo += 10;
	    } 
	    fitrange_hi -= 10;
	}

    	if (!isData) {
            int bin_lo = h->FindBin(fitrange_lo);
            int bin_hi = h->FindBin(fitrange_hi);
            
            for (int i = 1; i <= h->GetNbinsX(); i++) {
                if (i < bin_lo) continue;
                if (i >= bin_hi) continue;
                nZ += h->GetBinContent(i);
                eZ += pow(h->GetBinError(i),2);
            }
            eZ = sqrt(eZ);
    	} else {
	    if (MCTemp) {
		RooMsgService::instance().setSilentMode(kTRUE);
            	RooMsgService::instance().setGlobalKillBelow(ERROR);
            	gStyle->SetOptFit(1111);

	    	// build up model and fit
            	RooRealVar x("mass","mass",fitrange_lo,fitrange_hi) ;
            	x.setBins(fitrange_hi-fitrange_lo);
            	RooDataHist *data = new RooDataHist("data", "data", x, h);
	    	
    	    	RooAbsPdf *sig_pdf;
		RooAbsReal *dscb_bfunc = bindFunction(tf_mc, x);//deltaM and cw are parameters
    	    	RooRealVar dummy_a("dummy_a","dummy_a",0); dummy_a.setConstant(kTRUE);
    	    	RooChebychev dummy_bkg("dummy_bkg","dummy_bkg",x, RooArgList(dummy_a)); //this is now a constant
    	    	RooRealVar dummy_c("dummy_c","dummy_c",1); dummy_c.setConstant(kTRUE);
		RooDataHist* rdh_sig = new RooDataHist("rdhsig", "rdhsig", x, h_mc);
		if (Auto) {
		    if ((ptbin == -1 && isZeg) 
		        || (ptbin == 1 && isZeg)
		        || (ptbin == 2 && (etabin <= 4) && isZeg)
		        || (ptbin == 4)
		       )
		    {
		        sig_pdf = new RooRealSumPdf("dscb","dscb",*dscb_bfunc,dummy_bkg,dummy_c); //combine the constant term (bkg) and the term RooAbsReal (which is a function) into a PDF.
		    } else {
		        sig_pdf = new RooHistPdf("rhpsig", "rhpsig", x, *rdh_sig);
		    }
		} else if (Smooth) {
		    sig_pdf = new RooRealSumPdf("dscb","dscb",*dscb_bfunc,dummy_bkg,dummy_c); //combine the constant term (bkg) and the term RooAbsReal (which is a function) into a PDF.
		} else {
    	    	    sig_pdf = new RooHistPdf("rhpsig", "rhpsig", x, *rdh_sig);
		}
    	    	RooRealVar *nsig = new RooRealVar("nsig", "nsig", h->Integral()*0.9, 0, 2*h->Integral());
    	    	RooExtendPdf *sig = new RooExtendPdf("sig", "sig", *sig_pdf, *nsig);

		RooRealVar *pa0 = new RooRealVar("pa0", "pa0", 100, 0, 1000); 
		RooRealVar *pa1 = new RooRealVar("pa1", "pa1", 10, 0, 100); 
		RooRealVar *pa2 = new RooRealVar("pa2", "pa2", 10, 0, 100); 
		RooRealVar *pa3 = new RooRealVar("pa3", "pa3", 10, 0, 100); 
		RooAbsPdf *poly = new RooBernstein("poly", "poly", x, RooArgList(*pa0,*pa1,*pa2,*pa3));
		RooRealVar *nbkg = new RooRealVar("nbkg", "nbkg", h->Integral()*0.1, 0, h->Integral());
		RooExtendPdf *bkg = new RooExtendPdf("bkg", "bkg", *poly, *nbkg);
		
		RooAddPdf *model = new RooAddPdf("model", "model", RooArgList(*sig, *bkg));
		model->fitTo(*data, Extended(kTRUE)); //RooFitResult *results = model->fitTo(*data, RooFit::Save(),PrintEvalErrors(0),EvalErrorWall(kFALSE),Extended(kTRUE));
		RooFitResult *results = model->fitTo(*data, RooFit::Save(),Extended(kTRUE)); //RooFitResult *results = model->fitTo(*data, RooFit::Save(),PrintEvalErrors(0),EvalErrorWall(kFALSE),Extended(kTRUE));
		cout << "Fit results status: " << results->status() << endl;
		cout << "Nsig: " << nsig->getVal() << " " << nsig->getError() << endl;
		cout << "Ndata: " << h->Integral() << " " << sqrt(h->Integral()) << endl;
		cout << "Nbkg: " << nbkg->getVal() << " " << nbkg->getError() << endl;
		
		nZ = nsig->getVal();
		eZ = nsig->getError();
		
		// draw
		RooPlot* xframe = x.frame(Title("example")) ;
		data->plotOn(xframe,Name("data")) ;
		if (!ShowChi2) model->paramOn(xframe,Parameters(RooArgSet(*pa0,*pa1,*pa2,*pa3)),Layout(0.65,0.95),Format("NEU", AutoPrecision(1)));
		model->plotOn(xframe) ;
		Double_t chi2 = xframe->chiSquare();
		model->plotOn(xframe,Name("signal"),Components(*sig),LineColor(kRed)) ;
		model->plotOn(xframe,Name("bkg"),Components(*bkg),LineStyle(kDashed)) ;
		model->plotOn(xframe,Name("model"),Components(RooArgSet(*bkg,*sig))) ;
		
		TCanvas *tmpc = new TCanvas("tmp", "tmp", 800, 800);
		TPad* pad1 =  new TPad("pad1","pad1name",0.,0.20,1.,1.);   
		TPad* pad2 =  new TPad("pad2","pad1name",0.,0.05,1.,0.32);    
		pad1->Draw();
		pad1->SetRightMargin(0.05);
		pad2->Draw();
		pad2->SetGridy();
		pad2->SetRightMargin(0.05);
		pad2->SetBottomMargin(0.3);
		pad1->cd();
		pad1->SetLogy(); 
		
		double min = 9999999;
		double max = 0;
		for (int i = 1; i <= h->GetNbinsX(); i++) {
		    if (h->GetBinCenter(i) >= fitrange_lo && h->GetBinCenter(i) <= fitrange_hi) {
		        if (min > h->GetBinContent(i)) min = h->GetBinContent(i);
		        if (max < h->GetBinContent(i)) max = h->GetBinContent(i);
		    }
		}
		if (min <= 0) min = 1;
		xframe->GetYaxis()->SetRangeUser(min*0.5, max*12);
		xframe->GetYaxis()->SetTitleOffset(1) ; 
		xframe->Draw() ;
		
		// post-fit results
		double norm_sig = nsig->getVal();
		double norm_bkg = nbkg->getVal();
		RooAbsReal* int_sig = sig->createIntegral(x,NormSet(x),Range("cut"));
		
		int bin_lo = h->FindBin(fitrange_lo);
		int bin_hi = h->FindBin(fitrange_hi);
		int nbin = bin_hi - bin_lo;
		TH1F* h_data = new TH1F("h_data","h_data",nbin,fitrange_lo,fitrange_hi);
		TH1F* h_postfit_model = new TH1F("Postfit_model","Postfit_model",nbin,fitrange_lo,fitrange_hi);
		TH1F* h_postfit_sig = new TH1F("Postfit_sig","Postfit_sig",nbin,fitrange_lo,fitrange_hi);
		TH1F* h_postfit_bkg = new TH1F("Postfit_bkg","Postfit_bkg",nbin,fitrange_lo,fitrange_hi);
		for (int i = 1; i <= nbin; i++) {
		    x.setRange("tmprange",h_postfit_sig->GetBinLowEdge(i),h_postfit_sig->GetBinLowEdge(i)+h_postfit_sig->GetBinWidth(i));
		    RooAbsReal* int_sig = sig->createIntegral(x,NormSet(x),Range("tmprange"));
		    RooAbsReal* int_bkg = bkg->createIntegral(x,NormSet(x),Range("tmprange"));
		    double bin_cnt_sig = norm_sig * int_sig->getVal();
		    double bin_cnt_bkg = norm_bkg * int_bkg->getVal();
		    //double bin_err_sig = norm_sig * int_sig->getError();
		    //double bin_err_bkg = norm_bkg * int_bkg->getError();
		    h_data->SetBinContent(i, h->GetBinContent(h->FindBin(h_data->GetBinCenter(i))));
		    h_data->SetBinError(i, h->GetBinError(h->FindBin(h_data->GetBinCenter(i))));
		    h_postfit_model->SetBinContent(i, bin_cnt_sig+bin_cnt_bkg);
		    h_postfit_sig->SetBinContent(i, bin_cnt_sig);
		    h_postfit_bkg->SetBinContent(i, bin_cnt_bkg);
		}
		
		string region;
		char tmp_ch[100];
		if (isZeg) region = "Zeg"; else region = "Zee";
		if (ptbin >= 1 && etabin <= 0) {
		    sprintf(tmp_ch, "/PtBin%d", ptbin);
		    region += tmp_ch;
		} else if (ptbin <= 0 && etabin >= 1) {
		    sprintf(tmp_ch, "/EtaBin%d", etabin);
		    region += tmp_ch;
		} else if (ptbin >= 1 && etabin >= 1) {
		    sprintf(tmp_ch, "/PtBin%dEtaBin%d", ptbin, etabin);
		    region += tmp_ch;
		}
		xframe->SetTitle(region.c_str());
		xframe->GetXaxis()->SetTitle("Invariant mass");
		
		if (ShowChi2) {
		    TLegend *lg = new TLegend(0.7,0.67,0.9,0.9);
		    lg->SetBorderSize(0);
		    lg->SetFillColor(0);
		    lg->AddEntry("data", "Data", "P");
		    lg->AddEntry("model", "Sig+Bkg", "l");
		    lg->AddEntry("signal", "Sig", "l");
		    lg->AddEntry("bkg", "Bkg", "l");
		    lg->Draw("same");
		}
		    
		TLatex lt;
		lt.SetNDC();
		lt.SetTextFont(42);
		    
		if (ShowChi2) {
		    sprintf(tmp_ch, "chi2/dof=%.1f", chi2);
		    lt.DrawLatex(0.7, 0.61, tmp_ch);
		}
		    
		double m_lt_x = 0.2;
		double lt_y = 0.86;
		lt.SetTextFont(72);
		lt.DrawLatex(m_lt_x, lt_y, "ATLAS");
		lt.SetTextFont(42);
		double delx = 0.115*696*tmpc->GetWh()/(472*tmpc->GetWw());
		lt.DrawLatex(m_lt_x+delx, lt_y, "Internal");
		double ystep = -0.08;
		lt_y += ystep;
		lt.DrawLatex(m_lt_x, lt_y, "#sqrt{s}=13TeV, 36.1 fb^{-1}");
		//lt_y += ystep;
		//lt.DrawLatex(m_lt_x, lt_y, "#intL dt = 36.1 fb^{-1}");
		lt_y += ystep;
		lt.DrawLatex(m_lt_x, lt_y, region.c_str());
		
		pad2->cd();
		TH1F* h_data_model = (TH1F*)h_data->Clone();
		h_data_model->SetName("h_data_model");
		h_data_model->Divide(h_postfit_model);
		
		Double_t enlarge = 3;
		h_data_model->GetXaxis()->SetLabelSize(0.05*enlarge);
		h_data_model->GetXaxis()->SetTitleSize(0.05*enlarge);
		h_data_model->GetXaxis()->SetTitleOffset(1);
		h_data_model->GetYaxis()->SetLabelSize(0.05*enlarge);
		h_data_model->GetXaxis()->SetTitle("Invariant mass [GeV]");
		h_data_model->GetYaxis()->SetTitle("Data/Model");
		h_data_model->GetYaxis()->SetTitleSize(0.05*enlarge);
		h_data_model->GetYaxis()->SetTitleOffset(1/enlarge);
		h_data_model->GetYaxis()->SetNdivisions(505);
		h_data_model->SetMaximum(1.5);
		h_data_model->SetMinimum(0.5);
		
		h_data_model->Draw();
		
		string savename = "plots/EFake/Postfit_Zpeak"; 
		if (ptbin >= 1 && etabin <= 0) {
		    sprintf(tmp_ch, "_PtBin%d", ptbin);
		    savename += tmp_ch;
		} else if (ptbin <= 0 && etabin >= 1) {
		    sprintf(tmp_ch, "_EtaBin%d", etabin);
		    savename += tmp_ch;
		} else if (ptbin >= 1 && etabin >= 1) {
		    sprintf(tmp_ch, "_PtBin%d_EtaBin%d", ptbin, etabin);
		    savename += tmp_ch;
		}
		if (isZeg) savename += "_zeg";
		else savename += "_zee";
		
		savename += "_MCTemp";
		if (Smooth) savename += "_TKDE";
		if (Auto) savename += "_Auto";
		if (Cvt) savename += "_Cvt";
		if (UnCvt) savename += "_UnCvt";
		
		tmpc->SaveAs(string(savename + string(".pdf")).c_str());
		tmpc->SaveAs(string(savename + string(".png")).c_str());
		
		delete tmpc;
		delete h_data;
		delete h_postfit_model;
		delete h_postfit_sig;
		delete h_postfit_bkg;
		delete model;
		delete nsig;
		delete sig;
		delete nbkg;
		delete bkg;
	    } else if (SSCB) {
		RooMsgService::instance().setSilentMode(kTRUE);
            	RooMsgService::instance().setGlobalKillBelow(ERROR);
            	gStyle->SetOptFit(1111);

	    	// build up model and fit
            	RooRealVar x("mass","mass",fitrange_lo,fitrange_hi) ;
            	x.setBins(fitrange_hi-fitrange_lo);
            	RooDataHist *data = new RooDataHist("data", "data", x, h);

		RooRealVar *m = new RooRealVar("m","m",90,80,100) ;
    	    	RooRealVar *s = new RooRealVar("s","s",3,1,20) ;
		RooRealVar *a;
    	    	if (ptbin >= 2) {
    	    	    a = new RooRealVar("a","a",-1.,-20.,0.);
    	    	} else {
    	    	    a = new RooRealVar("a","a",1.,0.01,20.);
    	    	}
    	    	RooRealVar *n = new RooRealVar("n","n",2,0.01,1000) ;
		RooCBShape* cb = new RooCBShape("cb", "Crystal Ball", x, *m, *s, *a ,*n);
    	    	RooRealVar *nsig = new RooRealVar("nsig", "nsig", h->Integral()*0.9, 0, 2*h->Integral());
    	    	RooExtendPdf *sig = new RooExtendPdf("sig", "sig", *cb, *nsig);

		RooRealVar *pa0 = new RooRealVar("pa0", "pa0", 0.01, 0, 100); 
		RooRealVar *pa1 = new RooRealVar("pa1", "pa1", 0.01, 0, 100); 
		RooRealVar *pa2 = new RooRealVar("pa2", "pa2", 0.01, 0, 100); 
		RooRealVar *pa3 = new RooRealVar("pa3", "pa3", 0.01, 0, 100); 
		RooRealVar *pa4 = new RooRealVar("pa4", "pa4", 0.01, 0, 100); 
		RooAbsPdf *poly = new RooBernstein("poly", "poly", x, RooArgList(*pa0,*pa1,*pa2,*pa3));
		RooRealVar *nbkg = new RooRealVar("nbkg", "nbkg", h->Integral()*0.1, 0, h->Integral());
		RooExtendPdf *bkg = new RooExtendPdf("bkg", "bkg", *poly, *nbkg);

		RooAddPdf *model = new RooAddPdf("model", "model", RooArgList(*sig, *bkg));
		model->fitTo(*data, Extended(kTRUE)); //RooFitResult *results = model->fitTo(*data, RooFit::Save(),PrintEvalErrors(0),EvalErrorWall(kFALSE),Extended(kTRUE));
		RooFitResult *results = model->fitTo(*data, RooFit::Save(),Extended(kTRUE)); //RooFitResult *results = model->fitTo(*data, RooFit::Save(),PrintEvalErrors(0),EvalErrorWall(kFALSE),Extended(kTRUE));
		cout << "Fit results status: " << results->status() << endl;
		cout << "Nsig: " << nsig->getVal() << " " << nsig->getError() << endl;
		cout << "Ndata: " << h->Integral() << " " << sqrt(h->Integral()) << endl;
		cout << "Nbkg: " << nbkg->getVal() << " " << nbkg->getError() << endl;
		
		nZ = nsig->getVal();
		eZ = nsig->getError();
		
		// draw
		RooPlot* xframe = x.frame(Title("example")) ;
		data->plotOn(xframe,Name("data")) ;
		if (!ShowChi2) model->paramOn(xframe,Parameters(RooArgSet(*s,*a,*n,*pa0,*pa1,*pa2,*pa3)),Layout(0.65,0.95),Format("NEU", AutoPrecision(1)));
		model->plotOn(xframe) ;
		Double_t chi2 = xframe->chiSquare();
		model->plotOn(xframe,Name("signal"),Components(*sig),LineColor(kRed)) ;
		model->plotOn(xframe,Name("bkg"),Components(*bkg),LineStyle(kDashed)) ;
		model->plotOn(xframe,Name("model"),Components(RooArgSet(*bkg,*sig))) ;
		
		TCanvas *tmpc = new TCanvas("tmp", "tmp", 800, 800);
		TPad* pad1 =  new TPad("pad1","pad1name",0.,0.20,1.,1.);   
		TPad* pad2 =  new TPad("pad2","pad1name",0.,0.05,1.,0.32);    
		pad1->Draw();
		pad1->SetRightMargin(0.05);
		pad2->Draw();
		pad2->SetGridy();
		pad2->SetRightMargin(0.05);
		pad2->SetBottomMargin(0.3);
		pad1->cd();
		pad1->SetLogy(); 
		
		double min = 9999999;
		double max = 0;
		for (int i = 1; i <= h->GetNbinsX(); i++) {
		    if (h->GetBinCenter(i) >= fitrange_lo && h->GetBinCenter(i) <= fitrange_hi) {
		        if (min > h->GetBinContent(i)) min = h->GetBinContent(i);
		        if (max < h->GetBinContent(i)) max = h->GetBinContent(i);
		    }
		}
		if (min <= 0) min = 1;
		xframe->GetYaxis()->SetRangeUser(min*0.5, max*12);
		xframe->GetYaxis()->SetTitleOffset(1) ; 
		xframe->Draw() ;
		
		// post-fit results
		double norm_sig = nsig->getVal();
		double norm_bkg = nbkg->getVal();
		RooAbsReal* int_sig = sig->createIntegral(x,NormSet(x),Range("cut"));
		
		int bin_lo = h->FindBin(fitrange_lo);
		int bin_hi = h->FindBin(fitrange_hi);
		int nbin = bin_hi - bin_lo;
		TH1F* h_data = new TH1F("h_data","h_data",nbin,fitrange_lo,fitrange_hi);
		TH1F* h_postfit_model = new TH1F("Postfit_model","Postfit_model",nbin,fitrange_lo,fitrange_hi);
		TH1F* h_postfit_sig = new TH1F("Postfit_sig","Postfit_sig",nbin,fitrange_lo,fitrange_hi);
		TH1F* h_postfit_bkg = new TH1F("Postfit_bkg","Postfit_bkg",nbin,fitrange_lo,fitrange_hi);
		for (int i = 1; i <= nbin; i++) {
		    x.setRange("tmprange",h_postfit_sig->GetBinLowEdge(i),h_postfit_sig->GetBinLowEdge(i)+h_postfit_sig->GetBinWidth(i));
		    RooAbsReal* int_sig = sig->createIntegral(x,NormSet(x),Range("tmprange"));
		    RooAbsReal* int_bkg = bkg->createIntegral(x,NormSet(x),Range("tmprange"));
		    double bin_cnt_sig = norm_sig * int_sig->getVal();
		    double bin_cnt_bkg = norm_bkg * int_bkg->getVal();
		    //double bin_err_sig = norm_sig * int_sig->getError();
		    //double bin_err_bkg = norm_bkg * int_bkg->getError();
		    h_data->SetBinContent(i, h->GetBinContent(h->FindBin(h_data->GetBinCenter(i))));
		    h_data->SetBinError(i, h->GetBinError(h->FindBin(h_data->GetBinCenter(i))));
		    h_postfit_model->SetBinContent(i, bin_cnt_sig+bin_cnt_bkg);
		    h_postfit_sig->SetBinContent(i, bin_cnt_sig);
		    h_postfit_bkg->SetBinContent(i, bin_cnt_bkg);
		}
		
		string region;
		char tmp_ch[100];
		if (isZeg) region = "Zeg"; else region = "Zee";
		if (ptbin >= 1 && etabin <= 0) {
		    sprintf(tmp_ch, "/PtBin%d", ptbin);
		    region += tmp_ch;
		} else if (ptbin <= 0 && etabin >= 1) {
		    sprintf(tmp_ch, "/EtaBin%d", etabin);
		    region += tmp_ch;
		} else if (ptbin >= 1 && etabin >= 1) {
		    sprintf(tmp_ch, "/PtBin%dEtaBin%d", ptbin, etabin);
		    region += tmp_ch;
		}
		xframe->SetTitle(region.c_str());
		xframe->GetXaxis()->SetTitle("Invariant mass");
		
		if (ShowChi2) {
		    TLegend *lg = new TLegend(0.7,0.67,0.9,0.9);
		    lg->SetBorderSize(0);
		    lg->SetFillColor(0);
		    lg->AddEntry("data", "Data", "P");
		    lg->AddEntry("model", "Sig+Bkg", "l");
		    lg->AddEntry("signal", "Sig", "l");
		    lg->AddEntry("bkg", "Bkg", "l");
		    lg->Draw("same");
		}
		    
		TLatex lt;
		lt.SetNDC();
		lt.SetTextFont(42);
		    
		if (ShowChi2) {
		    sprintf(tmp_ch, "chi2/dof=%.1f", chi2);
		    lt.DrawLatex(0.7, 0.61, tmp_ch);
		}
		    
		double m_lt_x = 0.2;
		double lt_y = 0.86;
		lt.SetTextFont(72);
		lt.DrawLatex(m_lt_x, lt_y, "ATLAS");
		lt.SetTextFont(42);
		double delx = 0.115*696*tmpc->GetWh()/(472*tmpc->GetWw());
		lt.DrawLatex(m_lt_x+delx, lt_y, "Internal");
		double ystep = -0.08;
		lt_y += ystep;
		lt.DrawLatex(m_lt_x, lt_y, "#sqrt{s}=13TeV, 36.1 fb^{-1}");
		//lt_y += ystep;
		//lt.DrawLatex(m_lt_x, lt_y, "#intL dt = 36.1 fb^{-1}");
		lt_y += ystep;
		lt.DrawLatex(m_lt_x, lt_y, region.c_str());
		
		pad2->cd();
		TH1F* h_data_model = (TH1F*)h_data->Clone();
		h_data_model->SetName("h_data_model");
		h_data_model->Divide(h_postfit_model);
		
		Double_t enlarge = 3;
		h_data_model->GetXaxis()->SetLabelSize(0.05*enlarge);
		h_data_model->GetXaxis()->SetTitleSize(0.05*enlarge);
		h_data_model->GetXaxis()->SetTitleOffset(1);
		h_data_model->GetYaxis()->SetLabelSize(0.05*enlarge);
		h_data_model->GetXaxis()->SetTitle("Invariant mass [GeV]");
		h_data_model->GetYaxis()->SetTitle("Data/Model");
		h_data_model->GetYaxis()->SetTitleSize(0.05*enlarge);
		h_data_model->GetYaxis()->SetTitleOffset(1/enlarge);
		h_data_model->GetYaxis()->SetNdivisions(505);
		h_data_model->SetMaximum(1.5);
		h_data_model->SetMinimum(0.5);
		
		h_data_model->Draw();
		
		string savename = "plots/EFake/Postfit_Zpeak"; 
		if (ptbin >= 1 && etabin <= 0) {
		    sprintf(tmp_ch, "_PtBin%d", ptbin);
		    savename += tmp_ch;
		} else if (ptbin <= 0 && etabin >= 1) {
		    sprintf(tmp_ch, "_EtaBin%d", etabin);
		    savename += tmp_ch;
		} else if (ptbin >= 1 && etabin >= 1) {
		    sprintf(tmp_ch, "_PtBin%d_EtaBin%d", ptbin, etabin);
		    savename += tmp_ch;
		}
		if (isZeg) savename += "_zeg";
		else savename += "_zee";
		
		savename += "_SSCB";

		if (Cvt) savename += "_Cvt";
		if (UnCvt) savename += "_UnCvt";
		
		tmpc->SaveAs(string(savename + string(".pdf")).c_str());
		tmpc->SaveAs(string(savename + string(".png")).c_str());
		
		delete tmpc;
		delete h_data;
		delete h_postfit_model;
		delete h_postfit_sig;
		delete h_postfit_bkg;
		delete model;
		delete nsig;
		delete sig;
		delete nbkg;
		delete bkg;
	    } else {
		RooMsgService::instance().setSilentMode(kTRUE);
            	RooMsgService::instance().setGlobalKillBelow(ERROR);
            	gStyle->SetOptFit(1111);

	    	// build up model and fit
            	RooRealVar x("mass","mass",fitrange_lo,fitrange_hi) ;
            	x.setBins(fitrange_hi-fitrange_lo);
            	RooDataHist *data = new RooDataHist("data", "data", x, h);
	    	
	    	TF1 *dscb_tf1 = new TF1("dscb_tf1", fnc_dscb, fitrange_lo,fitrange_hi,6);
    	    	dscb_tf1->SetParameters(90,3,1,1,1,1);
    	    	RooRealVar *m = new RooRealVar("m","m",90,80,100) ;
    	    	RooRealVar *s = new RooRealVar("s","s",3,0,10) ;
    	    	RooRealVar *a1 = new RooRealVar("a1","a1",1.5,1,5.);
    	    	RooRealVar *p1 = new RooRealVar("p1","p1",4,3,50.);
    	    	RooRealVar *a2 = new RooRealVar("a2","a2",1.5,1,2.);
    	    	RooRealVar *p2 = new RooRealVar("p2","p2",4,3,50.);
    	    	RooAbsReal *dscb_bfunc = bindFunction(dscb_tf1, x, RooArgList(*m,*s,*a1,*p1,*a2,*p2));//deltaM and cw are parameters
    	    	RooRealVar dummy_a("dummy_a","dummy_a",0); dummy_a.setConstant(kTRUE);
    	    	RooChebychev dummy_bkg("dummy_bkg","dummy_bkg",x, RooArgList(dummy_a)); //this is now a constant
    	    	RooRealVar dummy_c("dummy_c","dummy_c",1); dummy_c.setConstant(kTRUE);
    	    	RooAbsPdf *dscb = new RooRealSumPdf("dscb","dscb",*dscb_bfunc,dummy_bkg,dummy_c); //combine the constant term (bkg) and the term RooAbsReal (which is a function) into a PDF.
    	    	RooRealVar *nsig = new RooRealVar("nsig", "nsig", h->Integral()*0.9, 0, 2*h->Integral());
    	    	RooExtendPdf *sig = new RooExtendPdf("sig", "sig", *dscb, *nsig);

	    	if (!GausBkg) {
	    	    RooRealVar *pa0 = new RooRealVar("pa0", "pa0", 10, 0, 100); 
    	    		RooRealVar *pa1 = new RooRealVar("pa1", "pa1", 10, 0, 100); 
    	    		RooRealVar *pa2 = new RooRealVar("pa2", "pa2", 10, 0, 100); 
    	    		RooRealVar *pa3 = new RooRealVar("pa3", "pa3", 10, 0, 100); 
	    		RooAbsPdf *poly = new RooBernstein("poly", "poly", x, RooArgList(*pa0,*pa1,*pa2,*pa3));
    	    		RooRealVar *nbkg = new RooRealVar("nbkg", "nbkg", h->Integral()*0.1, 0, h->Integral());
	    		RooExtendPdf *bkg = new RooExtendPdf("bkg", "bkg", *poly, *nbkg);

	    		if (RangeDown && ptbin == -1 && etabin == -1 && !isZeg) {
	    		    p1->setVal(20);
	    		    p2->setVal(40);
	    		    pa0->setVal(0.1);
	    		    pa1->setVal(0.1);
	    		    pa2->setVal(0.1);
	    		    pa3->setVal(0.1);
	    		}
	    		if (RangeDown && ptbin == 4 && etabin == 6 && !isZeg) {
	    		    p1->setVal(20);
	    		    p2->setVal(40);
	    		    pa0->setVal(0.1);
	    		    pa1->setVal(0.1);
	    		    pa2->setVal(0.1);
	    		    pa3->setVal(0.1);
	    		}
	    		if (ptbin == 2 && etabin == 2) {
	    		    p1->setVal(20);
	    		    p2->setVal(40);
	    		    pa0->setVal(0.1);
	    		    pa1->setVal(0.1);
	    		    pa2->setVal(0.1);
	    		    pa3->setVal(0.1);
	    		}
	    		if (ptbin == 2 && etabin == 3) {
	    		    p1->setVal(50);
	    		    p2->setVal(5);
	    		    pa0->setVal(0.1);
	    		    pa1->setVal(100);
	    		    pa2->setVal(50);
	    		    pa3->setVal(10);
	    		}
	    		if (ptbin == 2 && etabin == 6) {
	    		    p1->setVal(50);
	    		    p2->setVal(5);
	    		    pa0->setVal(0.1);
	    		    pa1->setVal(100);
	    		    pa2->setVal(50);
	    		    pa3->setVal(10);
	    		}
	    		if (ptbin == 4 && etabin == 2 && isZeg) {
	    		    p1->setVal(20);
	    		    p2->setVal(40);
	    		    pa0->setVal(0.1);
	    		    pa1->setVal(0.1);
	    		    pa2->setVal(0.1);
	    		    pa3->setVal(0.1);
	    		}
	    		
            		RooAddPdf *model = new RooAddPdf("model", "model", RooArgList(*sig, *bkg));
            		model->fitTo(*data, Extended(kTRUE)); //RooFitResult *results = model->fitTo(*data, RooFit::Save(),PrintEvalErrors(0),EvalErrorWall(kFALSE),Extended(kTRUE));
	    		if (ptbin == 4 && etabin == 2 && isZeg) {
	    		    model->fitTo(*data, Extended(kTRUE)); //RooFitResult *results = model->fitTo(*data, RooFit::Save(),PrintEvalErrors(0),EvalErrorWall(kFALSE),Extended(kTRUE));
	    		}
            		RooFitResult *results = model->fitTo(*data, RooFit::Save(),Extended(kTRUE)); //RooFitResult *results = model->fitTo(*data, RooFit::Save(),PrintEvalErrors(0),EvalErrorWall(kFALSE),Extended(kTRUE));
	    		cout << "Fit results status: " << results->status() << endl;
		cout << "Nsig: " << nsig->getVal() << " " << nsig->getError() << endl;
		cout << "Ndata: " << h->Integral() << " " << sqrt(h->Integral()) << endl;
		cout << "Nbkg: " << nbkg->getVal() << " " << nbkg->getError() << endl;

            		nZ = nsig->getVal();
            		eZ = nsig->getError();
    
            		// draw
            		RooPlot* xframe = x.frame(Title("example")) ;
            		data->plotOn(xframe,Name("data")) ;
	    		if (!ShowChi2) model->paramOn(xframe,Parameters(RooArgSet(*p1,*p2,*s,*a1,*a2,*pa0,*pa1,*pa2,*pa3)),Layout(0.65,0.95),Format("NEU", AutoPrecision(1)));
            		model->plotOn(xframe) ;
            		Double_t chi2 = xframe->chiSquare();
            		model->plotOn(xframe,Name("signal"),Components(*sig),LineColor(kRed)) ;
            		model->plotOn(xframe,Name("bkg"),Components(*bkg),LineStyle(kDashed)) ;
            		model->plotOn(xframe,Name("model"),Components(RooArgSet(*bkg,*sig))) ;
	    		
            		TCanvas *tmpc = new TCanvas("tmp", "tmp", 800, 800);
	    		TPad* pad1 =  new TPad("pad1","pad1name",0.,0.20,1.,1.);   
	    		TPad* pad2 =  new TPad("pad2","pad1name",0.,0.05,1.,0.32);    
	    		pad1->Draw();
	    		pad1->SetRightMargin(0.05);
    	    		pad2->Draw();
    	    		pad2->SetGridy();
	    		pad2->SetRightMargin(0.05);
	    		pad2->SetBottomMargin(0.3);
	    		pad1->cd();
	    		pad1->SetLogy(); 
	    		
	    		double min = 9999999;
	    		double max = 0;
	    		for (int i = 1; i <= h->GetNbinsX(); i++) {
	    		    if (h->GetBinCenter(i) >= fitrange_lo && h->GetBinCenter(i) <= fitrange_hi) {
	    		        if (min > h->GetBinContent(i)) min = h->GetBinContent(i);
	    		        if (max < h->GetBinContent(i)) max = h->GetBinContent(i);
	    		    }
	    		}
	    		if (min <= 0) min = 1;
	    		xframe->GetYaxis()->SetRangeUser(min*0.5, max*12);
	    		xframe->GetYaxis()->SetTitleOffset(1) ; 
	    		xframe->Draw() ;
	    		
	    		// post-fit results
	    		double norm_sig = nsig->getVal();
	    		double norm_bkg = nbkg->getVal();
	    		RooAbsReal* int_sig = sig->createIntegral(x,NormSet(x),Range("cut"));
	    		
	    		int bin_lo = h->FindBin(fitrange_lo);
	    		int bin_hi = h->FindBin(fitrange_hi);
	    		int nbin = bin_hi - bin_lo;
	    		TH1F* h_data = new TH1F("h_data","h_data",nbin,fitrange_lo,fitrange_hi);
	    		TH1F* h_postfit_model = new TH1F("Postfit_model","Postfit_model",nbin,fitrange_lo,fitrange_hi);
	    		TH1F* h_postfit_sig = new TH1F("Postfit_sig","Postfit_sig",nbin,fitrange_lo,fitrange_hi);
	    		TH1F* h_postfit_bkg = new TH1F("Postfit_bkg","Postfit_bkg",nbin,fitrange_lo,fitrange_hi);
	    		for (int i = 1; i <= nbin; i++) {
	    		    x.setRange("tmprange",h_postfit_sig->GetBinLowEdge(i),h_postfit_sig->GetBinLowEdge(i)+h_postfit_sig->GetBinWidth(i));
	    		    RooAbsReal* int_sig = sig->createIntegral(x,NormSet(x),Range("tmprange"));
	    		    RooAbsReal* int_bkg = bkg->createIntegral(x,NormSet(x),Range("tmprange"));
	    		    double bin_cnt_sig = norm_sig * int_sig->getVal();
	    		    double bin_cnt_bkg = norm_bkg * int_bkg->getVal();
	    		    //double bin_err_sig = norm_sig * int_sig->getError();
	    		    //double bin_err_bkg = norm_bkg * int_bkg->getError();
	    		    h_data->SetBinContent(i, h->GetBinContent(h->FindBin(h_data->GetBinCenter(i))));
	    		    h_data->SetBinError(i, h->GetBinError(h->FindBin(h_data->GetBinCenter(i))));
	    		    h_postfit_model->SetBinContent(i, bin_cnt_sig+bin_cnt_bkg);
	    		    h_postfit_sig->SetBinContent(i, bin_cnt_sig);
	    		    h_postfit_bkg->SetBinContent(i, bin_cnt_bkg);
	    		}
	    		
            		string region;
            		char tmp_ch[100];
            		if (isZeg) region = "Zeg"; else region = "Zee";
            		if (ptbin >= 1 && etabin <= 0) {
            		    sprintf(tmp_ch, "/PtBin%d", ptbin);
            		    region += tmp_ch;
            		} else if (ptbin <= 0 && etabin >= 1) {
            		    sprintf(tmp_ch, "/EtaBin%d", etabin);
            		    region += tmp_ch;
            		} else if (ptbin >= 1 && etabin >= 1) {
            		    sprintf(tmp_ch, "/PtBin%dEtaBin%d", ptbin, etabin);
            		    region += tmp_ch;
            		}
            		xframe->SetTitle(region.c_str());
            		xframe->GetXaxis()->SetTitle("Invariant mass");

	    	    if (ShowChi2) {
	    	        TLegend *lg = new TLegend(0.7,0.67,0.9,0.9);
	    	        lg->SetBorderSize(0);
	    	        lg->SetFillColor(0);
	    	        lg->AddEntry("data", "Data", "P");
	    	        lg->AddEntry("model", "Sig+Bkg", "l");
	    	        lg->AddEntry("signal", "Sig", "l");
	    	        lg->AddEntry("bkg", "Bkg", "l");
	    	        lg->Draw("same");
	    	    }
            		
            		TLatex lt;
            		lt.SetNDC();
            		lt.SetTextFont(42);
	    		
	    	    if (ShowChi2) {
	    	        sprintf(tmp_ch, "chi2/dof=%.1f", chi2);
	    	        lt.DrawLatex(0.7, 0.61, tmp_ch);
	    	    }
            		
            		double m_lt_x = 0.2;
            		double lt_y = 0.86;
            		lt.SetTextFont(72);
            		lt.DrawLatex(m_lt_x, lt_y, "ATLAS");
            		lt.SetTextFont(42);
            		double delx = 0.115*696*tmpc->GetWh()/(472*tmpc->GetWw());
            		lt.DrawLatex(m_lt_x+delx, lt_y, "Internal");
            		double ystep = -0.08;
            		lt_y += ystep;
			lt.DrawLatex(m_lt_x, lt_y, "#sqrt{s}=13TeV, 36.1 fb^{-1}");
            		//lt_y += ystep;
            		//lt.DrawLatex(m_lt_x, lt_y, "#intL dt = 36.1 fb^{-1}");
            		lt_y += ystep;
            		lt.DrawLatex(m_lt_x, lt_y, region.c_str());

	    		pad2->cd();
	    		TH1F* h_data_model = (TH1F*)h_data->Clone();
	    		h_data_model->SetName("h_data_model");
	    		h_data_model->Divide(h_postfit_model);

	    		Double_t enlarge = 3;
	    		h_data_model->GetXaxis()->SetLabelSize(0.05*enlarge);
	    		h_data_model->GetXaxis()->SetTitleSize(0.05*enlarge);
	    		h_data_model->GetXaxis()->SetTitleOffset(1);
	    		h_data_model->GetYaxis()->SetLabelSize(0.05*enlarge);
	    		h_data_model->GetXaxis()->SetTitle("Invariant mass [GeV]");
	    		h_data_model->GetYaxis()->SetTitle("Data/Model");
	    		h_data_model->GetYaxis()->SetTitleSize(0.05*enlarge);
	    		h_data_model->GetYaxis()->SetTitleOffset(1/enlarge);
	    		h_data_model->GetYaxis()->SetNdivisions(505);
	    		h_data_model->SetMaximum(1.5);
	    		h_data_model->SetMinimum(0.5);

	    		h_data_model->Draw();

            		string savename = "plots/EFake/Postfit_Zpeak"; 
            		if (ptbin >= 1 && etabin <= 0) {
            		    sprintf(tmp_ch, "_PtBin%d", ptbin);
            		    savename += tmp_ch;
            		} else if (ptbin <= 0 && etabin >= 1) {
            		    sprintf(tmp_ch, "_EtaBin%d", etabin);
            		    savename += tmp_ch;
            		} else if (ptbin >= 1 && etabin >= 1) {
            		    sprintf(tmp_ch, "_PtBin%d_EtaBin%d", ptbin, etabin);
            		    savename += tmp_ch;
            		}
            		if (isZeg) savename += "_zeg";
            		else savename += "_zee";

	    		if (RangeDown) savename += "_RDown";

			if (Cvt) savename += "_Cvt";
			if (UnCvt) savename += "_UnCvt";

			if (!NLOZy) {
			    tmpc->SaveAs(string(savename + string(".pdf")).c_str());
			    tmpc->SaveAs(string(savename + string(".png")).c_str());
			}

			if (isZeg && Nominal) {
    	    		    string savename = "results/Nominal/EFakeWeights";
    	    		    savename += ".root";
    	    		    cout << "save weight to file ->" << savename << endl; 
    	    		    TFile*fout = new TFile(savename.c_str(), "update");
			    TH1F* h_ratio = (TH1F*)h_postfit_sig->Clone();
			    for (int i = 1; i <= h_postfit_sig->GetNbinsX(); i++) {
			        double i_data = h->GetBinContent(h->FindBin(h_postfit_sig->GetBinCenter(i)));
			        double i_sig = h_postfit_sig->GetBinContent(i);
			        h_ratio->SetBinContent(i, i_sig/i_data);
			    }
			    char tmpname[50] ;
			    if (ptbin == -1 && etabin == -1) {
				sprintf(tmpname, "Weight");
			    } else if (ptbin != -1 && etabin != -1) {
				sprintf(tmpname, "WeightPt%dEta%d", ptbin, etabin);
			    }
    	    		    h_ratio->Write(tmpname);
    	    		    fout->Close();
    	    		    delete fout;
    	    		}

            		delete tmpc;
	    		delete h_data;
	    		delete h_postfit_model;
	    		delete h_postfit_sig;
	    		delete h_postfit_bkg;
	    		delete model;
	    		delete nsig;
	    		delete sig;
	    		delete nbkg;
	    		delete bkg;
	    	} else {
	    	    RooRealVar *mean = new RooRealVar("mean", "mean", 90, 0, 200); 
    	    		RooRealVar *sigma = new RooRealVar("sigma", "sigma", 30, 20, 100); 
	    		RooGaussian *poly = new RooGaussian("poly", "poly", x, *mean, *sigma);
    	    		RooRealVar *nbkg = new RooRealVar("nbkg", "nbkg", h->Integral()*0.1, 0, h->Integral());
	    		RooExtendPdf *bkg = new RooExtendPdf("bkg", "bkg", *poly, *nbkg);

	    		if (ptbin == 1 && etabin == 5 && !isZeg) {
	    		    p1->setVal(0.1);
	    		    p2->setVal(0.1);
	    		}

            		RooAddPdf *model = new RooAddPdf("model", "model", RooArgList(*sig, *bkg));
            		model->fitTo(*data, Extended(kTRUE)); //RooFitResult *results = model->fitTo(*data, RooFit::Save(),PrintEvalErrors(0),EvalErrorWall(kFALSE),Extended(kTRUE));
            		RooFitResult *results = model->fitTo(*data, RooFit::Save(),Extended(kTRUE)); //RooFitResult *results = model->fitTo(*data, RooFit::Save(),PrintEvalErrors(0),EvalErrorWall(kFALSE),Extended(kTRUE));
	    		cout << "Fit results status: " << results->status() << endl;
		cout << "Nsig: " << nsig->getVal() << " " << nsig->getError() << endl;
		cout << "Ndata: " << h->Integral() << " " << sqrt(h->Integral()) << endl;
		cout << "Nbkg: " << nbkg->getVal() << " " << nbkg->getError() << endl;

            		nZ = nsig->getVal();
            		eZ = nsig->getError();
    
            		// draw
            		RooPlot* xframe = x.frame(Title("example")) ;
            		data->plotOn(xframe,Name("data")) ;
	    		if (!ShowChi2) model->paramOn(xframe,Parameters(RooArgSet(*p1,*p2,*s,*a1,*a2,*mean,*sigma)),Layout(0.65,0.95),Format("NEU", AutoPrecision(1)));
            		model->plotOn(xframe) ;
            		Double_t chi2 = xframe->chiSquare();
            		model->plotOn(xframe,Name("signal"),Components(*sig),LineColor(kRed)) ;
            		model->plotOn(xframe,Name("bkg"),Components(*bkg),LineStyle(kDashed)) ;
            		model->plotOn(xframe,Name("model"),Components(RooArgSet(*bkg,*sig))) ;
	    		
            		TCanvas *tmpc = new TCanvas("tmp", "tmp", 800, 800);
	    		TPad* pad1 =  new TPad("pad1","pad1name",0.,0.20,1.,1.);   
	    		TPad* pad2 =  new TPad("pad2","pad1name",0.,0.05,1.,0.32);    
	    		pad1->Draw();
	    		pad1->SetRightMargin(0.05);
    	    		pad2->Draw();
    	    		pad2->SetGridy();
	    		pad2->SetRightMargin(0.05);
	    		pad2->SetBottomMargin(0.3);
	    		pad1->cd();
	    		pad1->SetLogy(); 
	    		
	    		double min = 9999999;
	    		double max = 0;
	    		for (int i = 1; i <= h->GetNbinsX(); i++) {
	    		    if (h->GetBinCenter(i) >= fitrange_lo && h->GetBinCenter(i) <= fitrange_hi) {
	    		        if (min > h->GetBinContent(i)) min = h->GetBinContent(i);
	    		        if (max < h->GetBinContent(i)) max = h->GetBinContent(i);
	    		    }
	    		}
	    		if (min <= 0) min = 1;
	    		xframe->GetYaxis()->SetRangeUser(min*0.5, max*12);
	    		xframe->GetYaxis()->SetTitleOffset(1) ; 
	    		xframe->Draw() ;
	    		
	    		// post-fit results
	    		double norm_sig = nsig->getVal();
	    		double norm_bkg = nbkg->getVal();
	    		RooAbsReal* int_sig = sig->createIntegral(x,NormSet(x),Range("cut"));
	    		
	    		int bin_lo = h->FindBin(fitrange_lo);
	    		int bin_hi = h->FindBin(fitrange_hi);
	    		int nbin = bin_hi - bin_lo;
	    		TH1F* h_data = new TH1F("h_data","h_data",nbin,fitrange_lo,fitrange_hi);
	    		TH1F* h_postfit_model = new TH1F("Postfit_model","Postfit_model",nbin,fitrange_lo,fitrange_hi);
	    		TH1F* h_postfit_sig = new TH1F("Postfit_sig","Postfit_sig",nbin,fitrange_lo,fitrange_hi);
	    		TH1F* h_postfit_bkg = new TH1F("Postfit_bkg","Postfit_bkg",nbin,fitrange_lo,fitrange_hi);
	    		for (int i = 1; i <= nbin; i++) {
	    		    x.setRange("tmprange",h_postfit_sig->GetBinLowEdge(i),h_postfit_sig->GetBinLowEdge(i)+h_postfit_sig->GetBinWidth(i));
	    		    RooAbsReal* int_sig = sig->createIntegral(x,NormSet(x),Range("tmprange"));
	    		    RooAbsReal* int_bkg = bkg->createIntegral(x,NormSet(x),Range("tmprange"));
	    		    double bin_cnt_sig = norm_sig * int_sig->getVal();
	    		    double bin_cnt_bkg = norm_bkg * int_bkg->getVal();
	    		    //double bin_err_sig = norm_sig * int_sig->getError();
	    		    //double bin_err_bkg = norm_bkg * int_bkg->getError();
	    		    h_data->SetBinContent(i, h->GetBinContent(h->FindBin(h_data->GetBinCenter(i))));
	    		    h_data->SetBinError(i, h->GetBinError(h->FindBin(h_data->GetBinCenter(i))));
	    		    h_postfit_model->SetBinContent(i, bin_cnt_sig+bin_cnt_bkg);
	    		    h_postfit_sig->SetBinContent(i, bin_cnt_sig);
	    		    h_postfit_bkg->SetBinContent(i, bin_cnt_bkg);
	    		}
	    		
            		string region;
            		char tmp_ch[100];
            		if (isZeg) region = "Zeg"; else region = "Zee";
            		if (ptbin >= 1 && etabin <= 0) {
            		    sprintf(tmp_ch, "/PtBin%d", ptbin);
            		    region += tmp_ch;
            		} else if (ptbin <= 0 && etabin >= 1) {
            		    sprintf(tmp_ch, "/EtaBin%d", etabin);
            		    region += tmp_ch;
            		} else if (ptbin >= 1 && etabin >= 1) {
            		    sprintf(tmp_ch, "/PtBin%dEtaBin%d", ptbin, etabin);
            		    region += tmp_ch;
            		}
            		xframe->SetTitle(region.c_str());
            		xframe->GetXaxis()->SetTitle("Invariant mass");

	    	    if (ShowChi2) {
	    	        TLegend *lg = new TLegend(0.7,0.67,0.9,0.9);
	    	        lg->SetBorderSize(0);
	    	        lg->SetFillColor(0);
	    	        lg->AddEntry("data", "Data", "P");
	    	        lg->AddEntry("model", "Sig+Bkg", "l");
	    	        lg->AddEntry("signal", "Sig", "l");
	    	        lg->AddEntry("bkg", "Bkg", "l");
	    	        lg->Draw("same");
	    	    }
            		
            		TLatex lt;
            		lt.SetNDC();
            		lt.SetTextFont(42);
	    		
	    	    if (ShowChi2) {
	    	        sprintf(tmp_ch, "chi2/dof=%.1f", chi2);
	    	        lt.DrawLatex(0.7, 0.61, tmp_ch);
	    	    }
            		
            		double m_lt_x = 0.2;
            		double lt_y = 0.86;
            		lt.SetTextFont(72);
            		lt.DrawLatex(m_lt_x, lt_y, "ATLAS");
            		lt.SetTextFont(42);
            		double delx = 0.115*696*tmpc->GetWh()/(472*tmpc->GetWw());
            		lt.DrawLatex(m_lt_x+delx, lt_y, "Internal");
            		double ystep = -0.08;
            		lt_y += ystep;
			lt.DrawLatex(m_lt_x, lt_y, "#sqrt{s}=13TeV, 36.1 fb^{-1}");
            		//lt_y += ystep;
            		//lt.DrawLatex(m_lt_x, lt_y, "#intL dt = 36.1 fb^{-1}");
            		lt_y += ystep;
            		lt.DrawLatex(m_lt_x, lt_y, region.c_str());

	    		pad2->cd();
	    		TH1F* h_data_model = (TH1F*)h_data->Clone();
	    		h_data_model->SetName("h_data_model");
	    		h_data_model->Divide(h_postfit_model);

	    		Double_t enlarge = 3;
	    		h_data_model->GetXaxis()->SetLabelSize(0.05*enlarge);
	    		h_data_model->GetXaxis()->SetTitleSize(0.05*enlarge);
	    		h_data_model->GetXaxis()->SetTitleOffset(1);
	    		h_data_model->GetYaxis()->SetLabelSize(0.05*enlarge);
	    		h_data_model->GetXaxis()->SetTitle("Invariant mass [GeV]");
	    		h_data_model->GetYaxis()->SetTitle("Data/Model");
	    		h_data_model->GetYaxis()->SetTitleSize(0.05*enlarge);
	    		h_data_model->GetYaxis()->SetTitleOffset(1/enlarge);
	    		h_data_model->GetYaxis()->SetNdivisions(505);
	    		h_data_model->SetMaximum(1.5);
	    		h_data_model->SetMinimum(0.5);

	    		h_data_model->Draw();

            		string savename = "plots/EFake/Postfit_Zpeak"; 
            		if (ptbin >= 1 && etabin <= 0) {
            		    sprintf(tmp_ch, "_PtBin%d", ptbin);
            		    savename += tmp_ch;
            		} else if (ptbin <= 0 && etabin >= 1) {
            		    sprintf(tmp_ch, "_EtaBin%d", etabin);
            		    savename += tmp_ch;
            		} else if (ptbin >= 1 && etabin >= 1) {
            		    sprintf(tmp_ch, "_PtBin%d_EtaBin%d", ptbin, etabin);
            		    savename += tmp_ch;
            		}
            		if (isZeg) savename += "_zeg";
            		else savename += "_zee";

	    		savename += "_Gaus";

			if (Cvt) savename += "_Cvt";
			if (UnCvt) savename += "_UnCvt";

            		tmpc->SaveAs(string(savename + string(".pdf")).c_str());
            		tmpc->SaveAs(string(savename + string(".png")).c_str());

            		delete tmpc;
	    		delete h_data;
	    		delete h_postfit_model;
	    		delete h_postfit_sig;
	    		delete h_postfit_bkg;
	    		delete model;
	    		delete nsig;
	    		delete sig;
	    		delete nbkg;
	    		delete bkg;
	    	}
	    }
        }
    }

    return pair<double,double> (nZ,eZ);
}

Results CalcuSF(int ptbin, int etabin) {
    Results r;

    string zeg_cut = "CutZeg";
    if (Cvt) zeg_cut += "Cvt";
    if (UnCvt) zeg_cut += "UnCvt";
    string zee_cut = "CutZee";
    
    if (ptbin > 2) {
	zeg_cut += "Reverse";
	zee_cut += "Reverse";
    }
    
    string fname_data_zeg = inputdir + "13TeV_" + zeg_cut + "_Data_EF1_zeg_Nominal_" + tag + ".root";
    string fname_data_zee = inputdir + "13TeV_" + zee_cut + "_Data_EF1_zee_Nominal_" + tag + ".root";
    string fname_MC_Zj_zeg = inputdir + "13TeV_" + zeg_cut + "_PhMatch_EFake_ZjetsElEl_Reco_EF1_zeg_Nominal_" + tag + ".root";
    string fname_MC_Zy_zeg;
    if (NLOZy) {
	fname_MC_Zy_zeg = inputdir + "13TeV_" + zeg_cut + "_PhMatch_TruePh_ZGammajetsElElNLO_Reco_EF1_zeg_Nominal_" + tag + ".root";
    } else {
	fname_MC_Zy_zeg = inputdir + "13TeV_" + zeg_cut + "_PhMatch_TruePh_ZjetsElEl_Reco_EF1_zeg_Nominal_" + tag + ".root";
    }
    string fname_MC_zee = inputdir + "13TeV_" + zee_cut + "_ZjetsElEl_Reco_EF1_zee_Nominal_" + tag + ".root";
    string fname_MC_zeg = inputdir + "13TeV_" + zeg_cut + "_ZjetsElEl_Reco_EF1_zeg_Nominal_" + tag + ".root";

    TFile* f_data_zeg = new TFile(fname_data_zeg.c_str());
    TFile* f_data_zee = new TFile(fname_data_zee.c_str());
    TFile* f_MC_Zj_zeg = new TFile(fname_MC_Zj_zeg.c_str());
    TFile* f_MC_Zy_zeg = new TFile(fname_MC_Zy_zeg.c_str());
    TFile* f_MC_zee = new TFile(fname_MC_zee.c_str());
    TFile* f_MC_zeg;
    if (MCTemp) {
	f_MC_zeg = new TFile(fname_MC_zeg.c_str());
    }

    if (!f_data_zeg->IsOpen()) {cout << "Cannot open file -> " << fname_data_zeg << endl; ; exit(-1);};
    if (!f_data_zee->IsOpen()) {cout << "Cannot open file -> " << fname_data_zee << endl; ; exit(-1);};
    if (!f_MC_Zj_zeg->IsOpen()) {cout << "Cannot open file -> " << fname_MC_Zj_zeg << endl; ; exit(-1);};
    if (!f_MC_Zy_zeg->IsOpen()) {cout << "Cannot open file -> " << fname_MC_Zy_zeg << endl; ; exit(-1);};
    if (!f_MC_zee->IsOpen()) {cout << "Cannot open file -> " << fname_MC_zee << endl; ; exit(-1);};
    if (MCTemp && !f_MC_zeg->IsOpen()) {cout << "Cannot open file -> " << fname_MC_zeg << endl; ; exit(-1);};

    string hprefix_zeg, hprefix_zee;
    if (ptbin <= 0 && etabin <= 0) {
	hprefix_zeg = "MPhLepForZ";
	hprefix_zee = "MLepLepForZ";
    } else if (ptbin >= 1 && etabin <= 0) {
	char tmp[50];
	sprintf(tmp, "InvariantMass_PtBin%d", ptbin);
	hprefix_zeg = tmp;
	hprefix_zee = tmp;
    } else if (ptbin <= 0 && etabin >= 1) {
	char tmp[50];
	sprintf(tmp, "InvariantMass_EtaBin%d", etabin);
	hprefix_zeg = tmp;
	hprefix_zee = tmp;
    } else if (ptbin >= 1 && etabin >= 1) {
	char tmp[50];
	sprintf(tmp, "InvariantMass_PtBin%d_EtaBin%d", ptbin, etabin);
	hprefix_zeg = tmp;
	hprefix_zee = tmp;
    } 

    string hname_data_zeg = hprefix_zeg + "_" + fname_data_zeg.substr(inputdir.size(), fname_data_zeg.find("_Nominal",0)-inputdir.size()+1) + "Nominal";
    string hname_data_zee = hprefix_zee + "_" + fname_data_zee.substr(inputdir.size(), fname_data_zee.find("_Nominal",0)-inputdir.size()+1) + "Nominal";
    string hname_MC_Zj_zeg = hprefix_zeg + "_" + fname_MC_Zj_zeg.substr(inputdir.size(), fname_MC_Zj_zeg.find("_Nominal",0)-inputdir.size()+1) + "Nominal";
    string hname_MC_Zy_zeg = hprefix_zeg + "_" + fname_MC_Zy_zeg.substr(inputdir.size(), fname_MC_Zy_zeg.find("_Nominal",0)-inputdir.size()+1) + "Nominal";
    string hname_MC_zee = hprefix_zee + "_" + fname_MC_zee.substr(inputdir.size(), fname_MC_zee.find("_Nominal",0)-inputdir.size()+1) + "Nominal";

    string hprefix_tf_zeg, hprefix_tf_zee;
    if (ptbin <= 0 && etabin <= 0) {
	hprefix_tf_zeg = "InvariantMassTF1";
	hprefix_tf_zee = "InvariantMassTF1";
    } else if (ptbin >= 1 && etabin <= 0) {
	char tmp[50];
	sprintf(tmp, "InvariantMassTF1_PtBin%d", ptbin);
	hprefix_tf_zeg = tmp;
	hprefix_tf_zee = tmp;
    } else if (ptbin <= 0 && etabin >= 1) {
	char tmp[50];
	sprintf(tmp, "InvariantMassTF1_EtaBin%d", etabin);
	hprefix_tf_zeg = tmp;
	hprefix_tf_zee = tmp;
    } else if (ptbin >= 1 && etabin >= 1) {
	char tmp[50];
	sprintf(tmp, "InvariantMassTF1_PtBin%d_EtaBin%d", ptbin, etabin);
	hprefix_tf_zeg = tmp;
	hprefix_tf_zee = tmp;
    } 
    string tfname_MC_zeg = hprefix_tf_zeg + "_" + fname_MC_zeg.substr(inputdir.size(), fname_MC_zeg.find("_Nominal",0)-inputdir.size()+1) + "Nominal";
    string tfname_MC_zee = hprefix_tf_zee + "_" + fname_MC_zee.substr(inputdir.size(), fname_MC_zee.find("_Nominal",0)-inputdir.size()+1) + "Nominal";

    TH1F* h_data_zeg = (TH1F*)f_data_zeg->Get(hname_data_zeg.c_str()); if (!h_data_zeg) {cout << "Cannot find hist -> " << hname_data_zeg << endl; f_data_zeg->ls(); exit(-1);}
    TH1F* h_data_zee = (TH1F*)f_data_zee->Get(hname_data_zee.c_str()); if (!h_data_zee) {cout << "Cannot find hist -> " << hname_data_zee << endl; f_data_zee->ls(); exit(-1);}
    TH1F* h_MC_Zj_zeg = (TH1F*)f_MC_Zj_zeg->Get(hname_MC_Zj_zeg.c_str()); if (!h_MC_Zj_zeg) {cout << "Cannot find hist -> " << hname_MC_Zj_zeg << endl; f_MC_Zj_zeg->ls(); exit(-1);}
    TH1F* h_MC_Zy_zeg = (TH1F*)f_MC_Zy_zeg->Get(hname_MC_Zy_zeg.c_str()); if (!h_MC_Zy_zeg) {cout << "Cannot find hist -> " << hname_MC_Zy_zeg << endl; f_MC_Zy_zeg->ls(); exit(-1);}
    TH1F* h_MC_zee = (TH1F*)f_MC_zee->Get(hname_MC_zee.c_str()); if (!h_MC_zee) {cout << "Cannot find hist -> " << hname_MC_zee << endl; f_MC_zee->ls(); exit(-1);}
    TH1F* h_MC_zeg = (TH1F*)h_MC_Zj_zeg->Clone(); h_MC_zeg->Add(h_MC_Zy_zeg);
    TF1* tf_MC_zeg;
    TF1* tf_MC_zee;
    if (MCTemp) {
	tf_MC_zeg = (TF1*)f_MC_zeg->Get(tfname_MC_zeg.c_str()); if (!tf_MC_zeg) {cout << "Cannot find hist -> " << tfname_MC_zeg << endl; f_MC_zeg->ls(); exit(-1);}
	tf_MC_zee = (TF1*)f_MC_zee->Get(tfname_MC_zee.c_str()); if (!tf_MC_zee) {cout << "Cannot find hist -> " << tfname_MC_zee << endl; f_MC_zee->ls(); exit(-1);}
    }

    pair<double,double> nZ_data_zeg;;
    pair<double,double> nZ_data_zee;;
    if (MCTemp) {
	nZ_data_zeg = FitMassPeak(h_data_zeg, ptbin, etabin, h_MC_zeg, tf_MC_zeg);
	nZ_data_zee = FitMassPeak(h_data_zee, ptbin, etabin, h_MC_zee, tf_MC_zee);
    } else {
	nZ_data_zeg = FitMassPeak(h_data_zeg, ptbin, etabin);
	nZ_data_zee = FitMassPeak(h_data_zee, ptbin, etabin);
    }
    pair<double,double> nZ_MC_Zj_zeg = FitMassPeak(h_MC_Zj_zeg, ptbin, etabin);
    pair<double,double> nZ_MC_Zy_zeg = FitMassPeak(h_MC_Zy_zeg, ptbin, etabin);
    pair<double,double> nZ_MC_zee = FitMassPeak(h_MC_zee, ptbin, etabin);

    cout << fixed << setprecision(precision);
    cout << "Ptbin: " << ptbin << " Etabin: " << etabin << endl;
    cout << setw(35) << "Data zeg" << setw(35) << "Data zee" << setw(35) << "MC Zj zeg" << setw(35) << "MC Zy zeg" << setw(35) << "MC zee" << endl;
    cout << setw(20) << nZ_data_zeg.first  << " +/- " << setw(10) << nZ_data_zeg.second;
    cout << setw(20) << nZ_data_zee.first  << " +/- " << setw(10) << nZ_data_zee.second;
    cout << setw(20) << nZ_MC_Zj_zeg.first << " +/- " << setw(10) << nZ_MC_Zj_zeg.second;
    cout << setw(20) << nZ_MC_Zy_zeg.first << " +/- " << setw(10) << nZ_MC_Zy_zeg.second;
    cout << setw(20) << nZ_MC_zee.first    << " +/- " << setw(10) << nZ_MC_zee.second;
    cout << endl;
    pair<double,double> nZ_MC_zeg (nZ_MC_Zj_zeg.first+nZ_MC_Zy_zeg.first, sqrt(pow(nZ_MC_Zj_zeg.second,2) + pow(nZ_MC_Zy_zeg.second,2)));
    pair<double,double> nZ_data_corr_zeg (nZ_data_zeg.first-nZ_MC_Zy_zeg.first, sqrt(pow(nZ_data_zeg.second,2) + pow(nZ_MC_Zy_zeg.second,2)));
    double fr_data = nZ_data_zeg.first/nZ_data_zee.first * 100;
    double fr_data_err = fr_data*sqrt(pow(nZ_data_zeg.second/nZ_data_zeg.first,2) + pow(nZ_data_zee.second/nZ_data_zee.first,2));
    double fr_MC = nZ_MC_zeg.first/nZ_MC_zee.first * 100;
    double fr_MC_err = fr_MC*sqrt(pow(nZ_MC_zeg.second/nZ_MC_zeg.first,2) + pow(nZ_MC_zee.second/nZ_MC_zee.first,2));
    double fr_data_corr = nZ_data_corr_zeg.first/nZ_data_zee.first * 100;
    double fr_data_corr_err = fr_data_corr*sqrt(pow(nZ_data_corr_zeg.second/nZ_data_corr_zeg.first,2) + pow(nZ_data_zee.second/nZ_data_zee.first,2));
    double fr_MC_Zj = nZ_MC_Zj_zeg.first/nZ_MC_zee.first * 100;
    double fr_MC_Zj_err = fr_MC_Zj*sqrt(pow(nZ_MC_Zj_zeg.second/nZ_MC_Zj_zeg.first,2) + pow(nZ_MC_zee.second/nZ_MC_zee.first,2));
    double sf = fr_data_corr/fr_MC_Zj;
    double sf_err = sf * sqrt(pow(fr_data_corr_err/fr_data_corr,2) + pow(fr_MC_Zj_err/fr_MC_Zj,2));
    cout << setw(35) << "data fake rate" << setw(35) << "MC fake rate" << setw(35) << "SF" << endl;
    cout << setw(20) << fr_data_corr << " +/- " << fr_data_corr_err;
    cout << setw(20) << fr_MC_Zj << " +/- " << fr_MC_Zj_err;
    cout << setw(20) << sf << " +/- " << sf_err;
    cout << endl;

    r.fr_data = fr_data_corr;
    r.fr_data_err = fr_data_corr_err;
    r.fr_MC = fr_MC_Zj;
    r.fr_MC_err = fr_MC_Zj_err;
    r.sf = sf;
    r.sf_err = sf_err;

    return r;
}

double fnc_dscb(double*xx,double*pp)
{
  double x   = xx[0];
  // gaussian core
  double mu  = pp[0];//mean
  double sig = pp[1];//variance
  // transition parameters
  double a1  = pp[2];
  double p1  = pp[3];
  double a2  = pp[4];
  double p2  = pp[5];
  
  double u   = (x-mu)/sig;
  double A1  = TMath::Power(p1/TMath::Abs(a1),p1)*TMath::Exp(-a1*a1/2);
  double A2  = TMath::Power(p2/TMath::Abs(a2),p2)*TMath::Exp(-a2*a2/2);
  double B1  = p1/TMath::Abs(a1) - TMath::Abs(a1);
  double B2  = p2/TMath::Abs(a2) - TMath::Abs(a2);

  double result = 1;
  if      (u<-a1) result *= A1*TMath::Power(B1-u,-p1);
  else if (u<a2)  result *= TMath::Exp(-u*u/2);
  else            result *= A2*TMath::Power(B2+u,-p2);
  return result;
}

double Try1PBkg (double val, double fitrange_lo, double fitrange_hi, TH1F*h) 
{
    RooRealVar x("mass","mass",fitrange_lo,fitrange_hi) ;
    x.setBins(fitrange_hi-fitrange_lo);
    RooDataHist *data = new RooDataHist("data", "data", x, h);
    TF1 *dscb_tf1 = new TF1("dscb_tf1", fnc_dscb, fitrange_lo,fitrange_hi,6);
    dscb_tf1->SetParameters(90,3,1.2,1.4,1.6,1.3);
    RooRealVar *m = new RooRealVar("m","m",90,fitrange_lo,fitrange_hi) ;
    RooRealVar *s = new RooRealVar("s","s",3,0,100) ;
    RooRealVar *a1 = new RooRealVar("a1","a1",1.2,0,20.);
    RooRealVar *p1 = new RooRealVar("p1","p1",1.4,0,20.);
    RooRealVar *a2 = new RooRealVar("a2","a2",1.6,0,20.);
    RooRealVar *p2 = new RooRealVar("p2","p2",1.3,0,20.);
    RooAbsReal *dscb_bfunc = bindFunction(dscb_tf1, x, RooArgList(*m,*s,*a1,*p1,*a2,*p2));//deltaM and cw are parameters
    RooRealVar dummy_a("dummy_a","dummy_a",0); dummy_a.setConstant(kTRUE);
    RooChebychev dummy_bkg("dummy_bkg","dummy_bkg",x, RooArgList(dummy_a)); //this is now a constant
    RooRealVar dummy_c("dummy_c","dummy_c",1); dummy_c.setConstant(kTRUE);
    RooAbsPdf *dscb = new RooRealSumPdf("dscb","dscb",*dscb_bfunc,dummy_bkg,dummy_c); //combine the constant term (bkg) and the term RooAbsReal (which is a function) into a PDF.
    RooRealVar *nsig = new RooRealVar("nsig", "nsig", h->Integral()*0.9, 0, 2*h->Integral());
    RooExtendPdf *sig = new RooExtendPdf("sig", "sig", *dscb, *nsig);
    RooRealVar *pa0 = new RooRealVar("pa0", "pa0", 0, -100, 100); pa0->setVal(val);
    RooBernstein *poly = new RooBernstein("poly", "poly", x, RooArgList(*pa0));
    RooRealVar *nbkg = new RooRealVar("nbkg", "nbkg", h->Integral()*0.1, 0, h->Integral());
    RooExtendPdf *bkg = new RooExtendPdf("bkg", "bkg", *poly, *nbkg);
    RooAddPdf *model = new RooAddPdf("model", "model", RooArgList(*sig, *bkg));
    RooFitResult *results = model->fitTo(*data, RooFit::Save(),Extended(kTRUE));
    RooPlot* xframe = x.frame(Title("example")) ;
    data->plotOn(xframe,Name("data")) ;
    model->plotOn(xframe) ;
    double chi2 = xframe->chiSquare();
    delete dscb;
    delete sig;
    delete xframe;
    delete poly;
    delete nbkg;
    delete bkg;
    delete model;
    delete m;
    delete s;
    delete a1;
    delete p1;
    delete a2;
    delete p2;
    delete nsig;
    delete data;
    delete dscb_tf1;
    delete pa0;

    return chi2;
}
double Try2PBkg (double val1, double val2, double fitrange_lo, double fitrange_hi, TH1F*h) 
{
    RooRealVar x("mass","mass",fitrange_lo,fitrange_hi) ;
    x.setBins(fitrange_hi-fitrange_lo);
    RooDataHist *data = new RooDataHist("data", "data", x, h);
    TF1 *dscb_tf1 = new TF1("dscb_tf1", fnc_dscb, fitrange_lo,fitrange_hi,6);
    dscb_tf1->SetParameters(90,3,1.2,1.4,1.6,1.3);
    RooRealVar *m = new RooRealVar("m","m",90,fitrange_lo,fitrange_hi) ;
    RooRealVar *s = new RooRealVar("s","s",3,0,100) ;
    RooRealVar *a1 = new RooRealVar("a1","a1",1.2,0,20.);
    RooRealVar *p1 = new RooRealVar("p1","p1",1.4,0,20.);
    RooRealVar *a2 = new RooRealVar("a2","a2",1.6,0,20.);
    RooRealVar *p2 = new RooRealVar("p2","p2",1.3,0,20.);
    RooAbsReal *dscb_bfunc = bindFunction(dscb_tf1, x, RooArgList(*m,*s,*a1,*p1,*a2,*p2));//deltaM and cw are parameters
    RooRealVar dummy_a("dummy_a","dummy_a",0); dummy_a.setConstant(kTRUE);
    RooChebychev dummy_bkg("dummy_bkg","dummy_bkg",x, RooArgList(dummy_a)); //this is now a constant
    RooRealVar dummy_c("dummy_c","dummy_c",1); dummy_c.setConstant(kTRUE);
    RooAbsPdf *dscb = new RooRealSumPdf("dscb","dscb",*dscb_bfunc,dummy_bkg,dummy_c); //combine the constant term (bkg) and the term RooAbsReal (which is a function) into a PDF.
    RooRealVar *nsig = new RooRealVar("nsig", "nsig", h->Integral()*0.9, 0, 2*h->Integral());
    RooExtendPdf *sig = new RooExtendPdf("sig", "sig", *dscb, *nsig);
    RooRealVar *pa0 = new RooRealVar("pa0", "pa0", 0, -100, 100); pa0->setVal(val1);
    RooRealVar *pa1 = new RooRealVar("pa1", "pa1", 0, -100, 100); pa1->setVal(val2);
    RooBernstein *poly = new RooBernstein("poly", "poly", x, RooArgList(*pa0,*pa1));
    RooRealVar *nbkg = new RooRealVar("nbkg", "nbkg", h->Integral()*0.1, 0, h->Integral());
    RooExtendPdf *bkg = new RooExtendPdf("bkg", "bkg", *poly, *nbkg);
    RooAddPdf *model = new RooAddPdf("model", "model", RooArgList(*sig, *bkg));
    RooFitResult *results = model->fitTo(*data, RooFit::Save(),Extended(kTRUE));
    RooPlot* xframe = x.frame(Title("example")) ;
    data->plotOn(xframe,Name("data")) ;
    model->plotOn(xframe) ;
    double chi2 = xframe->chiSquare();
    delete dscb;
    delete sig;
    delete xframe;
    delete poly;
    delete nbkg;
    delete bkg;
    delete model;
    delete m;
    delete s;
    delete a1;
    delete p1;
    delete a2;
    delete p2;
    delete nsig;
    delete data;
    delete dscb_tf1;
    delete pa0;
    delete pa1;

    return chi2;
}
double Try3PBkg (double val1, double val2, double val3, double fitrange_lo, double fitrange_hi, TH1F*h) 
{
    RooRealVar x("mass","mass",fitrange_lo,fitrange_hi) ;
    x.setBins(fitrange_hi-fitrange_lo);
    RooDataHist *data = new RooDataHist("data", "data", x, h);
    TF1 *dscb_tf1 = new TF1("dscb_tf1", fnc_dscb, fitrange_lo,fitrange_hi,6);
    dscb_tf1->SetParameters(90,3,1.2,1.4,1.6,1.3);
    RooRealVar *m = new RooRealVar("m","m",90,fitrange_lo,fitrange_hi) ;
    RooRealVar *s = new RooRealVar("s","s",3,0,100) ;
    RooRealVar *a1 = new RooRealVar("a1","a1",1.2,0,20.);
    RooRealVar *p1 = new RooRealVar("p1","p1",1.4,0,20.);
    RooRealVar *a2 = new RooRealVar("a2","a2",1.6,0,20.);
    RooRealVar *p2 = new RooRealVar("p2","p2",1.3,0,20.);
    RooAbsReal *dscb_bfunc = bindFunction(dscb_tf1, x, RooArgList(*m,*s,*a1,*p1,*a2,*p2));//deltaM and cw are parameters
    RooRealVar dummy_a("dummy_a","dummy_a",0); dummy_a.setConstant(kTRUE);
    RooChebychev dummy_bkg("dummy_bkg","dummy_bkg",x, RooArgList(dummy_a)); //this is now a constant
    RooRealVar dummy_c("dummy_c","dummy_c",1); dummy_c.setConstant(kTRUE);
    RooAbsPdf *dscb = new RooRealSumPdf("dscb","dscb",*dscb_bfunc,dummy_bkg,dummy_c); //combine the constant term (bkg) and the term RooAbsReal (which is a function) into a PDF.
    RooRealVar *nsig = new RooRealVar("nsig", "nsig", h->Integral()*0.9, 0, 2*h->Integral());
    RooExtendPdf *sig = new RooExtendPdf("sig", "sig", *dscb, *nsig);
    RooRealVar *pa0 = new RooRealVar("pa0", "pa0", 0, -100, 100); pa0->setVal(val1);
    RooRealVar *pa1 = new RooRealVar("pa1", "pa1", 0, -100, 100); pa1->setVal(val2);
    RooRealVar *pa2 = new RooRealVar("pa2", "pa2", 0, -100, 100); pa2->setVal(val3);
    RooBernstein *poly = new RooBernstein("poly", "poly", x, RooArgList(*pa0,*pa1,*pa2));
    RooRealVar *nbkg = new RooRealVar("nbkg", "nbkg", h->Integral()*0.1, 0, h->Integral());
    RooExtendPdf *bkg = new RooExtendPdf("bkg", "bkg", *poly, *nbkg);
    RooAddPdf *model = new RooAddPdf("model", "model", RooArgList(*sig, *bkg));
    RooFitResult *results = model->fitTo(*data, RooFit::Save(),Extended(kTRUE));
    RooPlot* xframe = x.frame(Title("example")) ;
    data->plotOn(xframe,Name("data")) ;
    model->plotOn(xframe) ;
    double chi2 = xframe->chiSquare();
    delete dscb;
    delete sig;
    delete xframe;
    delete poly;
    delete nbkg;
    delete bkg;
    delete model;
    delete m;
    delete s;
    delete a1;
    delete p1;
    delete a2;
    delete p2;
    delete nsig;
    delete data;
    delete dscb_tf1;
    delete pa0;
    delete pa1;
    delete pa2;

    return chi2;
}
double Try4PBkg (double val1, double val2, double val3, double val4, double fitrange_lo, double fitrange_hi, TH1F*h) 
{
    RooRealVar x("mass","mass",fitrange_lo,fitrange_hi) ;
    x.setBins(fitrange_hi-fitrange_lo);
    RooDataHist *data = new RooDataHist("data", "data", x, h);
    TF1 *dscb_tf1 = new TF1("dscb_tf1", fnc_dscb, fitrange_lo,fitrange_hi,6);
    dscb_tf1->SetParameters(90,3,1.2,1.4,1.6,1.3);
    RooRealVar *m = new RooRealVar("m","m",90,fitrange_lo,fitrange_hi) ;
    RooRealVar *s = new RooRealVar("s","s",3,0,100) ;
    RooRealVar *a1 = new RooRealVar("a1","a1",1.2,0,20.);
    RooRealVar *p1 = new RooRealVar("p1","p1",1.4,0,20.);
    RooRealVar *a2 = new RooRealVar("a2","a2",1.6,0,20.);
    RooRealVar *p2 = new RooRealVar("p2","p2",1.3,0,20.);
    RooAbsReal *dscb_bfunc = bindFunction(dscb_tf1, x, RooArgList(*m,*s,*a1,*p1,*a2,*p2));//deltaM and cw are parameters
    RooRealVar dummy_a("dummy_a","dummy_a",0); dummy_a.setConstant(kTRUE);
    RooChebychev dummy_bkg("dummy_bkg","dummy_bkg",x, RooArgList(dummy_a)); //this is now a constant
    RooRealVar dummy_c("dummy_c","dummy_c",1); dummy_c.setConstant(kTRUE);
    RooAbsPdf *dscb = new RooRealSumPdf("dscb","dscb",*dscb_bfunc,dummy_bkg,dummy_c); //combine the constant term (bkg) and the term RooAbsReal (which is a function) into a PDF.
    RooRealVar *nsig = new RooRealVar("nsig", "nsig", h->Integral()*0.9, 0, 2*h->Integral());
    RooExtendPdf *sig = new RooExtendPdf("sig", "sig", *dscb, *nsig);
    RooRealVar *pa0 = new RooRealVar("pa0", "pa0", 0, -100, 100); pa0->setVal(val1);
    RooRealVar *pa1 = new RooRealVar("pa1", "pa1", 0, -100, 100); pa1->setVal(val2);
    RooRealVar *pa2 = new RooRealVar("pa2", "pa2", 0, -100, 100); pa2->setVal(val3);
    RooRealVar *pa3 = new RooRealVar("pa3", "pa3", 0, -100, 100); pa3->setVal(val4);
    RooBernstein *poly = new RooBernstein("poly", "poly", x, RooArgList(*pa0,*pa1,*pa2,*pa3));
    RooRealVar *nbkg = new RooRealVar("nbkg", "nbkg", h->Integral()*0.1, 0, h->Integral());
    RooExtendPdf *bkg = new RooExtendPdf("bkg", "bkg", *poly, *nbkg);
    RooAddPdf *model = new RooAddPdf("model", "model", RooArgList(*sig, *bkg));
    RooFitResult *results = model->fitTo(*data, RooFit::Save(),Extended(kTRUE));
    RooPlot* xframe = x.frame(Title("example")) ;
    data->plotOn(xframe,Name("data")) ;
    model->plotOn(xframe) ;
    double chi2 = xframe->chiSquare();
    delete dscb;
    delete sig;
    delete xframe;
    delete poly;
    delete nbkg;
    delete bkg;
    delete model;
    delete m;
    delete s;
    delete a1;
    delete p1;
    delete a2;
    delete p2;
    delete nsig;
    delete data;
    delete dscb_tf1;
    delete pa0;
    delete pa1;
    delete pa2;
    delete pa3;

    return chi2;
}
double Try5PBkg (double val1, double val2, double val3, double val4, double val5, double fitrange_lo, double fitrange_hi, TH1F*h) 
{
    RooRealVar x("mass","mass",fitrange_lo,fitrange_hi) ;
    x.setBins(fitrange_hi-fitrange_lo);
    RooDataHist *data = new RooDataHist("data", "data", x, h);
    TF1 *dscb_tf1 = new TF1("dscb_tf1", fnc_dscb, fitrange_lo,fitrange_hi,6);
    dscb_tf1->SetParameters(90,3,1.2,1.4,1.6,1.3);
    RooRealVar *m = new RooRealVar("m","m",90,fitrange_lo,fitrange_hi) ;
    RooRealVar *s = new RooRealVar("s","s",3,0,100) ;
    RooRealVar *a1 = new RooRealVar("a1","a1",1.2,0,20.);
    RooRealVar *p1 = new RooRealVar("p1","p1",1.4,0,20.);
    RooRealVar *a2 = new RooRealVar("a2","a2",1.6,0,20.);
    RooRealVar *p2 = new RooRealVar("p2","p2",1.3,0,20.);
    RooAbsReal *dscb_bfunc = bindFunction(dscb_tf1, x, RooArgList(*m,*s,*a1,*p1,*a2,*p2));//deltaM and cw are parameters
    RooRealVar dummy_a("dummy_a","dummy_a",0); dummy_a.setConstant(kTRUE);
    RooChebychev dummy_bkg("dummy_bkg","dummy_bkg",x, RooArgList(dummy_a)); //this is now a constant
    RooRealVar dummy_c("dummy_c","dummy_c",1); dummy_c.setConstant(kTRUE);
    RooAbsPdf *dscb = new RooRealSumPdf("dscb","dscb",*dscb_bfunc,dummy_bkg,dummy_c); //combine the constant term (bkg) and the term RooAbsReal (which is a function) into a PDF.
    RooRealVar *nsig = new RooRealVar("nsig", "nsig", h->Integral()*0.9, 0, 2*h->Integral());
    RooExtendPdf *sig = new RooExtendPdf("sig", "sig", *dscb, *nsig);
    RooRealVar *pa0 = new RooRealVar("pa0", "pa0", 0, 0, 100); pa0->setVal(val1);
    RooRealVar *pa1 = new RooRealVar("pa1", "pa1", 0, 0, 100); pa1->setVal(val2);
    RooRealVar *pa2 = new RooRealVar("pa2", "pa2", 0, 0, 100); pa2->setVal(val3);
    RooRealVar *pa3 = new RooRealVar("pa3", "pa3", 0, 0, 100); pa3->setVal(val4);
    RooRealVar *pa4 = new RooRealVar("pa4", "pa4", 0, 0, 100); pa4->setVal(val5);
    RooBernstein *poly = new RooBernstein("poly", "poly", x, RooArgList(*pa0,*pa1,*pa2,*pa3,*pa4));
    RooRealVar *nbkg = new RooRealVar("nbkg", "nbkg", h->Integral()*0.1, 0, h->Integral());
    RooExtendPdf *bkg = new RooExtendPdf("bkg", "bkg", *poly, *nbkg);
    RooAddPdf *model = new RooAddPdf("model", "model", RooArgList(*sig, *bkg));
    RooFitResult *results = model->fitTo(*data, RooFit::Save(),Extended(kTRUE));
    RooPlot* xframe = x.frame(Title("example")) ;
    data->plotOn(xframe,Name("data")) ;
    model->plotOn(xframe) ;
    double chi2 = xframe->chiSquare();
    delete dscb;
    delete sig;
    delete xframe;
    delete poly;
    delete nbkg;
    delete bkg;
    delete model;
    delete m;
    delete s;
    delete a1;
    delete p1;
    delete a2;
    delete p2;
    delete nsig;
    delete data;
    delete dscb_tf1;
    delete pa0;
    delete pa1;
    delete pa2;
    delete pa3;
    delete pa4;

    return chi2;
}
