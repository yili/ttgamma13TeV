#include <TMath.h>
#include <iomanip>
#include <TFile.h>
#include <TH1.h>
#include <TIterator.h>
#include <TKey.h>
#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <map>
#include <string>
#include <time.h>

#include "StringPlayer.h"
#include "PlotComparor.h"

using namespace std;

void PrintHeader(double val_nom, double val_nom_err);
void PrintTail(double total_sys);

struct Raw {
    string channel;
    double expsys_nom;
    double expsys_nom_err;
    double theosys_nom;
    double theosys_nom_err;
    vector<double> expsys_ups;
    vector<double> expsys_dns;
    vector<double> theosys_ups;
    vector<double> theosys_dns;
    vector<double> theosys_ups_err;
    vector<double> theosys_dns_err;
};
Raw GetRaw(string Selection, string CutName, string Region, string SubRegion, string Process, string PhType, string Version);
Raw CombineRaw(vector<Raw> rin);
Raw DivideRaw(Raw num, Raw denom);
void ConvertSysNames(vector<string> &syslist, bool exp = true);

struct Results {
    vector<double> expsys_ups;
    vector<double> expsys_dns;
    vector<double> theosys_ups;
    vector<double> theosys_dns;
    double expsys_stat;
    double theosys_stat;
    double tot_expsys;
    double tot_theosys;
};
Results GetResults(Raw raw, string Process);
void PrintResults(vector<string> Channels, vector<Results> r_all);
void PrintResultsII(vector<string> Channels, vector<Results> r_all);

vector<string> expsyslist;
vector<string> theosyslist;
ofstream FailedJob;

bool debug = false;

int main(int argc, char* argv[]) {

    string Process = "TTBar";
    string PhType = "HFake";

    for (int i = 0; i < argc; i++) {
	if (!strcmp(argv[i],"--Process")) {
	    Process = argv[i+1];
   	}
	if (!strcmp(argv[i],"--PhType")) {
	    PhType = argv[i+1];
   	}
	if (!strcmp(argv[i],"--debug")) {
	    debug = true;
   	}
    }

    FailedJob.open("logs/FailedJobs.txt");

    vector<string> Channels;
    Channels.push_back("ejets");
    Channels.push_back("mujets");
    if (PhType != "EFake") {
	Channels.push_back("ee");
    	Channels.push_back("mumu");
    	Channels.push_back("emu");
    }

    vector<Raw> raw_all;
    vector<Raw> in_raw_sl; 
    vector<Raw> in_raw_dl; 
    vector<Results> results_all;
    for (int i = 0; i < Channels.size(); i++) {
	Raw raw = GetRaw("CutSR", "Cut:DrPhLep1.0", "CR1", Channels.at(i), Process, PhType, "Final02");
	if (Channels.at(i) == "ejets" || Channels.at(i) == "mujets") in_raw_sl.push_back(raw);
	if (Channels.at(i) == "ee" || Channels.at(i) == "mumu" || Channels.at(i) == "emu") in_raw_dl.push_back(raw);
	raw_all.push_back(raw);
	Results results = GetResults(raw, Process);
	results_all.push_back(results);
    }
    PrintResults(Channels, results_all);

    Channels.clear();
    results_all.clear();
    Channels.push_back("single lepton");
    if (PhType != "EFake") Channels.push_back("dilepton");

    Raw raw_sl = CombineRaw(in_raw_sl);
    Results results_sl = GetResults(raw_sl, Process);
    results_all.push_back(results_sl);
    Raw ratio;
    if (PhType != "EFake") { 
	Raw raw_dl = CombineRaw(in_raw_dl);
	Results results_dl = GetResults(raw_dl, Process);
	results_all.push_back(results_dl);

	ratio = DivideRaw(raw_dl, raw_sl);
    }
    PrintResults(Channels, results_all);

    if (PhType != "EFake") {
	Results results_ratio = GetResults(ratio, Process);
	results_all.clear();
	Channels.clear();
	Channels.push_back("dl/ll");
	results_all.push_back(results_ratio);
	PrintResults(Channels, results_all);
    }

    ConvertSysNames(expsyslist, true);
    ConvertSysNames(theosyslist, false);
    PrintResultsII(Channels, results_all);

    FailedJob.close();
    return 0;
}

void PrintHeader(double val_nom, double val_nom_err) {
    double scale = 1.0;
    cout << "\\scalebox{" << scale << "}{" << endl;
    cout << "\\begin{tabular}{c|c|c|c|c|c}" << endl;
    cout << "\\hline\\hline" << endl;
    cout << setw(60) << "Source" << "&" << setw(20) << "Up" << "&" << setw(20) << "RelSys(\\%)" << "&" << setw(20) << "Dn" << "&" << setw(20) << "RelSys(\\%)" << "&" << setw(20) << "Comment" << "\\\\" << "\\hline" << endl;
    double rel_nom_err = val_nom_err/val_nom * 100;
    cout << setw(60) << "Nominal" << "&\\multicolumn{5}{c}{" << setw(20) << val_nom << " $\\pm$ " << val_nom_err << " (" << rel_nom_err <<  "\\%)" << "}\\\\" << "\\hline" << endl;
}
void PrintTail(double total_sys) {
    cout << setw(60) << "Total sys(\\%)" << "&\\multicolumn{5}{c}{" << setw(20) << total_sys << "}\\\\" << endl;
    cout << "\\hline\\hline"<< endl;
    cout << "\\end{tabular}}" << endl;
}

Raw GetRaw(string Selection, string CutName, string Region, string SubRegion, string Process, string PhType, string Version) { 
    Raw r;
    r.channel = SubRegion;
    {
	vector<string> var_names;
    	vector<double> var_vals;
    	double val_nom;
	double val_nom_err;

    	string line;
    	ifstream ifile_variations;
    	ifile_variations.open("../config/Variations.txt");
    	while (getline(ifile_variations, line)) {
    	    if (line.size() == 0) continue;
    	
    	    int n1 = line.find_first_of(" ", 0);
    	    int n2 = line.size();
    	
    	    string treename = line.substr(0, n1);
    	    string variation = line.substr(n1+1, n2-n1-1);
    	    var_names.push_back(variation);
    	
    	    string fnamekey = "results/" + variation + "/13TeV_" + Selection + "_PhMatch_" + PhType + "_" + Process + "_Reco_" + Region + "_" + SubRegion + "_"; 
    	    string fnameapp = "_" + Version + ".root";
    	    string fname = fnamekey + variation + fnameapp;

    	    double val = val_nom; 
	    double val_err = val_nom_err;
	    if (debug) cout << fname << endl;
    	    TFile*f = new TFile(fname.c_str());
    	    if (f->IsOpen()) {
    	        string cutflowname = "Cutflow_"; cutflowname += fnamekey.substr(9+variation.size(), fnamekey.size()); cutflowname += variation;
    	        TH1F*h = (TH1F*)f->Get(cutflowname.c_str());

		if (!h) {
		    cout << "can't find " << cutflowname << endl;
		    f->ls();
		    exit(-1);
		}

    	        int nbin = h->GetNbinsX();
    	        for (int i = 0; i < nbin; i++) {
    	    	//if (variation == "MUON_MS__1up") {
    	    	//    cout << h->GetXaxis()->GetBinLabel(i+1) << " " << h->GetBinContent(i+1) << endl;
    	    	//}
    	            if (string(h->GetXaxis()->GetBinLabel(i+1)) == CutName) {
			val = h->GetBinContent(i+1);
			val_err = h->GetBinError(i+1);
			break;
    	            }
    	        }
    	        var_vals.push_back(val);
    	    } else {
		FailedJob << fname << endl;
    	        var_vals.push_back(val); // sys file not exist
    	    }

	    if (variation == "Nominal") {
		val_nom = var_vals.back();
		val_nom_err = val_err;
	   }
    	}

	r.expsys_nom = val_nom;
	r.expsys_nom_err = val_nom_err;

    	vector<string> sys_names;
    	vector<double> sys_ups;
    	vector<double> sys_dns;
    	for (int i = 0; i < var_names.size(); i++) {
    	    string var_name = var_names.at(i);
    	    double var_val = var_vals.at(i);
    	    if (var_name == "Nominal") continue;

    	    string sys_name;
    	    bool isup = true;

    	    string last2 = var_name.substr(var_name.size()-2, var_name.size());
    	    string last4 = var_name.substr(var_name.size()-4, var_name.size());
    	    if (last2 == "Up" || last2 == "up") {
    	        sys_name = var_name.substr(0, var_name.size()-2);
    	    } else if (last2 == "Dn") {
    	        sys_name = var_name.substr(0, var_name.size()-2);
    	        isup = false;
    	    } else if (last4 == "down" || last4 == "Down") {
    	        sys_name = var_name.substr(0, var_name.size()-4);
    	        isup = false;
    	    } else {
    	        sys_name = var_name;
    	    }

    	    bool exist = false;
    	    for (int j = 0; j < sys_names.size(); j++) {
    	        if (sys_names.at(j) == sys_name) {
    	            exist = true;
    	            break;
    	        }
    	    }
    	    if (!exist) {
    	        sys_names.push_back(sys_name);
    	        if (isup) {
    	    	sys_ups.push_back(var_val);
    	    	double sys_dn = val_nom; // other variation not exist
    	    	for (int j = 0; j < var_names.size(); j++) {
    	    	    string var_name_other = var_names.at(j);
    	    	    if (var_name_other.find(sys_name.c_str(),0) != string::npos && 
    	    		(var_name_other.substr(var_name_other.size()-2, var_name_other.size()) == "Dn" || var_name_other.substr(var_name_other.size()-4, var_name_other.size()) == "Down" || var_name_other.substr(var_name_other.size()-4, var_name_other.size()) == "Down")) {
    	    		sys_dn = var_vals.at(j);
    	    		break;
    	    	    }
    	    	}
    	    	sys_dns.push_back(sys_dn);
    	        } else {
    	    	sys_dns.push_back(var_val);
    	    	double sys_up = val_nom; // other variation not exist
    	    	for (int j = 0; j < var_names.size(); j++) {
    	    	    string var_name_other = var_names.at(j);
    	    	    if (var_name_other.find(sys_name.c_str(),0) != string::npos && 
    	    		(var_name_other.substr(var_name_other.size()-2, var_name_other.size()) == "Up" || var_name_other.substr(var_name_other.size()-2, var_name_other.size()) == "up")) {
    	    		sys_up = var_vals.at(j);
    	    		break;
    	    	    }
    	    	}
    	    	sys_ups.push_back(sys_up);
    	        }
    	    }
    	}

	if (expsyslist.size() == 0) {
	    for (int i = 0; i < sys_names.size(); i++) {
		expsyslist.push_back(sys_names.at(i));
	    }
	}

	r.expsys_ups = sys_ups;
	r.expsys_dns = sys_dns;
    }
    if (Process == "TTBar") {
	vector<string> var_names;
    	vector<double> var_vals;
    	vector<double> var_vals_err;
    	double val_nom;
	double val_nom_err;

	vector<string> variations;
	variations.push_back("TTBarAFNorm");
	variations.push_back("TTBarAFSherpa");
	variations.push_back("TTBarAFQCDUp");
	variations.push_back("TTBarAFQCDDn");
	for (int i = 0; i < variations.size(); i++) {
	    string variation = variations.at(i);
	    string fnamekey = "results/" + variation + "/13TeV_" + Selection + "_PhMatch_" + PhType + "_";
	    string fnameapp = "_Reco_" + Region + "_" + SubRegion + "_Nominal_" + Version + ".root";
	    string fname = fnamekey + variation + fnameapp;

    	    double val = val_nom; 
	    double val_err = val_nom_err;
    	    TFile*f = new TFile(fname.c_str());
    	    if (f->IsOpen()) {
    	        string cutflowname = "Cutflow_"; cutflowname += fnamekey.substr(9+variation.size(), fnamekey.size()); cutflowname += variation; cutflowname += "_Reco_" + Region + "_" + SubRegion + "_Nominal";
    	        TH1F*h = (TH1F*)f->Get(cutflowname.c_str());

    	        int nbin = h->GetNbinsX();
    	        for (int i = 0; i < nbin; i++) {
    	            if (string(h->GetXaxis()->GetBinLabel(i+1)) == CutName) {
			val = h->GetBinContent(i+1);
			val_err = h->GetBinError(i+1);
			break;
    	            }
    	        }
    	        var_vals.push_back(val);
    	        var_vals_err.push_back(val_err);
    	    } else {
    	        var_vals.push_back(val); // sys file not exist
    	        var_vals_err.push_back(val_err);
    	    }
	    var_names.push_back(variation);
	    if (variation == "TTBarAFNorm") {
		val_nom = var_vals.back();
		val_nom_err = val_err;
	   }
    	}

	r.theosys_nom = val_nom;
	r.theosys_nom_err = val_nom_err;

    	vector<string> sys_names;
    	vector<double> sys_ups;
    	vector<double> sys_dns;
    	vector<double> sys_ups_err;
    	vector<double> sys_dns_err;
    	for (int i = 0; i < var_names.size(); i++) {
    	    string var_name = var_names.at(i);
    	    double var_val = var_vals.at(i);
    	    double var_val_err = var_vals_err.at(i);
    	    if (var_name == "TTBarAFNorm") continue;

    	    string sys_name;
    	    bool isup = true;

    	    string last2 = var_name.substr(var_name.size()-2, var_name.size());
    	    string last4 = var_name.substr(var_name.size()-4, var_name.size());
    	    if (last2 == "Up") {
    	        sys_name = var_name.substr(0, var_name.size()-2);
    	    } else if (last2 == "Dn") {
    	        sys_name = var_name.substr(0, var_name.size()-2);
    	        isup = false;
    	    } else {
    	        sys_name = var_name;
    	    }

    	    bool exist = false;
    	    for (int j = 0; j < sys_names.size(); j++) {
    	        if (sys_names.at(j) == sys_name) {
    	            exist = true;
    	            break;
    	        }
    	    }
    	    if (!exist) {
    	        sys_names.push_back(sys_name);
    	        if (isup) {
    	    	sys_ups.push_back(var_val);
    	    	sys_ups_err.push_back(var_val_err);
    	    	double sys_dn = val_nom; // other variation not exist
    	    	double sys_dn_err = val_nom_err; // other variation not exist
    	    	for (int j = 0; j < var_names.size(); j++) {
    	    	    string var_name_other = var_names.at(j);
    	    	    if (var_name_other.find(sys_name.c_str(),0) != string::npos && 
    	    		(var_name_other.substr(var_name_other.size()-2, var_name_other.size()) == "Dn")) {
    	    		sys_dn = var_vals.at(j);
    	    		sys_dn_err = var_vals_err.at(j);
    	    		break;
    	    	    }
    	    	}
		sys_dns.push_back(sys_dn);
		sys_dns_err.push_back(sys_dn_err);
    	        } else {
    	    	sys_dns.push_back(var_val);
    	    	sys_dns_err.push_back(var_val_err);
    	    	double sys_up = val_nom; // other variation not exist
    	    	double sys_up_err = val_nom_err; // other variation not exist
    	    	for (int j = 0; j < var_names.size(); j++) {
    	    	    string var_name_other = var_names.at(j);
    	    	    if (var_name_other.find(sys_name.c_str(),0) != string::npos && 
    	    		(var_name_other.substr(var_name_other.size()-2, var_name_other.size()) == "Up")) {
    	    		sys_up = var_vals.at(j);
    	    		sys_up_err = var_vals_err.at(j);
    	    		break;
    	    	    }
    	    	}
    	    	sys_ups.push_back(sys_up);
    	        }
    	    }
    	}

	if (theosyslist.size() == 0) {
	    for (int i = 0; i < sys_names.size(); i++) {
		theosyslist.push_back(sys_names.at(i));
	    }
	}

	r.theosys_ups = sys_ups;
	r.theosys_dns = sys_dns;
	r.theosys_ups_err = sys_ups_err;
	r.theosys_dns_err = sys_dns_err;
    }

    return r;
}

Results GetResults(Raw raw, string Process) {
    Results r;
    {
	cout << fixed << setprecision(2);
    	PrintHeader(raw.expsys_nom, raw.expsys_nom_err);
    	double total_sys = 0;
    	vector<double> relsys_ups;
    	vector<double> relsys_dns;
	cout << raw.channel << endl;
    	for (int i = 0; i < expsyslist.size(); i++) {
    	    double sys_up = raw.expsys_ups.at(i);
    	    double sys_dn = raw.expsys_dns.at(i);
    	
    	    double relsys_up = (sys_up - raw.expsys_nom)/raw.expsys_nom * 100;
    	    double relsys_dn = (sys_dn - raw.expsys_nom)/raw.expsys_nom * 100;
    	
    	    bool samesign = false;
    	    if (relsys_up * relsys_dn > 0) {
    	        samesign = true;
    	    }
    	
    	    string sys_name_tmp = replace_str(expsyslist.at(i).c_str(), "__1", " ");
    	    sys_name_tmp = replaceChar(sys_name_tmp.c_str(), '_', ' ');
    	    cout << setw(60) << sys_name_tmp << "&" << setw(20) << sys_up << "&" << setw(20) << relsys_up << "&" << setw(20) << sys_dn << "&" << setw(20) << relsys_dn << "&";
    	    if (samesign) cout << setw(20) << "Same sign!";
    	
    	    cout << "\\\\" << "\\hline" << endl;
    	
    	    if (samesign) {
    	        relsys_up = fabs(relsys_up) > fabs(relsys_dn) ? fabs(relsys_up) : fabs(relsys_dn);
    	        relsys_dn = -1*relsys_up;
    	    }
    	
    	    double max_var = fabs(relsys_up) > fabs(relsys_dn) ? fabs(relsys_up) : fabs(relsys_dn);

    	    relsys_ups.push_back(relsys_up);
    	    relsys_dns.push_back(relsys_dn);
    	
    	    total_sys = sqrt(pow(total_sys,2) + pow(max_var,2));
    	}

	r.expsys_ups = relsys_ups;
	r.expsys_dns = relsys_dns;
	r.tot_expsys = total_sys;
	r.expsys_stat = raw.expsys_nom_err/raw.expsys_nom * 100;
    	PrintTail(total_sys);
    	cout << endl;
    }
    if (Process == "TTBar") {
	cout << fixed << setprecision(2);
    	PrintHeader(raw.theosys_nom, raw.theosys_nom_err);
    	double total_sys = 0;
    	vector<double> relsys_ups;
    	vector<double> relsys_dns;
    	vector<double> relsys_ups_err;
    	vector<double> relsys_dns_err;
	cout << raw.channel << endl;
    	for (int i = 0; i < theosyslist.size(); i++) {
    	    double sys_up = raw.theosys_ups.at(i);
    	    double sys_dn = raw.theosys_dns.at(i);
    	    double sys_up_err = raw.theosys_ups_err.at(i);
    	    double sys_dn_err = raw.theosys_dns_err.at(i);
    	
    	    double relsys_up = (sys_up - raw.theosys_nom)/raw.theosys_nom * 100;
    	    double relsys_dn = (sys_dn - raw.theosys_nom)/raw.theosys_nom * 100;
    	    double relsys_up_err = 100* (sys_up/raw.theosys_nom) * sqrt(pow(raw.theosys_nom_err/raw.theosys_nom,2) + pow(sys_up_err/sys_up,2));
    	    double relsys_dn_err = 100* (sys_dn/raw.theosys_nom) * sqrt(pow(raw.theosys_nom_err/raw.theosys_nom,2) + pow(sys_dn_err/sys_dn,2));
    	
    	    bool samesign = false;
    	    if (relsys_up * relsys_dn > 0) {
    	        samesign = true;
    	    }
    	
    	    string sys_name_tmp = replace_str(theosyslist.at(i).c_str(), "__1", " ");
    	    sys_name_tmp = replaceChar(sys_name_tmp.c_str(), '_', ' ');
    	    cout << setw(60) << sys_name_tmp << "&" << setw(20) << sys_up << " $\\pm$ " << sys_up_err << "&" << setw(20) << relsys_up << " $\\pm$ " << relsys_up_err << "&" << setw(20) << sys_dn << " $\\pm$ " << sys_dn_err << "&" << setw(20) << relsys_dn << " $\\pm$ " << relsys_dn_err << "&";
    	    if (samesign) cout << setw(20) << "Same sign!";
    	
    	    cout << "\\\\" << "\\hline" << endl;
    	
    	    if (samesign) {
    	        relsys_up = fabs(relsys_up) > fabs(relsys_dn) ? fabs(relsys_up) : fabs(relsys_dn);
    	        relsys_dn = -1*relsys_up;
    	        relsys_up_err = fabs(relsys_up) > fabs(relsys_dn) ? relsys_up_err : relsys_dn_err;
    	        relsys_dn_err = relsys_up_err;
    	    }
    	
    	    double max_var = fabs(relsys_up) > fabs(relsys_dn) ? fabs(relsys_up) : fabs(relsys_dn);
    	    double max_var_err = fabs(relsys_up) > fabs(relsys_dn) ? relsys_up_err : relsys_dn_err;

    	    relsys_ups.push_back(relsys_up);
    	    relsys_dns.push_back(relsys_dn);
    	    relsys_ups_err.push_back(relsys_up_err);
    	    relsys_dns_err.push_back(relsys_dn_err);
    	
    	    total_sys = sqrt(pow(total_sys,2) + pow(max_var,2));
    	}

	r.theosys_ups = relsys_ups;
	r.theosys_dns = relsys_dns;
	r.tot_theosys = total_sys;
	r.theosys_stat = (raw.theosys_nom_err/raw.theosys_nom * 100);
    	PrintTail(total_sys);
    	cout << endl;
    }

    return r;
}

void PrintResults(vector<string> Channels, vector<Results> r_all) {
    double scale = 1.0;
    cout << "\\scalebox{" << scale << "}{" << endl;
    cout << "\\begin{tabular}{c|c|c|c|c|c}" << endl;
    cout << "\\hline\\hline" << endl;
    cout << setw(60) << "Channel" << "&";
    for (int i = 0; i < Channels.size(); i++) {
	cout << setw(20) << Channels.at(i);
	if (i != Channels.size() - 1) cout << "&";
	else cout<< "\\\\" << "\\hline" << endl;
    }
    cout << setw(60) << "Nominal Stat" << "&";
    for (int i = 0; i < Channels.size(); i++) {
        cout << setw(20) << r_all.at(i).expsys_stat;
        if (i != Channels.size() - 1) cout << "&";
        else cout << "\\\\" << "\\hline" << endl;
    }
    for (int i = 0; i < expsyslist.size(); i++) {
	string sys_name_tmp = replace_str(expsyslist.at(i).c_str(), "__1", " ");
	sys_name_tmp = replaceChar(sys_name_tmp.c_str(), '_', ' ');
	cout << setw(60) << sys_name_tmp << "&";
	for (int j = 0; j < Channels.size(); j++) {
	    cout << setw(20) << r_all.at(j).expsys_ups.at(i) << " / " << r_all.at(j).expsys_dns.at(i);
	    if (j != Channels.size() - 1) cout << "&";
	    else cout << "\\\\" << "\\hline" << endl;
	}
    }
    cout << setw(60) << "Total sys(\\%)" << "&";
    for (int i = 0; i < Channels.size(); i++) {
        cout << setw(20) << r_all.at(i).tot_expsys;
        if (i != Channels.size() - 1) cout << "&";
        else cout << "\\\\" << endl;
    }
    cout << "\\scalebox{" << scale << "}{" << endl;
    cout << "\\begin{tabular}{c|c|c|c|c|c}" << endl;
    cout << "\\hline\\hline" << endl;
    cout << setw(60) << "Channel" << "&";
    for (int i = 0; i < Channels.size(); i++) {
	cout << setw(20) << Channels.at(i);
	if (i != Channels.size() - 1) cout << "&";
	else cout<< "\\\\" << "\\hline" << endl;
    }
    cout << setw(60) << "Nominal Stat" << "&";
    for (int i = 0; i < Channels.size(); i++) {
        cout << setw(20) << r_all.at(i).theosys_stat;
        if (i != Channels.size() - 1) cout << "&";
        else cout << "\\\\" << "\\hline" << endl;
    }
    for (int i = 0; i < theosyslist.size(); i++) {
	string sys_name_tmp = replace_str(theosyslist.at(i).c_str(), "__1", " ");
	sys_name_tmp = replaceChar(sys_name_tmp.c_str(), '_', ' ');
	cout << setw(60) << sys_name_tmp << "&";
	for (int j = 0; j < Channels.size(); j++) {
	    cout << setw(20) << r_all.at(j).theosys_ups.at(i) << " / " << r_all.at(j).theosys_dns.at(i);
	    if (j != Channels.size() - 1) cout << "&";
	    else cout << "\\\\" << "\\hline" << endl;
	}
    }
    cout << setw(60) << "Total sys(\\%)" << "&";
    for (int i = 0; i < Channels.size(); i++) {
        cout << setw(20) << r_all.at(i).tot_theosys;
        if (i != Channels.size() - 1) cout << "&";
        else cout << "\\\\" << endl;
    }
}

void PrintResultsII(vector<string> Channels, vector<Results> r_all) {
    ofstream ofile; ofile.open("logs/ForTRex.txt");
    for (int i = 0; i < expsyslist.size(); i++) {
	ofile << "Systematic: \"" << expsyslist.at(i) << "_HFake_Ext\"" << endl;
    	ofile << "  Title: \"" << expsyslist.at(i) << " HFake Ext\"" << endl;
    	ofile << "  NuisanceParameter: \"" << expsyslist.at(i) << "\"" << endl;
    	ofile << "  Type: OVERALL" << endl;
    	ofile << "  OverallUp: " << r_all.at(0).expsys_ups.at(i)/100 << endl;
    	ofile << "  OverallDown: " << r_all.at(0).expsys_dns.at(i)/100 << endl;
    	ofile << "  Samples: hadronfakes" << endl;	
    	ofile << "  Symmetrisation: ";
	if (expsyslist.at(i) == "JET_JER_SINGLE_NP") ofile << "ONESIDED" << endl;
	else if (expsyslist.at(i) == "MET_SoftTrk_ResoPara") ofile << "ONESIDED" << endl;
	else if (expsyslist.at(i) == "MET_SoftTrk_ResoPerp") ofile << "ONESIDED" << endl;
        else ofile << "TWOSIDED" << endl;
	ofile << endl;
    }
    for (int i = 0; i < theosyslist.size(); i++) {
	ofile << "Systematic: \"" << theosyslist.at(i) << "_HFake_Ext\"" << endl;
    	ofile << "  Title: \"" << theosyslist.at(i) << " HFake Ext\"" << endl;
    	ofile << "  NuisanceParameter: \"" << theosyslist.at(i) << "\"" << endl;
    	ofile << "  Type: OVERALL" << endl;
    	ofile << "  OverallUp: " << r_all.at(0).theosys_ups.at(i)/100 << endl;
    	ofile << "  OverallDown: " << r_all.at(0).theosys_dns.at(i)/100 << endl;
    	ofile << "  Samples: hadronfakes" << endl;
    	ofile << "  Symmetrisation: ";
	if (theosyslist.at(i) == "ttbar_sherpa") ofile << "ONESIDED" << endl;
        else ofile << "TWOSIDED" << endl;
	ofile << endl;
    }
}

Raw CombineRaw(vector<Raw> rin) {
    Raw rout;
    for (int i = 0; i < rin.size(); i++) {
	if (i == 0) {
	    rout.channel = rin.at(i).channel;
	    rout.expsys_nom = rin.at(i).expsys_nom;
	    rout.expsys_nom_err = rin.at(i).expsys_nom_err;
	    rout.theosys_nom = rin.at(i).theosys_nom;
	    rout.theosys_nom_err = rin.at(i).theosys_nom_err;
	    rout.expsys_ups = rin.at(i).expsys_ups;
	    rout.expsys_dns = rin.at(i).expsys_dns;
	    rout.theosys_ups = rin.at(i).theosys_ups;
	    rout.theosys_dns = rin.at(i).theosys_dns;
	    rout.theosys_ups_err = rin.at(i).theosys_ups_err;
	    rout.theosys_dns_err = rin.at(i).theosys_dns_err;
	} else {
	    rout.channel += "_"; rout.channel += rin.at(i).channel;
	    rout.expsys_nom += rin.at(i).expsys_nom;
	    rout.expsys_nom_err = sqrt(pow(rout.expsys_nom_err,2) + pow(rin.at(i).expsys_nom_err,2));
	    rout.theosys_nom += rin.at(i).theosys_nom;
	    rout.theosys_nom_err = sqrt(pow(rout.theosys_nom_err,2) + pow(rin.at(i).theosys_nom_err,2));
	    for (int j = 0; j < rin.at(i).expsys_ups.size(); j++) {
		rout.expsys_ups.at(j) += rin.at(i).expsys_ups.at(j);
		rout.expsys_dns.at(j) += rin.at(i).expsys_dns.at(j);
	    }
	    for (int j = 0; j < rin.at(i).theosys_ups.size(); j++) {
		rout.theosys_ups.at(j) += rin.at(i).theosys_ups.at(j);
		rout.theosys_dns.at(j) += rin.at(i).theosys_dns.at(j);
		rout.theosys_ups_err.at(j) = sqrt(pow(rout.theosys_ups_err.at(j),2) + pow(rin.at(i).theosys_ups_err.at(j),2));
		rout.theosys_dns_err.at(j) = sqrt(pow(rout.theosys_dns_err.at(j),2) + pow(rin.at(i).theosys_dns_err.at(j),2));
	    }
	}
    }
    return rout;
}

Raw DivideRaw(Raw num, Raw denom) {
    Raw rout;
    rout.channel = num.channel;
    rout.channel += "_over_"; rout.channel += denom.channel;
    rout.expsys_nom = num.expsys_nom/denom.expsys_nom;
    rout.expsys_nom_err = rout.expsys_nom*sqrt(pow(num.expsys_nom_err/num.expsys_nom,2) + pow(denom.expsys_nom_err/denom.expsys_nom,2));
    rout.theosys_nom = num.theosys_nom/denom.theosys_nom;
    rout.theosys_nom_err = rout.theosys_nom*sqrt(pow(num.theosys_nom_err/num.theosys_nom,2) + pow(denom.theosys_nom_err/denom.theosys_nom,2));
    for (int j = 0; j < num.expsys_ups.size(); j++) {
        rout.expsys_ups.push_back(num.expsys_ups.at(j)/denom.expsys_ups.at(j));
        rout.expsys_dns.push_back(num.expsys_dns.at(j)/denom.expsys_dns.at(j));
    }
    for (int j = 0; j < num.theosys_ups.size(); j++) {
        rout.theosys_ups.push_back(num.theosys_ups.at(j)/denom.theosys_ups.at(j));
        rout.theosys_dns.push_back(num.theosys_dns.at(j)/denom.theosys_dns.at(j));
        rout.theosys_ups_err.push_back(rout.theosys_ups.back()*sqrt(pow(num.theosys_ups_err.at(j)/num.theosys_ups.at(j),2) + pow(denom.theosys_ups_err.at(j)/denom.theosys_ups.at(j),2)));
        rout.theosys_dns_err.push_back(rout.theosys_dns.back()*sqrt(pow(num.theosys_dns_err.at(j)/num.theosys_dns.at(j),2) + pow(denom.theosys_dns_err.at(j)/denom.theosys_dns.at(j),2)));
    }
    return rout;
}

void ConvertSysNames(vector<string> &syslist, bool exp) {
    for (int i = 0; i < syslist.size(); i++) {
	if (exp) {
	    string sys_name_tmp = replace_str(syslist.at(i).c_str(), "__1", "");
	    if (sys_name_tmp == "PU") syslist.at(i) = "pileup";
	    else if (sys_name_tmp == "EL_Trigger_") syslist.at(i) = "leptonSF_EL_SF_Trigger";
	    else if (sys_name_tmp == "EL_Reco_") syslist.at(i) = "leptonSF_EL_SF_Reco";
	    else if (sys_name_tmp == "EL_ID_") syslist.at(i) = "leptonSF_EL_SF_ID";
	    else if (sys_name_tmp == "EL_Isol_") syslist.at(i) = "leptonSF_EL_SF_Isol";
	    else if (sys_name_tmp == "MU_Trigger_STAT_") syslist.at(i) = "leptonSF_MU_SF_Trigger_STAT";
	    else if (sys_name_tmp == "MU_Trigger_SYST_") syslist.at(i) = "leptonSF_MU_SF_Trigger_SYST";
	    else if (sys_name_tmp == "MU_ID_STAT_") syslist.at(i) = "leptonSF_MU_SF_ID_STAT";
	    else if (sys_name_tmp == "MU_ID_SYST_") syslist.at(i) = "leptonSF_MU_SF_ID_SYST";
	    else if (sys_name_tmp == "MU_ID_STAT_LOWPT_") syslist.at(i) = "leptonSF_MU_SF_ID_STAT_LOWPT";
	    else if (sys_name_tmp == "MU_ID_SYST_LOWPT_") syslist.at(i) = "leptonSF_MU_SF_ID_SYST_LOWPT";
	    else if (sys_name_tmp == "MU_Isol_STAT_") syslist.at(i) = "leptonSF_MU_SF_Isol_STAT";
	    else if (sys_name_tmp == "MU_Isol_SYST_") syslist.at(i) = "leptonSF_MU_SF_Isol_SYST";
	    else if (sys_name_tmp == "MU_TTVA_STAT_") syslist.at(i) = "leptonSF_MU_SF_TTVA_STAT";
	    else if (sys_name_tmp == "MU_TTVA_SYST_") syslist.at(i) = "leptonSF_MU_SF_TTVA_SYST";
	    else if (sys_name_tmp == "PH_ID_") syslist.at(i) = "ph_SF_eff";
	    else if (sys_name_tmp == "PH_Isol_") syslist.at(i) = "ph_SF_lowiso";
	    else if (sys_name_tmp == "PH_TrkIsol_") syslist.at(i) = "ph_SF_trkiso";
	    else if (sys_name_tmp == "JVT_") syslist.at(i) = "jvt";
	    else if (sys_name_tmp == "BTagCExt") syslist.at(i) = "bTagSF_Continuous_extrapolation_from_charm";
	    else if (sys_name_tmp == "BTagB0") syslist.at(i) = "bTagSF_Continuous_eigenvars_B0";
	    else if (sys_name_tmp == "BTagB1") syslist.at(i) = "bTagSF_Continuous_eigenvars_B1";
	    else if (sys_name_tmp == "BTagB2") syslist.at(i) = "bTagSF_Continuous_eigenvars_B2";
	    else if (sys_name_tmp == "BTagB3") syslist.at(i) = "bTagSF_Continuous_eigenvars_B3";
	    else if (sys_name_tmp == "BTagB4") syslist.at(i) = "bTagSF_Continuous_eigenvars_B4";
	    else if (sys_name_tmp == "BTagB5") syslist.at(i) = "bTagSF_Continuous_eigenvars_B5";
	    else if (sys_name_tmp == "BTagB6") syslist.at(i) = "bTagSF_Continuous_eigenvars_B6";
	    else if (sys_name_tmp == "BTagB7") syslist.at(i) = "bTagSF_Continuous_eigenvars_B7";
	    else if (sys_name_tmp == "BTagB8") syslist.at(i) = "bTagSF_Continuous_eigenvars_B8";
	    else if (sys_name_tmp == "BTagB9") syslist.at(i) = "bTagSF_Continuous_eigenvars_B9";
	    else if (sys_name_tmp == "BTagB10") syslist.at(i) = "bTagSF_Continuous_eigenvars_B10";
	    else if (sys_name_tmp == "BTagC0") syslist.at(i) = "bTagSF_Continuous_eigenvars_C0";
	    else if (sys_name_tmp == "BTagC1") syslist.at(i) = "bTagSF_Continuous_eigenvars_C1";
	    else if (sys_name_tmp == "BTagC2") syslist.at(i) = "bTagSF_Continuous_eigenvars_C2";
	    else if (sys_name_tmp == "BTagC3") syslist.at(i) = "bTagSF_Continuous_eigenvars_C3";
	    else if (sys_name_tmp == "BTagC4") syslist.at(i) = "bTagSF_Continuous_eigenvars_C4";
	    else if (sys_name_tmp == "BTagC5") syslist.at(i) = "bTagSF_Continuous_eigenvars_C5";
	    else if (sys_name_tmp == "BTagC6") syslist.at(i) = "bTagSF_Continuous_eigenvars_C6";
	    else if (sys_name_tmp == "BTagC7") syslist.at(i) = "bTagSF_Continuous_eigenvars_C7";
	    else if (sys_name_tmp == "BTagC8") syslist.at(i) = "bTagSF_Continuous_eigenvars_C8";
	    else if (sys_name_tmp == "BTagC9") syslist.at(i) = "bTagSF_Continuous_eigenvars_C9";
	    else if (sys_name_tmp == "BTagC10") syslist.at(i) = "bTagSF_Continuous_eigenvars_C10";
	    else if (sys_name_tmp == "BTagL0") syslist.at(i) = "bTagSF_Continuous_eigenvars_Light0";
	    else if (sys_name_tmp == "BTagL1") syslist.at(i) = "bTagSF_Continuous_eigenvars_Light1";
	    else if (sys_name_tmp == "BTagL2") syslist.at(i) = "bTagSF_Continuous_eigenvars_Light2";
	    else if (sys_name_tmp == "BTagL3") syslist.at(i) = "bTagSF_Continuous_eigenvars_Light3";
	    else if (sys_name_tmp == "BTagL4") syslist.at(i) = "bTagSF_Continuous_eigenvars_Light4";
	    else if (sys_name_tmp == "BTagL5") syslist.at(i) = "bTagSF_Continuous_eigenvars_Light5";
	    else if (sys_name_tmp == "BTagL6") syslist.at(i) = "bTagSF_Continuous_eigenvars_Light6";
	    else if (sys_name_tmp == "BTagL7") syslist.at(i) = "bTagSF_Continuous_eigenvars_Light7";
	    else if (sys_name_tmp == "BTagL8") syslist.at(i) = "bTagSF_Continuous_eigenvars_Light8";
	    else if (sys_name_tmp == "BTagL9") syslist.at(i) = "bTagSF_Continuous_eigenvars_Light9";
	    else if (sys_name_tmp == "BTagL10") syslist.at(i) = "bTagSF_Continuous_eigenvars_Light10";
	    else {
		syslist.at(i) = sys_name_tmp;
	    }
	} else {
	    if (syslist.at(i) == "TTBarAFSherpa") syslist.at(i) = "ttbar_sherpa";
	    else if (syslist.at(i) == "TTBarAFQCD") syslist.at(i) = "ttbar_A14tunes";
	}
    }
}
