#include <iostream>
#include <TLine.h>
#include <TLatex.h>
#include <TLegend.h>
#include <TStyle.h>
#include <TPad.h>
#include <TMathText.h>
#include "AtlasStyle.h"
#include <TFile.h>
#include <TH1.h>
#include <iomanip>
#include <TH2.h>
#include <string>
#include <sstream>
#include <algorithm>
#include <TCanvas.h>
#include <iterator>
#include <fstream>
#include <vector>

using namespace std;
string channel = "ljets";
double StatErr(double A, double eA, double B, double eB) {
    double BmA = B-A;
    double eBmA = sqrt(pow(eB,2)-pow(eA,2));
    double dfdA = BmA/pow(A+BmA,2);
    double dfdBmA = A/pow(A+BmA,2);
    double err = sqrt(pow(dfdA*eA,2)+pow(dfdBmA*eBmA,2));
}

int main(int argc, char * argv[]) {

    for (int i = 0; i < argc; i++) {
	if (!strcmp(argv[i],"--Channel")) {
	        channel = argv[i+1];
        }
    }

    SetAtlasStyle();

    TFile* f= new TFile("results/Nominal/Sig_Sys_Upgrade.root");

    string channellatex = "single lepton channel";
    string channelname2 = "sinlepton";
    if (channel == "ll") {
	channellatex = "dilepton channel";
	channelname2 = "dilepton";
    }

    vector<string> vars;
    vector<string> vartitles;
    vector<string> varlatexs;
    
    vars.push_back("ph_pt");
    vars.push_back("ph_abseta");
    vars.push_back("dR_ph_lep");
    vartitles.push_back("p_T(#gamma) [GeV]");
    vartitles.push_back("|#eta|(#gamma)");
    vartitles.push_back("#DeltaR(lepton,#gamma)");
    varlatexs.push_back("photon $p_T$");
    varlatexs.push_back("photon $|\\eta|$");
    varlatexs.push_back("$\\Delta R(\\text{lepton},\\gamma)$");
    if (channel == "ll") {
	vars.push_back("dphi_ll");
	vars.push_back("deta_ll");
	vartitles.back() = "min #DeltaR(lepton,#gamma)";
	vartitles.push_back("#Delta#Phi(lepton,lepton)");
	vartitles.push_back("#Delta#eta(lepton,lepton)");
	varlatexs.back() = ("min $\\Delta R(\\text{lepton},\\gamma)$");
	varlatexs.push_back("$\\Delta\\Phi(\\text{lepton},\\text{lepton})$");
	varlatexs.push_back("$\\Delta\\eta(\\text{lepton},\\text{lepton})$");
    }

    {
	string dRphname = "dR_truth_reco_photon_" + channel;
	string dRlepname = "dR_truth_reco_lepton_" + channel;
	TH1F* h_dRph = (TH1F*)f->Get(dRphname.c_str());
    	TH1F* h_dRlep = (TH1F*)f->Get(dRlepname.c_str());
	h_dRph->SetBinContent(h_dRph->GetNbinsX(), h_dRph->GetBinContent(h_dRph->GetNbinsX())+h_dRph->GetBinContent(h_dRph->GetNbinsX()+1));
	h_dRlep->SetBinContent(h_dRlep->GetNbinsX(), h_dRlep->GetBinContent(h_dRlep->GetNbinsX())+h_dRlep->GetBinContent(h_dRlep->GetNbinsX()+1));
    	h_dRph->Scale(1./h_dRph->Integral());
    	h_dRlep->Scale(1./h_dRlep->Integral());
	h_dRph->SetLineColor(kBlack);
	h_dRlep->SetLineColor(kRed);
	
	TCanvas *c = new TCanvas("c","c",800,800);
	c->SetRightMargin(0.05);
	c->SetLogy(true);
	
	h_dRlep->GetXaxis()->SetTitle("Obj. #DeltaR(reco,truth)");
	h_dRlep->GetYaxis()->SetTitle("Arbi. unit");
	h_dRlep->GetYaxis()->SetRangeUser(1e-6, 100);

	h_dRlep->Draw("hist");
	h_dRph->Draw("hist same");
	
	TLatex lt; 
	lt.SetNDC();
	lt.SetTextFont(72);
	lt.SetTextSize(0.04);
	lt.DrawLatex(0.20, 0.88, "ATLAS");
	lt.SetTextFont(42);
	lt.DrawLatex(0.36, 0.88, "Internal Simulation");
	lt.DrawLatex(0.20, 0.82, "#sqrt{s} = 13 TeV");
	if (channel == "ljets") {
	    lt.DrawLatex(0.20, 0.76, "single lepton channel");
	} else {
	    lt.DrawLatex(0.20, 0.76, "dilepton channel");
	}
	
	TLegend *lg = new TLegend(0.20, 0.65, 0.80, 0.75);
	lg->SetNColumns(2);
	lg->SetTextSize(0.04);
	lg->SetTextFont(42);
	lg->SetBorderSize(0);
	lg->SetFillColor(0);
	lg->AddEntry(h_dRph, "Photon", "l");
	lg->AddEntry(h_dRlep, "Lepton", "l");
	lg->Draw("same");
	
	string savename = "plots/unfolding/MatchDr_";
	savename += channel;
	savename += ".pdf";
	
	c->SaveAs(savename.c_str());
    }

    vector<double> effs;
    vector<double> match_effs;
    vector<double> migs;
    vector<double> match_migs;
    vector<double> err_effs;
    vector<double> err_match_effs;
    vector<double> err_migs;
    vector<double> err_match_migs;
	
    for (int iv = 0; iv < vars.size(); iv++) {
	string var = vars.at(iv);
	string vartitle = vartitles.at(iv);
	string varlatex = varlatexs.at(iv);

	double tmp_eff = 0;
	double tmp_match_eff = 0;
	double tmp_mig = 0;
	double tmp_match_mig = 0;
	double tmp_err_eff = 0;
	double tmp_err_match_eff = 0;
	double tmp_err_mig = 0;
	double tmp_err_match_mig = 0;

	string reconame = "h_"+var+"Nominal_"+channel+"_reco";
    	string truthname = "h_"+var+"Nominal_"+channel+"_truth";
    	string fidname = "h_"+var+"Nominal_"+channel+"_reco_fid";
    	string fidmatchname = "h_"+var+"Nominal_"+channel+"_reco_fid_match";
    	string failfidmatchname = "h_"+var+"Nominal_"+channel+"_reco_failfid_match";
	string migname1 = "mig_"+var+"_"+channelname2+"_weight0";
	string migname2 = "mig_"+var+"_"+channelname2+"_nomatch";

    	TH1F* h_reco = (TH1F*)f->Get(reconame.c_str());
    	TH1F* h_truth = (TH1F*)f->Get(truthname.c_str());
    	TH1F* h_fid = (TH1F*)f->Get(fidname.c_str());
    	TH1F* h_fid_match = (TH1F*)f->Get(fidmatchname.c_str());
    	TH1F* h_failfid_match = (TH1F*)f->Get(failfidmatchname.c_str());

    	TH1F* h_reco_eff = (TH1F*)h_fid_match->Clone();
    	TH1F* h_match_eff = (TH1F*) h_fid_match->Clone();
    	TH1F* h_mig = (TH1F*)h_fid_match->Clone();
	TH1F* h_mig_abs = (TH1F*)h_reco->Clone();
	TH1F* h_match_mig = (TH1F*)h_failfid_match->Clone();

    	h_reco_eff->Divide(h_truth);
    	h_match_eff->Divide(h_fid);
    	h_mig->Divide(h_reco);
	h_mig_abs->Add(h_fid,-1);
	for (int i = 1; i <= h_reco_eff->GetNbinsX(); i++) {
	    h_mig_abs->SetBinError(i, sqrt(pow(h_reco->GetBinError(i),2) - pow(h_fid->GetBinError(i),2)));
	}
	h_match_mig->Divide(h_mig_abs);

	double total_fid_match = 0;
	double total_truth = 0;
	double total_fid = 0;
	double total_reco = 0;
	double total_failfid_match = 0;
	double total_mig_abs = 0;
	double err_total_fid_match = 0;
	double err_total_truth = 0;
	double err_total_fid = 0;
	double err_total_reco = 0;
	double err_total_failfid_match = 0;
	double err_total_mig_abs = 0;
	for (int i = 1; i <= h_reco_eff->GetNbinsX(); i++) {
	    h_reco_eff->SetBinError(i, StatErr(h_fid_match->GetBinContent(i), h_fid_match->GetBinError(i), h_truth->GetBinContent(i), h_truth->GetBinError(i)));
	    h_match_eff->SetBinError(i, StatErr(h_fid_match->GetBinContent(i), h_fid_match->GetBinError(i), h_fid->GetBinContent(i), h_fid->GetBinError(i)));
	    h_mig->SetBinError(i, StatErr(h_fid_match->GetBinContent(i), h_fid_match->GetBinError(i), h_reco->GetBinContent(i), h_reco->GetBinError(i)));
	    h_match_mig->SetBinError(i, StatErr(h_failfid_match->GetBinContent(i), h_failfid_match->GetBinError(i), h_mig_abs->GetBinContent(i), h_mig_abs->GetBinError(i)));
	    
	    total_fid_match += h_fid_match->GetBinContent(i);
	    total_truth += h_truth->GetBinContent(i);
	    total_fid += h_fid->GetBinContent(i);
	    total_reco += h_reco->GetBinContent(i);
	    total_failfid_match += h_failfid_match->GetBinContent(i);
	    total_mig_abs += h_mig_abs->GetBinContent(i);

	    err_total_fid_match += pow(h_fid_match->GetBinContent(i), 2);
	    err_total_truth += pow(h_truth->GetBinContent(i), 2);
	    err_total_fid += pow(h_fid->GetBinContent(i), 2);
	    err_total_reco += pow(h_reco->GetBinContent(i), 2);
	    err_total_failfid_match += pow(h_failfid_match->GetBinContent(i), 2);
	    err_total_mig_abs += pow(h_mig_abs->GetBinContent(i), 2);
	}
	err_total_fid_match = sqrt(err_total_fid_match);
	err_total_truth = sqrt(err_total_truth);
	err_total_reco = sqrt(err_total_reco);
	err_total_fid = sqrt(err_total_fid);
	err_total_failfid_match = sqrt(err_total_failfid_match);
	err_total_mig_abs = sqrt(err_total_mig_abs);

	tmp_eff = total_fid_match/total_truth;
	tmp_match_eff = total_fid_match/total_fid;
	tmp_mig = total_fid_match/total_reco;
	tmp_match_mig = total_failfid_match/total_mig_abs;

	tmp_err_eff = StatErr(total_fid_match, err_total_fid_match, total_truth, err_total_truth);
	tmp_err_match_eff = StatErr(total_fid_match, err_total_fid_match, total_fid, err_total_fid);
	tmp_err_mig = StatErr(total_fid_match, err_total_fid_match, total_reco, err_total_reco);
	tmp_err_match_mig = StatErr(total_failfid_match, err_total_failfid_match, total_mig_abs, err_total_mig_abs);

	effs.push_back(tmp_eff);
	match_effs.push_back(tmp_match_eff);
	migs.push_back(tmp_mig);
	match_migs.push_back(tmp_match_mig);
	err_effs.push_back(tmp_err_eff);
	err_match_effs.push_back(tmp_err_match_eff);
	err_migs.push_back(tmp_err_mig);
	err_match_migs.push_back(tmp_err_match_mig);

	{
	   h_reco_eff->SetLineColor(kBlack);
    	   h_match_eff->SetLineColor(kRed);

    	   h_match_eff->GetYaxis()->SetRangeUser(0,1.8);

    	   TCanvas *c = new TCanvas("c","c",800,800);
    	   c->SetRightMargin(0.05);

    	   h_match_eff->GetYaxis()->SetTitle("Efficiency");
    	   h_match_eff->GetXaxis()->SetTitle(vartitle.c_str());

    	   h_match_eff->Draw();
    	   h_reco_eff->Draw("same");

    	   TLatex lt; 
    	   lt.SetNDC();
    	   lt.SetTextFont(72);
    	   lt.SetTextSize(0.04);
    	   lt.DrawLatex(0.20, 0.88, "ATLAS");
    	   lt.SetTextFont(42);
    	   lt.DrawLatex(0.36, 0.88, "Internal Simulation");
    	   lt.DrawLatex(0.20, 0.82, "#sqrt{s} = 13 TeV");
    	   if (channel == "ljets") {
    	       lt.DrawLatex(0.20, 0.76, "single lepton channel");
    	   } else {
    	       lt.DrawLatex(0.20, 0.76, "dilepton channel");
    	   }

    	   TLegend *lg = new TLegend(0.20, 0.65, 0.80, 0.75);
    	   lg->SetNColumns(2);
    	   lg->SetTextSize(0.04);
    	   lg->SetTextFont(42);
    	   lg->SetBorderSize(0);
    	   lg->SetFillColor(0);
    	   lg->AddEntry(h_match_eff, "Match. prob.");
    	   lg->AddEntry(h_reco_eff, "Efficiency");
    	   lg->Draw("same");

    	   string savename = "plots/unfolding/Eff_";
    	   savename += var + "_" + channel;
    	   savename += ".pdf";

    	   c->SaveAs(savename.c_str());
	}

	{
	   h_mig->SetLineColor(kBlack);
    	   h_match_mig->SetLineColor(kRed);

    	   h_match_mig->GetYaxis()->SetRangeUser(0,1.8);

    	   TCanvas *c = new TCanvas("c","c",800,800);
    	   c->SetRightMargin(0.05);

    	   h_match_mig->GetYaxis()->SetTitle("Migration");
    	   h_match_mig->GetXaxis()->SetTitle(vartitle.c_str());

    	   h_match_mig->Draw();
    	   h_mig->Draw("same");

    	   TLatex lt; 
    	   lt.SetNDC();
    	   lt.SetTextFont(72);
    	   lt.SetTextSize(0.04);
    	   lt.DrawLatex(0.20, 0.88, "ATLAS");
    	   lt.SetTextFont(42);
    	   lt.DrawLatex(0.36, 0.88, "Internal Simulation");
    	   lt.DrawLatex(0.20, 0.82, "#sqrt{s} = 13 TeV");
    	   if (channel == "ljets") {
    	       lt.DrawLatex(0.20, 0.76, "single lepton channel");
    	   } else {
    	       lt.DrawLatex(0.20, 0.76, "dilepton channel");
    	   }

    	   TLegend *lg = new TLegend(0.20, 0.65, 0.80, 0.75);
    	   lg->SetNColumns(2);
    	   lg->SetTextSize(0.04);
    	   lg->SetTextFont(42);
    	   lg->SetBorderSize(0);
    	   lg->SetFillColor(0);
    	   lg->AddEntry(h_match_mig, "Match. prob.");
    	   lg->AddEntry(h_mig, "1 - f_{mig}");
    	   lg->Draw("same");

    	   string savename = "plots/unfolding/Mig_";
    	   savename += var + "_" + channel;
    	   savename += ".pdf";

    	   c->SaveAs(savename.c_str());
	}

    	cout << "\\begin{table}[h]" << endl;
    	cout << "\\centering" << endl;
	cout << "\\scalebox{0.8}{" << endl;
    	cout << "\\begin{tabular}{ c|c|";
    	for (int i = 1; i <= h_reco_eff->GetNbinsX(); i++) {
    	    cout << "c";
    	    if (i != h_reco_eff->GetNbinsX()) {
    	        cout << " | ";
    	    } else cout << "}" << endl;
    	}
    	cout << "\\hline" << endl;
    	cout << varlatex << "& Overall & ";
    	for (int i = 1; i <= h_reco_eff->GetNbinsX(); i++) {
    	    cout << "Bin " << i;
    	    if (i != h_reco_eff->GetNbinsX()) {
    	        cout << " & ";
    	    } else cout << " \\\\" << endl;
    	}
    	cout << "\\hline" << endl;
    	cout << setw(13) << "Efficiency & ";
    	cout << fixed << setprecision(2) << tmp_eff << " $\\pm$ " << tmp_err_eff << " &";
    	for (int i = 1; i <= h_reco_eff->GetNbinsX(); i++) {
    	    cout << fixed << setprecision(2) << h_reco_eff->GetBinContent(i) << " $\\pm$ " << h_reco_eff->GetBinError(i);
    	    if (i != h_reco_eff->GetNbinsX()) {
    	        cout << " & ";
    	    } else cout << " \\\\" << endl;
    	}
    	cout << setw(13) << "Match. prob. & ";
    	cout << fixed << setprecision(2) << tmp_match_eff << " $\\pm$ " << tmp_err_match_eff << " &";
    	for (int i = 1; i <= h_reco_eff->GetNbinsX(); i++) {
    	    cout << fixed << setprecision(2) << h_match_eff->GetBinContent(i) << " $\\pm$ " << h_match_eff->GetBinError(i);
    	    if (i != h_reco_eff->GetNbinsX()) {
    	        cout << " & ";
    	    } else cout << " \\\\" << endl;
    	}
	cout << "\\hline" << endl;
    	cout << setw(13) << "1 - $f_{\\text{mig}}$ & ";
    	cout << fixed << setprecision(2) << tmp_mig << " $\\pm$ " << tmp_err_mig << " &";
    	for (int i = 1; i <= h_reco_eff->GetNbinsX(); i++) {
    	    cout << fixed << setprecision(2) << h_mig->GetBinContent(i) << " $\\pm$ " << h_mig->GetBinError(i);
    	    if (i != h_reco_eff->GetNbinsX()) {
    	        cout << " & ";
    	    } else cout << " \\\\" << endl;
    	}
    	cout << setw(13) << "Match. prob. & ";
    	cout << fixed << setprecision(2) << tmp_match_mig << " $\\pm$ " << tmp_err_match_mig << " &";
    	for (int i = 1; i <= h_reco_eff->GetNbinsX(); i++) {
    	    cout << fixed << setprecision(2) << h_mig->GetBinContent(i) << " $\\pm$ " << h_mig->GetBinError(i);
    	    if (i != h_reco_eff->GetNbinsX()) {
    	        cout << " & ";
    	    } else cout << " \\\\" << endl;
    	}
    	cout << "\\hline" << endl;
    	cout << "\\end{tabular}}"<<endl;
    	cout << "\\label{tab:diff_eff_" << var << "_" << channel;
    	cout << "}" << endl;
    	cout << "\\caption{The numerical values of the efficiency and one minus the fraction of outside migration (1-$f_{\\text{mig}}$)" << endl;
	cout << "for the " << varlatex << " of the " << channellatex << ".";
	cout << "The object-matching probabilities defined in the text are also shown.}";
	cout << endl;
    	cout << "\\end{table}" <<endl;
    }
}
