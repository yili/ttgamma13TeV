#include <TH1.h>
#include <TFile.h>
#include <iostream>

#include "StringPlayer.h"
#include "PlotComparor.h"
#include "ConfigReader.h"
#include "Logger.h"
#include "AtlasStyle.h"
#include "StringPlayer.h"

using namespace std;
string hfakereweight = "HFakeReweighted_";
bool showscale = false;

string GetTitle(string in) {
    string out;
    out = in;
    if (in == "ejets") out = "e+jets";
    if (in == "mujets") out = "#mu+jets";
    if (in == "emu") out = "e#mu";
    if (in == "mumu") out = "#mu#mu";
    return out;
}

int main(int argc, char * argv[]) {

    SetAtlasStyle();

    Logger *lg = new Logger("MAIN");
    lg->Info("s", "Hi, this is the main programm~");
    lg->NewLine();

    string filename;
    if (argc < 2) {
	lg->Info("s", "Will use default config file --> ../config/13TeV_draw_stack.cfg");
	filename = "../config/13TeV_draw_stack.cfg" ;
    } else {
	filename = argv[1];
    }

    for (int i = 0; i < argc; i++) {
	if (!strcmp(argv[i],"--NoHFweight")) {
	    hfakereweight = "";
   	}
	if (!strcmp(argv[i],"--HFweight025")) {
	    hfakereweight = "HFakeReweighted025_";
   	}
	if (!strcmp(argv[i],"--HFweight05")) {
	    hfakereweight = "HFakeReweighted05_";
   	}
	if (!strcmp(argv[i],"--HFweight075")) {
	    hfakereweight = "HFakeReweighted075_";
   	}
    }
     
    ConfigReader* rd = new ConfigReader(filename, '_', false);
    rd->Init();

    bool Print, Reverse, Debug, UseLogy, Simulation, HasData, CSquare, SaveRatio, OrderBin, MCtoData, Upgrade, NormBinWidth;
    rd->FillValueB("_Options", "Print", Print);
    rd->FillValueB("_Options", "Reverse", Reverse);
    rd->FillValueB("_Options", "UseLogy", UseLogy);
    rd->FillValueB("_Options", "Debug", Debug);
    rd->FillValueB("_Options", "Simulation", Simulation);
    rd->FillValueB("_Options", "HasData", HasData);
    rd->FillValueB("_Options", "CSquare", CSquare);
    rd->FillValueB("_Options", "SaveRatio", SaveRatio);
    rd->FillValueB("_Options", "OrderBin", OrderBin);
    rd->FillValueB("_Options", "MCtoData", MCtoData);
    rd->FillValueB("_Options", "Upgrade", Upgrade);
    rd->FillValueB("_Options", "NormBinWidth", NormBinWidth);

    if (Debug) cout << "tag1" << endl;

    string CMS = rd->GetValue("_CMS");
    string Variation = rd->GetValue("_Variation");
    string Cut = rd->GetValue("_Cut");
    string Region = rd->GetValue("_Region");
    string SubRegion = rd->GetValue("_SubRegion");
    string Channel = rd->GetValue("_Channel");
    string Tag = rd->GetValue("_Tag");
    double Lumi = rd->GetValueF("_Lumi");
    string YRangeRatio = rd->GetValue("_YRangeRatio");
    string LegendNC = rd->GetValue("_LegendNC");
    double LogyMin = rd->GetValueF("_LogyMin");
    double LogyScale = rd->GetValueF("_LogyScale");
    double LGoff = rd->GetValueF("_LGoff");
    string Yoffset = rd->GetValue("_Yoffset");
    double RatioUp = rd->GetValueF("_RatioUp");
    double RatioDn = rd->GetValueF("_RatioDn");
    string inputdir = rd->GetValue("_InputDir");
    string startcolor = rd->GetValue("_StartColor");
    string DataFile = rd->GetValue("_DataFile");
    vector<string> vars = rd->GetValueAll("_Var");
    if (Debug) cout << "tag2" << endl;
    vector<vector<string> > Stacks = rd->GetAmbiValueAll("_Stack");
    int n_proc = Stacks.size();
    vector<string> StackNames;
    vector<vector<string > > StackProcs;
    vector<vector<string > > StackInputs;
    vector<string> StackPhMatchs;
    vector<string> StackLpMatchs;
    vector<string> StackScales;
    vector<string > StackUncerts;
    vector<string > StackColors;
    vector<int> StackMerge;
    if (Debug) cout << "tag3" << endl;
    for (int i = 0; i < n_proc; i++) {
	if (Debug) cout << "tag4" << endl;
        StackNames.push_back(replaceChar(Stacks.at(i).at(0), '_', ' '));
	if (Debug) cout << "tag5" << endl;
	StackInputs.push_back(SplitToVector(Stacks.at(i).at(1)));
	if (Debug) cout << "tag6" << endl;
	StackPhMatchs.push_back(Stacks.at(i).at(2));
	if (Debug) cout << "tag7" << endl;
	StackLpMatchs.push_back(Stacks.at(i).at(3));
	if (Debug) cout << "tag8" << endl;
	StackScales.push_back(Stacks.at(i).at(4));
	if (Debug) cout << "tag9" << endl;
        StackUncerts.push_back(Stacks.at(i).at(5));
	if (Debug) cout << "tag10" << endl;
        StackColors.push_back(Stacks.at(i).at(6));
	if (Debug) cout << "tag11" << endl;
        StackMerge.push_back(atoi(Stacks.at(i).at(7).c_str()));
	if (Debug) cout << "tag12" << endl;
    }

    if (Debug) cout << "tag5" << endl;
    string MCType = "Reco";
    if (CMS == "14TeV") MCType = "Upgrade";
    if (Upgrade) MCType = "Upgrade";
    if (Debug) cout << "tag6" << endl;

    vector<vector<string> > Merges = rd->GetAmbiValueAll("_MergeHist");
    int n_merged = Merges.size();
    vector<int> MergeCodes;
    vector<string > MergeNames;
    vector<int> MergeColors;
    if (Debug) cout << "tag7" << endl;
    for (int i = 0; i < n_merged; i++) {
        MergeCodes.push_back(atoi(Merges.at(i).at(0).c_str()));
        MergeNames.push_back(replaceChar(Merges.at(i).at(1), '_', ' '));
        MergeColors.push_back(atoi(Merges.at(i).at(2).c_str()));
    }

    if (Debug) cout << "tag8" << endl;
    string HFakeScale = "";
    for (int i = 0; i < argc; i++) {
	if (!strcmp(argv[i],"--Cut")) {
	    Cut = argv[i+1];
   	}
	if (!strcmp(argv[i],"--SubRegion")) {
	    SubRegion = argv[i+1];
   	}
	if (!strcmp(argv[i],"--Tag")) {
	    Tag = argv[i+1];
   	}
	if (!strcmp(argv[i],"--HFakeScale")) {
	    HFakeScale = argv[i+1];
   	}
    }

    if (Debug) cout << "tag2" << endl;

    PlotComparor* PC = new PlotComparor();
    PC->SetSaveDir("plots/");
    PC->DrawRatio(true);
    PC->FixRatioRange(true);
    PC->SetMaxRatio(RatioUp);
    PC->SetMinRatio(RatioDn);
    if (CMS == "13TeV") PC->Is13TeV(true);
    if (CMS == "14TeV") PC->Is14TeV(true);
    PC->DataLumi(Lumi);
    PC->IsSimulation(Simulation);
    PC->ReverseStackOrder(Reverse);
    if (startcolor != "") {
	PC->SetStartColor(atoi(startcolor.c_str()));
    }
    PC->NormMCtoData(MCtoData);

    bool NoData = false;
    for (int ivr = 0; ivr < vars.size(); ivr++) {
        string var = vars.at(ivr);
    
        TFile* f_data = NULL;
	string fname ;
        if (DataFile != "") {
	    HasData = true;
	    fname = inputdir + CMS + "_" + Cut + "_Data_" + Region + "_" + SubRegion + "_" + Variation + "_" + Tag + ".root";
	    f_data = new TFile(fname.c_str());
        }
        TH1F* h_data = NULL;
        if (!f_data) {
	   cout << "Can't open data file " << fname << endl;
	   cout << "Will only show the MC stack " << endl;
	   NoData = true;
	   PC->DrawRatio(false);
        } else {
	    int n1 = fname.find("_Nominal", 0);
	    string namekey = fname.substr(inputdir.size(), n1-inputdir.size()+1);
	    string hname = var + "_"; hname += namekey; hname += "Nominal";
            if (f_data) {
	        h_data = (TH1F*)f_data->Get(hname.c_str());
            }
	}
	PC->HasData(HasData);
        PC->ClearAlterHs();
        PC->ClearAlterColors();
        PC->SetBaseH(h_data);
        PC->SetBaseHName("Data");
        PC->SetBaseColor(1);
	if (Yoffset != "") PC->SetYtitleOffset(atof(Yoffset.c_str()));
    
        vector<TH1F*> h_merges;
        for (int im = 0; im < n_merged; im++) {
	    TH1F* h_merged = NULL;
	    h_merges.push_back(h_merged);
        }
    
        double total = 0;
	vector<TFile*> tmpfs;
        for (int i = 0; i < n_proc; i++) {
            TH1F* h_stack_i = NULL;
            for (int j = 0; j < StackInputs.at(i).size(); j++) {
		string fname;
		if (StackInputs.at(i).at(j) != "QCD") {
		    if (MCType == "Upgrade" && StackInputs.at(i).at(j).find("ZGammajetsElEl",0) != string::npos) {
			fname = inputdir + "13TeV_CutSR_PhMatch_TruePh_ZGammajetsElElNLO_Reco_CR1_" + SubRegion + "_ZGammaReweighted_Nominal_Final06.root";
		    } else if (MCType == "Upgrade" && StackInputs.at(i).at(j).find("ZGammajetsMuMu",0) != string::npos) {
			fname = inputdir + "13TeV_CutSR_PhMatch_TruePh_ZGammajetsMuMuNLO_Reco_CR1_" + SubRegion + "_ZGammaReweighted_Nominal_Final06.root";
		    } else if (MCType == "Upgrade" && StackInputs.at(i).at(j).find("ZGammajetsTauTau",0) != string::npos) {
			fname = inputdir + "13TeV_CutSR_PhMatch_TruePh_ZGammajetsTauTauNLO_Reco_CR1_" + SubRegion + "_ZGammaReweighted_Nominal_Final06.root";
		    } else {
			fname = inputdir + CMS + "_" + Cut + "_";
		    	if (StackPhMatchs.at(i) == "TruePh") fname += "PhMatch_TruePh_";
		    	if (StackPhMatchs.at(i) == "Ancestor1") fname += "PhMatch_Ancestor1_";
		    	if (StackPhMatchs.at(i) == "Ancestor2") fname += "PhMatch_Ancestor2_";
		    	if (StackPhMatchs.at(i) == "Ancestor3") fname += "PhMatch_Ancestor3_";
		    	if (StackPhMatchs.at(i) == "HFake") fname += "PhMatch_HFake_";
		    	if (StackPhMatchs.at(i) == "EFake") fname += "PhMatch_EFake_";
		    	if (StackPhMatchs.at(i) == "EFakeTypeA") fname += "PhMatch_EFakeTypeA_";
		    	if (StackPhMatchs.at(i) == "EFakeTypeB") fname += "PhMatch_EFakeTypeB_";
		    	if (StackPhMatchs.at(i) == "EFakeTypeC") fname += "PhMatch_EFakeTypeC_";
		    	if (StackPhMatchs.at(i) == "EFakeTypeD") fname += "PhMatch_EFakeTypeD_";
		    	if (StackLpMatchs.at(i) == "TrueLp") fname += "LpMatch_TrueLp_";
		    	if (StackLpMatchs.at(i) == "HFake") fname += "LpMatch_HFake_";
		    	fname += StackInputs.at(i).at(j);
			if (MCType == "Upgrade" && StackPhMatchs.at(i) == "HFake") {
			   fname += "_"+MCType+"_" + Region + "_" + SubRegion + "_" + hfakereweight + Variation + "_" + Tag + ".root";
			} else {
			   fname += "_"+MCType+"_" + Region + "_" + SubRegion + "_" + Variation + "_" + Tag + ".root";
			}
		    }
		} else {
		    string Para = StackPhMatchs.at(i);
		    if (MCType == "Upgrade") {
			fname = inputdir + "13TeV_CutSR_QCD_CR1_" + SubRegion + "_" + Para + "_ZGammaReweighted_" + Variation + "_Final06.root";
		    } else {
			fname = inputdir + CMS + "_" + Cut + "_QCD_" + Region + "_" + SubRegion + "_" + Para + "_" + Variation + "_" + Tag + ".root";
		    }
		}
                TFile* f_tmp = new TFile(fname.c_str());
		if (!f_tmp) {
    	    	    cout << "Can't open -> " << fname << endl;
    	    	    exit(-1);
    	    	}
		tmpfs.push_back(f_tmp);

		int n1 = fname.find("_Nominal", 0);
		string namekey = fname.substr(inputdir.size(), n1-inputdir.size()+1);
		string hname = var + "_"; hname += namekey; hname += "Nominal";
                TH1F*h_tmp = (TH1F*)f_tmp->Get(hname.c_str());
		if (!h_tmp) {
    	    	    cout << "Can't get -> " << hname << endl;
    	    	    f_tmp->ls();
    	    	    exit(-1);
    	    	}
		if (StackNames.at(i) == "Hfake") {
		    if (HFakeScale != "") {
			StackScales.at(i) = HFakeScale;
		    }
		}
                h_tmp->Scale(atof(StackScales.at(i).c_str()));
                if (j == 0) {
		    h_stack_i = h_tmp;
		    if (showscale) {
		    if (atof(StackScales.at(i).c_str()) != 1 && StackNames.at(i).find("[x",0) == string::npos) {
	    	        StackNames.at(i) += "[x";
	    	        StackNames.at(i) += StackScales.at(i);
	    	        StackNames.at(i) += "]";
	    	    }
		    }
                } else {
		    h_stack_i->Add(h_tmp);
                }
            }
            double tot_err = 0;
            for (int j = 0; j < h_stack_i->GetNbinsX(); j++) {
                tot_err += pow(h_stack_i->GetBinError(j+1),2);
                //h_stack_i->SetBinError(j+1, sqrt(pow(h_stack_i->GetBinError(j+1),2) + pow(atof(StackUncerts.at(i).c_str())*h_stack_i->GetBinContent(j+1),2)));
                if (atof(StackUncerts.at(i).c_str()) != 0.) h_stack_i->SetBinError(j+1, atof(StackUncerts.at(i).c_str())*h_stack_i->GetBinContent(j+1));
            }
            tot_err = sqrt(tot_err);
            if (atof(StackUncerts.at(i).c_str()) != 0.) tot_err = atof(StackUncerts.at(i).c_str())*h_stack_i->Integral();
            total += h_stack_i->Integral();
    	if (StackMerge.at(i) == 0) {
    	    PC->AddAlterHName(StackNames.at(i));
    	    PC->AddAlterH(h_stack_i);
    	    PC->AddAlterColors(atoi(StackColors.at(i).c_str()));
    	} else {
    	    int tomerge = -1;
    	    for (int im = 0; im < n_merged; im++) {
    		if (MergeCodes.at(im) == StackMerge.at(i)) {
    		    tomerge = im;
    		    break;
    		}
    	    }
    	    if (tomerge == -1) {
    		lg->Err("s", "Can't find merge code for hist ->", StackNames.at(i).c_str());
    		exit(-1);
    	    }
    	    if (!h_merges.at(tomerge)) {
    		h_merges.at(tomerge) = h_stack_i;
    	    } else {
    		h_merges.at(tomerge)->Add(h_stack_i);
    	    }
    	}
            if (Print) cout << StackNames.at(i) << ": " << h_stack_i->Integral() << " " << tot_err << endl;
        }
        for (int im = 0; im < n_merged; im++) {
    	PC->AddAlterHName(MergeNames.at(im));
    	PC->AddAlterH(h_merges.at(im));
    	PC->AddAlterColors(MergeColors.at(im));
        }
    
        if (Print) cout << "Total: " << total << endl;
        if (Print && h_data) cout << "Data: " << h_data->Integral() << endl;
    
        //if (var.find("Eta",0) != string::npos || var.find("PtCone20",0) != string::npos) {
        //    PC->ShiftLegendX2(+0.05);
        //    PC->SetLegendScaleX(1.45);
        //}
        PC->ShiftLegendX(+0.1);
        string savename = CMS + "_Stack_" + var + "_" + Cut + "_" + Region + "_" + SubRegion;
	if (NoData) savename += "_NoData";
        PC->SetSaveName(savename);
        PC->ShiftLegendX(LGoff);
	if (Channel == "") PC->SetChannel(GetTitle(SubRegion));
	else PC->SetChannel(Channel);
	PC->SquareCanvas(CSquare);
	PC->OrderBin(OrderBin);
	PC->SaveRatio(SaveRatio);
	PC->SetYtitle("Events");
        //PC->SetXtitle("p_{T,l} [GeV]");
        //if (var.find("PhPt",0) != string::npos) PC->SetYtitle("Events/5 GeV");
        //if (var.find("PhEta",0) != string::npos) PC->SetYtitle("Events/0.2");
        if (UseLogy) PC->UseLogy(UseLogy, LogyMin, LogyScale);
	if (Upgrade && var.find("LeadPhPt",0) != string::npos) PC->UseLogy(true, 0.01, LogyScale);
	else PC->UseLogy(false, LogyMin, LogyScale);
	if (NormBinWidth) PC->NormBinWidth(true);
	if (YRangeRatio != "") PC->SetYRangeRatio(atof(YRangeRatio.c_str()));
	if (LegendNC != "") PC->SetLegendNC(atoi(LegendNC.c_str()));
	PC->ForcedLGPos(true);
	if (var.find("LeadPhPt",0) != string::npos && Upgrade) PC->SetRightMargin(0.04);
        PC->DrawStack();

	delete f_data;
	for (int i = 0; i < tmpfs.size(); i++) {
	    delete tmpfs.at(i);
	}
    }
}
