#include <TH1.h>
#include <TFile.h>
#include <iostream>

#include "StringPlayer.h"
#include "PlotComparor.h"
#include "ConfigReader.h"
#include "Logger.h"
#include "AtlasStyle.h"
#include "StringPlayer.h"

using namespace std;

int main(int argc, char * argv[]) {

    SetAtlasStyle();

    Logger *lg = new Logger("MAIN");
    lg->Info("s", "Hi, this is the main programm~");
    lg->NewLine();

    string filename = "../config/13TeV_draw_stack_final_basic.cfg" ;
    for (int i = 0; i < argc; i++) {
	if (!strcmp(argv[i],"--Config")) {
	    filename = argv[i+1];
   	}
    }
     
    ConfigReader* rd = new ConfigReader(filename, '_', false);
    rd->Init();

    bool Print, Reverse, Debug, UseLogy, Simulation, HasData, CSquare, SaveRatio, OrderBin, SplitSignal;
    rd->FillValueB("_Options", "Print", Print);
    rd->FillValueB("_Options", "Reverse", Reverse);
    rd->FillValueB("_Options", "UseLogy", UseLogy);
    rd->FillValueB("_Options", "Debug", Debug);
    rd->FillValueB("_Options", "Simulation", Simulation);
    rd->FillValueB("_Options", "HasData", HasData);
    rd->FillValueB("_Options", "CSquare", CSquare);
    rd->FillValueB("_Options", "SaveRatio", SaveRatio);
    rd->FillValueB("_Options", "OrderBin", OrderBin);
    rd->FillValueB("_Options", "SplitSignal", SplitSignal);

    if (Debug) cout << "tag1" << endl;

    string CMS = rd->GetValue("_CMS");
    string Variation = rd->GetValue("_Variation");
    string Cut = rd->GetValue("_Cut");
    string Region = rd->GetValue("_Region");
    string SubRegion = rd->GetValue("_SubRegion");
    string Tag = rd->GetValue("_Tag");
    double Lumi = rd->GetValueF("_Lumi");
    string YRangeRatio = rd->GetValue("_YRangeRatio");
    string LegendNC = rd->GetValue("_LegendNC");
    double LogyMin = rd->GetValueF("_LogyMin");
    double LogyScale = rd->GetValueF("_LogyScale");
    double LGoff = rd->GetValueF("_LGoff");
    double RatioUp = rd->GetValueF("_RatioUp");
    double RatioDn = rd->GetValueF("_RatioDn");
    string inputdir = rd->GetValue("_InputDir");
    int LGNC = rd->GetValueI("_LGNC");
    string startcolor = rd->GetValue("_StartColor");
    vector<string> vars = rd->GetValueAll("_Var");
    bool NoKfactor = false;

    string HFakeScale = "";
    string SignalScale = "";
    bool NoSys = false;
    bool NormMCtoData = false;
    for (int i = 0; i < argc; i++) {
	if (!strcmp(argv[i],"--Cut")) {
	    Cut = argv[i+1];
   	}
	if (!strcmp(argv[i],"--SubRegion")) {
	    SubRegion = argv[i+1];
   	}
	if (!strcmp(argv[i],"--Tag")) {
	    Tag = argv[i+1];
   	}
	if (!strcmp(argv[i],"--HFakeScale")) {
	    HFakeScale = argv[i+1];
   	}
	if (!strcmp(argv[i],"--SignalScale")) {
	    SignalScale = argv[i+1];
   	}
	if (!strcmp(argv[i],"--NoKfactor")) {
	    NoKfactor = true;
   	}
	if (!strcmp(argv[i],"--NoSys")) {
	    NoSys = true;
   	}
	if (!strcmp(argv[i],"--MCtoData")) {
	    NormMCtoData = true;
   	}
    }

    string DataFile = "Data";
    vector<string> StackNames;
    vector<string> StackInputs;
    vector<string> StackPhMatchs;
    vector<string> StackScales;
    vector<string > StackUncerts;
    vector<string > StackColors;
    string sig_sys = "0.0";
    string hfake_sys = "0.0";
    string efake_sys = "0.0";
    string Wy_sys = "0.0";
    string Zy_sys = "0.0";
    string QCD_sys = "0.0";
    string Other_sys = "0.0";
    if (!NoSys) {
	sig_sys = "0.15";
	hfake_sys = "0.4";
	efake_sys = "0.2";
	Wy_sys = "0.5";
	Zy_sys = "0.5";
	QCD_sys = "0.5";
	Other_sys = "0.5";
	for (int i = 0; i < argc; i++) {
    	    if (!strcmp(argv[i],"--SigSys")) {
    	        sig_sys = argv[i+1];
    	    }
    	}
    }

    if (SplitSignal) {
    StackNames.push_back("ttyISR"); StackInputs.push_back("Signal"); StackPhMatchs.push_back("Ancestor1"); StackScales.push_back("1"); StackUncerts.push_back(sig_sys); StackColors.push_back("632");
    StackNames.push_back("ttyTop"); StackInputs.push_back("Signal"); StackPhMatchs.push_back("Ancestor2"); StackScales.push_back("1"); StackUncerts.push_back(sig_sys); StackColors.push_back("633");
    StackNames.push_back("ttyFSR"); StackInputs.push_back("Signal"); StackPhMatchs.push_back("Ancestor3"); StackScales.push_back("1"); StackUncerts.push_back(sig_sys); StackColors.push_back("634");
    } else {
    StackNames.push_back("tty"); StackInputs.push_back("Signal"); StackPhMatchs.push_back("TruePh"); StackScales.push_back("1"); StackUncerts.push_back(sig_sys); StackColors.push_back("632");
    }
    StackNames.push_back("Hfake"); StackInputs.push_back("TTBar,WjetsEl,WjetsMu,WjetsTau,ZjetsElEl,ZjetsMuMu,ZjetsTauTau,Diboson,STWT,STOthers"); StackPhMatchs.push_back("HFake"); StackScales.push_back("1.5"); StackUncerts.push_back(hfake_sys); StackColors.push_back("876");
    StackNames.push_back("Efake"); StackInputs.push_back("TTBar,WjetsEl,WjetsMu,WjetsTau,ZjetsElEl,ZjetsMuMu,ZjetsTauTau,Diboson,STWT,STOthers"); StackPhMatchs.push_back("EFake"); StackScales.push_back("1"); StackUncerts.push_back(efake_sys); StackColors.push_back("619");
    StackNames.push_back("Wy"); StackInputs.push_back("WGammajetsElNLO,WGammajetsMuNLO,WGammajetsTauNLO"); StackPhMatchs.push_back("TruePh"); StackScales.push_back("1"); StackUncerts.push_back(Wy_sys); StackColors.push_back("401");
    StackNames.push_back("Zy"); StackInputs.push_back("ZGammajetsElElNLO,ZGammajetsMuMuNLO,ZGammajetsTauTauNLO"); StackPhMatchs.push_back("TruePh"); StackScales.push_back("1"); StackUncerts.push_back(Zy_sys); StackColors.push_back("62");
    if (SubRegion == "ejets") {StackNames.push_back("QCD"); StackInputs.push_back("QCD"); StackPhMatchs.push_back("UPS_eta_mtw"); StackScales.push_back("1"); StackUncerts.push_back(QCD_sys); StackColors.push_back("97");}
    if (SubRegion == "mujets") {StackNames.push_back("QCD"); StackInputs.push_back("QCD"); StackPhMatchs.push_back("PS_pt_mtw"); StackScales.push_back("1"); StackUncerts.push_back(QCD_sys); StackColors.push_back("97");}
    StackNames.push_back("Other"); StackInputs.push_back("Diboson,STWT,STOthers"); StackPhMatchs.push_back("TruePh"); StackScales.push_back("1"); StackUncerts.push_back(Other_sys); StackColors.push_back("95");

    int n_proc = StackNames.size();

    if (Debug) cout << "tag2" << endl;

    PlotComparor* PC = new PlotComparor();
    PC->SetSaveDir("plots/");
    PC->DrawRatio(true);
    PC->FixRatioRange(true);
    PC->SetMaxRatio(RatioUp);
    PC->SetMinRatio(RatioDn);
    if (CMS == "13TeV") PC->Is13TeV(true);
    PC->DataLumi(Lumi);
    PC->IsSimulation(Simulation);
    PC->HasData(HasData);
    PC->SetYtitle("Events");
    PC->ReverseStackOrder(Reverse);
    PC->NormMCtoData(NormMCtoData);
    if (startcolor != "") {
	PC->SetStartColor(atoi(startcolor.c_str()));
    }

    for (int ivr = 0; ivr < vars.size(); ivr++) {
        string var = vars.at(ivr);
    
        TFile* f_data = NULL;
	string fname ;
        if (DataFile != "") {
	    fname = inputdir + CMS + "_" + Cut + "_Data_" + Region + "_" + SubRegion + "_" + Variation + "_" + Tag + ".root";
	    f_data = new TFile(fname.c_str());
        }
        if (!f_data) {
	   cout << "Can't open data file " << fname << endl;
	   cout << "Will only show the MC stack " << endl;
        }
        TH1F* h_data = NULL;
	int n1 = fname.find("_Nominal", 0);
	string namekey = fname.substr(inputdir.size(), n1-inputdir.size()+1);
	string hname = var + "_"; hname += namekey; hname += "Nominal";
        if (f_data) {
	    h_data = (TH1F*)f_data->Get(hname.c_str());
        }
        PC->ClearAlterHs();
        PC->ClearAlterColors();
        PC->SetBaseH(h_data);
        PC->SetBaseHName("Data");
        PC->SetBaseColor(1);
    
        double total = 0;
	vector<TFile*> tmpfs;
        for (int i = 0; i < n_proc; i++) {
            TH1F* h_stack_i = NULL;
	    vector<string> Inputs = SplitToVector(StackInputs.at(i), ",");
            for (int j = 0; j < Inputs.size(); j++) {
		string fname;
		if (Inputs.at(j) != "QCD") {
		    fname = inputdir + CMS + "_" + Cut + "_";
		    if (StackPhMatchs.at(i) != "") fname += "PhMatch_" + StackPhMatchs.at(i) + "_";
		    fname += Inputs.at(j);
		    if (Inputs.at(j) != "Signal") {
		    	fname += "_Reco_" + Region + "_" + SubRegion + "_" + Variation + "_" + Tag + ".root";
		    } else {
			if (!NoKfactor) {
			   fname += "_Reco_" + Region + "_" + SubRegion + "_" + Variation + "_" + Tag + ".root";
			} else {
			   fname += "_Reco_" + Region + "_" + SubRegion + "_NoKfactor_" + Variation + "_" + Tag + ".root";
			}
		    }
		} else {
		    string Para = StackPhMatchs.at(i);
		    fname = inputdir + CMS + "_" + Cut + "_QCD_" + Region + "_" + SubRegion + "_" + Para + "_" + Variation + "_" + Tag + ".root";
		}
                TFile* f_tmp = new TFile(fname.c_str());
		if (!f_tmp) {
    	    	    cout << "Can't open -> " << fname << endl;
    	    	    exit(-1);
    	    	}
		tmpfs.push_back(f_tmp);

		int n1 = fname.find("_Nominal", 0);
		string namekey = fname.substr(inputdir.size(), n1-inputdir.size()+1);
		string hname = var + "_"; hname += namekey; hname += "Nominal";
                TH1F*h_tmp = (TH1F*)f_tmp->Get(hname.c_str());
		if (!h_tmp) {
    	    	    cout << "Can't get -> " << hname << endl;
    	    	    f_tmp->ls();
    	    	    exit(-1);
    	    	}
		if (StackNames.at(i) == "Hfake") {
		    if (HFakeScale != "") {
			StackScales.at(i) = HFakeScale;
		    }
		}
		if (StackNames.at(i).find("tty",0) != string::npos) {
		    if (SignalScale != "") {
			StackScales.at(i) = SignalScale;
		    }
		}
                h_tmp->Scale(atof(StackScales.at(i).c_str()));
                if (j == 0) {
		    h_stack_i = h_tmp;
		    if (atof(StackScales.at(i).c_str()) != 1 && StackNames.at(i).find("[x",0) == string::npos) {
	    	        StackNames.at(i) += "[x";
	    	        StackNames.at(i) += StackScales.at(i);
	    	        StackNames.at(i) += "]";
	    	    }
                } else {
		    h_stack_i->Add(h_tmp);
                }
            }
            double tot_err = 0;
            for (int j = 0; j < h_stack_i->GetNbinsX(); j++) {
                tot_err += pow(h_stack_i->GetBinError(j+1),2);
                //h_stack_i->SetBinError(j+1, sqrt(pow(h_stack_i->GetBinError(j+1),2) + pow(atof(StackUncerts.at(i).c_str())*h_stack_i->GetBinContent(j+1),2)));
                if (atof(StackUncerts.at(i).c_str()) != 0.) h_stack_i->SetBinError(j+1, atof(StackUncerts.at(i).c_str())*h_stack_i->GetBinContent(j+1));
            }
            tot_err = sqrt(tot_err);
            if (atof(StackUncerts.at(i).c_str()) != 0.) tot_err = atof(StackUncerts.at(i).c_str())*h_stack_i->Integral();
            total += h_stack_i->Integral();
    	    PC->AddAlterHName(StackNames.at(i));
    	    PC->AddAlterH(h_stack_i);
    	    PC->AddAlterColors(atoi(StackColors.at(i).c_str()));
            if (Print) cout << StackNames.at(i) << ": " << h_stack_i->Integral() << " " << tot_err << endl;
        }
    
        if (Print) cout << "Total: " << total << endl;
        if (Print && h_data) cout << "Data: " << h_data->Integral() << endl;
    
        //if (var.find("Eta",0) != string::npos || var.find("PtCone20",0) != string::npos) {
        //    PC->ShiftLegendX2(+0.05);
        //    PC->SetLegendScaleX(1.45);
        //}
        PC->ShiftLegendX(+0.1);
        string savename = CMS + "_Stack_" + var + "_" + Cut + "_" + Region + "_" + SubRegion;
        PC->SetSaveName(savename);
        PC->ShiftLegendX(LGoff);
        PC->SetLegendNC(LGNC);
	PC->SetChannel(SubRegion);
	PC->SquareCanvas(CSquare);
	PC->OrderBin(OrderBin);
	PC->SaveRatio(SaveRatio);
        //PC->SetXtitle("p_{T,l} [GeV]");
        //if (var.find("PhPt",0) != string::npos) PC->SetYtitle("Events/5 GeV");
        //if (var.find("PhEta",0) != string::npos) PC->SetYtitle("Events/0.2");
        if (UseLogy) PC->UseLogy(UseLogy, LogyMin, LogyScale);
	if (YRangeRatio != "") PC->SetYRangeRatio(atof(YRangeRatio.c_str()));
	if (LegendNC != "") PC->SetLegendNC(atoi(LegendNC.c_str()));
        PC->DrawStack();

	delete f_data;
	for (int i = 0; i < tmpfs.size(); i++) {
	    delete tmpfs.at(i);
	}
    }
}
