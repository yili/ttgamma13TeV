#include <TFile.h>
#include <TH1.h>
#include <fstream>
#include <iostream>
#include <iomanip>
#include <iostream>
#include <vector>
#include <string>

using namespace std;

int main(int argc, char * argv[]) {

    string tag;
    for (int i = 0; i < argc; i++) {
	if (!strcmp(argv[i],"--Tag")) {
	    tag = argv[i+1];
        }
    }
    
    vector<string> inputs;
    if (tag == "Signal") {
	inputs.push_back("/eos/atlas/atlascerngroupdisk/phys-top/upgrade/HL_v5/user.finelli.410389.MadGraphPythia8EvtGe.DAOD_TRUTH1.e6540_p3401.v5_output_root/user.finelli.14596483._000001.output.root");
	inputs.push_back("/eos/atlas/atlascerngroupdisk/phys-top/upgrade/HL_v5/user.finelli.410389.MadGraphPythia8EvtGe.DAOD_TRUTH1.e6540_p3401.v5_output_root/user.finelli.14596483._000003.output.root");
	inputs.push_back("/eos/atlas/atlascerngroupdisk/phys-top/upgrade/HL_v5/user.finelli.410389.MadGraphPythia8EvtGe.DAOD_TRUTH1.e6540_p3401.v5_output_root/user.finelli.14596483._000004.output.root");
	inputs.push_back("/eos/atlas/atlascerngroupdisk/phys-top/upgrade/HL_v5/user.finelli.410389.MadGraphPythia8EvtGe.DAOD_TRUTH1.e6540_p3401.v5_output_root/user.finelli.14596483._000005.output.root");
	inputs.push_back("/eos/atlas/atlascerngroupdisk/phys-top/upgrade/HL_v5/user.finelli.410389.MadGraphPythia8EvtGe.DAOD_TRUTH1.e6540_p3401.v5_output_root/user.finelli.14596483._000006.output.root");
	inputs.push_back("/eos/atlas/atlascerngroupdisk/phys-top/upgrade/HL_v5/user.finelli.410389.MadGraphPythia8EvtGe.DAOD_TRUTH1.e6540_p3401.v5_output_root/user.finelli.14596483._000007.output.root");
    }
    else if (tag == "TTbar5") {
        inputs.push_back("/eos/atlas/atlascerngroupdisk/phys-top/upgrade/HL_v5/user.finelli.410501.PowhegPythia8EvtGen_.DAOD_TRUTH1.e6538_p3401.v5_output_root/user.finelli.14799720._000001.output.root");
        inputs.push_back("/eos/atlas/atlascerngroupdisk/phys-top/upgrade/HL_v5/user.finelli.410501.PowhegPythia8EvtGen_.DAOD_TRUTH1.e6538_p3401.v5_output_root/user.finelli.14799720._000002.output.root");
        inputs.push_back("/eos/atlas/atlascerngroupdisk/phys-top/upgrade/HL_v5/user.finelli.410501.PowhegPythia8EvtGen_.DAOD_TRUTH1.e6538_p3401.v5_output_root/user.finelli.14799720._000003.output.root.2");
        inputs.push_back("/eos/atlas/atlascerngroupdisk/phys-top/upgrade/HL_v5/user.finelli.410501.PowhegPythia8EvtGen_.DAOD_TRUTH1.e6538_p3401.v5_output_root/user.finelli.14799720._000004.output.root");
        inputs.push_back("/eos/atlas/atlascerngroupdisk/phys-top/upgrade/HL_v5/user.finelli.410501.PowhegPythia8EvtGen_.DAOD_TRUTH1.e6538_p3401.v5_output_root/user.finelli.14799720._000005.output.root");
        inputs.push_back("/eos/atlas/atlascerngroupdisk/phys-top/upgrade/HL_v5/user.finelli.410501.PowhegPythia8EvtGen_.DAOD_TRUTH1.e6538_p3401.v5_output_root/user.finelli.14799720._000006.output.root");
        inputs.push_back("/eos/atlas/atlascerngroupdisk/phys-top/upgrade/HL_v5/user.finelli.410501.PowhegPythia8EvtGen_.DAOD_TRUTH1.e6538_p3401.v5_output_root/user.finelli.14799720._000007.output.root");
        inputs.push_back("/eos/atlas/atlascerngroupdisk/phys-top/upgrade/HL_v5/user.finelli.410501.PowhegPythia8EvtGen_.DAOD_TRUTH1.e6538_p3401.v5_output_root/user.finelli.14799720._000008.output.root");
        inputs.push_back("/eos/atlas/atlascerngroupdisk/phys-top/upgrade/HL_v5/user.finelli.410501.PowhegPythia8EvtGen_.DAOD_TRUTH1.e6538_p3401.v5_output_root/user.finelli.14799720._000009.output.root");
        inputs.push_back("/eos/atlas/atlascerngroupdisk/phys-top/upgrade/HL_v5/user.finelli.410501.PowhegPythia8EvtGen_.DAOD_TRUTH1.e6538_p3401.v5_output_root/user.finelli.14799720._000010.output.root");
    }
    else if (tag == "TTbar6") {
	inputs.push_back("/eos/atlas/atlascerngroupdisk/phys-top/upgrade/HL_v6/user.adurglis.410501.PowhegPythia8EvtGen_.DAOD_TRUTH1.e6538_p3401.v06_output_root/user.adurglis.15814371._000001.output.root");
    	inputs.push_back("/eos/atlas/atlascerngroupdisk/phys-top/upgrade/HL_v6/user.adurglis.410501.PowhegPythia8EvtGen_.DAOD_TRUTH1.e6538_p3401.v06_output_root/user.adurglis.15814371._000002.output.root");
    	inputs.push_back("/eos/atlas/atlascerngroupdisk/phys-top/upgrade/HL_v6/user.adurglis.410501.PowhegPythia8EvtGen_.DAOD_TRUTH1.e6538_p3401.v06_output_root/user.adurglis.15814371._000003.output.root");
    	inputs.push_back("/eos/atlas/atlascerngroupdisk/phys-top/upgrade/HL_v6/user.adurglis.410501.PowhegPythia8EvtGen_.DAOD_TRUTH1.e6538_p3401.v06_output_root/user.adurglis.15814371._000004.output.root");
    	inputs.push_back("/eos/atlas/atlascerngroupdisk/phys-top/upgrade/HL_v6/user.adurglis.410501.PowhegPythia8EvtGen_.DAOD_TRUTH1.e6538_p3401.v06_output_root/user.adurglis.15814371._000005.output.root");
    	inputs.push_back("/eos/atlas/atlascerngroupdisk/phys-top/upgrade/HL_v6/user.adurglis.410501.PowhegPythia8EvtGen_.DAOD_TRUTH1.e6538_p3401.v06_output_root/user.adurglis.15814371._000006.output.root");
    	inputs.push_back("/eos/atlas/atlascerngroupdisk/phys-top/upgrade/HL_v6/user.adurglis.410501.PowhegPythia8EvtGen_.DAOD_TRUTH1.e6538_p3401.v06_output_root/user.adurglis.15814371._000007.output.root");
    	inputs.push_back("/eos/atlas/atlascerngroupdisk/phys-top/upgrade/HL_v6/user.adurglis.410501.PowhegPythia8EvtGen_.DAOD_TRUTH1.e6538_p3401.v06_output_root/user.adurglis.15814371._000008.output.root");
    	inputs.push_back("/eos/atlas/atlascerngroupdisk/phys-top/upgrade/HL_v6/user.adurglis.410501.PowhegPythia8EvtGen_.DAOD_TRUTH1.e6538_p3401.v06_output_root/user.adurglis.15814371._000009.output.root");
    	inputs.push_back("/eos/atlas/atlascerngroupdisk/phys-top/upgrade/HL_v6/user.adurglis.410501.PowhegPythia8EvtGen_.DAOD_TRUTH1.e6538_p3401.v06_output_root/user.adurglis.15814371._000010.output.root");
    } else {
	exit;
    }


    vector<TH1F*> h_up;
    {
	TH1F*h_ej = NULL;
    	for (int i = 0; i < inputs.size(); i++) {
    	    TFile* f= new TFile(inputs.at(i).c_str());
    	    TDirectoryFile*dr = (TDirectoryFile*)f->Get("ejets_gamma_basic");
    	    TH1F*h=(TH1F*)dr->Get("cutflow_upgrade_level");
	    cout << inputs.at(i) << endl;
	    h->Print("all");
    	    if (i == 0) {
    	        h_ej = (TH1F*)h->Clone();
    	    } else {
    	        h_ej->Add(h);
    	    }
    	}

    	TH1F*h_muj = NULL;
    	for (int i = 0; i < inputs.size(); i++) {
    	    TFile* f= new TFile(inputs.at(i).c_str());
    	    TDirectoryFile*dr = (TDirectoryFile*)f->Get("mujets_gamma_basic");
    	    TH1F*h=(TH1F*)dr->Get("cutflow_upgrade_level");
    	    if (i == 0) {
    	        h_muj = (TH1F*)h->Clone();
    	    } else {
    	        h_muj->Add(h);
    	    }
    	}

    	TH1F*h_ee = NULL;
    	for (int i = 0; i < inputs.size(); i++) {
    	    TFile* f= new TFile(inputs.at(i).c_str());
    	    TDirectoryFile*dr = (TDirectoryFile*)f->Get("ee_gamma_basic");
    	    TH1F*h=(TH1F*)dr->Get("cutflow_upgrade_level");
    	    if (i == 0) {
    	        h_ee = (TH1F*)h->Clone();
    	    } else {
    	        h_ee->Add(h);
    	    }
    	}

    	TH1F*h_emu = NULL;
    	for (int i = 0; i < inputs.size(); i++) {
    	    TFile* f= new TFile(inputs.at(i).c_str());
    	    TDirectoryFile*dr = (TDirectoryFile*)f->Get("emu_gamma_basic");
    	    TH1F*h=(TH1F*)dr->Get("cutflow_upgrade_level");
    	    if (i == 0) {
    	        h_emu = (TH1F*)h->Clone();
    	    } else {
    	        h_emu->Add(h);
    	    }
    	}

    	TH1F*h_mumu = NULL;
    	for (int i = 0; i < inputs.size(); i++) {
    	    TFile* f= new TFile(inputs.at(i).c_str());
    	    TDirectoryFile*dr = (TDirectoryFile*)f->Get("mumu_gamma_basic");
    	    TH1F*h=(TH1F*)dr->Get("cutflow_upgrade_level");
    	    if (i == 0) {
    	        h_mumu = (TH1F*)h->Clone();
    	    } else {
    	        h_mumu->Add(h);
    	    }
    	}

    	//h_ej->Print("All");
    	//h_muj->Print("All");
    	//h_ee->Print("All");
    	//h_emu->Print("All");
    	//h_mumu->Print("All");
	h_up.push_back(h_ej);
	h_up.push_back(h_muj);
	h_up.push_back(h_ee);
	h_up.push_back(h_emu);
	h_up.push_back(h_mumu);
    }

    vector<TH1F*> h_pl;
    {
	TH1F*h_ej = NULL;
    	for (int i = 0; i < inputs.size(); i++) {
    	    TFile* f= new TFile(inputs.at(i).c_str());
    	    TDirectoryFile*dr = (TDirectoryFile*)f->Get("ejets_gamma_basic");
    	    TH1F*h=(TH1F*)dr->Get("cutflow_particle_level");
    	    if (i == 0) {
    	        h_ej = (TH1F*)h->Clone();
    	    } else {
    	        h_ej->Add(h);
    	    }
    	}

    	TH1F*h_muj = NULL;
    	for (int i = 0; i < inputs.size(); i++) {
    	    TFile* f= new TFile(inputs.at(i).c_str());
    	    TDirectoryFile*dr = (TDirectoryFile*)f->Get("mujets_gamma_basic");
    	    TH1F*h=(TH1F*)dr->Get("cutflow_particle_level");
    	    if (i == 0) {
    	        h_muj = (TH1F*)h->Clone();
    	    } else {
    	        h_muj->Add(h);
    	    }
    	}

    	TH1F*h_ee = NULL;
    	for (int i = 0; i < inputs.size(); i++) {
    	    TFile* f= new TFile(inputs.at(i).c_str());
    	    TDirectoryFile*dr = (TDirectoryFile*)f->Get("ee_gamma_basic");
    	    TH1F*h=(TH1F*)dr->Get("cutflow_particle_level");
    	    if (i == 0) {
    	        h_ee = (TH1F*)h->Clone();
    	    } else {
    	        h_ee->Add(h);
    	    }
    	}

    	TH1F*h_emu = NULL;
    	for (int i = 0; i < inputs.size(); i++) {
    	    TFile* f= new TFile(inputs.at(i).c_str());
    	    TDirectoryFile*dr = (TDirectoryFile*)f->Get("emu_gamma_basic");
    	    TH1F*h=(TH1F*)dr->Get("cutflow_particle_level");
    	    if (i == 0) {
    	        h_emu = (TH1F*)h->Clone();
    	    } else {
    	        h_emu->Add(h);
    	    }
    	}

    	TH1F*h_mumu = NULL;
    	for (int i = 0; i < inputs.size(); i++) {
    	    TFile* f= new TFile(inputs.at(i).c_str());
    	    TDirectoryFile*dr = (TDirectoryFile*)f->Get("mumu_gamma_basic");
    	    TH1F*h=(TH1F*)dr->Get("cutflow_particle_level");
    	    if (i == 0) {
    	        h_mumu = (TH1F*)h->Clone();
    	    } else {
    	        h_mumu->Add(h);
    	    }
    	}

    	//h_ej->Print("All");
    	//h_muj->Print("All");
    	//h_ee->Print("All");
    	//h_emu->Print("All");
    	//h_mumu->Print("All");
	h_pl.push_back(h_ej);
	h_pl.push_back(h_muj);
	h_pl.push_back(h_ee);
	h_pl.push_back(h_emu);
	h_pl.push_back(h_mumu);
    }

    for (int i = 0; i < h_up.size(); i++) {
	TH1F* h_upi = h_up.at(i);
	TH1F* h_pli = h_pl.at(i);

	cout << setw(20) << "Upgrade" << setw(20) << "Particle" << setw(20) << "Ratio" << endl;
	for (int j = 0; j < h_upi->GetNbinsX(); j++) {
	    double n_upi = h_upi->GetBinContent(j+1);
	    double n_pli = h_pli->GetBinContent(j+1);
	    double n_ratio = 0;
	    if (n_pli!= 0)  n_ratio = n_upi/n_pli;	    
	    cout << setw(20) << n_upi << setw(20) << n_pli << setw(20) << n_ratio << endl;
	}
    }
}
