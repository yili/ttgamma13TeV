{
    TCanvas* c = new TCanvas("c","",800,600);
    c->Divide(5,5);

    {
    c->cd(1);
    TFile* f= new TFile("results/Nominal//13TeV_CutZegReverse_ZjetsElEl_Reco_EF1_zeg_Nominal_Final03.root");
    TH1F* h = (TH1F*)f->Get("MPhLepForZ_13TeV_CutZegReverse_ZjetsElEl_Reco_EF1_zeg_Nominal");
    TF1* tf = (TF1*)f->Get("InvariantMassTF1_13TeV_CutZegReverse_ZjetsElEl_Reco_EF1_zeg_Nominal");
    cout << h->Integral() << endl;
    h->Scale(1/h->Integral());

    h->Draw();
    tf->Draw("same");
    }

    {
    c->cd(2);
    TFile* f= new TFile("results/Nominal//13TeV_CutZegReverse_ZjetsElEl_Reco_EF1_zeg_Nominal_Final03.root");
    TH1F* h = (TH1F*)f->Get("InvariantMass_PtBin1_EtaBin1_13TeV_CutZegReverse_ZjetsElEl_Reco_EF1_zeg_Nominal");
    TF1* tf = (TF1*)f->Get("InvariantMassTF1_PtBin1_EtaBin1_13TeV_CutZegReverse_ZjetsElEl_Reco_EF1_zeg_Nominal");
    cout << h->Integral() << endl;
    h->Scale(1/h->Integral());

    h->Draw();
    tf->Draw("same");
    }
    {
    c->cd(3);
    TFile* f= new TFile("results/Nominal//13TeV_CutZegReverse_ZjetsElEl_Reco_EF1_zeg_Nominal_Final03.root");
    TH1F* h = (TH1F*)f->Get("InvariantMass_PtBin1_EtaBin2_13TeV_CutZegReverse_ZjetsElEl_Reco_EF1_zeg_Nominal");
    TF1* tf = (TF1*)f->Get("InvariantMassTF1_PtBin1_EtaBin2_13TeV_CutZegReverse_ZjetsElEl_Reco_EF1_zeg_Nominal");
    cout << h->Integral() << endl;
    h->Scale(1/h->Integral());

    h->Draw();
    tf->Draw("same");
    }
    {
    c->cd(4);
    TFile* f= new TFile("results/Nominal//13TeV_CutZegReverse_ZjetsElEl_Reco_EF1_zeg_Nominal_Final03.root");
    TH1F* h = (TH1F*)f->Get("InvariantMass_PtBin1_EtaBin3_13TeV_CutZegReverse_ZjetsElEl_Reco_EF1_zeg_Nominal");
    TF1* tf = (TF1*)f->Get("InvariantMassTF1_PtBin1_EtaBin3_13TeV_CutZegReverse_ZjetsElEl_Reco_EF1_zeg_Nominal");
    cout << h->Integral() << endl;
    h->Scale(1/h->Integral());

    h->Draw();
    tf->Draw("same");
    }
    {
    c->cd(5);
    TFile* f= new TFile("results/Nominal//13TeV_CutZegReverse_ZjetsElEl_Reco_EF1_zeg_Nominal_Final03.root");
    TH1F* h = (TH1F*)f->Get("InvariantMass_PtBin1_EtaBin5_13TeV_CutZegReverse_ZjetsElEl_Reco_EF1_zeg_Nominal");
    TF1* tf = (TF1*)f->Get("InvariantMassTF1_PtBin1_EtaBin5_13TeV_CutZegReverse_ZjetsElEl_Reco_EF1_zeg_Nominal");
    cout << h->Integral() << endl;
    h->Scale(1/h->Integral());

    h->Draw();
    tf->Draw("same");
    }
    {
    c->cd(6);
    TFile* f= new TFile("results/Nominal//13TeV_CutZegReverse_ZjetsElEl_Reco_EF1_zeg_Nominal_Final03.root");
    TH1F* h = (TH1F*)f->Get("InvariantMass_PtBin1_EtaBin6_13TeV_CutZegReverse_ZjetsElEl_Reco_EF1_zeg_Nominal");
    TF1* tf = (TF1*)f->Get("InvariantMassTF1_PtBin1_EtaBin6_13TeV_CutZegReverse_ZjetsElEl_Reco_EF1_zeg_Nominal");
    cout << h->Integral() << endl;
    h->Scale(1/h->Integral());

    h->Draw();
    tf->Draw("same");
    }
    {
    c->cd(7);
    TFile* f= new TFile("results/Nominal//13TeV_CutZegReverse_ZjetsElEl_Reco_EF1_zeg_Nominal_Final03.root");
    TH1F* h = (TH1F*)f->Get("InvariantMass_PtBin2_EtaBin1_13TeV_CutZegReverse_ZjetsElEl_Reco_EF1_zeg_Nominal");
    TF1* tf = (TF1*)f->Get("InvariantMassTF1_PtBin2_EtaBin1_13TeV_CutZegReverse_ZjetsElEl_Reco_EF1_zeg_Nominal");
    cout << h->Integral() << endl;
    h->Scale(1/h->Integral());

    h->Draw();
    tf->Draw("same");
    }
    {
    c->cd(8);
    TFile* f= new TFile("results/Nominal//13TeV_CutZegReverse_ZjetsElEl_Reco_EF1_zeg_Nominal_Final03.root");
    TH1F* h = (TH1F*)f->Get("InvariantMass_PtBin2_EtaBin2_13TeV_CutZegReverse_ZjetsElEl_Reco_EF1_zeg_Nominal");
    TF1* tf = (TF1*)f->Get("InvariantMassTF1_PtBin2_EtaBin2_13TeV_CutZegReverse_ZjetsElEl_Reco_EF1_zeg_Nominal");
    cout << h->Integral() << endl;
    h->Scale(1/h->Integral());

    h->Draw();
    tf->Draw("same");
    }
    {
    c->cd(9);
    TFile* f= new TFile("results/Nominal//13TeV_CutZegReverse_ZjetsElEl_Reco_EF1_zeg_Nominal_Final03.root");
    TH1F* h = (TH1F*)f->Get("InvariantMass_PtBin2_EtaBin3_13TeV_CutZegReverse_ZjetsElEl_Reco_EF1_zeg_Nominal");
    TF1* tf = (TF1*)f->Get("InvariantMassTF1_PtBin2_EtaBin3_13TeV_CutZegReverse_ZjetsElEl_Reco_EF1_zeg_Nominal");
    cout << h->Integral() << endl;
    h->Scale(1/h->Integral());

    h->Draw();
    tf->Draw("same");
    }
    {
    c->cd(10);
    TFile* f= new TFile("results/Nominal//13TeV_CutZegReverse_ZjetsElEl_Reco_EF1_zeg_Nominal_Final03.root");
    TH1F* h = (TH1F*)f->Get("InvariantMass_PtBin2_EtaBin5_13TeV_CutZegReverse_ZjetsElEl_Reco_EF1_zeg_Nominal");
    TF1* tf = (TF1*)f->Get("InvariantMassTF1_PtBin2_EtaBin5_13TeV_CutZegReverse_ZjetsElEl_Reco_EF1_zeg_Nominal");
    cout << h->Integral() << endl;
    h->Scale(1/h->Integral());

    h->Draw();
    tf->Draw("same");
    }
    {
    c->cd(11);
    TFile* f= new TFile("results/Nominal//13TeV_CutZegReverse_ZjetsElEl_Reco_EF1_zeg_Nominal_Final03.root");
    TH1F* h = (TH1F*)f->Get("InvariantMass_PtBin2_EtaBin6_13TeV_CutZegReverse_ZjetsElEl_Reco_EF1_zeg_Nominal");
    TF1* tf = (TF1*)f->Get("InvariantMassTF1_PtBin2_EtaBin6_13TeV_CutZegReverse_ZjetsElEl_Reco_EF1_zeg_Nominal");
    cout << h->Integral() << endl;
    h->Scale(1/h->Integral());

    h->Draw();
    tf->Draw("same");
    }
    {
    c->cd(12);
    TFile* f= new TFile("results/Nominal//13TeV_CutZegReverse_ZjetsElEl_Reco_EF1_zeg_Nominal_Final03.root");
    TH1F* h = (TH1F*)f->Get("InvariantMass_PtBin3_EtaBin1_13TeV_CutZegReverse_ZjetsElEl_Reco_EF1_zeg_Nominal");
    TF1* tf = (TF1*)f->Get("InvariantMassTF1_PtBin3_EtaBin1_13TeV_CutZegReverse_ZjetsElEl_Reco_EF1_zeg_Nominal");
    cout << h->Integral() << endl;
    h->Scale(1/h->Integral());

    h->Draw();
    tf->Draw("same");
    }
    {
    c->cd(13);
    TFile* f= new TFile("results/Nominal//13TeV_CutZegReverse_ZjetsElEl_Reco_EF1_zeg_Nominal_Final03.root");
    TH1F* h = (TH1F*)f->Get("InvariantMass_PtBin3_EtaBin2_13TeV_CutZegReverse_ZjetsElEl_Reco_EF1_zeg_Nominal");
    TF1* tf = (TF1*)f->Get("InvariantMassTF1_PtBin3_EtaBin2_13TeV_CutZegReverse_ZjetsElEl_Reco_EF1_zeg_Nominal");
    cout << h->Integral() << endl;
    h->Scale(1/h->Integral());

    h->Draw();
    tf->Draw("same");
    }
    {
    c->cd(14);
    TFile* f= new TFile("results/Nominal//13TeV_CutZegReverse_ZjetsElEl_Reco_EF1_zeg_Nominal_Final03.root");
    TH1F* h = (TH1F*)f->Get("InvariantMass_PtBin3_EtaBin3_13TeV_CutZegReverse_ZjetsElEl_Reco_EF1_zeg_Nominal");
    TF1* tf = (TF1*)f->Get("InvariantMassTF1_PtBin3_EtaBin3_13TeV_CutZegReverse_ZjetsElEl_Reco_EF1_zeg_Nominal");
    cout << h->Integral() << endl;
    h->Scale(1/h->Integral());

    h->Draw();
    tf->Draw("same");
    }
    {
    c->cd(15);
    TFile* f= new TFile("results/Nominal//13TeV_CutZegReverse_ZjetsElEl_Reco_EF1_zeg_Nominal_Final03.root");
    TH1F* h = (TH1F*)f->Get("InvariantMass_PtBin3_EtaBin5_13TeV_CutZegReverse_ZjetsElEl_Reco_EF1_zeg_Nominal");
    TF1* tf = (TF1*)f->Get("InvariantMassTF1_PtBin3_EtaBin5_13TeV_CutZegReverse_ZjetsElEl_Reco_EF1_zeg_Nominal");
    cout << h->Integral() << endl;
    h->Scale(1/h->Integral());

    h->Draw();
    tf->Draw("same");
    }
    {
    c->cd(16);
    TFile* f= new TFile("results/Nominal//13TeV_CutZegReverse_ZjetsElEl_Reco_EF1_zeg_Nominal_Final03.root");
    TH1F* h = (TH1F*)f->Get("InvariantMass_PtBin3_EtaBin6_13TeV_CutZegReverse_ZjetsElEl_Reco_EF1_zeg_Nominal");
    TF1* tf = (TF1*)f->Get("InvariantMassTF1_PtBin3_EtaBin6_13TeV_CutZegReverse_ZjetsElEl_Reco_EF1_zeg_Nominal");
    cout << h->Integral() << endl;
    h->Scale(1/h->Integral());

    h->Draw();
    tf->Draw("same");
    }
    {
    c->cd(17);
    TFile* f= new TFile("results/Nominal//13TeV_CutZegReverse_ZjetsElEl_Reco_EF1_zeg_Nominal_Final03.root");
    TH1F* h = (TH1F*)f->Get("InvariantMass_PtBin4_EtaBin1_13TeV_CutZegReverse_ZjetsElEl_Reco_EF1_zeg_Nominal");
    TF1* tf = (TF1*)f->Get("InvariantMassTF1_PtBin4_EtaBin1_13TeV_CutZegReverse_ZjetsElEl_Reco_EF1_zeg_Nominal");
    cout << h->Integral() << endl;
    h->Scale(1/h->Integral());

    h->Draw();
    tf->Draw("same");
    }
    {
    c->cd(18);
    TFile* f= new TFile("results/Nominal//13TeV_CutZegReverse_ZjetsElEl_Reco_EF1_zeg_Nominal_Final03.root");
    TH1F* h = (TH1F*)f->Get("InvariantMass_PtBin4_EtaBin2_13TeV_CutZegReverse_ZjetsElEl_Reco_EF1_zeg_Nominal");
    TF1* tf = (TF1*)f->Get("InvariantMassTF1_PtBin4_EtaBin2_13TeV_CutZegReverse_ZjetsElEl_Reco_EF1_zeg_Nominal");
    cout << h->Integral() << endl;
    h->Scale(1/h->Integral());

    h->Draw();
    tf->Draw("same");
    }
    {
    c->cd(19);
    TFile* f= new TFile("results/Nominal//13TeV_CutZegReverse_ZjetsElEl_Reco_EF1_zeg_Nominal_Final03.root");
    TH1F* h = (TH1F*)f->Get("InvariantMass_PtBin4_EtaBin3_13TeV_CutZegReverse_ZjetsElEl_Reco_EF1_zeg_Nominal");
    TF1* tf = (TF1*)f->Get("InvariantMassTF1_PtBin4_EtaBin3_13TeV_CutZegReverse_ZjetsElEl_Reco_EF1_zeg_Nominal");
    cout << h->Integral() << endl;
    h->Scale(1/h->Integral());

    h->Draw();
    tf->Draw("same");
    }
    {
    c->cd(20);
    TFile* f= new TFile("results/Nominal//13TeV_CutZegReverse_ZjetsElEl_Reco_EF1_zeg_Nominal_Final03.root");
    TH1F* h = (TH1F*)f->Get("InvariantMass_PtBin4_EtaBin5_13TeV_CutZegReverse_ZjetsElEl_Reco_EF1_zeg_Nominal");
    TF1* tf = (TF1*)f->Get("InvariantMassTF1_PtBin4_EtaBin5_13TeV_CutZegReverse_ZjetsElEl_Reco_EF1_zeg_Nominal");
    cout << h->Integral() << endl;
    h->Scale(1/h->Integral());

    h->Draw();
    tf->Draw("same");
    }
    {
    c->cd(21);
    TFile* f= new TFile("results/Nominal//13TeV_CutZegReverse_ZjetsElEl_Reco_EF1_zeg_Nominal_Final03.root");
    TH1F* h = (TH1F*)f->Get("InvariantMass_PtBin4_EtaBin6_13TeV_CutZegReverse_ZjetsElEl_Reco_EF1_zeg_Nominal");
    TF1* tf = (TF1*)f->Get("InvariantMassTF1_PtBin4_EtaBin6_13TeV_CutZegReverse_ZjetsElEl_Reco_EF1_zeg_Nominal");
    cout << h->Integral() << endl;
    h->Scale(1/h->Integral());

    h->Draw();
    tf->Draw("same");
    }
//    {
//    c->cd(2);
//    TFile* f= new TFile("results/Nominal//13TeV_CutZee_PhMatch_TruePh_ZGammajetsElElNLO_Reco_EF1_zee_Nominal_Final03.root");
//    f->ls();
//    TH1F* h = (TH1F*)f->Get("MLepLepForZ_13TeV_CutZee_PhMatch_TruePh_ZGammajetsElElNLO_Reco_EF1_zee_Nominal");
//    TF1* tf = (TF1*)f->Get("InvariantMassTF1_13TeV_CutZee_PhMatch_TruePh_ZGammajetsElElNLO_Reco_EF1_zee_Nominal");
//    cout << h->Integral() << endl;
//    h->Scale(1/h->Integral());
//
//    h->Draw();
//    tf->Draw("same");
//    }
}

