#include <TFile.h>
#include <TString.h>
#include <TH1D.h>
#include <TH2.h>
#include <TCanvas.h>
#include <TLegend.h>
#include <TPad.h>
#include <TLine.h>
#include <string>
#include <TLatex.h>
#include <TMath.h>
#include <vector>
#include <TGraphErrors.h>
#include <map>
#include <TIterator.h>
#include <TKey.h>
#include <TObject.h>
#include <THStack.h>

#include "AtlasStyle.h"
#include "PlotComparor.h"
#include "StringPlayer.h"
#include "ConfigReader.h"
#include "Logger.h"

using namespace std;

int main(int argc, char * argv[]) 
{
    vector<TString> vars;
    vars.push_back("NGoodPh");
    vars.push_back("Njet");

    SetAtlasStyle();

    PlotComparor* PC = new PlotComparor();
    PC->DrawRatio(true);
    PC->SetSaveDir("plots/Upgrade/");
    PC->NormToUnit(true);
    string drawoptions = "hist";
    drawoptions += " _e";
    PC->SetDrawOption(drawoptions.c_str());

    for (int i = 0; i < vars.size(); i++) {
	TString var = vars.at(i);

	TFile* f_14 = new TFile("results/Upgrade/14TeV_CutNone_PhMatch_TruePh_LpMatch_TrueLp_Signal_Upgrade_UR_ejets_Nominal_U07.root");
    	TH1F* h_14 = (TH1F*)f_14->Get(var+"_14TeV_CutNone_PhMatch_TruePh_LpMatch_TrueLp_Signal_Upgrade_UR_ejets_Nominal");

	TFile* f_14_noOR = new TFile("results/Upgrade/14TeV_CutNone_PhMatch_TruePh_LpMatch_TrueLp_Signal_Upgrade_UR_ejets_NoPhJOR_Nominal_U07.root");
    	TH1F* h_14_noOR = (TH1F*)f_14_noOR->Get(var+"_14TeV_CutNone_PhMatch_TruePh_LpMatch_TrueLp_Signal_Upgrade_UR_ejets_NoPhJOR_Nominal");

    	TFile* f_13 = new TFile("results/Nominal/13TeV_CutNone_PhMatch_TruePh_Signal_Reco_CR1_ejets_Nominal_Final06.root");
    	TH1F* h_13 = (TH1F*)f_13->Get(var+"_13TeV_CutNone_PhMatch_TruePh_Signal_Reco_CR1_ejets_Nominal");

    	PC->SetChannel("ejets");

    	PC->ClearAlterHs();
    	PC->SetBaseH(h_13);
    	PC->SetBaseHName("13TeV w OR");
    	PC->AddAlterH(h_14);
    	PC->AddAlterHName("14TeV w OR");
    	PC->AddAlterH(h_14_noOR);
    	PC->AddAlterHName("14TeV w/o OR");
    	string savename = "CompareShape1314Siganl_"+string(var.Data())+"_ejets";
    	PC->SetSaveName(savename);
	PC->IsSimulation(true);
        PC->SetLHCInfo("#sqrt{s} = 14 TeV v.s. #sqrt{s} = 13 TeV");
    	PC->Is14TeV(true);
    	PC->SetMaxRatio(2.0);
    	PC->SetMinRatio(0);
	PC->SquareCanvas(true);
	if (var == "Njet") PC->SetXtitle("N_{j}");
	if (var == "NGoodPh") PC->SetXtitle("N_{#gamma}");
    	//PC->DataLumi(36.5);
    	PC->Compare();
    }

    TFile* f_13 = new TFile("/afs/cern.ch/work/y/yili/private/Analysis/Unfolding_Upgrade/external/PL5_nominal.root");
    TFile* f_14_truth = new TFile("/afs/cern.ch/work/y/yili/private/Analysis/Siegen13TeVWork/MainCode/run/results/Nominal/Sig_Sys_Upgrade3.root");
    TFile* f_14_mig = new TFile("/afs/cern.ch/work/y/yili/private/Analysis/Siegen13TeVWork/MainCode/run/results/Nominal/Sig_Sys_Upgrade2.root");
    TFile* f_14_reco = new TFile("/afs/cern.ch/work/y/yili/private/Analysis/Siegen13TeVWork/MainCode/run/results/Nominal/Sig_Sys_Upgrade.root");

    vector<string> newvars;
    newvars.push_back("ph_pt");
    newvars.push_back("ph_abseta");
    newvars.push_back("dR_ph_lep");
    newvars.push_back("deta_ll");
    newvars.push_back("dphi_ll");

    TH1F* h_ZGammaCorr_ljets = NULL;
    TH1F* h_ZGammaCorr_ll = NULL;

    vector<string> chans;
    chans.push_back("ljets");
    chans.push_back("ll");
    for (int ivar = 0; ivar < newvars.size(); ivar++) {
	string var = newvars.at(ivar);
	TString TSvar = var.c_str();
	for (int ich = 0; ich < 2; ich++) {
	    if (ich == 0) {
	        if (ivar > 2) continue;
	    }
	    string chan = chans.at(ich);
	    TString TSchan = chan.c_str();
	    TH1F* h_13_truth = (TH1F*)f_13->Get("h_"+TSvar+"Nominal_"+TSchan+"_truth");
            TH1F* h_13_reco = (TH1F*)f_13->Get("h_"+TSvar+"Nominal_"+TSchan+"_reco");
            TH1F* h_14_truth = (TH1F*)f_14_truth->Get("h_"+TSvar+"Nominal_"+TSchan+"_truth");
            TH1F* h_14_reco = (TH1F*)f_14_reco->Get("h_"+TSvar+"Nominal_"+TSchan+"_reco");
            TH2F* h_13_mig = (TH2F*)f_13->Get("h2_"+TSvar+"Nominal_"+TSchan);
            TH2F* h_14_mig = (TH2F*)f_14_mig->Get("h2_"+TSvar+"Nominal_"+TSchan);
            TH1F* h_13_reco_mig = (TH1F*)h_13_reco->Clone();
            for (int ix = 0; ix < h_13_mig->GetNbinsX(); ix++) {
    	    double proj_ix = 0;
    	    for (int iy = 0; iy < h_13_mig->GetNbinsY(); iy++) {
    	        proj_ix += h_13_mig->GetBinContent(ix+1, iy+1);
    	    }
    	    h_13_reco_mig->SetBinContent(ix+1, proj_ix);
            }
            TH1F* h_13_truth_mig = (TH1F*)h_13_truth->Clone();
            for (int iy = 0; iy < h_13_mig->GetNbinsX(); iy++) {
    	    double proj_iy = 0;
    	    for (int ix = 0; ix < h_13_mig->GetNbinsY(); ix++) {
    	        proj_iy += h_13_mig->GetBinContent(ix+1, iy+1);
    	    }
    	    h_13_truth_mig->SetBinContent(iy+1, proj_iy);
            }
            TH1F* h_14_reco_mig = (TH1F*)h_14_reco->Clone();
            for (int ix = 0; ix < h_14_mig->GetNbinsX(); ix++) {
    	    double proj_ix = 0;
    	    for (int iy = 0; iy < h_14_mig->GetNbinsY(); iy++) {
    	        proj_ix += h_14_mig->GetBinContent(ix+1, iy+1);
    	    }
    	    h_14_reco_mig->SetBinContent(ix+1, proj_ix);
            }
            TH1F* h_14_truth_mig = (TH1F*)h_14_truth->Clone();
            for (int iy = 0; iy < h_14_mig->GetNbinsX(); iy++) {
    	    double proj_iy = 0;
    	    for (int ix = 0; ix < h_14_mig->GetNbinsY(); ix++) {
    	        proj_iy += h_14_mig->GetBinContent(ix+1, iy+1);
    	    }
    	    h_14_truth_mig->SetBinContent(iy+1, proj_iy);
            }
    
            TH1F* h_13_eff = (TH1F*)h_13_truth_mig->Clone();
            h_13_eff->Divide(h_13_truth);
            TH1F* h_14_eff = (TH1F*)h_14_truth_mig->Clone();
            h_14_eff->Divide(h_14_truth);
            TH1F* h_13_eff2 = (TH1F*)h_13_reco_mig->Clone();
            h_13_eff2->Divide(h_13_reco);
            TH1F* h_14_eff2 = (TH1F*)h_14_reco_mig->Clone();
            h_14_eff2->Divide(h_14_reco);
    
            if (chan == "ljets") PC->SetChannel("Single lepton");
            else PC->SetChannel("Dilepton");
            
            PC->NormToUnit(false);
            PC->ClearAlterHs();
            PC->SetBaseH(h_13_eff);
            PC->SetBaseHName("13TeV");
            PC->AddAlterH(h_14_eff);
            PC->AddAlterHName("14TeV");
            string savename = "CompareSigEff1314_"+var+"_"+chan;
            PC->SetSaveName(savename);
            PC->IsSimulation(true);
            PC->SetLHCInfo("#sqrt{s} = 14 TeV v.s. #sqrt{s} = 13 TeV");
            PC->Is14TeV(true);
            PC->SetMaxRatio(1.2);
            PC->SetMinRatio(0.5);
            PC->SquareCanvas(true);
	    PC->SetLegendPos(0.75,0.75,0.92,0.94);
	    if (ivar == 0) PC->SetXtitle("p_{T}(#gamma) [GeV]");
	    if (ivar == 1) PC->SetXtitle("|#eta(#gamma)|");
	    if (ivar == 2 && ich == 0) PC->SetXtitle("#DeltaR(#gamma,#it{l})");
	    if (ivar == 2 && ich == 1) PC->SetXtitle("#DeltaR(#gamma,#it{l})_{min}");
	    if (ivar == 3) PC->SetXtitle("|#Delta#eta(#it{l},#it{l})|");
	    if (ivar == 4) PC->SetXtitle("#Delta#phi(#it{l},#it{l})");
            PC->SetYtitle("#epsilon(#gamma)");
            PC->ForcedLGPos(true);
            PC->Compare();

            //PC->NormToUnit(true);
            //PC->ClearAlterHs();
            //PC->SetBaseH(h_13_reco);
            //PC->SetBaseHName("13TeV");
            //PC->AddAlterH(h_14_reco);
            //PC->AddAlterHName("14TeV");
            //savename = "CompareSigShapeReco1314_"+var+"_"+chan;
            //PC->SetSaveName(savename);
            //PC->IsSimulation(true);
            //PC->SetLHCInfo("");
            //PC->Is14TeV(true);
            //PC->SetMaxRatio(1.5);
            //PC->SetMinRatio(0.5);
            //PC->SquareCanvas(true);
	    //if (ivar == 0) PC->SetXtitle("Reco p_{T}(#gamma) [GeV]");
	    //if (ivar == 1) PC->SetXtitle("|#eta(#gamma)|");
	    //if (ivar == 2 && ich == 0) PC->SetXtitle("#DeltaR(#gamma,#it{l})");
	    //if (ivar == 2 && ich == 1) PC->SetXtitle("#DeltaR(#gamma,#it{l})_{min}");
	    //if (ivar == 3) PC->SetXtitle("|#Delta#eta(#it{l},#it{l})|");
	    //if (ivar == 4) PC->SetXtitle("#Delta#phi(#it{l},#it{l})");
            //PC->SetYtitle("A. U.");
            //PC->ForcedLGPos(true);
            //PC->Compare();

	    if (ivar == 0) {
		if (ich == 0) {
		    h_ZGammaCorr_ljets = (TH1F*)h_14_eff->Clone();
		    h_ZGammaCorr_ljets->Divide(h_13_eff);
		}
		if (ich == 1) {
		    h_ZGammaCorr_ll = (TH1F*)h_14_eff->Clone();
		    h_ZGammaCorr_ll->Divide(h_13_eff);
		}
	    }

            PC->NormToUnit(true);
            PC->ClearAlterHs();
            PC->SetBaseH(h_13_truth);
            PC->SetBaseHName("13TeV");
            PC->AddAlterH(h_14_truth);
            PC->AddAlterHName("14TeV");
            savename = "CompareSigShapeTruth1314_"+var+"_"+chan;
            PC->SetSaveName(savename);
            PC->IsSimulation(true);
            PC->SetLHCInfo("#sqrt{s} = 14 TeV v.s. #sqrt{s} = 13 TeV");
            PC->Is14TeV(true);
            PC->SetMaxRatio(1.5);
            PC->SetMinRatio(0.5);
            PC->SquareCanvas(true);
	    PC->SetLegendPos(0.75,0.75,0.92,0.94);
	    if (ivar == 0) PC->SetXtitle("p_{T}(#gamma) [GeV]");
	    if (ivar == 1) PC->SetXtitle("|#eta(#gamma)|");
	    if (ivar == 2 && ich == 0) PC->SetXtitle("#DeltaR(#gamma,#it{l})");
	    if (ivar == 2 && ich == 1) PC->SetXtitle("#DeltaR(#gamma,#it{l})_{min}");
	    if (ivar == 3) PC->SetXtitle("|#Delta#eta(#it{l},#it{l})|");
	    if (ivar == 4) PC->SetXtitle("#Delta#phi(#it{l},#it{l})");
            PC->SetYtitle("A. U.");
            PC->ForcedLGPos(true);
            PC->Compare();
	}
    }

    TFile* f = new TFile("results/Upgrade/ZGammaCorr.root", "recreate");
    f->cd();
    h_ZGammaCorr_ljets->Write("ZGammaCorr_ljets");
    h_ZGammaCorr_ll->Write("ZGammaCorr_ll");
    f->Close();
    delete f;
}
