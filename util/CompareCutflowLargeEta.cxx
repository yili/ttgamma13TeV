#include <TFile.h>
#include <TLegend.h>
#include <TLatex.h>
#include <TH2F.h>
#include <TCanvas.h>
#include <TStyle.h>
#include <TH1F.h>
#include <iostream>
#include <iomanip>
#include "AtlasStyle.h"
#include "SumWeightTree.h"
#include <map>
#include <fstream>

using namespace std;

string dir_13tev = "results/Upgrade/";
string dir_14tev = "results/Upgrade/";
string run2label = "Nominal Eta";
string HLlabel = "Eta 4.0";

bool debug = false;
string hfakereweight = "HFakeReweighted_";

void FindLastBin(TH1F* h, double &n, double &e, bool debug = false);
void RecordCutflow(TH1F* h, vector<pair<string,double> > &cf, string channel, string cme);

int main(int argc, char * argv[]) 
{
    for (int i = 0; i < argc; i++) {
	if (!strcmp(argv[i],"--debug")) {
	    debug = true;
   	}
    }

    cout << fixed << setprecision(1) << endl;
    
    vector<string> channels;
    channels.push_back("ejets");
    channels.push_back("mujets");
    channels.push_back("ee");
    channels.push_back("emu");
    channels.push_back("mumu");

    vector<string> channel_titles;
    channel_titles.push_back("\\chejets");
    channel_titles.push_back("\\chmujets");
    channel_titles.push_back("\\chee");
    channel_titles.push_back("\\chemu");
    channel_titles.push_back("\\chmumu");

    vector<string> processes;
    processes.push_back("Signal");
    processes.push_back("HFake");
    //processes.push_back("EFake");
    //processes.push_back("Prompt");
    //processes.push_back("FakeLep");

    vector<string> processe_titles;
    processe_titles.push_back("t#bar{t}#gamma");
    processe_titles.push_back("Had-fake");
    //processe_titles.push_back("e-fake");
    //processe_titles.push_back("Prompt #gamma");
    //processe_titles.push_back("Fake lepton");

    //vector<vector<pair<string,double> > > Signal_Cutflows_13tev;
    //vector<vector<pair<string,double> > > Signal_Cutflows_14tev;
    //vector<vector<TH1F*> > hss_leadphpt_14tev;
    //vector<vector<TH1F*> > hss_leadpheta_14tev;
    //vector<vector<TH1F*> > hss_drphlep_14tev;
    //vector<vector<TH1F*> > hss_detall_14tev;
    //vector<vector<TH1F*> > hss_dphill_14tev;

    //vector<TH1F*> h_ratio_procs;
    //vector<TH1F*> h_ratio_signals;

    vector<vector<double> > proc_nss_13tev;
    vector<vector<double> > proc_ess_13tev;
    vector<vector<double> > proc_nss_14tev;
    vector<vector<double> > proc_ess_14tev;

    for (int ich = 0; ich < channels.size(); ich++) {
	string channel = channels.at(ich);

    	vector<vector<string> > fnamess_13tev;
    	vector<vector<string> > hnamess_13tev;
    	vector<vector<string> > fnamess_14tev;
    	vector<vector<string> > hnamess_14tev;

    	vector<string> fnames_Signal_13tev; 
    	vector<string> hnames_Signal_13tev; 
    	vector<string> fnames_Signal_14tev; 
    	vector<string> hnames_Signal_14tev; 
    	fnames_Signal_13tev.push_back("14TeV_CutSRUpgrade_PhMatch_TruePh_LpMatch_TrueLp_Signal_Upgrade_UR_"+channel+"_LargeEta_Nominal_U07.root"); 
    	hnames_Signal_13tev.push_back("Cutflow_14TeV_CutSRUpgrade_PhMatch_TruePh_LpMatch_TrueLp_Signal_Upgrade_UR_"+channel+"_LargeEta_Nominal"); 
    	fnames_Signal_14tev.push_back("14TeV_CutSRUpgrade_PhMatch_TruePh_LpMatch_TrueLp_Signal_Upgrade_UR_"+channel+"_Nominal_U07.root"); 
    	hnames_Signal_14tev.push_back("Cutflow_14TeV_CutSRUpgrade_PhMatch_TruePh_LpMatch_TrueLp_Signal_Upgrade_UR_"+channel+"_Nominal"); 
    	fnamess_13tev.push_back(fnames_Signal_13tev);
    	hnamess_13tev.push_back(hnames_Signal_13tev);
    	fnamess_14tev.push_back(fnames_Signal_14tev);
    	hnamess_14tev.push_back(hnames_Signal_14tev);

    	vector<string> fnames_HFake_13tev; 
    	vector<string> hnames_HFake_13tev; 
    	vector<string> fnames_HFake_14tev; 
    	vector<string> hnames_HFake_14tev; 
    	fnames_HFake_13tev.push_back("14TeV_CutSRUpgrade_PhMatch_HFake_LpMatch_TrueLp_TTBar_Upgrade_UR_"+channel+"_"+hfakereweight+"LargeEta_Nominal_U07.root");
    	if (channel != "emu") fnames_HFake_13tev.push_back("14TeV_CutSRUpgrade_PhMatch_HFake_LpMatch_TrueLp_Zjets_Upgrade_UR_"+channel+"_"+hfakereweight+"LargeEta_Nominal_U07.root");
    	fnames_HFake_13tev.push_back("14TeV_CutSRUpgrade_PhMatch_HFake_LpMatch_TrueLp_ST_Upgrade_UR_"+channel+"_"+hfakereweight+"LargeEta_Nominal_U07.root");
    	fnames_HFake_13tev.push_back("14TeV_CutSRUpgrade_PhMatch_HFake_LpMatch_TrueLp_Diboson_Upgrade_UR_"+channel+"_"+hfakereweight+"LargeEta_Nominal_U07.root");
    	if (channel == "ejets" || channel == "mujets") fnames_HFake_13tev.push_back("14TeV_CutSRUpgrade_PhMatch_HFake_LpMatch_TrueLp_Wjets_Upgrade_UR_"+channel+"_"+hfakereweight+"LargeEta_Nominal_U07.root");
    	hnames_HFake_13tev.push_back("Cutflow_14TeV_CutSRUpgrade_PhMatch_HFake_LpMatch_TrueLp_TTBar_Upgrade_UR_"+channel+"_"+hfakereweight+"LargeEta_Nominal");
    	if (channel != "emu") hnames_HFake_13tev.push_back("Cutflow_14TeV_CutSRUpgrade_PhMatch_HFake_LpMatch_TrueLp_Zjets_Upgrade_UR_"+channel+"_"+hfakereweight+"LargeEta_Nominal");
    	hnames_HFake_13tev.push_back("Cutflow_14TeV_CutSRUpgrade_PhMatch_HFake_LpMatch_TrueLp_ST_Upgrade_UR_"+channel+"_"+hfakereweight+"LargeEta_Nominal");
    	hnames_HFake_13tev.push_back("Cutflow_14TeV_CutSRUpgrade_PhMatch_HFake_LpMatch_TrueLp_Diboson_Upgrade_UR_"+channel+"_"+hfakereweight+"LargeEta_Nominal");
    	if (channel == "ejets" || channel == "mujets") hnames_HFake_13tev.push_back("Cutflow_14TeV_CutSRUpgrade_PhMatch_HFake_LpMatch_TrueLp_Wjets_Upgrade_UR_"+channel+"_"+hfakereweight+"LargeEta_Nominal");
    	fnames_HFake_14tev.push_back("14TeV_CutSRUpgrade_PhMatch_HFake_LpMatch_TrueLp_TTBar_Upgrade_UR_"+channel+"_"+hfakereweight+"Nominal_U07.root");
    	if (channel != "emu") fnames_HFake_14tev.push_back("14TeV_CutSRUpgrade_PhMatch_HFake_LpMatch_TrueLp_Zjets_Upgrade_UR_"+channel+"_"+hfakereweight+"Nominal_U07.root");
    	fnames_HFake_14tev.push_back("14TeV_CutSRUpgrade_PhMatch_HFake_LpMatch_TrueLp_ST_Upgrade_UR_"+channel+"_"+hfakereweight+"Nominal_U07.root");
    	fnames_HFake_14tev.push_back("14TeV_CutSRUpgrade_PhMatch_HFake_LpMatch_TrueLp_Diboson_Upgrade_UR_"+channel+"_"+hfakereweight+"Nominal_U07.root");
    	if (channel == "ejets" || channel == "mujets") fnames_HFake_14tev.push_back("14TeV_CutSRUpgrade_PhMatch_HFake_LpMatch_TrueLp_Wjets_Upgrade_UR_"+channel+"_"+hfakereweight+"Nominal_U07.root");
    	hnames_HFake_14tev.push_back("Cutflow_14TeV_CutSRUpgrade_PhMatch_HFake_LpMatch_TrueLp_TTBar_Upgrade_UR_"+channel+"_"+hfakereweight+"Nominal");
    	if (channel != "emu") hnames_HFake_14tev.push_back("Cutflow_14TeV_CutSRUpgrade_PhMatch_HFake_LpMatch_TrueLp_Zjets_Upgrade_UR_"+channel+"_"+hfakereweight+"Nominal");
    	hnames_HFake_14tev.push_back("Cutflow_14TeV_CutSRUpgrade_PhMatch_HFake_LpMatch_TrueLp_ST_Upgrade_UR_"+channel+"_"+hfakereweight+"Nominal");
    	hnames_HFake_14tev.push_back("Cutflow_14TeV_CutSRUpgrade_PhMatch_HFake_LpMatch_TrueLp_Diboson_Upgrade_UR_"+channel+"_"+hfakereweight+"Nominal");
    	if (channel == "ejets" || channel == "mujets") hnames_HFake_14tev.push_back("Cutflow_14TeV_CutSRUpgrade_PhMatch_HFake_LpMatch_TrueLp_Wjets_Upgrade_UR_"+channel+"_"+hfakereweight+"Nominal");
    	fnamess_13tev.push_back(fnames_HFake_13tev);
    	hnamess_13tev.push_back(hnames_HFake_13tev);
    	fnamess_14tev.push_back(fnames_HFake_14tev);
    	hnamess_14tev.push_back(hnames_HFake_14tev);

    	//vector<string> fnames_EFake_13tev; 
    	//vector<string> hnames_EFake_13tev; 
    	//vector<string> fnames_EFake_14tev; 
    	//vector<string> hnames_EFake_14tev; 
    	//fnames_EFake_13tev.push_back("13TeV_CutSR_PhMatch_EFake_Diboson_Reco_CR1_"+channel+"_Nominal_Final02.root");
    	//fnames_EFake_13tev.push_back("13TeV_CutSR_PhMatch_EFake_STOthers_Reco_CR1_"+channel+"_Nominal_Final02.root");
    	//fnames_EFake_13tev.push_back("13TeV_CutSR_PhMatch_EFake_STWT_Reco_CR1_"+channel+"_Nominal_Final02.root");
    	//fnames_EFake_13tev.push_back("13TeV_CutSR_PhMatch_EFake_TTBar_Reco_CR1_"+channel+"_Nominal_Final02.root");
    	//fnames_EFake_13tev.push_back("13TeV_CutSR_PhMatch_EFake_WjetsEl_Reco_CR1_"+channel+"_Nominal_Final02.root");
    	//fnames_EFake_13tev.push_back("13TeV_CutSR_PhMatch_EFake_WjetsMu_Reco_CR1_"+channel+"_Nominal_Final02.root");
    	//fnames_EFake_13tev.push_back("13TeV_CutSR_PhMatch_EFake_WjetsTau_Reco_CR1_"+channel+"_Nominal_Final02.root");
    	//fnames_EFake_13tev.push_back("13TeV_CutSR_PhMatch_EFake_ZjetsElEl_Reco_CR1_"+channel+"_Nominal_Final02.root");
    	//fnames_EFake_13tev.push_back("13TeV_CutSR_PhMatch_EFake_ZjetsMuMu_Reco_CR1_"+channel+"_Nominal_Final02.root");
    	//fnames_EFake_13tev.push_back("13TeV_CutSR_PhMatch_EFake_ZjetsTauTau_Reco_CR1_"+channel+"_Nominal_Final02.root");
    	//hnames_EFake_13tev.push_back("Cutflow_13TeV_CutSR_PhMatch_EFake_Diboson_Reco_CR1_"+channel+"_Nominal");
    	//hnames_EFake_13tev.push_back("Cutflow_13TeV_CutSR_PhMatch_EFake_STOthers_Reco_CR1_"+channel+"_Nominal");
    	//hnames_EFake_13tev.push_back("Cutflow_13TeV_CutSR_PhMatch_EFake_STWT_Reco_CR1_"+channel+"_Nominal");
    	//hnames_EFake_13tev.push_back("Cutflow_13TeV_CutSR_PhMatch_EFake_TTBar_Reco_CR1_"+channel+"_Nominal");
    	//hnames_EFake_13tev.push_back("Cutflow_13TeV_CutSR_PhMatch_EFake_WjetsEl_Reco_CR1_"+channel+"_Nominal");
    	//hnames_EFake_13tev.push_back("Cutflow_13TeV_CutSR_PhMatch_EFake_WjetsMu_Reco_CR1_"+channel+"_Nominal");
    	//hnames_EFake_13tev.push_back("Cutflow_13TeV_CutSR_PhMatch_EFake_WjetsTau_Reco_CR1_"+channel+"_Nominal");
    	//hnames_EFake_13tev.push_back("Cutflow_13TeV_CutSR_PhMatch_EFake_ZjetsElEl_Reco_CR1_"+channel+"_Nominal");
    	//hnames_EFake_13tev.push_back("Cutflow_13TeV_CutSR_PhMatch_EFake_ZjetsMuMu_Reco_CR1_"+channel+"_Nominal");
    	//hnames_EFake_13tev.push_back("Cutflow_13TeV_CutSR_PhMatch_EFake_ZjetsTauTau_Reco_CR1_"+channel+"_Nominal");
    	//if (channel == "ejets") {
    	//    fnames_EFake_14tev.push_back("14TeV_CutSRUpgrade_PhMatch_EFake_LpMatch_TrueLp_TTBar_Upgrade_UR_"+channel+"_Nominal_U07.root");
    	//	fnames_EFake_14tev.push_back("14TeV_CutSRUpgrade_PhMatch_EFake_LpMatch_TrueLp_Zjets_Upgrade_UR_"+channel+"_Nominal_U07.root");
    	//	hnames_EFake_14tev.push_back("Cutflow_14TeV_CutSRUpgrade_PhMatch_EFake_LpMatch_TrueLp_TTBar_Upgrade_UR_"+channel+"_Nominal");
    	//	hnames_EFake_14tev.push_back("Cutflow_14TeV_CutSRUpgrade_PhMatch_EFake_LpMatch_TrueLp_Zjets_Upgrade_UR_"+channel+"_Nominal");
    	//} else if (channel == "mujets") {
    	//    fnames_EFake_14tev.push_back("14TeV_CutSRUpgrade_PhMatch_EFake_LpMatch_TrueLp_TTBar_Upgrade_UR_"+channel+"_Nominal_U07.root");
    	//	hnames_EFake_14tev.push_back("Cutflow_14TeV_CutSRUpgrade_PhMatch_EFake_LpMatch_TrueLp_TTBar_Upgrade_UR_"+channel+"_Nominal");
    	//}
    	//fnamess_13tev.push_back(fnames_EFake_13tev);
    	//hnamess_13tev.push_back(hnames_EFake_13tev);
    	//fnamess_14tev.push_back(fnames_EFake_14tev);
    	//hnamess_14tev.push_back(hnames_EFake_14tev);

    	//vector<string> fnames_Prompt_13tev; 
    	//vector<string> hnames_Prompt_13tev; 
    	//vector<string> fnames_Prompt_14tev; 
    	//vector<string> hnames_Prompt_14tev; 
    	//fnames_Prompt_13tev.push_back("13TeV_CutSR_PhMatch_TruePh_WGammajetsElNLO_Reco_CR1_"+channel+"_Nominal_Final02.root");
    	//fnames_Prompt_13tev.push_back("13TeV_CutSR_PhMatch_TruePh_WGammajetsMuNLO_Reco_CR1_"+channel+"_Nominal_Final02.root");
    	//fnames_Prompt_13tev.push_back("13TeV_CutSR_PhMatch_TruePh_WGammajetsTauNLO_Reco_CR1_"+channel+"_Nominal_Final02.root");
    	//fnames_Prompt_13tev.push_back("13TeV_CutSR_PhMatch_TruePh_ZGammajetsElElNLO_Reco_CR1_"+channel+"_Nominal_Final02.root");
    	//fnames_Prompt_13tev.push_back("13TeV_CutSR_PhMatch_TruePh_ZGammajetsMuMuNLO_Reco_CR1_"+channel+"_Nominal_Final02.root");
    	//fnames_Prompt_13tev.push_back("13TeV_CutSR_PhMatch_TruePh_ZGammajetsTauTauNLO_Reco_CR1_"+channel+"_Nominal_Final02.root");
    	//fnames_Prompt_13tev.push_back("13TeV_CutSR_PhMatch_TruePh_Diboson_Reco_CR1_"+channel+"_Nominal_Final02.root");
    	//fnames_Prompt_13tev.push_back("13TeV_CutSR_PhMatch_TruePh_STWT_Reco_CR1_"+channel+"_Nominal_Final02.root");
    	//fnames_Prompt_13tev.push_back("13TeV_CutSR_PhMatch_TruePh_STOthers_Reco_CR1_"+channel+"_Nominal_Final02.root");
    	//hnames_Prompt_13tev.push_back("Cutflow_13TeV_CutSR_PhMatch_TruePh_WGammajetsElNLO_Reco_CR1_"+channel+"_Nominal");
    	//hnames_Prompt_13tev.push_back("Cutflow_13TeV_CutSR_PhMatch_TruePh_WGammajetsMuNLO_Reco_CR1_"+channel+"_Nominal");
    	//hnames_Prompt_13tev.push_back("Cutflow_13TeV_CutSR_PhMatch_TruePh_WGammajetsTauNLO_Reco_CR1_"+channel+"_Nominal");
    	//hnames_Prompt_13tev.push_back("Cutflow_13TeV_CutSR_PhMatch_TruePh_ZGammajetsElElNLO_Reco_CR1_"+channel+"_Nominal");
    	//hnames_Prompt_13tev.push_back("Cutflow_13TeV_CutSR_PhMatch_TruePh_ZGammajetsMuMuNLO_Reco_CR1_"+channel+"_Nominal");
    	//hnames_Prompt_13tev.push_back("Cutflow_13TeV_CutSR_PhMatch_TruePh_ZGammajetsTauTauNLO_Reco_CR1_"+channel+"_Nominal");
    	//hnames_Prompt_13tev.push_back("Cutflow_13TeV_CutSR_PhMatch_TruePh_Diboson_Reco_CR1_"+channel+"_Nominal");
    	//hnames_Prompt_13tev.push_back("Cutflow_13TeV_CutSR_PhMatch_TruePh_STWT_Reco_CR1_"+channel+"_Nominal");
    	//hnames_Prompt_13tev.push_back("Cutflow_13TeV_CutSR_PhMatch_TruePh_STOthers_Reco_CR1_"+channel+"_Nominal");
	//fnames_Prompt_14tev.push_back("14TeV_CutSRUpgrade_PhMatch_TruePh_LpMatch_TrueLp_ST_Upgrade_UR_"+channel+"_Nominal_U07.root");
	//if (channel != "emu") fnames_Prompt_14tev.push_back("14TeV_CutSRUpgrade_PhMatch_TruePh_LpMatch_TrueLp_Diboson_Upgrade_UR_"+channel+"_Nominal_U07.root");
	//if (channel == "ejets" || channel == "mujets") fnames_Prompt_14tev.push_back("14TeV_CutSRUpgrade_PhMatch_TruePh_LpMatch_TrueLp_Wjets_Upgrade_UR_"+channel+"_Nominal_U07.root");
	//hnames_Prompt_14tev.push_back("Cutflow_14TeV_CutSRUpgrade_PhMatch_TruePh_LpMatch_TrueLp_ST_Upgrade_UR_"+channel+"_Nominal");
	//if (channel != "emu") hnames_Prompt_14tev.push_back("Cutflow_14TeV_CutSRUpgrade_PhMatch_TruePh_LpMatch_TrueLp_Diboson_Upgrade_UR_"+channel+"_Nominal");
	//if (channel == "ejets" || channel == "mujets") hnames_Prompt_14tev.push_back("Cutflow_14TeV_CutSRUpgrade_PhMatch_TruePh_LpMatch_TrueLp_Wjets_Upgrade_UR_"+channel+"_Nominal");
	//if (channel == "ejets") {
	//    fnames_Prompt_14tev.push_back("13TeV_CutSR_PhMatch_TruePh_ZGammajetsElElNLO_Reco_CR1_"+channel+"_ZGammaReweighted_Nominal_Final06.root");
	//    hnames_Prompt_14tev.push_back("Cutflow_13TeV_CutSR_PhMatch_TruePh_ZGammajetsElElNLO_Reco_CR1_"+channel+"_ZGammaReweighted_Nominal");
	//    fnames_Prompt_14tev.push_back("13TeV_CutSR_PhMatch_TruePh_ZGammajetsTauTauNLO_Reco_CR1_"+channel+"_ZGammaReweighted_Nominal_Final06.root");
	//    hnames_Prompt_14tev.push_back("Cutflow_13TeV_CutSR_PhMatch_TruePh_ZGammajetsTauTauNLO_Reco_CR1_"+channel+"_ZGammaReweighted_Nominal");
	//}
	//if (channel == "mujets") {
	//    fnames_Prompt_14tev.push_back("13TeV_CutSR_PhMatch_TruePh_ZGammajetsMuMuNLO_Reco_CR1_"+channel+"_ZGammaReweighted_Nominal_Final06.root");
	//    hnames_Prompt_14tev.push_back("Cutflow_13TeV_CutSR_PhMatch_TruePh_ZGammajetsMuMuNLO_Reco_CR1_"+channel+"_ZGammaReweighted_Nominal");
	//    fnames_Prompt_14tev.push_back("13TeV_CutSR_PhMatch_TruePh_ZGammajetsTauTauNLO_Reco_CR1_"+channel+"_ZGammaReweighted_Nominal_Final06.root");
	//    hnames_Prompt_14tev.push_back("Cutflow_13TeV_CutSR_PhMatch_TruePh_ZGammajetsTauTauNLO_Reco_CR1_"+channel+"_ZGammaReweighted_Nominal");
	//}
	//if (channel == "ee") {
	//    fnames_Prompt_14tev.push_back("13TeV_CutSR_PhMatch_TruePh_ZGammajetsElElNLO_Reco_CR1_"+channel+"_ZGammaReweighted_Nominal_Final06.root");
	//    hnames_Prompt_14tev.push_back("Cutflow_13TeV_CutSR_PhMatch_TruePh_ZGammajetsElElNLO_Reco_CR1_"+channel+"_ZGammaReweighted_Nominal");
	//    fnames_Prompt_14tev.push_back("13TeV_CutSR_PhMatch_TruePh_ZGammajetsTauTauNLO_Reco_CR1_"+channel+"_ZGammaReweighted_Nominal_Final06.root");
	//    hnames_Prompt_14tev.push_back("Cutflow_13TeV_CutSR_PhMatch_TruePh_ZGammajetsTauTauNLO_Reco_CR1_"+channel+"_ZGammaReweighted_Nominal");
	//}
	//if (channel == "mumu") {
	//    fnames_Prompt_14tev.push_back("13TeV_CutSR_PhMatch_TruePh_ZGammajetsMuMuNLO_Reco_CR1_"+channel+"_ZGammaReweighted_Nominal_Final06.root");
	//    hnames_Prompt_14tev.push_back("Cutflow_13TeV_CutSR_PhMatch_TruePh_ZGammajetsMuMuNLO_Reco_CR1_"+channel+"_ZGammaReweighted_Nominal");
	//    fnames_Prompt_14tev.push_back("13TeV_CutSR_PhMatch_TruePh_ZGammajetsTauTauNLO_Reco_CR1_"+channel+"_ZGammaReweighted_Nominal_Final06.root");
	//    hnames_Prompt_14tev.push_back("Cutflow_13TeV_CutSR_PhMatch_TruePh_ZGammajetsTauTauNLO_Reco_CR1_"+channel+"_ZGammaReweighted_Nominal");
	//}
	//if (channel == "emu") {
	//    fnames_Prompt_14tev.push_back("13TeV_CutSR_PhMatch_TruePh_ZGammajetsTauTauNLO_Reco_CR1_"+channel+"_ZGammaReweighted_Nominal_Final06.root");
	//    hnames_Prompt_14tev.push_back("Cutflow_13TeV_CutSR_PhMatch_TruePh_ZGammajetsTauTauNLO_Reco_CR1_"+channel+"_ZGammaReweighted_Nominal");
	//}
    	//fnamess_13tev.push_back(fnames_Prompt_13tev);
    	//hnamess_13tev.push_back(hnames_Prompt_13tev);
    	//fnamess_14tev.push_back(fnames_Prompt_14tev);
    	//hnamess_14tev.push_back(hnames_Prompt_14tev);

    	//vector<string> fnames_FakeLep_13tev; 
    	//vector<string> hnames_FakeLep_13tev; 
    	//vector<string> fnames_FakeLep_14tev; 
    	//vector<string> hnames_FakeLep_14tev; 
    	//if (channel == "ejets") {
	//	fnames_FakeLep_13tev.push_back("13TeV_CutSR_QCD_CR1_"+channel+"_UPS_eta_mtw_Nominal_Final06.root");
    	//	hnames_FakeLep_13tev.push_back("Cutflow_13TeV_CutSR_QCD_CR1_"+channel+"_UPS_eta_mtw_Nominal");
    	//	fnames_FakeLep_14tev.push_back("13TeV_CutSR_QCD_CR1_"+channel+"_UPS_eta_mtw_ZGammaReweighted_Nominal_Final06.root");
    	//	hnames_FakeLep_14tev.push_back("Cutflow_13TeV_CutSR_QCD_CR1_"+channel+"_UPS_eta_mtw_ZGammaReweighted_Nominal");
    	//} else if (channel == "mujets") {
	//	fnames_FakeLep_13tev.push_back("13TeV_CutSR_QCD_CR1_"+channel+"_PS_pt_mtw_Nominal_Final06.root");
    	//	hnames_FakeLep_13tev.push_back("Cutflow_13TeV_CutSR_QCD_CR1_"+channel+"_PS_pt_mtw_Nominal");
    	//	fnames_FakeLep_14tev.push_back("13TeV_CutSR_QCD_CR1_"+channel+"_PS_pt_mtw_ZGammaReweighted_Nominal_Final06.root");
    	//	hnames_FakeLep_14tev.push_back("Cutflow_13TeV_CutSR_QCD_CR1_"+channel+"_PS_pt_mtw_ZGammaReweighted_Nominal");
    	//}
    	//fnamess_13tev.push_back(fnames_FakeLep_13tev);
    	//hnamess_13tev.push_back(hnames_FakeLep_13tev);
    	//fnamess_14tev.push_back(fnames_FakeLep_14tev);
    	//hnamess_14tev.push_back(hnames_FakeLep_14tev);

    	vector<double> proc_ns_13tev;
    	vector<double> proc_es_13tev;
    	vector<double> proc_ns_14tev;
    	vector<double> proc_es_14tev;

	//vector<pair<string,double> > Signal_Cutflow_13tev;
	//vector<pair<string,double> > Signal_Cutflow_14tev;
	//vector<TH1F*> hs_leadphpt_14tev;
	//vector<TH1F*> hs_leadpheta_14tev;
	//vector<TH1F*> hs_drphlep_14tev;
	//vector<TH1F*> hs_detall_14tev;
	//vector<TH1F*> hs_dphill_14tev;

    	for (int ipro = 0; ipro < processes.size(); ipro ++) {
	    if (debug) cout << "tag0" << endl;
    	    vector<string> fnames_13tev = fnamess_13tev.at(ipro);
	    vector<string> hnames_13tev = hnamess_13tev.at(ipro);
	    double ns_13tev = 0;
	    double es_13tev = 0;
	    for (int i = 0; i < fnames_13tev.size(); i++) {
	        string fname_13tev = dir_13tev + fnames_13tev.at(i);
	        string hname_13tev = hnames_13tev.at(i);
	        TFile* f_13tev = new TFile(fname_13tev.c_str());
	        TH1F* h_cf_13tev = (TH1F*)f_13tev->Get(hname_13tev.c_str());
	        if (!h_cf_13tev) {
	    	f_13tev->ls();
	    	cout << "can't find " << hname_13tev << endl;
	    	exit;
	        }
	        double n_13tev, e_13tev; FindLastBin(h_cf_13tev, n_13tev, e_13tev);
	        //if (processes.at(ipro) == "Signal") RecordCutflow(h_cf_13tev, Signal_Cutflow_13tev, channel, "13tev");

		//if (processes.at(ipro) == "HFake") {
		//    n_13tev *= 1.5;
		//    e_13tev *= 1.5;
		//}
		//if (processes.at(ipro) == "Signal" && (channel == "ejets" || channel == "mujets")) {
    	    	//    n_13tev *= 1.30/1.16;
		//    e_13tev *= 1.30/1.16;
    	    	//}
    	    	//if (processes.at(ipro) == "Signal" && (channel == "ee" || channel == "emu" || channel == "mumu")) {
    	    	//    n_13tev *= 1.44/1.16;
    	    	//    e_13tev *= 1.44/1.16;
    	    	//}

	        ns_13tev += n_13tev;
	        es_13tev = sqrt(pow(es_13tev,2) + pow(e_13tev,2));
	    }
	    vector<string> fnames_14tev = fnamess_14tev.at(ipro);
	    vector<string> hnames_14tev = hnamess_14tev.at(ipro);
	    double ns_14tev = 0;
	    double es_14tev = 0;
	    //TH1F* h_leadphpt_14tev = NULL;
	    //TH1F* h_leadpheta_14tev = NULL;
	    //TH1F* h_drphlep_14tev = NULL;
	    //TH1F* h_detall_14tev = NULL;
	    //TH1F* h_dphill_14tev = NULL;
	    if (debug) cout << "tag01" << endl;
	    for (int i = 0; i < fnames_14tev.size(); i++) {
	        string fname_14tev;
		if (fname_14tev.find("13TeV",0) == string::npos) fname_14tev = dir_14tev + fnames_14tev.at(i);
		else fname_14tev = dir_13tev + fnames_14tev.at(i);
	        string hname_14tev = hnames_14tev.at(i);
		if (debug) cout << "tag02" << endl;
	        TFile* f_14tev = new TFile(fname_14tev.c_str());
		if (debug) cout << "tag03" << endl;
	        TH1F* h_cf_14tev = (TH1F*)f_14tev->Get(hname_14tev.c_str());
		if (debug) cout << "tag04" << endl;
	        if (!h_cf_14tev) {
			f_14tev->ls();
	        	cout << "can't find " << hname_14tev << endl;
	        	exit;
	        }

	        double n_14tev, e_14tev; FindLastBin(h_cf_14tev, n_14tev, e_14tev);
	        //if (processes.at(ipro) == "Signal") RecordCutflow(h_cf_14tev, Signal_Cutflow_14tev, channel, "14tev");
		//if (debug) cout << "tag1" << endl;
		//string hname_14tev_leadphpt = hname_14tev; 
		//hname_14tev_leadphpt.replace(0, 7, "LeadPhPtUnfold");
		//TH1F* h_tmp_leadphpt_14tev = (TH1F*)f_14tev->Get(hname_14tev_leadphpt.c_str());
		//string hname_14tev_leadpheta = hname_14tev; 
		//hname_14tev_leadpheta.replace(0, 7, "LeadPhAbsEtaUnfold");
		//TH1F* h_tmp_leadpheta_14tev = (TH1F*)f_14tev->Get(hname_14tev_leadpheta.c_str());
		//string hname_14tev_drphlep = hname_14tev; 
		//hname_14tev_drphlep.replace(0, 7, "LeadPhMinDrPhLepUnfold");
		//TH1F* h_tmp_drphlep_14tev = (TH1F*)f_14tev->Get(hname_14tev_drphlep.c_str());
		//string hname_14tev_detall = hname_14tev; 
		//hname_14tev_detall.replace(0, 7, "DEtaLepLepUnfold");
		//TH1F* h_tmp_detall_14tev = (TH1F*)f_14tev->Get(hname_14tev_detall.c_str());
		//string hname_14tev_dphill = hname_14tev; 
		//hname_14tev_dphill.replace(0, 7, "DPhiLepLepUnfold");
		//TH1F* h_tmp_dphill_14tev = (TH1F*)f_14tev->Get(hname_14tev_dphill.c_str());

		double scale = 1;
		if (fname_14tev.find("ZGammajets",0) != string::npos) scale = 83*1.08;
		if (fname_14tev.find("QCD",0) != string::npos) scale = 83;

		n_14tev *= scale;
		e_14tev *= scale;
		//if (h_tmp_leadphpt_14tev != NULL) h_tmp_leadphpt_14tev->Scale(scale);
		//if (h_tmp_leadpheta_14tev != NULL) h_tmp_leadpheta_14tev->Scale(scale);
		//if (h_tmp_drphlep_14tev != NULL) h_tmp_drphlep_14tev->Scale(scale);
		//if (h_tmp_detall_14tev != NULL) h_tmp_detall_14tev->Scale(scale);
		//if (h_tmp_dphill_14tev != NULL) h_tmp_dphill_14tev->Scale(scale);

	        ns_14tev += n_14tev;
	        es_14tev = sqrt(pow(es_14tev,2) + pow(e_14tev,2));
		//if (h_leadphpt_14tev == NULL) h_leadphpt_14tev = h_tmp_leadphpt_14tev;
		//else h_leadphpt_14tev->Add(h_tmp_leadphpt_14tev);
		//if (h_leadpheta_14tev == NULL) h_leadpheta_14tev = h_tmp_leadpheta_14tev;
		//else h_leadpheta_14tev->Add(h_tmp_leadpheta_14tev);
		//if (h_drphlep_14tev == NULL) h_drphlep_14tev = h_tmp_drphlep_14tev;
		//else h_drphlep_14tev->Add(h_tmp_drphlep_14tev);
		//if (h_detall_14tev == NULL) h_detall_14tev = h_tmp_detall_14tev;
		//else h_detall_14tev->Add(h_tmp_detall_14tev);
		//if (h_dphill_14tev == NULL) h_dphill_14tev = h_tmp_dphill_14tev;
		//else h_dphill_14tev->Add(h_tmp_dphill_14tev);
	    }
	    if (debug) cout << "tag13" << endl;
    	    //if (processes.at(ipro) == "HFake") {
	    //    if (channel == "ejets") scale = 0.36158;
	    //    if (channel == "mujets") scale = 0.35294;
	    //    if (channel == "ee") scale = 0.16889;
	    //    if (channel == "emu") scale = 0.11048;
	    //    if (channel == "mumu") scale = 0.087362;
    	    //}
	    //if (h_leadphpt_14tev != NULL) for (int ib = 1; ib <= h_leadphpt_14tev->GetNbinsX(); ib++) h_leadphpt_14tev->SetBinError(ib, 0);
	    //if (h_leadpheta_14tev != NULL) for (int ib = 1; ib <= h_leadpheta_14tev->GetNbinsX(); ib++) h_leadpheta_14tev->SetBinError(ib, 0);
	    //if (h_drphlep_14tev != NULL) for (int ib = 1; ib <= h_drphlep_14tev->GetNbinsX(); ib++) h_drphlep_14tev->SetBinError(ib, 0);
	    //if (h_detall_14tev != NULL) for (int ib = 1; ib <= h_detall_14tev->GetNbinsX(); ib++) h_detall_14tev->SetBinError(ib, 0);
	    //if (h_dphill_14tev != NULL) for (int ib = 1; ib <= h_dphill_14tev->GetNbinsX(); ib++) h_dphill_14tev->SetBinError(ib, 0);
	    //if (debug) cout << "tag14" << endl;
    	    proc_ns_13tev.push_back(ns_13tev);
    	    proc_es_13tev.push_back(es_13tev);
    	    proc_ns_14tev.push_back(ns_14tev);
    	    proc_es_14tev.push_back(es_14tev);
	    //hs_leadphpt_14tev.push_back(h_leadphpt_14tev);
	    //hs_leadpheta_14tev.push_back(h_leadpheta_14tev);
	    //hs_drphlep_14tev.push_back(h_drphlep_14tev);
	    //hs_detall_14tev.push_back(h_detall_14tev);
	    //hs_dphill_14tev.push_back(h_dphill_14tev);
	    //if (debug) cout << "tag15" << endl;
    	}
	proc_nss_13tev.push_back(proc_ns_13tev);
	proc_ess_13tev.push_back(proc_es_13tev);
	proc_nss_14tev.push_back(proc_ns_14tev);
	proc_ess_14tev.push_back(proc_es_14tev);
	if (debug) cout << "tag2" << endl;

	//Signal_Cutflows_13tev.push_back(Signal_Cutflow_13tev);
	//Signal_Cutflows_14tev.push_back(Signal_Cutflow_14tev);
	//hss_leadphpt_14tev.push_back(hs_leadphpt_14tev);
	//hss_leadpheta_14tev.push_back(hs_leadpheta_14tev);
	//hss_drphlep_14tev.push_back(hs_drphlep_14tev);
	//hss_detall_14tev.push_back(hs_detall_14tev);
	//hss_dphill_14tev.push_back(hs_dphill_14tev);

	if (debug) cout << "tag21" << endl;

    }
	   cout << "\\scalebox{0.8}{" << endl;
    	   cout << "\\begin{tabular}{c|c|";
    	   for (int i = 0; i < channels.size(); i++) {
    	       cout << "c";
    	       if (i != channels.size() - 1) cout << "|";
    	       else cout << "}";
    	   }
    	   cout << endl;
	   cout << "\\hline" << endl;
	   cout << "\\hline" << endl;
    	   cout << setw(15) << "\\multicolumn{2}{c|}{Channel} " << endl;
    	   cout << setw(20) << " &";
    	   for (int i = 0; i < channels.size(); i++) {
    	       cout << setw(20) << channel_titles.at(i);
    	       if (i != channels.size() - 1) cout << " & ";
    	       else cout << " \\\\";
    	   }
    	   cout << endl;
	   cout << "\\hline" << endl;

    	cout << "\\multirow{2}{*}{" << processes.at(0) << "} " << endl;
    	cout << setw(20) << ("& " + HLlabel + " &");
    	for (int i = 0; i < channels.size(); i++) {
    	    char tmp[100];
    	    if (proc_nss_14tev.at(i).at(0) != 0) sprintf(tmp, "%.1f +/- %.1f", proc_nss_14tev.at(i).at(0), proc_ess_14tev.at(i).at(0));
    	    else sprintf(tmp, "-");
    	    cout << setw(20) << tmp;
    	    if (i != channels.size() - 1) cout << " & ";
    	    else cout << " \\\\";
    	}
    	cout << endl;
    	cout << setw(20) << ("& " + run2label + " &");
    	for (int i = 0; i < channels.size(); i++) {
    	    char tmp[100];
    	    if (proc_nss_13tev.at(i).at(0) != 0) sprintf(tmp, "%.1f +/- %.1f", proc_nss_13tev.at(i).at(0), proc_ess_13tev.at(i).at(0));
    	    else sprintf(tmp, "-");
    	    cout << setw(20) << tmp;
    	    if (i != channels.size() - 1) cout << " & ";
    	    else cout << " \\\\";
    	}
    	cout << endl;
	    cout << "\\hline" << endl;
    	cout << "\\multirow{2}{*}{" << processes.at(1) << "} " << endl;
    	cout << setw(20) << ("& " + HLlabel + " &");
    	for (int i = 0; i < channels.size(); i++) {
    	    char tmp[100];
    	    if (proc_nss_14tev.at(i).at(1) != 0) sprintf(tmp, "%.1f +/- %.1f", proc_nss_14tev.at(i).at(1), proc_ess_14tev.at(i).at(1));
    	    else sprintf(tmp, "-");
    	    cout << setw(20) << tmp;
    	    if (i != channels.size() - 1) cout << " & ";
    	    else cout << " \\\\";
    	}
    	cout << endl;
    	cout << setw(20) << ("& " + run2label + " &");
    	for (int i = 0; i < channels.size(); i++) {
    	    char tmp[100];
    	    if (proc_nss_13tev.at(i).at(1) != 0) sprintf(tmp, "%.1f +/- %.1f", proc_nss_13tev.at(i).at(1), proc_ess_13tev.at(i).at(1));
    	    else sprintf(tmp, "-");
    	    cout << setw(20) << tmp;
    	    if (i != channels.size() - 1) cout << " & ";
    	    else cout << " \\\\";
    	}
    	cout << endl;
	    cout << "\\hline" << endl;
	    cout << "\\hline" << endl;
	   cout << "\\end{tabular}}" << endl;
	   cout << endl;

	   ofstream ofile;
	   ofile.open("log_cf3");

	   ofile << "\\scalebox{0.8}{" << endl;
    	   ofile << "\\begin{tabular}{c|c|";
    	   for (int i = 0; i < channels.size(); i++) {
    	       ofile << "c";
    	       if (i != channels.size() - 1) ofile << "|";
    	       else ofile << "}";
    	   }
    	   ofile << endl;
	   ofile << "\\hline" << endl;
	   ofile << "\\hline" << endl;
    	   ofile << setw(15) << "\\multicolumn{2}{c|}{Channel} " << endl;
    	   ofile << setw(20) << " &";
    	   for (int i = 0; i < channels.size(); i++) {
    	       ofile << setw(20) << channel_titles.at(i);
    	       if (i != channels.size() - 1) ofile << " & ";
    	       else ofile << " \\\\";
    	   }
    	   ofile << endl;
	   ofile << "\\hline" << endl;

    	ofile << "\\multirow{2}{*}{" << processes.at(0) << "} " << endl;
    	ofile << setw(20) << ("& " + HLlabel + " &");
    	for (int i = 0; i < channels.size(); i++) {
    	    char tmp[100];
    	    if (proc_nss_14tev.at(i).at(0) != 0) sprintf(tmp, "%.1f +/- %.1f", proc_nss_14tev.at(i).at(0), proc_ess_14tev.at(i).at(0));
    	    else sprintf(tmp, "-");
    	    ofile << setw(20) << tmp;
    	    if (i != channels.size() - 1) ofile << " & ";
    	    else ofile << " \\\\";
    	}
    	ofile << endl;
    	ofile << setw(20) << ("& " + run2label + " &");
    	for (int i = 0; i < channels.size(); i++) {
    	    char tmp[100];
    	    if (proc_nss_13tev.at(i).at(0) != 0) sprintf(tmp, "%.1f +/- %.1f", proc_nss_13tev.at(i).at(0), proc_ess_13tev.at(i).at(0));
    	    else sprintf(tmp, "-");
    	    ofile << setw(20) << tmp;
    	    if (i != channels.size() - 1) ofile << " & ";
    	    else ofile << " \\\\";
    	}
    	ofile << endl;
	    ofile << "\\hline" << endl;
    	ofile << "\\multirow{2}{*}{" << processes.at(1) << "} " << endl;
    	ofile << setw(20) << ("& " + HLlabel + " &");
    	for (int i = 0; i < channels.size(); i++) {
    	    char tmp[100];
    	    if (proc_nss_14tev.at(i).at(1) != 0) sprintf(tmp, "%.1f +/- %.1f", proc_nss_14tev.at(i).at(1), proc_ess_14tev.at(i).at(1));
    	    else sprintf(tmp, "-");
    	    ofile << setw(20) << tmp;
    	    if (i != channels.size() - 1) ofile << " & ";
    	    else ofile << " \\\\";
    	}
    	ofile << endl;
    	ofile << setw(20) << ("& " + run2label + " &");
    	for (int i = 0; i < channels.size(); i++) {
    	    char tmp[100];
    	    if (proc_nss_13tev.at(i).at(1) != 0) sprintf(tmp, "%.1f +/- %.1f", proc_nss_13tev.at(i).at(1), proc_ess_13tev.at(i).at(1));
    	    else sprintf(tmp, "-");
    	    ofile << setw(20) << tmp;
    	    if (i != channels.size() - 1) ofile << " & ";
    	    else ofile << " \\\\";
    	}
    	ofile << endl;
	    ofile << "\\hline" << endl;
	    ofile << "\\hline" << endl;
	   ofile << "\\end{tabular}}" << endl;
	   ofile << endl;
    
    if (debug) cout << "tag3" << endl;

    //for (int ich = 0; ich < channels.size(); ich++) {
    //    string channel = channels.at(ich);
    //    vector<pair<string, double > > Signal_Cutflow_13tev = Signal_Cutflows_13tev.at(ich);
    //    vector<pair<string, double > > Signal_Cutflow_14tev = Signal_Cutflows_14tev.at(ich);

    //    if (ich == 0) {
    //       cout << "\\scalebox{1.0}{" << endl;
    //	   cout << "\\begin{tabular}{c|c|";
    //	   for (int i = 0; i < Signal_Cutflow_13tev.size(); i++) {
    //	       cout << "c";
    //	       if (i != Signal_Cutflow_13tev.size() - 1) cout << "|";
    //	       else cout << "}";
    //	   }
    //	   cout << endl;
    //       cout << "\\hline" << endl;
    //       cout << "\\hline" << endl;
    //	   cout << setw(15) << "\\multicolumn{2}{c|}{Cut} " << endl;
    //	   cout << setw(20) << " &";
    //        for (int i = 0; i < Signal_Cutflow_13tev.size(); i++) {
    //            string cutname = Signal_Cutflow_13tev.at(i).first;
    //            cout << setw(10) << cutname;
    //            if (i != Signal_Cutflow_13tev.size() - 1) cout << " &";
    //            else cout << " \\\\";
    //        }
    //        cout << endl;
    //    }

    //    cout << "\\hline" << endl;
    //	cout << "\\multirow{3}{*}{" << channel_titles.at(ich) << "} " << endl;
    //	cout << setw(20) << ("& " + HLlabel + " &");
    //    for (int i = 0; i < Signal_Cutflow_14tev.size(); i++) {
    //        double signal_14tev = Signal_Cutflow_14tev.at(i).second;
    //        if (signal_14tev != 0) cout << setw(10) << signal_14tev;
    //        else cout << setw(10) << "-";
    //        if (i != Signal_Cutflow_14tev.size() - 1) cout << " &";
    //        else cout << " \\\\";
    //    }
    //    cout << endl;

    //	cout << setw(20) << ("& " + run2label + "&");
    //    for (int i = 0; i < Signal_Cutflow_13tev.size(); i++) {
    //        double signal_13tev = Signal_Cutflow_13tev.at(i).second;
    //        if (signal_13tev != 0) cout << setw(10) << signal_13tev;
    //        else cout << setw(10) << "-";
    //        if (i != Signal_Cutflow_13tev.size() - 1) cout << " &";
    //        else cout << " \\\\";
    //    }
    //    cout << endl;

    //    TH1F* h_ratio_signal = new TH1F(string(channel+"_ratio_signal").c_str(),"",Signal_Cutflow_13tev.size(),0,Signal_Cutflow_13tev.size());
    //	cout << setw(20) << "& Ratio &";
    //    for (int i = 0; i < Signal_Cutflow_13tev.size(); i++) {
    //        double signal_13tev = Signal_Cutflow_13tev.at(i).second;
    //        double signal_14tev = Signal_Cutflow_14tev.at(i).second;
    //        double ratio = signal_14tev/signal_13tev;
    //        if (signal_13tev != 0 && signal_14tev != 0 ) {
    //    	cout << setw(10) << ratio;
    //    	h_ratio_signal->SetBinContent(i+1, ratio);
    //        } else {
    //    	cout << setw(10) << "-";
    //    	h_ratio_signal->SetBinContent(i+1, 0);
    //        }
    //        h_ratio_signal->GetXaxis()->SetBinLabel(i+1, Signal_Cutflow_13tev.at(i).first.c_str());
    //        h_ratio_signal->SetBinError(i+1, 0);
    //        if (i != Signal_Cutflow_14tev.size() - 1) cout << " &";
    //        else cout << " \\\\";
    //    }
    //    h_ratio_signals.push_back(h_ratio_signal);
    //    cout << endl;

    //    if (ich == channels.size() - 1) {
    //        cout << "\\hline" << endl;
    //        cout << "\\hline" << endl;
    //       cout << "\\end{tabular}}" << endl;
    //       cout << endl;
    //    }
    //}

    //SetAtlasStyle();
    //gStyle->SetErrorX(0.0001);

    //{
    //    TCanvas *c = new TCanvas("c","c",800,600);
    //	c->SetLeftMargin(0.15);
    //	c->SetRightMargin(0.10);
    //    //c->SetLogy(true);

    //    TLegend *lg = new TLegend(0.20, 0.64, 0.80, 0.74);
    //    lg->SetNColumns(3);
    //    lg->SetTextSize(0.04);
    //    lg->SetTextFont(42);
    //    lg->SetBorderSize(0);
    //    lg->SetFillColor(0);

    //	for (int i = 0; i < h_ratio_procs.size(); i++) {
    //	    h_ratio_procs.at(i)->SetMarkerStyle(30+i-1);
    //	    //if ((i+1)%5 != 0) h_ratio_procs.at(i)->SetMarkerStyle(30+i);
    //        //else h_ratio_procs.at(i)->SetMarkerStyle(30+i+1);
    //        h_ratio_procs.at(i)->SetMarkerSize(1.3);
    //	    if ((i+1)%5 != 0) h_ratio_procs.at(i)->SetMarkerColor(i+1);
    //        else h_ratio_procs.at(i)->SetMarkerColor(i+2);
    //	    if ((i+1)%5 != 0) h_ratio_procs.at(i)->SetLineColor(i+1);
    //        else h_ratio_procs.at(i)->SetLineColor(i+2);
    //	    h_ratio_procs.at(i)->SetLineWidth(1);
    //        if (i == 0) {
    //    	h_ratio_procs.at(i)->GetYaxis()->SetRangeUser(0,450);
    //    	h_ratio_procs.at(i)->GetYaxis()->SetTitle("HL-LHC / 13 TeV");
    //    	h_ratio_procs.at(i)->GetYaxis()->SetTitleOffset(1.4);
    //    	h_ratio_procs.at(i)->GetXaxis()->SetTitle("Process");
    //    	h_ratio_procs.at(i)->GetXaxis()->SetNdivisions(505);
    //    	h_ratio_procs.at(i)->GetXaxis()->SetLabelSize(0.05);
    //    	h_ratio_procs.at(i)->GetXaxis()->SetTitleOffset(1.4);
    //    	h_ratio_procs.at(i)->Draw("E P");
    //        } else {
    //    	h_ratio_procs.at(i)->Draw("same E P");
    //        }
    //        lg->AddEntry(h_ratio_procs.at(i), channels.at(i).c_str(), "P");
    //	}
    //    lg->Draw("same");

    //    TLatex lt; 
    //    lt.SetNDC();
    //    lt.SetTextFont(72);
    //    lt.SetTextSize(0.04);
    //    lt.DrawLatex(0.20, 0.88, "ATLAS");
    //    lt.SetTextFont(42);
    //    lt.DrawLatex(0.3, 0.88, "Internal Simulation");
    //    lt.DrawLatex(0.20, 0.82, "#sqrt{s}=14TeV, 3000 fb^{-1} v.s. #sqrt{s}=13TeV, 36 fb^{-1} ");
    //    lt.DrawLatex(0.20, 0.76, "Ratio of Event yeild");

    //    string savetag = "plots/Upgrade/Proc_ratio";
    //    c->SaveAs(string(savetag + ".png").c_str());
    //    c->SaveAs(string(savetag + ".pdf").c_str());
    //}

    //{
    //    TCanvas *c = new TCanvas("c","c",800,600);
    //	c->SetLeftMargin(0.15);
    //	c->SetRightMargin(0.10);

    //    TLegend *lg = new TLegend(0.20, 0.64, 0.80, 0.74);
    //    lg->SetNColumns(3);
    //    lg->SetTextSize(0.04);
    //    lg->SetTextFont(42);
    //    lg->SetBorderSize(0);
    //    lg->SetFillColor(0);

    //	for (int i = 0; i < channels.size(); i++) {
    //	    h_ratio_signals.at(i)->SetMarkerStyle(30+i-1);
    //        h_ratio_signals.at(i)->SetMarkerSize(1.3);
    //	    if ((i+1)%5 != 0) h_ratio_signals.at(i)->SetMarkerColor(i+1);
    //        else h_ratio_signals.at(i)->SetMarkerColor(i+2);
    //	    if ((i+1)%5 != 0) h_ratio_signals.at(i)->SetLineColor(i+1);
    //        else h_ratio_signals.at(i)->SetLineColor(i+2);
    //	    h_ratio_signals.at(i)->SetLineWidth(1);
    //        if (i == 0) {
    //    	h_ratio_signals.at(i)->GetXaxis()->SetTitle("Cut");
    //    	h_ratio_signals.at(i)->GetYaxis()->SetTitle("HL-LHC / 13 TeV");
    //    	h_ratio_signals.at(i)->GetYaxis()->SetRangeUser(30,280);
    //    	h_ratio_signals.at(i)->GetYaxis()->SetTitleOffset(1.4);
    //    	h_ratio_signals.at(i)->GetXaxis()->SetNdivisions(909);
    //    	h_ratio_signals.at(i)->GetXaxis()->SetLabelSize(0.05);
    //    	h_ratio_signals.at(i)->GetXaxis()->SetTitleOffset(1.4);
    //    	h_ratio_signals.at(i)->Draw("E P");
    //        } else {
    //    	h_ratio_signals.at(i)->Draw("same E P");
    //        }
    //        lg->AddEntry(h_ratio_signals.at(i), channels.at(i).c_str(), "P");
    //	}
    //    lg->Draw("same");

    //    TLatex lt; 
    //    lt.SetNDC();
    //    lt.SetTextFont(72);
    //    lt.SetTextSize(0.04);
    //    lt.DrawLatex(0.20, 0.88, "ATLAS");
    //    lt.SetTextFont(42);
    //    lt.DrawLatex(0.30, 0.88, "Internal Simulation");
    //    lt.DrawLatex(0.20, 0.82, "#sqrt{s}=14TeV, 3000 fb^{-1} v.s. #sqrt{s}=13TeV, 36 fb^{-1} ");
    //    lt.DrawLatex(0.20, 0.76, "Signal cutflow");

    //    string savetag = "plots/Upgrade/Signal_ratio";
    //    c->SaveAs(string(savetag + ".png").c_str());
    //    c->SaveAs(string(savetag + ".pdf").c_str());
    //}
}

void FindLastBin(TH1F* h, double &n, double &e, bool debug) {
    bool found = false;
    for (int i = 1; i <= h->GetNbinsX(); i++) {
	if (debug) cout << h->GetXaxis()->GetBinLabel(i) << " " << h->GetBinContent(i) << endl;
	if (string(h->GetXaxis()->GetBinLabel(i)).find("DrPhLep1.0",0) != string::npos) {
	    n = h->GetBinContent(i);
	    e = h->GetBinError(i);
	    found = true;
	    break;
	}
    }
    if (!found) {
	n = 0;
	e = 0;
    }
}

void RecordCutflow(TH1F* h, vector<pair<string,double> > &cf, string channel, string cme) {
    for (int i = 1; i <= h->GetNbinsX(); i++) {
	string cutname = h->GetXaxis()->GetBinLabel(i);
	double cutval = h->GetBinContent(i);
	if (channel == "ejets" || channel == "mujets") {
	    if (cme == "13tev") cutval *= 1.30/1.16;
	}
	if (channel == "ee" || channel == "mumu" || channel == "emu") {
	    if (cme == "13tev") cutval *= 1.44/1.16;
	}
	if (cutname == "Cut:Region") continue;
	if (cutname == "Cut:MuPt27.5") continue;
	if (cutname == "Cut:OneGoodPh") continue;
	if (cutname == "Cut:PhIsoFCT") continue;
	if (channel != "ejets" && cutname == "Cut:ZVeto") cutval = 0;
	if (channel != "ee" && channel != "mumu" && (cutname == "Cut:MllZveto" || cutname == "Cut:MllyZveto" || cutname == "Cut:MET>30")) cutval = 0;
	if (cutname == "Cut:PhMatch") cutname = "Cut:n(#gamma)";
	if (cutname == "Cut:SubRegion") cutname = "Cut:n(l)";
	if (cutname == "Cut:NjetGeq4") cutname = "Cut:n(j)";
	if (cutname == "Cut:NbjetGeq1") cutname = "Cut:n(bj)";
	if (cutname == "Cut:ZVeto") cutname = "Cut:m(e,#gamma) veto";
	if (cutname == "Cut:MllZveto") cutname = "Cut:m(l,l) veto";
	if (cutname == "Cut:MllyZveto") cutname = "Cut:m(l,l,#gamma) veto";
	if (cutname == "Cut:MET>30") cutname = "Cut:MET";
	if (cutname == "Cut:DrPhLep1.0") cutname = "Cut:#DeltaR(#gamma,l)";
	cutname = cutname.substr(4, cutname.size()-4);
	cf.push_back(pair<string,double>(cutname, cutval));
	if (string(h->GetXaxis()->GetBinLabel(i+1)).find("STOP",0) != string::npos) break;
    }
}
