#include <iostream>
#include <TLine.h>
#include <TLatex.h>
#include <TLegend.h>
#include <TStyle.h>
#include <TPad.h>
#include <TMathText.h>
#include "AtlasStyle.h"
#include <TFile.h>
#include <TH1.h>
#include <iomanip>
#include <TH2.h>
#include <string>
#include <sstream>
#include <algorithm>
#include <TCanvas.h>
#include <iterator>
#include <fstream>
#include <vector>

using namespace std;
string channel = "sinlepton";

int main(int argc, char * argv[]) {


    TString obs;
    TString channel;
    TFile* fin = new TFile("/afs/cern.ch/work/y/yili/private/Analysis/Unfolding/Results_Setp2/"+obs+"_normalised_uncertainties_"+chanel+"_3itr.root");

    TH1F* data = (TH1F*)fin->Get("data_unfolded");
    TH1F* truth_nominal = (TH1F*)fin->Get("theory_nominal");
    TH1F* truth_ISRFSRUp = (TH1F*)fin->Get("theory_ISRFSRUp");
    TH1F* truth_ISRFSRDn = (TH1F*)fin->Get("theory_ISRFSRDn");
    TH1F* truth_PS = (TH1F*)fin->Get("theory_PS");
    TH1F* truth_TTBar = (TH1F*)fin->Get("theory_TTBar");

    TH1F* h_stat_rel = (TH1F*)fin->Get("stat_rel");
    TH1F* h_syst_up_rel = (TH1F*)fin->Get("syst_up_rel");
    TH1F* h_syst_down_rel = (TH1F*)fin->Get("syst_down_rel");
    TH1F* h_tot_up_rel = (TH1F*)fin->Get("tot_up_rel");
    TH1F* h_tot_down_rel = (TH1F*)fin->Get("tot_down_rel");

    vector<double> tX;
    vector<double> tY;
    vector<double> x_values_up;
    vector<double> x_values_down;
    vector<double> y_values_up;
    vector<double> y_values_down;
    vector<double> y_values_stat_up;
    vector<double> y_values_stat_down;
    vector<double> y_values_ratio_down;
    vector<double> y_values_ratio_up;
    vector<double> y_values_stat_ratio_down;
    vector<double> y_values_stat_ratio_up;
    vector<double> y_values_ratio2_down;
    vector<double> y_values_ratio2_up;
    vector<double> y_values_stat_ratio2_down;
    vector<double> y_values_stat_ratio2_up;

    int tNbins = data->GetNbinsX()
    TGraphAsymmErrors* gr = new TGraphAsymmErrors(tNbins)
    TGraphAsymmErrors* gr_stat = new TGraphAsymmErrors(tNbins)
    TGraphAsymmErrors* gr_stat_ratio = new TGraphAsymmErrors(tNbins)
    TGraphAsymmErrors* gr_tot_ratio = new TGraphAsymmErrors(tNbins)
    TGraphAsymmErrors* gr_stat_ratio2 = new TGraphAsymmErrors(tNbins)
    TGraphAsymmErrors* gr_tot_ratio2 = new TGraphAsymmErrors(tNbins)
    gr->SetFillColor(kOrange-2)
    gr_stat->SetFillColor(kOrange-3)
    gr_tot_ratio->SetFillColor(kOrange-2)
    gr_stat_ratio->SetFillColor(kOrange-3)
    gr_tot_ratio2->SetFillColor(kOrange-2)
    gr_stat_ratio2->SetFillColor(kOrange-3)

    for (int iBin = 0; iBin < tNbins; iBin++) {
        tHistBin=iBin+1
    
    	tX.push_back(data->GetXaxis()->GetBinCenter(tHistBin))
	tY.push_back(data->GetBinContent(tHistBin))
	x_values_down.push_back(tX[iBin]-data->GetXaxis()->GetBinLowEdge(tHistBin))
	x_values_up.push_back(data->GetXaxis()->GetBinUpEdge(tHistBin))
    
        y_values_down.push_back(h_tot_down_rel->GetBinContent(tHistBin)*data->GetBinContent(tHistBin)/100.)
        y_values_up.push_back(h_tot_up_rel->GetBinContent(tHistBin)*data->GetBinContent(tHistBin)/100.)
    	y_values_stat_down.push_back(h_stat_rel->GetBinContent(tHistBin)*data->GetBinContent(tHistBin)/100.)
        y_values_stat_up.push_back(h_stat_rel->GetBinContent(tHistBin)*data->GetBinContent(tHistBin)/100.)
    
        y_values_ratio_down.push_back(y_values_down[iBin]/tY[iBin])
        y_values_ratio_up.push_back(y_values_up[iBin]/tY[iBin])
        y_values_stat_ratio_down.push_back(y_values_stat_down[iBin]/tY[iBin])
        y_values_stat_ratio_up.push_back(y_values_stat_up[iBin]/tY[iBin])
    
	y_values_ratio2_down.push_back(y_values_down[iBin]/truth_nominal->GetBinContent(tHistBin))
	y_values_ratio2_up.push_back(y_values_up[iBin]/truth_nominal->GetBinContent(tHistBin))
	y_values_stat_ratio2_down.push_back(y_values_stat_down[iBin]/truth_nominal->GetBinContent(tHistBin))
	y_values_stat_ratio2_up.push_back(y_values_stat_up[iBin]/truth_nominal->GetBinContent(tHistBin))
    }
    
    for (int i = 0; i < tNbins; i++) {
	gr->SetPoint(i, tX[i], tY[i])
	gr->SetPointError(i, x_values_down[i], x_values_down[i], y_values_down[i], y_values_up[i])
	gr_stat->SetPoint(i, tX[i], tY[i])
	gr_stat->SetPointError(i, x_values_down[i], x_values_down[i], y_values_stat_down[i], y_values_stat_up[i])
    
    	gr_tot_ratio->SetPoint(i, tX[i], 1)
	gr_tot_ratio->SetPointError(i, x_values_down[i], x_values_down[i], y_values_ratio_down[i], y_values_ratio_up[i])
	gr_stat_ratio->SetPoint(i, tX[i], 1)
	gr_stat_ratio->SetPointError(i, x_values_down[i], x_values_down[i], y_values_stat_ratio_down[i], y_values_stat_ratio_up[i])
    
    	gr_tot_ratio2->SetPoint(i, tX[i], data->GetBinContent(i+1)/truth_nominal->GetBinContent(i+1))
	gr_tot_ratio2->SetPointError(i, x_values_down[i], x_values_down[i], y_values_ratio2_down[i], y_values_ratio2_up[i])
	gr_stat_ratio2->SetPoint(i, tX[i], data->GetBinContent(i+1)/truth_nominal->GetBinContent(i+1))
	gr_stat_ratio2->SetPointError(i, x_values_down[i], x_values_down[i], y_values_stat_ratio2_down[i], y_values_stat_ratio2_up[i])

    }

    TMultiGraph* multi_graph_main    = new TMultiGraph()
    TMultiGraph* multi_graph_ratio    = new TMultiGraph()
    TMultiGraph* multi_graph_ratio2    = new TMultiGraph()
    multi_graph_main->Add(gr)
    multi_graph_main->Add(gr_stat)
    multi_graph_ratio->Add(gr_tot_ratio)
    multi_graph_ratio->Add(gr_stat_ratio)
    multi_graph_ratio2->Add(gr_tot_ratio2)
    multi_graph_ratio2->Add(gr_stat_ratio2)

    TCanvas *c1 = new TCanvas("c", "c", 700, 700)
    c1->cd()

    TPad* mainPad  = new TPad("mainPad", "top",    0.0, 0.4, 1.0, 1.00) 
    TPad* ratioPad = new TPad("ratioPad","bottom", 0.0, 0.18, 1.0, 0.4) 
    TPad* ratioPad2 = new TPad("ratioPad2","bottom2", 0.0, 0.00, 1.0, 0.22) 
    mainPad->SetBottomMargin(0.037)
    ratioPad->SetTopMargin(0.037)
    ratioPad2->SetTopMargin(0.03)
    ratioPad2->SetBottomMargin(0.45)
    mainPad->Draw()    
    ratioPad->Draw()
    ratioPad2->Draw()
    mainPad->cd()
    
    data->SetMarkerSize(1->5)
    
    gr->SetMarkerColor(0)
    gr->SetLineColor(0)
    gr->SetMarkerSize(0)
    
    gr_stat->SetMarkerSize(1->3)
    gr_stat->SetLineColor(0)
    
    gr_tot_ratio->SetMarkerColor(1)
    gr_tot_ratio->SetLineColor(1)
    gr_tot_ratio->SetMarkerSize(0)
    
    gr_stat_ratio->SetMarkerColor(1)
    gr_stat_ratio->SetLineColor(0)
    gr_stat_ratio->SetMarkerSize(0)
    
    gr_tot_ratio2->SetMarkerColor(1)
    gr_tot_ratio2->SetLineColor(1)
    gr_tot_ratio2->SetMarkerSize(0)
    
    gr_stat_ratio2->SetMarkerColor(1)
    gr_stat_ratio2->SetLineColor(0)
    gr_stat_ratio2->SetMarkerSize(0)
    
    truth_nominal->SetMarkerSize(0) 
    truth_nominal->SetLineWidth(2)
    truth_nominal->SetLineColor(kRed)
    
    truth_ISRFSRUp->SetMarkerSize(0) 
    truth_ISRFSRUp->SetLineWidth(2)
    truth_ISRFSRUp->SetLineColor(kGreen)
    
    truth_ISRFSRDn->SetMarkerSize(0) 
    truth_ISRFSRDn->SetLineWidth(2)
    truth_ISRFSRDn->SetLineColor(kCyan)
    
    truth_PS->SetMarkerSize(0) 
    truth_PS->SetLineWidth(2)
    truth_PS->SetLineColor(kBlue)
    
    truth_TTBar->SetMarkerSize(0) 
    truth_TTBar->SetLineWidth(2)
    truth_TTBar->SetLineStyle(2)
    truth_TTBar->SetLineColor(kRed)
    
    TLegend *legend = new TLegend(0.50, 0.50, 0.80, 0.90)
    legend->SetFillColor(0)
    legend->SetLineColor(0)
    legend->SetTextSize(0.042)
    legend->AddEntry(data,   "#bf{Unfolded data}",           "P")
    legend->AddEntry(truth_nominal, "#bf{MG5_aMC + Pythia8}",           "L")
    legend->AddEntry(truth_PS, "#bf{MG5_aMC + Herwig7}",           "L")
    legend->AddEntry(truth_ISRFSRUp, "#bf{MG5_aMC + Pythia8 (A14 Up)}",           "L")
    legend->AddEntry(truth_ISRFSRDn, "#bf{MG5_aMC + Pythia8 (A14 Down)}",           "L")
    legend->AddEntry(truth_TTBar, "#bf{Powheg + Pythia8 t#bar{t}}",           "L")
    legend->AddEntry(gr_stat,   "#bf{Stat->}",          "FE")
    legend->AddEntry(gr,    "#bf{Stat #oplus Syst->}",   "FE")
    
    gr_stat_ratio->GetXaxis()->SetTitle(obsxtitle)
    gr_stat_ratio->GetYaxis()->SetTitle(obsytitle)
    
    multi_graph_main->Draw("A E2 P")
    multi_graph_main->SetMinimum(0)
    if (obs == "ph_pt") {
	multi_graph_main->SetMaximum(0.06)
    if (obs == "ph_eta") {
	multi_graph_main->SetMaximum(1.4)
    if (channel == "sinlepton" && obs == "dR_lep_ph") {
	multi_graph_main->SetMaximum(0.9)
    if (channel == "dilepton" && obs == "dR_lep_ph") {
	multi_graph_main->SetMaximum(1.5)
    if (channel == "dilepton" && obs == "dPhi_lep") {
	multi_graph_main->SetMaximum(1.2)
    if (channel == "dilepton" && obs == "dEta_lep") {
	multi_graph_main->SetMaximum(1.4)
    	
    multi_graph_main.GetXaxis()->SetTitle(obsxtitle)
    multi_graph_main.GetYaxis()->SetTitle(obsytitle)
    if (obs.name == "ph_pt") {
        multi_graph_main.GetYaxis()->SetTitleOffset(1.4)
    } else {
        multi_graph_main.GetYaxis()->SetTitleOffset(1.2)
    }
    multi_graph_main->GetXaxis()->SetLabelSize(0)
    if (obs == "ph_pt") multi_graph_main->GetXaxis()->SetLimits(20, 300)
    if (obs == "ph_eta") multi_graph_main->GetXaxis()->SetLimits(0, 2.37)
    if (obs == "dR_lep_ph") multi_graph_main->GetXaxis()->SetLimits(1, 6.0)
    if (obs == "dEta_lep") multi_graph_main->GetXaxis()->SetLimits(0, 2.5)
    if (obs == "dPhi_lep") multi_graph_main->GetXaxis()->SetLimits(0, 3.14)
    multi_graph_main->GetYaxis()->SetLabelSize(0.055)
    truth_nominal->SetLineStyle(1)
    truth_ISRFSRUp->SetLineStyle(1)
    truth_ISRFSRDn->SetLineStyle(1)
    truth_PS->SetLineStyle(1)
    truth_nominal->Draw("same A HIST ][")
    truth_PS->Draw("same A HIST ][")
    truth_ISRFSRUp->Draw("same A HIST ][")
    truth_ISRFSRDn->Draw("same A HIST ][")
    truth_TTBar->Draw("same A HIST")
    data->Draw("same P")
    legend->Draw("same")
    
    al.make_ATLAS_label( 0.19, 0.85,  1, c1, 0.05, al.ATLAS_PRELIM)
    al.make_ATLAS_string( 0.19, 0.79, al.LUMI_STRING, 0.05)
    if normalise:
        text_box = "Normalised cross-section"
    else:
        text_box = "absolute cross-section"
    #make_ATLAS_string( 0.60, 0.87, text_box, 0.035)
    al.make_ATLAS_string( 0.19, 0.73, text_box, 0.05)    
    if channel == "sinlepton":
       text_box = "Single lepton"
    else:
       text_box = "Dilepton"
    al.make_ATLAS_string(0.19, 0.67, text_box, 0.05)    
    
    #gPad.RedrawAxis()
    gPad.Modified()
    gPad.Update()
    c1.Modified()
    c1.Update()
    
    ratioPad.cd()
    ratio_nominal = truth_nominal.Clone()
    ratio_ISRFSRUp = truth_ISRFSRUp.Clone()
    ratio_ISRFSRDn = truth_ISRFSRDn.Clone()
    ratio_PS = truth_PS.Clone()
    #ratio_R1F05 = truth_R1F05.Clone()
    #ratio_R1F2 = truth_R1F2.Clone()
    #ratio_R05F1 = truth_R05F1.Clone()
    #ratio_R2F1 = truth_R2F1.Clone()
    #ratio_PDFUp = truth_PDFUp.Clone()
    #ratio_PDFDn = truth_PDFDn.Clone()
    ratio_TTBar = truth_TTBar.Clone()
    ratio_nominal.Divide(data)
    ratio_ISRFSRUp.Divide(data)
    ratio_ISRFSRDn.Divide(data)
    ratio_PS.Divide(data)
    #ratio_R1F05.Divide(data)
    #ratio_R1F2.Divide(data)
    #ratio_R05F1.Divide(data)
    #ratio_R2F1.Divide(data)
    #ratio_PDFUp.Divide(data)
    #ratio_PDFDn.Divide(data)
    ratio_TTBar.Divide(data)
    multi_graph_ratio.Draw("A E2 P")
    ratio_nominal.Draw("sameHIST ][")
    ratio_ISRFSRUp.Draw("sameHIST ][")
    ratio_ISRFSRDn.Draw("sameHIST ][")
    ratio_PS.Draw("sameHIST ][")
    #ratio_R1F05.Draw("sameHIST")
    #ratio_R1F2.Draw("sameHIST")
    #ratio_R05F1.Draw("sameHIST")
    #ratio_R2F1.Draw("sameHIST")
    #ratio_PDFUp.Draw("sameHIST")
    #ratio_PDFDn.Draw("sameHIST")
    ratio_TTBar.Draw("sameHIST")
    gPad.RedrawAxis()
    
    #multi_graph_ratio.GetXaxis().SetTitle(obs.latex_name_axis)
    multi_graph_ratio.GetYaxis().SetTitle("Pred./Data")
    if obs.name == "ph_pt": multi_graph_ratio.GetXaxis().SetLimits(20, 300)
    if obs.name == "ph_eta": multi_graph_ratio.GetXaxis().SetLimits(0, 2.37)
    if (obs.name == "dR_lep_ph"): multi_graph_ratio.GetXaxis().SetLimits(1, 6.0)
    if (obs.name == "dEta_lep"): multi_graph_ratio.GetXaxis().SetLimits(0, 2.5)
    if (obs.name == "dPhi_lep"): multi_graph_ratio.GetXaxis().SetLimits(0, 3.14)
    multi_graph_ratio.GetYaxis().SetRangeUser(0.4, 1.6)
    multi_graph_ratio.GetYaxis().SetNdivisions(606)
    if obs.name == "dEta_lep" and channel == "sinlepton":
    	multi_graph_ratio.GetYaxis().SetRangeUser(0.7, 1.3)
    	multi_graph_ratio.GetYaxis().SetNdivisions(305)
    elif obs.name == "ph_pt" or obs.name == "ph_eta" or obs.name == "dEta_lep":
    	multi_graph_ratio.GetYaxis().SetRangeUser(0.4, 1.6)
    	multi_graph_ratio.GetYaxis().SetNdivisions(305)
    elif obs.name ==  "dR_lep_ph" and channel == "sinlepton":
    	multi_graph_ratio.GetYaxis().SetRangeUser(0.7, 1.3)
    	multi_graph_ratio.GetYaxis().SetNdivisions(305)
    elif obs.name ==  "dR_lep_ph" and channel == "dilepton":
    	multi_graph_ratio.GetYaxis().SetRangeUser(0.2, 1.8)
    	multi_graph_ratio.GetYaxis().SetNdivisions(305)
    elif obs.name ==  "dPhi_lep" and channel == "dilepton":
    	multi_graph_ratio.GetYaxis().SetRangeUser(0.2, 1.8)
    	multi_graph_ratio.GetYaxis().SetNdivisions(305)
    multi_graph_ratio.GetYaxis().SetLabelFont(43) # Absolute font size in pixel (precision 3)
    multi_graph_ratio.GetYaxis().SetLabelSize(24)
    multi_graph_ratio.GetXaxis().SetLabelFont(43) # Absolute font size in pixel (precision 3)
    multi_graph_ratio.GetXaxis().SetLabelSize(24)
    multi_graph_ratio.GetYaxis().SetTitleSize(0.13)
    multi_graph_ratio.GetXaxis().SetTitleSize(0.14)
    multi_graph_ratio.GetXaxis().SetTitleOffset(1.1)
    multi_graph_ratio.GetYaxis().SetTitleOffset(0.48)
    multi_graph_ratio.GetXaxis().SetLabelSize(0)
    
    ratioPad2.cd()
    ratio2_nominal = truth_nominal.Clone()
    ratio2_ISRFSRUp = truth_ISRFSRUp.Clone()
    ratio2_ISRFSRDn = truth_ISRFSRDn.Clone()
    ratio2_PS = truth_PS.Clone()
    #ratio2_R1F05 = truth_R1F05.Clone()
    #ratio2_R1F2 = truth_R1F2.Clone()
    #ratio2_R05F1 = truth_R05F1.Clone()
    #ratio2_R2F1 = truth_R2F1.Clone()
    #ratio2_PDFUp = truth_PDFUp.Clone()
    #ratio2_PDFDn = truth_PDFDn.Clone()
    ratio2_TTBar = truth_TTBar.Clone()
    #data2 = data.Clone()
    #data2.Divide(truth_nominal)
    ratio2_ISRFSRUp.Divide(truth_nominal)
    ratio2_ISRFSRDn.Divide(truth_nominal)
    ratio2_PS.Divide(truth_nominal)
    #ratio2_R1F05.Divide(truth_nominal)
    #ratio2_R1F2.Divide(truth_nominal)
    #ratio2_R05F1.Divide(truth_nominal)
    #ratio2_R2F1.Divide(truth_nominal)
    #ratio2_PDFUp.Divide(truth_nominal)
    #ratio2_PDFDn.Divide(truth_nominal)
    ratio2_TTBar.Divide(truth_nominal)
    
    multi_graph_ratio2.Draw("A E2 P")
    ratio2_ISRFSRUp.Draw("sameHIST ][")
    ratio2_ISRFSRDn.Draw("sameHIST ][")
    ratio2_PS.Draw("sameHIST ][")
    #ratio2_R1F05.Draw("sameHIST")
    #ratio2_R1F2.Draw("sameHIST")
    #ratio2_R05F1.Draw("sameHIST")
    #ratio2_R2F1.Draw("sameHIST")
    #ratio2_PDFUp.Draw("sameHIST")
    #ratio2_PDFDn.Draw("sameHIST")
    ratio2_TTBar.Draw("sameHIST")
    gPad.RedrawAxis()
    
    multi_graph_ratio2.GetXaxis().SetTitle(obs.latex_name_axis)
    multi_graph_ratio2.GetYaxis().SetTitle("Other/Nom.")
    if obs.name == "ph_pt": multi_graph_ratio2.GetXaxis().SetLimits(20, 300)
    if obs.name == "ph_eta": multi_graph_ratio2.GetXaxis().SetLimits(0, 2.37)
    if (obs.name == "dR_lep_ph"): multi_graph_ratio2.GetXaxis().SetLimits(1, 6.0)
    if (obs.name == "dEta_lep"): multi_graph_ratio2.GetXaxis().SetLimits(0, 2.5)
    if (obs.name == "dPhi_lep"): multi_graph_ratio2.GetXaxis().SetLimits(0, 3.14)
    multi_graph_ratio2.GetYaxis().SetRangeUser(0.4, 1.6)
    multi_graph_ratio2.GetYaxis().SetNdivisions(606)
    if obs.name == "dEta_lep" and channel == "sinlepton":
    	multi_graph_ratio2.GetYaxis().SetRangeUser(0.7, 1.3)
    	multi_graph_ratio2.GetYaxis().SetNdivisions(305)
    elif obs.name == "ph_pt" or obs.name == "ph_eta" or obs.name == "dEta_lep":
    	multi_graph_ratio2.GetYaxis().SetRangeUser(0.4, 1.6)
    	multi_graph_ratio2.GetYaxis().SetNdivisions(305)
    elif obs.name ==  "dR_lep_ph" and channel == "sinlepton":
    	multi_graph_ratio2.GetYaxis().SetRangeUser(0.7, 1.3)
    	multi_graph_ratio2.GetYaxis().SetNdivisions(305)
    elif obs.name ==  "dR_lep_ph" and channel == "dilepton":
    	multi_graph_ratio2.GetYaxis().SetRangeUser(0.2, 1.8)
    	multi_graph_ratio2.GetYaxis().SetNdivisions(305)
    elif obs.name ==  "dPhi_lep" and channel == "dilepton":
    	multi_graph_ratio2.GetYaxis().SetRangeUser(0.2, 1.8)
    	multi_graph_ratio2.GetYaxis().SetNdivisions(305)
    multi_graph_ratio2.GetYaxis().SetLabelFont(43) # Absolute font size in pixel (precision 3)
    multi_graph_ratio2.GetYaxis().SetLabelSize(24)
    multi_graph_ratio2.GetXaxis().SetLabelFont(43) # Absolute font size in pixel (precision 3)
    multi_graph_ratio2.GetXaxis().SetLabelSize(24)
    multi_graph_ratio2.GetYaxis().SetTitleSize(0.13)
    multi_graph_ratio2.GetXaxis().SetTitleSize(0.16)
    multi_graph_ratio2.GetXaxis().SetTitleOffset(1.15)
    multi_graph_ratio2.GetYaxis().SetTitleOffset(0.45)
    
    gPad.RedrawAxis()
    gPad.Modified()
    gPad.Update()
    c1.Modified()
    c1.Update()
    plot_name = ""
    if normalise:
    	plot_name = "nomalised_"
    c1.SaveAs("./Results_Step3/unfolded_"+plot_name+obs.name+"_"+channel+"_"+str(n_iter)+"itr.pdf")
    c1.SaveAs("./Results_Step3/unfolded_"+plot_name+obs.name+"_"+channel+"_"+str(n_iter)+"itr.eps")
    c1.SaveAs("./Results_Step3/unfolded_"+plot_name+obs.name+"_"+channel+"_"+str(n_iter)+"itr.png")
    c1.SaveAs("./Results_Step3/unfolded_"+plot_name+obs.name+"_"+channel+"_"+str(n_iter)+"itr.C")
