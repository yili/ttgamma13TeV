#include <TFile.h>
#include <TH1D.h>
#include <TCanvas.h>
#include <TLegend.h>
#include <TPad.h>
#include <TLine.h>
#include <string>
#include <TLatex.h>
#include <TMath.h>
#include <vector>
#include <TGraphErrors.h>
#include <map>
#include <TIterator.h>
#include <TKey.h>
#include <TObject.h>
#include <THStack.h>

#include "AtlasStyle.h"
#include "PlotComparor.h"
#include "StringPlayer.h"
#include "ConfigReader.h"
#include "Logger.h"

using namespace std;

int main(int argc, char * argv[]) 
{
    SetAtlasStyle();

    PlotComparor* PC = new PlotComparor();
    PC->SetSaveDir("plots/Upgrade/");
    string drawoptions = "hist";
    drawoptions += " _e";
    PC->SetDrawOption(drawoptions.c_str());

    TFile* f_ejets_13_sig = new TFile("results/Nominal/13TeV_CutSR_PhMatch_TruePh_Signal_Reco_CR1_ejets_Nominal_Final02.root");
    TFile* f_ejets_13_hfake = new TFile("results/Nominal/13TeV_CutSR_PhMatch_HFake_TTBar_Reco_CR1_ejets_Nominal_Final02.root");
    TFile* f_ejets_14_sig = new TFile("results/Upgrade/14TeV_CutSRUpgrade_PhMatch_TruePh_LpMatch_TrueLp_Signal_Upgrade_UR_ejets_Nominal_U07.root");
    TFile* f_ejets_14_hfake = new TFile("results/Upgrade/14TeV_CutSRUpgrade_PhMatch_HFake_LpMatch_TrueLp_TTBar_Upgrade_UR_ejets_Nominal_U07.root");
    TH1F* h_ejets_13_sig = (TH1F*)f_ejets_13_sig->Get("LeadPhPtCoarse_13TeV_CutSR_PhMatch_TruePh_Signal_Reco_CR1_ejets_Nominal");
    TH1F* h_ejets_13_hfake = (TH1F*)f_ejets_13_hfake->Get("LeadPhPtCoarse_13TeV_CutSR_PhMatch_HFake_TTBar_Reco_CR1_ejets_Nominal");
    TH1F* h_ejets_14_sig = (TH1F*)f_ejets_14_sig->Get("LeadPhPtCoarse_14TeV_CutSRUpgrade_PhMatch_TruePh_LpMatch_TrueLp_Signal_Upgrade_UR_ejets_Nominal");
    TH1F* h_ejets_14_hfake = (TH1F*)f_ejets_14_hfake->Get("LeadPhPtCoarse_14TeV_CutSRUpgrade_PhMatch_HFake_LpMatch_TrueLp_TTBar_Upgrade_UR_ejets_Nominal");
    TH1F* h_ejets_ratio_13 = (TH1F*)h_ejets_13_hfake->Clone();
    h_ejets_ratio_13->Scale(1.5);
    h_ejets_ratio_13->Divide(h_ejets_13_sig);
    TH1F* h_ejets_ratio_14 = (TH1F*)h_ejets_14_hfake->Clone();
    h_ejets_ratio_14->Divide(h_ejets_14_sig);

    TFile* f_mujets_13_sig = new TFile("results/Nominal/13TeV_CutSR_PhMatch_TruePh_Signal_Reco_CR1_mujets_Nominal_Final02.root");
    TFile* f_mujets_13_hfake = new TFile("results/Nominal/13TeV_CutSR_PhMatch_HFake_TTBar_Reco_CR1_mujets_Nominal_Final02.root");
    TFile* f_mujets_14_sig = new TFile("results/Upgrade/14TeV_CutSRUpgrade_PhMatch_TruePh_LpMatch_TrueLp_Signal_Upgrade_UR_mujets_Nominal_U07.root");
    TFile* f_mujets_14_hfake = new TFile("results/Upgrade/14TeV_CutSRUpgrade_PhMatch_HFake_LpMatch_TrueLp_TTBar_Upgrade_UR_mujets_Nominal_U07.root");
    TH1F* h_mujets_13_sig = (TH1F*)f_mujets_13_sig->Get("LeadPhPtCoarse_13TeV_CutSR_PhMatch_TruePh_Signal_Reco_CR1_mujets_Nominal");
    TH1F* h_mujets_13_hfake = (TH1F*)f_mujets_13_hfake->Get("LeadPhPtCoarse_13TeV_CutSR_PhMatch_HFake_TTBar_Reco_CR1_mujets_Nominal");
    TH1F* h_mujets_14_sig = (TH1F*)f_mujets_14_sig->Get("LeadPhPtCoarse_14TeV_CutSRUpgrade_PhMatch_TruePh_LpMatch_TrueLp_Signal_Upgrade_UR_mujets_Nominal");
    TH1F* h_mujets_14_hfake = (TH1F*)f_mujets_14_hfake->Get("LeadPhPtCoarse_14TeV_CutSRUpgrade_PhMatch_HFake_LpMatch_TrueLp_TTBar_Upgrade_UR_mujets_Nominal");
    TH1F* h_mujets_ratio_13 = (TH1F*)h_mujets_13_hfake->Clone();
    h_mujets_ratio_13->Scale(1.5);
    h_mujets_ratio_13->Divide(h_mujets_13_sig);
    TH1F* h_mujets_ratio_14 = (TH1F*)h_mujets_14_hfake->Clone();
    h_mujets_ratio_14->Divide(h_mujets_14_sig);

    TFile* f_ee_13_sig = new TFile("results/Nominal/13TeV_CutSR_PhMatch_TruePh_Signal_Reco_CR1_ee_Nominal_Final02.root");
    TFile* f_ee_13_hfake = new TFile("results/Nominal/13TeV_CutSR_PhMatch_HFake_TTBar_Reco_CR1_ee_Nominal_Final02.root");
    TFile* f_ee_14_sig = new TFile("results/Upgrade/14TeV_CutSRUpgrade_PhMatch_TruePh_LpMatch_TrueLp_Signal_Upgrade_UR_ee_Nominal_U07.root");
    TFile* f_ee_14_hfake = new TFile("results/Upgrade/14TeV_CutSRUpgrade_PhMatch_HFake_LpMatch_TrueLp_TTBar_Upgrade_UR_ee_Nominal_U07.root");
    TH1F* h_ee_13_sig = (TH1F*)f_ee_13_sig->Get("LeadPhPtCoarse_13TeV_CutSR_PhMatch_TruePh_Signal_Reco_CR1_ee_Nominal");
    TH1F* h_ee_13_hfake = (TH1F*)f_ee_13_hfake->Get("LeadPhPtCoarse_13TeV_CutSR_PhMatch_HFake_TTBar_Reco_CR1_ee_Nominal");
    TH1F* h_ee_14_sig = (TH1F*)f_ee_14_sig->Get("LeadPhPtCoarse_14TeV_CutSRUpgrade_PhMatch_TruePh_LpMatch_TrueLp_Signal_Upgrade_UR_ee_Nominal");
    TH1F* h_ee_14_hfake = (TH1F*)f_ee_14_hfake->Get("LeadPhPtCoarse_14TeV_CutSRUpgrade_PhMatch_HFake_LpMatch_TrueLp_TTBar_Upgrade_UR_ee_Nominal");
    TH1F* h_ee_ratio_13 = (TH1F*)h_ee_13_hfake->Clone();
    h_ee_ratio_13->Scale(1.5);
    h_ee_ratio_13->Divide(h_ee_13_sig);
    TH1F* h_ee_ratio_14 = (TH1F*)h_ee_14_hfake->Clone();
    h_ee_ratio_14->Divide(h_ee_14_sig);

    TFile* f_emu_13_sig = new TFile("results/Nominal/13TeV_CutSR_PhMatch_TruePh_Signal_Reco_CR1_emu_Nominal_Final02.root");
    TFile* f_emu_13_hfake = new TFile("results/Nominal/13TeV_CutSR_PhMatch_HFake_TTBar_Reco_CR1_emu_Nominal_Final02.root");
    TFile* f_emu_14_sig = new TFile("results/Upgrade/14TeV_CutSRUpgrade_PhMatch_TruePh_LpMatch_TrueLp_Signal_Upgrade_UR_emu_Nominal_U07.root");
    TFile* f_emu_14_hfake = new TFile("results/Upgrade/14TeV_CutSRUpgrade_PhMatch_HFake_LpMatch_TrueLp_TTBar_Upgrade_UR_emu_Nominal_U07.root");
    TH1F* h_emu_13_sig = (TH1F*)f_emu_13_sig->Get("LeadPhPtCoarse_13TeV_CutSR_PhMatch_TruePh_Signal_Reco_CR1_emu_Nominal");
    TH1F* h_emu_13_hfake = (TH1F*)f_emu_13_hfake->Get("LeadPhPtCoarse_13TeV_CutSR_PhMatch_HFake_TTBar_Reco_CR1_emu_Nominal");
    TH1F* h_emu_14_sig = (TH1F*)f_emu_14_sig->Get("LeadPhPtCoarse_14TeV_CutSRUpgrade_PhMatch_TruePh_LpMatch_TrueLp_Signal_Upgrade_UR_emu_Nominal");
    TH1F* h_emu_14_hfake = (TH1F*)f_emu_14_hfake->Get("LeadPhPtCoarse_14TeV_CutSRUpgrade_PhMatch_HFake_LpMatch_TrueLp_TTBar_Upgrade_UR_emu_Nominal");
    TH1F* h_emu_ratio_13 = (TH1F*)h_emu_13_hfake->Clone();
    h_emu_ratio_13->Scale(1.5);
    h_emu_ratio_13->Divide(h_emu_13_sig);
    TH1F* h_emu_ratio_14 = (TH1F*)h_emu_14_hfake->Clone();
    h_emu_ratio_14->Divide(h_emu_14_sig);

    TFile* f_mumu_13_sig = new TFile("results/Nominal/13TeV_CutSR_PhMatch_TruePh_Signal_Reco_CR1_mumu_Nominal_Final02.root");
    TFile* f_mumu_13_hfake = new TFile("results/Nominal/13TeV_CutSR_PhMatch_HFake_TTBar_Reco_CR1_mumu_Nominal_Final02.root");
    TFile* f_mumu_14_sig = new TFile("results/Upgrade/14TeV_CutSRUpgrade_PhMatch_TruePh_LpMatch_TrueLp_Signal_Upgrade_UR_mumu_Nominal_U07.root");
    TFile* f_mumu_14_hfake = new TFile("results/Upgrade/14TeV_CutSRUpgrade_PhMatch_HFake_LpMatch_TrueLp_TTBar_Upgrade_UR_mumu_Nominal_U07.root");
    TH1F* h_mumu_13_sig = (TH1F*)f_mumu_13_sig->Get("LeadPhPtCoarse_13TeV_CutSR_PhMatch_TruePh_Signal_Reco_CR1_mumu_Nominal");
    TH1F* h_mumu_13_hfake = (TH1F*)f_mumu_13_hfake->Get("LeadPhPtCoarse_13TeV_CutSR_PhMatch_HFake_TTBar_Reco_CR1_mumu_Nominal");
    TH1F* h_mumu_14_sig = (TH1F*)f_mumu_14_sig->Get("LeadPhPtCoarse_14TeV_CutSRUpgrade_PhMatch_TruePh_LpMatch_TrueLp_Signal_Upgrade_UR_mumu_Nominal");
    TH1F* h_mumu_14_hfake = (TH1F*)f_mumu_14_hfake->Get("LeadPhPtCoarse_14TeV_CutSRUpgrade_PhMatch_HFake_LpMatch_TrueLp_TTBar_Upgrade_UR_mumu_Nominal");
    TH1F* h_mumu_ratio_13 = (TH1F*)h_mumu_13_hfake->Clone();
    h_mumu_ratio_13->Scale(1.5);
    h_mumu_ratio_13->Divide(h_mumu_13_sig);
    TH1F* h_mumu_ratio_14 = (TH1F*)h_mumu_14_hfake->Clone();
    h_mumu_ratio_14->Divide(h_mumu_14_sig);

    //PC->NormToUnit(false);
    //PC->ClearAlterHs();
    //PC->SetChannel("ejets");
    //PC->SetBaseH(h_ejets_ratio_13);
    //PC->SetBaseHName("13 TeV");
    //PC->AddAlterH(h_ejets_ratio_14);
    //PC->AddAlterHName("14 TeV");
    //string savename = "ForHFakeCorr_ejets";
    //PC->SetSaveName(savename);
    //PC->IsSimulation(true);
    //PC->SetLHCInfo("");
    //PC->Is14TeV(true);
    ////PC->SquareCanvas(true);
    //PC->SetXtitle("p_{T}(#gamma) [GeV]");
    //PC->SetYtitle("t#bar{t} had-fake / t#bar{t}#gamma");
    //PC->Compare();

    //PC->NormToUnit(false);
    //PC->ClearAlterHs();
    //PC->SetChannel("mujets");
    //PC->SetBaseH(h_mujets_ratio_13);
    //PC->SetBaseHName("13 TeV");
    //PC->AddAlterH(h_mujets_ratio_14);
    //PC->AddAlterHName("14 TeV");
    //savename = "ForHFakeCorr_mujets";
    //PC->SetSaveName(savename);
    //PC->IsSimulation(true);
    //PC->SetLHCInfo("");
    //PC->Is14TeV(true);
    ////PC->SquareCanvas(true);
    //PC->SetXtitle("p_{T}(#gamma) [GeV]");
    //PC->SetYtitle("t#bar{t} had-fake / t#bar{t}#gamma");
    //PC->Compare();

    TH1F* h_ljets_13_hfake = (TH1F*)h_ejets_13_hfake->Clone();
    h_ljets_13_hfake->Add(h_mujets_13_hfake);
    h_ljets_13_hfake->Scale(1.5);
    TH1F* h_ljets_13_sig = (TH1F*)h_ejets_13_sig->Clone();
    h_ljets_13_sig->Add(h_mujets_13_sig);
    TH1F* h_ljets_14_hfake = (TH1F*)h_ejets_14_hfake->Clone();
    h_ljets_14_hfake->Add(h_mujets_14_hfake);
    TH1F* h_ljets_14_sig = (TH1F*)h_ejets_14_sig->Clone();
    h_ljets_14_sig->Add(h_mujets_14_sig);

    TH1F* h_ljets_ratio_13 = (TH1F*)h_ljets_13_hfake->Clone();
    h_ljets_ratio_13->Divide(h_ljets_13_sig);
    TH1F* h_ljets_ratio_14 = (TH1F*)h_ljets_14_hfake->Clone();
    h_ljets_ratio_14->Divide(h_ljets_14_sig);

    TH1F* h_ljets_finalratio = (TH1F*)h_ljets_ratio_13->Clone();
    h_ljets_finalratio->Divide(h_ljets_ratio_14);

    TH1F* h_ll_13_hfake = (TH1F*)h_ee_13_hfake->Clone();
    h_ll_13_hfake->Add(h_emu_13_hfake);
    h_ll_13_hfake->Add(h_mumu_13_hfake);
    h_ll_13_hfake->Scale(1.5);
    TH1F* h_ll_13_sig = (TH1F*)h_ee_13_sig->Clone();
    h_ll_13_sig->Add(h_emu_13_sig);
    h_ll_13_sig->Add(h_mumu_13_sig);
    TH1F* h_ll_14_hfake = (TH1F*)h_ee_14_hfake->Clone();
    h_ll_14_hfake->Add(h_emu_14_hfake);
    h_ll_14_hfake->Add(h_mumu_14_hfake);
    TH1F* h_ll_14_sig = (TH1F*)h_ee_14_sig->Clone();
    h_ll_14_sig->Add(h_emu_14_sig);
    h_ll_14_sig->Add(h_mumu_14_sig);

    TH1F* h_ll_ratio_13 = (TH1F*)h_ll_13_hfake->Clone();
    h_ll_ratio_13->Divide(h_ll_13_sig);
    TH1F* h_ll_ratio_14 = (TH1F*)h_ll_14_hfake->Clone();
    h_ll_ratio_14->Divide(h_ll_14_sig);

    TH1F* h_ll_finalratio = (TH1F*)h_ll_ratio_13->Clone();
    h_ll_finalratio->Divide(h_ll_ratio_14);

    PC->NormToUnit(false);
    PC->DrawRatio(true);
    PC->ClearAlterHs();
    PC->SetChannel("Single lepton");
    PC->SetBaseH(h_ljets_ratio_14);
    PC->SetBaseHName("14 TeV");
    PC->AddAlterH(h_ljets_ratio_13);
    PC->AddAlterHName("13 TeV");
    string savename = "ForHFakeCorr_ljets";
    PC->SetSaveName(savename);
    PC->IsSimulation(true);
    PC->SetLegendPos(0.75,0.75,0.92,0.94);
    PC->SetLHCInfo("#sqrt{s} = 14 TeV v.s. #sqrt{s} = 13 TeV");
    PC->Is14TeV(true);
    PC->SquareCanvas(true);
    PC->SetXtitle("p_{T}(#gamma) [GeV]");
    PC->SetYtitle("t#bar{t} had-fake / t#bar{t}#gamma");
    PC->SetMaxRatio(1.1);
    PC->SetMinRatio(0);
    PC->Compare();

    PC->NormToUnit(false);
    PC->DrawRatio(true);
    PC->ClearAlterHs();
    PC->SetChannel("Dilepton");
    PC->SetBaseH(h_ll_ratio_14);
    PC->SetBaseHName("14 TeV");
    PC->AddAlterH(h_ll_ratio_13);
    PC->AddAlterHName("13 TeV");
    savename = "ForHFakeCorr_ll";
    PC->SetSaveName(savename);
    PC->IsSimulation(true);
    PC->SetLegendPos(0.75,0.75,0.92,0.94);
    PC->SetLHCInfo("#sqrt{s} = 14 TeV v.s. #sqrt{s} = 13 TeV");
    PC->Is14TeV(true);
    PC->SquareCanvas(true);
    PC->SetXtitle("p_{T}(#gamma) [GeV]");
    PC->SetYtitle("t#bar{t} had-fake / t#bar{t}#gamma");
    PC->SetMaxRatio(1.1);
    PC->SetMinRatio(0);
    PC->Compare();

    TFile* f = new TFile("results/Upgrade/HFakeCorr.root", "recreate");
    f->cd();
    h_ljets_finalratio->Write("HFakeCorr_ljets");
    h_ll_finalratio->Write("HFakeCorr_ll");
    f->Close();
    delete f;
}
