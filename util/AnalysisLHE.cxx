#include <time.h>
#include "StringPlayer.h"
#include <TH2.h>
#include <algorithm>
#include <stdio.h>
#include <stdlib.h>
#include <TKey.h>
#include <TIterator.h>
#include <TH1.h>
#include <TLorentzVector.h>
#include <TStyle.h>
#include <TCanvas.h>
#include <TStopwatch.h>
#include <iostream>
#include <fstream>
#include <stdio.h>
#include <stdlib.h>
#include <TFile.h>

using namespace std;

int FindRemoveMax(vector<double> &input, bool debug) {
    int pos = 0;
    double max = -1;
    for (int i = 0; i < input.size(); i++) {
	if (input.at(i) > max) {
	    max = input.at(i); 
	    pos = i;
	}
    }
    if (debug) cout << setprecision(3) << max << endl;
    input.at(pos) = -1;
    return pos;
}

void OrderFVector(vector<double> fv, vector<int> &order, bool debug = false) {
    order.clear();
    for (int i = 0; i < fv.size(); i++) {
	order.push_back(FindRemoveMax(fv, debug));
    }
}

int main(int argc, char * argv[]) 
{
    double xsec = 3356;
    string input;
    string output = "output";;
    int maxevt = -1;

    bool debug = false;
    bool donjet = false;
    for (int i = 0; i < argc; i++) {
	if (!strcmp(argv[i],"--input")) {
	    input = argv[i+1];
   	}
	if (!strcmp(argv[i],"--output")) {
	    output = argv[i+1];
   	}
	if (!strcmp(argv[i],"--maxevt")) {
	    maxevt = atoi(string(argv[i+1]).c_str());
   	}
	if (!strcmp(argv[i],"--xsec")) {
	    xsec = atof(string(argv[i+1]).c_str());
   	}
	if (!strcmp(argv[i],"--debug")) {
	    debug = true;
   	}
	if (!strcmp(argv[i],"--donjet")) {
	    donjet = true;
   	}
    }

    ifstream ifile;
    ifile.open(input.c_str());

    char tmpline[10000];
    bool newevt = false;
    int nline = 0;
    int nevt = 0;
    vector<double> scales;
    vector<double> scales_M;
    vector<string> event;
    TH1F* h_scale = new TH1F("scale","scale",50,0,500);
    TH1F* h_scale_sl = new TH1F("scale_sl","scale_sl",50,0,500);
    TH1F* h_scale_dl = new TH1F("scale_dl","scale_dl",50,0,500);
    TH1F* h_scale_M = new TH1F("scale_M","scale_M",50,0,500);
    TH1F* h_weight = new TH1F("weight","weight",20,0,0.02);
    TH1F* h_sl_phpt = new TH1F("sl_phpt","sl_phpt",200,0,1000);
    TH1F* h_sl_pheta = new TH1F("sl_pheta","sl_pheta",100,-5,5);
    TH1F* h_sl_leppt = new TH1F("sl_leppt","sl_leppt",200,0,1000);
    TH1F* h_sl_lepeta = new TH1F("sl_lepeta","sl_lepeta",100,-5,5);
    TH1F* h_sl_jet1pt = new TH1F("sl_jet1pt","sl_jet1pt",200,0,1000);
    TH1F* h_sl_jet1eta = new TH1F("sl_jet1eta","sl_jet1eta",100,-5,5);
    TH1F* h_sl_jet2pt = new TH1F("sl_jet2pt","sl_jet2pt",200,0,1000);
    TH1F* h_sl_jet2eta = new TH1F("sl_jet2eta","sl_jet2eta",100,-5,5);
    TH1F* h_sl_jet3pt = new TH1F("sl_jet3pt","sl_jet3pt",200,0,1000);
    TH1F* h_sl_jet3eta = new TH1F("sl_jet3eta","sl_jet3eta",100,-5,5);
    TH1F* h_sl_jet4pt = new TH1F("sl_jet4pt","sl_jet4pt",200,0,1000);
    TH1F* h_sl_jet4eta = new TH1F("sl_jet4eta","sl_jet4eta",100,-5,5);
    TH1F* h_dl_phpt = new TH1F("dl_phpt","dl_phpt",200,0,1000);
    TH1F* h_dl_pheta = new TH1F("dl_pheta","dl_pheta",100,-5,5);
    TH1F* h_dl_elpt = new TH1F("dl_elpt","dl_elpt",200,0,1000);
    TH1F* h_dl_eleta = new TH1F("dl_eleta","dl_eleta",100,-5,5);
    TH1F* h_dl_mupt = new TH1F("dl_mupt","dl_mupt",200,0,1000);
    TH1F* h_dl_mueta = new TH1F("dl_mueta","dl_mueta",100,-5,5);
    TH1F* h_dl_jet1pt = new TH1F("dl_jet1pt","dl_jet1pt",200,0,1000);
    TH1F* h_dl_jet1eta = new TH1F("dl_jet1eta","dl_jet1eta",100,-5,5);
    TH1F* h_dl_jet2pt = new TH1F("dl_jet2pt","dl_jet2pt",200,0,1000);
    TH1F* h_dl_jet2eta = new TH1F("dl_jet2eta","dl_jet2eta",100,-5,5);
    h_sl_phpt->Sumw2();
    h_sl_pheta->Sumw2();
    h_sl_leppt->Sumw2();
    h_sl_lepeta->Sumw2();
    h_sl_jet1pt->Sumw2();
    h_sl_jet1eta->Sumw2();
    h_sl_jet2pt->Sumw2();
    h_sl_jet2eta->Sumw2();
    h_sl_jet3pt->Sumw2();
    h_sl_jet3eta->Sumw2();
    h_sl_jet4pt->Sumw2();
    h_sl_jet4eta->Sumw2();
    h_dl_phpt->Sumw2();
    h_dl_pheta->Sumw2();
    h_dl_elpt->Sumw2();
    h_dl_eleta->Sumw2();
    h_dl_mupt->Sumw2();
    h_dl_mueta->Sumw2();
    h_dl_jet1pt->Sumw2();
    h_dl_jet1eta->Sumw2();
    h_dl_jet2pt->Sumw2();
    h_dl_jet2eta->Sumw2();
    int slevt = 0;
    int dlevt = 0;
    string forK_sl_cf_name[10] = {"ej", "basicel", "goodel", "meph", "goodph", "basicjet", "goodjet", "dRphj", "dRphe", "dRbj"};
    string forK_dl_cf_name[13] = {"emu", "basicelmu", "goodelmu", "meph", "goodph", "basicjet", "goodjet", "dRphj", "dRl0j", "dRl1j", "dRphl0", "dRphl1", "dRbj"};
    int forK_sl_cf[10] = {};
    int forK_dl_cf[13] = {};
    int ncf_sl = 10;
    int ncf_dl = 13;
    string forXcheck_dl_cf_name[3] = {"emu", "phl", "phj"};
    int forXcheck_dl_cf[3] = {};
    while (ifile.getline(tmpline, 10000, '\n')) {
	TString strline = tmpline;
	if (maxevt != -1) {
	    if (nevt > maxevt) break;
	}
	nline++;

	if (newevt) {
	    if (strline.Contains("<mgrwt>")) {
	      newevt = false;
	    }
	}

	if (newevt) {
	    event.push_back(tmpline);
	} else {
	    if (event.size() != 0) {
		if (nevt % 500 == 0) cout << "this is the " << nevt << " event" << endl;

		nevt++;
		string evtinfo = event.at(0);
		istringstream iss(evtinfo);
		int cnt = 0;
		double weight = 0;
		double scale = 0;
		for (string word; iss >> word;) {
		    cnt++;
		    if (cnt == 3) {
			weight = atof(word.c_str());
		    };
		    if (cnt == 4) {
			//scales.push_back(atof(word.c_str()));
			//cout << scales.back() << endl;
			scale = atof(word.c_str());
		    };
		}
		h_scale->Fill(scale,weight);
		h_weight->Fill(weight);
		double scale_M = 0;
		//for (int i = 1; i < event.size(); i++) {
		//    string pinfo = event.at(i);
		//    istringstream iss(pinfo);
		//    vector<string> vpinfo;
		//    for (string word; iss >> word;) {
		//	vpinfo.push_back(word);
		//    }
		//    if (vpinfo.at(1) == "1") {
		//	double px = atof(vpinfo.at(6).c_str());
		//	double py = atof(vpinfo.at(7).c_str());
		//	double pz = atof(vpinfo.at(8).c_str());
		//	double E = atof(vpinfo.at(9).c_str());
		//	double M = atof(vpinfo.at(10).c_str());
		//	//cout << pinfo << endl;
		//	//cout << px << " " << py <<  " " << pz <<  " " << E << endl;
		//	scale_M += sqrt(pow(M,2) + pow(px,2) + pow(py,2));
		//    }
		//}
		scale_M /= 2;
		h_scale_M->Fill(scale_M,weight);
		//scales_M.push_back(scale);
		//cout << scales_M.back() << endl;

		vector<TLorentzVector> tlv_l;
		TLorentzVector tlv_e;
		TLorentzVector tlv_m;
		vector<TLorentzVector> tlv_j;
		vector<TLorentzVector> tlv_bj;
		TLorentzVector tlv_y;
		int nlep = 0;
		int nel = 0;
		int nmu = 0;
		for (int l = 0; l < event.size(); l++) {
		    if (debug) cout << event.at(l) << endl;
		    if (l == 0) continue;
    
		    istringstream iss(event.at(l));
		    vector<string> words;
		    for (string word; iss >> word;) {
			words.push_back(word);
		    }

		    bool is_lep = false;
		    bool is_j = false;
		    bool is_ph = false;
		    if (words.size() < 10) {
			nevt--;
			break;
		    }
		    int pdg = atoi(words.at(0).c_str());
		    if (fabs(pdg) == 11 || fabs(pdg) == 13 || fabs(pdg) == 15) is_lep = true;
		    if (fabs(pdg) == 22) is_ph = true;
		    if (fabs(pdg) == 1 || fabs(pdg) == 2 || fabs(pdg) == 3 || fabs(pdg) == 4 || fabs(pdg) == 5) is_j = true;
		    if (is_lep) {
			if (fabs(pdg) == 11) nel++;
			if (fabs(pdg) == 13) nmu++;
		        TLorentzVector tmp_tlv_l;
		        tmp_tlv_l.SetPxPyPzE(atof(words.at(6).c_str()),atof(words.at(7).c_str()),atof(words.at(8).c_str()),atof(words.at(9).c_str()));
			tlv_l.push_back(tmp_tlv_l);
			if (fabs(pdg) == 11) tlv_e = tmp_tlv_l;
			if (fabs(pdg) == 13) tlv_m = tmp_tlv_l;
			nlep ++;
		    }
		    if (is_ph) {
		        tlv_y.SetPxPyPzE(atof(words.at(6).c_str()),atof(words.at(7).c_str()),atof(words.at(8).c_str()),atof(words.at(9).c_str()));
			if (debug) cout << tlv_y.Px() << " " << tlv_y.Py() << " " << tlv_y.Pz() << endl;
		    }
		    if (is_j) {
		        tlv_y.SetPxPyPzE(atof(words.at(6).c_str()),atof(words.at(7).c_str()),atof(words.at(8).c_str()),atof(words.at(9).c_str()));
		        TLorentzVector tmp_tlv_j;
		        tmp_tlv_j.SetPxPyPzE(atof(words.at(6).c_str()),atof(words.at(7).c_str()),atof(words.at(8).c_str()),atof(words.at(9).c_str()));
			tlv_j.push_back(tmp_tlv_j);
			if (fabs(pdg) == 5) tlv_bj.push_back(tmp_tlv_j);
		    }
		}

		if (nlep == 2) dlevt++;
		if (nlep == 1) slevt++;

		// cuts
		if (nel == 1 && nmu == 0 && nlep == 1) {
		    forK_sl_cf[0]++;
		    if (tlv_l.at(0).Pt() > 15 && fabs(tlv_l.at(0).Eta()) < 5) {
		        forK_sl_cf[1]++;
			if (tlv_l.at(0).Pt() > 25 && fabs(tlv_l.at(0).Eta()) < 2.5) {
			    forK_sl_cf[2]++;
			    if (tlv_y.Pt() > 15 && fabs(tlv_y.Eta()) < 5.0) {
				forK_sl_cf[3]++;
				if (tlv_y.Pt() > 20 && fabs(tlv_y.Eta()) < 2.37) {
				    forK_sl_cf[4]++;
				    int nbasicgoodj = 0;
		            	    for (int i = 0; i < tlv_j.size(); i++) {
		            	        if (tlv_j.at(i).Pt() > 10 && fabs(tlv_j.at(i).Eta()) < 5.0) {
		            	    	nbasicgoodj++;
		            	        }
		            	    }
				    if (!donjet || nbasicgoodj >= 4) {
					forK_sl_cf[5]++;
					int ngoodj = 0;
					vector<TLorentzVector> tlv_gj;
					vector<double> tlv_gj_pt;
		            		for (int i = 0; i < tlv_j.size(); i++) {
		            		    if (tlv_j.at(i).Pt() > 25 && fabs(tlv_j.at(i).Eta()) < 2.5) {
						tlv_gj.push_back(tlv_j.at(i));
						tlv_gj_pt.push_back(tlv_j.at(i).Pt());
					        ngoodj++;
		            		    }
		            		}
					vector<int> order_gj;
					OrderFVector(tlv_gj_pt, order_gj);
					if (!donjet || ngoodj >= 4) {
					    forK_sl_cf[6]++;
					    double mindr_phj = 999;
					    for (int i = 0; i < tlv_j.size(); i++) {
					        if (tlv_j.at(i).Pt() > 25 && fabs(tlv_j.at(i).Eta()) < 2.5) {
					    	if (tlv_j.at(i).DeltaR(tlv_y) < mindr_phj) {
					    	    mindr_phj = tlv_j.at(i).DeltaR(tlv_y);
					    	}
					        }
					    }
					    if (mindr_phj > 0.4) {
						forK_sl_cf[7]++;
						double mindr_phl0 = tlv_l.at(0).DeltaR(tlv_y);
						if (mindr_phl0 > 1.0) {
						    forK_sl_cf[8]++;
						    double drbj = tlv_bj.at(0).DeltaR(tlv_bj.at(1));
//						    if (drbj > 0.4) {
						    if (1) {
							forK_sl_cf[9]++;
							h_sl_phpt->Fill(tlv_y.Pt());
							h_sl_pheta->Fill(tlv_y.Eta());
							h_sl_leppt->Fill(tlv_l.at(0).Pt());
							h_sl_lepeta->Fill(tlv_l.at(0).Eta());
							if (tlv_gj.size() >= 1) h_sl_jet1pt->Fill(tlv_gj.at(order_gj.at(0)).Pt());
							if (tlv_gj.size() >= 1) h_sl_jet1eta->Fill(tlv_gj.at(order_gj.at(0)).Eta());
							if (tlv_gj.size() >= 2) h_sl_jet2pt->Fill(tlv_gj.at(order_gj.at(1)).Pt());
							if (tlv_gj.size() >= 2) h_sl_jet2eta->Fill(tlv_gj.at(order_gj.at(1)).Eta());
							if (tlv_gj.size() >= 3) h_sl_jet3pt->Fill(tlv_gj.at(order_gj.at(2)).Pt());
							if (tlv_gj.size() >= 3) h_sl_jet3eta->Fill(tlv_gj.at(order_gj.at(2)).Eta());
							if (tlv_gj.size() >= 4) h_sl_jet4pt->Fill(tlv_gj.at(order_gj.at(3)).Pt());
							if (tlv_gj.size() >= 4) h_sl_jet4eta->Fill(tlv_gj.at(order_gj.at(3)).Eta());
							h_scale_sl->Fill(scale);
						    }
						}
					    }
					}
				    }
				}
			    }
			}    
		    }
		}
		if (nel == 1 && nmu == 1) {
		    forXcheck_dl_cf[0]++;
		    {
			double mindr_phl = min(tlv_l.at(0).DeltaR(tlv_y),tlv_l.at(1).DeltaR(tlv_y));
		    	if (mindr_phl > 0.5) {
		    	   forXcheck_dl_cf[1]++;
			    double mindr_phj = 999;
		            for (int i = 0; i < tlv_j.size(); i++) {
		                if (tlv_j.at(i).Pt() > 25 && fabs(tlv_j.at(i).Eta()) < 2.5) {
		            	if (tlv_j.at(i).DeltaR(tlv_y) < mindr_phj) {
		            	    mindr_phj = tlv_j.at(i).DeltaR(tlv_y);
		            	}
		                }
		            }
		            if (mindr_phj > 0.2) {
		    	       forXcheck_dl_cf[2]++;
			    }
		    	}
		    }
		    forK_dl_cf[0]++;
		    if ((tlv_l.at(0).Pt() > 15 || tlv_l.at(1).Pt() > 15) && fabs(tlv_l.at(0).Eta()) < 5 && fabs(tlv_l.at(1).Eta()) < 5) {
		        forK_dl_cf[1]++;
			if ((tlv_l.at(0).Pt() > 25 || tlv_l.at(1).Pt() > 25) && fabs(tlv_l.at(0).Eta()) < 2.5 && fabs(tlv_l.at(1).Eta()) < 2.5) {
			    forK_dl_cf[2]++;
			    if (tlv_y.Pt() > 15 && fabs(tlv_y.Eta()) < 5.0) {
				forK_dl_cf[3]++;
				if (tlv_y.Pt() > 20 && fabs(tlv_y.Eta()) < 2.37) {
				    forK_dl_cf[4]++;
				    int nbasicgoodj = 0;
		            	    for (int i = 0; i < tlv_j.size(); i++) {
		            	        if (tlv_j.at(i).Pt() > 10 && fabs(tlv_j.at(i).Eta()) < 5.0) {
		            	    	nbasicgoodj++;
		            	        }
		            	    }
				    if (!donjet || nbasicgoodj >= 2) {
					forK_dl_cf[5]++;
					int ngoodj = 0;
					vector<TLorentzVector> tlv_gj;
					vector<double> tlv_gj_pt;
		            		for (int i = 0; i < tlv_j.size(); i++) {
		            		    if (tlv_j.at(i).Pt() > 25 && fabs(tlv_j.at(i).Eta()) < 2.5) {
					        ngoodj++;
						tlv_gj.push_back(tlv_j.at(i));
						tlv_gj_pt.push_back(tlv_j.at(i).Pt());
		            		    }
		            		}
					vector<int> order_gj;
					OrderFVector(tlv_gj_pt, order_gj);
					if (!donjet || ngoodj >= 2) {
					    forK_dl_cf[6]++;
					    double mindr_phj = 999;
					    for (int i = 0; i < tlv_j.size(); i++) {
					        if (tlv_j.at(i).Pt() > 25 && fabs(tlv_j.at(i).Eta()) < 2.5) {
					    	if (tlv_j.at(i).DeltaR(tlv_y) < mindr_phj) {
					    	    mindr_phj = tlv_j.at(i).DeltaR(tlv_y);
					    	}
					        }
					    }
					    if (mindr_phj > 0.4) {
						forK_dl_cf[7]++;
						double mindr_l0j = 999;
						for (int i = 0; i < tlv_j.size(); i++) {
						    if (tlv_j.at(i).Pt() > 25 && fabs(tlv_j.at(i).Eta()) < 2.5) {
						       if (tlv_j.at(i).DeltaR(tlv_l.at(0)) < mindr_l0j) {
							       mindr_l0j = tlv_j.at(i).DeltaR(tlv_l.at(0));
							   }
						    }
						}
						double mindr_l1j = 999;
						for (int i = 0; i < tlv_j.size(); i++) {
						    if (tlv_j.at(i).Pt() > 25 && fabs(tlv_j.at(i).Eta()) < 2.5) {
						       if (tlv_j.at(i).DeltaR(tlv_l.at(1)) < mindr_l1j) {
							       mindr_l1j = tlv_j.at(i).DeltaR(tlv_l.at(1));
							   }
						    }
						}
						if (mindr_l0j > 0.4) {
						    forK_dl_cf[8]++;
						    if (mindr_l1j > 0.4) {
							forK_dl_cf[9]++;
							double mindr_phl0 = tlv_l.at(0).DeltaR(tlv_y);
							if (mindr_phl0 > 1.0) {
							    forK_dl_cf[10]++;
							    double mindr_phl1 = tlv_l.at(1).DeltaR(tlv_y);
							    if (mindr_phl1 > 1.0) {
								forK_dl_cf[11]++;
								double drbj = tlv_bj.at(0).DeltaR(tlv_bj.at(1));
//								if (drbj > 0.4) {
								if (1) {
								    forK_dl_cf[12]++;
								    h_dl_phpt->Fill(tlv_y.Pt());
								    h_dl_pheta->Fill(tlv_y.Eta());
								    h_dl_elpt->Fill(tlv_e.Pt());
								    h_dl_eleta->Fill(tlv_e.Eta());
								    h_dl_mupt->Fill(tlv_m.Pt());
								    h_dl_mueta->Fill(tlv_e.Eta());
								    if (order_gj.size() >= 1) h_dl_jet1pt->Fill(tlv_gj.at(order_gj.at(0)).Pt());
								    if (order_gj.size() >= 1) h_dl_jet1eta->Fill(tlv_gj.at(order_gj.at(0)).Eta());
								    if (order_gj.size() >= 2) h_dl_jet2pt->Fill(tlv_gj.at(order_gj.at(1)).Pt());
								    if (order_gj.size() >= 2) h_dl_jet2eta->Fill(tlv_gj.at(order_gj.at(1)).Eta());
								    h_scale_dl->Fill(scale);
								}
							    }
							}
						    }
						}
					    }
					}
				    }
				}
			    }
			}    
		    }
		}
	    }
	    event.clear();
	}

	if (!newevt) {
	   if (strline.Contains("<event>")) {
		newevt = true;
	    }
	}
    }

    cout << "In total " << nevt << " events" << endl;
    cout << "DL events: " << dlevt << " " << double(dlevt)/nevt << endl;
    cout << "SL events: " << slevt << " " << double(slevt)/nevt << endl;

    TH1F*h_sl_cf = new TH1F("sl_cf", "sl_cf", ncf_sl+1, 0, ncf_sl+1);
    h_sl_cf->SetBinContent(1, nevt);
    h_sl_cf->SetBinError(1, sqrt(nevt));
    h_sl_cf->GetXaxis()->SetBinLabel(1, "Total");
    for (int i = 0; i < ncf_sl; i++) {
	cout << forK_sl_cf_name[i] << ": " << forK_sl_cf[i] << endl;
	h_sl_cf->SetBinContent(i+2, forK_sl_cf[i]);
	h_sl_cf->SetBinError(i+2, sqrt(forK_sl_cf[i]));
	h_sl_cf->GetXaxis()->SetBinLabel(i+2, forK_sl_cf_name[i].c_str());
    }
    TH1F*h_dl_cf = new TH1F("dl_cf", "dl_cf", ncf_dl+1, 0, ncf_dl+1);
    h_dl_cf->SetBinContent(1, nevt);
    h_dl_cf->SetBinError(1, sqrt(nevt));
    h_dl_cf->GetXaxis()->SetBinLabel(1, "Total");
    for (int i = 0; i < ncf_dl; i++) {
	cout << forK_dl_cf_name[i] << ": " << forK_dl_cf[i] << endl;
	h_dl_cf->SetBinContent(i+2, forK_dl_cf[i]);
	h_dl_cf->SetBinError(i+2, sqrt(forK_dl_cf[i]));
	h_dl_cf->GetXaxis()->SetBinLabel(i+2, forK_dl_cf_name[i].c_str());
    }
    //for (int i = 0; i < 3; i++) {
    //    cout << forXcheck_dl_cf_name[i] << ": " << forXcheck_dl_cf[i] << endl;
    //    h_dl_cf->SetBinContent(i+2, forXcheck_dl_cf[i]);
    //    h_dl_cf->SetBinError(i+2, sqrt(forXcheck_dl_cf[i]));
    //    h_dl_cf->GetXaxis()->SetBinLabel(i+2, forXcheck_dl_cf_name[i].c_str());
    //}

    double acc_dl = forK_dl_cf[ncf_dl-1]/double(nevt);
    double e_acc_dl = acc_dl * (1/sqrt(forK_dl_cf[ncf_dl-1]));
    cout << "acc dl: " << acc_dl << " " << e_acc_dl << endl;

    double acc_sl = forK_sl_cf[ncf_sl-1]/double(nevt);
    double e_acc_sl = acc_sl * (1/sqrt(forK_sl_cf[ncf_sl-1]));
    cout << "acc sl: " << acc_sl << " " << e_acc_sl << endl;

    //{
    //double acc = forXcheck_dl_cf[2]/double(nevt);
    //double e_acc = acc * (1/sqrt(forXcheck_dl_cf[2]));
    //cout << "acc (forXcheck): " << acc << " " << e_acc << endl;
    //}

    double scale = 0.5*xsec/nevt;
    //h_sl_phpt->Scale(scale);
    //h_sl_pheta->Scale(scale);
    //h_sl_leppt->Scale(scale);
    //h_sl_lepeta->Scale(scale);
    //h_sl_jet1pt->Scale(scale);
    //h_sl_jet1eta->Scale(scale);
    //h_sl_jet2pt->Scale(scale);
    //h_sl_jet2eta->Scale(scale);
    //h_sl_jet3pt->Scale(scale);
    //h_sl_jet3eta->Scale(scale);
    //h_sl_jet4pt->Scale(scale);
    //h_sl_jet4eta->Scale(scale);
    //h_dl_phpt->Scale(scale);
    //h_dl_pheta->Scale(scale);
    //h_dl_elpt->Scale(scale);
    //h_dl_eleta->Scale(scale);
    //h_dl_mupt->Scale(scale);
    //h_dl_mueta->Scale(scale);
    //h_dl_jet1pt->Scale(scale);
    //h_dl_jet1eta->Scale(scale);
    //h_dl_jet2pt->Scale(scale);
    //h_dl_jet2eta->Scale(scale);
    cout << "Int xsec sl phpt: " << h_sl_phpt->Integral(1, h_sl_phpt->GetNbinsX()+1) << endl;
    cout << "Int xsec sl pheta: " << h_sl_pheta->Integral(1, h_sl_pheta->GetNbinsX()+1) << endl;
    cout << "Int xsec dl phpt: " << h_dl_phpt->Integral(1, h_dl_phpt->GetNbinsX()+1) << endl;
    cout << "Int xsec dl pheta: " << h_dl_pheta->Integral(1, h_dl_pheta->GetNbinsX()+1) << endl;

    string savename = output + ".root";
    cout << "save output to " << savename << endl;
    TFile* f=new TFile(savename.c_str(),"recreate");
    h_scale->Write();
    h_scale_sl->Write();
    h_scale_dl->Write();
    h_scale_M->Write();
    h_weight->Write();
    h_sl_phpt->Write();
    h_sl_pheta->Write();
    h_sl_leppt->Write();
    h_sl_lepeta->Write();
    h_sl_jet1pt->Write();
    h_sl_jet1eta->Write();
    h_sl_jet2pt->Write();
    h_sl_jet2eta->Write();
    h_sl_jet3pt->Write();
    h_sl_jet3eta->Write();
    h_sl_jet4pt->Write();
    h_sl_jet4eta->Write();
    h_dl_phpt->Write();
    h_dl_pheta->Write();
    h_dl_elpt->Write();
    h_dl_eleta->Write();
    h_dl_mupt->Write();
    h_dl_mueta->Write();
    h_dl_jet1pt->Write();
    h_dl_jet1eta->Write();
    h_dl_jet2pt->Write();
    h_dl_jet2eta->Write();
    h_sl_cf->Write();
    h_dl_cf->Write();
    f->Close();
}
