#include <TFile.h>
#include <TH1D.h>
#include <TCanvas.h>
#include <TLegend.h>
#include <TPad.h>
#include <TLine.h>
#include <string>
#include <TLatex.h>
#include <TMath.h>
#include <vector>
#include <TGraphErrors.h>
#include <map>
#include <TIterator.h>
#include <TKey.h>
#include <TObject.h>
#include <THStack.h>

#include "AtlasStyle.h"
#include "PlotComparor.h"
#include "StringPlayer.h"
#include "ConfigReader.h"
#include "Logger.h"
#include <sys/types.h>
#include <dirent.h>

typedef std::vector<std::string> stringvec;
void read_directory(const std::string& name, stringvec& v)
{
    DIR* dirp = opendir(name.c_str());
    struct dirent * dp;
    while ((dp = readdir(dirp)) != NULL) {
	if (string(dp->d_name).find("user",0) == string::npos) continue;
        v.push_back(dp->d_name);
    }
    closedir(dirp);
}
struct Process{
    string name;
    double xsec;
    double xsec_err;
    double xsec_fidu;
    double xsec_fidu_err;
    double acc;
    double acc_err;
    double init_norm;
    double final_norm;
    double final_norm_err;
    TH1F* h_ph_pt_unfold;
};
void GetProcess(Process &proc, string dir) {
    cout << "Get process -> " << proc.name << endl;
    stringvec files; read_directory(dir, files);
    TH1F* h_cutflow = NULL;
    TH1F* h_ph_pt_unfold = NULL;
    for (int i = 0; i < files.size(); i++) {
	TFile* f = new TFile((dir+files.at(i)).c_str());
	TH1F* h_cutflow_tmp = (TH1F*)f->Get(string("ATLAS_2017_I1604029/cutflow").c_str());
	TH1F* h_ph_pt_unfold_tmp = (TH1F*)f->Get(string("ATLAS_2017_I1604029/ph_pt_unfold").c_str());
	if (!h_cutflow_tmp) {
	    cout << "WARN: can't find histogram -> ATLAS_2017_I1604029/cutflow" << endl;
	    cout << "      in file -> " << dir+files.at(i) << endl;
	    f->ls();
	}
	if (!h_ph_pt_unfold_tmp) {
	    cout << "WARN: can't find histogram -> ATLAS_2017_I1604029/ph_pt_unfold" << endl;
	    cout << "      in file -> " << dir+files.at(i) << endl;
	    f->ls();
	}
	if (h_cutflow == NULL) h_cutflow = (TH1F*)h_cutflow_tmp->Clone();
	else h_cutflow->Add(h_cutflow_tmp);
	if (h_ph_pt_unfold == NULL) h_ph_pt_unfold = (TH1F*)h_ph_pt_unfold_tmp->Clone();
	else h_ph_pt_unfold->Add(h_ph_pt_unfold_tmp);
	//f->Close();
	//delete f;
    }

    proc.init_norm = h_cutflow->GetBinContent(1);
    if (proc.name.find("isr",0) != string::npos) {
	proc.final_norm = h_cutflow->GetBinContent(8);
	proc.final_norm_err = h_cutflow->GetBinError(8);
    } else {
	proc.final_norm = h_cutflow->GetBinContent(7);
	proc.final_norm_err = h_cutflow->GetBinError(7);
    }
    proc.xsec_err = 0;
    proc.acc = proc.final_norm/proc.init_norm;
    proc.acc_err = proc.acc * proc.final_norm_err/proc.final_norm;
    proc.xsec_fidu = proc.xsec*proc.acc;
    proc.xsec_fidu_err = proc.xsec_fidu * proc.acc_err/proc.acc;
    for (int i = 1; i <= h_ph_pt_unfold->GetNbinsX(); i++) {
	double xsec = h_ph_pt_unfold->GetBinContent(i) / proc.init_norm * proc.xsec;
	double xsec_err = xsec*h_ph_pt_unfold->GetBinError(i)/h_ph_pt_unfold->GetBinContent(i);
	h_ph_pt_unfold->SetBinContent(i, xsec);
	h_ph_pt_unfold->SetBinError(i, xsec_err);
	//cout << proc.xsec << " " << xsec << endl;
    }
    proc.h_ph_pt_unfold = h_ph_pt_unfold;
}
void MergeProcesses(Process &sigma1, Process &sigma2, Process &m, Process&p, Process&n) {
    sigma1.xsec = (p.xsec - m.xsec)/2;
    sigma2.xsec = (p.xsec + m.xsec)/2 - n.xsec;
    sigma1.xsec_err = 0;
    sigma2.xsec_err = 0;
    sigma1.xsec_fidu = (p.xsec_fidu - m.xsec_fidu)/2;
    sigma2.xsec_fidu = (p.xsec_fidu + m.xsec_fidu)/2 - n.xsec_fidu;
    sigma1.xsec_fidu_err = sqrt(pow(p.xsec_fidu_err/2,2) + pow(m.xsec_fidu_err/2,2));
    sigma2.xsec_fidu_err = sqrt(pow(p.xsec_fidu_err/2,2) + pow(m.xsec_fidu_err/2,2) + pow(n.xsec_fidu_err,2));
    sigma1.h_ph_pt_unfold = (TH1F*)p.h_ph_pt_unfold->Clone();
    sigma2.h_ph_pt_unfold = (TH1F*)p.h_ph_pt_unfold->Clone();
    for (int i = 1; i <= p.h_ph_pt_unfold->GetNbinsX(); i++) {
	double xsec_m = m.h_ph_pt_unfold->GetBinContent(i);
	double xsec_p = p.h_ph_pt_unfold->GetBinContent(i);
	double xsec_n = n.h_ph_pt_unfold->GetBinContent(i);
	double xsec_m_relerr = m.h_ph_pt_unfold->GetBinError(i)/m.h_ph_pt_unfold->GetBinContent(i);
	double xsec_p_relerr = p.h_ph_pt_unfold->GetBinError(i)/p.h_ph_pt_unfold->GetBinContent(i);
	double xsec_n_relerr = n.h_ph_pt_unfold->GetBinError(i)/n.h_ph_pt_unfold->GetBinContent(i);
	double xsec_m_err = xsec_m*xsec_m_relerr;
	double xsec_p_err = xsec_p*xsec_p_relerr;
	double xsec_n_err = xsec_n*xsec_n_relerr;
	double xsec_sigma1 = (xsec_p - xsec_m)/2;
	double xsec_sigma2 = (xsec_p + xsec_m)/2 - xsec_n;
	double xsec_sigma1_err = sqrt(pow(xsec_p_err/2,2) + pow(xsec_m_err/2,2));
	double xsec_sigma2_err = sqrt(pow(xsec_p_err/2,2) + pow(xsec_m_err/2,2) + pow(xsec_n_err,2));
	//cout << xsec_n << endl;
	//cout << xsec_m << " " << xsec_p << endl;
	//cout << xsec_sigma1 <<  " " << xsec_sigma2 << endl;

	sigma1.h_ph_pt_unfold->SetBinContent(i, xsec_sigma1);
	sigma1.h_ph_pt_unfold->SetBinError(i, xsec_sigma1_err);
	sigma2.h_ph_pt_unfold->SetBinContent(i, xsec_sigma2);
	sigma2.h_ph_pt_unfold->SetBinError(i, xsec_sigma2_err);
    }
}

using namespace std;

int main(int argc, char * argv[]) {
    Logger *lg = new Logger("MAIN");
    lg->Info("s", "Hi, this is the main programm~");

    Process sm_sl_nominal_isr; sm_sl_nominal_isr.name = "sm_sl_nominal_isr"; sm_sl_nominal_isr.xsec = 5.4338E-03*10e6; GetProcess(sm_sl_nominal_isr, "/eos/atlas/user/y/yili/EventStore/Rivet/user.yili.mc15_14TeV.410389.ttgamma_nonallhadronic.evgen.EVNT.e6540.rivet.v03_Rivet/");
    Process sm_sl_nominal; sm_sl_nominal.name = "sm_sl_nominal"; sm_sl_nominal.xsec = 5.4338E-03*10e6; GetProcess(sm_sl_nominal, "/eos/atlas/user/y/yili/EventStore/Rivet/user.yili.mc15_14TeV.410389.ttgamma_nonallhadronic.evgen.EVNT.e6540.rivet.v02_Rivet/");
    Process sm_sl; sm_sl.name = "sm_sl"; sm_sl.xsec = 3.1886E-03*5.0707E-01*10e6; GetProcess(sm_sl, "/eos/atlas/user/y/yili/EventStore/Rivet/user.yili.mc15_14TeV.410382.ttgamma_eft_sm.evgen.EVNT.e7091.rivet.v02_Rivet/");
    Process sl_ctbm; sl_ctbm.name = "sl_ctbm"; sl_ctbm.xsec = 3.3458E-03*5.0840E-01*10e6; GetProcess(sl_ctbm, "/eos/atlas/user/y/yili/EventStore/Rivet/user.yili.mc15_14TeV.410383.ttgamma_eft_ctbm.evgen.EVNT.e7091.rivet.v02_Rivet/");
    Process sl_ctbp; sl_ctbp.name = "sl_ctbp"; sl_ctbp.xsec = 3.5657E-03*5.1024E-01*10e6; GetProcess(sl_ctbp, "/eos/atlas/user/y/yili/EventStore/Rivet/user.yili.mc15_14TeV.410384.ttgamma_eft_ctbp.evgen.EVNT.e7091.rivet.v02_Rivet/");
    Process sl_ctgm; sl_ctgm.name = "sl_ctgm"; sl_ctgm.xsec = 0.00293115*0.506645*10e6; GetProcess(sl_ctgm, "/eos/atlas/user/y/yili/EventStore/Rivet/user.yili.mc15_14TeV.410385.ttgamma_eft_ctgm.evgen.EVNT.e7091.rivet.v02_Rivet/");
    Process sl_ctgp; sl_ctgp.name = "sl_ctgp"; sl_ctgp.xsec = 0.00346197*0.506235*10e6; GetProcess(sl_ctgp, "/eos/atlas/user/y/yili/EventStore/Rivet/user.yili.mc15_14TeV.410386.ttgamma_eft_ctgp.evgen.EVNT.e7091.rivet.v02_Rivet/");
    Process sl_ctwm; sl_ctwm.name = "sl_ctwm"; sl_ctwm.xsec = 0.00318128*0.508865*10e6; GetProcess(sl_ctwm, "/eos/atlas/user/y/yili/EventStore/Rivet/user.yili.mc15_14TeV.410387.ttgamma_eft_ctwm.evgen.EVNT.e7091.rivet.v02_Rivet/");
    Process sl_ctwp; sl_ctwp.name = "sl_ctwp"; sl_ctwp.xsec = 0.00327284*0.50797*10e6; GetProcess(sl_ctwp, "/eos/atlas/user/y/yili/EventStore/Rivet/user.yili.mc15_14TeV.410388.ttgamma_eft_ctwp.evgen.EVNT.e7091.rivet.v02_Rivet/");
    Process sl_ctbsigma1; sl_ctbsigma1.name = "sl_ctbsigma1"; Process sl_ctbsigma2; sl_ctbsigma2.name = "sl_ctbsigma2"; 
    MergeProcesses(sl_ctbsigma1, sl_ctbsigma2, sl_ctbm, sl_ctbp, sm_sl);
    Process sl_ctgsigma1; sl_ctgsigma1.name = "sl_ctgsigma1"; Process sl_ctgsigma2; sl_ctgsigma2.name = "sl_ctgsigma2"; 
    MergeProcesses(sl_ctgsigma1, sl_ctgsigma2, sl_ctgm, sl_ctgp, sm_sl);
    Process sl_ctwsigma1; sl_ctwsigma1.name = "sl_ctwsigma1"; Process sl_ctwsigma2; sl_ctwsigma2.name = "sl_ctwsigma2"; 
    MergeProcesses(sl_ctwsigma1, sl_ctwsigma2, sl_ctwm, sl_ctwp, sm_sl);

    Process sm_ll_nominal_isr; sm_ll_nominal_isr.name = "sm_ll_nominal_isr"; sm_ll_nominal_isr.xsec = 5.4338E-03*10e6; GetProcess(sm_ll_nominal_isr, "/eos/atlas/user/y/yili/EventStore/Rivet/user.yili.mc15_14TeV.410389.ttgamma_nonallhadronic.evgen.EVNT.e6540.rivet.ll.v03_Rivet/");
    Process sm_ll_nominal; sm_ll_nominal.name = "sm_ll_nominal"; sm_ll_nominal.xsec = 5.4338E-03*10e6; GetProcess(sm_ll_nominal, "/eos/atlas/user/y/yili/EventStore/Rivet/user.yili.mc15_14TeV.410389.ttgamma_nonallhadronic.evgen.EVNT.e6540.rivet.ll.v02_Rivet/");
    Process sm_ll; sm_ll.name = "sm_ll"; sm_ll.xsec = 3.1886E-03*5.0707E-01*10e6; GetProcess(sm_ll, "/eos/atlas/user/y/yili/EventStore/Rivet/user.yili.mc15_14TeV.410382.ttgamma_eft_sm.evgen.EVNT.e7091.rivet.ll.v02_Rivet/");
    Process ll_ctbm; ll_ctbm.name = "ll_ctbm"; ll_ctbm.xsec = 3.3458E-03*5.0840E-01*10e6; GetProcess(ll_ctbm, "/eos/atlas/user/y/yili/EventStore/Rivet/user.yili.mc15_14TeV.410383.ttgamma_eft_ctbm.evgen.EVNT.e7091.rivet.ll.v02_Rivet/");
    Process ll_ctbp; ll_ctbp.name = "ll_ctbp"; ll_ctbp.xsec = 3.5657E-03*5.1024E-01*10e6; GetProcess(ll_ctbp, "/eos/atlas/user/y/yili/EventStore/Rivet/user.yili.mc15_14TeV.410384.ttgamma_eft_ctbp.evgen.EVNT.e7091.rivet.ll.v02_Rivet/");
    Process ll_ctgm; ll_ctgm.name = "ll_ctgm"; ll_ctgm.xsec = 0.00293115*0.506645*10e6; GetProcess(ll_ctgm, "/eos/atlas/user/y/yili/EventStore/Rivet/user.yili.mc15_14TeV.410385.ttgamma_eft_ctgm.evgen.EVNT.e7091.rivet.ll.v02_Rivet/");
    Process ll_ctgp; ll_ctgp.name = "ll_ctgp"; ll_ctgp.xsec = 0.00346197*0.506235*10e6; GetProcess(ll_ctgp, "/eos/atlas/user/y/yili/EventStore/Rivet/user.yili.mc15_14TeV.410386.ttgamma_eft_ctgp.evgen.EVNT.e7091.rivet.ll.v02_Rivet/");
    Process ll_ctwm; ll_ctwm.name = "ll_ctwm"; ll_ctwm.xsec = 0.00318128*0.508865*10e6; GetProcess(ll_ctwm, "/eos/atlas/user/y/yili/EventStore/Rivet/user.yili.mc15_14TeV.410387.ttgamma_eft_ctwm.evgen.EVNT.e7091.rivet.ll.v02_Rivet/");
    Process ll_ctwp; ll_ctwp.name = "ll_ctwp"; ll_ctwp.xsec = 0.00327284*0.50797*10e6; GetProcess(ll_ctwp, "/eos/atlas/user/y/yili/EventStore/Rivet/user.yili.mc15_14TeV.410388.ttgamma_eft_ctwp.evgen.EVNT.e7091.rivet.ll.v02_Rivet/");
    Process ll_ctbsigma1; ll_ctbsigma1.name = "ll_ctbsigma1"; Process ll_ctbsigma2; ll_ctbsigma2.name = "ll_ctbsigma2"; 
    MergeProcesses(ll_ctbsigma1, ll_ctbsigma2, ll_ctbm, ll_ctbp, sm_ll);
    Process ll_ctgsigma1; ll_ctgsigma1.name = "ll_ctgsigma1"; Process ll_ctgsigma2; ll_ctgsigma2.name = "ll_ctgsigma2"; 
    MergeProcesses(ll_ctgsigma1, ll_ctgsigma2, ll_ctgm, ll_ctgp, sm_ll);
    Process ll_ctwsigma1; ll_ctwsigma1.name = "ll_ctwsigma1"; Process ll_ctwsigma2; ll_ctwsigma2.name = "ll_ctwsigma2"; 
    MergeProcesses(ll_ctwsigma1, ll_ctwsigma2, ll_ctwm, ll_ctwp, sm_ll);

    cout << fixed << setprecision(2);

    cout << setw(15) << "nominal isr" << setw(15) << "err" << setw(15) << "nominal all" << setw(15) << "err" << endl;
    cout << setw(15) << sm_sl_nominal_isr.xsec_fidu << setw(15) << sm_sl_nominal_isr.xsec_fidu_err << setw(15) << sm_sl_nominal.xsec_fidu << setw(15) << sm_sl_nominal.xsec_fidu_err << endl;
    cout << setw(15) << sm_ll_nominal_isr.xsec_fidu << setw(15) << sm_ll_nominal_isr.xsec_fidu_err << setw(15) << sm_ll_nominal.xsec_fidu << setw(15) << sm_ll_nominal.xsec_fidu_err << endl;
    cout << endl;

    cout << setw(15) << "" << setw(15) << "xsec(m)" << setw(15) << "err" << setw(15) << "xsec(p)" << setw(15) << "err" << setw(15) << "sigma1" << setw(15) << "err" << setw(15) << "sigma2" << setw(15) << "err" << endl;
    cout << setw(15) << "sm_sl" << setw(15) << sm_sl.xsec << setw(15) << sm_sl.xsec_err << endl;
    cout << setw(15) << "sl_ctb" << setw(15) << sl_ctbp.xsec << setw(15) << sl_ctbp.xsec_err << setw(15) << sl_ctbm.xsec << setw(15) << sl_ctbm.xsec_err << setw(15) << sl_ctbsigma1.xsec << setw(15) << sl_ctbsigma1.xsec_err << setw(15) << sl_ctbsigma2.xsec << setw(15) << sl_ctbsigma2.xsec_err << endl;
    cout << setw(15) << "sl_ctg" << setw(15) << sl_ctgp.xsec << setw(15) << sl_ctgp.xsec_err << setw(15) << sl_ctgm.xsec << setw(15) << sl_ctgm.xsec_err << setw(15) << sl_ctgsigma1.xsec << setw(15) << sl_ctgsigma1.xsec_err << setw(15) << sl_ctgsigma2.xsec << setw(15) << sl_ctgsigma2.xsec_err << endl;
    cout << setw(15) << "sl_ctw" << setw(15) << sl_ctwp.xsec << setw(15) << sl_ctwp.xsec_err << setw(15) << sl_ctwm.xsec << setw(15) << sl_ctwm.xsec_err << setw(15) << sl_ctwsigma1.xsec << setw(15) << sl_ctwsigma1.xsec_err << setw(15) << sl_ctwsigma2.xsec << setw(15) << sl_ctwsigma2.xsec_err << endl;
    cout << setw(15) << "sm_ll" << setw(15) << sm_ll.xsec << setw(15) << sm_ll.xsec_err << endl;
    cout << setw(15) << "ll_ctb" << setw(15) << ll_ctbp.xsec << setw(15) << ll_ctbp.xsec_err << setw(15) << ll_ctbm.xsec << setw(15) << ll_ctbm.xsec_err << setw(15) << ll_ctbsigma1.xsec << setw(15) << ll_ctbsigma1.xsec_err << setw(15) << ll_ctbsigma2.xsec << setw(15) << ll_ctbsigma2.xsec_err << endl;
    cout << setw(15) << "ll_ctg" << setw(15) << ll_ctgp.xsec << setw(15) << ll_ctgp.xsec_err << setw(15) << ll_ctgm.xsec << setw(15) << ll_ctgm.xsec_err << setw(15) << ll_ctgsigma1.xsec << setw(15) << ll_ctgsigma1.xsec_err << setw(15) << ll_ctgsigma2.xsec << setw(15) << ll_ctgsigma2.xsec_err << endl;
    cout << setw(15) << "ll_ctw" << setw(15) << ll_ctwp.xsec << setw(15) << ll_ctwp.xsec_err << setw(15) << ll_ctwm.xsec << setw(15) << ll_ctwm.xsec_err << setw(15) << ll_ctwsigma1.xsec << setw(15) << ll_ctwsigma1.xsec_err << setw(15) << ll_ctwsigma2.xsec << setw(15) << ll_ctwsigma2.xsec_err << endl;
    cout << endl;

    cout << setw(15) << "" << setw(15) << "xsec(m)" << setw(15) << "err" << setw(15) << "xsec(p)" << setw(15) << "err" << setw(15) << "sigma1" << setw(15) << "err" << setw(15) << "sigma2" << setw(15) << "err" << endl;
    cout << setw(15) << "sm_sl" << setw(15) << sm_sl.xsec_fidu << setw(15) << sm_sl.xsec_fidu_err << endl;
    cout << setw(15) << "sl_ctb" << setw(15) << sl_ctbp.xsec_fidu << setw(15) << sl_ctbp.xsec_fidu_err << setw(15) << sl_ctbm.xsec_fidu << setw(15) << sl_ctbm.xsec_fidu_err << setw(15) << sl_ctbsigma1.xsec_fidu << setw(15) << sl_ctbsigma1.xsec_fidu_err << setw(15) << sl_ctbsigma2.xsec_fidu << setw(15) << sl_ctbsigma2.xsec_fidu_err << endl;
    cout << setw(15) << "sl_ctg" << setw(15) << sl_ctgp.xsec_fidu << setw(15) << sl_ctgp.xsec_fidu_err << setw(15) << sl_ctgm.xsec_fidu << setw(15) << sl_ctgm.xsec_fidu_err << setw(15) << sl_ctgsigma1.xsec_fidu << setw(15) << sl_ctgsigma1.xsec_fidu_err << setw(15) << sl_ctgsigma2.xsec_fidu << setw(15) << sl_ctgsigma2.xsec_fidu_err << endl;
    cout << setw(15) << "sl_ctw" << setw(15) << sl_ctwp.xsec_fidu << setw(15) << sl_ctwp.xsec_fidu_err << setw(15) << sl_ctwm.xsec_fidu << setw(15) << sl_ctwm.xsec_fidu_err << setw(15) << sl_ctwsigma1.xsec_fidu << setw(15) << sl_ctwsigma1.xsec_fidu_err << setw(15) << sl_ctwsigma2.xsec_fidu << setw(15) << sl_ctwsigma2.xsec_fidu_err << endl;
    cout << setw(15) << "sm_ll" << setw(15) << sm_ll.xsec_fidu << setw(15) << sm_ll.xsec_fidu_err << endl;
    cout << setw(15) << "ll_ctb" << setw(15) << ll_ctbp.xsec_fidu << setw(15) << ll_ctbp.xsec_fidu_err << setw(15) << ll_ctbm.xsec_fidu << setw(15) << ll_ctbm.xsec_fidu_err << setw(15) << ll_ctbsigma1.xsec_fidu << setw(15) << ll_ctbsigma1.xsec_fidu_err << setw(15) << ll_ctbsigma2.xsec_fidu << setw(15) << ll_ctbsigma2.xsec_fidu_err << endl;
    cout << setw(15) << "ll_ctg" << setw(15) << ll_ctgp.xsec_fidu << setw(15) << ll_ctgp.xsec_fidu_err << setw(15) << ll_ctgm.xsec_fidu << setw(15) << ll_ctgm.xsec_fidu_err << setw(15) << ll_ctgsigma1.xsec_fidu << setw(15) << ll_ctgsigma1.xsec_fidu_err << setw(15) << ll_ctgsigma2.xsec_fidu << setw(15) << ll_ctgsigma2.xsec_fidu_err << endl;
    cout << setw(15) << "ll_ctw" << setw(15) << ll_ctwp.xsec_fidu << setw(15) << ll_ctwp.xsec_fidu_err << setw(15) << ll_ctwm.xsec_fidu << setw(15) << ll_ctwm.xsec_fidu_err << setw(15) << ll_ctwsigma1.xsec_fidu << setw(15) << ll_ctwsigma1.xsec_fidu_err << setw(15) << ll_ctwsigma2.xsec_fidu << setw(15) << ll_ctwsigma2.xsec_fidu_err << endl;
    cout << endl;


    TFile* fout = new TFile("results/Upgrade/EFTeffects.root","recreate");
    fout->cd();

    sm_sl_nominal_isr.h_ph_pt_unfold->Write(sm_sl_nominal_isr.name.c_str());
    sm_sl_nominal.h_ph_pt_unfold->Write(sm_sl_nominal.name.c_str());
    sm_sl.h_ph_pt_unfold->Write(sm_sl.name.c_str());
    sl_ctbm.h_ph_pt_unfold->Write(sl_ctbm.name.c_str());
    sl_ctbp.h_ph_pt_unfold->Write(sl_ctbp.name.c_str());
    sl_ctgm.h_ph_pt_unfold->Write(sl_ctgm.name.c_str());
    sl_ctgp.h_ph_pt_unfold->Write(sl_ctgp.name.c_str());
    sl_ctwm.h_ph_pt_unfold->Write(sl_ctwm.name.c_str());
    sl_ctwp.h_ph_pt_unfold->Write(sl_ctwp.name.c_str());
    sl_ctbsigma1.h_ph_pt_unfold->Write(sl_ctbsigma1.name.c_str());
    sl_ctbsigma2.h_ph_pt_unfold->Write(sl_ctbsigma2.name.c_str());
    sl_ctgsigma1.h_ph_pt_unfold->Write(sl_ctgsigma1.name.c_str());
    sl_ctgsigma2.h_ph_pt_unfold->Write(sl_ctgsigma2.name.c_str());
    sl_ctwsigma1.h_ph_pt_unfold->Write(sl_ctwsigma1.name.c_str());
    sl_ctwsigma2.h_ph_pt_unfold->Write(sl_ctwsigma2.name.c_str());

    sm_ll_nominal_isr.h_ph_pt_unfold->Write(sm_ll_nominal_isr.name.c_str());
    sm_ll_nominal.h_ph_pt_unfold->Write(sm_ll_nominal.name.c_str());
    sm_ll.h_ph_pt_unfold->Write(sm_ll.name.c_str());
    ll_ctbm.h_ph_pt_unfold->Write(ll_ctbm.name.c_str());
    ll_ctbp.h_ph_pt_unfold->Write(ll_ctbp.name.c_str());
    ll_ctgm.h_ph_pt_unfold->Write(ll_ctgm.name.c_str());
    ll_ctgp.h_ph_pt_unfold->Write(ll_ctgp.name.c_str());
    ll_ctwm.h_ph_pt_unfold->Write(ll_ctwm.name.c_str());
    ll_ctwp.h_ph_pt_unfold->Write(ll_ctwp.name.c_str());
    ll_ctbsigma1.h_ph_pt_unfold->Write(ll_ctbsigma1.name.c_str());
    ll_ctbsigma2.h_ph_pt_unfold->Write(ll_ctbsigma2.name.c_str());
    ll_ctgsigma1.h_ph_pt_unfold->Write(ll_ctgsigma1.name.c_str());
    ll_ctgsigma2.h_ph_pt_unfold->Write(ll_ctgsigma2.name.c_str());
    ll_ctwsigma1.h_ph_pt_unfold->Write(ll_ctwsigma1.name.c_str());
    ll_ctwsigma2.h_ph_pt_unfold->Write(ll_ctwsigma2.name.c_str());
}   
