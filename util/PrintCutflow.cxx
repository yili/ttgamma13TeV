#include <TMath.h>
#include <iomanip>
#include <TFile.h>
#include <TH1.h>
#include <TIterator.h>
#include <TKey.h>
#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <map>
#include <string>
#include <time.h>

#include "Logger.h"
#include "StringPlayer.h"
#include "ConfigReader.h"

using namespace std;

int FindToStop(int oldstop, TH1F* h, vector<string>&cuts);
void PrintCutflow(string tag, vector<TH1F*> h, vector<string> cutnames);
void PrintCutflow2(string tag, vector<TH1F*> h, vector<string> cutnames);
void AddHist(TH1F* h, TH1F* tmp);
float scale = 1;
int Precision = 2;
string saveto;

int main(int argc, char* argv[]) {

    Logger *lg = new Logger("MAIN");
    lg->Info("s", "Hi, this is the main program~");
    lg->NewLine();

    string filename;
    if (argc < 2) {
	lg->Info("s", "Will use default config file --> ../config/13TeV_print_cutflow.cfg");
	filename = "../config/13TeV_print_cutflow.cfg" ;
    } else {
	filename = argv[1];
    }
    saveto = "cutflows/";
    saveto += filename.substr(10, filename.size()-13);

    bool Debug = false;
    bool ShowTotBkg = false;
    bool ShowTotMC = false;
    for (int i = 0; i < argc; i++) {
	if (!strcmp(argv[i],"--debug")) {
	    Debug = true;
   	}
	if (!strcmp(argv[i],"--ShowTotBkg")) {
	    ShowTotBkg = true;
   	}
	if (!strcmp(argv[i],"--ShowTotMC")) {
	    ShowTotMC = true;
   	}
	if (!strcmp(argv[i],"--Precision")) {
	    Precision = atoi(string(argv[i+1]).c_str());
   	}
    }

    ConfigReader* rd = new ConfigReader(filename, '_', false);
    rd->Init();

    string inputdir = rd->GetValue("_InputDir");
    vector<vector<string> > Processes = rd->GetAmbiValueAll("_Proc");
    string Scale = rd->GetValue("_Scale");
    if (Scale != "") {
	cout << "use scale " << Scale << endl;
	scale = atof(Scale.c_str());
    }
    int n_proc = Processes.size();
    vector<string> ProcTitles;
    vector<string> ProcSyss;
    vector<vector<string > > ProcInputs;
    vector<string> ProcSFs;
    for (int i = 0; i < n_proc; i++) {
        ProcTitles.push_back(Processes.at(i).at(0));
        ProcSyss.push_back(Processes.at(i).at(1));
	ProcInputs.push_back(SplitToVector(Processes.at(i).at(2)));
	ProcSFs.push_back(Processes.at(i).at(3));
    }

    string Region;
    string SubRegion;
    if (ProcInputs.at(0).at(0).find("_CR1_",0) != string::npos) Region = "CR1";
    else if (ProcInputs.at(0).at(0).find("_EF1_",0) != string::npos) Region = "EF1";
    else if (ProcInputs.at(0).at(0).find("_UR_",0) != string::npos) Region = "UR";

    if (ProcInputs.at(0).at(0).find("_ejets_",0) != string::npos) SubRegion = "ejets";
    else if (ProcInputs.at(0).at(0).find("_mujets_",0) != string::npos) SubRegion = "mujets";
    else if (ProcInputs.at(0).at(0).find("_ee_",0) != string::npos) SubRegion = "ee";
    else if (ProcInputs.at(0).at(0).find("_mumu_",0) != string::npos) SubRegion = "mumu";
    else if (ProcInputs.at(0).at(0).find("_emu_",0) != string::npos) SubRegion = "emu";
    else if (ProcInputs.at(0).at(0).find("_zeg_",0) != string::npos) SubRegion = "zeg";
    else if (ProcInputs.at(0).at(0).find("_zee_",0) != string::npos) SubRegion = "zee";

    vector<TH1F*> hs;
    TH1F* h_data = NULL;
    TH1F* h_total_bkg = NULL;
    TH1F* h_total_mc = NULL;
    TH1F* h_data_mc = NULL;
    int ToStop = 0;
    vector<string> cuts;
    for (int i = 0; i < ProcInputs.size(); i++) {
	TH1F* h = NULL;
	string variation = "_";
	variation += ProcSyss.at(i);
	for (int j = 0; j < ProcInputs.at(i).size(); j++) {
	    if (Debug) cout << ProcInputs.at(i).at(j) << endl;
	    if (ProcInputs.at(i).at(j).find("*",0) == string::npos) {
		string fname = inputdir + ProcInputs.at(i).at(j);
		TFile* f = new TFile(fname.c_str());
		if (!f) {
		    cout << "Error: cannot open " << ProcInputs.at(i).at(j) << endl;
		    exit(-1);
		}
		string namekey;
		if (fname.find("AllSys",0) != string::npos) {
		    int n1 = fname.find("AllSys", 0);
		    namekey = fname.substr(inputdir.size(), n1-5);
		} else if (fname.find(variation.c_str(),0) != string::npos) {
		    int n1 = fname.find(variation.c_str(), 0);
		    namekey = fname.substr(inputdir.size(), n1-inputdir.size()+1);
		}
		string cutflowname = "Cutflow_"; cutflowname += namekey; cutflowname += ProcSyss.at(i);
		TH1F* tmp = (TH1F*)f->Get(cutflowname.c_str());
		if (Debug) {
		    cout << cutflowname << endl;
		    f->ls();
		}
		if (!tmp) {
		    cout << "Warn: cannot find " << cutflowname << endl;
		    exit(-1);
		}
		tmp->Scale(atof(ProcSFs.at(i).c_str()));
		if (h == NULL) {
		    h = (TH1F*)tmp->Clone();
		    ToStop = FindToStop(ToStop, h, cuts);
		} else {
		    int ToStopold = FindToStop(ToStop, h, cuts);
		    int ToStopnew = FindToStop(ToStop, tmp, cuts);
		    if (ToStopnew > ToStopold) {
		        AddHist(tmp,h);
		        h = (TH1F*)tmp->Clone();
		        ToStop = ToStopnew;
		    } else {
		        AddHist(h,tmp);
		        ToStop = ToStopold;
		    }
		}
	    } else {
		time_t seconds = time (NULL);
        	char tmplist[1000];
        	sprintf(tmplist, "log.%ld.tmp", seconds);
        	char tmprun[1000];
        	sprintf(tmprun, "ls %s > %s", (inputdir+ProcInputs.at(i).at(j)).c_str(), tmplist);
        	system(tmprun);
		ifstream ifile;
		ifile.open(tmplist);
		string line;
		while (getline(ifile,line)) {
		    string fname = line;
		    TFile* f = new TFile(fname.c_str());
		    if (!f) {
		        cout << "Error: cannot open " << fname << endl;
		        exit(-1);
		    }
		    string namekey;
		    if (fname.find("AllSys",0) != string::npos) {
		        int n1 = fname.find("AllSys", 0);
		        namekey = fname.substr(inputdir.size(), n1-6);
		    } else if (fname.find(variation.c_str(),0) != string::npos) {
		        int n1 = fname.find(variation.c_str(), 0);
			namekey = fname.substr(inputdir.size(), n1-inputdir.size()+1);
		    }
		    string cutflowname = "Cutflow_"; cutflowname += namekey; cutflowname += ProcSyss.at(i);
		    TH1F* tmp = (TH1F*)f->Get(cutflowname.c_str());
		    if (!tmp) {
		        cout << "Warn: cannot find " << cutflowname << endl;
			    exit(-1);
		    }
		    tmp->Scale(atof(ProcSFs.at(i).c_str()));
		    if (tmp->GetBinContent(1) != 0) {
		        if (h == NULL) {
		            h = (TH1F*)tmp->Clone();
			    ToStop = FindToStop(ToStop, h, cuts);
		        } else {
			    int ToStopold = FindToStop(ToStop, h, cuts);
			    int ToStopnew = FindToStop(ToStop, tmp, cuts);
			    if (ToStopnew >= ToStopold) {
			        AddHist(tmp,h);
			        h = (TH1F*)tmp->Clone();
				ToStop = ToStopnew;
			    } else {
			        AddHist(h,tmp);
				ToStop = ToStopold;
			    }
		        }
		    }
		}
		sprintf(tmprun, "rm %s", tmplist);
		system(tmprun);
	    }
	    if (Debug) cout << "Tag1" << endl;
	}
	if (Debug) cout << "Tag2" << endl;

	if (Debug) {
	    cout << h << endl;
	}

	h->SetName(ProcTitles.at(i).c_str());

	if (Debug) cout << "Tag3" << endl;

	if (ProcTitles.at(i) != "Data") hs.push_back(h);
	if (ProcTitles.at(i) == "Data") {
	    h_data = (TH1F*)h->Clone();
	    h_data->SetName("Data");
	}
	if (ProcTitles.at(i) != "Signal" && ProcTitles.at(i) != "Data") {
	    if (!h_total_bkg) {
	        h_total_bkg = (TH1F*)h->Clone();
	        h_total_bkg->SetName("TotalBkg");
	    } else {
		h_total_bkg->Add(h);
	    }
	}
	if (ProcTitles.at(i) != "Data") {
	    if (!h_total_mc) {
	        h_total_mc = (TH1F*)h->Clone();
	        h_total_mc->SetName("TotalSigBkg");
	    } else {
		h_total_mc->Add(h);
	    }
	}
    }
    if (h_total_bkg && ShowTotBkg) hs.push_back(h_total_bkg);
    if (h_total_mc && ShowTotMC) hs.push_back(h_total_mc);
    if (h_data && h_total_mc) {
        h_data_mc = (TH1F*)h_data->Clone();
        h_data_mc->SetName("Data/MC");
        h_data_mc->Divide(h_total_mc);
    }
    if (h_data) hs.push_back(h_data);
    if (h_data_mc) hs.push_back(h_data_mc);
    
    cout << endl;
    string tag = SubRegion;
    PrintCutflow(tag, hs, cuts);
    //PrintCutflow2(tag, hs, cuts);
    
    return 0;
}

int FindToStop(int oldstop, TH1F* h, vector<string>&cuts) {
    int ToStop = 0;
    for (int i = 1; i <= h->GetNbinsX(); i++) {
	if ((h->GetBinContent(i)) == 0) {
	//if (string(h->GetXaxis()->GetBinLabel(i)).find("STOP",0) != string::npos) {
	    ToStop = i-1;
	    break;
	}
    }
    if (ToStop <= oldstop) {
	return oldstop;
    } else {
	cuts.clear();
	for (int i = 1; i <= ToStop; i++) {
    	    cuts.push_back(h->GetXaxis()->GetBinLabel(i));
	}
	return ToStop;
    }
}

void PrintCutflow(string tag, vector<TH1F*> hs, vector<string> cutnames) {
    if (cutnames.size() == 0) return;
    //if (!hs.at(0)) return;

    cout << "\\documentclass{article}" << endl;
    cout << "\\usepackage{graphicx}" << endl;
    cout << "\\begin{document}" << endl;
    cout << "\\scalebox{" << scale << "}{" << endl;
    cout << "\\begin{tabular}{c|";
    for (int i = 0; i < hs.size(); i++) {
	if (i != hs.size() - 1) cout << "c|";
	else cout << "c";
    }
    cout << "}" << endl;
    
    cout << fixed << setprecision(Precision);
    cout << setw(20) << tag << " &";
    for (int i = 0; i < hs.size(); i++) {
	if (i != hs.size() - 1) cout << setw(35) << hs.at(i)->GetName() << " &";
	else cout << setw(26) << hs.at(i)->GetName() << " \\\\";
    }
    cout << endl;
    for (int i = 0; i < cutnames.size(); i++) {
	if (i != cutnames.size()) cout << "\\hline" << endl;
	cout << setw(20) << cutnames.at(i) << " &";
	for (int j = 0; j < hs.size(); j++) {
	    //int bin_id = -1;
	    //for (int k = 0; k < hs.at(j)->GetNbinsX(); k++) {
	    //    if (hs.at(j)->GetXaxis()->GetBinLabel(k+1) == cutnames.at(i)) {
	    //	bin_id = k;
	    //	break;
	    //    }
	    //}
	    //if (bin_id != -1) {
	    //    cout << setw(20) << hs.at(j)->GetBinContent(bin_id+1) << " +/- " << setw(10) << hs.at(j)->GetBinError(bin_id+1);
	        cout << setw(20) << hs.at(j)->GetBinContent(i+1) << " +/- " << setw(10) << hs.at(j)->GetBinError(i+1);
	    //} else {
	    //    cout << setw(20) << "--" << " +/- " << setw(10) << "--";
	    //}
	    if (j != hs.size() - 1) cout << " &";
	    else cout << " \\\\";
	}
	cout << endl;
    }
    cout << "\\end{tabular}}" << endl;
    cout << "\\end{document}" << endl;
    cout << endl;

    string saveto_tex = saveto; saveto_tex += "tex";
    cout << "Save cutflow.tex to -> " << saveto_tex << endl;
    ofstream ofile_tex;
    ofile_tex.open(saveto_tex.c_str());

    ofile_tex << "\\documentclass{article}" << endl;
    ofile_tex << "\\usepackage{graphicx}" << endl;
    ofile_tex << "\\begin{document}" << endl;
    ofile_tex << "\\scalebox{" << scale << "}{" << endl;
    ofile_tex << "\\begin{tabular}{c|";
    for (int i = 0; i < hs.size(); i++) {
	if (i != hs.size() - 1) ofile_tex << "c|";
	else ofile_tex << "c";
    }
    ofile_tex << "}" << endl;
    
    ofile_tex << fixed << setprecision(2);
    ofile_tex << setw(20) << tag << " &";
    for (int i = 0; i < hs.size(); i++) {
	if (i != hs.size() - 1) ofile_tex << setw(26) << hs.at(i)->GetName() << " &";
	else ofile_tex << setw(26) << hs.at(i)->GetName() << " \\\\";
    }
    ofile_tex << endl;
    for (int i = 0; i < cutnames.size(); i++) {
	if (i != cutnames.size()) ofile_tex << "\\hline" << endl;
	ofile_tex << setw(20) << cutnames.at(i) << " &";
	for (int j = 0; j < hs.size(); j++) {
	    int bin_id = -1;
	    for (int k = 0; k < hs.at(j)->GetNbinsX(); k++) {
	        if (hs.at(j)->GetXaxis()->GetBinLabel(k+1) == cutnames.at(i)) {
	    	bin_id = k;
	    	break;
	        }
	    }
	    if (bin_id != -1) {
	        ofile_tex << setw(15) << hs.at(j)->GetBinContent(bin_id+1) << " +/- " << setw(6) << hs.at(j)->GetBinError(bin_id+1);
	    } else {
	        ofile_tex << setw(15) << "--" << " +/- " << setw(6) << "--";
	    }
	    if (j != hs.size() - 1) ofile_tex << " &";
	    else ofile_tex << " \\\\";
	}
	ofile_tex << endl;
    }
    ofile_tex << "\\end{tabular}}" << endl;
    ofile_tex << "\\end{document}" << endl;
    ofile_tex << endl;

    ofile_tex.close();

    string saveto_txt = saveto; saveto_txt += "txt";
    cout << "Save cutflow.txt to -> " << saveto_txt << endl;
    ofstream ofile_txt;
    ofile_txt.open(saveto_txt.c_str());

    ofile_txt << fixed << setprecision(2);
    ofile_txt << setw(20) << tag << " &";
    for (int i = 0; i < hs.size(); i++) {
	if (i != hs.size() - 1) ofile_txt << setw(24) << hs.at(i)->GetName() << " &";
	else ofile_txt << setw(24) << hs.at(i)->GetName() << " \\\\";
    }
    ofile_txt << endl;
    for (int i = 0; i < cutnames.size(); i++) {
	for (int j = 0; j < hs.size(); j++) {
	    //int bin_id = -1;
	    //for (int k = 0; k < hs.at(j)->GetNbinsX(); k++) {
	    //    if (hs.at(j)->GetXaxis()->GetBinLabel(k+1) == cutnames.at(i)) {
	    //	bin_id = k;
	    //	break;
	    //    }
	    //}
	    //if (bin_id != -1) {
	    //    ofile_txt << setw(15) << hs.at(j)->GetBinContent(bin_id+1) << " +/- " << setw(6) << hs.at(j)->GetBinError(bin_id+1);
	        ofile_txt << setw(15) << hs.at(j)->GetBinContent(i+1) << " +/- " << setw(6) << hs.at(j)->GetBinError(i+1);
	    //} else {
	    //    ofile_txt << setw(15) << "--" << " +/- " << setw(6) << "--";
	    //}
	    if (j != hs.size() - 1) ofile_txt << " ,";
	}
	ofile_txt << endl;
    }
    ofile_txt << endl;

    ofile_txt.close();
}

void PrintCutflow2(string tag, vector<TH1F*> hs, vector<string> cutnames) {
    if (cutnames.size() == 0) return;
    //if (!hs.at(0)) return;
    
    cout << "name   type    value   date" << endl;
    cout << fixed << setprecision(Precision);
    for (int i = 0; i < cutnames.size(); i++) {
	for (int j = 0; j < hs.size(); j++) {
	    cout << hs.at(j)->GetName() << "	" << (string(hs.at(j)->GetName()) == "Signal" ? "Sig" : "Bkg") << " " << hs.at(j)->GetBinContent(i+1) << "  " << cutnames.at(i) << endl;
	}
	cout << endl;
    }
}

void AddHist(TH1F* h, TH1F* tmp) {
    for (int i = 0; i < h->GetNbinsX(); i++) {
	int bin_id = -1;
	string hlabel_i = h->GetXaxis()->GetBinLabel(i+1);
	if (hlabel_i == "") continue;
	for (int j = 0; j < tmp->GetNbinsX(); j++) {
	    string hlabel_j = tmp->GetXaxis()->GetBinLabel(j+1);
	    if (hlabel_j == "") continue;
	    if (hlabel_i == hlabel_j) {
		bin_id = j;
		break;
	    }
	}
	if (bin_id != -1) {
	    h->SetBinContent(i+1, h->GetBinContent(i+1) + tmp->GetBinContent(bin_id+1));
	    h->SetBinError(i+1, sqrt(pow(h->GetBinError(i+1),2) + pow(tmp->GetBinError(bin_id+1),2)));
	}
    }
}
