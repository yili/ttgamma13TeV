#include <TFile.h>
#include <TH2.h>
#include <iostream>
#include <iomanip>
#include <TString.h>
#include "Logger.h"
#include "PlotComparor.h"

using namespace std;

int main()
{
    TFile* f_ee = new TFile("results/Nominal/13TeV_CutZee_ZjetsElEl_Reco_EF1_zee_Nominal_Final03.root");
    TFile* f_TypeA = new TFile("results/Nominal/13TeV_CutZeg_PhMatch_EFakeTypeA_ZjetsElEl_Reco_EF1_zeg_Nominal_Final03.root");
    TFile* f_TypeB = new TFile("results/Nominal/13TeV_CutZeg_PhMatch_EFakeTypeB_ZjetsElEl_Reco_EF1_zeg_Nominal_Final03.root");
    TFile* f_TypeC = new TFile("results/Nominal/13TeV_CutZeg_PhMatch_EFakeTypeC_ZjetsElEl_Reco_EF1_zeg_Nominal_Final03.root");
    TFile* f_TypeD = new TFile("results/Nominal/13TeV_CutZeg_PhMatch_EFakeTypeD_ZjetsElEl_Reco_EF1_zeg_Nominal_Final03.root");

    TH1F* h_ee = (TH1F*)f_ee->Get("Cutflow_13TeV_CutZee_ZjetsElEl_Reco_EF1_zee_Nominal");
    TH1F* h_TypeA = (TH1F*)f_TypeA->Get("Cutflow_13TeV_CutZeg_PhMatch_EFakeTypeA_ZjetsElEl_Reco_EF1_zeg_Nominal");
    TH1F* h_TypeB = (TH1F*)f_TypeB->Get("Cutflow_13TeV_CutZeg_PhMatch_EFakeTypeB_ZjetsElEl_Reco_EF1_zeg_Nominal");
    TH1F* h_TypeC = (TH1F*)f_TypeC->Get("Cutflow_13TeV_CutZeg_PhMatch_EFakeTypeC_ZjetsElEl_Reco_EF1_zeg_Nominal");
    TH1F* h_TypeD = (TH1F*)f_TypeD->Get("Cutflow_13TeV_CutZeg_PhMatch_EFakeTypeD_ZjetsElEl_Reco_EF1_zeg_Nominal");

    double n_ee = h_ee->GetBinContent(5);     double e_ee = h_ee->GetBinError(5); 
    double n_TypeA = h_TypeA->GetBinContent(8);     double e_TypeA = h_TypeA->GetBinError(8); 
    double n_TypeB = h_TypeB->GetBinContent(8);     double e_TypeB = h_TypeB->GetBinError(8); 
    double n_TypeC = h_TypeC->GetBinContent(8);     double e_TypeC = h_TypeC->GetBinError(8); 
    double n_TypeD = h_TypeD->GetBinContent(8);     double e_TypeD = h_TypeD->GetBinError(8); 
    double n_ey = n_TypeA + n_TypeB + n_TypeC + n_TypeD;
    double e_ey = sqrt(pow(e_TypeA,2)+pow(e_TypeB,2)+pow(e_TypeC,2)+pow(e_TypeD,2));

    double n_fr_TypeA = 100*n_TypeA/n_ee; double e_fr_TypeA = n_fr_TypeA*e_TypeA/n_TypeA;
    double n_fr_TypeB = 100*n_TypeB/n_ee; double e_fr_TypeB = n_fr_TypeB*e_TypeB/n_TypeB;
    double n_fr_TypeC = 100*n_TypeC/n_ee; double e_fr_TypeC = n_fr_TypeC*e_TypeC/n_TypeC;
    double n_fr_TypeD = 100*n_TypeD/n_ee; double e_fr_TypeD = n_fr_TypeD*e_TypeD/n_TypeD;
    double n_fr_ey = 100*n_ey/n_ee; double e_fr_ey = n_fr_ey*e_ey/n_ey;

    cout << fixed << setprecision(2);
    cout << "Type(a) & " << n_TypeA << " $\\pm$ " << e_TypeA << " & " << n_ee <<  " $\\pm$ " << e_ee << " & " << n_fr_TypeA << " $\\pm$ " << e_fr_TypeA << " \\\\ \\hline " << endl; 
    cout << "Type(b) & " << n_TypeB << " $\\pm$ " << e_TypeB << " & " << n_ee <<  " $\\pm$ " << e_ee << " & " << n_fr_TypeB << " $\\pm$ " << e_fr_TypeB << " \\\\ \\hline " << endl; 
    cout << "Type(c) & " << n_TypeC << " $\\pm$ " << e_TypeC << " & " << n_ee <<  " $\\pm$ " << e_ee << " & " << n_fr_TypeC << " $\\pm$ " << e_fr_TypeC << " \\\\ \\hline " << endl; 
    cout << "Type(d) & " << n_TypeD << " $\\pm$ " << e_TypeD << " & " << n_ee <<  " $\\pm$ " << e_ee << " & " << n_fr_TypeD << " $\\pm$ " << e_fr_TypeD << " \\\\ \\hline " << endl; 
    cout << "All & " << n_ey << " $\\pm$ " << e_ey << " & " << n_ee <<  " $\\pm$ " << e_ee << " & " << n_fr_ey << " $\\pm$ " << e_fr_ey << "\\hline " << endl; 

    TH1F* h_ee_Pt = (TH1F*)f_ee->Get("SubLepPtForFR13TeV_13TeV_CutZee_ZjetsElEl_Reco_EF1_zee_Nominal");
    TH1F* h_TypeA_Pt = (TH1F*)f_TypeA->Get("LeadPhPtForFR13TeV_13TeV_CutZeg_PhMatch_EFakeTypeA_ZjetsElEl_Reco_EF1_zeg_Nominal");
    TH1F* h_TypeB_Pt = (TH1F*)f_TypeB->Get("LeadPhPtForFR13TeV_13TeV_CutZeg_PhMatch_EFakeTypeB_ZjetsElEl_Reco_EF1_zeg_Nominal");
    TH1F* h_TypeC_Pt = (TH1F*)f_TypeC->Get("LeadPhPtForFR13TeV_13TeV_CutZeg_PhMatch_EFakeTypeC_ZjetsElEl_Reco_EF1_zeg_Nominal");
    TH1F* h_TypeD_Pt = (TH1F*)f_TypeD->Get("LeadPhPtForFR13TeV_13TeV_CutZeg_PhMatch_EFakeTypeD_ZjetsElEl_Reco_EF1_zeg_Nominal");

    TH1F* h_fr_TypeA_Pt = new TH1F("fr_TypeA_Pt", "fr_TypeA_Pt", h_TypeA_Pt->GetNbinsX(), 0, h_TypeA_Pt->GetNbinsX());
    TH1F* h_fr_TypeB_Pt = new TH1F("fr_TypeB_Pt", "fr_TypeB_Pt", h_TypeB_Pt->GetNbinsX(), 0, h_TypeB_Pt->GetNbinsX());
    TH1F* h_fr_TypeC_Pt = new TH1F("fr_TypeC_Pt", "fr_TypeC_Pt", h_TypeC_Pt->GetNbinsX(), 0, h_TypeC_Pt->GetNbinsX());
    TH1F* h_fr_TypeD_Pt = new TH1F("fr_TypeD_Pt", "fr_TypeD_Pt", h_TypeD_Pt->GetNbinsX(), 0, h_TypeD_Pt->GetNbinsX());
    for (int i = 1; i <= h_fr_TypeA_Pt->GetNbinsX(); i++) {
	double n_tmp_ee = h_ee_Pt->GetBinContent(i);
	double n_tmp_TypeA = h_TypeA_Pt->GetBinContent(i); double e_tmp_TypeA = h_TypeA_Pt->GetBinError(i);
	double n_tmp_TypeB = h_TypeB_Pt->GetBinContent(i); double e_tmp_TypeB = h_TypeB_Pt->GetBinError(i);
	double n_tmp_TypeC = h_TypeC_Pt->GetBinContent(i); double e_tmp_TypeC = h_TypeC_Pt->GetBinError(i);
	double n_tmp_TypeD = h_TypeD_Pt->GetBinContent(i); double e_tmp_TypeD = h_TypeD_Pt->GetBinError(i);
	double n_tmp_fr_TypeA = 0; double e_tmp_fr_TypeA = 0; if (n_tmp_ee != 0) {n_tmp_fr_TypeA = n_tmp_TypeA/n_tmp_ee; e_tmp_fr_TypeA = e_tmp_TypeA/n_tmp_ee;}
	double n_tmp_fr_TypeB = 0; double e_tmp_fr_TypeB = 0; if (n_tmp_ee != 0) {n_tmp_fr_TypeB = n_tmp_TypeB/n_tmp_ee; e_tmp_fr_TypeB = e_tmp_TypeB/n_tmp_ee;}
	double n_tmp_fr_TypeC = 0; double e_tmp_fr_TypeC = 0; if (n_tmp_ee != 0) {n_tmp_fr_TypeC = n_tmp_TypeC/n_tmp_ee; e_tmp_fr_TypeC = e_tmp_TypeC/n_tmp_ee;}
	double n_tmp_fr_TypeD = 0; double e_tmp_fr_TypeD = 0; if (n_tmp_ee != 0) {n_tmp_fr_TypeD = n_tmp_TypeD/n_tmp_ee; e_tmp_fr_TypeD = e_tmp_TypeD/n_tmp_ee;}

	if (i != h_fr_TypeA_Pt->GetNbinsX()) {
	    char tmp[100];
	    //cout << h_TypeA_Pt->GetXaxis()->GetBinLowEdge(i) << " " << h_TypeA_Pt->GetXaxis()->GetBinLowEdge(i)+h_TypeA_Pt->GetXaxis()->GetBinWidth(i) << endl;
	    sprintf(tmp, "[%0.f,%0.f]", h_TypeA_Pt->GetXaxis()->GetBinLowEdge(i), h_TypeA_Pt->GetXaxis()->GetBinLowEdge(i)+h_TypeA_Pt->GetXaxis()->GetBinWidth(i));
	    h_fr_TypeA_Pt->GetXaxis()->SetBinLabel(i, tmp);
	} else {
	    h_fr_TypeA_Pt->GetXaxis()->SetBinLabel(i, "[>70]");
	}

	h_fr_TypeA_Pt->SetBinContent(i, n_tmp_fr_TypeA); h_fr_TypeA_Pt->SetBinError(i, e_tmp_fr_TypeA);
	h_fr_TypeB_Pt->SetBinContent(i, n_tmp_fr_TypeB); h_fr_TypeB_Pt->SetBinError(i, e_tmp_fr_TypeB);
	h_fr_TypeC_Pt->SetBinContent(i, n_tmp_fr_TypeC); h_fr_TypeC_Pt->SetBinError(i, e_tmp_fr_TypeC);
	h_fr_TypeD_Pt->SetBinContent(i, n_tmp_fr_TypeD); h_fr_TypeD_Pt->SetBinError(i, e_tmp_fr_TypeD);
    }

    h_fr_TypeA_Pt->SetLineWidth(3);
    h_fr_TypeB_Pt->SetLineWidth(3);
    h_fr_TypeC_Pt->SetLineWidth(3);
    h_fr_TypeD_Pt->SetLineWidth(3);
    h_fr_TypeA_Pt->GetXaxis()->SetLabelSize(0.06);
    h_fr_TypeA_Pt->GetXaxis()->SetTitleSize(0.05);
    h_fr_TypeA_Pt->GetXaxis()->SetTitleOffset(1.2);
    h_fr_TypeA_Pt->GetYaxis()->SetLabelSize(0.045);
    h_fr_TypeA_Pt->GetYaxis()->SetTitleSize(0.05);
    h_fr_TypeA_Pt->GetYaxis()->SetTitleOffset(1.3);

    TH1F* h_ee_Eta = (TH1F*)f_ee->Get("SubLepAbsEtaForFR13TeV_13TeV_CutZee_ZjetsElEl_Reco_EF1_zee_Nominal");
    TH1F* h_TypeA_Eta = (TH1F*)f_TypeA->Get("LeadPhAbsEtaForFR13TeV_13TeV_CutZeg_PhMatch_EFakeTypeA_ZjetsElEl_Reco_EF1_zeg_Nominal");
    TH1F* h_TypeB_Eta = (TH1F*)f_TypeB->Get("LeadPhAbsEtaForFR13TeV_13TeV_CutZeg_PhMatch_EFakeTypeB_ZjetsElEl_Reco_EF1_zeg_Nominal");
    TH1F* h_TypeC_Eta = (TH1F*)f_TypeC->Get("LeadPhAbsEtaForFR13TeV_13TeV_CutZeg_PhMatch_EFakeTypeC_ZjetsElEl_Reco_EF1_zeg_Nominal");
    TH1F* h_TypeD_Eta = (TH1F*)f_TypeD->Get("LeadPhAbsEtaForFR13TeV_13TeV_CutZeg_PhMatch_EFakeTypeD_ZjetsElEl_Reco_EF1_zeg_Nominal");

    TH1F* h_fr_TypeA_Eta = new TH1F("fr_TypeA_Eta", "fr_TypeA_Eta", h_TypeA_Eta->GetNbinsX(), 0, h_TypeA_Eta->GetNbinsX());
    TH1F* h_fr_TypeB_Eta = new TH1F("fr_TypeB_Eta", "fr_TypeB_Eta", h_TypeB_Eta->GetNbinsX(), 0, h_TypeB_Eta->GetNbinsX());
    TH1F* h_fr_TypeC_Eta = new TH1F("fr_TypeC_Eta", "fr_TypeC_Eta", h_TypeC_Eta->GetNbinsX(), 0, h_TypeC_Eta->GetNbinsX());
    TH1F* h_fr_TypeD_Eta = new TH1F("fr_TypeD_Eta", "fr_TypeD_Eta", h_TypeD_Eta->GetNbinsX(), 0, h_TypeD_Eta->GetNbinsX());
    for (int i = 1; i <= h_fr_TypeA_Eta->GetNbinsX(); i++) {
	double n_tmp_ee = h_ee_Eta->GetBinContent(i);
	double n_tmp_TypeA = h_TypeA_Eta->GetBinContent(i); double e_tmp_TypeA = h_TypeA_Eta->GetBinError(i);
	double n_tmp_TypeB = h_TypeB_Eta->GetBinContent(i); double e_tmp_TypeB = h_TypeB_Eta->GetBinError(i);
	double n_tmp_TypeC = h_TypeC_Eta->GetBinContent(i); double e_tmp_TypeC = h_TypeC_Eta->GetBinError(i);
	double n_tmp_TypeD = h_TypeD_Eta->GetBinContent(i); double e_tmp_TypeD = h_TypeD_Eta->GetBinError(i);
	double n_tmp_fr_TypeA = 0; double e_tmp_fr_TypeA = 0; if (n_tmp_ee != 0) {n_tmp_fr_TypeA = n_tmp_TypeA/n_tmp_ee; e_tmp_fr_TypeA = e_tmp_TypeA/n_tmp_ee;}
	double n_tmp_fr_TypeB = 0; double e_tmp_fr_TypeB = 0; if (n_tmp_ee != 0) {n_tmp_fr_TypeB = n_tmp_TypeB/n_tmp_ee; e_tmp_fr_TypeB = e_tmp_TypeB/n_tmp_ee;}
	double n_tmp_fr_TypeC = 0; double e_tmp_fr_TypeC = 0; if (n_tmp_ee != 0) {n_tmp_fr_TypeC = n_tmp_TypeC/n_tmp_ee; e_tmp_fr_TypeC = e_tmp_TypeC/n_tmp_ee;}
	double n_tmp_fr_TypeD = 0; double e_tmp_fr_TypeD = 0; if (n_tmp_ee != 0) {n_tmp_fr_TypeD = n_tmp_TypeD/n_tmp_ee; e_tmp_fr_TypeD = e_tmp_TypeD/n_tmp_ee;}

	char tmp[100];
	//cout << h_TypeA_Eta->GetXaxis()->GetBinLowEdge(i) << " " << h_TypeA_Eta->GetXaxis()->GetBinLowEdge(i)+h_TypeA_Eta->GetXaxis()->GetBinWidth(i) << endl;
	sprintf(tmp, "[%.1f,%.1f]", h_TypeA_Eta->GetXaxis()->GetBinLowEdge(i), h_TypeA_Eta->GetXaxis()->GetBinLowEdge(i)+h_TypeA_Eta->GetXaxis()->GetBinWidth(i));
	h_fr_TypeA_Eta->GetXaxis()->SetBinLabel(i, tmp);

	h_fr_TypeA_Eta->SetBinContent(i, n_tmp_fr_TypeA); h_fr_TypeA_Eta->SetBinError(i, e_tmp_fr_TypeA);
	h_fr_TypeB_Eta->SetBinContent(i, n_tmp_fr_TypeB); h_fr_TypeB_Eta->SetBinError(i, e_tmp_fr_TypeB);
	h_fr_TypeC_Eta->SetBinContent(i, n_tmp_fr_TypeC); h_fr_TypeC_Eta->SetBinError(i, e_tmp_fr_TypeC);
	h_fr_TypeD_Eta->SetBinContent(i, n_tmp_fr_TypeD); h_fr_TypeD_Eta->SetBinError(i, e_tmp_fr_TypeD);
    }

    h_fr_TypeA_Eta->SetLineWidth(3);
    h_fr_TypeB_Eta->SetLineWidth(3);
    h_fr_TypeC_Eta->SetLineWidth(3);
    h_fr_TypeD_Eta->SetLineWidth(3);
    h_fr_TypeA_Eta->GetXaxis()->SetLabelSize(0.06);
    h_fr_TypeA_Eta->GetXaxis()->SetTitleSize(0.05);
    h_fr_TypeA_Eta->GetXaxis()->SetTitleOffset(1.3);
    h_fr_TypeA_Eta->GetYaxis()->SetLabelSize(0.045);
    h_fr_TypeA_Eta->GetYaxis()->SetTitleSize(0.05);
    h_fr_TypeA_Eta->GetYaxis()->SetTitleOffset(1.3);

    PlotComparor* PC = new PlotComparor();
    PC->SetSaveDir("plots/");
    PC->Is13TeV(true);
    PC->IsSimulation(true);

    PC->ClearAlterHs();
    PC->SetBaseH(h_fr_TypeA_Pt);
    PC->SetBaseHName("(a)mis-reco.");
    PC->AddAlterH(h_fr_TypeB_Pt);
    PC->AddAlterHName("(b)mis-match");
    PC->AddAlterH(h_fr_TypeC_Pt);
    PC->AddAlterHName("(c)prompt QED");
    PC->AddAlterH(h_fr_TypeD_Pt);
    PC->AddAlterHName("(d)non-prompt QED");
    PC->SetChannel("MC Fake Rate");
    PC->SetSaveName("13TeV_1DMCFakeRate_Pt");
    PC->DrawRatio(false);
    PC->SetDrawOption("hist e");
    PC->SetXtitle("Pt [GeV]");
    PC->SetYtitle("F.R.");
    PC->Compare();   

    PC->ClearAlterHs();
    PC->SetBaseH(h_fr_TypeA_Eta);
    PC->SetBaseHName("(a)mis-reco.");
    PC->AddAlterH(h_fr_TypeB_Eta);
    PC->AddAlterHName("(b)mis-match");
    PC->AddAlterH(h_fr_TypeC_Eta);
    PC->AddAlterHName("(c)prompt QED");
    PC->AddAlterH(h_fr_TypeD_Eta);
    PC->AddAlterHName("(d)non-prompt QED");
    PC->SetChannel("MC Fake Rate");
    PC->SetSaveName("13TeV_1DMCFakeRate_Eta");
    PC->DrawRatio(false);
    PC->SetDrawOption("hist e");
    PC->SetXtitle("|#eta|");
    PC->SetYtitle("F.R.");
    PC->SetRightMargin(0.05);
    PC->Compare();   
}
