#include <TH1F.h>
#include <algorithm>
#include <PlotComparor.h>
#include <TFile.h>
#include <iostream>
#include <fstream>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include "StringPlayer.h"
#include <TH2.h>
#include <algorithm>
#include <stdio.h>
#include <stdlib.h>
#include <TKey.h>
#include <TIterator.h>
#include <TH1.h>
#include <TLorentzVector.h>
#include <TStyle.h>
#include <TCanvas.h>
#include <TStopwatch.h>
#include <iostream>
#include <fstream>
#include <stdio.h>
#include <stdlib.h>
#include <TFile.h>

using namespace std;

int main()
{
    PlotComparor* PC = new PlotComparor();
    PC->DrawRatio(true);
    PC->SetSaveDir("plots/");
    PC->SetRatioYtitle("Ratio");    
    PC->ShowOverflow(false);

    TString vars[7] = {"LeadPhPPT", "LeadPhPt", "LeadPhMinDrPhLep", "Njet", "LeadJetPt", "MET", "dPhiPhMET"};
    TString phmatches[2] = {"HFake", "EFake"};
    for (int i = 0; i < 7; i++) 
    {
	for (int j = 0; j < 2; j++) {
	    TString var = vars[i];
    	    TString phmatch = phmatches[j];
    	    TString channel = "ejets";
    	    TFile* f = new TFile("results/Nominal/13TeV_CutSR_PhMatch_"+phmatch+"_TTBarAFNorm_Reco_CR1_"+channel+"_Nominal_Final02.root");
    	    TFile* f_notau = new TFile("results/Nominal/13TeV_CutSR_PhMatch_"+phmatch+"_TTBarAFNorm_Reco_CR1_"+channel+"_TTNoTau_Nominal_Final02.root");
    	    TFile* f_var = new TFile("results/Nominal/13TeV_CutSR_PhMatch_"+phmatch+"_TTBarAFPS_Reco_CR1_"+channel+"_Nominal_Final02.root");
    	    TFile* f_var_notau = new TFile("results/Nominal/13TeV_CutSR_PhMatch_"+phmatch+"_TTBarAFPS_Reco_CR1_"+channel+"_TTNoTau_Nominal_Final02.root");
    	    if (!f) cout << "can't open f" << endl;
    	    if (!f_notau) cout << "can't open f notau" << endl;
    	    if (!f_var) cout << "can't open f var" << endl;
    	    if (!f_var_notau) cout << "can't open f var notau" << endl;

    	    TH1F* h = (TH1F*)f->Get(var+"_13TeV_CutSR_PhMatch_"+phmatch+"_TTBarAFNorm_Reco_CR1_"+channel+"_Nominal");
    	    TH1F* h_notau = (TH1F*)f_notau->Get(var+"_13TeV_CutSR_PhMatch_"+phmatch+"_TTBarAFNorm_Reco_CR1_"+channel+"_TTNoTau_Nominal");
    	    TH1F* h_var = (TH1F*)f_var->Get(var+"_13TeV_CutSR_PhMatch_"+phmatch+"_TTBarAFPS_Reco_CR1_"+channel+"_Nominal");
    	    TH1F* h_var_notau = (TH1F*)f_var_notau->Get(var+"_13TeV_CutSR_PhMatch_"+phmatch+"_TTBarAFPS_Reco_CR1_"+channel+"_TTNoTau_Nominal");

    	    TH1F* h_sys = (TH1F*)h->Clone();
    	    h_sys->Divide(h_var);
    	    TH1F* h_sys_notau = (TH1F*)h_notau->Clone();
    	    h_sys_notau->Divide(h_var_notau);

    	    PC->SetChannel(channel.Data());

    	    PC->ClearAlterHs();
    	    PC->SetBaseH(h_sys);
    	    PC->SetBaseHName("w tau");
    	    PC->AddAlterH(h_sys_notau);
    	    PC->AddAlterHName("w/o tau");
    	    PC->SetXtitle(var.Data());
    	    PC->SetYtitle("Nevt");
    	    PC->SetMinRatio(0.5);
    	    PC->SetMaxRatio(1.5);
    	    PC->UseLogy(false);
    	    TString savename = "H7bug_" + var + "_" + channel + "_" + phmatch;
    	    PC->SetSaveName(savename.Data());
    	    PC->SetLogo("H7 issue");
    	    PC->Compare();
	}
    }
}
