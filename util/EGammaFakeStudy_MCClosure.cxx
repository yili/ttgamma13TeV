#include <TFile.h>
#include <TH1F.h>
#include <iostream>
#include "PlotComparor.h"

using namespace std;

void MCClosure(bool reverse, bool ispt);

int main() {
    MCClosure(false, true);
    MCClosure(true, true);
    MCClosure(false, false);
    MCClosure(true, false);
}

void MCClosure(bool reverse, bool ispt)
{
    PlotComparor* PC = new PlotComparor();

    TString rtag = "";
    if (reverse) rtag = "Reverse";
    TString vtag = "";
    if (ispt) vtag= "PtCoarse";
    else vtag = "EtaCoarse";

    TFile *f_zeg_TypeA = new TFile("results/Results_ZjetsElEl_Reco_FULL_13TeV_EF1_zeg_CutZeg"+rtag+"_PhMatch_EFakeTypeA_Lumiweighted_V009.01.root");
    TH1F* h_zeg_TypeA = (TH1F*)f_zeg_TypeA->Get("LeadPh"+vtag+"_EF1_zeg_Reco_ZjetsElEl_Nominal");
    TFile *f_zeg_TypeB = new TFile("results/Results_ZjetsElEl_Reco_FULL_13TeV_EF1_zeg_CutZeg"+rtag+"_PhMatch_EFakeTypeB_Lumiweighted_V009.01.root");
    TH1F* h_zeg_TypeB = (TH1F*)f_zeg_TypeB->Get("LeadPh"+vtag+"_EF1_zeg_Reco_ZjetsElEl_Nominal");
    TFile *f_zeg_TypeC = new TFile("results/Results_ZjetsElEl_Reco_FULL_13TeV_EF1_zeg_CutZeg"+rtag+"_PhMatch_EFakeTypeC_Lumiweighted_V009.01.root");
    TH1F* h_zeg_TypeC = (TH1F*)f_zeg_TypeC->Get("LeadPh"+vtag+"_EF1_zeg_Reco_ZjetsElEl_Nominal");
    TFile *f_zeg_TypeD = new TFile("results/Results_ZjetsElEl_Reco_FULL_13TeV_EF1_zeg_CutZeg"+rtag+"_PhMatch_EFakeTypeD_Lumiweighted_V009.01.root");
    TH1F* h_zeg_TypeD = (TH1F*)f_zeg_TypeD->Get("LeadPh"+vtag+"_EF1_zeg_Reco_ZjetsElEl_Nominal");
    TFile *f_zee = new TFile("results/Results_ZjetsElEl_Reco_FULL_13TeV_EF1_zee_CutZee"+rtag+"_Lumiweighted_V009.01.root");
    TH1F* h_zee;
    if (!reverse) h_zee = (TH1F*)f_zee->Get("SubLep"+vtag+"_EF1_zee_Reco_ZjetsElEl_Nominal");
    else h_zee = (TH1F*)f_zee->Get("LeadLep"+vtag+"_EF1_zee_Reco_ZjetsElEl_Nominal");

    TFile* f_tteg_TypeA = new TFile("results/Results_TTBar_Reco_FULL_13TeV_CR1_ejets_CutTTeg"+rtag+"_TTBarEE_PhMatch_EFake2TypeA_Lumiweighted_V009.01.root");
    TH1F* h_tteg_TypeA = (TH1F*)f_tteg_TypeA->Get("LeadPh"+vtag+"_CR1_ejets_Reco_TTBar_Nominal");
    TFile* f_tteg_TypeB = new TFile("results/Results_TTBar_Reco_FULL_13TeV_CR1_ejets_CutTTeg"+rtag+"_TTBarEE_PhMatch_EFake2TypeB_Lumiweighted_V009.01.root");
    TH1F* h_tteg_TypeB = (TH1F*)f_tteg_TypeB->Get("LeadPh"+vtag+"_CR1_ejets_Reco_TTBar_Nominal");
    TFile* f_tteg_TypeC = new TFile("results/Results_TTBar_Reco_FULL_13TeV_CR1_ejets_CutTTeg"+rtag+"_TTBarEE_PhMatch_EFake2TypeC_Lumiweighted_V009.01.root");
    TH1F* h_tteg_TypeC = (TH1F*)f_tteg_TypeC->Get("LeadPh"+vtag+"_CR1_ejets_Reco_TTBar_Nominal");
    TFile* f_tteg_TypeD = new TFile("results/Results_TTBar_Reco_FULL_13TeV_CR1_ejets_CutTTeg"+rtag+"_TTBarEE_PhMatch_EFake2TypeD_Lumiweighted_V009.01.root");
    TH1F* h_tteg_TypeD = (TH1F*)f_tteg_TypeD->Get("LeadPh"+vtag+"_CR1_ejets_Reco_TTBar_Nominal");
    TFile *f_ttee = new TFile("results/Results_TTBar_Reco_FULL_13TeV_EF1_ttel_ee_CutTTee"+rtag+"_TTBarEE_Lumiweighted_V009.01.root");
    TH1F* h_ttee;
    if (!reverse) h_ttee = (TH1F*)f_ttee->Get("SubLep"+vtag+"_EF1_ttel_ee_Reco_TTBar_Nominal");
    else h_ttee = (TH1F*)f_ttee->Get("LeadLep"+vtag+"_EF1_ttel_ee_Reco_TTBar_Nominal");

    TFile* f_ttmg_TypeA = new TFile("results/Results_TTBar_Reco_FULL_13TeV_CR1_mujets_CutTTmug"+rtag+"_TTBarEM_PhMatch_EFake2TypeA_Lumiweighted_V009.01.root");
    TH1F* h_ttmg_TypeA = (TH1F*)f_ttmg_TypeA->Get("LeadPh"+vtag+"_CR1_mujets_Reco_TTBar_Nominal");
    TFile* f_ttmg_TypeB = new TFile("results/Results_TTBar_Reco_FULL_13TeV_CR1_mujets_CutTTmug"+rtag+"_TTBarEM_PhMatch_EFake2TypeB_Lumiweighted_V009.01.root");
    TH1F* h_ttmg_TypeB = (TH1F*)f_ttmg_TypeB->Get("LeadPh"+vtag+"_CR1_mujets_Reco_TTBar_Nominal");
    TFile* f_ttmg_TypeC = new TFile("results/Results_TTBar_Reco_FULL_13TeV_CR1_mujets_CutTTmug"+rtag+"_TTBarEM_PhMatch_EFake2TypeC_Lumiweighted_V009.01.root");
    TH1F* h_ttmg_TypeC = (TH1F*)f_ttmg_TypeC->Get("LeadPh"+vtag+"_CR1_mujets_Reco_TTBar_Nominal");
    TFile* f_ttmg_TypeD = new TFile("results/Results_TTBar_Reco_FULL_13TeV_CR1_mujets_CutTTmug"+rtag+"_TTBarEM_PhMatch_EFake2TypeD_Lumiweighted_V009.01.root");
    TH1F* h_ttmg_TypeD = (TH1F*)f_ttmg_TypeD->Get("LeadPh"+vtag+"_CR1_mujets_Reco_TTBar_Nominal");
    TFile *f_ttme = new TFile("results/Results_TTBar_Reco_FULL_13TeV_EF1_ttel_mue_CutTTmue"+rtag+"_TTBarEM_Lumiweighted_V009.01.root");
    TH1F* h_ttme;
    if (!reverse) h_ttme = (TH1F*)f_ttme->Get("SubLep"+vtag+"_EF1_ttel_mue_Reco_TTBar_Nominal");
    else h_ttme = (TH1F*)f_ttme->Get("LeadLep"+vtag+"_EF1_ttel_mue_Reco_TTBar_Nominal");

    cout << "h_zeg_TypeA: " << h_zeg_TypeA << endl;
    cout << "h_zeg_TypeB: " << h_zeg_TypeB << endl;
    cout << "h_zeg_TypeC: " << h_zeg_TypeC << endl;
    cout << "h_zeg_TypeD: " << h_zeg_TypeD << endl;
    cout << "h_zee: " << h_zee << endl;
    cout << "h_tteg_TypeA: " << h_tteg_TypeA << endl;
    cout << "h_tteg_TypeB: " << h_tteg_TypeB << endl;
    cout << "h_tteg_TypeC: " << h_tteg_TypeC << endl;
    cout << "h_tteg_TypeD: " << h_tteg_TypeD << endl;
    cout << "h_ttee: " << h_ttee << endl;
    cout << "h_ttmg_TypeA: " << h_ttmg_TypeA << endl;
    cout << "h_ttmg_TypeB: " << h_ttmg_TypeB << endl;
    cout << "h_ttmg_TypeC: " << h_ttmg_TypeC << endl;
    cout << "h_ttmg_TypeD: " << h_ttmg_TypeD << endl;
    cout << "h_ttme: " << h_ttme << endl;

    double n_zeg_TypeA = h_zeg_TypeA->Integral(0, h_zeg_TypeA->GetNbinsX()+1);
    double n_zeg_TypeB = h_zeg_TypeB->Integral(0, h_zeg_TypeB->GetNbinsX()+1);
    double n_zeg_TypeC = h_zeg_TypeC->Integral(0, h_zeg_TypeC->GetNbinsX()+1);
    double n_zeg_TypeD = h_zeg_TypeD->Integral(0, h_zeg_TypeD->GetNbinsX()+1);
    double n_zee = h_zee->Integral(0, h_zee->GetNbinsX()+1);
    double fr_zee_TypeA = n_zeg_TypeA/n_zee;
    double fr_zee_TypeB = n_zeg_TypeB/n_zee;
    double fr_zee_TypeC = n_zeg_TypeC/n_zee;
    double fr_zee_TypeD = n_zeg_TypeD/n_zee;

    double n_tteg_TypeA = h_tteg_TypeA->Integral(0, h_tteg_TypeA->GetNbinsX()+1);
    double n_tteg_TypeB = h_tteg_TypeB->Integral(0, h_tteg_TypeB->GetNbinsX()+1);
    double n_tteg_TypeC = h_tteg_TypeC->Integral(0, h_tteg_TypeC->GetNbinsX()+1);
    double n_tteg_TypeD = h_tteg_TypeD->Integral(0, h_tteg_TypeD->GetNbinsX()+1);
    double n_ttee = h_ttee->Integral(0, h_ttee->GetNbinsX()+1);
    double fr_ttee_TypeA = n_tteg_TypeA/n_ttee;
    double fr_ttee_TypeB = n_tteg_TypeB/n_ttee;
    double fr_ttee_TypeC = n_tteg_TypeC/n_ttee;
    double fr_ttee_TypeD = n_tteg_TypeD/n_ttee;

    double n_ttmg_TypeA = h_ttmg_TypeA->Integral(0, h_ttmg_TypeA->GetNbinsX()+1);
    double n_ttmg_TypeB = h_ttmg_TypeB->Integral(0, h_ttmg_TypeB->GetNbinsX()+1);
    double n_ttmg_TypeC = h_ttmg_TypeC->Integral(0, h_ttmg_TypeC->GetNbinsX()+1);
    double n_ttmg_TypeD = h_ttmg_TypeD->Integral(0, h_ttmg_TypeD->GetNbinsX()+1);
    double n_ttme = h_ttme->Integral(0, h_ttme->GetNbinsX()+1);
    double fr_ttme_TypeA = n_ttmg_TypeA/n_ttme;
    double fr_ttme_TypeB = n_ttmg_TypeB/n_ttme;
    double fr_ttme_TypeC = n_ttmg_TypeC/n_ttme;
    double fr_ttme_TypeD = n_ttmg_TypeD/n_ttme;

    cout << "FR " << rtag << " " << vtag << endl;
    cout << "Zee vs TTee vs TTme" << endl;
    cout << fr_zee_TypeA << " " << fr_ttee_TypeA << " " << fr_ttme_TypeA << endl;
    cout << fr_zee_TypeB << " " << fr_ttee_TypeB << " " << fr_ttme_TypeB << endl;
    cout << fr_zee_TypeC << " " << fr_ttee_TypeC << " " << fr_ttme_TypeC << endl;
    cout << fr_zee_TypeD << " " << fr_ttee_TypeD << " " << fr_ttme_TypeD << endl;

    h_zeg_TypeA->Divide(h_zee);
    h_zeg_TypeB->Divide(h_zee);
    h_zeg_TypeC->Divide(h_zee);
    h_zeg_TypeD->Divide(h_zee);
    h_tteg_TypeA->Divide(h_ttee);
    h_tteg_TypeB->Divide(h_ttee);
    h_tteg_TypeC->Divide(h_ttee);
    h_tteg_TypeD->Divide(h_ttee);
    h_ttmg_TypeA->Divide(h_ttme);
    h_ttmg_TypeB->Divide(h_ttme);
    h_ttmg_TypeC->Divide(h_ttme);
    h_ttmg_TypeD->Divide(h_ttme);

    TH1F* h_zeg_TypeABD = (TH1F*)h_zeg_TypeA->Clone();
    h_zeg_TypeABD->Add(h_zeg_TypeB);
    h_zeg_TypeABD->Add(h_zeg_TypeD);
    TH1F* h_tteg_TypeABD = (TH1F*)h_tteg_TypeA->Clone();
    h_tteg_TypeABD->Add(h_tteg_TypeB);
    h_tteg_TypeABD->Add(h_tteg_TypeD);
    TH1F* h_ttmg_TypeABD = (TH1F*)h_ttmg_TypeA->Clone();
    h_ttmg_TypeABD->Add(h_ttmg_TypeB);
    h_ttmg_TypeABD->Add(h_ttmg_TypeD);

    PC->SetSaveDir("plots/");
    PC->Is13TeV(true);
    //PC->DrawRatio(true);
    //PC->NormToUnit(true);
    PC->HasData(false);

    PC->ClearAlterHs();
    PC->SetBaseH(h_zeg_TypeA);
    PC->SetBaseHName("Zee");
    PC->AddAlterH(h_tteg_TypeA);
    PC->AddAlterHName("TTee");
    PC->AddAlterH(h_ttmg_TypeA);
    PC->AddAlterHName("TTme");
    PC->SetChannel("Type (a)");
    PC->SetSaveName(("Compare_EGammaFR_MC_TypeA_"+vtag+"_"+rtag).Data());
    if (ispt) PC->SetRangeY(0, 0.035);
    else PC->SetRangeY(0, 0.075);
    PC->Compare();
    PC->ClearAlterHs();
    PC->SetBaseH(h_zeg_TypeB);
    PC->SetBaseHName("Zee");
    PC->AddAlterH(h_tteg_TypeB);
    PC->AddAlterHName("TTee");
    PC->AddAlterH(h_ttmg_TypeB);
    PC->AddAlterHName("TTme");
    PC->SetChannel("Type (b)");
    PC->SetSaveName(("Compare_EGammaFR_MC_TypeB_"+vtag+"_"+rtag).Data());
    if (ispt) PC->SetRangeY(0, 0.020);
    else PC->SetRangeY(0, 0.045);
    PC->Compare();
    PC->ClearAlterHs();
    PC->SetBaseH(h_zeg_TypeC);
    PC->SetBaseHName("Zee");
    PC->AddAlterH(h_tteg_TypeC);
    PC->AddAlterHName("TTee");
    PC->AddAlterH(h_ttmg_TypeC);
    PC->AddAlterHName("TTme");
    PC->SetChannel("Type (c)");
    PC->SetSaveName(("Compare_EGammaFR_MC_TypeC_"+vtag+"_"+rtag).Data());
    if (ispt) PC->SetRangeY(0, 0.025);
    else PC->SetRangeY(0, 0.025);
    PC->Compare();
    PC->ClearAlterHs();
    PC->SetBaseH(h_zeg_TypeD);
    PC->SetBaseHName("Zee");
    PC->AddAlterH(h_tteg_TypeD);
    PC->AddAlterHName("TTee");
    PC->AddAlterH(h_ttmg_TypeD);
    PC->AddAlterHName("TTme");
    PC->SetChannel("Type (d)");
    PC->SetSaveName(("Compare_EGammaFR_MC_TypeD_"+vtag+"_"+rtag).Data());
    if (ispt) PC->SetRangeY(0, 0.007);
    else PC->SetRangeY(0, 0.0045);
    PC->Compare();
    PC->ClearAlterHs();
    PC->SetBaseH(h_zeg_TypeABD);
    PC->SetBaseHName("Zee");
    PC->AddAlterH(h_tteg_TypeABD);
    PC->AddAlterHName("TTee");
    PC->AddAlterH(h_ttmg_TypeABD);
    PC->AddAlterHName("TTme");
    PC->SetChannel("Type (a+b+d))");
    PC->SetSaveName(("Compare_EGammaFR_MC_TypeABD_"+vtag+"_"+rtag).Data());
    if (ispt) PC->SetRangeY(0, 0.07);
    else PC->SetRangeY(0, 0.15);
    PC->DrawRatio(true);
    PC->SetMaxRatio(1.5);
    PC->SetMinRatio(0.5);
    PC->Compare();
}
