#include <TFile.h>
#include <TH2.h>
#include <iostream>
#include <iomanip>
#include <TString.h>

using namespace std;

TH2F* convertTH2(TH2F* hin) {
    Double_t bins_eta[6] = {0, 0.5, 1, 1.52, 2, 2.37};
    Double_t bins_pt[5] = {20, 35, 45, 60, 9999};
    TString newname = "Final_";
    newname += hin->GetName();
    TH2F* hout = new TH2F(newname, newname, 5, bins_eta, 4, bins_pt);
    int etacnt = 0;
    for (int ieta = 1; ieta <= hin->GetNbinsX(); ieta++) {
	if (ieta == 4) continue;
	etacnt++;
	for (int ipt = 1; ipt <= hin->GetNbinsY(); ipt++) {
	    hout->SetBinContent(etacnt, ipt, hin->GetBinContent(ieta, ipt));
	    hout->SetBinError(etacnt, ipt, hin->GetBinError(ieta, ipt));
	}
    }
    return hout;
}

int main()
{
    TFile* f_nom = new TFile("results/Nominal/EFakeSFs.root");
    TFile* f_RDown = new TFile("results/Nominal/EFakeSFs_RDown.root");
    TFile* f_Gaus = new TFile("results/Nominal/EFakeSFs_Gaus.root");
    TFile* f_MCTemp_Auto = new TFile("results/Nominal/EFakeSFs_MCTemp_Auto.root");
    TFile* f_Zy = new TFile("results/Nominal/EFakeSFs_Zy.root");

    double sf_nom = ((TH1F*)f_nom->Get("Overall_SF"))->GetBinContent(1);
    double sf_nom_err = ((TH1F*)f_nom->Get("Overall_SF"))->GetBinError(1);
    double sf_RDown = ((TH1F*)f_RDown->Get("Overall_SF"))->GetBinContent(1);
    double sf_RDown_err = ((TH1F*)f_RDown->Get("Overall_SF"))->GetBinError(1);
    double sf_Gaus = ((TH1F*)f_Gaus->Get("Overall_SF"))->GetBinContent(1);
    double sf_Gaus_err = ((TH1F*)f_Gaus->Get("Overall_SF"))->GetBinError(1);
    double sf_MCTemp_Auto = ((TH1F*)f_MCTemp_Auto->Get("Overall_SF"))->GetBinContent(1);
    double sf_MCTemp_Auto_err = ((TH1F*)f_MCTemp_Auto->Get("Overall_SF"))->GetBinError(1);
    double sf_Zy = ((TH1F*)f_Zy->Get("Overall_SF"))->GetBinContent(1);
    double sf_Zy_err = ((TH1F*)f_Zy->Get("Overall_SF"))->GetBinError(1);

    double sys_RDown = (sf_RDown - sf_nom)/sf_nom;
    double sys_Gaus = (sf_Gaus - sf_nom)/sf_nom;
    double sys_MCTemp_Auto = (sf_MCTemp_Auto - sf_nom)/sf_nom;
    double sys_Zy = (sf_Zy - sf_nom)/sf_nom;
    double sys_tot = sqrt(pow(sys_RDown,2) + pow(sys_Gaus,2) + pow(sys_MCTemp_Auto,2) + pow(sys_Zy,2));
    double sys_stat = sf_nom_err/sf_nom;
    double sys = sqrt(pow(sys_tot,2) + pow(sys_stat,2));

    cout << fixed << setprecision(2) << endl;
    cout << "\\scalebox{0.8}{" << endl;
    cout << "\\begin{tabular}{c|c|c|c|c|c}" << endl;
    cout << "\\hline" << endl;
    cout << "\\hline" << endl;
    cout << setw(10) << "Source" << "&" << setw(10) << "Stat." << "&" << setw(10) << "Range" << "&" << setw(10) << "Bkg Shape" << "&" << setw(10) << "Sig Shape" << "&" << setw(10) << "Zy Sub." << "\\\\" << "\\hline" << endl;
    cout << setw(10) << sf_nom << "&" << setw(10) << sf_nom*sf_nom_err << "&" << setw(10) << sf_nom*sys_RDown << "&" << setw(10) << sf_nom*sys_Gaus << "&" << setw(10) << sf_nom*sys_MCTemp_Auto << "&" << setw(10) << sf_nom*sys_Zy << "\\\\" << "\\hline" << endl;
    cout << setw(10) << "(in \\%)" << "&" << setw(10) << sys_stat*100 << "&" << setw(10) << sys_RDown*100 << "&" << setw(10) << sys_Gaus*100 << "&" << setw(10) << sys_MCTemp_Auto*100 << "&" << setw(10) << sys_Zy*100 << "\\\\" << "\\hline" << endl;
    cout << setw(10) << "Total" << "& \\multicolumn{5}{c}{" << sf_nom*sys << " (" << sys << ")}" << "\\\\" << "\\hline" << endl;
    cout << "\\hline" << endl;
    cout << "\\end{tabular}}" << endl;

    TH2F* h_nom = (TH2F*)f_nom->Get("FR_SF");
    TH2F* h_RDown = (TH2F*)f_RDown->Get("FR_SF");
    TH2F* h_Gaus = (TH2F*)f_Gaus->Get("FR_SF");
    TH2F* h_MCTemp_Auto = (TH2F*)f_MCTemp_Auto->Get("FR_SF");
    TH2F* h_Zy = (TH2F*)f_Zy->Get("FR_SF");
    TH2F* h_nom_final = (TH2F*)h_nom->Clone();

    for (int i = 1; i <= h_nom->GetNbinsX(); i++) {
	if (i == 4) continue;
	for (int j = 1; j <= h_nom->GetNbinsY(); j++) {
	    double sf_nom = h_nom->GetBinContent(i,j);
	    double sf_nom_err = h_nom->GetBinError(i,j);
	    double sf_RDown = h_RDown->GetBinContent(i,j);
    	    double sf_RDown_err = h_RDown->GetBinError(i,j);
    	    double sf_Gaus = h_Gaus->GetBinContent(i,j);
    	    double sf_Gaus_err = h_Gaus->GetBinError(i,j);
    	    double sf_MCTemp_Auto = h_MCTemp_Auto->GetBinContent(i,j);
    	    double sf_MCTemp_Auto_err = h_MCTemp_Auto->GetBinError(i,j);
    	    double sf_Zy = h_Zy->GetBinContent(i,j);
    	    double sf_Zy_err = h_Zy->GetBinError(i,j);

    	    double sys_RDown = (sf_RDown - sf_nom)/sf_nom;
    	    double sys_Gaus = (sf_Gaus - sf_nom)/sf_nom;
    	    double sys_MCTemp_Auto = (sf_MCTemp_Auto - sf_nom)/sf_nom;
    	    double sys_Zy = (sf_Zy - sf_nom)/sf_nom;
    	    double sys_tot = sqrt(pow(sys_RDown,2) + pow(sys_Gaus,2) + pow(sys_MCTemp_Auto,2) + pow(sys_Zy,2));
    	    double sys_stat = sf_nom_err/sf_nom;
    	    double sys = sqrt(pow(sys_tot,2) + pow(sys_stat,2));

	    h_nom_final->SetBinError(i,j,sf_nom*sys);
	}
    }

    TH2F* h_nom_stat_pt2_eta4 = (TH2F*)h_nom->Clone();
    h_nom_stat_pt2_eta4->SetBinContent(5,2,h_nom_stat_pt2_eta4->GetBinContent(5,2)+h_nom_stat_pt2_eta4->GetBinError(5,2));
    TH2F* h_nom_stat_pt3_eta3 = (TH2F*)h_nom->Clone();
    h_nom_stat_pt3_eta3->SetBinContent(3,3,h_nom_stat_pt3_eta3->GetBinContent(3,3)+h_nom_stat_pt3_eta3->GetBinError(3,3));
    TH2F* h_nom_stat_pt4_eta1 = (TH2F*)h_nom->Clone();
    h_nom_stat_pt4_eta1->SetBinContent(1,4,h_nom_stat_pt4_eta1->GetBinContent(1,4)+h_nom_stat_pt4_eta1->GetBinError(1,4));
    TH2F* h_nom_stat_pt4_eta2 = (TH2F*)h_nom->Clone();
    h_nom_stat_pt4_eta2->SetBinContent(2,4,h_nom_stat_pt4_eta2->GetBinContent(2,4)+h_nom_stat_pt4_eta2->GetBinError(2,4));
    TH2F* h_nom_stat_pt4_eta3 = (TH2F*)h_nom->Clone();
    h_nom_stat_pt4_eta3->SetBinContent(3,4,h_nom_stat_pt4_eta3->GetBinContent(3,4)+h_nom_stat_pt4_eta3->GetBinError(3,4));
    TH2F* h_nom_stat_pt4_eta5 = (TH2F*)h_nom->Clone();
    h_nom_stat_pt4_eta5->SetBinContent(6,4,h_nom_stat_pt4_eta5->GetBinContent(6,4)+h_nom_stat_pt4_eta5->GetBinError(6,4));

    h_nom->SetName("EFake_SF_Nominal");
    h_RDown->SetName("EFake_SF_RangeDown");
    h_Gaus->SetName("EFake_SF_BkgFunc");
    h_MCTemp_Auto->SetName("EFake_SF_SigFunc");
    h_Zy->SetName("EFake_SF_TypeC");
    h_nom_stat_pt2_eta4->SetName("EFake_SF_Stat_Pt2Eta4");
    h_nom_stat_pt3_eta3->SetName("EFake_SF_Stat_Pt3Eta3");
    h_nom_stat_pt4_eta1->SetName("EFake_SF_Stat_Pt4Eta1");
    h_nom_stat_pt4_eta2->SetName("EFake_SF_Stat_Pt4Eta2");
    h_nom_stat_pt4_eta3->SetName("EFake_SF_Stat_Pt4Eta3");
    h_nom_stat_pt4_eta5->SetName("EFake_SF_Stat_Pt4Eta5");
    h_nom_final->SetName("EFake_SF_Total");

    TH2F* h_new_nom = convertTH2(h_nom);
    TH2F* h_new_RDown = convertTH2(h_RDown);
    TH2F* h_new_Gaus = convertTH2(h_Gaus);
    TH2F* h_new_MCTemp_Auto = convertTH2(h_MCTemp_Auto);
    TH2F* h_new_Zy = convertTH2(h_Zy);
    TH2F* h_new_nom_stat_pt2_eta4 = convertTH2(h_nom_stat_pt2_eta4);
    TH2F* h_new_nom_stat_pt3_eta3  = convertTH2(h_nom_stat_pt3_eta3);
    TH2F* h_new_nom_stat_pt4_eta1  = convertTH2(h_nom_stat_pt4_eta1);
    TH2F* h_new_nom_stat_pt4_eta2  = convertTH2(h_nom_stat_pt4_eta2);
    TH2F* h_new_nom_stat_pt4_eta3  = convertTH2(h_nom_stat_pt4_eta3);
    TH2F* h_new_nom_stat_pt4_eta5  = convertTH2(h_nom_stat_pt4_eta5);
    TH2F* h_new_nom_final  = convertTH2(h_nom_final);

    TFile* f_nom_final = new TFile("results/Nominal/EFakeSFs_Final.root","recreate");
    h_nom->Write();
    h_RDown->Write();
    h_Gaus->Write();
    h_MCTemp_Auto->Write();
    h_Zy->Write();
    h_nom_stat_pt2_eta4->Write();
    h_nom_stat_pt3_eta3->Write();
    h_nom_stat_pt4_eta1->Write();
    h_nom_stat_pt4_eta2->Write();
    h_nom_stat_pt4_eta3->Write();
    h_nom_stat_pt4_eta5->Write();
    h_nom_final->Write();
    h_new_nom->Write();
    h_new_RDown->Write();
    h_new_Gaus->Write();
    h_new_MCTemp_Auto->Write();
    h_new_Zy->Write();
    h_new_nom_stat_pt2_eta4->Write();
    h_new_nom_stat_pt3_eta3->Write();
    h_new_nom_stat_pt4_eta1->Write();
    h_new_nom_stat_pt4_eta2->Write();
    h_new_nom_stat_pt4_eta3->Write();
    h_new_nom_stat_pt4_eta5->Write();
    h_new_nom_final->Write();
    f_nom_final->Close();
    delete f_nom_final;
}
