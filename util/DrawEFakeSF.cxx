#include <TCanvas.h>
#include <TStyle.h>
#include <TH1F.h>
#include <TH2.h>
#include <TFile.h>

#include <iostream>
#include <iomanip>

#include "PlotComparor.h"
#include "AtlasStyle.h"

using namespace std;

vector<TH1F*> DrawMap(TH2F*h, string tag = "");

int main(int argc, char* argv[]) 
{
    SetAtlasStyle();

    TFile* f_nom = new TFile("results/Nominal/EFakeSFs.root");
    TFile* f_RDown = new TFile("results/Nominal/EFakeSFs_RDown.root");
    TFile* f_Gaus = new TFile("results/Nominal/EFakeSFs_Gaus.root");
    TFile* f_MCTemp_Auto = new TFile("results/Nominal/EFakeSFs_MCTemp_Auto.root");
    TFile* f_Zy = new TFile("results/Nominal/EFakeSFs_Zy.root");

    double sf_nom = ((TH1F*)f_nom->Get("Overall_SF"))->GetBinContent(1);
    double sf_nom_err = ((TH1F*)f_nom->Get("Overall_SF"))->GetBinError(1);
    double sf_RDown = ((TH1F*)f_RDown->Get("Overall_SF"))->GetBinContent(1);
    double sf_RDown_err = ((TH1F*)f_RDown->Get("Overall_SF"))->GetBinError(1);
    double sf_Gaus = ((TH1F*)f_Gaus->Get("Overall_SF"))->GetBinContent(1);
    double sf_Gaus_err = ((TH1F*)f_Gaus->Get("Overall_SF"))->GetBinError(1);
    double sf_MCTemp_Auto = ((TH1F*)f_MCTemp_Auto->Get("Overall_SF"))->GetBinContent(1);
    double sf_MCTemp_Auto_err = ((TH1F*)f_MCTemp_Auto->Get("Overall_SF"))->GetBinError(1);
    double sf_Zy = ((TH1F*)f_Zy->Get("Overall_SF"))->GetBinContent(1);
    double sf_Zy_err = ((TH1F*)f_Zy->Get("Overall_SF"))->GetBinError(1);

    double sys_RDown = (sf_RDown - sf_nom)/sf_nom;
    double sys_Gaus = (sf_Gaus - sf_nom)/sf_nom;
    double sys_MCTemp_Auto = (sf_MCTemp_Auto - sf_nom)/sf_nom;
    double sys_Zy = (sf_Zy - sf_nom)/sf_nom;
    double sys_tot = sqrt(pow(sys_RDown,2) + pow(sys_Gaus,2) + pow(sys_MCTemp_Auto,2) + pow(sys_Zy,2));
    double sys_stat = sf_nom_err/sf_nom;
    double sys = sqrt(pow(sys_tot,2) + pow(sys_stat,2));

    cout << fixed << setprecision(2) << endl;
    cout << "\\scalebox{0.8}{" << endl;
    cout << "\\begin{tabular}{c|c|c|c|c|c}" << endl;
    cout << "\\hline" << endl;
    cout << "\\hline" << endl;
    cout << setw(10) << "Source" << "&" << setw(10) << "Stat." << "&" << setw(10) << "Range" << "&" << setw(10) << "Bkg Shape" << "&" << setw(10) << "Sig Shape" << "&" << setw(10) << "Zy Sub." << "\\\\" << "\\hline" << endl;
    cout << setw(10) << sf_nom << "&" << setw(10) << sf_nom*sf_nom_err << "&" << setw(10) << sf_nom*sys_RDown << "&" << setw(10) << sf_nom*sys_Gaus << "&" << setw(10) << sf_nom*sys_MCTemp_Auto << "&" << setw(10) << sf_nom*sys_Zy << "\\\\" << "\\hline" << endl;
    cout << setw(10) << "(in \\%)" << "&" << setw(10) << sys_stat*100 << "&" << setw(10) << sys_RDown*100 << "&" << setw(10) << sys_Gaus*100 << "&" << setw(10) << sys_MCTemp_Auto*100 << "&" << setw(10) << sys_Zy*100 << "\\\\" << "\\hline" << endl;
    cout << setw(10) << "Total" << "& \\multicolumn{5}{c}{" << sf_nom*sys << " (" << sys << ")}" << "\\\\" << "\\hline" << endl;
    cout << "\\hline" << endl;
    cout << "\\end{tabular}}" << endl;

    TH2F* h_nom = (TH2F*)f_nom->Get("FR_SF");
    TH2F* h_RDown = (TH2F*)f_RDown->Get("FR_SF");
    TH2F* h_Gaus = (TH2F*)f_Gaus->Get("FR_SF");
    TH2F* h_MCTemp_Auto = (TH2F*)f_MCTemp_Auto->Get("FR_SF");
    TH2F* h_Zy = (TH2F*)f_Zy->Get("FR_SF");
    TH2F* h_nom_final = (TH2F*)h_nom->Clone();

    for (int i = 1; i <= h_nom->GetNbinsX(); i++) {
	if (i == 4) continue;
	for (int j = 1; j <= h_nom->GetNbinsY(); j++) {
	    double sf_nom = h_nom->GetBinContent(i,j);
	    double sf_nom_err = h_nom->GetBinError(i,j);
	    double sf_RDown = h_RDown->GetBinContent(i,j);
    	    double sf_RDown_err = h_RDown->GetBinError(i,j);
    	    double sf_Gaus = h_Gaus->GetBinContent(i,j);
    	    double sf_Gaus_err = h_Gaus->GetBinError(i,j);
    	    double sf_MCTemp_Auto = h_MCTemp_Auto->GetBinContent(i,j);
    	    double sf_MCTemp_Auto_err = h_MCTemp_Auto->GetBinError(i,j);
    	    double sf_Zy = h_Zy->GetBinContent(i,j);
    	    double sf_Zy_err = h_Zy->GetBinError(i,j);

    	    double sys_RDown = (sf_RDown - sf_nom)/sf_nom;
    	    double sys_Gaus = (sf_Gaus - sf_nom)/sf_nom;
    	    double sys_MCTemp_Auto = (sf_MCTemp_Auto - sf_nom)/sf_nom;
    	    double sys_Zy = (sf_Zy - sf_nom)/sf_nom;
    	    double sys_tot = sqrt(pow(sys_RDown,2) + pow(sys_Gaus,2) + pow(sys_MCTemp_Auto,2) + pow(sys_Zy,2));
    	    double sys_stat = sf_nom_err/sf_nom;
    	    double sys = sqrt(pow(sys_tot,2) + pow(sys_stat,2));

	    h_nom_final->SetBinError(i,j,sf_nom*sys);
	}
    }

    vector<TH1F*> h_1d_nom = DrawMap(h_nom);
    vector<TH1F*> h_1d_RDown = DrawMap(h_RDown, "_RDown");
    vector<TH1F*> h_1d_Gaus = DrawMap(h_Gaus, "_Gaus");
    vector<TH1F*> h_1d_MCTemp_Auto = DrawMap(h_MCTemp_Auto, "_MCTemp_Auto");
    vector<TH1F*> h_1d_Zy = DrawMap(h_Zy, "_Zy");
    vector<TH1F*> h_1d_Final = DrawMap(h_nom_final, "_Final");

    PlotComparor* PC = new PlotComparor();
    PC->Is13TeV(true);
    PC->DataLumi(36.1);
    PC->SetSaveDir("plots/EFake/");
    for (int i = 0; i < h_1d_nom.size(); i++) {
	PC->ClearAlterHs();
	PC->SetBaseH(h_1d_Final.at(i));
	PC->SetBaseHName("Total");
	PC->AddAlterH(h_1d_nom.at(i));
	PC->AddAlterHName("Stat");
	PC->AddAlterH(h_1d_RDown.at(i));
	PC->AddAlterHName("Range");
	PC->AddAlterH(h_1d_Gaus.at(i));
	PC->AddAlterHName("Bkg");
	PC->AddAlterH(h_1d_MCTemp_Auto.at(i));
	PC->AddAlterHName("Signal");
	PC->AddAlterH(h_1d_Zy.at(i));
	PC->AddAlterHName("Zy");
	char tmp[50];
	sprintf(tmp, "PtBin%d", i+1);
	PC->SetChannel(tmp);
	string savename = "Map_EFake_SF_"; savename += tmp;
	PC->SetSaveName(savename);
	PC->SetXtitle("|#eta|");
    	PC->SetXtitleSize(0.05);
    	PC->SetYtitle("F.R.");
    	PC->SetYRangeRatio(1.8);
    	PC->SetLineWidth(3);
    	PC->SetLegendNC(2);

	PC->Compare();
    }

    delete PC;
}

vector<TH1F*> DrawMap(TH2F*h, string tag) {
    TCanvas *c = new TCanvas("c","c",800,600);
    c->SetTopMargin(0.02);
    c->SetLeftMargin(0.12);
    c->SetBottomMargin(0.12);

    h->SetTitle("");

    h->GetYaxis()->SetBinLabel(1, "[25,35]");
    h->GetYaxis()->SetBinLabel(2, "[35,45]");
    h->GetYaxis()->SetBinLabel(3, "[45,60]");
    h->GetYaxis()->SetBinLabel(4, "[60,inf]");
    h->GetXaxis()->SetBinLabel(1, "[0,0.5]");
    h->GetXaxis()->SetBinLabel(2, "[0.5,1]");
    h->GetXaxis()->SetBinLabel(3, "[1,1.37]");
    h->GetXaxis()->SetBinLabel(4, "[1.37,1.52]");
    h->GetXaxis()->SetBinLabel(5, "[1.52,2]");
    h->GetXaxis()->SetBinLabel(6, "[2,2.37]");

    h->GetYaxis()->SetTitle("Pt [GeV]");
    h->GetXaxis()->SetTitle("|#eta|");

    gStyle->SetPaintTextFormat(".2f");
    gStyle->SetOptStat(0);
    h->SetMarkerSize(2);
    //h->GetZaxis()->SetRangeUser(0.5,2.5);
    h->SetContour(100);
    //Int_t colors[] = {3, 2};
    //gStyle->SetPalette((sizeof(colors)/sizeof(Int_t)), colors);
    gStyle->SetPalette(55);
    h->Draw("colz text E");

    TString savename = "plots/EFake/Map_EFake_SF";
    if (tag != "") savename += tag;

    c->SaveAs(savename + ".pdf");
    c->SaveAs(savename + ".png");
    c->SaveAs(savename + ".C");

    delete c;

    vector<TH1F*> h_1ds;
    for (int i = 1; i <= h->GetNbinsY(); i++) {
	char tmp[50];
	sprintf(tmp, "1dh_ptbin%d%s", i, tag.c_str());
	TH1F* h1d = new TH1F(tmp,tmp,h->GetNbinsX()-1,0,h->GetNbinsX()-1);
	int j2 = 0;
	for (int j = 1; j <= h->GetNbinsX(); j++) {
	    if (j == 4) continue;
	    j2++;
	    h1d->SetBinContent(j2, h->GetBinContent(j,i));
	    if (tag == "" || tag == "_Final") h1d->SetBinError(j2, h->GetBinError(j,i));
	    else h1d->SetBinError(j2, 0);
	    h1d->GetXaxis()->SetBinLabel(j2, h->GetXaxis()->GetBinLabel(j));
	}
	h_1ds.push_back(h1d);
    }
    return h_1ds;
}
