#include <iostream>
#include <TLatex.h>
#include <TCanvas.h>
#include <iomanip>
#include <TFile.h>
#include <TH1F.h>
#include <vector>
#include "StringPlayer.h"
#include "EDM.h"
#include "PlotComparor.h"
#include <TLine.h>

using namespace std;

void DoComparison(TString ps, TString chan, TString tag = "", bool dopeak = false);

int main()

{
    DoComparison("UPS", "ejets");
    //DoComparison("UPS", "mujets");
    DoComparison("PS", "mujets");

    DoComparison("UPS", "ejets", "pt", true);
    //DoComparison("UPS", "mujets", "dPhi", true);
    DoComparison("PS", "mujets", "dPhi", true);
}

void DoComparison(TString ps, TString chan, TString tag, bool dopeak) {
    vector<TString> paras;
    paras.push_back("pt");
    paras.push_back("eta");
    paras.push_back("dR");
    paras.push_back("jetpt");
    paras.push_back("dPhi");
    paras.push_back("nbtag");
    paras.push_back("mtw");
    paras.push_back("pt:eta");
    paras.push_back("pt:dR");
    paras.push_back("pt:jetpt");
    paras.push_back("pt:dPhi");
    paras.push_back("pt:nbtag");
    paras.push_back("pt:mtw");
    paras.push_back("eta:dR");
    paras.push_back("eta:jetpt");
    paras.push_back("eta:dPhi");
    paras.push_back("eta:nbtag");
    paras.push_back("eta:mtw");
    paras.push_back("dR:jetpt");
    paras.push_back("dR:dPhi");
    paras.push_back("dR:nbtag");
    paras.push_back("dR:mtw");
    paras.push_back("jetpt:dPhi");
    paras.push_back("jetpt:nbtag");
    paras.push_back("jetpt:mtw");
    paras.push_back("dPhi:nbtag");
    paras.push_back("dPhi:mtw");
    paras.push_back("nbtag:mtw");
    paras.push_back("pt:eta:dR");
    paras.push_back("pt:eta:jetpt");
    paras.push_back("pt:eta:dPhi");
    paras.push_back("pt:eta:nbtag");
    paras.push_back("pt:eta:mtw");
    paras.push_back("pt:dR:jetpt");
    paras.push_back("pt:dR:dPhi");
    paras.push_back("pt:dR:nbtag");
    paras.push_back("pt:dR:mtw");
    paras.push_back("pt:jetpt:dPhi");
    paras.push_back("pt:jetpt:nbtag");
    paras.push_back("pt:jetpt:mtw");
    paras.push_back("pt:dPhi:nbtag");
    paras.push_back("pt:dPhi:mtw");
    paras.push_back("pt:nbtag:mtw");
    paras.push_back("eta:dR:jetpt");
    paras.push_back("eta:dR:dPhi");
    paras.push_back("eta:dR:nbtag");
    paras.push_back("eta:dR:mtw");
    paras.push_back("eta:jetpt:dPhi");
    paras.push_back("eta:jetpt:nbtag");
    paras.push_back("eta:jetpt:mtw");
    paras.push_back("eta:dPhi:nbtag");
    paras.push_back("eta:dPhi:mtw");
    paras.push_back("eta:nbtag:mtw");
    paras.push_back("dR:jetpt:dPhi");
    paras.push_back("dR:jetpt:nbtag");
    paras.push_back("dR:jetpt:mtw");
    paras.push_back("dR:dPhi:nbtag");
    paras.push_back("dR:dPhi:mtw");
    paras.push_back("dR:nbtag:mtw");
    paras.push_back("jetpt:dPhi:nbtag");
    paras.push_back("jetpt:dPhi:mtw");
    paras.push_back("jetpt:nbtag:mtw");
    paras.push_back("dPhi:nbtag:mtw");

    vector<bool> para_skip;
    for (int i = 0; i < paras.size(); i++) {
	if (tag == "") para_skip.push_back(false);
	else {
	    if (tag == "pt") {
		if (paras.at(i).Contains("pt:") && !paras.at(i).Contains("jetpt:") ) {
		    //cout << "!!!!!!!!Skip " << paras.at(i) << endl;
		    para_skip.push_back(true);
		} else if (paras.at(i).Contains("pt:jetpt:")) {
		    //cout << "!!!!!!!!Skip " << paras.at(i) << endl;
		    para_skip.push_back(true);
		} else if (paras.at(i) == "pt") {
		    //cout << "!!!!!!!!Skip " << paras.at(i) << endl;
		    para_skip.push_back(true);
		} else {
		    para_skip.push_back(false);
		}
	    }
	    else {
		if (paras.at(i).Contains(tag.Data())) {
		    //cout << "!!!!!!!!Skip " << paras.at(i) << endl;
		    para_skip.push_back(true);
		} else {
		    para_skip.push_back(false);
		}
	    }
	}
    }

    //cout << setw(20) << " ";
    //for (int i = 0; i < names.size(); i++) {
    //    cout << " &" << setw(20) << names.at(i);
    //}
    //cout << " &" << setw(20) << "Aver.+Enve.";
    //cout << "\\\\" << endl;

    TString file = "results/Results_QCD_13TeV_CR1_"+chan+"_CutSR_"+ps+"_V007.04.root";
    TFile* f = new TFile(file);

    {
    PlotComparor* PC = new PlotComparor();
    PC->SetSaveDir("plots/");
    PC->NoLegend();
    PC->Is13TeV(true);
    PC->DataLumi(36.1);
    PC->HasData(true);
    vector<TString> vars;
    vars.push_back("LeadLepPt");
    vars.push_back("LeadJetPt");
    vars.push_back("LeadPhPt");
    vars.push_back("MWT");
    vars.push_back("MET");
    vars.push_back("dPhiLepMET");
    for (int i = 0; i < vars.size(); i++) {
	PC->ClearAlterHs();
	int firsthist = false;
	for (int j = 0; j < paras.size(); j++) {
	    if (para_skip.at(j)) continue;
	    char tmp[100];
	    sprintf(tmp, "%s_Para%d_CR1_%s_QCD_Nominal", vars.at(i).Data(), j+1, chan.Data());
	    TH1F* h = (TH1F*)f->Get(tmp);
	    if (!h) {
		cout << "cant' find h: " << tmp << endl;
		f->ls();
		exit(-1);
	    }
	    if (!firsthist) {
		firsthist = true;
		PC->SetBaseH(h);
		PC->SetBaseHName(paras.at(j).Data());
	    } else {
		PC->AddAlterH(h);
		PC->AddAlterHName(paras.at(j).Data());
	    }
	}
	TString savename = "CompareShape_QCD_SR_63Paras";
	savename += "_" + chan + "_" + ps ;
	savename += "_" + vars.at(i);
	if (tag != "") savename += "_NO" + tag;
	PC->SetSaveName(savename.Data());
	PC->Compare();
    }
    delete PC;
    }

    double n_av = 0;
    double n_max = 0;
    double n_min = 999999;
    vector<double> ns;
    vector<double> es;

    TH1F* h_all = new TH1F("results","",63,0,63);
    TH1F* h_all2;
    if (chan == "ejets" && ps == "UPS") h_all2 = new TH1F("results2","",200,0,1000);
    else if (chan == "mujets" && ps == "UPS") h_all2 = new TH1F("results2","",20,0,200);
    else if (chan == "mujets" && ps == "PS") h_all2 = new TH1F("results2","",100,0,500);
    h_all->GetYaxis()->SetTitle("Pred.");
    for (int i = 0; i < paras.size(); i++) {
	char tmp[100];
	sprintf(tmp, "Cutflow_Para%d_CR1_%s_QCD_Nominal", i+1, chan.Data());
	TH1F* h = (TH1F*)f->Get(tmp);
	double n = 0;
	double e = 0;
	for (int j = 0; j < h->GetNbinsX(); j++) {
	    //cout << h->GetXaxis()->GetBinLabel(j+1) << " " << h->GetBinContent(j+1) << endl;
	    //if (i == 24 && chan == "ejets") {
	    //    cout << paras.at(i) << ": " << h->GetXaxis()->GetBinLabel(j+1) << " " << h->GetBinContent(j+1) << endl;
	    //}
	    if (TString(h->GetXaxis()->GetBinLabel(j+1)) == "Cut:DrPhLep1.0") {
		n = h->GetBinContent(j+1);
		e = h->GetBinError(j+1);
	    }
	}
	if (para_skip.at(i)) {
	    n = -999;
	    e = -999;
	}
	h_all->SetBinContent(i+1, n);
	h_all->SetBinError(i+1, e);
	h_all->GetXaxis()->SetBinLabel(i+1, paras.at(i));
	h_all->GetXaxis()->SetLabelSize(0.03);
	h_all2->Fill(n);
	n_av += n;
	if (n < n_min) n_min = n;
	if (n > n_max) n_max = n;
	ns.push_back(n);
	es.push_back(e);
	//cout << files.at(i) << " " << n << " " << e << endl;
	//cout << " &" << setw(10) << n << "$\\pm$" << setw(5) << e;
    }
    n_av /= paras.size();
    double n_enve = (n_max - n_min)/2;
    //double e_ej_UPS_err = (n_ej_UPS_max - n_ej_UPS_min)/2;
    //cout << " &" << setw(10) << n_ej_UPS_av << "$\\pm$" << setw(5) << e_ej_UPS_err;
    //cout << "\\\\" << endl;
    //cout << endl;
    TCanvas *c = new TCanvas("c","",800,800);
    c->SetBottomMargin(0.2);
    if (chan == "ejets" && ps == "UPS") h_all->GetYaxis()->SetRangeUser(0,1000);
    else if (chan == "mujets" && ps == "UPS") h_all->GetYaxis()->SetRangeUser(0,200);
    else if (chan == "mujets" && ps == "PS") h_all->GetYaxis()->SetRangeUser(0,500);
    h_all->GetXaxis()->LabelsOption("v");
    h_all->Draw("e");

    {   
        double m_lt_x = 0.25;
        double lt_y = 0.85;
        TLatex lt;
        lt.SetNDC();
        lt.SetTextFont(72);
        lt.DrawLatex(m_lt_x, lt_y, "ATLAS");
        lt.SetTextFont(42);
        double delx = 0.115*696*c->GetWh()/(472*c->GetWw());
        lt.DrawLatex(m_lt_x+delx, lt_y, "Internal");
        double ystep = -0.08;
        lt_y += ystep;
        lt.DrawLatex(m_lt_x, lt_y, "#sqrt{s}=13TeV, 36.1 fb^{-1}");
        //lt_y += ystep;
        //lt.DrawLatex(m_lt_x, lt_y, "#intL dt = 36.1 fb^{-1}");
        lt_y += ystep;
        lt.DrawLatex(m_lt_x, lt_y, chan.Data());
    }

    TString save = "plots/QCD_63Paras_" + ps + "_" + chan;
    if (tag != "") save += "_NO" + tag;
    c->SaveAs(save+"-1.png");
    c->SaveAs(save+"-1.pdf");
    delete c;
    c = new TCanvas("c","",600,600);
    h_all2->GetYaxis()->SetRangeUser(0, h_all2->GetMaximum()*1.1);
    h_all2->GetXaxis()->SetTitle("QCD Pred.");
    h_all2->GetYaxis()->SetTitle("N(para)");
    h_all2->Draw("pl");
    save = "plots/QCD_63Paras_" + ps + "_" + chan ;
    if (tag != "") save += "_NO" + tag;
    c->SaveAs(save+"-2.png");
    c->SaveAs(save+"-2.pdf");


    double mean = h_all2->GetMean();
    double rms = h_all2->GetRMS();

    cout << "mean = " << mean << endl;
    cout << "rms = " << rms << endl;
    vector<double> diffs;
    vector<string> onesigma_paras;
    for (int i = 0; i < ns.size(); i++) {
        diffs.push_back(fabs(ns.at(i)-mean));
    }

    if (dopeak) {
	double peak_val = -1;
    	int peak_idx = -1;
    	int up_idx = -1;
    	int dn_idx = -1;
    	for (int i = 0; i < h_all2->GetNbinsX(); i++) {
    	    if (h_all2->GetBinContent(i+1) > peak_val) {
    	        peak_val = h_all2->GetBinContent(i+1);
    	        peak_idx = i+1;
    	    }
    	}

    	double lowbin_lo=0;
    	double lowbin_hi=0;
    	double highbin_lo=0;
    	double highbin_hi=0;
    	if (chan == "mujets" && ps == "PS") {
    	    lowbin_lo = 5;
    	    lowbin_hi = 10;
    	    highbin_lo = 135;
    	    highbin_hi = 140;
    	} else if (chan == "ejets" && ps == "UPS") {
    	    lowbin_lo = 155;
    	    lowbin_hi = 160;
    	    highbin_lo = 425;
    	    highbin_hi = 430;
    	}

    	vector<int> prepeakbins;
    	int lobin = -1;
    	int hibin = -1;
    	cout << "PEAK bin = " << peak_idx << endl;
    	for (int i = 0; i < ns.size(); i++) {
    	    if (ns.at(i) < (h_all2->GetBinLowEdge(peak_idx)+h_all2->GetBinWidth(peak_idx)) && ns.at(i) > h_all2->GetBinLowEdge(peak_idx)) {
    	        prepeakbins.push_back(i);
    	        cout << "Para in peak: " << paras.at(i) << " = " << ns.at(i) << endl;
    	    }
    	    if (ns.at(i) < lowbin_hi && ns.at(i) > lowbin_lo) {
    	        lobin = i;
    	        cout << "Para at low end: " << paras.at(i) << endl;
    	    }
    	    if (ns.at(i) < highbin_hi && ns.at(i) > highbin_lo) {
    	        hibin = i;
    	        cout << "Para at high end: " << paras.at(i) << endl;
    	    }
    	}

	double peak_av = 0;
	for (int i = 0; i < prepeakbins.size(); i++) {
	    peak_av += ns.at(prepeakbins.at(i));
	}
	peak_av /= prepeakbins.size();
	cout << "average peak = " << peak_av;
	
	int pkbin = -1;
	//if (chan == "ejets" && ps == "UPS") pkbin = 28;
	//else if (chan == "mujets" && ps == "PS") pkbin = 8;
	double mindiff = 999;
	for (int i = 0; i < prepeakbins.size(); i++) {
	    double tmpdiff = fabs(ns.at(prepeakbins.at(i)) - peak_av);\
	    if (chan == "ejets" && ps == "UPS") {
		if (paras.at(prepeakbins.at(i)) == "dPhi") continue;
	    }
	    cout << "peak:" << paras.at(prepeakbins.at(i)) << " diff = " << tmpdiff << endl;
	    if (tmpdiff < mindiff) {
		mindiff = tmpdiff;
		pkbin = prepeakbins.at(i);
	    }
	}

	//cout << "Index: peak " << pkbin << " up " << hibin << " down " << lobin << endl;
	//cout << "Results: peak " << ns.at(pkbin) << " up " << ns.at(hibin) << " down " << ns.at(lobin) << endl;
	string channel = "\\ch";	channel += chan.Data();
	cout << endl;
	cout << setw(17) << " " << " & " << setw(18) << "down" << " & " << setw(18) << "peak" << " & " << setw(18) << "up" << "\\\\ \\hline" << endl;
	cout << setw(11) << channel << " para. & " << setw(18) << paras.at(lobin) << " & " << setw(18) << paras.at(pkbin) << " & " << setw(18) << paras.at(hibin) << "\\\\ \\hline" << endl;
	cout << setw(15) << channel << " N & "
	         << setw(6) << ns.at(lobin) << " $\\pm$ " << setw(5) << es.at(lobin) << " & "
	         << setw(6) << ns.at(pkbin) << " $\\pm$ " << setw(5) << es.at(pkbin) << " & "
	         << setw(6) << ns.at(hibin) << " $\\pm$ " << setw(5) << es.at(hibin) << "\\\\ \\hline" << endl;
	cout << setw(11) << channel << " index & " << setw(18) << lobin << " & " << setw(18) << pkbin << " & " << setw(18) << hibin << "\\\\ \\hline" << endl;
	//cout << "peak " << pkbin << " " << paras.at(pkbin) << " " << ns.at(pkbin) << endl;
	//cout << "donw " << lobin << " " << paras.at(lobin) << " " << ns.at(lobin) << endl;
	//cout << "up   " << hibin << " " << paras.at(hibin) << " " << ns.at(hibin) << endl;

	//cout << "Origin Index: " << endl;
	//for (int i = 0; i < tmpparas.size(); i++) {
	//    if (tmpparas.at(i) == paras.at(pkbin)) cout << "peak " << i << endl;
	//    if (tmpparas.at(i) == paras.at(hibin)) cout << "up " << i << endl;
	//    if (tmpparas.at(i) == paras.at(lobin)) cout << "down " << i << endl;
	//}

    	{
    	PlotComparor* PC = new PlotComparor();
    	PC->SetSaveDir("plots/");
    	PC->Is13TeV(true);
    	PC->DataLumi(36.1);
    	PC->HasData(true);
    	vector<TString> vars;
    	vars.push_back("LeadLepPt");
    	vars.push_back("LeadJetPt");
    	vars.push_back("LeadPhPt");
    	vars.push_back("MWT");
    	vars.push_back("MET");
    	vars.push_back("dPhiLepMET");
    	for (int i = 0; i < vars.size(); i++) {
    	    PC->ClearAlterHs();
    	    //for (int k = 0; k < peakbins.size(); k++) {
    	    //    int j = peakbins.at(k);
    	    //    char tmp[100];
    	    //    sprintf(tmp, "%s_Para%d_CR1_%s_QCD_Nominal", vars.at(i).Data(), j+1, chan.Data());
    	    //    TH1F* h = (TH1F*)f->Get(tmp);
    	    //    if (!h) {
    	    //	cout << "cant' find h: " << tmp << endl;
    	    //	f->ls();
    	    //	exit(-1);
    	    //    }
    	    //    if (k == 0) {
    	    //	PC->SetBaseH(h);
    	    //	PC->SetBaseHName(("Peak_"+paras.at(j)).Data());
    	    //    } else {
    	    //	PC->AddAlterH(h);
    	    //	PC->AddAlterHName(("Peak_"+paras.at(j)).Data());
	    //    PC->AddAlterColors(1);
    	    //    }
    	    //}
    	    char tmp[100];
    	    int j = pkbin;
    	    if (j != -1) {
    	        sprintf(tmp, "%s_Para%d_CR1_%s_QCD_Nominal", vars.at(i).Data(), j+1, chan.Data());
    	        TH1F* h_pk = (TH1F*)f->Get(tmp);
    	        PC->SetBaseH(h_pk);
    	        PC->SetBaseHName(("Peak_"+paras.at(j)).Data());
    	    }
    	    j = lobin;
    	    if (j != -1) {
    	        sprintf(tmp, "%s_Para%d_CR1_%s_QCD_Nominal", vars.at(i).Data(), j+1, chan.Data());
    	        TH1F* h_lo = (TH1F*)f->Get(tmp);
    	        PC->AddAlterH(h_lo);
    	        PC->AddAlterHName(("Low_"+paras.at(j)).Data());
		PC->AddAlterColors(2);
    	    }
    	    j = hibin;
    	    if (j != -1) {
    	        sprintf(tmp, "%s_Para%d_CR1_%s_QCD_Nominal", vars.at(i).Data(), j+1, chan.Data());
    	        TH1F* h_hi = (TH1F*)f->Get(tmp);
    	        PC->AddAlterH(h_hi);
    	        PC->AddAlterHName(("High_"+paras.at(j)).Data());
		PC->AddAlterColors(3);
    	    }
    	    TString savename = "CompareShape_QCD_SR_PeakParas";
    	    savename += "_" + chan + "_" + ps ;
    	    savename += "_" + vars.at(i);
    	    if (tag != "") savename += "_NO" + tag;
    	    PC->SetSaveName(savename.Data());
	    if (chan == "mujets" && ps == "PS") PC->SetRangeY(-20, 150);
	    PC->SetChannel(chan.Data());
    	    PC->Compare();
    	}
    	delete PC;
    	}

	c = new TCanvas("c","",800,800);
    	h_all2->Draw("pl");
    	double x1 = h_all2->GetBinCenter(peak_idx);
    	double y1 = 0;
    	double x2 = x1;
    	double y2 = h_all2->GetMaximum();
    	TLine* l_cen = new TLine(x1, y1, x2, y2);
	l_cen->SetLineColor(kRed);
	l_cen->SetLineWidth(2);
	l_cen->Draw("same");
    	x1 = (lowbin_lo+lowbin_hi)/2;
    	y1 = 0;
    	x2 = x1;
    	y2 = h_all2->GetMaximum();
    	TLine* l_low = new TLine(x1, y1, x2, y2);
	l_low->SetLineColor(kBlue);
	l_low->SetLineWidth(2);
	l_low->Draw("same");
    	x1 = (highbin_lo+highbin_hi)/2;
    	y1 = 0;
    	x2 = x1;
    	y2 = h_all2->GetMaximum();
    	TLine* l_high = new TLine(x1, y1, x2, y2);
	l_high->SetLineColor(kBlue);
	l_high->SetLineWidth(2);
	l_high->Draw("same");

	{   
    	    double m_lt_x = 0.5;
    	    double lt_y = 0.85;
    	    TLatex lt;
    	    lt.SetNDC();
    	    lt.SetTextFont(72);
    	    lt.DrawLatex(m_lt_x, lt_y, "ATLAS");
    	    lt.SetTextFont(42);
    	    double delx = 0.115*696*c->GetWh()/(472*c->GetWw());
    	    lt.DrawLatex(m_lt_x+delx, lt_y, "Internal");
    	    double ystep = -0.08;
    	    lt_y += ystep;
    	    lt.DrawLatex(m_lt_x, lt_y, "#sqrt{s}=13TeV");
    	    lt_y += ystep;
    	    lt.DrawLatex(m_lt_x, lt_y, "#intL dt = 36.1 fb^{-1}");
    	    lt_y += ystep;
    	    lt.DrawLatex(m_lt_x, lt_y, chan.Data());
    	}

	save = "plots/QCD_63Paras_" + ps + "_" + chan ;
    	if (tag != "") save += "_NO" + tag;
    	c->SaveAs(save+"-3.png");
    	c->SaveAs(save+"-3.pdf");
    }

    vector<int> orders;
    OrderFVector(diffs, orders);
    int npl = 3;
    cout << "\\scalebox{0.48}{" << endl;
    cout << "\\begin{tabular}{" << endl;
    for (int i = 0; i < npl; i++) {
	cout << "c|c|c";
	if (i != npl - 1) cout << "|";
	else cout << "}" << endl;
    }
	cout << "\\hline" << endl;
    for (int i = 0; i < npl; i++) {
	cout << "Para. & Pred. & wrt Mean";
	if (i != npl - 1) cout << "&";
	else cout << "\\\\" << endl;
    }
    cout<< fixed << setprecision(1) << endl;
    for (int i = diffs.size() - 1; i >= 0;) {
	cout << "\\hline" << endl;
	for (int j = 0; j < npl; j++) {
	    if (i - j >= 0) {
		int idx = orders.at(i - j);
		if (j < npl - 1) {
		    cout << setw(20) << paras.at(idx) << " & " << setw(10) << ns.at(idx) << " $\\pm$ " << setw(10) << es.at(idx) << " & " << setw(10) << 100*diffs.at(idx)/mean << "\\% &";
		} else {
		    cout << setw(20) << paras.at(idx) << " & " << setw(10) << ns.at(idx) << " $\\pm$ " << setw(10) << es.at(idx) << " & " << setw(10) << 100*diffs.at(idx)/mean << "\\% \\\\" << endl;
		}
	    } else {
		if (diffs.size() % npl != 0) cout << "\\\\" << endl;
	    }
	}
	i -= npl;
    }
	cout << "\\hline" << endl;
    cout << "\\end{tabular}}" << endl;

}
