#include <TFile.h>
#include <TH1D.h>
#include <TCanvas.h>
#include <TLegend.h>
#include <TPad.h>
#include <TLine.h>
#include <string>
#include <TLatex.h>
#include <TMath.h>
#include <vector>
#include <TGraphErrors.h>
#include <map>
#include <TIterator.h>
#include <TKey.h>
#include <TObject.h>
#include <THStack.h>
#include <fstream>
#include "AtlasStyle.h"
#include "PlotComparor.h"
#include "StringPlayer.h"
#include "ConfigReader.h"
#include "Logger.h"

using namespace std;

string GetSysTitle(string sys) {
    string title;
    if (sys == "ttbarISRFSR") title = "$t\\bar{t}$ ISR/FSR";
    else if (sys == "ttbarGen") title = "$t\\bar{t}$ MG5 vs Sherpa";
    else if (sys == "PU") title = "Pile-up";
    else if (sys == "JESNP1") title = "JES NP 1";
    else if (sys == "WGammaNorm") title = "$W\\gamma$ norm.";
    else if (sys == "FakeLepNorm") title = "Fake-lep norm.";
    else if (sys == "ZGammaNorm") title = "$Z\\gamma$ norm.";
    else if (sys == "EFakeNorm1") title = "e-fake norm. 1";
    else if (sys == "EFakeNorm2") title = "e-fake norm. 2";
    else if (sys == "PhotonEff") title = "Photon eff.";
    else if (sys == "JESRhoTopo") title = "JES Rho topo.";
    else if (sys == "SignalPS") title = "$t\\bar{t}\\gamma$ PY8 vs H7";
    else if (sys == "STNorm") title = "Single top norm.";
    else if (sys == "intLumi") title = "Luminosity";
    else if (sys == "ZGammaQCDScale") title = "$Z\\gamma$ QCD scale";
    else if (sys == "DibosonNorm") title = "Diboson norm.";
    else title = sys;

    return title;
}
int main(int argc, char * argv[]) 
{

    vector<string> channels;
    channels.push_back("ejets");
    channels.push_back("mujets");
    channels.push_back("ee");
    channels.push_back("emu");
    channels.push_back("mumu");

    vector<string> finalsys;
    finalsys.push_back("$t\\bar{t}\\gamma$ PY8 vs H7");
    finalsys.push_back("$t\\bar{t}$ ISR/FSR");
    finalsys.push_back("$t\\bar{t}$ MG5 vs Sherpa");
    finalsys.push_back("$W\\gamma$ norm.");
    finalsys.push_back("$Z\\gamma$ norm.");
    finalsys.push_back("$Z\\gamma$ QCD scale");
    finalsys.push_back("Single top norm.");
    finalsys.push_back("Diboson norm.");
    finalsys.push_back("Fake-lep norm.");
    finalsys.push_back("e-fake norm. 1");
    finalsys.push_back("e-fake norm. 2");
    finalsys.push_back("JES NP 1");
    finalsys.push_back("JES Rho topo.");
    finalsys.push_back("Photon eff.");
    finalsys.push_back("Pile-up");
    finalsys.push_back("Luminosity");

    vector<vector<pair<double, string> > > allsyss;
    for (int ich = 0; ich < channels.size(); ich++) {
	string channel = channels.at(ich);

    	ifstream ifile;
    	string ifilename = "/afs/cern.ch/work/y/yili/private/Analysis/TRexFitter/run/FitExample_"+channel+"/Fits/NPRanking.txt";
    	ifile.open(ifilename.c_str());
    	string line;
    	vector<pair<double,string> > allsys;
    	double totalsys = 0;
    	while(getline(ifile,line)) {
    	    istringstream iss(line);
    	    vector<string> words;
    	    for (string word; iss >> word;) {
    	        words.push_back(word);
    	    }
    	    string sysname = words.at(0);
    	    double up = atof(words.at(6).c_str());
    	    double down = atof(words.at(7).c_str());
    	    double sys = fabs(up) > fabs(down) ? fabs(up) : fabs(down);
    	    sys *= 100;
    	    allsys.push_back(pair<double,string>(sys, GetSysTitle(sysname)));
    	    totalsys = sqrt(pow(totalsys,2) + pow(sys,2));
    	}
    	sort(allsys.rbegin(), allsys.rend());
	allsyss.push_back(allsys);

    	cout << fixed << setprecision(1);
    	cout << "\\scalebox{0.8}{" << endl;
    	cout << "\\begin{tabular}{c|c}";
    	cout << "\\hline" << endl;
    	cout << "\\hline" << endl;
    	cout << "\\multicolumn{2}{c}{\\ch"<<channel<<"} \\\\" << endl;
    	cout << "\\hline" << endl;
    	cout << "Source & $\\Delta\\mu$ \\\\" << endl;
    	cout << "\\hline" << endl;
    	for (int i = 0; i < allsys.size(); i++) {
    	    string sysname = allsys.at(i).second;
    	    double sys = allsys.at(i).first;
    	    char tmp[10];
    	    sprintf(tmp, "%.1f", sys);
    	    if (sys < 0.05) sprintf(tmp, "%s", "<0.1");
    	    cout << setw(30) << sysname << " & " << setw(10) << tmp << "\\% \\\\" << endl;
    	}
    	cout << "\\hline" << endl;
    	cout << setw(30) << "Total Sys." << " & " << setw(10) << totalsys << endl;
    	cout << "\\hline" << endl;
    	cout << "\\hline" << endl;
    	cout << "\\end{tabular}}" << endl;

    	ofstream ofile;
    	string ofilename = "log_sysrank_" + channel;
    	ofile.open(ofilename.c_str());
    	ofile << fixed << setprecision(1);
    	ofile << "\\scalebox{0.8}{" << endl;
    	ofile << "\\begin{tabular}{c|c}";
    	ofile << "\\hline" << endl;
    	ofile << "\\hline" << endl;
    	ofile << "\\multicolumn{2}{c}{\\ch"<<channel<<"} \\\\" << endl;
    	ofile << "\\hline" << endl;
    	ofile << "Source & $\\Delta\\mu$ \\\\" << endl;
    	ofile << "\\hline" << endl;
    	for (int i = 0; i < allsys.size(); i++) {
    	    string sysname = allsys.at(i).second;
    	    double sys = allsys.at(i).first;
    	    char tmp[10];
    	    sprintf(tmp, "%.1f", sys);
    	    if (sys < 0.05) sprintf(tmp, "%s", "<0.1");
    	    ofile << setw(30) << sysname << " & " << setw(10) << tmp << "\\% \\\\" << endl;
    	}
    	ofile << "\\hline" << endl;
    	ofile << setw(30) << "Total Sys." << " & " << setw(10) << totalsys << "\\% \\\\" << endl;
    	ofile << "\\hline" << endl;
    	ofile << "\\hline" << endl;
    	ofile << "\\end{tabular}}" << endl;
    	ofile.close();
    }
    vector<double> totalsyss;
    for (int ich = 0; ich < channels.size(); ich++) {
	double totalsys = 0;
	vector<pair<double,string> > allsys = allsyss.at(ich);
	for (int isys = 0; isys < allsys.size(); isys++) {
	    totalsys = sqrt(pow(totalsys,2) + pow(allsys.at(isys).first,2));
	}
	totalsyss.push_back(totalsys);
    }

    cout << "\\scalebox{0.8}{" << endl;
    cout << "\\begin{tabular}{c|c|c|c|c|c}";
    cout << "\\hline" << endl;
    cout << "\\hline" << endl;
    cout << setw(30) << "Source" << " & ";
    for (int ich = 0; ich < channels.size(); ich++) {
	cout << "\\ch" << channels.at(ich);
	if (ich != channels.size() - 1) cout << " & ";
	else cout << " \\\\" << endl;
    }
    cout << "\\hline" << endl;
    for (int i = 0; i < finalsys.size(); i++) {
	string sys = finalsys.at(i);
    	cout << setw(30) << sys << " & ";
	for (int ich = 0; ich < channels.size(); ich++) {
	    vector<pair<double,string> > allsys = allsyss.at(ich);
	    double sysich = -1;
	    for (int isys = 0; isys < allsys.size(); isys++) {
		if (allsys.at(isys).second == sys) {
		    sysich = allsys.at(isys).first;
		    break;
		}
	    }
	    char tmp[10];
	    if (sysich == -1) cout << setw(10) << "" << "  ";
	    else { 
		if (sysich < 0.05) sprintf(tmp, "%s", "<0.1");
		else sprintf(tmp, "%.1f", sysich);
		cout << setw(10) << tmp << "\\%";
	    }
	    if (ich != channels.size() - 1) cout << " & ";
	    else cout << " \\\\" << endl;
	}
    }
    cout << "\\hline" << endl;
    cout << setw(30) << "Total Sys." << " & ";
    for (int ich = 0; ich < channels.size(); ich++) {
        char tmp[10];
        sprintf(tmp, "%.1f", totalsyss.at(ich));
        cout << setw(10) << tmp << "\\%";
        if (ich != channels.size() - 1) cout << " & ";
        else cout << " \\\\" << endl;
    }
    cout << "\\hline" << endl;
    cout << "\\hline" << endl;
    cout << "\\end{tabular}}" << endl;

    ofstream ofile;
    ofile.open("log_sysrank");
    ofile << "\\scalebox{0.8}{" << endl;
    ofile << "\\begin{tabular}{l|c|c|c|c|c}";
    ofile << "\\hline" << endl;
    ofile << "\\hline" << endl;
    ofile << setw(30) << "Source" << " & ";
    for (int ich = 0; ich < channels.size(); ich++) {
	ofile << "\\ch" << channels.at(ich);
	if (ich != channels.size() - 1) ofile << " & ";
	else ofile << " \\\\" << endl;
    }
    ofile << "\\hline" << endl;
    for (int i = 0; i < finalsys.size(); i++) {
	string sys = finalsys.at(i);
    	ofile << setw(30) << sys << " & ";
	for (int ich = 0; ich < channels.size(); ich++) {
	    vector<pair<double,string> > allsys = allsyss.at(ich);
	    double sysich = -1;
	    for (int isys = 0; isys < allsys.size(); isys++) {
		if (allsys.at(isys).second == sys) {
		    sysich = allsys.at(isys).first;
		    break;
		}
	    }
	    char tmp[10];
	    if (sysich == -1) ofile << setw(10) << "" << "  ";
	    else { 
		if (sysich < 0.05) sprintf(tmp, "%s", "<0.1");
		else sprintf(tmp, "%.1f", sysich);
		ofile << setw(10) << tmp << "\\%";
	    }
	    if (ich != channels.size() - 1) ofile << " & ";
	    else ofile << " \\\\" << endl;
	}
    }
    ofile << "\\hline" << endl;
    ofile << setw(30) << "Total Sys." << " & ";
    for (int ich = 0; ich < channels.size(); ich++) {
        char tmp[10];
        sprintf(tmp, "%.1f", totalsyss.at(ich));
        ofile << setw(10) << tmp << "\\%";
        if (ich != channels.size() - 1) ofile << " & ";
        else ofile << " \\\\" << endl;
    }
    ofile << "\\hline" << endl;
    ofile << "\\hline" << endl;
    ofile << "\\end{tabular}}" << endl;
    ofile.close();
}
