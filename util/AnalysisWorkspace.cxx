#include "Logger.h"
#include "WorkspaceAnalyzor.h"
#include "ConfigReader.h"

#include <TStopwatch.h>
#include <THStack.h>
#include <TKey.h>
#include <TFile.h>
#include <TH1F.h>
#include <TF1.h>
#include <TLegend.h>
#include <TStyle.h>

#include <iostream>
#include <sstream>
#include <string>
#include <iomanip>
#include <string>
#include <libgen.h>
#include <stdio.h>      
#include <stdlib.h> 
#include <set>

using namespace std;

int main(int argc, char * argv[]) {

    TStopwatch t; t.Start();

    Logger *lg = new Logger("MAIN");
    lg->Info("s", "Hi, this is the main programm~");
    lg->NewLine();

    if (argc < 2) {
	lg->Err("s", "Missing input parameter [config file]");
	return 0;
    }

    // ------------------ READ CONFIG -------------------
    string filename = argv[1];
    if (filename.find("--",0) != string::npos) filename = "../config/8TeV_analysis_workspace.cfg";
    ConfigReader* rd = new ConfigReader(filename, '_', false);
    rd->Init();

    string Mode = rd->GetValue("_Mode");
    bool Inverse, Silent, Save, Asimov, ShutUp, Test;
    vector<string> ModeOptions = rd->GetValueAll("_ModeOptions");
    rd->FillValueB("_ModeOptions", "Inverse", Inverse);
    rd->FillValueB("_ModeOptions", "Silent", Silent);
    rd->FillValueB("_ModeOptions", "ShutUp", ShutUp);
    rd->FillValueB("_ModeOptions", "Save", Save);
    rd->FillValueB("_ModeOptions", "Asimov", Asimov);
    rd->FillValueB("_ModeOptions", "Test", Test);

    string SaveDir = rd->GetValue("_SaveDir");
    vector<string> SaveKeyWords = rd->GetValueAll("_SaveKeyWords");

    string FitOption = rd->GetValue("_FitOption");
    if (FitOption == "") FitOption = "Plain";
    string SysStudyOption = rd->GetValue("_SysStudyOption");
    if (SysStudyOption == "") SysStudyOption = "SysStat";
    int RefPOI = rd->GetValueI("_RefPOI");
    string PlotDir = rd->GetValue("_PlotDir");
    bool PLC, Stack, Correlation, Pull, Logy;
    rd->FillValueB("_PlotOptions", "PLC", PLC);
    rd->FillValueB("_PlotOptions", "Stack", Stack);
    rd->FillValueB("_PlotOptions", "Correlation", Correlation);
    rd->FillValueB("_PlotOptions", "Pull", Pull);
    rd->FillValueB("_PlotOptions", "Logy", Logy);

    string PullOrder = rd->GetValue("_PullOrder");
    string FileName = rd->GetValue("_FileName");
    string WSName = rd->GetValue("_WSName");
    string MCName = rd->GetValue("_MCName");
    string DataName = rd->GetValue("_DataName");
    vector<string> FuncFilter = rd->GetValueAll("_FunctionFilter");

    bool LinearityCheck = false;
    for (int i = 0; i < argc; i++) {
	if (!strcmp(argv[i],"--mode")) {
	    Mode = argv[i+1];
   	}
	if (!strcmp(argv[i],"--workspace")) {
	    FileName = argv[i+1];
   	}
	if (!strcmp(argv[i],"--save")) {
	    Save = true;
   	}
	if (!strcmp(argv[i],"--test")) {
	    Test = true;
   	}
	if (!strcmp(argv[i],"--lcheck")) {
	    LinearityCheck = true;
   	}
	if (!strcmp(argv[i],"--shutup")) {
	    ShutUp = true;
   	}
	if (!strcmp(argv[i],"--inverse") && !Inverse) {
	    Inverse = true;
	    ModeOptions.push_back("Inverse");
   	}
	if (!strcmp(argv[i],"--asimov") && !Asimov) {
	    Asimov = true;
	    ModeOptions.push_back("Asimov");
   	}
	if (!strcmp(argv[i],"--noplc")) {
	    PLC = false;
   	}
	if (!strcmp(argv[i],"--plc")) {
	    PLC = true;
   	}
	if (!strcmp(argv[i],"--pullorder")) {
	    PullOrder = argv[i+1];
   	}
	if (!strcmp(argv[i],"--nostack")) {
	    Stack = false;
   	}
	if (!strcmp(argv[i],"--stack")) {
	    Stack = true;
   	}
	if (!strcmp(argv[i],"--poi1")) {
	    RefPOI = 1;
   	}
	if (!strcmp(argv[i],"--poi2")) {
	    RefPOI = 2;
   	}
	if (!strcmp(argv[i],"--poi3")) {
	    RefPOI = 3;
   	}
	if (!strcmp(argv[i],"--poi4")) {
	    RefPOI = 4;
   	}
	if (!strcmp(argv[i],"--poi5")) {
	    RefPOI = 5;
   	}
	if (!strcmp(argv[i],"--corr")) {
	    Correlation = true;
   	}
	if (!strcmp(argv[i],"--pull")) {
	    Pull = true;
   	}
	if (!strcmp(argv[i],"--fitoption")) {
	    FitOption = argv[i+1];
   	}
	if (!strcmp(argv[i],"--sysstudyoption")) {
	    SysStudyOption = argv[i+1];
   	}
	if (!strcmp(argv[i],"--tag")) {
	    SaveKeyWords.push_back(string(argv[i+1]));
   	}
    }

    if (FileName.find("Diff_Pt",0) != string::npos) PlotDir += "/diffpt/";
    else if (FileName.find("Diff_Eta",0) != string::npos) PlotDir += "/diffeta/";
    else PlotDir += "/fiducial/";

    if (Mode != "Print" && Mode != "Fit" && Mode != "Plot" && Mode != "SysStudy" && Mode != "ShowSys") {
	lg->Err("ss", "Unsupported mode -->", Mode.c_str());
	return 0;
    }

    // ---------------------------------------------------------


    // ------------------ DIGEST CONFIG -------------------------
    string savename;
    if (Save) {
        savename = "Results"; 
	savename += "_"; savename += Mode; savename += "_";
	int n1 = FileName.find("Workspace_",0);
	int n2 = FileName.find(".root",0);
	string subfilename = FileName.substr(n1+10,n2-n1-10);
	savename += subfilename;
	for (int i = 0; i < SaveKeyWords.size(); i++) {
    	    savename += "_"; savename += SaveKeyWords.at(i);
    	}
	for (int i = 0; i < ModeOptions.size(); i++) {
    	    if (ModeOptions.at(i) == "Silent") continue;
    	    if (ModeOptions.at(i) == "Save") continue;
    	    savename += "_"; savename += ModeOptions.at(i);
    	}
	if (Mode == "SysStudy") {
	    savename += "_"; savename += "RefPOI";
	    char tmpchar[10];
	    sprintf(tmpchar, "%d", RefPOI);
	    savename += tmpchar;
	}
        savename += ".root";
    }
    // ---------------------------------------------------------------


    // ---------------------------------------------------------------
    lg->Info("s", "============= Config Summary ==============");
    lg->Info("ss", "File name            -->", FileName.c_str());
    lg->Info("ss", "Worspace name        -->", WSName.c_str());
    lg->Info("ss", "Model config name    -->", MCName.c_str());
    if (!Asimov) lg->Info("ss", "Data set name        -->", DataName.c_str());
    else lg->Warn("s", "Will use asimov data set!");
    lg->Info("s", "-------------------------------------------");
    if (Mode != "Print") {
	if (!LinearityCheck) {
	lg->Info("ss", "Mode                 -->", Mode.c_str());
	if (Mode == "Fit") {
    	    lg->Info("ss", "Fit type ?           -->", FitOption.c_str());
    	    lg->Info("ss", "Fix systematics?     -->", Inverse ? "yes" : "no");
    	}
    	if (Mode == "Plot") {
    	    lg->Info("ss", "Fix systematics?     -->", Inverse ? "yes" : "no");
    	    lg->Info("ss", "Draw PLC Plot ?      -->", PLC ? "yes" : "no");
    	    lg->Info("ss", "Draw Stack Hists ?   -->", Stack ? "yes" : "no");
    	    lg->Info("ss", "Draw Correlation ?   -->", Correlation ? "yes" : "no");
    	    lg->Info("ss", "Draw Nuis pull ?     -->", Pull ? "yes" : "no");
	    if (PullOrder != "") lg->Warn("ss", "Will show the leading sys from order file ->", PullOrder.c_str());
    	    lg->Info("ss", "Use y log sclae ?    -->", Logy ? "yes" : "no");
    	    lg->Info("ss", "Plots save to ?      -->", PlotDir.c_str());
    	}
	if (Mode == "SysStudy") {
    	    lg->Info("ss", "Sys study type ?     -->", SysStudyOption.c_str());
	    lg->Warn("sd", "Reference POI ?-->", RefPOI);
	    if (SysStudyOption == "AllSys") {
		lg->Info("ss", "Sys study nuissance ?-->", Inverse ? "fixed" : "floated");
	    }
	}
	if (Save) {
    	    lg->Info("ss", "Save results to ?    -->", savename.c_str());
	}
	} else {
    	    lg->Info("s", "Will do linearity check!");
	}
    } 
    lg->Info("s", "===========================================");



    // ---------------------- INIT ANALYZOR -------------------------
    lg->NewLine();
    lg->Info("s", "Initializing workspace analyzor...");

    WorkspaceAnalyzor *WA = new WorkspaceAnalyzor();
    WA->SetFileName(FileName.c_str());
    WA->SetWSName(WSName);
    WA->SetMCName(MCName);
    WA->SetDataName(DataName);
    WA->SetSilentMode(Silent);
    if (Test) WA->Debug();
    if (ShutUp) WA->ShutUp();
    if (FuncFilter.size() != 0) WA->SetFuncFilter(FuncFilter);

    if (Save) {
        WA->SetSaveDir(SaveDir);
        WA->SetSaveName(savename);
    }

    WA->Init();
    lg->Info("s", "Workspace analyzor initialized.");
    lg->NewLine();
    // -------------------------------------------------------
    
    if (LinearityCheck) {
	WA->LinearityCheck();
	return 1;
    }

    if (Mode == "ShowSys") {
	WA->PrintSysNames();
    }

    if (Asimov) WA->UseAsimovData();

    if (Mode == "Print") {
        lg->Info("s", "PRINT workspace!");
        WA->Print();
    }


    if (Mode == "Fit") {
        lg->Info("s", "FIT model");
	if (Inverse) WA->FixAllSys();
        WA->Fit(FitOption);
	if (Save) WA->Save();
    }


    if (Mode == "Plot") {
        lg->Info("s", "DO plots!");
	if (PullOrder != "") WA->UsePullOrder(PullOrder);
	WA->SetPlotDir(PlotDir);
	WA->SetInverseMode(Inverse);
        WA->Plot(PLC, Stack, Correlation, Pull, Logy);
	if (Save) WA->Save();
    }


    if (Mode == "SysStudy") {
        lg->Info("s", "SYSTEMATICS sutdy");
	WA->SetInverseMode(Inverse);
	WA->SetReferencePOI(RefPOI);
	//if (SysStudyOption != "ToyAll") {
	WA->SysStudy(SysStudyOption);
	//} else {
	//    vector<string>* sysnames = WA->GetSysNames();

	//    vector<string> sys_done;
	//    TFile* f= new TFile("results/Results_SysStudy_Toy.root");
	//    TIter next(f->GetListOfKeys());
	//    TKey *key;
	//    while ((key = (TKey*)next())) {
	//	string name = key->GetName();
	//	sys_done.push_back(name);
	//    }

	//    for (int i = 0; i < sysnames->size(); i++) {
	//	bool done = false;
	//	for (int j = 0; j < sys_done.size(); j++) {
	//	    if (sys_done.at(j) == sysnames->at(i)) {
	//		done = true;
	//		break;
	//	    }
	//	}
	//	if (done) continue;

	//	string tmpoption = "Toy";
	//	tmpoption += sysnames->at(i);
	//	lg->Info("ss", "SysStudy of", tmpoption.c_str());
	//	WA->SysStudy(tmpoption);
	//    }
	//}
	if (Save) WA->Save();
    }


    t.Stop();
    float timeused = t.RealTime();
    float cpuused = t.CpuTime();
    lg->NewLine();
    lg->Info("s", "Finished with time comsuption ==>");
    lg->Info("sfsfsfs", "Real time -->", timeused, "seconds /", timeused/60, "minutes /", timeused/3600, "hours /");
    lg->Info("sfsfsfs", "Cpu time -->", cpuused, "seconds /", cpuused/60, "minutes /", cpuused/3600, "hours /");

    return 0;
}

//string SplitFilename(string str)
//{
//    std::size_t found = str.find_last_of("/\\");
//    std::size_t size = str.find_last_of(".");
//    return str.substr(found+1, size-found-1);
//}
