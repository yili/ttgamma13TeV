#include <iostream>
#include <TFile.h>
#include <iomanip>
#include <TH1F.h>
#include <vector>
#include <iomanip>

using namespace std;

Double_t bins_one[] = {0,1};
Double_t bins_pt[] = {20,25,30,35,40,45,50,55,60,65,70,75};
Double_t bins_eta[] = {0,0.2,0.4,0.6,0.8,1.0,1.2,1.37,1.52,1.7,1.9,2.1,2.37};

void CalcuSys(bool reverse, bool ispt, bool iseta, string fileoption = "recreate");
void CalcuSysPtMerged(string fileoption = "recreate");
void CalcuStatEta(string fileoption);

int main()
{
    CalcuSys(false, false, false, "recreate");
    CalcuSys(false, true, false, "update");
    CalcuSys(true, true, false, "update");
    CalcuSysPtMerged("update");
    CalcuSys(false, false, true, "update");
    CalcuSys(true, false, true, "update");
    CalcuStatEta("update");
}

void CalcuSys(bool reverse, bool ispt, bool iseta, string fileoption)
{
    TString rtag = "";
    if (reverse) rtag = "_Reverse";
    TString varname;
    if (ispt) varname = "_Ptbins_";
    else if (iseta) varname = "_Etabins_";
    else varname = "_";
    
    TFile* f =   new TFile("results/EGammaFakeRates_EF1_zee_Pt_Eta" + rtag + "_BkgGaus.root");
    TFile* f_m = new TFile("results/EGammaFakeRates_EF1_zee_Pt_Eta" + rtag + "_MCTemp_BkgGaus.root");
    TFile* f_e = new TFile("results/EGammaFakeRates_EF1_zee_Pt_Eta" + rtag + "_RExpand_BkgGaus.root");
    TFile* f_s = new TFile("results/EGammaFakeRates_EF1_zee_Pt_Eta" + rtag + "_RShrink_BkgGaus.root");
    TFile* f_zg =new TFile("results/EGammaFakeRates_EF1_zee_Pt_Eta" + rtag + "_BkgGaus_ZGamma.root");
    TH1F* h =     (TH1F*)f->Get("FR_SF" + varname + "EF1_zee_Nominal");
    TH1F* h_m = (TH1F*)f_m->Get("FR_SF" + varname + "EF1_zee_Nominal");
    TH1F* h_e = (TH1F*)f_e->Get("FR_SF" + varname + "EF1_zee_Nominal");
    TH1F* h_s = (TH1F*)f_s->Get("FR_SF" + varname + "EF1_zee_Nominal");
    TH1F* h_zg=(TH1F*)f_zg->Get("FR_SF" + varname + "EF1_zee_Nominal");
    if (!h) {
	cout << "can't find " << "FR_SF" << varname << "EF1_zee_Nominal" << endl;
	f->ls();
    }
    if (!h_m) {
	cout << "can't find " << "FR_SF" << varname << "EF1_zee_Nominal" << endl;
	f_m->ls();
    }
    if (!h_e) {
	cout << "can't find " << "FR_SF" << varname << "EF1_zee_Nominal" << endl;
	f_e->ls();
    }
    if (!h_s) {
	cout << "can't find " << "FR_SF" << varname << "EF1_zee_Nominal" << endl;
	f_s->ls();
    }
    if (!h_zg) {
	cout << "can't find " << "FR_SF" << varname << "EF1_zee_Nominal" << endl;
	f_zg->ls();
    }
    
    int binnum;
    TH1F* h_SF;
    TH1F* h_SF_N;
    TH1F* h_SF_M;
    TH1F* h_SF_E;
    TH1F* h_SF_S;
    TH1F* h_SF_ZG;
    if (ispt) {
        binnum = sizeof(bins_pt)/sizeof(Double_t) - 1;
        h_SF = new TH1F("SF" + varname + rtag, "SF" + varname + rtag, binnum, bins_pt);
        h_SF_N = new TH1F("SF_N" + varname + rtag, "SF_N" + varname + rtag, binnum, bins_pt);
        h_SF_M = new TH1F("SF_M" + varname + rtag, "SF_M" + varname + rtag, binnum, bins_pt);
        h_SF_E = new TH1F("SF_E" + varname + rtag, "SF_E" + varname + rtag, binnum, bins_pt);
        h_SF_S = new TH1F("SF_S" + varname + rtag, "SF_S" + varname + rtag, binnum, bins_pt);
        h_SF_ZG = new TH1F("SF_ZG" + varname + rtag, "SF_ZG" + varname + rtag, binnum, bins_pt);
    } else if (iseta) {
        binnum = sizeof(bins_eta)/sizeof(Double_t) - 1;
        h_SF = new TH1F("SF" + varname + rtag, "SF" + varname + rtag, binnum, bins_eta);
        h_SF_N = new TH1F("SF_N" + varname + rtag, "SF_N" + varname + rtag, binnum, bins_eta);
        h_SF_M = new TH1F("SF_M" + varname + rtag, "SF_M" + varname + rtag, binnum, bins_eta);
        h_SF_E = new TH1F("SF_E" + varname + rtag, "SF_E" + varname + rtag, binnum, bins_eta);
        h_SF_S = new TH1F("SF_S" + varname + rtag, "SF_S" + varname + rtag, binnum, bins_eta);
        h_SF_ZG = new TH1F("SF_ZG" + varname + rtag, "SF_ZG" + varname + rtag, binnum, bins_eta);
    } else {
        binnum = sizeof(bins_one)/sizeof(Double_t) - 1;
        h_SF = new TH1F("SF" + varname + rtag, "SF" + varname + rtag, binnum, bins_one);
        h_SF_N = new TH1F("SF_N" + varname + rtag, "SF_N" + varname + rtag, binnum, bins_one);
        h_SF_M = new TH1F("SF_M" + varname + rtag, "SF_M" + varname + rtag, binnum, bins_one);
        h_SF_E = new TH1F("SF_E" + varname + rtag, "SF_E" + varname + rtag, binnum, bins_one);
        h_SF_S = new TH1F("SF_S" + varname + rtag, "SF_S" + varname + rtag, binnum, bins_one);
        h_SF_ZG = new TH1F("SF_ZG" + varname + rtag, "SF_ZG" + varname + rtag, binnum, bins_one);
    }
 
    if (reverse) cout << "Reverse" << endl;
    if (ispt) cout << "Pt differential" << endl;
    else if (iseta) cout << "Eta differential" << endl;
    else cout << "Overall" << endl;

    cout << fixed << setprecision(3);
    for (int i = 1; i <= binnum; i++) {
        double sf = h->GetBinContent(i);
        double sf_m = h_m->GetBinContent(i);
        double sf_e = h_e->GetBinContent(i);
        double sf_s = h_s->GetBinContent(i);
        double sf_zg = h_zg->GetBinContent(i);
        double e_sf = h->GetBinError(i);
        double e_m = sf_m - sf;
        double e_e = sf_e - sf;
        double e_s = sf_s - sf;
	double e_r = fabs(e_e) > fabs(e_s) ? fabs(e_e) : fabs(e_s);
        double e_zg = sf_zg - sf;
	double e_tot = sqrt(pow(e_sf,2) + pow(e_m,2) + pow(e_r,2) + pow(e_zg,2));
	//if (ispt && !reverse) {
	//    if (i >= 7) continue;
	//}
	//if (ispt && reverse) {
	//    if (i < 7) continue;
	//}
	double binlo, binhi;
	char binrange[100];
	if (ispt) {
	    binlo = bins_pt[i-1]; 
	    binhi = bins_pt[i]; 
	    sprintf(binrange, "[%.0f,%.0f] GeV", binlo, binhi);
	} else {
	    binlo = bins_eta[i-1]; 
	    binhi = bins_eta[i]; 
	    sprintf(binrange, "[%.1f,%.1f]", binlo, binhi);
	}
        cout << setw(10) << binrange << "& " << setw(7) << sf 
                                    << " $\\pm$ " << setw(7) << e_sf << "(stat.)"
        			    << " $\\pm$ " << setw(7) << e_m << "(sig.)"
        			    << " $\\pm$ " << setw(7) << e_e << "/" << setw(7) << e_s << "(range)"
        			    << " $\\pm$ " << setw(7) << e_zg << "(sub.)"
				    << " & " << setw(7) << sf << " $\\pm$ " << setw(7) << e_tot << "(tot.)" << "\\\\ \\hline"
        			    << endl;
        h_SF->SetBinContent(i, sf); h_SF->SetBinError(i, e_tot);
        h_SF_N->SetBinContent(i, sf); h_SF_N->SetBinError(i, e_sf);
        //h_SF_M->SetBinContent(i, sf_m); h_SF_M->SetBinError(i, e_m);
        //h_SF_E->SetBinContent(i, sf_e); h_SF_E->SetBinError(i, e_s);
        //h_SF_S->SetBinContent(i, sf_s); h_SF_S->SetBinError(i, e_s);
        //h_SF_ZG->SetBinContent(i, sf_zg); h_SF_ZG->SetBinError(i, e_zg);
        h_SF_M->SetBinContent(i, sf_m); h_SF_M->SetBinError(i, 0);
        h_SF_E->SetBinContent(i, sf_e); h_SF_E->SetBinError(i, 0);
        h_SF_S->SetBinContent(i, sf_s); h_SF_S->SetBinError(i, 0);
        h_SF_ZG->SetBinContent(i, sf_zg); h_SF_ZG->SetBinError(i, 0);
    }
    //h_SF->SetBinContent(1, h_SF->GetBinContent(2));
    //h_SF->SetBinError(1, h_SF->GetBinError(2));
    
    //for (int i = 1; i <= binnum; i++) {
    //    cout << "bin " << i << ": " << h_SF->GetBinContent(i) << " " << h_SF->GetBinError(i) << endl;
    //}
    
    cout << "Save results to results/EGammaSF.root" << endl;
    TFile* newf = new TFile("results/EGammaSF.root", fileoption.c_str());
    h_SF->Write();
    h_SF_N->Write();
    h_SF_M->Write();
    h_SF_E->Write();
    h_SF_S->Write();
    h_SF_ZG->Write();
    //newf->ls();
    newf->Close();
    delete newf;
}   

void CalcuSysPtMerged(string fileoption)
{
    TFile* f =   new TFile("results/EGammaFakeRates_EF1_zee_Pt_Eta_BkgGaus.root");
    TFile* f_m = new TFile("results/EGammaFakeRates_EF1_zee_Pt_Eta_MCTemp_BkgGaus.root");
    TFile* f_e = new TFile("results/EGammaFakeRates_EF1_zee_Pt_Eta_RExpand_BkgGaus.root");
    TFile* f_s = new TFile("results/EGammaFakeRates_EF1_zee_Pt_Eta_RShrink_BkgGaus.root");
    TFile* f_zg =new TFile("results/EGammaFakeRates_EF1_zee_Pt_Eta_BkgGaus_ZGamma.root");
    TFile* f_r =   new TFile("results/EGammaFakeRates_EF1_zee_Pt_Eta_Reverse_BkgGaus.root");
    TFile* f_r_m = new TFile("results/EGammaFakeRates_EF1_zee_Pt_Eta_Reverse_MCTemp_BkgGaus.root");
    TFile* f_r_e = new TFile("results/EGammaFakeRates_EF1_zee_Pt_Eta_Reverse_RExpand_BkgGaus.root");
    TFile* f_r_s = new TFile("results/EGammaFakeRates_EF1_zee_Pt_Eta_Reverse_RShrink_BkgGaus.root");
    TFile* f_r_zg =new TFile("results/EGammaFakeRates_EF1_zee_Pt_Eta_Reverse_BkgGaus_ZGamma.root");
    TH1F* h =     (TH1F*)f->Get("FR_SF_Ptbins_EF1_zee_Nominal");
    TH1F* h_m = (TH1F*)f_m->Get("FR_SF_Ptbins_EF1_zee_Nominal");
    TH1F* h_e = (TH1F*)f_e->Get("FR_SF_Ptbins_EF1_zee_Nominal");
    TH1F* h_s = (TH1F*)f_s->Get("FR_SF_Ptbins_EF1_zee_Nominal");
    TH1F* h_zg=(TH1F*)f_zg->Get("FR_SF_Ptbins_EF1_zee_Nominal");
    TH1F* h_r =     (TH1F*)f_r->Get("FR_SF_Ptbins_EF1_zee_Nominal");
    TH1F* h_r_m = (TH1F*)f_r_m->Get("FR_SF_Ptbins_EF1_zee_Nominal");
    TH1F* h_r_e = (TH1F*)f_r_e->Get("FR_SF_Ptbins_EF1_zee_Nominal");
    TH1F* h_r_s = (TH1F*)f_r_s->Get("FR_SF_Ptbins_EF1_zee_Nominal");
    TH1F* h_r_zg=(TH1F*)f_r_zg->Get("FR_SF_Ptbins_EF1_zee_Nominal");
    
    int binnum;
    TH1F* h_SF;
    TH1F* h_SF_N;
    TH1F* h_SF_M;
    TH1F* h_SF_E;
    TH1F* h_SF_S;
    TH1F* h_SF_ZG;
    binnum = sizeof(bins_pt)/sizeof(Double_t) - 1;
    h_SF = new TH1F("SF_PtMerged_Ptbins_", "SF_PtMerged_Ptbins_", binnum, bins_pt);
    h_SF_N = new TH1F("SF_PtMerged_N_Ptbins_", "SF_PtMerged_N_Ptbins_", binnum, bins_pt);
    h_SF_M = new TH1F("SF_PtMerged_M_Ptbins_", "SF_PtMerged_M_Ptbins_", binnum, bins_pt);
    h_SF_E = new TH1F("SF_PtMerged_E_Ptbins_", "SF_PtMerged_E_Ptbins_", binnum, bins_pt);
    h_SF_S = new TH1F("SF_PtMerged_S_Ptbins_", "SF_PtMerged_S_Ptbins_", binnum, bins_pt);
    h_SF_ZG = new TH1F("SF_PtMerged_ZG_Ptbins_", "SF_PtMerged_ZG_Ptbins_", binnum, bins_pt);
 
    cout << fixed << setprecision(3);
    for (int i = 1; i <= binnum; i++) {
        double sf;
        double sf_m;
        double sf_e;
        double sf_s;
        double sf_zg;
        double e_sf;
	if (i < 7) {
	    sf = h->GetBinContent(i);
	    sf_m = h_m->GetBinContent(i);
	    sf_e = h_e->GetBinContent(i);
	    sf_s = h_s->GetBinContent(i);
	    sf_zg = h_zg->GetBinContent(i);
	    e_sf = h->GetBinError(i);
	} else {
	    sf = h_r->GetBinContent(i);
	    sf_m = h_r_m->GetBinContent(i);
	    sf_e = h_r_e->GetBinContent(i);
	    sf_s = h_r_s->GetBinContent(i);
	    sf_zg = h_r_zg->GetBinContent(i);
	    e_sf = h_r->GetBinError(i);
	}
        double e_m = sf_m - sf;
        double e_e = sf_e - sf;
        double e_s = sf_s - sf;
	double e_r = fabs(e_e) > fabs(e_s) ? fabs(e_e) : fabs(e_s);
        double e_zg = sf_zg - sf;
	double e_tot = sqrt(pow(e_sf,2) + pow(e_m,2) + pow(e_r,2) + pow(e_zg,2));

	double binlo, binhi;
	char binrange[100];
	binlo = bins_pt[i-1]; 
	binhi = bins_pt[i]; 
	sprintf(binrange, "[%.0f,%.0f] GeV", binlo, binhi);
        cout << setw(10) << binrange << "& " << setw(7) << sf 
                                    << " $\\pm$ " << setw(7) << e_sf << "(stat.)"
        			    << " $\\pm$ " << setw(7) << e_m << "(sig.)"
        			    << " $\\pm$ " << setw(7) << e_e << "/" << setw(7) << e_s << "(range)"
        			    << " $\\pm$ " << setw(7) << e_zg << "(sub.)"
				    << " & " << setw(7) << sf << " $\\pm$ " << setw(7) << e_tot << "(tot.)" << "\\\\ \\hline"
        			    << endl;
        h_SF->SetBinContent(i, sf); h_SF->SetBinError(i, e_tot);
        h_SF_N->SetBinContent(i, sf); h_SF_N->SetBinError(i, e_sf);
        //h_SF_M->SetBinContent(i, sf_m); h_SF_M->SetBinError(i, e_m);
        //h_SF_E->SetBinContent(i, sf_e); h_SF_E->SetBinError(i, e_e);
        //h_SF_S->SetBinContent(i, sf_s); h_SF_S->SetBinError(i, e_s);
        //h_SF_ZG->SetBinContent(i, sf_zg); h_SF_ZG->SetBinError(i, e_zg);
        h_SF_M->SetBinContent(i, sf_m); h_SF_M->SetBinError(i, 0);
        h_SF_E->SetBinContent(i, sf_e); h_SF_E->SetBinError(i, 0);
        h_SF_S->SetBinContent(i, sf_s); h_SF_S->SetBinError(i, 0);
        h_SF_ZG->SetBinContent(i, sf_zg); h_SF_ZG->SetBinError(i, 0);
    }
    //h_SF->SetBinContent(1, h_SF->GetBinContent(2));
    //h_SF->SetBinError(1, h_SF->GetBinError(2));
    
    //for (int i = 1; i <= binnum; i++) {
    //    cout << "bin " << i << ": " << h_SF->GetBinContent(i) << " " << h_SF->GetBinError(i) << endl;
    //}
    
    cout << "Save results to results/EGammaSF.root" << endl;
    TFile* newf = new TFile("results/EGammaSF.root", fileoption.c_str());
    h_SF->Write();
    h_SF_N->Write();
    h_SF_M->Write();
    h_SF_E->Write();
    h_SF_S->Write();
    h_SF_ZG->Write();
    //newf->ls();
    newf->Close();
    delete newf;
}   

void CalcuStatEta(string fileoption)
{
    TFile* f =   new TFile("results/EGammaFakeRates_EF1_zee_Pt_Eta_BkgGaus.root");
    TH1F* h =     (TH1F*)f->Get("FR_SF_Etabins_EF1_zee_Nominal");
    
    int binnum;
    binnum = sizeof(bins_eta)/sizeof(Double_t) - 1;
    TH1F* h_SF_Stat_Up = new TH1F("SF_StatUp_Etabins_", "SF_StatUp_Etabins_", binnum, bins_eta);
 
    for (int i = 1; i <= binnum; i++) {
        double sf = h->GetBinContent(i);
        double e_sf = h->GetBinError(i);
        h_SF_Stat_Up->SetBinContent(i, sf + e_sf);
        h_SF_Stat_Up->SetBinError(i, 0);
    }
    
    cout << "Save results to results/EGammaSF.root" << endl;
    TFile* newf = new TFile("results/EGammaSF.root", fileoption.c_str());
    h_SF_Stat_Up->Write();
    //newf->ls();
    newf->Close();
    delete newf;
}   
