#include <TH1F.h>
#include <algorithm>
#include <PlotComparor.h>
#include <TFile.h>
#include <iostream>
#include <fstream>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include "StringPlayer.h"
#include <TH2.h>
#include <algorithm>
#include <stdio.h>
#include <stdlib.h>
#include <TKey.h>
#include <TIterator.h>
#include <TH1.h>
#include <TLorentzVector.h>
#include <TStyle.h>
#include <TCanvas.h>
#include <TStopwatch.h>
#include <iostream>
#include <fstream>
#include <stdio.h>
#include <stdlib.h>
#include <TFile.h>

using namespace std;

int main()
{
    Double_t bins_phpt[] = {20,35,50,65,80,95,110,140,180,300};
    int binnum_phpt = sizeof(bins_phpt)/sizeof(Double_t) - 1;
    Double_t bins_pheta[] = {0, 0.2, 0.4, 0.6, 0.8, 1.0, 1.2, 1.7, 2.37};
    int binnum_pheta = sizeof(bins_pheta)/sizeof(Double_t) - 1;
    TH1F* h_phpt_LO_Markus_mt = new TH1F("phpt_LO_Markus_mt","phpt_LO_Markus_mt",binnum_phpt, bins_phpt);
    TH1F* h_phpt_NLO_Markus_mt = new TH1F("phpt_NLO_Markus_mt","phpt_NLO_Markus_mt",binnum_phpt, bins_phpt);
    TH1F* h_phpt_LO_MG5_dyn = new TH1F("phpt_LO_MG5_dyn","phpt_LO_MG5_dyn",binnum_phpt, bins_phpt);
    TH1F* h_phpt_LO_MG5_dyn_bare = new TH1F("phpt_LO_MG5_dyn_bare","phpt_LO_MG5_dyn_bare",binnum_phpt, bins_phpt);
    TH1F* h_pheta_LO_Markus_mt = new TH1F("pheta_LO_Markus_mt","pheta_LO_Markus_mt",binnum_pheta, bins_pheta);
    TH1F* h_pheta_NLO_Markus_mt = new TH1F("pheta_NLO_Markus_mt","pheta_NLO_Markus_mt",binnum_pheta, bins_pheta);
    TH1F* h_pheta_LO_MG5_dyn = new TH1F("pheta_LO_MG5_dyn","pheta_LO_MG5_dyn",binnum_pheta, bins_pheta);
    TH1F* h_pheta_LO_MG5_dyn_bare = new TH1F("pheta_LO_MG5_dyn_bare","pheta_LO_MG5_dyn_bare",binnum_pheta, bins_pheta);

    {
    ifstream ifile; ifile.open("/afs/cern.ch/user/y/yili/LHC.LOLO.dat");
    string line;
    while (getline(ifile,line)) {
	istringstream iss(line);
	double bin_low = -1;
	double n_bin = -1;
	double e_bin = -1;
	int idx = 0;
	int ihist = 0;
	for (string word; iss >> word;) {
	    if (idx == 0) ihist = atoi(word.substr(0,word.size()-1).c_str());
	    if (idx == 1) bin_low = atof(word.substr(0,word.size()-1).c_str());
	    if (idx == 2) n_bin = atof(word.substr(0,word.size()-1).c_str());
	    if (idx == 3) e_bin = atof(word.substr(0,word.size()-1).c_str());
	    idx++;
	}
	if (ihist == 7) {
	    int ibin = h_phpt_LO_Markus_mt->FindBin(bin_low);
	    if (ibin > h_phpt_LO_Markus_mt->GetNbinsX()) ibin = h_phpt_LO_Markus_mt->GetNbinsX();
	    h_phpt_LO_Markus_mt->SetBinContent(ibin, h_phpt_LO_Markus_mt->GetBinContent(ibin)+n_bin);
	    h_phpt_LO_Markus_mt->SetBinError(ibin, sqrt(pow(h_phpt_LO_Markus_mt->GetBinError(ibin),2)+pow(e_bin,2)));
	}
	if (ihist == 8) {
	    int ibin = h_pheta_LO_Markus_mt->FindBin(fabs(bin_low+0.05));
	    h_pheta_LO_Markus_mt->SetBinContent(ibin, h_pheta_LO_Markus_mt->GetBinContent(ibin)+n_bin);
	    h_pheta_LO_Markus_mt->SetBinError(ibin, sqrt(pow(h_pheta_LO_Markus_mt->GetBinError(ibin),2)+pow(e_bin,2)));
	}
    }
    }

    cout << "Markus LO Integral phpt: " << h_phpt_LO_Markus_mt->Integral() << endl;
    cout << "Markus LO Integral pheta: " << h_pheta_LO_Markus_mt->Integral() << endl;

    {
    ifstream ifile; ifile.open("/afs/cern.ch/user/y/yili/LHC.NLO3.dat");
    string line;
    while (getline(ifile,line)) {
	istringstream iss(line);
	double bin_low = -1;
	double n_bin = -1;
	double e_bin = -1;
	int idx = 0;
	int ihist = 0;
	for (string word; iss >> word;) {
	    if (idx == 0) ihist = atoi(word.substr(0,word.size()-1).c_str());
	    if (idx == 1) bin_low = atof(word.substr(0,word.size()-1).c_str());
	    if (idx == 2) n_bin = atof(word.substr(0,word.size()-1).c_str());
	    if (idx == 3) e_bin = atof(word.substr(0,word.size()-1).c_str());
	    idx++;
	}
	if (ihist == 7) {
	    int ibin = h_phpt_NLO_Markus_mt->FindBin(bin_low);
	    if (ibin > h_phpt_NLO_Markus_mt->GetNbinsX()) ibin = h_phpt_NLO_Markus_mt->GetNbinsX();
	    h_phpt_NLO_Markus_mt->SetBinContent(ibin, h_phpt_NLO_Markus_mt->GetBinContent(ibin)+n_bin);
	    h_phpt_NLO_Markus_mt->SetBinError(ibin, sqrt(pow(h_phpt_NLO_Markus_mt->GetBinError(ibin),2)+pow(e_bin,2)));
	}
	if (ihist == 8) {
	    int ibin = h_pheta_NLO_Markus_mt->FindBin(fabs(bin_low+0.05));
	    h_pheta_NLO_Markus_mt->SetBinContent(ibin, h_pheta_NLO_Markus_mt->GetBinContent(ibin)+n_bin);
	    h_pheta_NLO_Markus_mt->SetBinError(ibin, sqrt(pow(h_pheta_NLO_Markus_mt->GetBinError(ibin),2)+pow(e_bin,2)));
	}
    }
    }

    cout << "Markus NLO Integral phpt: " << h_phpt_NLO_Markus_mt->Integral() << endl;
    cout << "Markus NLO Integral pheta: " << h_pheta_NLO_Markus_mt->Integral() << endl;

    {
    ifstream ifile; ifile.open("/afs/cern.ch/work/y/yili/private/Analysis/Rivet/ttgamma8TeV/kfactor_ll_grid/Merged.yoda");
    //ifstream ifile; ifile.open("/afs/cern.ch/work/y/yili/private/Analysis/Rivet/ttgamma8TeV/kfactor_ll_largeStat/Rivet.yoda");
    string line;
    bool ispt = false;
    bool iseta = false;
    while (getline(ifile,line)) {
	if (line.size() == 0) continue;

	if (line.find("BEGIN",0) != string::npos) {
	    if (line.find("photon_pT",0) != string::npos) ispt = true;
	    else ispt = false;
	    if (line.find("photon_eta",0) != string::npos) iseta = true;
	    else iseta = false;
	}

	if (line.find("BEGIN",0) != string::npos) continue;
	if (line.find("Path",0) != string::npos) continue;
	if (line.find("Title",0) != string::npos) continue;
	if (line.find("Type",0) != string::npos) continue;
	if (line.find("XLabel",0) != string::npos) continue;
	if (line.find("YLabel",0) != string::npos) continue;
	if (line.find("Total",0) != string::npos) continue;
	if (line.find("Underflow",0) != string::npos) continue;
	if (line.find("Overflow",0) != string::npos) continue;
	if (line.find("END",0) != string::npos) continue;
	if (line.find("#",0) != string::npos) continue;

        istringstream iss(line);
        double bin_low = -1;
        double n_bin = -1;
        int idx = 0;
        for (string word; iss >> word;) {
            if (idx == 0) bin_low = atof(word.c_str());
            if (idx == 6) n_bin = atof(word.c_str());
            idx++;
        }
	if (ispt) {
	    int ibin = h_phpt_LO_MG5_dyn->FindBin(bin_low);
	    if (ibin > h_phpt_LO_MG5_dyn->GetNbinsX()) ibin = h_phpt_LO_MG5_dyn->GetNbinsX();
	    h_phpt_LO_MG5_dyn->SetBinContent(ibin, h_phpt_LO_MG5_dyn->GetBinContent(ibin)+n_bin);
	    h_phpt_LO_MG5_dyn->SetBinError(ibin, sqrt(pow(h_phpt_LO_MG5_dyn->GetBinError(ibin),2)+n_bin));
	}
	if (iseta) {
	    int ibin = h_pheta_LO_MG5_dyn->FindBin(fabs(bin_low+0.05));
	    h_pheta_LO_MG5_dyn->SetBinContent(ibin, h_pheta_LO_MG5_dyn->GetBinContent(ibin)+n_bin);
	    h_pheta_LO_MG5_dyn->SetBinError(ibin, sqrt(pow(h_pheta_LO_MG5_dyn->GetBinError(ibin),2)+n_bin));
	}
    }
    }

    {
    TFile* f_LO_MG5_dyn = new TFile("/afs/cern.ch/work/y/yili/private/MySVN/Code/TTG/run/output.root");
    TH1F* h_phpt_LO_MG5_dyn_raw = (TH1F*)f_LO_MG5_dyn->Get("phpt");
    TH1F* h_pheta_LO_MG5_dyn_raw = (TH1F*)f_LO_MG5_dyn->Get("pheta");
    for (int i = 1; i < h_phpt_LO_MG5_dyn_raw->GetNbinsX(); i++) {
	double bin_low = h_phpt_LO_MG5_dyn_raw->GetXaxis()->GetBinLowEdge(i);
	double n_bin = h_phpt_LO_MG5_dyn_raw->GetBinContent(i);
	double e_bin = h_phpt_LO_MG5_dyn_raw->GetBinError(i);
	int ibin = h_phpt_LO_MG5_dyn_bare->FindBin(bin_low);
	if (ibin > h_phpt_LO_MG5_dyn_bare->GetNbinsX()) ibin = h_phpt_LO_MG5_dyn_bare->GetNbinsX();
	h_phpt_LO_MG5_dyn_bare->SetBinContent(ibin, h_phpt_LO_MG5_dyn_bare->GetBinContent(ibin)+n_bin);
	h_phpt_LO_MG5_dyn_bare->SetBinError(ibin, sqrt(pow(h_phpt_LO_MG5_dyn_bare->GetBinError(ibin),2)+pow(e_bin,2)));
    }
    for (int i = 1; i < h_pheta_LO_MG5_dyn_raw->GetNbinsX(); i++) {
	double bin_low = h_pheta_LO_MG5_dyn_raw->GetXaxis()->GetBinLowEdge(i);
	double n_bin = h_pheta_LO_MG5_dyn_raw->GetBinContent(i);
	double e_bin = h_pheta_LO_MG5_dyn_raw->GetBinError(i);
	int ibin = h_pheta_LO_MG5_dyn_bare->FindBin(fabs(bin_low+0.05));
	h_pheta_LO_MG5_dyn_bare->SetBinContent(ibin, h_pheta_LO_MG5_dyn_bare->GetBinContent(ibin)+n_bin);
	h_pheta_LO_MG5_dyn_bare->SetBinError(ibin, sqrt(pow(h_pheta_LO_MG5_dyn_bare->GetBinError(ibin),2)+pow(e_bin,2)));
    }
    }

    h_phpt_LO_Markus_mt->Scale(16.2/3.24274);
    h_phpt_NLO_Markus_mt->Scale(28.6/5.71596);
    h_pheta_LO_Markus_mt->Scale(16.2/162.145);
    h_pheta_NLO_Markus_mt->Scale(28.6/285.549);

    h_phpt_LO_MG5_dyn->Scale(0.5*3356. /22485000);
    h_pheta_LO_MG5_dyn->Scale(0.5*3356./22485000);

    PlotComparor* PC = new PlotComparor();
    PC->DrawRatio(true);
    PC->SetSaveDir("plots/");
    PC->SetChannel("tt->e+mu-");
    PC->SetRatioYtitle("Ratio");    
    PC->ShowOverflow(false);

    PC->ClearAlterHs();
    h_phpt_LO_Markus_mt->SetLineWidth(3);
    h_phpt_NLO_Markus_mt->SetLineWidth(3);
    h_phpt_LO_MG5_dyn->SetLineWidth(3);
    h_phpt_LO_MG5_dyn_bare->SetLineWidth(3);
    PC->SetBaseH(h_phpt_LO_MG5_dyn);
    PC->SetBaseHName("MG5+PS LO dyn");
    PC->AddAlterH(h_phpt_LO_Markus_mt);
    PC->AddAlterHName("TOPAZ LO mt");
    PC->AddAlterH(h_phpt_LO_MG5_dyn_bare);
    PC->AddAlterHName("MG5 LO dyn");
    PC->AddAlterH(h_phpt_NLO_Markus_mt);
    PC->AddAlterHName("TOPAZ NLO mt");
    PC->SetXtitle("photon pt [GeV]");
    PC->SetYtitle("xsection [fb]");
    PC->SetMinRatio(1.6);
    PC->SetMaxRatio(2.2);
    PC->UseLogy(true);
    PC->LogyMin(0.01);
    PC->SetSaveName("Kfactor_emu_phpt");
    PC->SetLogo("tty Kfactor");
    PC->Compare();

    PC->ClearAlterHs();
    h_pheta_LO_Markus_mt->SetLineWidth(3);
    h_pheta_NLO_Markus_mt->SetLineWidth(3);
    h_pheta_LO_MG5_dyn->SetLineWidth(3);
    h_pheta_LO_MG5_dyn_bare->SetLineWidth(3);
    PC->SetBaseH(h_pheta_LO_MG5_dyn);
    PC->SetBaseHName("MG5+PS LO dyn");
    PC->AddAlterH(h_pheta_LO_Markus_mt);
    PC->AddAlterHName("TOPAZ LO mt");
    PC->AddAlterH(h_pheta_LO_MG5_dyn_bare);
    PC->AddAlterHName("MG5 LO dyn");
    PC->AddAlterH(h_pheta_NLO_Markus_mt);
    PC->AddAlterHName("TOPAZ NLO mt");
    PC->SetXtitle("photon eta");
    PC->SetYtitle("xsection [fb]");
    PC->SetMinRatio(1.6);
    PC->SetMaxRatio(2.2);
    PC->UseLogy(true);
    PC->LogyMin(0.1);
    PC->SetSaveName("Kfactor_emu_pheta");
    PC->SetLogo("tty Kfactor");
    PC->Compare();

    TFile* fout = new TFile("Kfactor_new.root","recreate");

    TH1F* h_kfac_phpt = (TH1F*)h_phpt_NLO_Markus_mt->Clone("ll_phpt");
    h_kfac_phpt->Sumw2();
    h_kfac_phpt->Divide(h_phpt_LO_MG5_dyn);
    TH1F* h_kfac_pheta = (TH1F*)h_pheta_NLO_Markus_mt->Clone("ll_pheta");
    h_kfac_pheta->Sumw2();
    h_kfac_pheta->Divide(h_pheta_LO_MG5_dyn);

    h_kfac_phpt->Write();
    h_kfac_pheta->Write();
    fout->Close();
    delete fout;
}
