#include <TFile.h>
#include <TH1F.h>
#include <TH2F.h>
#include <TIterator.h>
#include <TKey.h>
#include <TObject.h>
#include <iostream>

using namespace std;

int main ()
{
    vector<string> files1;
    files1.push_back("results/Results_ZjetsElEl_Reco_FULL_13TeV_EF2_zeg_CutZeg_PhMatch19_Lumiweighted_Test5.root");
    files1.push_back("results/Results_ZjetsElEl_Reco_FULL_13TeV_EF2_zeg_CutZegCvt_PhMatch19_Lumiweighted_Test5.root");
    files1.push_back("results/Results_ZjetsElEl_Reco_FULL_13TeV_EF2_zeg_CutZegUnCvt_PhMatch19_Lumiweighted_Test5.root");
    files1.push_back("results/Results_TTBar_Reco_FULL_13TeV_VR1_ejets_CutTTeg_TTBarEE_PhMatch22_Lumiweighted_Test5.root");
    vector<string> files2;
    files2.push_back("results/Results_ZGammajetsElEl_Reco_FULL_13TeV_EF2_zeg_CutZeg_PhMatch19_Lumiweighted_Test5.root");
    files2.push_back("results/Results_ZGammajetsElEl_Reco_FULL_13TeV_EF2_zeg_CutZegCvt_PhMatch19_Lumiweighted_Test5.root");
    files2.push_back("results/Results_ZGammajetsElEl_Reco_FULL_13TeV_EF2_zeg_CutZegUnCvt_PhMatch19_Lumiweighted_Test5.root");
    files2.push_back("results/Results_Signal_Reco_AFII_13TeV_VR1_ejets_CutTTeg_TTBarEE_PhMatch22_Lumiweighted_Test5.root");
    vector<string> savenames;
    savenames.push_back("results/SubtractedHistograms.root");
    savenames.push_back("results/SubtractedHistograms_Cvt.root");
    savenames.push_back("results/SubtractedHistograms_UnCvt.root");
    savenames.push_back("results/SubtractedHistograms_TTBar.root");

    for (int i = 0; i < files1.size(); i++) {
	TFile *f1 = new TFile(files1.at(i).c_str());
	TFile *f2 = new TFile(files2.at(i).c_str());
	string savename = savenames.at(i);

	TIter next(f1->GetListOfKeys());
	TKey* key;
	bool newfile = true;
	while ((key = (TKey*)next())) {
	    TObject* obj = (TObject*)key->ReadObj();
	    if (obj->InheritsFrom(TH1F::Class())) {
	        TH1F*h1 = (TH1F*)key->ReadObj();
	        TString hname1 = h1->GetName();
		TString hname2 = hname1;
	        hname2.ReplaceAll("ZjetsElEl", "ZGammajetsElEl");
	        hname2.ReplaceAll("TTBar", "Signal");
	        TH1F*h2 = (TH1F*)f2->Get(hname2);
	        if (!h2) {
	    	cout << "Can't find histogram in f2 -> " << hname2 << endl;
	    	f2->ls();
	    	exit(-1);
	        }
	
	        TH1F*h3=(TH1F*)h1->Clone();
	        for (int i = 0; i < h3->GetNbinsX(); i++) {
	    	h3->SetBinContent(i+1, h1->GetBinContent(i+1) - h2->GetBinContent(i+1));
	    	h3->SetBinError(i+1, sqrt(pow(h1->GetBinError(i+1),2) + pow(h2->GetBinError(i+1),2)));
	        }
	
	        string option;
	        if (newfile) option = "recreate";
	        else option = "update";
	        TFile*f3= new TFile(savename.c_str(), option.c_str());
	        newfile = false;
	        f3->cd();
	    
	        TString hname3 = hname2;
	        hname3.ReplaceAll("ZGammajetsElEl", "ZjetsElEl_Minus_ZGammajetsElEl");
	        hname3.ReplaceAll("Signal", "TTBar_Minus_Signal");
	        h3->Write(hname3);
	        
	        f3->Close();
	        delete f3;
	    } else {
	        TH2F*hh1 = (TH2F*)key->ReadObj();
	        TString hname1 = hh1->GetName();
		TString hname2 = hname1;
	        hname2.ReplaceAll("ZjetsElEl", "ZGammajetsElEl");
	        hname2.ReplaceAll("TTBar", "Signal");
	        TH2F*hh2 = (TH2F*)f2->Get(hname2);
	        if (!hh2) {
	    	cout << "Can't find histogram in f2 -> " << hname2 << endl;
	    	f2->ls();
	    	exit(-1);
	        }
	        
	        TH2F*hh3=(TH2F*)hh1->Clone();
	        for (int i = 0; i < hh3->GetNbinsX(); i++) {
	    	for (int j = 0; j < hh3->GetNbinsY(); j++) {
	          	    hh3->SetBinContent(i+1, j+1, hh1->GetBinContent(i+1, j+1) - hh2->GetBinContent(i+1, j+1));
	          	    hh3->SetBinError(i+1, sqrt(pow(hh1->GetBinError(i+1, j+1),2) + pow(hh2->GetBinError(i+1, j+1),2)));
	          	}
	        }
	
	        string option;
	        if (newfile) option = "recreate";
	        else option = "update";
	        TFile*f3= new TFile(savename.c_str(), option.c_str());
	        newfile = false;
	        f3->cd();
	    
	        TString hname3 = hname2;
	        hname3.ReplaceAll("ZGammajetsElEl", "ZjetsElEl_Minus_ZGammajetsElEl");
	        hname3.ReplaceAll("Signal", "TTBar_Minus_Signal");
	        hh3->Write(hname3);
	        
	        f3->Close();
	        delete f3;
	    }
	}
    }

    return 0;
}
