#include <TFile.h>
#include <TText.h>
#include <TPad.h>
#include <fstream>
#include <TLegend.h>
#include <TLatex.h>
#include <TH2F.h>
#include <TCanvas.h>
#include <TStyle.h>
#include <TH1F.h>
#include <iostream>
#include <iomanip>
#include "AtlasStyle.h"
#include "SumWeightTree.h"
#include <map>

using namespace std;

TH1F* ConvertDR(TH1F*h, string tag);
TH1F* Convert(TH1F*h, string tag);
TH1F* CombineHist(TH1F*h_stat, TH1F*h_sys, TH1F*h_tot);

string var;
bool fidu = true;
bool do1313 = false;
bool do1414 = false;
bool swap1314 = false;
bool doabs = false;
string chan;

int main(int argc, char * argv[]) {

    for (int i = 0; i < argc; i++) {
	if (!strcmp(argv[i],"--diff")) {
	    fidu = false;
   	}
	if (!strcmp(argv[i],"--chan")) {
	    chan = argv[i+1];
   	}
	if (!strcmp(argv[i],"--var")) {
	    var = argv[i+1];
   	}
	if (!strcmp(argv[i],"--1313")) {
	    do1313 = true;
   	}
	if (!strcmp(argv[i],"--1414")) {
	    do1414 = true;
   	}
	if (!strcmp(argv[i],"--swap1314")) {
	    swap1314 = true;
   	}
	if (!strcmp(argv[i],"--abs")) {
	    doabs = true;
   	}
    }

    SetAtlasStyle();
    gStyle->SetErrorX(0.0001);

    TH1F* h_13_stat = NULL;
    TH1F* h_13_sys = NULL;
    TH1F* h_13_tot = NULL;
    TH1F* h_14_stat = NULL;
    TH1F* h_14_stat_axis = NULL;
    TH1F* h_14_sys = NULL;
    TH1F* h_14_tot = NULL;

    double ljets_stat500 = 0;
    double ljets_sys500 = 0;
    double ljets_tot500 = 0;
    double emu_stat500 = 0;
    double emu_sys500 = 0;
    double emu_tot500 = 0;
    double ll_stat500 = 0;
    double ll_sys500 = 0;
    double ll_tot500 = 0;

    if (fidu) {
	h_13_stat = new TH1F("13_stat","",5,0,5); 
	h_13_sys = new TH1F("13_sys","",5,0,5); 
	h_13_tot = new TH1F("13_tot","",5,0,5); 
	h_14_stat = new TH1F("14_stat","",5,0,5); 
	h_14_sys = new TH1F("14_sys","",5,0,5); 
	h_14_tot = new TH1F("14_tot","",5,0,5); 

	h_13_stat->SetBinContent(1, 2.8);
    	h_13_stat->SetBinContent(2, 3.0);
    	h_13_stat->SetBinContent(3, 8.1);
    	h_13_stat->SetBinContent(4, 4.6);
    	h_13_stat->SetBinContent(5, 10);

    	h_13_tot->SetBinContent(1, 8.4);
    	h_13_tot->SetBinContent(2, 8.9);
    	h_13_tot->SetBinContent(3, 12);
    	h_13_tot->SetBinContent(4, 7.3);
    	h_13_tot->SetBinContent(5, 14);

    	h_13_sys->SetBinContent(1, sqrt(pow(h_13_tot->GetBinContent(1),2) - pow(h_13_stat->GetBinContent(1),2)));
    	h_13_sys->SetBinContent(2, sqrt(pow(h_13_tot->GetBinContent(2),2) - pow(h_13_stat->GetBinContent(2),2)));
    	h_13_sys->SetBinContent(3, sqrt(pow(h_13_tot->GetBinContent(3),2) - pow(h_13_stat->GetBinContent(3),2)));
    	h_13_sys->SetBinContent(4, sqrt(pow(h_13_tot->GetBinContent(4),2) - pow(h_13_stat->GetBinContent(4),2)));
    	h_13_sys->SetBinContent(5, sqrt(pow(h_13_tot->GetBinContent(5),2) - pow(h_13_stat->GetBinContent(5),2)));

	string run = "grep SigXsec /afs/cern.ch/work/y/yili/private/Analysis/TRexFitter/run_Stat/FitExample_*/Fits/* > /tmp/yili/log.stat";
	{
	    system(run.c_str());
	    ifstream ifile;
	    ifile.open("/tmp/yili/log.stat");
	    string line;
	    while (getline(ifile,line)) {
	        int n1 = line.find(" +", 0);
	        int n2 = line.find(" -", 0);
	        string substr = line.substr(n1+2, line.size()-n2-2);
		if (line.find("FitExample_ejets",0) != string::npos) h_14_stat->SetBinContent(1, 100*atof(substr.c_str())/1.02);
		if (line.find("FitExample_mujets",0) != string::npos) h_14_stat->SetBinContent(2, 100*atof(substr.c_str())/0.97);
		if (line.find("FitExample_ee",0) != string::npos) h_14_stat->SetBinContent(3, 100*atof(substr.c_str())/1.01);
		if (line.find("FitExample_emu",0) != string::npos) h_14_stat->SetBinContent(4, 100*atof(substr.c_str())/1.01);
		if (line.find("FitExample_mumu",0) != string::npos) h_14_stat->SetBinContent(5, 100*atof(substr.c_str())/1.01);
	    }
	}
	{
	    run = "grep SigXsec /afs/cern.ch/work/y/yili/private/Analysis/TRexFitter/run/FitExample_*/Fits/* > /tmp/yili/log.sys";
	    system(run.c_str());
	    ifstream ifile;
	    ifile.open("/tmp/yili/log.sys");
	    string line;
	    while (getline(ifile,line)) {
	        int n1 = line.find(" +", 0);
	        int n2 = line.find(" -", 0);
	        string substr = line.substr(n1+2, line.size()-n2-2);
		if (line.find("FitExample_ejets",0) != string::npos) h_14_tot->SetBinContent(1, 100*atof(substr.c_str())/1.02);
		if (line.find("FitExample_mujets",0) != string::npos) h_14_tot->SetBinContent(2, 100*atof(substr.c_str())/0.97);
		if (line.find("FitExample_ee",0) != string::npos) h_14_tot->SetBinContent(3, 100*atof(substr.c_str())/1.01);
		if (line.find("FitExample_emu",0) != string::npos) h_14_tot->SetBinContent(4, 100*atof(substr.c_str())/1.01);
		if (line.find("FitExample_mumu",0) != string::npos) h_14_tot->SetBinContent(5, 100*atof(substr.c_str())/1.01);
	    }
	}

    	h_14_sys->SetBinContent(1, sqrt(pow(h_14_tot->GetBinContent(1),2) - pow(h_14_stat->GetBinContent(1),2)));
    	h_14_sys->SetBinContent(2, sqrt(pow(h_14_tot->GetBinContent(2),2) - pow(h_14_stat->GetBinContent(2),2)));
    	h_14_sys->SetBinContent(3, sqrt(pow(h_14_tot->GetBinContent(3),2) - pow(h_14_stat->GetBinContent(3),2)));
    	h_14_sys->SetBinContent(4, sqrt(pow(h_14_tot->GetBinContent(4),2) - pow(h_14_stat->GetBinContent(4),2)));
    	h_14_sys->SetBinContent(5, sqrt(pow(h_14_tot->GetBinContent(5),2) - pow(h_14_stat->GetBinContent(5),2)));
    } else {
	string fname_13;
	if (do1414) {
	    if (doabs) fname_13 = "/afs/cern.ch/work/y/yili/private/Analysis/Unfolding_Upgrade/Results_Step2_pt1000/" + var + "_uncertainties_" + chan + "_3itr.root";
	    else fname_13 = "/afs/cern.ch/work/y/yili/private/Analysis/Unfolding_Upgrade/Results_Step2_pt1000/" + var + "_normalised_uncertainties_" + chan + "_3itr.root";
	}
	else fname_13 = "/afs/cern.ch/work/y/yili/private/Analysis/Unfolding/Results_Step2/" + var + "_normalised_uncertainties_" + chan + "_3itr.root";
	TFile* f_13 = new TFile(fname_13.c_str());
	string fname_14;
	if (do1313) fname_14 = "/afs/cern.ch/work/y/yili/private/Analysis/Unfolding_Optimised2/Results_Step2_pt1000/" + var + "_normalised_uncertainties_" + chan + "_3itr.root";
	else if (do1414) {
	    if (doabs) fname_14 = "/afs/cern.ch/work/y/yili/private/Analysis/Unfolding_Upgrade/Results_Step2_emuonly_pt1000/" + var + "_uncertainties_" + chan + "_3itr.root";
	    else fname_14 = "/afs/cern.ch/work/y/yili/private/Analysis/Unfolding_Upgrade/Results_Step2_emuonly_pt1000/" + var + "_normalised_uncertainties_" + chan + "_3itr.root";
	}
	else {
	    if (chan == "sinlepton") {
		fname_14 = "/afs/cern.ch/work/y/yili/private/Analysis/Unfolding_Upgrade/Results_Step2_pt1000/" + var + "_normalised_uncertainties_" + chan + "_3itr.root";
	    } else {
		fname_14 = "/afs/cern.ch/work/y/yili/private/Analysis/Unfolding_Upgrade/Results_Step2_emuonly_pt1000/" + var + "_normalised_uncertainties_" + chan + "_3itr.root";
	    }
	}
	TFile* f_14 = new TFile(fname_14.c_str());

	TH1F* h_13_stat_tmp = (TH1F*)f_13->Get("stat_rel");
	TH1F* h_13_sys1 = (TH1F*)f_13->Get("syst_up_rel");
	TH1F* h_13_sys2 = (TH1F*)f_13->Get("syst_down_rel");
	TH1F* h_13_tot1 = (TH1F*)f_13->Get("tot_up_rel");
	TH1F* h_13_tot2 = (TH1F*)f_13->Get("tot_down_rel");
	h_14_stat = (TH1F*)f_14->Get("stat_rel");
	h_14_stat_axis = (TH1F*)f_14->Get("stat_rel")->Clone();
	TH1F* h_14_sys1 = (TH1F*)f_14->Get("syst_up_rel");
	TH1F* h_14_sys2 = (TH1F*)f_14->Get("syst_down_rel");
	TH1F* h_14_tot1 = (TH1F*)f_14->Get("tot_up_rel");
	TH1F* h_14_tot2 = (TH1F*)f_14->Get("tot_down_rel");


	h_13_stat = (TH1F*)h_14_stat->Clone("stat_rel");
	for (int i = 0; i < h_13_stat_tmp->GetNbinsX(); i++) {
	    h_13_stat->SetBinContent(i+1, h_13_stat_tmp->GetBinContent(i+1));
	}
	h_13_sys = (TH1F*) h_14_sys1->Clone("syst_rel");
	h_13_tot = (TH1F*) h_14_tot1->Clone("tot_rel");
	for (int i = 0; i < h_13_sys1->GetNbinsX(); i++) {
	    double sys1 = fabs(h_13_sys1->GetBinContent(i+1));
	    double sys2 = fabs(h_13_sys2->GetBinContent(i+1));
	    double sys = sys1 > sys2 ? sys1 : sys2;
	    h_13_sys->SetBinContent(i+1, sys);
	}
	for (int i = 0; i < h_13_tot1->GetNbinsX(); i++) {
	    double tot1 = fabs(h_13_tot1->GetBinContent(i+1));
	    double tot2 = fabs(h_13_tot2->GetBinContent(i+1));
	    double tot = tot1 > tot2 ? tot1 : tot2;
	    h_13_tot->SetBinContent(i+1, tot);
	}

	h_14_sys = (TH1F*) h_14_sys1->Clone("syst_rel");
	h_14_tot = (TH1F*) h_14_tot1->Clone("tot_rel");
	for (int i = 0; i < h_14_sys1->GetNbinsX(); i++) {
	    double sys1 = fabs(h_14_sys1->GetBinContent(i+1));
	    double sys2 = fabs(h_14_sys2->GetBinContent(i+1));
	    double sys = sys1 > sys2 ? sys1 : sys2;
	    h_14_sys->SetBinContent(i+1, sys);
	}
	for (int i = 0; i < h_14_tot1->GetNbinsX(); i++) {
	    double tot1 = fabs(h_14_tot1->GetBinContent(i+1));
	    double tot2 = fabs(h_14_tot2->GetBinContent(i+1));
	    double tot = tot1 > tot2 ? tot1 : tot2;
	    h_14_tot->SetBinContent(i+1, tot);
	}

	//if (do1313 && var == "dR_lep_ph") {
	    h_13_stat = Convert((TH1F*)h_13_stat->Clone(), "13");
	    h_13_sys = Convert((TH1F*)h_13_sys->Clone(), "13");
	    h_13_tot = Convert((TH1F*)h_13_tot->Clone(), "13");
	    h_14_stat = Convert((TH1F*)h_14_stat->Clone(), "14");
	    h_14_sys = Convert((TH1F*)h_14_sys->Clone(), "14");
	    h_14_tot = Convert((TH1F*)h_14_tot->Clone(), "14");
	//}
    }

    for (int i = 0; i < h_13_stat->GetNbinsX(); i++) {
	h_13_stat->SetBinError(i+1, 0);
    }
    for (int i = 0; i < h_13_sys->GetNbinsX(); i++) {
	h_13_sys->SetBinError(i+1, 0);
    }
    for (int i = 0; i < h_13_tot->GetNbinsX(); i++) {
	h_13_tot->SetBinError(i+1, 0);
    }
    for (int i = 0; i < h_14_stat->GetNbinsX(); i++) {
	h_14_stat->SetBinError(i+1, 0);
    }
    for (int i = 0; i < h_14_sys->GetNbinsX(); i++) {
	h_14_sys->SetBinError(i+1, 0);
    }
    for (int i = 0; i < h_14_tot->GetNbinsX(); i++) {
	h_14_tot->SetBinError(i+1, 0);
    }

    TCanvas *c = new TCanvas("c","c",800,600);
    c->SetLeftMargin(0.15);
    c->SetRightMargin(0.10);
    //c->SetLogy(true);
    
    cout << "tag1" << endl;
    TLegend *lg;
    if (fidu) lg = new TLegend(0.20, 0.69, 0.85, 0.79);
    else lg = new TLegend(0.18, 0.64, 0.85, 0.74);

    lg->SetNColumns(3);
    lg->SetTextSize(0.04);
    if (!fidu) lg->SetTextSize(0.035);
    lg->SetTextFont(42);
    lg->SetBorderSize(0);
    lg->SetFillColor(0);
    
    //h_13_stat->SetMarkerStyle(34);
    //h_13_sys->SetMarkerStyle(20);
    //h_13_tot->SetMarkerStyle(23);
    //
    //h_13_stat->SetMarkerSize(1.3);
    //h_13_sys->SetMarkerSize(1.3);
    //h_13_tot->SetMarkerSize(1.3);
    //
    //h_13_stat->SetMarkerColor(2);
    //h_13_sys->SetMarkerColor(3);
    //h_13_tot->SetMarkerColor(1);
    //
    //h_13_stat->SetLineColor(2);
    //h_13_sys->SetLineColor(3);
    //h_13_tot->SetLineColor(1);
    
    //h_14_stat->SetMarkerStyle(28);
    //h_14_sys->SetMarkerStyle(24);
    //h_14_tot->SetMarkerStyle(32);
    //
    //h_14_stat->SetMarkerSize(1.3);
    //h_14_sys->SetMarkerSize(1.3);
    //h_14_tot->SetMarkerSize(1.3);
    //
    //h_14_stat->SetMarkerColor(2);
    //h_14_sys->SetMarkerColor(3);
    //h_14_tot->SetMarkerColor(1);
    //
    //h_14_stat->SetLineColor(2);
    //h_14_sys->SetLineColor(3);
    //h_14_tot->SetLineColor(1);
    //
    //h_14_stat->SetLineStyle(2);
    //h_14_sys->SetLineStyle(2);
    //h_14_tot->SetLineStyle(2);
    
    cout << "tag2" << endl;
    if (fidu) {
        h_14_stat->GetXaxis()->SetBinLabel(1, "e+jets");
        h_14_stat->GetXaxis()->SetBinLabel(2, "#mu+jets");
        h_14_stat->GetXaxis()->SetBinLabel(3, "ee");
        h_14_stat->GetXaxis()->SetBinLabel(4, "e#mu");
        h_14_stat->GetXaxis()->SetBinLabel(5, "#mu#mu");
    }
    cout << "tag3" << endl;
    h_14_stat->GetYaxis()->SetRangeUser(0,h_13_tot->GetMaximum()*2.0);
    h_14_stat->GetYaxis()->SetTitle("Uncertainty (%)");
    h_14_stat->GetYaxis()->SetTitleOffset(1.0);
    if (fidu) {
	h_14_stat->GetXaxis()->SetTitle("Channel");
    } else {
	if (var == "ph_pt") {
	    h_14_stat->GetXaxis()->SetTitle("p_{T}(#gamma) [GeV]");
	}
	if (var == "ph_eta") {
	    h_14_stat->GetXaxis()->SetTitle("|#eta(#gamma)|");
	}
	if (var == "dR_lep_ph" && chan == "sinlepton") {
	    h_14_stat->GetXaxis()->SetTitle("#DeltaR(#gamma,#it{l})");
	}
	if (var == "dR_lep_ph" && chan == "dilepton") {
	    h_14_stat->GetXaxis()->SetTitle("#DeltaR(#gamma,#it{l})_{min}");
	}
	if (var == "dEta_lep") {
	    h_14_stat->GetXaxis()->SetTitle("|#Delta#eta(#it{l},#it{l})|");
	}
	if (var == "dPhi_lep") {
	    h_14_stat->GetXaxis()->SetTitle("#Delta#phi(#it{l},#it{l})");
	}
    }
    if (fidu) {
	h_14_stat->GetXaxis()->SetNdivisions(505);
	h_14_stat->GetXaxis()->SetLabelSize(0.08);
    }
    h_14_stat->GetXaxis()->SetTitleOffset(1.4);
    cout << "tag4" << endl;

    h_14_stat->SetLineColor(0); h_14_stat->SetLineWidth(0);
    h_14_sys->SetLineColor(0); h_14_sys->SetLineWidth(0);
    h_14_tot->SetLineColor(0); h_14_tot->SetLineWidth(0);
    h_13_stat->SetLineColor(0); h_13_stat->SetLineWidth(0);
    h_13_sys->SetLineColor(0); h_13_sys->SetLineWidth(0);
    h_13_tot->SetLineColor(0); h_13_tot->SetLineWidth(0);

    h_14_stat->SetStats(0);
    h_14_sys->SetStats(0);
    h_14_tot->SetStats(0);
    h_13_stat->SetStats(0);
    h_13_sys->SetStats(0);
    h_13_tot->SetStats(0);

    h_14_stat->SetFillColor(kGreen+2);
    h_13_stat->SetFillColor(kGreen-9);
    h_14_sys->SetFillColor(kBlue);
    h_13_sys->SetFillColor(kBlue-9);
    h_14_tot->SetFillColor(kRed);
    h_13_tot->SetFillColor(kRed-9);
    
    h_13_stat->SetBarWidth(0.27);
    h_14_stat->SetBarWidth(0.27);
    h_13_sys->SetBarWidth(0.27);
    h_14_sys->SetBarWidth(0.27);
    h_13_tot->SetBarWidth(0.27);
    h_14_tot->SetBarWidth(0.27);
    cout << "tag5" << endl;

    h_13_stat->SetBarOffset(0.095);
    h_14_stat->SetBarOffset(0.095);
     h_13_sys->SetBarOffset(0.095+0.27);
     h_14_sys->SetBarOffset(0.095+0.27);
     h_13_tot->SetBarOffset(0.095+0.27*2);
     h_14_tot->SetBarOffset(0.095+0.27*2);

    //h_13_stat->SetBarWidth(0.14);
    //h_14_stat->SetBarWidth(0.14);
    //h_13_sys->SetBarWidth(0.14);
    //h_14_sys->SetBarWidth(0.14);
    //h_13_tot->SetBarWidth(0.14);
    //h_14_tot->SetBarWidth(0.14);

    //h_13_stat->SetBarOffset(0.08);
    //h_14_stat->SetBarOffset(0.22);
    // h_13_sys->SetBarOffset(0.36);
    // h_14_sys->SetBarOffset(0.50);
    // h_13_tot->SetBarOffset(0.64);
    // h_14_tot->SetBarOffset(0.78);

    if (!fidu) {
	for (int i = 0; i< h_14_stat->GetNbinsX(); i++) {
	    char binlab[50];
	    sprintf(binlab, "Bin-%d", i+1);
    	    h_14_stat->GetXaxis()->SetBinLabel(i+1, "");
    	}
	h_14_stat->GetXaxis()->SetLabelSize(0.0000001);
    }

    cout << "tag6" << endl;
    h_14_stat->Draw("bar");
    if (do1414) {
	if (swap1314) {
	    h_14_stat->Draw("same bar"); lg->AddEntry(h_14_stat, "14 TeV stat.(emu)", "F");
    	    h_14_sys->Draw("same bar"); lg->AddEntry(h_14_sys, "14 TeV sys.(emu)", "F");
    	    h_14_tot->Draw("same bar"); lg->AddEntry(h_14_tot, "14 TeV tot.(emu)", "F");
	} else {
	    h_13_stat->Draw("same bar"); lg->AddEntry(h_13_stat, "14 TeV stat.", "F");
    	    h_13_sys->Draw("same bar"); lg->AddEntry(h_13_sys, "14 TeV sys.", "F");
    	    h_13_tot->Draw("same bar"); lg->AddEntry(h_13_tot, "14 TeV tot.", "F");
	}
    } else {
        h_13_stat->Draw("same bar"); lg->AddEntry(h_13_stat, "13 TeV stat.", "F");
    	h_13_sys->Draw("same bar"); lg->AddEntry(h_13_sys, "13 TeV sys.", "F");
    	h_13_tot->Draw("same bar"); lg->AddEntry(h_13_tot, "13 TeV tot.", "F");
    }
    if (do1313) {
        h_14_stat->Draw("same bar"); lg->AddEntry(h_14_stat, "13 TeV stat.(R)", "F");
    	h_14_sys->Draw("same bar"); lg->AddEntry(h_14_sys, "13 TeV sys.(R)", "F");
    	h_14_tot->Draw("same bar"); lg->AddEntry(h_14_tot, "13 TeV tot.(R)", "F");
    } else if (do1414) {
	if (swap1314) {
	    h_13_stat->Draw("same bar"); lg->AddEntry(h_13_stat, "14 TeV stat.", "F");
    	    h_13_sys->Draw("same bar"); lg->AddEntry(h_13_sys, "14 TeV sys.", "F");
    	    h_13_tot->Draw("same bar"); lg->AddEntry(h_13_tot, "14 TeV tot.", "F");
	} else {
	    h_14_stat->Draw("same bar"); lg->AddEntry(h_14_stat, "14 TeV stat.(emu)", "F");
    	    h_14_sys->Draw("same bar"); lg->AddEntry(h_14_sys, "14 TeV sys.(emu)", "F");
    	    h_14_tot->Draw("same bar"); lg->AddEntry(h_14_tot, "14 TeV tot.(emu)", "F");
	}
    } else {
        h_14_stat->Draw("same bar"); lg->AddEntry(h_14_stat, "14 TeV stat.", "F");
    	h_14_sys->Draw("same bar"); lg->AddEntry(h_14_sys, "14 TeV sys.", "F");
    	h_14_tot->Draw("same bar"); lg->AddEntry(h_14_tot, "14 TeV tot.", "F");
    }
    lg->Draw("same");

    cout << "tag7" << endl;
    if (!fidu) {
    TText *ttext = new TText();
    ttext->SetTextSize(0.05);
    ttext->SetTextAlign(22);
    double y_pos = -0.04*h_14_stat->GetMaximum();
    for (int bin_x = 1; bin_x < h_14_stat_axis->GetNbinsX()+1; bin_x++) {
	char new_label[50];
        if (var.find("pt",0) != string::npos) {
	    sprintf(new_label, "%.0f", h_14_stat_axis->GetXaxis()->GetBinLowEdge(bin_x));
        } else if (var.find("dPhi",0) != string::npos) {
	    sprintf(new_label, "%.2f", h_14_stat_axis->GetXaxis()->GetBinLowEdge(bin_x));
        } else {
	    sprintf(new_label, "%.1f", h_14_stat_axis->GetXaxis()->GetBinLowEdge(bin_x));
	}
	if (h_14_stat_axis->GetXaxis()->GetBinLowEdge(bin_x) == 0)
	    sprintf(new_label, "0");
	if (int(h_14_stat_axis->GetXaxis()->GetBinLowEdge(bin_x)*10)%10 == 0)
	    sprintf(new_label, "%.0f", h_14_stat_axis->GetXaxis()->GetBinLowEdge(bin_x));
	ttext->DrawText(h_14_stat->GetXaxis()->GetBinLowEdge(bin_x),y_pos, new_label);

	if (bin_x == h_14_stat_axis->GetNbinsX()) {
	    char new_label2[50];
	    if (var.find("pt",0) != string::npos) {
	        sprintf(new_label2, "%.0f", h_14_stat_axis->GetXaxis()->GetBinLowEdge(bin_x)+h_14_stat_axis->GetXaxis()->GetBinWidth(bin_x));
            } else if (var.find("dPhi",0) != string::npos) {
	        sprintf(new_label2, "%.2f", h_14_stat_axis->GetXaxis()->GetBinLowEdge(bin_x)+h_14_stat_axis->GetXaxis()->GetBinWidth(bin_x));
            } else {
	        sprintf(new_label2, "%.1f", h_14_stat_axis->GetXaxis()->GetBinLowEdge(bin_x)+h_14_stat_axis->GetXaxis()->GetBinWidth(bin_x));
	    }
	    if (int((h_14_stat_axis->GetXaxis()->GetBinLowEdge(bin_x)+h_14_stat_axis->GetXaxis()->GetBinWidth(bin_x))*10)%10 == 0)
	        sprintf(new_label2, "%.0f", h_14_stat_axis->GetXaxis()->GetBinLowEdge(bin_x)+h_14_stat_axis->GetXaxis()->GetBinWidth(bin_x));
	    ttext->DrawText(h_14_stat->GetXaxis()->GetBinLowEdge(bin_x)+h_14_stat->GetXaxis()->GetBinWidth(bin_x),y_pos, new_label2);
	}
    }}
    cout << "tag8" << endl;
    
    TLatex lt; 
    lt.SetNDC();
    lt.SetTextFont(72);
    lt.SetTextSize(0.04);
    lt.DrawLatex(0.20, 0.88, "ATLAS");
    lt.SetTextFont(42);
    lt.DrawLatex(0.3, 0.88, "Simulation Preliminary");
    if (do1313) {
	lt.DrawLatex(0.20, 0.82, "#sqrt{s} = 13 TeV, 36 fb^{-1} ");
    } else if (do1414) {
	lt.DrawLatex(0.20, 0.82, "#sqrt{s} = 14 TeV, 3 ab^{-1} ");
    } else lt.DrawLatex(0.20, 0.82, "#sqrt{s} = 14 TeV, 3 ab^{-1} v.s. #sqrt{s} = 13 TeV, 36 fb^{-1} ");
    if (!fidu) {
	string channel;
	if (chan == "sinlepton") channel = "Single lepton";
	else channel = "Dilepotn";
	if (!doabs) channel += ", normalised";
	else channel += ", absolute";
	lt.DrawLatex(0.20, 0.76, channel.c_str());
    }

    gPad->RedrawAxis();
    cout << "tag9" << endl;
    
    string savetag;
    if (fidu) savetag = "plots/Upgrade/Uncert_comparison";
    else savetag = "plots/Upgrade/Uncert_comparison_diff_" + var + "_" + chan;
    if (do1313) savetag += "_1313";
    if (do1414) savetag += "_1414";
    if (swap1314) savetag += "_swap";
    if (doabs) savetag += "_abs";
    c->SaveAs(string(savetag + ".png").c_str());
    c->SaveAs(string(savetag + ".pdf").c_str());

    //TH1F* h_13 = CombineHist(h_13_stat, h_13_sys, h_13_tot);
    //TH1F* h_14 = CombineHist(h_14_stat, h_14_sys, h_14_tot);

    //h_13->SetMarkerStyle(34);
    //h_13->SetMarkerSize(1.3);
    //h_13->SetMarkerColor(2);
    //h_13->SetLineColor(2);
    //h_14->SetMarkerStyle(28);
    //h_14->SetMarkerSize(1.3);
    //h_14->SetMarkerColor(2);
    //h_14->SetLineColor(2);
    //h_14->SetLineStyle(2);
    //if (fidu) {
    //    h_14->GetXaxis()->SetBinLabel(1, "ejets");
    //    h_14->GetXaxis()->SetBinLabel(2, "mujets");
    //    h_14->GetXaxis()->SetBinLabel(3, "ee");
    //    h_14->GetXaxis()->SetBinLabel(4, "emu");
    //    h_14->GetXaxis()->SetBinLabel(5, "mumu");
    //}
    //h_14->GetYaxis()->SetRangeUser(0,h_13_tot->GetMaximum()*2.0);
    //h_14->GetYaxis()->SetTitle("Uncertainty (%)");
    //h_14->GetYaxis()->SetTitleOffset(1.0);
    //if (fidu) {
    //    h_14->GetXaxis()->SetTitle("Channel");
    //} else {
    //    if (var == "ph_pt") {
    //        h_14->GetXaxis()->SetTitle("p_{T}(#gamma) [GeV]");
    //    }
    //    if (var == "ph_eta") {
    //        h_14->GetXaxis()->SetTitle("|#eta(#gamma)|");
    //    }
    //    if (var == "dR_lep_ph" && chan == "sinlepton") {
    //        h_14->GetXaxis()->SetTitle("#DeltaR(#gamma,#it{l})");
    //    }
    //    if (var == "dR_lep_ph" && chan == "dilepton") {
    //        h_14->GetXaxis()->SetTitle("#DeltaR(#gamma,#it{l})_{min}");
    //    }
    //    if (var == "dEta_lep") {
    //        h_14->GetXaxis()->SetTitle("|#Delta#eta(#it{l},#it{l})|");
    //    }
    //    if (var == "dPhi_lep") {
    //        h_14->GetXaxis()->SetTitle("#Delta#phi(#it{l},#it{l})");
    //    }
    //}
    //if (fidu) {
    //    h_14->GetXaxis()->SetNdivisions(505);
    //    h_14->GetXaxis()->SetLabelSize(0.08);
    //}
    //h_14->GetXaxis()->SetTitleOffset(1.4);
    //h_14->Draw("E P");

    //TLegend *lg2;
    //if (fidu) lg2 = new TLegend(0.20, 0.69, 0.85, 0.79);
    //else lg2 = new TLegend(0.18, 0.64, 0.85, 0.74);
    //lg2->SetNColumns(3);
    //lg2->SetTextSize(0.04);
    //if (!fidu) lg2->SetTextSize(0.035);
    //lg2->SetTextFont(42);
    //lg2->SetBorderSize(0);
    //lg2->SetFillColor(0);
    //if (do1414) {
    //    h_13->Draw("same E P"); lg2->AddEntry(h_13, "14 TeV", "P");
    //} else {
    //    h_13->Draw("same E P"); lg2->AddEntry(h_13, "13 TeV", "P");
    //}
    //if (do1313) {
    //    h_14->Draw("same E P"); lg2->AddEntry(h_14, "13 TeV(R)", "P");
    //} else if (do1414) {
    //    h_14->Draw("same E P"); lg2->AddEntry(h_14, "14 TeV(emu)", "P");
    //} else {
    //    h_14->Draw("same E P"); lg2->AddEntry(h_14, "14 TeV", "P");
    //}
    //lg2->Draw("same");

    //lt.DrawLatex(0.20, 0.88, "ATLAS");
    //lt.SetTextFont(42);
    //lt.DrawLatex(0.3, 0.88, "Internal Simulation");
    //if (do1313) {
    //    lt.DrawLatex(0.20, 0.82, "#sqrt{s}=13TeV, 36 fb^{-1} ");
    //} else if (do1414) {
    //    lt.DrawLatex(0.20, 0.82, "#sqrt{s}=14TeV, 3000 fb^{-1} ");
    //} else lt.DrawLatex(0.20, 0.82, "#sqrt{s}=14TeV, 3000 fb^{-1} v.s. #sqrt{s}=13TeV, 36 fb^{-1} ");
    //if (!fidu) {
    //    string channel;
    //    if (chan == "sinlepton") channel = "Single lepton";
    //    else channel = "Dilepotn";
    //    if (!doabs) channel += ", normalised";
    //    else channel += ", absolute";
    //    lt.DrawLatex(0.20, 0.76, channel.c_str());
    //}
    //
    //if (fidu) savetag = "plots/Upgrade/Uncert_comparison2";
    //else savetag = "plots/Upgrade/Uncert_comparison2_diff_" + var + "_" + chan;
    //if (do1313) savetag += "_1313";
    //if (do1414) savetag += "_1414";
    //if (doabs) savetag += "_abs";
    //c->SaveAs(string(savetag + ".png").c_str());
    //c->SaveAs(string(savetag + ".pdf").c_str());
    
    if (fidu) {
	cout << fixed << setprecision(1) << endl;
	cout << "\\multirow{2}{*}{Stat.}" << endl;
	cout << setw(10) << "& HL-LHC & " 
	     << setw(10) << h_14_stat->GetBinContent(1) << "\\%" << " & "
	     << setw(10) << h_14_stat->GetBinContent(2) << "\\%" << " & "
	     << setw(10) << h_14_stat->GetBinContent(3) << "\\%" << " & "
	     << setw(10) << h_14_stat->GetBinContent(4) << "\\%" << " & "
	     << setw(10) << h_14_stat->GetBinContent(5) << "\\%" << " \\\\ "
	     << endl;
	cout << "\\cline{2-7}" << endl;
	cout << setw(10) << "& Run-2 (36 fb$^{-1}$) & " 
	     << setw(10) << h_13_stat->GetBinContent(1) << "\\%" << " & "
	     << setw(10) << h_13_stat->GetBinContent(2) << "\\%" << " & "
	     << setw(10) << h_13_stat->GetBinContent(3) << "\\%" << " & "
	     << setw(10) << h_13_stat->GetBinContent(4) << "\\%" << " & "
	     << setw(10) << h_13_stat->GetBinContent(5) << "\\%" << " \\\\ "
	     << endl;
	cout << "\\hline" << endl;
	cout << "\\multirow{2}{*}{Sys.}" << endl;
	cout << setw(10) << "& HL-LHC & " 
	     << setw(10) << h_14_sys->GetBinContent(1) << "\\%" << " & "
	     << setw(10) << h_14_sys->GetBinContent(2) << "\\%" << " & "
	     << setw(10) << h_14_sys->GetBinContent(3) << "\\%" << " & "
	     << setw(10) << h_14_sys->GetBinContent(4) << "\\%" << " & "
	     << setw(10) << h_14_sys->GetBinContent(5) << "\\%" << " \\\\ "
	     << endl;
	cout << "\\cline{2-7}" << endl;
	cout << setw(10) << "& Run-2 (36 fb$^{-1}$) & " 
	     << setw(10) << h_13_sys->GetBinContent(1) << "\\%" << " & "
	     << setw(10) << h_13_sys->GetBinContent(2) << "\\%" << " & "
	     << setw(10) << h_13_sys->GetBinContent(3) << "\\%" << " & "
	     << setw(10) << h_13_sys->GetBinContent(4) << "\\%" << " & "
	     << setw(10) << h_13_sys->GetBinContent(5) << "\\%" << " \\\\ "
	     << endl;
	cout << "\\hline" << endl;
	cout << "\\multirow{2}{*}{Total}" << endl;
	cout << setw(10) << "& HL-LHC & " 
	     << setw(10) << h_14_tot->GetBinContent(1) << "\\%" << " & "
	     << setw(10) << h_14_tot->GetBinContent(2) << "\\%" << " & "
	     << setw(10) << h_14_tot->GetBinContent(3) << "\\%" << " & "
	     << setw(10) << h_14_tot->GetBinContent(4) << "\\%" << " & "
	     << setw(10) << h_14_tot->GetBinContent(5) << "\\%" << " \\\\ "
	     << endl;
	cout << "\\cline{2-7}" << endl;
	cout << setw(10) << "& Run-2 (36 fb$^{-1}$) & " 
	     << setw(10) << h_13_tot->GetBinContent(1) << "\\%" << " & "
	     << setw(10) << h_13_tot->GetBinContent(2) << "\\%" << " & "
	     << setw(10) << h_13_tot->GetBinContent(3) << "\\%" << " & "
	     << setw(10) << h_13_tot->GetBinContent(4) << "\\%" << " & "
	     << setw(10) << h_13_tot->GetBinContent(5) << "\\%" << " \\\\ "
	     << endl;

	ofstream ofile;
	ofile.open("log_fidu");
	ofile << fixed << setprecision(1) << endl;
	ofile << "\\multirow{2}{*}{Stat.}" << endl;
	ofile << setw(10) << "& HL-LHC & " 
	     << setw(10) << h_14_stat->GetBinContent(1) << "\\%" << " & "
	     << setw(10) << h_14_stat->GetBinContent(2) << "\\%" << " & "
	     << setw(10) << h_14_stat->GetBinContent(3) << "\\%" << " & "
	     << setw(10) << h_14_stat->GetBinContent(4) << "\\%" << " & "
	     << setw(10) << h_14_stat->GetBinContent(5) << "\\%" << " \\\\ "
	     << endl;
	ofile << "\\cline{2-7}" << endl;
	ofile << setw(10) << "& Run-2 (36 fb$^{-1}$) & " 
	     << setw(10) << h_13_stat->GetBinContent(1) << "\\%" << " & "
	     << setw(10) << h_13_stat->GetBinContent(2) << "\\%" << " & "
	     << setw(10) << h_13_stat->GetBinContent(3) << "\\%" << " & "
	     << setw(10) << h_13_stat->GetBinContent(4) << "\\%" << " & "
	     << setw(10) << h_13_stat->GetBinContent(5) << "\\%" << " \\\\ "
	     << endl;
	ofile << "\\hline" << endl;
	ofile << "\\multirow{2}{*}{Sys.}" << endl;
	ofile << setw(10) << "& HL-LHC & " 
	     << setw(10) << h_14_sys->GetBinContent(1) << "\\%" << " & "
	     << setw(10) << h_14_sys->GetBinContent(2) << "\\%" << " & "
	     << setw(10) << h_14_sys->GetBinContent(3) << "\\%" << " & "
	     << setw(10) << h_14_sys->GetBinContent(4) << "\\%" << " & "
	     << setw(10) << h_14_sys->GetBinContent(5) << "\\%" << " \\\\ "
	     << endl;
	ofile << "\\cline{2-7}" << endl;
	ofile << setw(10) << "& Run-2 (36 fb$^{-1}$) & " 
	     << setw(10) << h_13_sys->GetBinContent(1) << "\\%" << " & "
	     << setw(10) << h_13_sys->GetBinContent(2) << "\\%" << " & "
	     << setw(10) << h_13_sys->GetBinContent(3) << "\\%" << " & "
	     << setw(10) << h_13_sys->GetBinContent(4) << "\\%" << " & "
	     << setw(10) << h_13_sys->GetBinContent(5) << "\\%" << " \\\\ "
	     << endl;
	ofile << "\\hline" << endl;
	ofile << "\\multirow{2}{*}{Total}" << endl;
	ofile << setw(10) << "& HL-LHC & " 
	     << setw(10) << h_14_tot->GetBinContent(1) << "\\%" << " & "
	     << setw(10) << h_14_tot->GetBinContent(2) << "\\%" << " & "
	     << setw(10) << h_14_tot->GetBinContent(3) << "\\%" << " & "
	     << setw(10) << h_14_tot->GetBinContent(4) << "\\%" << " & "
	     << setw(10) << h_14_tot->GetBinContent(5) << "\\%" << " \\\\ "
	     << endl;
	ofile << "\\cline{2-7}" << endl;
	ofile << setw(10) << "& Run-2 (36 fb$^{-1}$) & " 
	     << setw(10) << h_13_tot->GetBinContent(1) << "\\%" << " & "
	     << setw(10) << h_13_tot->GetBinContent(2) << "\\%" << " & "
	     << setw(10) << h_13_tot->GetBinContent(3) << "\\%" << " & "
	     << setw(10) << h_13_tot->GetBinContent(4) << "\\%" << " & "
	     << setw(10) << h_13_tot->GetBinContent(5) << "\\%" << " \\\\ "
	     << endl;
	ofile.close();
    } else {
	cout << fixed << setprecision(1) << endl;
	cout << "\\multirow{2}{*}{Stat.}" << endl;
	cout << setw(10) << "& HL-LHC & ";
	for (int i = 0; i < h_14_stat->GetNbinsX(); i++) {
	     if (i != h_14_stat->GetNbinsX()-1) cout << setw(10) << h_14_stat->GetBinContent(i+1) << "\\%" << " & ";
	     else cout << setw(10) << h_14_stat->GetBinContent(i+1) << "\\%" << " \\\\ "<< endl;
	     if (do1414 && doabs && chan == "dilepton" && i == h_14_stat->GetNbinsX()-1) {
		 emu_stat500 = h_14_stat->GetBinContent(i+1);
	     }
	     if (!do1414 && !do1313 && doabs && chan == "sinlepton" && i == h_14_stat->GetNbinsX()-1) {
		 ljets_stat500 = h_14_stat->GetBinContent(i+1);
	     }
	}
	cout << "\\cline{2-7}" << endl;
	cout << setw(10) << "& Run-2 (36 fb$^{-1}$) & "; 
	for (int i = 0; i < h_13_stat->GetNbinsX(); i++) {
	     if (i != h_13_stat->GetNbinsX()-1) cout << setw(10) << h_13_stat->GetBinContent(i+1) << "\\%" << " & ";
	     else cout << setw(10) << h_13_stat->GetBinContent(i+1) << "\\%" << " \\\\ "<< endl;
	     if (do1414 && doabs && chan == "dilepton" && i == h_13_stat->GetNbinsX()-1) {
		 ll_stat500 = h_13_stat->GetBinContent(i+1);
	     }
	}
	cout << "\\cline{2-7}" << endl;
	cout << setw(10) << "& Ratio & "; 
	for (int i = 0; i < h_13_stat->GetNbinsX(); i++) {
	     if (i != h_13_stat->GetNbinsX()-1) cout << setw(10) << h_13_stat->GetBinContent(i+1)/h_14_stat->GetBinContent(i+1) << "\\%" << " & ";
	     else cout << setw(10) << h_13_stat->GetBinContent(i+1)/h_14_stat->GetBinContent(i+1) << "\\%" << " \\\\ "<< endl;
	}
	cout << "\\hline" << endl;
	cout << "\\multirow{2}{*}{Sys.}" << endl;
	cout << setw(10) << "& HL-LHC & ";
	for (int i = 0; i < h_14_sys->GetNbinsX(); i++) {
	     if (i != h_14_sys->GetNbinsX()-1) cout << setw(10) << h_14_sys->GetBinContent(i+1) << "\\%" << " & ";
	     else cout << setw(10) << h_14_sys->GetBinContent(i+1) << "\\%" << " \\\\ "<< endl;
	     if (do1414 && doabs && chan == "dilepton" && i == h_14_sys->GetNbinsX()-1) {
		 emu_sys500 = h_14_sys->GetBinContent(i+1);
	     }
	     if (!do1414 && !do1313 && doabs && chan == "sinlepton" && i == h_14_sys->GetNbinsX()-1) {
		 ljets_sys500 = h_14_sys->GetBinContent(i+1);
	     }
	}
	cout << "\\cline{2-7}" << endl;
	cout << setw(10) << "& Run-2 (36 fb$^{-1}$) & "; 
	for (int i = 0; i < h_13_sys->GetNbinsX(); i++) {
	     if (i != h_13_sys->GetNbinsX()-1) cout << setw(10) << h_13_sys->GetBinContent(i+1) << "\\%" << " & ";
	     else cout << setw(10) << h_13_sys->GetBinContent(i+1) << "\\%" << " \\\\ "<< endl;
	     if (do1414 && doabs && chan == "dilepton" && i == h_13_sys->GetNbinsX()-1) {
		 ll_sys500 = h_13_sys->GetBinContent(i+1);
	     }
	}
	cout << "\\cline{2-7}" << endl;
	cout << setw(10) << "& Ratio & "; 
	for (int i = 0; i < h_13_sys->GetNbinsX(); i++) {
	     if (i != h_13_sys->GetNbinsX()-1) cout << setw(10) << h_13_sys->GetBinContent(i+1)/h_14_sys->GetBinContent(i+1) << "\\%" << " & ";
	     else cout << setw(10) << h_13_sys->GetBinContent(i+1)/h_14_sys->GetBinContent(i+1) << "\\%" << " \\\\ "<< endl;
	}
	cout << "\\hline" << endl;
	cout << "\\multirow{2}{*}{Total}" << endl;
	cout << setw(10) << "& HL-LHC & ";
	for (int i = 0; i < h_14_tot->GetNbinsX(); i++) {
	     if (i != h_14_tot->GetNbinsX()-1) cout << setw(10) << h_14_tot->GetBinContent(i+1) << "\\%" << " & ";
	     else cout << setw(10) << h_14_tot->GetBinContent(i+1) << "\\%" << " \\\\ "<< endl;
	     if (do1414 && doabs && chan == "dilepton" && i == h_14_tot->GetNbinsX()-1) {
		 emu_tot500 = h_14_tot->GetBinContent(i+1);
	     }
	     if (!do1414 && !do1313 && doabs && chan == "sinlepton" && i == h_14_tot->GetNbinsX()-1) {
		 ljets_tot500 = h_14_tot->GetBinContent(i+1);
	     }
	}
	cout << "\\cline{2-7}" << endl;
	cout << setw(10) << "& Run-2 (36 fb$^{-1}$) & "; 
	for (int i = 0; i < h_13_tot->GetNbinsX(); i++) {
	     if (i != h_13_tot->GetNbinsX()-1) cout << setw(10) << h_13_tot->GetBinContent(i+1) << "\\%" << " & ";
	     else cout << setw(10) << h_13_tot->GetBinContent(i+1) << "\\%" << " \\\\ "<< endl;
	     if (do1414 && doabs && chan == "dilepton" && i == h_13_tot->GetNbinsX()-1) {
		 ll_tot500 = h_13_tot->GetBinContent(i+1);
	     }
	}
	cout << "\\cline{2-7}" << endl;
	cout << setw(10) << "& Ratio & "; 
	for (int i = 0; i < h_13_tot->GetNbinsX(); i++) {
	     if (i != h_13_tot->GetNbinsX()-1) cout << setw(10) << h_13_tot->GetBinContent(i+1)/h_14_tot->GetBinContent(i+1) << "\\%" << " & ";
	     else cout << setw(10) << h_13_tot->GetBinContent(i+1)/h_14_tot->GetBinContent(i+1) << "\\%" << " \\\\ "<< endl;
	}
    }

    if (do1414 && doabs && chan == "dilepton" && var == "ph_pt") {
	cout << fixed << setprecision(1);
	cout << setw(20) << "\\chemu" << " & ";
	cout << setw(10) << emu_stat500 << "\\% & " << setw(10) << emu_sys500 << "\\% & " << setw(10) << emu_tot500 << "\\% \\\\" << endl;
	cout << setw(20) << "\\chll" << " & ";
	cout << setw(10) << ll_stat500 << "\\% & " << setw(10) << ll_sys500 << "\\% & " << setw(10) << ll_tot500 << "\\% \\\\" << endl;

	ofstream ofile;
	ofile.open("log_500emu");
	ofile << fixed << setprecision(1);
	ofile << setw(20) << "\\chemu" << " & ";
	ofile << setw(10) << emu_stat500 << "\\% & " << setw(10) << emu_sys500 << "\\% & " << setw(10) << emu_tot500 << "\\% \\\\" << endl;
	ofile << setw(20) << "\\chll" << " & ";
	ofile << setw(10) << ll_stat500 << "\\% & " << setw(10) << ll_sys500 << "\\% & " << setw(10) << ll_tot500 << "\\% \\\\" << endl;
	ofile.close();
    }
    if (!do1414 && !do1313 && doabs && chan == "sinlepton" && var == "ph_pt") {
	cout << fixed << setprecision(1);
	cout << setw(20) << "\\chljets" << " & ";
	cout << setw(10) << ljets_stat500 << "\\% & " << setw(10) << ljets_sys500 << "\\% & " << setw(10) << ljets_tot500 << "\\% \\\\" << endl;

	ofstream ofile;
	ofile.open("log_500ljets");
	ofile << fixed << setprecision(1);
	ofile << setw(20) << "\\chljets" << " & ";
	ofile << setw(10) << ljets_stat500 << "\\% & " << setw(10) << ljets_sys500 << "\\% & " << setw(10) << ljets_tot500 << "\\% \\\\" << endl;
	ofile.close();
    }
}

TH1F* ConvertDR(TH1F*h, string tag) {
    Double_t bins[] = {1.0,1.2,1.4,1.6,1.8,2.0,2.2,2.4,2.6,3.5};
    int binnum = sizeof(bins)/sizeof(Double_t) - 1;
    string hname = h->GetName();
    hname += "Copy";
    hname += tag;
    TH1F* htmp = new TH1F(hname.c_str(), "", binnum, bins); 
    htmp->Sumw2();
    for (int i = 0; i < h->GetNbinsX(); i++) {
	htmp->SetBinContent(i+1, h->GetBinContent(i+1));
	htmp->SetBinError(i+1, h->GetBinError(i+1));
    }
    return htmp;
}

TH1F* Convert(TH1F*h, string tag) {
    string hname = h->GetName();
    hname += "Copy";
    TH1F* htmp = NULL;
    if (var == "ph_pt" && tag == "13" && !do1414) {
	htmp = new TH1F(hname.c_str(), "", h->GetNbinsX(), 0 , h->GetNbinsX()); 
    } else {
	htmp = new TH1F(hname.c_str(), "", h->GetNbinsX(), 0 , h->GetNbinsX()); 
    }
    htmp->Sumw2();
    for (int i = 0; i < h->GetNbinsX(); i++) {
	htmp->SetBinContent(i+1, h->GetBinContent(i+1));
    }
    return htmp;
}

TH1F* CombineHist(TH1F*h_stat, TH1F*h_sys, TH1F*h_tot) {
    TH1F* h_com = new TH1F(h_stat->GetName(), "", h_stat->GetNbinsX()*3, 0, h_stat->GetNbinsX()*3);
    for (int i = 0; i < h_stat->GetNbinsX(); i++) {
	h_com->SetBinContent(3*i+1, h_stat->GetBinContent(i+1));
    }
    for (int i = 0; i < h_sys->GetNbinsX(); i++) {
	h_com->SetBinContent(3*i+2, h_sys->GetBinContent(i+1));
    }
    for (int i = 0; i < h_tot->GetNbinsX(); i++) {
	h_com->SetBinContent(3*i+3, h_tot->GetBinContent(i+1));
    }
    return h_com;
}

