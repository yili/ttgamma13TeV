#include<TH1F.h>
#include <RooFitResult.h>
#include<TLatex.h>
#include<TLegend.h>
#include<TLine.h>
#include<TH2F.h>
#include<TString.h>
#include<TPaveStats.h>
#include<TFile.h>
#include<TROOT.h>
#include<TStyle.h>
#include<RooRealVar.h>
#include<RooPlot.h>
#include<TF1.h>
#include<TCanvas.h>
#include <RooFit.h>
#include <RooRealVar.h>
#include <RooExponential.h>
#include <RooGaussian.h>
#include <RooCBShape.h>
#include <RooWorkspace.h>
#include <RooDataSet.h>
#include "RooPolynomial.h"
#include <RooRealVar.h>
#include <RooArgSet.h>
#include <RooWorkspace.h>
#include <RooDataHist.h>
#include <RooHist.h>
#include <RooHistPdf.h>
#include <RooProduct.h>
#include <RooCurve.h>
#include <RooAddPdf.h>
#include <RooAbsPdf.h>
#include <RooAddition.h>
#include <RooProdPdf.h>
#include <RooGaussModel.h>
#include <RooBinning.h>
#include <RooGenericPdf.h>
#include <RooExtendPdf.h>
#include <RooPoisson.h>
#include <RooLognormal.h>
#include <RooCategory.h>
#include <RooSimultaneous.h>

#include <RooStats/RooStatsUtils.h>
#include <RooStats/ModelConfig.h>
#include <RooStats/MCMCInterval.h>
#include <RooStats/MCMCIntervalPlot.h>

#include"Logger.h"
#include"PlotComparor.h"
#include "StringPlayer.h"
#include "Logger.h"
#include "ConfigReader.h"
#include "AtlasStyle.h"

using namespace std;
using namespace RooFit;
using namespace RooStats;

TH1F* FitResolution2(TH2F* h_input, bool exp = true);
void FitResolution(TH1F* h_input, int ibin, double &n_sigma, double &e_sigma);
void StatUncert(TH1F* h_input);
void BinWidth(TH1F* h_input, TH1F* h_reso);
void DrawMigration(TH2F* h_input);
TString varname;
TString channel;
bool fitreso = false;

int main() 
{
    SetAtlasStyle();

    TFile* f_input = new TFile("ForDiffFit.root");

    channel = "ljets";

    varname = "Pt"; 
    TH2F* h2_sl_forbinning_phpt = (TH2F*)f_input->Get("sl_forbinning_phpt");
    TH1F* h_sl_resolution_phpt = FitResolution2(h2_sl_forbinning_phpt, false);
    TH1F *h_sl_recopt = (TH1F*)f_input->Get("sl_reco_phpt");
    BinWidth(h_sl_recopt, h_sl_resolution_phpt);
    StatUncert(h_sl_recopt);
    TH2F *h_sl_mig_pt_row = (TH2F*)f_input->Get("sl_mig_pt_row");
    DrawMigration(h_sl_mig_pt_row);

    varname = "Eta";
    TH2F* h2_sl_forbinning_pheta = (TH2F*)f_input->Get("sl_forbinning_pheta");
    TH1F* h_sl_resolution_pheta = FitResolution2(h2_sl_forbinning_pheta, false);
    TH1F *h_sl_recoeta = (TH1F*)f_input->Get("sl_reco_pheta");
    BinWidth(h_sl_recoeta, h_sl_resolution_pheta);
    StatUncert(h_sl_recoeta);
    TH2F *h_sl_mig_eta_row = (TH2F*)f_input->Get("sl_mig_eta_row");
    DrawMigration(h_sl_mig_eta_row);
    
    varname = "DrPhL";
    TH2F* h2_sl_forbinning_drphl = (TH2F*)f_input->Get("sl_forbinning_drphl");
    TH1F* h_sl_resolution_drphl = FitResolution2(h2_sl_forbinning_drphl, false);
    TH1F *h_sl_reco_drphl = (TH1F*)f_input->Get("sl_reco_drphl");
    BinWidth(h_sl_reco_drphl, h_sl_resolution_drphl);
    StatUncert(h_sl_reco_drphl);
    TH2F *h_sl_mig_drphl_row = (TH2F*)f_input->Get("sl_mig_drphl_row");
    DrawMigration(h_sl_mig_drphl_row);

    channel = "ll";

    varname = "Pt"; 
    TH2F* h2_dl_forbinning_phpt = (TH2F*)f_input->Get("dl_forbinning_phpt");
    FitResolution2(h2_dl_forbinning_phpt, false);
    TH1F *h_dl_recopt = (TH1F*)f_input->Get("dl_reco_phpt");
    BinWidth(h_dl_recopt, h_sl_resolution_phpt);
    StatUncert(h_dl_recopt);
    TH2F *h_dl_mig_pt_row = (TH2F*)f_input->Get("dl_mig_pt_row");
    DrawMigration(h_dl_mig_pt_row);

    varname = "Eta";
    TH2F* h2_dl_forbinning_pheta = (TH2F*)f_input->Get("dl_forbinning_pheta");
    FitResolution2(h2_dl_forbinning_pheta, false);
    TH1F *h_dl_recoeta = (TH1F*)f_input->Get("dl_reco_pheta");
    BinWidth(h_dl_recoeta, h_sl_resolution_pheta);
    StatUncert(h_dl_recoeta);
    TH2F *h_dl_mig_eta_row = (TH2F*)f_input->Get("dl_mig_eta_row");
    DrawMigration(h_dl_mig_eta_row);

    varname = "DrPhL";
    TH2F* h2_dl_forbinning_drphl = (TH2F*)f_input->Get("dl_forbinning_drphl");
    TH1F* h_dl_resolution_drphl = FitResolution2(h2_dl_forbinning_drphl, false);
    TH1F *h_dl_reco_drphl = (TH1F*)f_input->Get("dl_reco_drphl");
    BinWidth(h_dl_reco_drphl, h_sl_resolution_drphl);
    StatUncert(h_dl_reco_drphl);
    TH2F *h_dl_mig_drphl_row = (TH2F*)f_input->Get("dl_mig_drphl_row");
    DrawMigration(h_dl_mig_drphl_row);
}

void DrawMigration(TH2F* h_input) {
    gStyle->SetPaintTextFormat("2.0f");
    int nbins_x = h_input->GetNbinsX();
    double x_lo = h_input->GetXaxis()->GetBinLowEdge(1);
    double x_hi = h_input->GetXaxis()->GetBinLowEdge(nbins_x) + h_input->GetXaxis()->GetBinWidth(nbins_x);
    int nbins_y = h_input->GetNbinsY();
    double y_lo = h_input->GetYaxis()->GetBinLowEdge(1);
    double y_hi = h_input->GetYaxis()->GetBinLowEdge(nbins_y) + h_input->GetYaxis()->GetBinWidth(nbins_y);
    TH2F*h_copy = new TH2F("copy", "copy", nbins_x, 0, nbins_x, nbins_y, 0, nbins_y);
    for (int i = 1; i <= h_input->GetNbinsX(); i++) {
	for (int j = 1; j <= h_input->GetNbinsY(); j++) {
	    h_copy->SetBinContent(i,j,100*h_input->GetBinContent(i,j));
	    h_copy->SetBinError(i,j,100*h_input->GetBinError(i,j));
	}
    }
    TCanvas* c = new TCanvas("c","c",800,800);
    if (varname == "Pt" && channel == "ljets") h_copy->SetMarkerSize(1.2);
    else if (varname == "Pt" && channel == "ll") h_copy->SetMarkerSize(1.8);
    else if (varname == "Eta" && channel == "ljets") h_copy->SetMarkerSize(1.3);
    else if (varname == "Eta" && channel == "ll") h_copy->SetMarkerSize(1.8);
    else if (varname == "DrPhL" && channel == "ljets") h_copy->SetMarkerSize(1.3);
    else if (varname == "DrPhL" && channel == "ll") h_copy->SetMarkerSize(2);
    //if (varname == "DrPhL") h_copy->SetMarkerSize(0.7);
    //h_copy->GetZaxis()->SetRangeUser(0, 1);

    if (varname == "Pt") h_copy->GetXaxis()->SetTitle("Reco. Photon Pt Bin");
    else if (varname == "Eta") h_copy->GetXaxis()->SetTitle("Reco. Photon |Eta| Bin");
    else if (varname == "DrPhL") h_copy->GetXaxis()->SetTitle("Reco. #DeltaR(#gamma,l) Bin");

    if (varname == "Pt") h_copy->GetYaxis()->SetTitle("Truth Photon Pt Bin");
    else if (varname == "Eta") h_copy->GetYaxis()->SetTitle("Truth Photon |Eta| Bin");
    else if (varname == "DrPhL") h_copy->GetYaxis()->SetTitle("Truth #DeltaR(#gamma,l) Bin");

    Double_t levels[] = {0, 20, 40, 60, 80, 100};
    h_copy->SetContour((sizeof(levels)/sizeof(Double_t)), levels);

    h_copy->Draw("COL text");

    TLatex lt;
    lt.SetNDC();
    lt.SetTextFont(72);
    lt.DrawLatex(0.2, 0.85, "ATLAS");
    lt.SetTextFont(42);
    lt.DrawLatex(0.37, 0.85, "Internal Simulation");
    lt.DrawLatex(0.2, 0.78, "#sqrt{s}=13TeV");
    lt.DrawLatex(0.2, 0.71, "Norm. to Row (%)");

    char tmp_ch[100];
    sprintf(tmp_ch, "plots/unfolding/MigMatrix_%s_%s", varname.Data(), channel.Data());
    TString savename = tmp_ch;
    c->SaveAs((savename + ".png").Data());
    c->SaveAs((savename + ".pdf").Data());
    delete h_copy;
    delete c;
}

void BinWidth(TH1F* h_input, TH1F* h_reso) {
    TH1F* h_width = (TH1F*)h_input->Clone();
    for (int i = 1; i <= h_input->GetNbinsX(); i++) {
	h_width->SetBinContent(i, h_input->GetBinWidth(i));
	h_width->SetBinError(i, 0);
    }
    TH1F* h_reso2 = (TH1F*)h_width->Clone();
    for (int i = 1; i <= h_width->GetNbinsX(); i++) {
	double reso = h_reso->GetBinContent(h_reso->FindBin(h_reso2->GetBinCenter(i)));
	h_reso2->SetBinContent(i, 2*reso);
	h_reso2->SetBinError(i, 0);
    }

    TCanvas* c = new TCanvas("c","c",800,800);

    if (varname == "Pt") h_width->GetXaxis()->SetTitle("Photon Pt [GeV]");
    else if (varname == "Eta") h_width->GetXaxis()->SetTitle("Photon |Eta|");
    else if (varname == "DrPhL") h_width->GetXaxis()->SetTitle("#DeltaR(#gamma,l)");
    
    if (varname == "Eta" || varname == "DrPhL") h_width->GetYaxis()->SetTitleOffset(1.5);
    
    if (varname == "DrPhL") h_width->GetYaxis()->SetRangeUser(0, 1.5*h_width->GetMaximum());
    
    if (varname == "Pt") h_width->GetYaxis()->SetTitle("#DeltaPt [GeV]");
    else if (varname == "Eta") h_width->GetYaxis()->SetTitle("#Delta|Eta|");
    else if (varname == "DrPhL") h_width->GetYaxis()->SetTitle("#Delta#DeltaR(#gamma,l)");

    h_width->SetLineWidth(2);
    h_width->GetYaxis()->SetRangeUser(0, 1.1*h_width->GetMaximum());
    h_width->Draw();
    h_reso2->SetLineWidth(2);
    h_reso2->SetLineColor(kRed);
    h_reso2->Draw("same");

    TLatex lt;
    lt.SetNDC();
    lt.SetTextFont(72);
    lt.DrawLatex(0.2, 0.85, "ATLAS");
    lt.SetTextFont(42);
    lt.DrawLatex(0.37, 0.85, "Internal Simulation");
    lt.DrawLatex(0.2, 0.78, "#sqrt{s}=13TeV");

    TLegend *lg = new TLegend(0.2, 0.6, 0.5, 0.75);
    lg->SetNColumns(1);
    lg->SetTextSize(0.04);
    lg->SetBorderSize(0);
    lg->SetFillColor(0);
    lg->AddEntry(h_width, "Bin Width", "l");
    lg->AddEntry(h_reso2, "2 #times Reso.", "l");
    lg->Draw("same");

    char tmp_ch[100];
    sprintf(tmp_ch, "plots/unfolding/BinWidth_%s_%s", varname.Data(), channel.Data());
    TString savename = tmp_ch;
    c->SaveAs((savename + ".png").Data());
    c->SaveAs((savename + ".pdf").Data());
    delete c;
}

void StatUncert(TH1F* h_input) {
    TH1F* h_stat_err = (TH1F*)h_input->Clone();
    for (int i = 1; i <= h_input->GetNbinsX(); i++) {
        h_stat_err->SetBinContent(i, 1./sqrt(h_input->GetBinContent(i)));
        h_stat_err->SetBinError(i, 0);
    }
    TCanvas *tmpc = new TCanvas("tmp", "tmp", 800, 800);

    if (varname == "Pt") h_input->GetXaxis()->SetTitle("Photon Pt [GeV]");
    else if (varname == "Eta") h_input->GetXaxis()->SetTitle("Photon |Eta|");
    else if (varname == "DrPhL") h_input->GetXaxis()->SetTitle("#DeltaR(#gamma,l)");

    h_input->GetYaxis()->SetTitle("N(Event)");

    h_input->GetYaxis()->SetTitleOffset(1.5);

    if (varname == "Eta") h_input->GetYaxis()->SetRangeUser(0,1.5*h_input->GetMaximum());
    else if (varname == "DrPhL") h_input->GetYaxis()->SetRangeUser(0,1.5*h_input->GetMaximum());

    h_input->Draw("e");

    TLatex lt;
    lt.SetNDC();
    lt.SetTextFont(72);
    lt.DrawLatex(0.2, 0.85, "ATLAS");
    lt.SetTextFont(42);
    lt.DrawLatex(0.37, 0.85, "Internal Simulation");
    lt.DrawLatex(0.2, 0.78, "#sqrt{s}=13TeV");

    char tmp_ch[100];
    sprintf(tmp_ch, "plots/unfolding/RecoVar_%s_%s", varname.Data(), channel.Data());
    TString savename = tmp_ch;
    tmpc->SaveAs((savename + ".png").Data());
    tmpc->SaveAs((savename + ".pdf").Data());

    if (channel == "ljets") h_stat_err->GetYaxis()->SetRangeUser(0,0.18);
    else if (channel == "ll") h_stat_err->GetYaxis()->SetRangeUser(0,0.23);
    h_stat_err->SetLineWidth(2);

    if (varname == "Pt") h_stat_err->GetXaxis()->SetTitle("Photon Pt [GeV]");
    else if (varname == "Eta") h_stat_err->GetXaxis()->SetTitle("Photon |Eta|");
    else if (varname == "DrPhL") h_stat_err->GetXaxis()->SetTitle("#DeltaR(#gamma,l)");
    
    h_stat_err->GetYaxis()->SetTitleOffset(1.4);
    
    h_stat_err->GetYaxis()->SetTitle("1/#sqrt{S}");

    h_stat_err->Draw();

    int bin_number = h_stat_err->GetNbinsX();
    float bin_low = h_stat_err->GetXaxis()->GetXmin();
    float bin_high = h_stat_err->GetXaxis()->GetXmax();
    Double_t x1 = h_stat_err->GetBinLowEdge(1);
    Double_t x2 = h_stat_err->GetBinLowEdge(bin_number) + h_stat_err->GetBinWidth(bin_number);
    Double_t y1,y2;
    if (channel == "ljets") {
	y1 = 0.10;
	y2 = 0.10;
    } else {
	y1 = 0.15;
	y2 = 0.15;
    }
    TLine* central_line = new TLine(x1, y1, x2, y2);
    central_line->SetLineColor(kRed);
    central_line->SetLineWidth(2);
    central_line->SetLineStyle(2);
    central_line->Draw("same");

    lt.SetNDC();
    lt.SetTextFont(72);
    lt.DrawLatex(0.2, 0.85, "ATLAS");
    lt.SetTextFont(42);
    lt.DrawLatex(0.37, 0.85, "Internal Simulation");
    lt.DrawLatex(0.2, 0.78, "#sqrt{s}=13TeV");
    
    sprintf(tmp_ch, "plots/unfolding/StatErr_%s_%s", varname.Data(), channel.Data());
    savename = tmp_ch;
    tmpc->SaveAs((savename + ".png").Data());
    tmpc->SaveAs((savename + ".pdf").Data());
    
    delete tmpc;
}

TH1F* FitResolution2(TH2F* h_input, bool exp) {

    TH1F* h_out;

    int nbins_x = h_input->GetNbinsX();
    double x_lo = h_input->GetXaxis()->GetBinLowEdge(1);
    double x_hi = h_input->GetXaxis()->GetBinLowEdge(nbins_x) + h_input->GetXaxis()->GetBinWidth(nbins_x);
    int nbins_y = h_input->GetNbinsY();
    double y_lo = h_input->GetYaxis()->GetBinLowEdge(1);
    double y_hi = h_input->GetYaxis()->GetBinLowEdge(nbins_y) + h_input->GetYaxis()->GetBinWidth(nbins_y);
    TH1F* h_resolution = new TH1F("resolution","resolution",nbins_x, x_lo, x_hi);
    for (int i = 1; i <= nbins_x; i++) {
        char tmpchar[100];
        sprintf(tmpchar, "xbin_%i", i);
        TH1F *h_profile = new TH1F(tmpchar, tmpchar, nbins_y, y_lo, y_hi);
        for (int j = 1; j <= nbins_y; j++) {
            h_profile->SetBinContent(j, h_input->GetBinContent(i,j));
            h_profile->SetBinError(j, h_input->GetBinError(i,j));
        }
    
        double n_sigma, e_sigma;
        FitResolution(h_profile, i, n_sigma, e_sigma);
	delete h_profile;
        h_resolution->SetBinContent(i, n_sigma);
        h_resolution->SetBinError(i, e_sigma);
    }

    if (!fitreso) {
	TCanvas *tmpc = new TCanvas("tmp", "tmp", 800, 800);
	char tmp_ch[100];
	sprintf(tmp_ch, "plots/unfolding/Reso_overall_%s_%s", varname.Data(), channel.Data());

	if (varname == "Pt") h_resolution->GetXaxis()->SetTitle("Photon Pt [GeV]");
	else if (varname == "Eta") h_resolution->GetXaxis()->SetTitle("Photon |Eta|");
	else if (varname == "DrPhL") h_resolution->GetXaxis()->SetTitle("#DeltaR(#gamma,l)");

	if (varname == "Eta" || varname == "DrPhL") h_resolution->GetYaxis()->SetTitleOffset(1.5);

	if (varname == "Eta") h_resolution->GetYaxis()->SetRangeUser(0, 1.5*h_resolution->GetMaximum());
	else if (varname == "DrPhL") h_resolution->GetYaxis()->SetRangeUser(0, 1.5*h_resolution->GetMaximum());

	h_resolution->GetYaxis()->SetTitle("Resolution");

	h_resolution->Draw();
	TLatex lt;
	lt.SetNDC();
	lt.SetTextFont(72);
	lt.DrawLatex(0.2, 0.85, "ATLAS");
	lt.SetTextFont(42);
	lt.DrawLatex(0.37, 0.85, "Internal Simulation");
	lt.DrawLatex(0.2, 0.78, "#sqrt{s}=13TeV");
        TString savename = tmp_ch;
	tmpc->SaveAs((savename + ".png").Data());
	tmpc->SaveAs((savename + ".pdf").Data());
	delete tmpc;
	h_out = (TH1F*)h_resolution->Clone();
	delete h_resolution;
	return h_out;
    }
    
    RooRealVar x("x","x",x_lo,x_hi);
    x.setBins(nbins_x);
    
    RooDataHist *data = new RooDataHist("data", "data", x, h_resolution);
    
    TCanvas *tmpc = new TCanvas("tmp", "tmp", 800, 800);
    char tmp_ch[100];
    TF1* f_reso;
    if (exp) {
	RooRealVar *c = new RooRealVar("c", "c", 1, -10, 10);
    	RooExponential *exp = new RooExponential("exp", "exp", x, *c);
	RooRealVar *norm = new RooRealVar("norm", "norm", 1, 0, 10);
	RooExtendPdf *model = new RooExtendPdf("model", "model", *exp, *norm);
    	model->fitTo(*data,SumW2Error(kTRUE));
    	
    	RooPlot* xframe = x.frame(Title("Resolution")) ;
    	data->plotOn(xframe) ;
    	model->plotOn(xframe) ;
    	
    	xframe->Draw();
    	
    	Double_t chi2 = xframe->chiSquare();
    	cout << "Fit chi2/dof = " << chi2 << endl;
    	sprintf(tmp_ch, "chi2/dof=%.1f", chi2);
    	TLatex lt;
    	lt.SetNDC();
    	lt.SetTextFont(42);
    	lt.DrawLatex(0.2, 0.75, tmp_ch);
    	
	delete exp;
	delete c;
    	delete data;
    } else {
	RooRealVar *norm = new RooRealVar("norm","",100,0,10000);
	RooRealVar *a0 = new RooRealVar("a0", "a0", 0.01, -10, 10);
    	RooRealVar *a1 = new RooRealVar("a1", "a1", 0.01, -10, 10);
    	RooRealVar *a2 = new RooRealVar("a2", "a2", 0.01, -10, 10);
    	RooRealVar *a3 = new RooRealVar("a3", "a3", 0.01, -10, 10);
    	RooRealVar *a4 = new RooRealVar("a4", "a4", 0.01, -10, 10);
    	RooRealVar *a5 = new RooRealVar("a5", "a5", 0.01, -10, 10);
    	RooPolynomial *poly;
	if (varname == "Eta") {
	    poly = new RooPolynomial("poly", "poly", x, RooArgList(*a0,*a1,*a2,*a3),0);
	} else if (varname == "DrPhL") {
	    poly = new RooPolynomial("poly", "poly", x, RooArgList(*a0,*a1,*a2,*a3,*a4),0);
	} else {
	    poly = new RooPolynomial("poly", "poly", x, RooArgList(*a0,*a1,*a2),0);
	}
	RooExtendPdf *model = new RooExtendPdf("ext","",*poly,*norm);
    	model->fitTo(*data,SumW2Error(kTRUE));
	cout << "Fit again!" << endl;
    	RooFitResult *result = model->fitTo(*data,SumW2Error(kTRUE),Extended(),RooFit::Save());
	result->Print("v");
    	
    	RooPlot* xframe = x.frame(Title("Resolution")) ;
    	data->plotOn(xframe) ;
    	model->plotOn(xframe) ;
	//poly->paramOn(xframe, Format("NELU", AutoPrecision(2)), Layout(0.1,0.4,0.9));
    	
	if (varname == "Pt") {
	    xframe->SetMinimum(0);
	    xframe->SetMaximum(6);
	} else if (varname == "Eta") {
	    xframe->SetMinimum(0.005);
	    xframe->SetMaximum(0.04);
	}
    	xframe->Draw();

	//f_reso = (TF1*)poly->asTF(RooArgList(x),RooArgList(*Norm,*a0,*a1,*a2));
	//f_reso = (TF1*)poly->asTF(RooArgList(x),RooArgList(*a0,*a1,*a2),x);
	//cout << "int: " << f_reso->Integral(x_lo, x_hi) << endl;
	//TH1F h_tmp("h_tmp","h_tmp",100,x_lo,x_hi);
	//h_tmp.Add(f_reso,norm);
	//h_tmp.Draw();
	//f_reso->SetLineColor(kRed);
	//f_reso->Draw("lsame");

	h_out = (TH1F*)h_resolution->Clone();
	for (int i = 1; i <= nbins_x; i++) {
    	    x.setRange("tmprange",h_resolution->GetBinLowEdge(i),h_resolution->GetBinLowEdge(i)+h_resolution->GetBinWidth(i));
    	    RooAbsReal* int_x = model->createIntegral(x,NormSet(x),Range("tmprange"));
    	    double bin_x = int_x->getVal()*norm->getVal();
	    h_out->SetBinContent(i, bin_x);
	    h_out->SetBinError(i, 0);
	    //cout << h_resolution->GetBinLowEdge(i) << " " << h_resolution->GetBinLowEdge(i)+h_resolution->GetBinWidth(i) << " " << bin_x << " " << h_resolution->GetBinContent(i) << endl;
    	}
    	
    	Double_t chi2 = xframe->chiSquare();
    	//cout << "Fit chi2/dof = " << chi2 << endl;
    	sprintf(tmp_ch, "chi2/dof=%.1f", chi2);
    	TLatex lt;
    	lt.SetNDC();
    	lt.SetTextFont(42);
    	lt.DrawLatex(0.2, 0.75, tmp_ch);
	tmpc->ls();
    	
    	delete data;
    }
    if (exp) {
       sprintf(tmp_ch, "plots/unfolding/ResoFit_overall_%s_%s_Exp", varname.Data(), channel.Data());
    } else {
       sprintf(tmp_ch, "plots/unfolding/ResoFit_overall_%s_%s_Poly", varname.Data(), channel.Data());
    }
    TString savename = tmp_ch;
    tmpc->SaveAs((savename + ".png").Data());
    tmpc->SaveAs((savename + ".pdf").Data());
    delete tmpc;

    return h_out;
}

void FitResolution(TH1F* h_input, int ibin, double &n_sigma, double &e_sigma) {
    
    if (!fitreso) {
	n_sigma = h_input->GetStdDev();
	e_sigma = h_input->GetStdDevError();
	return;
    }

    RooMsgService::instance().setSilentMode(kTRUE);
    RooMsgService::instance().setGlobalKillBelow(WARNING);

    int nbins_x = h_input->GetNbinsX();
    double x_lo = h_input->GetXaxis()->GetBinLowEdge(1);
    double x_hi = h_input->GetXaxis()->GetBinLowEdge(nbins_x) + h_input->GetXaxis()->GetBinWidth(nbins_x);
    RooRealVar x("x","x",x_lo,x_hi);
    x.setBins(nbins_x);

    RooDataHist *data = new RooDataHist("data", "data", x, h_input);
    RooRealVar *m = new RooRealVar("m","m",h_input->GetMean(),x_lo,x_hi) ;
    RooRealVar *s = new RooRealVar("s","s",h_input->GetStdDev(),0,5*h_input->GetStdDev()) ;
    RooGaussian *gaus = new RooGaussian("gaus","Gaussian Function", x, *m, *s);
    gaus->fitTo(*data,SumW2Error(kTRUE));
    gaus->fitTo(*data,SumW2Error(kTRUE));

    RooPlot* xframe = x.frame(Title("Resolution")) ;
    data->plotOn(xframe) ;
    gaus->plotOn(xframe) ;

    TCanvas *tmpc = new TCanvas("tmp", "tmp", 800, 800);
    xframe->Draw();

    Double_t chi2 = xframe->chiSquare(2);
    cout << "Fit chi2/dof = " << chi2 << endl;
    char tmp_ch[100];
    sprintf(tmp_ch, "chi2/dof=%.1f", chi2);
    TLatex lt;
    lt.SetNDC();
    lt.SetTextFont(42);
    lt.DrawLatex(0.66, 0.59, tmp_ch);

    sprintf(tmp_ch, "plots/unfolding/ResoFit_bin_%i_%s_%s", ibin, varname.Data(), channel.Data());
    TString savename = tmp_ch;
    tmpc->SaveAs((savename + ".png").Data());
    tmpc->SaveAs((savename + ".pdf").Data());

    n_sigma = s->getVal();
    e_sigma = s->getError();

    delete tmpc;
    delete gaus;
    delete m;
    delete s;
    delete data;
}
