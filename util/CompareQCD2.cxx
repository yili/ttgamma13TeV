{
    {
    TString ps = "UPS";
    TString chan = "ejets";

    vector<TString> paras;
    paras.push_back("pt");
    paras.push_back("eta");
    paras.push_back("dR");
    paras.push_back("jetpt");
    paras.push_back("dPhi");
    paras.push_back("nbtag");
    paras.push_back("mtw");
    paras.push_back("pt:eta");
    paras.push_back("pt:dR");
    paras.push_back("pt:jetpt");
    paras.push_back("pt:dPhi");
    paras.push_back("pt:nbtag");
    paras.push_back("pt:mtw");
    paras.push_back("eta:dR");
    paras.push_back("eta:jetpt");
    paras.push_back("eta:dPhi");
    paras.push_back("eta:nbtag");
    paras.push_back("eta:mtw");
    paras.push_back("dR:jetpt");
    paras.push_back("dR:dPhi");
    paras.push_back("dR:nbtag");
    paras.push_back("dR:mtw");
    paras.push_back("jetpt:dPhi");
    paras.push_back("jetpt:nbtag");
    paras.push_back("jetpt:mtw");
    paras.push_back("dPhi:nbtag");
    paras.push_back("dPhi:mtw");
    paras.push_back("nbtag:mtw");

    //cout << setw(20) << " ";
    //for (int i = 0; i < names.size(); i++) {
    //    cout << " &" << setw(20) << names.at(i);
    //}
    //cout << " &" << setw(20) << "Aver.+Enve.";
    //cout << "\\\\" << endl;

    TString file = "results/Results_QCD_13TeV_CR1_"+chan+"_CutSR_Para1_"+ps+"_V007.02.root";
    TFile* f = new TFile(file);

    double n_av = 0;
    double n_max = 0;
    double n_min = 999999;

    TH1F* h_all = new TH1F("results","",28,0,28);
    h_all->GetYaxis()->SetTitle("Pred.");
    for (int i = 0; i < paras.size(); i++) {
	char tmp[100];
	sprintf(tmp, "Cutflow_Para%d_CR1_%s_QCD_Nominal", i+1, chan.Data());
	TH1F* h = (TH1F*)f->Get(tmp);
	double n = 0;
	double e = 0;
	for (int j = 0; j < h->GetNbinsX(); j++) {
	    //cout << h->GetXaxis()->GetBinLabel(j+1) << " " << h->GetBinContent(j+1) << endl;
	    if (TString(h->GetXaxis()->GetBinLabel(j+1)) == "Cut:DrPhLep1.0") {
		n = h->GetBinContent(j+1);
		e = h->GetBinError(j+1);
	    }
	}
	h_all->SetBinContent(i+1, n);
	h_all->SetBinError(i+1, e);
	h_all->GetXaxis()->SetBinLabel(i+1, paras.at(i));
	n_av += n;
	if (n < n_min) n_min = n;
	if (n > n_max) n_max = n;
	//cout << files.at(i) << " " << n << " " << e << endl;
	//cout << " &" << setw(10) << n << "$\\pm$" << setw(5) << e;
    }
    n_av /= paras.size();
    double n_enve = (n_max - n_min)/2;
    //double e_ej_UPS_err = (n_ej_UPS_max - n_ej_UPS_min)/2;
    //cout << " &" << setw(10) << n_ej_UPS_av << "$\\pm$" << setw(5) << e_ej_UPS_err;
    //cout << "\\\\" << endl;
    //cout << endl;
    TCanvas *c = new TCanvas("c","",800,600);
    c->SetBottomMargin(0.2);
    h_all->GetXaxis()->LabelsOption("v");
    h_all->Draw("e");
    TString save = "QCD_28Paras_" + ps + "_" + chan + ".pdf";
    c->SaveAs(save);

    cout << n_av << " " << n_enve << endl;
    }

    {
    TString ps = "UPS";
    TString chan = "mujets";

    vector<TString> paras;
    paras.push_back("pt");
    paras.push_back("eta");
    paras.push_back("dR");
    paras.push_back("jetpt");
    paras.push_back("dPhi");
    paras.push_back("nbtag");
    paras.push_back("mtw");
    paras.push_back("pt:eta");
    paras.push_back("pt:dR");
    paras.push_back("pt:jetpt");
    paras.push_back("pt:dPhi");
    paras.push_back("pt:nbtag");
    paras.push_back("pt:mtw");
    paras.push_back("eta:dR");
    paras.push_back("eta:jetpt");
    paras.push_back("eta:dPhi");
    paras.push_back("eta:nbtag");
    paras.push_back("eta:mtw");
    paras.push_back("dR:jetpt");
    paras.push_back("dR:dPhi");
    paras.push_back("dR:nbtag");
    paras.push_back("dR:mtw");
    paras.push_back("jetpt:dPhi");
    paras.push_back("jetpt:nbtag");
    paras.push_back("jetpt:mtw");
    paras.push_back("dPhi:nbtag");
    paras.push_back("dPhi:mtw");
    paras.push_back("nbtag:mtw");

    //cout << setw(20) << " ";
    //for (int i = 0; i < names.size(); i++) {
    //    cout << " &" << setw(20) << names.at(i);
    //}
    //cout << " &" << setw(20) << "Aver.+Enve.";
    //cout << "\\\\" << endl;

    TString file = "results/Results_QCD_13TeV_CR1_"+chan+"_CutSR_Para1_"+ps+"_V007.02.root";
    TFile* f = new TFile(file);

    double n_av = 0;
    double n_max = 0;
    double n_min = 999999;

    TH1F* h_all = new TH1F("results","",28,0,28);
    h_all->GetYaxis()->SetTitle("Pred.");
    for (int i = 0; i < paras.size(); i++) {
	char tmp[100];
	sprintf(tmp, "Cutflow_Para%d_CR1_%s_QCD_Nominal", i+1, chan.Data());
	TH1F* h = (TH1F*)f->Get(tmp);
	double n = 0;
	double e = 0;
	for (int j = 0; j < h->GetNbinsX(); j++) {
	    //cout << h->GetXaxis()->GetBinLabel(j+1) << " " << h->GetBinContent(j+1) << endl;
	    if (TString(h->GetXaxis()->GetBinLabel(j+1)) == "Cut:DrPhLep1.0") {
		n = h->GetBinContent(j+1);
		e = h->GetBinError(j+1);
	    }
	}
	h_all->SetBinContent(i+1, n);
	h_all->SetBinError(i+1, e);
	h_all->GetXaxis()->SetBinLabel(i+1, paras.at(i));
	n_av += n;
	if (n < n_min) n_min = n;
	if (n > n_max) n_max = n;
	//cout << files.at(i) << " " << n << " " << e << endl;
	//cout << " &" << setw(10) << n << "$\\pm$" << setw(5) << e;
    }
    n_av /= paras.size();
    double n_enve = (n_max - n_min)/2;
    //double e_ej_UPS_err = (n_ej_UPS_max - n_ej_UPS_min)/2;
    //cout << " &" << setw(10) << n_ej_UPS_av << "$\\pm$" << setw(5) << e_ej_UPS_err;
    //cout << "\\\\" << endl;
    //cout << endl;
    TCanvas *c = new TCanvas("c","",800,600);
    c->SetBottomMargin(0.2);
    h_all->GetXaxis()->LabelsOption("v");
    h_all->Draw("e");
    TString save = "QCD_28Paras_" + ps + "_" + chan + ".pdf";
    c->SaveAs(save);

    cout << n_av << " " << n_enve << endl;
    }

    {
    TString ps = "PS";
    TString chan = "mujets";

    vector<TString> paras;
    paras.push_back("pt");
    paras.push_back("eta");
    paras.push_back("dR");
    paras.push_back("jetpt");
    paras.push_back("dPhi");
    paras.push_back("nbtag");
    paras.push_back("mtw");
    paras.push_back("pt:eta");
    paras.push_back("pt:dR");
    paras.push_back("pt:jetpt");
    paras.push_back("pt:dPhi");
    paras.push_back("pt:nbtag");
    paras.push_back("pt:mtw");
    paras.push_back("eta:dR");
    paras.push_back("eta:jetpt");
    paras.push_back("eta:dPhi");
    paras.push_back("eta:nbtag");
    paras.push_back("eta:mtw");
    paras.push_back("dR:jetpt");
    paras.push_back("dR:dPhi");
    paras.push_back("dR:nbtag");
    paras.push_back("dR:mtw");
    paras.push_back("jetpt:dPhi");
    paras.push_back("jetpt:nbtag");
    paras.push_back("jetpt:mtw");
    paras.push_back("dPhi:nbtag");
    paras.push_back("dPhi:mtw");
    paras.push_back("nbtag:mtw");

    //cout << setw(20) << " ";
    //for (int i = 0; i < names.size(); i++) {
    //    cout << " &" << setw(20) << names.at(i);
    //}
    //cout << " &" << setw(20) << "Aver.+Enve.";
    //cout << "\\\\" << endl;

    TString file = "results/Results_QCD_13TeV_CR1_"+chan+"_CutSR_Para1_"+ps+"_V007.02.root";
    TFile* f = new TFile(file);

    double n_av = 0;
    double n_max = 0;
    double n_min = 999999;

    TH1F* h_all = new TH1F("results","",28,0,28);
    h_all->GetYaxis()->SetTitle("Pred.");
    for (int i = 0; i < paras.size(); i++) {
	char tmp[100];
	sprintf(tmp, "Cutflow_Para%d_CR1_%s_QCD_Nominal", i+1, chan.Data());
	TH1F* h = (TH1F*)f->Get(tmp);
	double n = 0;
	double e = 0;
	for (int j = 0; j < h->GetNbinsX(); j++) {
	    //cout << h->GetXaxis()->GetBinLabel(j+1) << " " << h->GetBinContent(j+1) << endl;
	    if (TString(h->GetXaxis()->GetBinLabel(j+1)) == "Cut:DrPhLep1.0") {
		n = h->GetBinContent(j+1);
		e = h->GetBinError(j+1);
	    }
	}
	h_all->SetBinContent(i+1, n);
	h_all->SetBinError(i+1, e);
	h_all->GetXaxis()->SetBinLabel(i+1, paras.at(i));
	n_av += n;
	if (n < n_min) n_min = n;
	if (n > n_max) n_max = n;
	//cout << files.at(i) << " " << n << " " << e << endl;
	//cout << " &" << setw(10) << n << "$\\pm$" << setw(5) << e;
    }
    n_av /= paras.size();
    double n_enve = (n_max - n_min)/2;
    //double e_ej_UPS_err = (n_ej_UPS_max - n_ej_UPS_min)/2;
    //cout << " &" << setw(10) << n_ej_UPS_av << "$\\pm$" << setw(5) << e_ej_UPS_err;
    //cout << "\\\\" << endl;
    //cout << endl;
    TCanvas *c = new TCanvas("c","",800,600);
    c->SetBottomMargin(0.2);
    h_all->GetXaxis()->LabelsOption("v");
    h_all->Draw("e");
    TString save = "QCD_28Paras_" + ps + "_" + chan + ".pdf";
    c->SaveAs(save);

    cout << n_av << " " << n_enve << endl;
    }
}
