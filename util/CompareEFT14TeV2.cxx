#include <TFile.h>
#include <fstream>
#include <TLegend.h>
#include <TLatex.h>
#include <TH2F.h>
#include <TCanvas.h>
#include <TStyle.h>
#include <TH1F.h>
#include <iostream>
#include <iomanip>
#include "PlotComparor.h"
#include "AtlasStyle.h"
#include "SumWeightTree.h"
#include <map>

using namespace std;
bool emuonly = true;

void format_histogram(TH1F* h);
void smooth_histogram(TH1F* hvar, TH1F* hnom);
void MergeHistograms(TH1F* h1, TH1F* h2, TH1F* hm, TH1F* hp, TH1F* hn);

int main(int argc, char * argv[]) 
{
    for (int i = 0; i < argc; i++) {
	if (!strcmp(argv[i],"--dilepton")) {
	    emuonly = false;
   	}
    }

    TFile *fin = new TFile("results/Upgrade/EFTeffects.root");
    TH1F* h_sm_sl_nominal_isr = (TH1F*)fin->Get("sm_sl_nominal_isr");
    TH1F* h_sm_sl_nominal = (TH1F*)fin->Get("sm_sl_nominal");
    TH1F* h_sm_sl = (TH1F*)fin->Get("sm_sl");
    TH1F* h_sm_ctbm_sl = (TH1F*)fin->Get("sl_ctbm");
    TH1F* h_sm_ctbp_sl = (TH1F*)fin->Get("sl_ctbp");
    TH1F* h_sm_ctgm_sl = (TH1F*)fin->Get("sl_ctgm");
    TH1F* h_sm_ctgp_sl = (TH1F*)fin->Get("sl_ctgp");
    TH1F* h_sm_ctwm_sl = (TH1F*)fin->Get("sl_ctwm");
    TH1F* h_sm_ctwp_sl = (TH1F*)fin->Get("sl_ctwp");
    TH1F* h_sm_ll_nominal_isr = (TH1F*)fin->Get("sm_ll_nominal_isr");
    TH1F* h_sm_ll_nominal = (TH1F*)fin->Get("sm_ll_nominal");
    TH1F* h_sm_ll = (TH1F*)fin->Get("sm_ll");
    TH1F* h_sm_ctbm_ll = (TH1F*)fin->Get("ll_ctbm");
    TH1F* h_sm_ctbp_ll = (TH1F*)fin->Get("ll_ctbp");
    TH1F* h_sm_ctgm_ll = (TH1F*)fin->Get("ll_ctgm");
    TH1F* h_sm_ctgp_ll = (TH1F*)fin->Get("ll_ctgp");
    TH1F* h_sm_ctwm_ll = (TH1F*)fin->Get("ll_ctwm");
    TH1F* h_sm_ctwp_ll = (TH1F*)fin->Get("ll_ctwp");

    TH1F* h_sl_ctbsigma1 = (TH1F*)fin->Get("sl_ctbsigma1")->Clone("sl_ctbsigma1");
    TH1F* h_sl_ctbsigma2 = (TH1F*)fin->Get("sl_ctbsigma2")->Clone("sl_ctbsigma2");
    TH1F* h_sl_ctgsigma1 = (TH1F*)fin->Get("sl_ctgsigma1")->Clone("sl_ctgsigma1");
    TH1F* h_sl_ctgsigma2 = (TH1F*)fin->Get("sl_ctgsigma2")->Clone("sl_ctgsigma2");
    TH1F* h_sl_ctwsigma1 = (TH1F*)fin->Get("sl_ctwsigma1")->Clone("sl_ctwsigma1");
    TH1F* h_sl_ctwsigma2 = (TH1F*)fin->Get("sl_ctwsigma2")->Clone("sl_ctwsigma2");
    TH1F* h_ll_ctbsigma1 = (TH1F*)fin->Get("ll_ctbsigma1")->Clone("ll_ctbsigma1");
    TH1F* h_ll_ctbsigma2 = (TH1F*)fin->Get("ll_ctbsigma2")->Clone("ll_ctbsigma2");
    TH1F* h_ll_ctgsigma1 = (TH1F*)fin->Get("ll_ctgsigma1")->Clone("ll_ctgsigma1");
    TH1F* h_ll_ctgsigma2 = (TH1F*)fin->Get("ll_ctgsigma2")->Clone("ll_ctgsigma2");
    TH1F* h_ll_ctwsigma1 = (TH1F*)fin->Get("ll_ctwsigma1")->Clone("ll_ctwsigma1");
    TH1F* h_ll_ctwsigma2 = (TH1F*)fin->Get("ll_ctwsigma2")->Clone("ll_ctwsigma2");

    //smooth_histogram(h_sm_ctbm_sl, h_sm_sl);
    //smooth_histogram(h_sm_ctbp_sl, h_sm_sl);
    //smooth_histogram(h_sm_ctgm_sl, h_sm_sl);
    //smooth_histogram(h_sm_ctgp_sl, h_sm_sl);
    //smooth_histogram(h_sm_ctwm_sl, h_sm_sl);
    //smooth_histogram(h_sm_ctwp_sl, h_sm_sl);
    //smooth_histogram(h_sm_ctbm_ll, h_sm_ll);
    //smooth_histogram(h_sm_ctbp_ll, h_sm_ll);
    //smooth_histogram(h_sm_ctgm_ll, h_sm_ll);
    //smooth_histogram(h_sm_ctgp_ll, h_sm_ll);
    //smooth_histogram(h_sm_ctwm_ll, h_sm_ll);
    //smooth_histogram(h_sm_ctwp_ll, h_sm_ll);
    //MergeHistograms(h_sl_ctbsigma1, h_sl_ctbsigma2, h_sm_ctbm_sl, h_sm_ctbp_sl, h_sm_sl);
    //MergeHistograms(h_sl_ctgsigma1, h_sl_ctgsigma2, h_sm_ctgm_sl, h_sm_ctgp_sl, h_sm_sl);
    //MergeHistograms(h_sl_ctwsigma1, h_sl_ctwsigma2, h_sm_ctwm_sl, h_sm_ctwp_sl, h_sm_sl);
    //MergeHistograms(h_ll_ctbsigma1, h_ll_ctbsigma2, h_sm_ctbm_ll, h_sm_ctbp_ll, h_sm_ll);
    //MergeHistograms(h_ll_ctgsigma1, h_ll_ctgsigma2, h_sm_ctgm_ll, h_sm_ctgp_ll, h_sm_ll);
    //MergeHistograms(h_ll_ctwsigma1, h_ll_ctwsigma2, h_sm_ctwm_ll, h_sm_ctwp_ll, h_sm_ll);

    smooth_histogram(h_sl_ctbsigma1, h_sm_sl);
    smooth_histogram(h_sl_ctbsigma2, h_sm_sl);
    smooth_histogram(h_sl_ctgsigma1, h_sm_sl);
    smooth_histogram(h_sl_ctgsigma2, h_sm_sl);
    smooth_histogram(h_sl_ctwsigma1, h_sm_sl);
    smooth_histogram(h_sl_ctwsigma2, h_sm_sl);
    smooth_histogram(h_ll_ctbsigma1, h_sm_ll);
    smooth_histogram(h_ll_ctbsigma2, h_sm_ll);
    smooth_histogram(h_ll_ctgsigma1, h_sm_ll);
    smooth_histogram(h_ll_ctgsigma2, h_sm_ll);
    smooth_histogram(h_ll_ctwsigma1, h_sm_ll);
    smooth_histogram(h_ll_ctwsigma2, h_sm_ll);

    TFile* fin2_sl = new TFile("results/Nominal/Sig_Sys_Upgrade3_pt1000.root");
    TFile* fin2_ll;
    if (emuonly) fin2_ll= new TFile("results/Nominal/Sig_Sys_Upgrade3_emuonly_pt1000.root");
    else fin2_ll = new TFile("results/Nominal/Sig_Sys_Upgrade3_pt1000.root");
    TH1F* h_sm_sl_real = (TH1F*)fin2_sl->Get("h_lumi_ph_ptNominal_ljets_truth");
    TH1F* h_sm_ll_real = (TH1F*)fin2_ll->Get("h_lumi_ph_ptNominal_ll_truth");
    if (!h_sm_sl_real) {
	cout << "ERROR: can't find h_lumi_ph_ptNominal_ljets_truth in Sig_Sys_Upgrade3_pt1000.root" << endl;
	exit(-1);
    }

    TFile* f_sl_sys = new TFile("/afs/cern.ch/work/y/yili/private/Analysis/Unfolding_Upgrade/Results_Step2_pt1000/ph_pt_uncertainties_sinlepton_3itr.root");
    TH1F* h_sl_sys_up = (TH1F*)f_sl_sys->Get("tot_up_rel");
    TH1F* h_sl_sys_down = (TH1F*)f_sl_sys->Get("tot_up_rel");

    TH1F* h_sl_ctbsigma1_real = (TH1F*)h_sl_ctbsigma1->Clone("sl_ctbsigma1_real"); h_sl_ctbsigma1_real->Divide(h_sm_sl);
    TH1F* h_sl_ctbsigma2_real = (TH1F*)h_sl_ctbsigma2->Clone("sl_ctbsigma2_real"); h_sl_ctbsigma2_real->Divide(h_sm_sl);
    TH1F* h_sm_sl_frac = (TH1F*)h_sm_sl_real->Clone("sm_sl_frac");
    for (int i = 1; i <= h_sl_ctbsigma1_real->GetNbinsX(); i++) {
	double xsec_all_real = h_sm_sl_real->GetBinContent(i);
	double xsec_all = h_sm_sl_nominal->GetBinContent(i);
	double xsec_isr = h_sm_sl_nominal_isr->GetBinContent(i);
	double xsec_fsr = xsec_all - xsec_isr;
	double frac_isr = xsec_isr/(xsec_isr+xsec_fsr);
	h_sm_sl_frac->SetBinContent(i, frac_isr);
	h_sm_sl_frac->SetBinError(i, 0);
	double xsec_isr_real = xsec_all_real * frac_isr;
	double frac_sigma1 = h_sl_ctbsigma1_real->GetBinContent(i);
	double frac_sigma2 = h_sl_ctbsigma2_real->GetBinContent(i);
	double frac_sigma1_relerr = h_sl_ctbsigma1_real->GetBinError(i)/h_sl_ctbsigma1_real->GetBinContent(i);
	double frac_sigma2_relerr = h_sl_ctbsigma2_real->GetBinError(i)/h_sl_ctbsigma2_real->GetBinContent(i);
	double xsec_isr_sigma1_real = xsec_isr_real * frac_sigma1;
	double xsec_isr_sigma2_real = xsec_isr_real * frac_sigma2;
	double xsec_isr_sigma1_real_err = xsec_isr_sigma1_real * frac_sigma1_relerr;
	double xsec_isr_sigma2_real_err = xsec_isr_sigma2_real * frac_sigma2_relerr;
	//cout << xsec_all_real << " " << frac_isr << " " << xsec_isr_real << " " << frac_sigma1 << " " << frac_sigma2 << " " << xsec_isr_sigma1_real << " " << xsec_isr_sigma2_real << endl;
	h_sl_ctbsigma1_real->SetBinContent(i, xsec_isr_sigma1_real);
	h_sl_ctbsigma1_real->SetBinError(i, xsec_isr_sigma1_real_err);
	h_sl_ctbsigma2_real->SetBinContent(i, xsec_isr_sigma2_real);
	h_sl_ctbsigma2_real->SetBinError(i, xsec_isr_sigma2_real_err);
    }
    TH1F* h_sl_ctgsigma1_real = (TH1F*)h_sl_ctgsigma1->Clone("sl_ctgsigma1_real"); h_sl_ctgsigma1_real->Divide(h_sm_sl);
    TH1F* h_sl_ctgsigma2_real = (TH1F*)h_sl_ctgsigma2->Clone("sl_ctgsigma2_real"); h_sl_ctgsigma2_real->Divide(h_sm_sl);
    for (int i = 1; i <= h_sl_ctgsigma1_real->GetNbinsX(); i++) {
	double xsec_all_real = h_sm_sl_real->GetBinContent(i);
	double xsec_all = h_sm_sl_nominal->GetBinContent(i);
	double xsec_isr = h_sm_sl_nominal_isr->GetBinContent(i);
	double xsec_fsr = xsec_all - xsec_isr;
	double frac_isr = xsec_isr/(xsec_isr+xsec_fsr);
	double xsec_isr_real = xsec_all_real * frac_isr;
	double frac_sigma1 = h_sl_ctgsigma1_real->GetBinContent(i);
	double frac_sigma2 = h_sl_ctgsigma2_real->GetBinContent(i);
	double frac_sigma1_relerr = h_sl_ctgsigma1_real->GetBinError(i)/h_sl_ctgsigma1_real->GetBinContent(i);
	double frac_sigma2_relerr = h_sl_ctgsigma2_real->GetBinError(i)/h_sl_ctgsigma2_real->GetBinContent(i);
	double xsec_isr_sigma1_real = xsec_isr_real * frac_sigma1;
	double xsec_isr_sigma2_real = xsec_isr_real * frac_sigma2;
	double xsec_isr_sigma1_real_err = xsec_isr_sigma1_real * frac_sigma1_relerr;
	double xsec_isr_sigma2_real_err = xsec_isr_sigma2_real * frac_sigma2_relerr;
	h_sl_ctgsigma1_real->SetBinContent(i, xsec_isr_sigma1_real);
	h_sl_ctgsigma1_real->SetBinError(i, xsec_isr_sigma1_real_err);
	h_sl_ctgsigma2_real->SetBinContent(i, xsec_isr_sigma2_real);
	h_sl_ctgsigma2_real->SetBinError(i, xsec_isr_sigma2_real_err);
    }
    TH1F* h_sl_ctwsigma1_real = (TH1F*)h_sl_ctwsigma1->Clone("sl_ctwsigma1_real"); h_sl_ctwsigma1_real->Divide(h_sm_sl);
    TH1F* h_sl_ctwsigma2_real = (TH1F*)h_sl_ctwsigma2->Clone("sl_ctwsigma2_real"); h_sl_ctwsigma2_real->Divide(h_sm_sl);
    for (int i = 1; i <= h_sl_ctwsigma1_real->GetNbinsX(); i++) {
	double xsec_all_real = h_sm_sl_real->GetBinContent(i);
	double xsec_all = h_sm_sl_nominal->GetBinContent(i);
	double xsec_isr = h_sm_sl_nominal_isr->GetBinContent(i);
	double xsec_fsr = xsec_all - xsec_isr;
	double frac_isr = xsec_isr/(xsec_isr+xsec_fsr);
	double xsec_isr_real = xsec_all_real * frac_isr;
	double frac_sigma1 = h_sl_ctwsigma1_real->GetBinContent(i);
	double frac_sigma2 = h_sl_ctwsigma2_real->GetBinContent(i);
	double frac_sigma1_relerr = h_sl_ctwsigma1_real->GetBinError(i)/h_sl_ctwsigma1_real->GetBinContent(i);
	double frac_sigma2_relerr = h_sl_ctwsigma2_real->GetBinError(i)/h_sl_ctwsigma2_real->GetBinContent(i);
	double xsec_isr_sigma1_real = xsec_isr_real * frac_sigma1;
	double xsec_isr_sigma2_real = xsec_isr_real * frac_sigma2;
	double xsec_isr_sigma1_real_err = xsec_isr_sigma1_real * frac_sigma1_relerr;
	double xsec_isr_sigma2_real_err = xsec_isr_sigma2_real * frac_sigma2_relerr;
	h_sl_ctwsigma1_real->SetBinContent(i, xsec_isr_sigma1_real);
	h_sl_ctwsigma1_real->SetBinError(i, xsec_isr_sigma1_real_err);
	h_sl_ctwsigma2_real->SetBinContent(i, xsec_isr_sigma2_real);
	h_sl_ctwsigma2_real->SetBinError(i, xsec_isr_sigma2_real_err);
    }
    TH1F* h_ll_ctbsigma1_real = (TH1F*)h_ll_ctbsigma1->Clone("ll_ctbsigma1_real"); h_ll_ctbsigma1_real->Divide(h_sm_ll);
    TH1F* h_ll_ctbsigma2_real = (TH1F*)h_ll_ctbsigma2->Clone("ll_ctbsigma2_real"); h_ll_ctbsigma2_real->Divide(h_sm_ll);
    TH1F* h_sm_ll_frac = (TH1F*)h_sm_ll_real->Clone("sm_ll_frac");
    for (int i = 1; i <= h_ll_ctbsigma1_real->GetNbinsX(); i++) {
	double xsec_all_real = h_sm_ll_real->GetBinContent(i);
	double xsec_all = h_sm_ll_nominal->GetBinContent(i);
	double xsec_isr = h_sm_ll_nominal_isr->GetBinContent(i);
	double xsec_fsr = xsec_all - xsec_isr;
	double frac_isr = xsec_isr/(xsec_isr+xsec_fsr);
	h_sm_ll_frac->SetBinContent(i, frac_isr);
	h_sm_ll_frac->SetBinError(i, 0);
	double xsec_isr_real = xsec_all_real * frac_isr;
	double frac_sigma1 = h_ll_ctbsigma1_real->GetBinContent(i);
	double frac_sigma2 = h_ll_ctbsigma2_real->GetBinContent(i);
	double frac_sigma1_relerr = h_ll_ctbsigma1_real->GetBinError(i)/h_ll_ctbsigma1_real->GetBinContent(i);
	double frac_sigma2_relerr = h_ll_ctbsigma2_real->GetBinError(i)/h_ll_ctbsigma2_real->GetBinContent(i);
	double xsec_isr_sigma1_real = xsec_isr_real * frac_sigma1;
	double xsec_isr_sigma2_real = xsec_isr_real * frac_sigma2;
	double xsec_isr_sigma1_real_err = xsec_isr_sigma1_real * frac_sigma1_relerr;
	double xsec_isr_sigma2_real_err = xsec_isr_sigma2_real * frac_sigma2_relerr;
	h_ll_ctbsigma1_real->SetBinContent(i, xsec_isr_sigma1_real);
	h_ll_ctbsigma1_real->SetBinError(i, xsec_isr_sigma1_real_err);
	h_ll_ctbsigma2_real->SetBinContent(i, xsec_isr_sigma2_real);
	h_ll_ctbsigma2_real->SetBinError(i, xsec_isr_sigma2_real_err);
    }
    TH1F* h_ll_ctgsigma1_real = (TH1F*)h_ll_ctgsigma1->Clone("ll_ctgsigma1_real"); h_ll_ctgsigma1_real->Divide(h_sm_ll);
    TH1F* h_ll_ctgsigma2_real = (TH1F*)h_ll_ctgsigma2->Clone("ll_ctgsigma2_real"); h_ll_ctgsigma2_real->Divide(h_sm_ll);
    for (int i = 1; i <= h_ll_ctgsigma1_real->GetNbinsX(); i++) {
	double xsec_all_real = h_sm_ll_real->GetBinContent(i);
	double xsec_all = h_sm_ll_nominal->GetBinContent(i);
	double xsec_isr = h_sm_ll_nominal_isr->GetBinContent(i);
	double xsec_fsr = xsec_all - xsec_isr;
	double frac_isr = xsec_isr/(xsec_isr+xsec_fsr);
	double xsec_isr_real = xsec_all_real * frac_isr;
	double frac_sigma1 = h_ll_ctgsigma1_real->GetBinContent(i);
	double frac_sigma2 = h_ll_ctgsigma2_real->GetBinContent(i);
	double frac_sigma1_relerr = h_ll_ctgsigma1_real->GetBinError(i)/h_ll_ctgsigma1_real->GetBinContent(i);
	double frac_sigma2_relerr = h_ll_ctgsigma2_real->GetBinError(i)/h_ll_ctgsigma2_real->GetBinContent(i);
	double xsec_isr_sigma1_real = xsec_isr_real * frac_sigma1;
	double xsec_isr_sigma2_real = xsec_isr_real * frac_sigma2;
	double xsec_isr_sigma1_real_err = xsec_isr_sigma1_real * frac_sigma1_relerr;
	double xsec_isr_sigma2_real_err = xsec_isr_sigma2_real * frac_sigma2_relerr;
	h_ll_ctgsigma1_real->SetBinContent(i, xsec_isr_sigma1_real);
	h_ll_ctgsigma1_real->SetBinError(i, xsec_isr_sigma1_real_err);
	h_ll_ctgsigma2_real->SetBinContent(i, xsec_isr_sigma2_real);
	h_ll_ctgsigma2_real->SetBinError(i, xsec_isr_sigma2_real_err);
    }
    TH1F* h_ll_ctwsigma1_real = (TH1F*)h_ll_ctwsigma1->Clone("ll_ctwsigma1_real"); h_ll_ctwsigma1_real->Divide(h_sm_ll);
    TH1F* h_ll_ctwsigma2_real = (TH1F*)h_ll_ctwsigma2->Clone("ll_ctwsigma2_real"); h_ll_ctwsigma2_real->Divide(h_sm_ll);
    for (int i = 1; i <= h_ll_ctwsigma1_real->GetNbinsX(); i++) {
	double xsec_all_real = h_sm_ll_real->GetBinContent(i);
	double xsec_all = h_sm_ll_nominal->GetBinContent(i);
	double xsec_isr = h_sm_ll_nominal_isr->GetBinContent(i);
	double xsec_fsr = xsec_all - xsec_isr;
	double frac_isr = xsec_isr/(xsec_isr+xsec_fsr);
	double xsec_isr_real = xsec_all_real * frac_isr;
	double frac_sigma1 = h_ll_ctwsigma1_real->GetBinContent(i);
	double frac_sigma2 = h_ll_ctwsigma2_real->GetBinContent(i);
	double frac_sigma1_relerr = h_ll_ctwsigma1_real->GetBinError(i)/h_ll_ctwsigma1_real->GetBinContent(i);
	double frac_sigma2_relerr = h_ll_ctwsigma2_real->GetBinError(i)/h_ll_ctwsigma2_real->GetBinContent(i);
	double xsec_isr_sigma1_real = xsec_isr_real * frac_sigma1;
	double xsec_isr_sigma2_real = xsec_isr_real * frac_sigma2;
	double xsec_isr_sigma1_real_err = xsec_isr_sigma1_real * frac_sigma1_relerr;
	double xsec_isr_sigma2_real_err = xsec_isr_sigma2_real * frac_sigma2_relerr;
	h_ll_ctwsigma1_real->SetBinContent(i, xsec_isr_sigma1_real);
	h_ll_ctwsigma1_real->SetBinError(i, xsec_isr_sigma1_real_err);
	h_ll_ctwsigma2_real->SetBinContent(i, xsec_isr_sigma2_real);
	h_ll_ctwsigma2_real->SetBinError(i, xsec_isr_sigma2_real_err);
    }

    TH1F* h_sl_ctbsigma1_rel = (TH1F*)h_sl_ctbsigma1->Clone("sl_ctbsigma1_rel"); h_sl_ctbsigma1_rel->Divide(h_sm_sl);
    TH1F* h_sl_ctbsigma2_rel = (TH1F*)h_sl_ctbsigma2->Clone("sl_ctbsigma2_rel"); h_sl_ctbsigma2_rel->Divide(h_sm_sl);
    TH1F* h_sl_ctgsigma1_rel = (TH1F*)h_sl_ctgsigma1->Clone("sl_ctgsigma1_rel"); h_sl_ctgsigma1_rel->Divide(h_sm_sl);
    TH1F* h_sl_ctgsigma2_rel = (TH1F*)h_sl_ctgsigma2->Clone("sl_ctgsigma2_rel"); h_sl_ctgsigma2_rel->Divide(h_sm_sl);
    TH1F* h_sl_ctwsigma1_rel = (TH1F*)h_sl_ctwsigma1->Clone("sl_ctwsigma1_rel"); h_sl_ctwsigma1_rel->Divide(h_sm_sl);
    TH1F* h_sl_ctwsigma2_rel = (TH1F*)h_sl_ctwsigma2->Clone("sl_ctwsigma2_rel"); h_sl_ctwsigma2_rel->Divide(h_sm_sl);
    TH1F* h_ll_ctbsigma1_rel = (TH1F*)h_ll_ctbsigma1->Clone("ll_ctbsigma1_rel"); h_ll_ctbsigma1_rel->Divide(h_sm_ll);
    TH1F* h_ll_ctbsigma2_rel = (TH1F*)h_ll_ctbsigma2->Clone("ll_ctbsigma2_rel"); h_ll_ctbsigma2_rel->Divide(h_sm_ll);
    TH1F* h_ll_ctgsigma1_rel = (TH1F*)h_ll_ctgsigma1->Clone("ll_ctgsigma1_rel"); h_ll_ctgsigma1_rel->Divide(h_sm_ll);
    TH1F* h_ll_ctgsigma2_rel = (TH1F*)h_ll_ctgsigma2->Clone("ll_ctgsigma2_rel"); h_ll_ctgsigma2_rel->Divide(h_sm_ll);
    TH1F* h_ll_ctwsigma1_rel = (TH1F*)h_ll_ctwsigma1->Clone("ll_ctwsigma1_rel"); h_ll_ctwsigma1_rel->Divide(h_sm_ll);
    TH1F* h_ll_ctwsigma2_rel = (TH1F*)h_ll_ctwsigma2->Clone("ll_ctwsigma2_rel"); h_ll_ctwsigma2_rel->Divide(h_sm_ll);

    TH1F* h_sm_ctbsigma1_sl = (TH1F*)h_sm_sl_real->Clone("sm_ctbsigma1_sl");
    h_sm_ctbsigma1_sl->Add(h_sl_ctbsigma1_real);
    TH1F* h_sm_ctbsigma1_ctbsigma2_sl = (TH1F*)h_sm_sl_real->Clone("sm_ctbsigma1_ctbsigma2_sl");
    h_sm_ctbsigma1_ctbsigma2_sl->Add(h_sl_ctbsigma1_real);
    h_sm_ctbsigma1_ctbsigma2_sl->Add(h_sl_ctbsigma2_real);
    //for (int i = 1; i <= h_sm_ctbsigma1_sl->GetNbinsX(); i++) cout << h_sm_sl_real->GetBinContent(i) << " " << h_sm_ctbsigma1_sl->GetBinContent(i) << " " << h_sm_ctbsigma1_ctbsigma2_sl->GetBinContent(i) << endl;
    TH1F* h_sm_ctgsigma1_sl = (TH1F*)h_sm_sl_real->Clone("sm_ctgsigma1_sl");
    h_sm_ctgsigma1_sl->Add(h_sl_ctgsigma1_real);
    TH1F* h_sm_ctgsigma1_ctgsigma2_sl = (TH1F*)h_sm_sl_real->Clone("sm_ctgsigma1_ct_sl");
    h_sm_ctgsigma1_ctgsigma2_sl->Add(h_sl_ctgsigma1_real);
    h_sm_ctgsigma1_ctgsigma2_sl->Add(h_sl_ctgsigma2_real);
    TH1F* h_sm_ctwsigma1_sl = (TH1F*)h_sm_sl_real->Clone("sm_ctwsigma1_sl");
    h_sm_ctwsigma1_sl->Add(h_sl_ctwsigma1_real);
    TH1F* h_sm_ctwsigma1_ctwsigma2_sl = (TH1F*)h_sm_sl_real->Clone("sm_ctwsigma1_ctwsigma2_sl");
    h_sm_ctwsigma1_ctwsigma2_sl->Add(h_sl_ctwsigma1_real);
    h_sm_ctwsigma1_ctwsigma2_sl->Add(h_sl_ctwsigma2_real);

    TH1F* h_sm_ctbsigma1_ll = (TH1F*)h_sm_ll_real->Clone("sm_ctbsigma1_ll");
    h_sm_ctbsigma1_ll->Add(h_ll_ctbsigma1_real);
    TH1F* h_sm_ctbsigma1_ctbsigma2_ll = (TH1F*)h_sm_ll_real->Clone("sm_ctbsigma1_ctbsigma2_ll");
    h_sm_ctbsigma1_ctbsigma2_ll->Add(h_ll_ctbsigma1_real);
    h_sm_ctbsigma1_ctbsigma2_ll->Add(h_ll_ctbsigma2_real);
    TH1F* h_sm_ctgsigma1_ll = (TH1F*)h_sm_ll_real->Clone("sm_ctgsigma1_ll");
    h_sm_ctgsigma1_ll->Add(h_ll_ctgsigma1_real);
    TH1F* h_sm_ctgsigma1_ctgsigma2_ll = (TH1F*)h_sm_ll_real->Clone("sm_ctgsigma1_ct_ll");
    h_sm_ctgsigma1_ctgsigma2_ll->Add(h_ll_ctgsigma1_real);
    h_sm_ctgsigma1_ctgsigma2_ll->Add(h_ll_ctgsigma2_real);
    TH1F* h_sm_ctwsigma1_ll = (TH1F*)h_sm_ll_real->Clone("sm_ctwsigma1_ll");
    h_sm_ctwsigma1_ll->Add(h_ll_ctwsigma1_real);
    TH1F* h_sm_ctwsigma1_ctwsigma2_ll = (TH1F*)h_sm_ll_real->Clone("sm_ctwsigma1_ctwsigma2_ll");
    h_sm_ctwsigma1_ctwsigma2_ll->Add(h_ll_ctwsigma1_real);
    h_sm_ctwsigma1_ctwsigma2_ll->Add(h_ll_ctwsigma2_real);

    format_histogram(h_sm_sl_nominal_isr);
    format_histogram(h_sm_sl_nominal);
    format_histogram(h_sm_sl);
    format_histogram(h_sm_sl_real);
    format_histogram(h_sm_ctbm_sl);
    format_histogram(h_sm_ctbp_sl);
    format_histogram(h_sl_ctbsigma1);
    format_histogram(h_sl_ctbsigma2);
    format_histogram(h_sl_ctbsigma1_real);
    format_histogram(h_sl_ctbsigma2_real);
    format_histogram(h_sm_ctbsigma1_sl);
    format_histogram(h_sm_ctbsigma1_ctbsigma2_sl);
    format_histogram(h_sm_ctgm_sl);
    format_histogram(h_sm_ctgp_sl);
    format_histogram(h_sl_ctgsigma1);
    format_histogram(h_sl_ctgsigma2);
    format_histogram(h_sl_ctgsigma1_real);
    format_histogram(h_sl_ctgsigma2_real);
    format_histogram(h_sm_ctgsigma1_sl);
    format_histogram(h_sm_ctgsigma1_ctgsigma2_sl);
    format_histogram(h_sm_ctwm_sl);
    format_histogram(h_sm_ctwp_sl);
    format_histogram(h_sl_ctwsigma1);
    format_histogram(h_sl_ctwsigma2);
    format_histogram(h_sl_ctwsigma1_real);
    format_histogram(h_sl_ctwsigma2_real);
    format_histogram(h_sm_ctwsigma1_sl);
    format_histogram(h_sm_ctwsigma1_ctwsigma2_sl);

    format_histogram(h_sm_ll_nominal_isr);
    format_histogram(h_sm_ll_nominal);
    format_histogram(h_sm_ll);
    format_histogram(h_sm_ll_real);
    format_histogram(h_sm_ctbm_ll);
    format_histogram(h_sm_ctbp_ll);
    format_histogram(h_ll_ctbsigma1);
    format_histogram(h_ll_ctbsigma2);
    format_histogram(h_ll_ctbsigma1_real);
    format_histogram(h_ll_ctbsigma2_real);
    format_histogram(h_sm_ctbsigma1_ll);
    format_histogram(h_sm_ctbsigma1_ctbsigma2_ll);
    format_histogram(h_sm_ctgm_ll);
    format_histogram(h_sm_ctgp_ll);
    format_histogram(h_ll_ctgsigma1);
    format_histogram(h_ll_ctgsigma2);
    format_histogram(h_ll_ctgsigma1_real);
    format_histogram(h_ll_ctgsigma2_real);
    format_histogram(h_sm_ctgsigma1_ll);
    format_histogram(h_sm_ctgsigma1_ctgsigma2_ll);
    format_histogram(h_sm_ctwm_ll);
    format_histogram(h_sm_ctwp_ll);
    format_histogram(h_ll_ctwsigma1);
    format_histogram(h_ll_ctwsigma2);
    format_histogram(h_ll_ctwsigma1_real);
    format_histogram(h_ll_ctwsigma2_real);
    format_histogram(h_sm_ctwsigma1_ll);
    format_histogram(h_sm_ctwsigma1_ctwsigma2_ll);

    double int_sm_sl_real = 0;
    for (int i = 1; i <= h_sm_sl_real->GetNbinsX(); i++) {
	int_sm_sl_real += h_sm_sl_real->GetBinWidth(i) * h_sm_sl_real->GetBinContent(i);
    }
    double int_sm_ll_real = 0;
    for (int i = 1; i <= h_sm_ll_real->GetNbinsX(); i++) {
	int_sm_ll_real += h_sm_ll_real->GetBinWidth(i) * h_sm_ll_real->GetBinContent(i);
    }
    cout << "Integral " << int_sm_sl_real << endl;
    cout << "Integral " << int_sm_ll_real << endl;

    cout << fixed << setprecision(1) << endl;
    for (int i = 1; i <= h_sm_sl_real->GetNbinsX(); i++) {
	char tmp[50];
	sprintf(tmp, "%.3f +/- %.3f", h_sm_sl_real->GetBinContent(i), h_sm_sl_real->GetBinError(i));
	cout << setw(25) << tmp;
	if (i != h_sm_sl_real->GetNbinsX()) cout << "&";
	else cout << "\\\\" << endl;
    }
    for (int i = 1; i <= h_sl_ctbsigma1_real->GetNbinsX(); i++) {
	char tmp[50];
	sprintf(tmp, "%.3f +/- %.3f", h_sl_ctbsigma1_real->GetBinContent(i), h_sl_ctbsigma1_real->GetBinError(i));
	cout << setw(25) << tmp;
	if (i != h_sl_ctbsigma1_real->GetNbinsX()) cout << "&";
	else cout << "\\\\" << endl;
    }
    for (int i = 1; i <= h_sl_ctbsigma2_real->GetNbinsX(); i++) {
	char tmp[50];
	sprintf(tmp, "%.3f +/- %.3f", h_sl_ctbsigma2_real->GetBinContent(i), h_sl_ctbsigma2_real->GetBinError(i));
	cout << setw(25) << tmp;
	if (i != h_sl_ctbsigma2_real->GetNbinsX()) cout << "&";
	else cout << "\\\\" << endl;
    }
    for (int i = 1; i <= h_sl_ctgsigma1_real->GetNbinsX(); i++) {
	char tmp[50];
	sprintf(tmp, "%.3f +/- %.3f", h_sl_ctgsigma1_real->GetBinContent(i), h_sl_ctgsigma1_real->GetBinError(i));
	cout << setw(25) << tmp;
	if (i != h_sl_ctgsigma1_real->GetNbinsX()) cout << "&";
	else cout << "\\\\" << endl;
    }
    for (int i = 1; i <= h_sl_ctgsigma2_real->GetNbinsX(); i++) {
	char tmp[50];
	sprintf(tmp, "%.3f +/- %.3f", h_sl_ctgsigma2_real->GetBinContent(i), h_sl_ctgsigma2_real->GetBinError(i));
	cout << setw(25) << tmp;
	if (i != h_sl_ctgsigma2_real->GetNbinsX()) cout << "&";
	else cout << "\\\\" << endl;
    }
    for (int i = 1; i <= h_sl_ctwsigma1_real->GetNbinsX(); i++) {
	char tmp[50];
	sprintf(tmp, "%.3f +/- %.3f", h_sl_ctwsigma1_real->GetBinContent(i), h_sl_ctwsigma1_real->GetBinError(i));
	cout << setw(25) << tmp;
	if (i != h_sl_ctwsigma1_real->GetNbinsX()) cout << "&";
	else cout << "\\\\" << endl;
    }
    for (int i = 1; i <= h_sl_ctwsigma2_real->GetNbinsX(); i++) {
	char tmp[50];
	sprintf(tmp, "%.3f +/- %.3f", h_sl_ctwsigma2_real->GetBinContent(i), h_sl_ctwsigma2_real->GetBinError(i));
	cout << setw(25) << tmp;
	if (i != h_sl_ctwsigma2_real->GetNbinsX()) cout << "&";
	else cout << "\\\\" << endl;
    }

    for (int i = 1; i <= h_sm_ll_real->GetNbinsX(); i++) {
	char tmp[50];
	sprintf(tmp, "%.3f +/- %.3f", h_sm_ll_real->GetBinContent(i), h_sm_ll_real->GetBinError(i));
	cout << setw(25) << tmp;
	if (i != h_sm_ll_real->GetNbinsX()) cout << "&";
	else cout << "\\\\" << endl;
    }
    for (int i = 1; i <= h_ll_ctbsigma1_real->GetNbinsX(); i++) {
	char tmp[50];
	sprintf(tmp, "%.3f +/- %.3f", h_ll_ctbsigma1_real->GetBinContent(i), h_ll_ctbsigma1_real->GetBinError(i));
	cout << setw(25) << tmp;
	if (i != h_ll_ctbsigma1_real->GetNbinsX()) cout << "&";
	else cout << "\\\\" << endl;
    }
    for (int i = 1; i <= h_ll_ctbsigma2_real->GetNbinsX(); i++) {
	char tmp[50];
	sprintf(tmp, "%.3f +/- %.3f", h_ll_ctbsigma2_real->GetBinContent(i), h_ll_ctbsigma2_real->GetBinError(i));
	cout << setw(25) << tmp;
	if (i != h_ll_ctbsigma2_real->GetNbinsX()) cout << "&";
	else cout << "\\\\" << endl;
    }
    for (int i = 1; i <= h_ll_ctgsigma1_real->GetNbinsX(); i++) {
	char tmp[50];
	sprintf(tmp, "%.3f +/- %.3f", h_ll_ctgsigma1_real->GetBinContent(i), h_ll_ctgsigma1_real->GetBinError(i));
	cout << setw(25) << tmp;
	if (i != h_ll_ctgsigma1_real->GetNbinsX()) cout << "&";
	else cout << "\\\\" << endl;
    }
    for (int i = 1; i <= h_ll_ctgsigma2_real->GetNbinsX(); i++) {
	char tmp[50];
	sprintf(tmp, "%.3f +/- %.3f", h_ll_ctgsigma2_real->GetBinContent(i), h_ll_ctgsigma2_real->GetBinError(i));
	cout << setw(25) << tmp;
	if (i != h_ll_ctgsigma2_real->GetNbinsX()) cout << "&";
	else cout << "\\\\" << endl;
    }
    for (int i = 1; i <= h_ll_ctwsigma1_real->GetNbinsX(); i++) {
	char tmp[50];
	sprintf(tmp, "%.3f +/- %.3f", h_ll_ctwsigma1_real->GetBinContent(i), h_ll_ctwsigma1_real->GetBinError(i));
	cout << setw(25) << tmp;
	if (i != h_ll_ctwsigma1_real->GetNbinsX()) cout << "&";
	else cout << "\\\\" << endl;
    }
    for (int i = 1; i <= h_ll_ctwsigma2_real->GetNbinsX(); i++) {
	char tmp[50];
	sprintf(tmp, "%.3f +/- %.3f", h_ll_ctwsigma2_real->GetBinContent(i), h_ll_ctwsigma2_real->GetBinError(i));
	cout << setw(25) << tmp;
	if (i != h_ll_ctwsigma2_real->GetNbinsX()) cout << "&";
	else cout << "\\\\" << endl;
    }

    if (emuonly) {
	TFile* fout = new TFile("results/Upgrade/ForEFTFitter.root", "recreate");
    	fout->cd();
    	h_sm_sl_real->Write("sm_sl");
	h_sm_sl_frac->Write("sm_sl_frac");
    	h_sm_ll_real->Write("sm_ll");
	h_sm_ll_frac->Write("sm_ll_frac");
    	h_sl_ctbsigma1_real->Write("sl_ctbsigma1");
    	h_sl_ctbsigma2_real->Write("sl_ctbsigma2");
    	h_sl_ctgsigma1_real->Write("sl_ctgsigma1");
    	h_sl_ctgsigma2_real->Write("sl_ctgsigma2");
    	h_sl_ctwsigma1_real->Write("sl_ctwsigma1");
    	h_sl_ctwsigma2_real->Write("sl_ctwsigma2");
    	h_ll_ctbsigma1_real->Write("ll_ctbsigma1");
    	h_ll_ctbsigma2_real->Write("ll_ctbsigma2");
    	h_ll_ctgsigma1_real->Write("ll_ctgsigma1");
    	h_ll_ctgsigma2_real->Write("ll_ctgsigma2");
    	h_ll_ctwsigma1_real->Write("ll_ctwsigma1");
    	h_ll_ctwsigma2_real->Write("ll_ctwsigma2");
    	h_sl_ctbsigma1_rel->Write("sl_ctbsigma1_rel");
    	h_sl_ctbsigma2_rel->Write("sl_ctbsigma2_rel");
    	h_sl_ctgsigma1_rel->Write("sl_ctgsigma1_rel");
    	h_sl_ctgsigma2_rel->Write("sl_ctgsigma2_rel");
    	h_sl_ctwsigma1_rel->Write("sl_ctwsigma1_rel");
    	h_sl_ctwsigma2_rel->Write("sl_ctwsigma2_rel");
    	h_ll_ctbsigma1_rel->Write("ll_ctbsigma1_rel");
    	h_ll_ctbsigma2_rel->Write("ll_ctbsigma2_rel");
    	h_ll_ctgsigma1_rel->Write("ll_ctgsigma1_rel");
    	h_ll_ctgsigma2_rel->Write("ll_ctgsigma2_rel");
    	h_ll_ctwsigma1_rel->Write("ll_ctwsigma1_rel");
    	h_ll_ctwsigma2_rel->Write("ll_ctwsigma2_rel");
    }
    
    PlotComparor* PC = new PlotComparor();
    PC->UseLogy(true, 0.000001, 10000);
    PC->DrawRatio(true);
    PC->SetSaveDir("plots/Upgrade/");
    PC->NormToUnit(false);
    string drawoptions = "hist";
    drawoptions += " _e";
    PC->SetLineWidth(2);
    PC->SetDrawOption(drawoptions.c_str());
    string savename;
    
    PC->ClearAlterHs();
    PC->SetBaseH(h_sm_sl_nominal);
    PC->SetBaseHName("All sources");
    PC->ShiftLegendY(0.01);
    PC->ShiftLegendX(0.05);
    PC->AddAlterH(h_sm_sl_nominal_isr);
    PC->AddAlterHName("ISR + off-shell top");
    savename = "CompareEFT_ph_pt_sl_isr";
    PC->SetSaveName(savename);
    PC->Is14TeV(true,false);
    PC->IsSimulation(true);
    PC->SetMaxRatio(1.2);
    PC->SetMinRatio(0);
    PC->SetChannel("Single lepton");
    PC->SetYtitle("#frac{d#sigma}{dp_{T}(#gamma)} [fb/GeV]");
    PC->SetYtitleOffset(1.4);
    PC->SetXtitle("p_{T}(#gamma) [GeV]");
    PC->SetRatioYtitle("Ratio");
    PC->SetATLASFlag(2);
    PC->Compare();
    PC->UseLogy(true, 0.0000001, 1000);

    PC->ClearAlterHs();
    PC->SetBaseH(h_sm_ll_nominal);
    PC->SetBaseHName("All sources");
    PC->ShiftLegendY(0.01);
    PC->ShiftLegendX(0.05);
    PC->AddAlterH(h_sm_ll_nominal_isr);
    PC->AddAlterHName("ISR + off-shell top");
    savename = "CompareEFT_ph_pt_ll_isr";
    PC->SetSaveName(savename);
    PC->Is14TeV(true,false);
    PC->IsSimulation(true);
    PC->SetMaxRatio(1.2);
    PC->SetMinRatio(0);
    PC->SetChannel("Dilepton");
    PC->SetYtitle("#frac{d#sigma}{dp_{T}(#gamma)} [fb/GeV]");
    PC->SetYtitleOffset(1.4);
    PC->SetXtitle("p_{T}(#gamma) [GeV]");
    PC->SetRatioYtitle("Ratio");
    PC->SetATLASFlag(2);
    PC->Compare();

    PC->UseLogy(true, 0.00001, 10);

    PC->ClearAlterHs();
    PC->SetBaseH(h_sm_sl_real);
    PC->SetBaseHName("SM");
    PC->AddAlterH(h_sm_ctbsigma1_sl);
    PC->AddAlterHName("SM + #sigma_{1,tB}");
    PC->AddAlterH(h_sm_ctbsigma1_ctbsigma2_sl);
    PC->AddAlterHName("SM + #sigma_{1,tB} + #sigma_{2,tB}");
    savename = "CompareEFT_ph_pt_sl_ctb12";
    PC->SetSaveName(savename);
    PC->Is14TeV(true,false);
    PC->IsSimulation(true);
    PC->SetMaxRatio(5);
    PC->SetMinRatio(0.7);
    PC->SetChannel("Single lepton");
    PC->SetYtitle("#frac{d#sigma}{dp_{T}(#gamma)} [fb/GeV]");
    PC->SetYtitleOffset(1.4);
    PC->SetXtitle("p_{T}(#gamma) [GeV]");
    PC->SetYtitleOffset(1.4);
    PC->SetXtitle("p_{T}(#gamma) [GeV]");
    PC->ShiftLegendX(0.4);
    PC->Compare();

    PC->ClearAlterHs();
    PC->SetBaseH(h_sm_sl_real);
    PC->SetBaseHName("SM");
    PC->AddAlterH(h_sm_ctgsigma1_sl);
    PC->AddAlterHName("SM + #sigma_{1,tG}");
    PC->AddAlterH(h_sm_ctgsigma1_ctgsigma2_sl);
    PC->AddAlterHName("SM + #sigma_{1,tG} + #sigma_{2,tG}");
    savename = "CompareEFT_ph_pt_sl_ctg12";
    PC->SetSaveName(savename);
    PC->Is14TeV(true,false);
    PC->IsSimulation(true);
    PC->SetMaxRatio(1.7);
    PC->SetMinRatio(0.3);
    PC->SetChannel("Single lepton");
    PC->SetYtitle("#frac{d#sigma}{dp_{T}(#gamma)} [fb/GeV]");
    PC->SetYtitleOffset(1.4);
    PC->SetXtitle("p_{T}(#gamma) [GeV]");
    PC->ShiftLegendX(0.4);
    PC->Compare();

    PC->ClearAlterHs();
    PC->SetBaseH(h_sm_sl_real);
    PC->SetBaseHName("SM");
    PC->AddAlterH(h_sm_ctwsigma1_sl);
    PC->AddAlterHName("SM + #sigma_{1,tW}");
    PC->AddAlterH(h_sm_ctwsigma1_ctwsigma2_sl);
    PC->AddAlterHName("SM + #sigma_{1,tW} + #sigma_{2,tW}");
    savename = "CompareEFT_ph_pt_sl_ctw12";
    PC->SetSaveName(savename);
    PC->Is14TeV(true,false);
    PC->IsSimulation(true);
    PC->SetMaxRatio(1.7);
    PC->SetMinRatio(0.3);
    PC->SetChannel("Single lepton");
    PC->SetYtitle("#frac{d#sigma}{dp_{T}(#gamma)} [fb/GeV]");
    PC->SetYtitleOffset(1.4);
    PC->SetXtitle("p_{T}(#gamma) [GeV]");
    PC->ShiftLegendX(0.4);
    PC->Compare();

    PC->ClearAlterHs();
    PC->SetBaseH(h_sm_ll_real);
    PC->SetBaseHName("SM");
    PC->AddAlterH(h_sm_ctbsigma1_ll);
    PC->AddAlterHName("SM + #sigma_{1,tB}");
    PC->AddAlterH(h_sm_ctbsigma1_ctbsigma2_ll);
    PC->AddAlterHName("SM + #sigma_{1,tB} + #sigma_{2,tB}");
    savename = "CompareEFT_ph_pt_ll_ctb12";
    PC->SetSaveName(savename);
    PC->Is14TeV(true,false);
    PC->IsSimulation(true);
    PC->SetMaxRatio(5);
    PC->SetMinRatio(0.7);
    PC->SetChannel("Dilepton");
    PC->SetYtitle("#frac{d#sigma}{dp_{T}(#gamma)} [fb/GeV]");
    PC->SetYtitleOffset(1.4);
    PC->SetXtitle("p_{T}(#gamma) [GeV]");
    PC->ShiftLegendX(0.4);
    PC->Compare();

    PC->ClearAlterHs();
    PC->SetBaseH(h_sm_ll_real);
    PC->SetBaseHName("SM");
    PC->AddAlterH(h_sm_ctgsigma1_ll);
    PC->AddAlterHName("SM + #sigma_{1,tG}");
    PC->AddAlterH(h_sm_ctgsigma1_ctgsigma2_ll);
    PC->AddAlterHName("SM + #sigma_{1,tG} + #sigma_{2,tG}");
    savename = "CompareEFT_ph_pt_ll_ctg12";
    PC->SetSaveName(savename);
    PC->Is14TeV(true,false);
    PC->IsSimulation(true);
    PC->SetMaxRatio(1.7);
    PC->SetMinRatio(0.3);
    PC->SetChannel("Dilepton");
    PC->SetYtitle("#frac{d#sigma}{dp_{T}(#gamma)} [fb/GeV]");
    PC->SetYtitleOffset(1.4);
    PC->SetXtitle("p_{T}(#gamma) [GeV]");
    PC->ShiftLegendX(0.4);
    PC->Compare();

    PC->ClearAlterHs();
    PC->SetBaseH(h_sm_ll_real);
    PC->SetBaseHName("SM");
    PC->AddAlterH(h_sm_ctwsigma1_ll);
    PC->AddAlterHName("SM + #sigma_{1,tW}");
    PC->AddAlterH(h_sm_ctwsigma1_ctwsigma2_ll);
    PC->AddAlterHName("SM + #sigma_{1,tW} + #sigma_{2,tW}");
    savename = "CompareEFT_ph_pt_ll_ctw12";
    PC->SetSaveName(savename);
    PC->Is14TeV(true,false);
    PC->IsSimulation(true);
    PC->SetMaxRatio(1.7);
    PC->SetMinRatio(0.3);
    PC->SetChannel("Dilepton");
    PC->SetYtitle("#frac{d#sigma}{dp_{T}(#gamma)} [fb/GeV]");
    PC->SetYtitleOffset(1.4);
    PC->SetXtitle("p_{T}(#gamma) [GeV]");
    PC->ShiftLegendX(0.4);
    PC->Compare();

    PC->UseLogy(true, 0.0000001, 1000);

    PC->ClearAlterHs();
    PC->SetBaseH(h_sm_sl);
    PC->SetBaseHName("SM");
    PC->AddAlterH(h_sm_ctbm_sl);
    PC->AddAlterHName("C_{tB} = -5");
    PC->AddAlterH(h_sm_ctbp_sl);
    PC->AddAlterHName("C_{tB} = +5");
    savename = "CompareEFT_ph_pt_sl_ctbmp";
    PC->SetSaveName(savename);
    PC->Is14TeV(true,false);
    PC->IsSimulation(true);
    PC->SetMaxRatio(5);
    PC->SetMinRatio(0.7);
    PC->SetChannel("Single lepton");
    PC->SetYtitle("#frac{d#sigma}{dp_{T}(#gamma)} [fb/GeV]");
    PC->SetYtitleOffset(1.4);
    PC->SetXtitle("p_{T}(#gamma) [GeV]");
    PC->Compare();

    PC->UseLogy(true, 0.0000001, 10);

    PC->ClearAlterHs();
    PC->SetBaseH(h_sm_sl);
    PC->SetBaseHName("SM");
    PC->AddAlterH(h_sm_ctgm_sl);
    PC->AddAlterHName("C_{tG} = -0.3");
    PC->AddAlterH(h_sm_ctgp_sl);
    PC->AddAlterHName("C_{tG} = +0.3");
    savename = "CompareEFT_ph_pt_sl_ctgmp";
    PC->SetSaveName(savename);
    PC->Is14TeV(true,false);
    PC->IsSimulation(true);
    PC->SetMaxRatio(1.7);
    PC->SetMinRatio(0.3);
    PC->SetChannel("Single lepton");
    PC->SetYtitle("#frac{d#sigma}{dp_{T}(#gamma)} [fb/GeV]");
    PC->SetYtitleOffset(1.4);
    PC->SetXtitle("p_{T}(#gamma) [GeV]");
    PC->Compare();

    PC->UseLogy(true, 0.0000001, 10);

    PC->ClearAlterHs();
    PC->SetBaseH(h_sm_sl);
    PC->SetBaseHName("SM");
    PC->AddAlterH(h_sm_ctwm_sl);
    PC->AddAlterHName("C_{tW} = -2");
    PC->AddAlterH(h_sm_ctwp_sl);
    PC->AddAlterHName("C_{tW} = +2");
    savename = "CompareEFT_ph_pt_sl_ctwmp";
    PC->SetSaveName(savename);
    PC->Is14TeV(true,false);
    PC->IsSimulation(true);
    PC->SetMaxRatio(1.7);
    PC->SetMinRatio(0.3);
    PC->SetChannel("Single lepton");
    PC->SetYtitle("#frac{d#sigma}{dp_{T}(#gamma)} [fb/GeV]");
    PC->SetYtitleOffset(1.4);
    PC->SetXtitle("p_{T}(#gamma) [GeV]");
    PC->Compare();

    PC->UseLogy(true, 0.0000001, 1000);

    PC->ClearAlterHs();
    PC->SetBaseH(h_sm_ll);
    PC->SetBaseHName("SM");
    PC->AddAlterH(h_sm_ctbm_ll);
    PC->AddAlterHName("C_{tB} = -5");
    PC->AddAlterH(h_sm_ctbp_ll);
    PC->AddAlterHName("C_{tB} = +5");
    savename = "CompareEFT_ph_pt_ll_ctbmp";
    PC->SetSaveName(savename);
    PC->Is14TeV(true,false);
    PC->IsSimulation(true);
    PC->SetMaxRatio(5);
    PC->SetMinRatio(0.7);
    PC->SetChannel("Dilepton");
    PC->SetYtitle("#frac{d#sigma}{dp_{T}(#gamma)} [fb/GeV]");
    PC->SetYtitleOffset(1.4);
    PC->SetXtitle("p_{T}(#gamma) [GeV]");
    PC->Compare();

    PC->UseLogy(true, 0.0000001, 10);

    PC->ClearAlterHs();
    PC->SetBaseH(h_sm_ll);
    PC->SetBaseHName("SM");
    PC->AddAlterH(h_sm_ctgm_ll);
    PC->AddAlterHName("C_{tG} = -0.3");
    PC->AddAlterH(h_sm_ctgp_ll);
    PC->AddAlterHName("C_{tG} = +0.3");
    savename = "CompareEFT_ph_pt_ll_ctgmp";
    PC->SetSaveName(savename);
    PC->Is14TeV(true,false);
    PC->IsSimulation(true);
    PC->SetMaxRatio(1.7);
    PC->SetMinRatio(0.3);
    PC->SetChannel("Dilepton");
    PC->SetYtitle("#frac{d#sigma}{dp_{T}(#gamma)} [fb/GeV]");
    PC->SetYtitleOffset(1.4);
    PC->SetXtitle("p_{T}(#gamma) [GeV]");
    PC->Compare();

    PC->UseLogy(true, 0.0000001, 10);

    PC->ClearAlterHs();
    PC->SetBaseH(h_sm_ll);
    PC->SetBaseHName("SM");
    PC->AddAlterH(h_sm_ctwm_ll);
    PC->AddAlterHName("C_{tW} = -2");
    PC->AddAlterH(h_sm_ctwp_ll);
    PC->AddAlterHName("C_{tW} = +2");
    savename = "CompareEFT_ph_pt_ll_ctwmp";
    PC->SetSaveName(savename);
    PC->Is14TeV(true,false);
    PC->IsSimulation(true);
    PC->SetMaxRatio(1.7);
    PC->SetMinRatio(0.3);
    PC->SetChannel("Dilepton");
    PC->SetYtitle("#frac{d#sigma}{dp_{T}(#gamma)} [fb/GeV]");
    PC->SetYtitleOffset(1.4);
    PC->SetXtitle("p_{T}(#gamma) [GeV]");
    PC->Compare();
}

void format_histogram(TH1F* h){
    h->Scale(1./3000.);
    for (int i = 1; i <= h->GetNbinsX(); i++) {
	double binwidth = h->GetBinWidth(i);
	h->SetBinContent(i, h->GetBinContent(i)/binwidth);
	h->SetBinError(i, h->GetBinError(i)/binwidth);
    }
}

void smooth_histogram(TH1F* hvar, TH1F* hnom) {
    TH1F* h_ratio = (TH1F*)hvar->Clone(string(hvar->GetName()+string("_Tmp")).c_str());
    h_ratio->Divide(hnom);
    //h_ratio->Smooth();
    //string hname = hvar->GetName();
    //if (hname.find("ctb",0) == string::npos) {
    //    h_ratio->Scale(0.00000);
    //}
    for (int i = 1; i <= hnom->GetNbinsX(); i++) {
	double xsec_nom = hnom->GetBinContent(i);
	double ratio = h_ratio->GetBinContent(i);
	double relerr = hvar->GetBinError(i)/hvar->GetBinContent(i);
	hvar->SetBinContent(i, xsec_nom*ratio);
	hvar->SetBinError(i, relerr*hvar->GetBinContent(i));
    }
}

void MergeHistograms(TH1F* h1, TH1F* h2, TH1F* hm, TH1F* hp, TH1F* hn) {
    for (int i = 1; i <= hn->GetNbinsX(); i++) {
	double xsec_m = hm->GetBinContent(i);
	double xsec_p = hp->GetBinContent(i);
	double xsec_n = hn->GetBinContent(i);
	double xsec_m_relerr = hm->GetBinError(i)/hm->GetBinContent(i);
	double xsec_p_relerr = hp->GetBinError(i)/hp->GetBinContent(i);
	double xsec_n_relerr = hn->GetBinError(i)/hn->GetBinContent(i);
	double xsec_m_err = xsec_m*xsec_m_relerr;
	double xsec_p_err = xsec_p*xsec_p_relerr;
	double xsec_n_err = xsec_n*xsec_n_relerr;
	double xsec_sigma1 = (xsec_p - xsec_m)/2;
	double xsec_sigma2 = (xsec_p + xsec_m)/2 - xsec_n;
	double xsec_sigma1_err = sqrt(pow(xsec_p_err/2,2) + pow(xsec_m_err/2,2));
	double xsec_sigma2_err = sqrt(pow(xsec_p_err/2,2) + pow(xsec_m_err/2,2) + pow(xsec_n_err,2));

	h1->SetBinContent(i, xsec_sigma1);
	h1->SetBinError(i, xsec_sigma1_err);
	h2->SetBinContent(i, xsec_sigma2);
	h2->SetBinError(i, xsec_sigma2_err);
    }
}
