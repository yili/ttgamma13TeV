{
    TFile* f_tty = new TFile("results/Nominal/Sig_Sys_ScalePdf.root");
    TFile* f_tt = new TFile("results/Nominal/Sig_Sys_TTBar.root");

    TCanvas*c = new TCanvas("c","c",800,600);
    c->Divide(5,2);

    int subc = 0;

    {
	subc++;
    	c->cd(subc); string name = "reco_ej_dr_truth_ph_parton_b1";
    	TH1F*h_tty = (TH1F*)f_tty->Get(name.c_str());
    	TH1F*h_tt = (TH1F*)f_tt->Get(name.c_str());
    	h_tty->SetLineColor(kRed);
    	h_tt->SetLineColor(kBlue);
    	h_tty->Scale(1/h_tty->Integral());
    	h_tt->Scale(1/h_tt->Integral());
	h_tt->GetXaxis()->SetTitle(name.c_str());
    	h_tt->Draw();
    	h_tty->Draw("same");
    }
    {
	subc++;
    	c->cd(subc); string name = "reco_ej_dr_truth_ph_parton_b2";
    	TH1F*h_tty = (TH1F*)f_tty->Get(name.c_str());
    	TH1F*h_tt = (TH1F*)f_tt->Get(name.c_str());
    	h_tty->SetLineColor(kRed);
    	h_tt->SetLineColor(kBlue);
    	h_tty->Scale(1/h_tty->Integral());
    	h_tt->Scale(1/h_tt->Integral());
	h_tt->GetXaxis()->SetTitle(name.c_str());
    	h_tt->Draw();
    	h_tty->Draw("same");
    }
    {
	subc++;
    	c->cd(subc); string name = "reco_ej_dr_truth_ph_parton_el1";
    	TH1F*h_tty = (TH1F*)f_tty->Get(name.c_str());
    	TH1F*h_tt = (TH1F*)f_tt->Get(name.c_str());
    	h_tty->SetLineColor(kRed);
    	h_tt->SetLineColor(kBlue);
    	h_tty->Scale(1/h_tty->Integral());
    	h_tt->Scale(1/h_tt->Integral());
	h_tt->GetXaxis()->SetTitle(name.c_str());
    	h_tt->Draw();
    	h_tty->Draw("same");
    }
    {
	subc++;
    	c->cd(subc); string name = "reco_ej_dr_truth_ph_parton_q1";
    	TH1F*h_tty = (TH1F*)f_tty->Get(name.c_str());
    	TH1F*h_tt = (TH1F*)f_tt->Get(name.c_str());
    	h_tty->SetLineColor(kRed);
    	h_tt->SetLineColor(kBlue);
    	h_tty->Scale(1/h_tty->Integral());
    	h_tt->Scale(1/h_tt->Integral());
	h_tt->GetXaxis()->SetTitle(name.c_str());
    	h_tt->Draw();
    	h_tty->Draw("same");
    }
    {
	subc++;
    	c->cd(subc); string name = "reco_ej_dr_truth_ph_parton_q2";
    	TH1F*h_tty = (TH1F*)f_tty->Get(name.c_str());
    	TH1F*h_tt = (TH1F*)f_tt->Get(name.c_str());
    	h_tty->SetLineColor(kRed);
    	h_tt->SetLineColor(kBlue);
    	h_tty->Scale(1/h_tty->Integral());
    	h_tt->Scale(1/h_tt->Integral());
	h_tt->GetXaxis()->SetTitle(name.c_str());
    	h_tt->Draw();
    	h_tty->Draw("same");
    }
//    {
//	subc++;
//    	c->cd(subc); string name = "reco_ej_dr_truth_ph_qcd_recoil";
//    	TH1F*h_tty = (TH1F*)f_tty->Get(name.c_str());
//    	TH1F*h_tt = (TH1F*)f_tt->Get(name.c_str());
//    	h_tty->SetLineColor(kRed);
//    	h_tt->SetLineColor(kBlue);
//    	h_tty->Scale(1/h_tty->Integral());
//    	h_tt->Scale(1/h_tt->Integral());
//	h_tt->GetXaxis()->SetTitle(name.c_str());
//    	h_tt->Draw();
//    	h_tty->Draw("same");
//    }
    {
	subc++;
    	c->cd(subc); string name = "reco_emu_dr_truth_ph_parton_b1";
    	TH1F*h_tty = (TH1F*)f_tty->Get(name.c_str());
    	TH1F*h_tt = (TH1F*)f_tt->Get(name.c_str());
    	h_tty->SetLineColor(kRed);
    	h_tt->SetLineColor(kBlue);
    	h_tty->Scale(1/h_tty->Integral());
    	h_tt->Scale(1/h_tt->Integral());
	h_tt->GetXaxis()->SetTitle(name.c_str());
    	h_tt->Draw();
    	h_tty->Draw("same");
    }
    {
	subc++;
    	c->cd(subc); string name = "reco_emu_dr_truth_ph_parton_b2";
    	TH1F*h_tty = (TH1F*)f_tty->Get(name.c_str());
    	TH1F*h_tt = (TH1F*)f_tt->Get(name.c_str());
    	h_tty->SetLineColor(kRed);
    	h_tt->SetLineColor(kBlue);
    	h_tty->Scale(1/h_tty->Integral());
    	h_tt->Scale(1/h_tt->Integral());
	h_tt->GetXaxis()->SetTitle(name.c_str());
    	h_tt->Draw();
    	h_tty->Draw("same");
    }
    {
	subc++;
    	c->cd(subc); string name = "reco_emu_dr_truth_ph_parton_el1";
    	TH1F*h_tty = (TH1F*)f_tty->Get(name.c_str());
    	TH1F*h_tt = (TH1F*)f_tt->Get(name.c_str());
    	h_tty->SetLineColor(kRed);
    	h_tt->SetLineColor(kBlue);
    	h_tty->Scale(1/h_tty->Integral());
    	h_tt->Scale(1/h_tt->Integral());
	h_tt->GetXaxis()->SetTitle(name.c_str());
    	h_tt->Draw();
    	h_tty->Draw("same");
    }
    {
	subc++;
    	c->cd(subc); string name = "reco_emu_dr_truth_ph_parton_mu1";
    	TH1F*h_tty = (TH1F*)f_tty->Get(name.c_str());
    	TH1F*h_tt = (TH1F*)f_tt->Get(name.c_str());
    	h_tty->SetLineColor(kRed);
    	h_tt->SetLineColor(kBlue);
    	h_tty->Scale(1/h_tty->Integral());
    	h_tt->Scale(1/h_tt->Integral());
	h_tt->GetXaxis()->SetTitle(name.c_str());
    	h_tt->Draw();
    	h_tty->Draw("same");
    }
//    {
//	subc++;
//    	c->cd(subc); string name = "reco_emu_dr_truth_ph_qcd_recoil";
//    	TH1F*h_tty = (TH1F*)f_tty->Get(name.c_str());
//    	TH1F*h_tt = (TH1F*)f_tt->Get(name.c_str());
//    	h_tty->SetLineColor(kRed);
//    	h_tt->SetLineColor(kBlue);
//    	h_tty->Scale(1/h_tty->Integral());
//    	h_tt->Scale(1/h_tt->Integral());
//	h_tt->GetXaxis()->SetTitle(name.c_str());
//    	h_tt->Draw();
//    	h_tty->Draw("same");
//    }
}
