#include <TMath.h>
#include <iomanip>
#include <TFile.h>
#include <TH1.h>
#include <TIterator.h>
#include <TKey.h>
#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <map>
#include <string>

#include "Logger.h"
#include "StringPlayer.h"
#include "ConfigReader.h"

using namespace std;

int FindIstop(int oldstop, TH1F* h, vector<string>&cuts);
void PrintCutflow(string tag, vector<TH1F*> h, vector<string> cutnames);

int main(int argc, char* argv[]) {

    Logger *lg = new Logger("MAIN");
    lg->Info("s", "Hi, this is the main program~");
    lg->NewLine();

    string filename;
    if (argc < 2) {
	lg->Info("s", "Will use default config file --> ../config/13TeV_table_cutflow.cfg");
	filename = "../config/13TeV_table_cutflow.cfg" ;
    } else {
	filename = argv[1];
    }

    ConfigReader* rd = new ConfigReader(filename, '_', false);
    rd->Init();

    vector<vector<string> > Processes = rd->GetAmbiValueAll("_Proc");
    int n_proc = Processes.size();
    vector<string> ProcTitles;
    vector<string> ProcTypes;
    vector<string> ProcSyss;
    vector<vector<string > > ProcNames;
    vector<vector<string > > ProcInputs;
    vector<vector<string > > ProcSFs;
    for (int i = 0; i < n_proc; i++) {
        ProcTitles.push_back(Processes.at(i).at(0));
        ProcTypes.push_back(Processes.at(i).at(1));
        ProcSyss.push_back(Processes.at(i).at(2));
	ProcNames.push_back(SplitToVector(Processes.at(i).at(3)));
	ProcInputs.push_back(SplitToVector(Processes.at(i).at(4)));
	ProcSFs.push_back(SplitToVector(Processes.at(i).at(5)));
    }

    vector<TH1F*> hs_el;
    vector<TH1F*> hs_mu;
    vector<TH1F*> hs_em;
    vector<TH1F*> hs_ee;
    vector<TH1F*> hs_mm;
    int istop_el = 0;
    int istop_mu = 0;
    int istop_em = 0;
    int istop_ee = 0;
    int istop_mm = 0;
    vector<string> cuts_el;
    vector<string> cuts_mu;
    vector<string> cuts_em;
    vector<string> cuts_ee;
    vector<string> cuts_mm;
    bool hasel = true;
    bool hasmu = true;
    bool hasem = true;
    bool hasee = true;
    bool hasmm = true;
    for (int i = 0; i < n_proc; i++) {
	TH1F* h_el = NULL;
	TH1F* h_mu = NULL;
	TH1F* h_em = NULL;
	TH1F* h_ee = NULL;
	TH1F* h_mm = NULL;
	for (int j = 0; j < ProcInputs.at(i).size(); j++) {
	    string hname_el = "Cutflow_EJ_"; hname_el += ProcTypes.at(i); hname_el += "_"; if (ProcTypes.at(i) != "Data") {hname_el += ProcNames.at(i).at(j); hname_el += "_";} hname_el += ProcSyss.at(i);
	    string hname_mu = "Cutflow_MJ_"; hname_mu += ProcTypes.at(i); hname_mu += "_"; if (ProcTypes.at(i) != "Data") {hname_mu += ProcNames.at(i).at(j); hname_mu += "_";} hname_mu += ProcSyss.at(i);
	    string hname_em = "Cutflow_EM_"; hname_em += ProcTypes.at(i); hname_em += "_"; if (ProcTypes.at(i) != "Data") {hname_em += ProcNames.at(i).at(j); hname_em += "_";} hname_em += ProcSyss.at(i);
	    string hname_ee = "Cutflow_EE_"; hname_ee += ProcTypes.at(i); hname_ee += "_"; if (ProcTypes.at(i) != "Data") {hname_ee += ProcNames.at(i).at(j); hname_ee += "_";} hname_ee += ProcSyss.at(i);
	    string hname_mm = "Cutflow_MM_"; hname_mm += ProcTypes.at(i); hname_mm += "_"; if (ProcTypes.at(i) != "Data") {hname_mm += ProcNames.at(i).at(j); hname_mm += "_";} hname_mm += ProcSyss.at(i);
	    if (ProcInputs.at(i).at(j).find("*",0) == string::npos) {
		TFile* f = new TFile(ProcInputs.at(i).at(j).c_str());
		if (!f) {
		    cout << "Error: cannot open " << ProcInputs.at(i).at(j) << endl;
		    exit(-1);
		}
		TH1F* tmp_el = (TH1F*)f->Get(hname_el.c_str());
		TH1F* tmp_mu = (TH1F*)f->Get(hname_mu.c_str());
		TH1F* tmp_em = (TH1F*)f->Get(hname_em.c_str());
		TH1F* tmp_ee = (TH1F*)f->Get(hname_ee.c_str());
		TH1F* tmp_mm = (TH1F*)f->Get(hname_mm.c_str());
		if (hasel && !tmp_el) {
		    cout << "Warn: cannot find " << hname_el << endl;
		    hasel = false;
		}
		if (hasmu && !tmp_mu) {
		    cout << "Warn: cannot find " << hname_mu << endl;
		    hasmu = false;
		}
		if (hasem && !tmp_em) {
		    cout << "Warn: cannot find " << hname_em << endl;
		    hasem = false;
		}
		if (hasee && !tmp_ee) {
		    cout << "Warn: cannot find " << hname_ee << endl;
		    hasee = false;
		}
		if (hasmm && !tmp_mm) {
		    cout << "Warn: cannot find " << hname_mm << endl;
		    hasmm = false;
		}
		if (ProcSFs.at(i).size() >= j+1) {
		    if (hasel) tmp_el->Scale(atof(ProcSFs.at(i).at(j).c_str()));
		    if (hasmu) tmp_mu->Scale(atof(ProcSFs.at(i).at(j).c_str()));
		    if (hasem) tmp_em->Scale(atof(ProcSFs.at(i).at(j).c_str()));
		    if (hasee) tmp_ee->Scale(atof(ProcSFs.at(i).at(j).c_str()));
		    if (hasmm) tmp_mm->Scale(atof(ProcSFs.at(i).at(j).c_str()));
		}
		if (hasel && tmp_el->GetBinContent(1) != 0) {
		    if (h_el == NULL) {
		        h_el = (TH1F*)tmp_el->Clone();
		    } else {
			h_el->Add(tmp_el);
		    }
			istop_el = FindIstop(istop_el, tmp_el, cuts_el);
		}
		if (hasmu && tmp_mu->GetBinContent(1) != 0) {
		    if (h_mu == NULL) {
		        h_mu = (TH1F*)tmp_mu->Clone();
		    } else {
			h_mu->Add(tmp_mu);
		    }
			istop_mu = FindIstop(istop_mu, tmp_mu, cuts_mu);
		}
		if (hasem && tmp_em->GetBinContent(1) != 0) {
		    if (h_em == NULL) {
		        h_em = (TH1F*)tmp_em->Clone();
		    } else {
			h_em->Add(tmp_em);
		    }
			istop_em = FindIstop(istop_em, tmp_em, cuts_em);
		}
		if (hasee && tmp_ee->GetBinContent(1) != 0) {
		    if (h_ee == NULL) {
		        h_ee = (TH1F*)tmp_ee->Clone();
		    } else {
			h_ee->Add(tmp_ee);
		    }
			istop_ee = FindIstop(istop_ee, tmp_ee, cuts_ee);
		}
		if (hasmm && tmp_mm->GetBinContent(1) != 0) {
		    if (h_mm == NULL) {
		        h_mm = (TH1F*)tmp_mm->Clone();
		    } else {
			h_mm->Add(tmp_mm);
		    }
			istop_mm = FindIstop(istop_mm, tmp_mm, cuts_mm);
		}
	    } else {
		if (ProcSFs.at(i).size() != 0) {
		    cout << "Danger! Asterisk used. Will not apply SF!" << endl;
		}
		char run[1000];
		sprintf(run, "ls %s > log.tmp", ProcInputs.at(i).at(j).c_str());
		system(run);
		ifstream ifile;
		ifile.open("log.tmp");
		string line;
		while (getline(ifile,line)) {
		    TFile* f = new TFile(line.c_str());
		    if (!f) {
		        cout << "Error: cannot open " << line << endl;
		        exit(-1);
		    }
		    TH1F* tmp_el = (TH1F*)f->Get(hname_el.c_str());
		    TH1F* tmp_mu = (TH1F*)f->Get(hname_mu.c_str());
		    TH1F* tmp_em = (TH1F*)f->Get(hname_em.c_str());
		    TH1F* tmp_ee = (TH1F*)f->Get(hname_ee.c_str());
		    TH1F* tmp_mm = (TH1F*)f->Get(hname_mm.c_str());
		    if (hasel && !tmp_el) {
		        cout << "Warn: cannot find " << hname_el << endl;
		        hasel = false;
		    }
		    if (hasmu && !tmp_mu) {
		        cout << "Warn: cannot find " << hname_mu << endl;
		        hasmu = false;
		    }
		    if (hasem && !tmp_em) {
		        cout << "Warn: cannot find " << hname_em << endl;
		        hasem = false;
		    }
		    if (hasee && !tmp_ee) {
		        cout << "Warn: cannot find " << hname_ee << endl;
		        hasee = false;
		    }
		    if (hasmm && !tmp_mm) {
		        cout << "Warn: cannot find " << hname_mm << endl;
		        hasmm = false;
		    }
		    if (hasel && tmp_el->GetBinContent(1) != 0) {
		        if (h_el == NULL) {
		            h_el = (TH1F*)tmp_el->Clone();
		        } else {
			   h_el->Add(tmp_el);
		        }
			istop_el = FindIstop(istop_el, tmp_el, cuts_el);
		    }
		    if (hasmu && tmp_mu->GetBinContent(1) != 0) {
		        if (h_mu == NULL) {
		            h_mu = (TH1F*)tmp_mu->Clone();
		        } else {
		    	h_mu->Add(tmp_mu);
		        }
			istop_mu = FindIstop(istop_mu, tmp_mu, cuts_mu);
		    }
		    if (hasem && tmp_em->GetBinContent(1) != 0) {
		        if (h_em == NULL) {
		            h_em = (TH1F*)tmp_em->Clone();
		        } else {
		    	h_em->Add(tmp_em);
		        }
			istop_em = FindIstop(istop_em, tmp_em, cuts_em);
		    }
		    if (hasee && tmp_ee->GetBinContent(1) != 0) {
		        if (h_ee == NULL) {
		            h_ee = (TH1F*)tmp_ee->Clone();
		        } else {
		    	h_ee->Add(tmp_ee);
		        }
			istop_ee = FindIstop(istop_ee, tmp_ee, cuts_ee);
		    }
		    if (hasmm && tmp_mm->GetBinContent(1) != 0) {
		        if (h_mm == NULL) {
		            h_mm = (TH1F*)tmp_mm->Clone();
		        } else {
		    	h_mm->Add(tmp_mm);
		        }
			istop_mm = FindIstop(istop_mm, tmp_mm, cuts_mm);
		    }
		}
		sprintf(run, "rm log.tmp");
		system(run);
	    }
	}

	if (hasel) h_el->SetName(ProcTitles.at(i).c_str());
	if (hasmu) h_mu->SetName(ProcTitles.at(i).c_str());
	if (hasem) h_em->SetName(ProcTitles.at(i).c_str());
	if (hasee) h_ee->SetName(ProcTitles.at(i).c_str());
	if (hasmm) h_mm->SetName(ProcTitles.at(i).c_str());
	hs_el.push_back(h_el);
	hs_mu.push_back(h_mu);
	hs_em.push_back(h_em);
	hs_ee.push_back(h_ee);
	hs_mm.push_back(h_mm);
    }
    
    cout << endl;
    PrintCutflow("EJ", hs_el, cuts_el);
    PrintCutflow("MJ", hs_mu, cuts_mu);
    PrintCutflow("EM", hs_em, cuts_em);
    PrintCutflow("EE", hs_ee, cuts_ee);
    PrintCutflow("MM", hs_mm, cuts_mm);
    
    return 0;
}

int FindIstop(int oldstop, TH1F* h, vector<string>&cuts) {
    int istop = 0;
    for (int i = 1; i <= h->GetNbinsX(); i++) {
	if (string(h->GetXaxis()->GetBinLabel(i)).find("STOP",0) != string::npos) {
	    istop = i-1;
	    break;
	}
    }
    if (istop <= oldstop) {
	return oldstop;
    } else {
	cuts.clear();
	for (int i = 1; i <= istop; i++) {
    	    cuts.push_back(h->GetXaxis()->GetBinLabel(i));
	}
	return istop;
    }
}

void PrintCutflow(string tag, vector<TH1F*> hs, vector<string> cutnames) {
    if (cutnames.size() == 0) return;
    //if (!hs.at(0)) return;

    cout << "\\scalebox{1.0}{" << endl;
    cout << "\\begin{tabular}{c|";
    for (int i = 0; i < hs.size(); i++) {
	if (i != hs.size() - 1) cout << "c|";
	else cout << "c";
    }
    cout << "}" << endl;
    
    cout << fixed << setprecision(1);
    cout << setw(13) << tag << " &";
    for (int i = 0; i < hs.size(); i++) {
	if (i != hs.size() - 1) cout << setw(13) << hs.at(i)->GetName() << " &";
	else cout << setw(12) << hs.at(i)->GetName() << " \\\\";
    }
    cout << endl;
    for (int i = 0; i < cutnames.size(); i++) {
	if (i != cutnames.size()) cout << "\\hline" << endl;
	cout << setw(13) << cutnames.at(i) << " &";
	for (int j = 0; j < hs.size(); j++) {
	    if (j != hs.size() - 1) cout << setw(13) << hs.at(j)->GetBinContent(i+1) << " &";
	    else cout << setw(12) << hs.at(j)->GetBinContent(i+1) << " \\\\";
	}
	cout << endl;
    }
    cout << "\\end{tabular}}";
    cout << endl;
}
