#include <TRandom3.h>
#include <iomanip>
//#include "RecoLevel2.h"
#include "PartonLevel2.h"
#include <TStopwatch.h>
#include <TLorentzVector.h>
#include <iostream>
#include <TH1F.h>
#include <TH2F.h>
#include <TFile.h>
#include <map>

using namespace std;

bool debug = false;
int NEvt = -1;

void AddOverflow(TH1F *h);

int main(int argc, char * argv[]) {

    TRandom *m_random = new TRandom3();
    TStopwatch t; t.Start();

    for (int i = 0; i < argc; i++) {
	if (!strcmp(argv[i],"--debug")) {
	    debug = true;
   	}
	if (!strcmp(argv[i],"--NEvt")) {
	    NEvt = atoi(string(argv[i+1]).c_str());
   	}
    }

    vector<string> filenames;
    filenames.push_back("/afs/cern.ch/work/a/arej/public//output_410389_new.root");

    TH1F* h_Event_Category = new TH1F("Event_Category","",21,-1,20);
    TH1F* h_cat1_t_diff1_M = new TH1F("cat1_t_diff1_M","",200,-1,1);
    TH1F* h_cat2_t_diff1_M = new TH1F("cat2_t_diff1_M","",200,-1,1);
    TH1F* h_cat3_t_diff2_M = new TH1F("cat3_t_diff2_M","",200,-1,1);
    TH1F* h_cat4_tbar_diff2_M = new TH1F("cat4_tbar_diff2_M","",200,-1,1);
    TH1F* h_cat5_t_diff2_M = new TH1F("cat5_t_diff2_M","",200,-1,1);
    TH1F* h_cat6_tbar_diff2_M = new TH1F("cat6_tbar_diff2_M","",200,-1,1);

    // for nominal study
    for (int ifile = 0; ifile < filenames.size(); ifile++) {
	TFile* f = new TFile(filenames.at(ifile).c_str());

    	TTree* t_parton = (TTree*)f->Get("truth");
    	//TTree* t_reco = (TTree*)f->Get("nominal");

    	//if (t_parton->BuildIndex("runNumber", "eventNumber") < 0){
    	//    std::cerr << "Could not build particle level index!" << std::endl;
    	//    std::abort();
    	//}

	//int totentry_reco = t_reco->GetEntriesFast();
	int totentry_parton = t_parton->GetEntriesFast();

    	//RecoLevel2 RecoTree(t_reco);
    	PartonLevel2 PartonTree(t_parton);

	//cout << "reco: " << totentry_reco << endl;
	cout << "parton: " << totentry_parton << endl;

	int totentry = totentry_parton;

    	if (NEvt != -1) totentry = NEvt;
    	for (int ientry = 0; ientry < totentry; ientry++) {
    	    if (ientry%10000 == 0) cout << "this is the " << ientry << " entry" << endl;
    	    PartonTree.GetEntry(ientry);
    	    //RecoTree.GetEntry(ientry);
	    //Int_t nBytesRead = t_parton->GetEntryWithIndex(RecoTree.runNumber, RecoTree.eventNumber);
	    //if ( nBytesRead < 0 ) {
    	    //	continue;
	    //}

    	    double weight = PartonTree.weight_mc;
	    weight = 1;

	    TLorentzVector t_aFSR; t_aFSR.SetPtEtaPhiM(PartonTree.MC_t_afterFSR_pt, PartonTree.MC_t_afterFSR_eta, PartonTree.MC_t_afterFSR_phi, PartonTree.MC_t_afterFSR_m);
	    TLorentzVector t_bFSR; t_bFSR.SetPtEtaPhiM(PartonTree.MC_t_beforeFSR_pt, PartonTree.MC_t_beforeFSR_eta, PartonTree.MC_t_beforeFSR_phi, PartonTree.MC_t_beforeFSR_m);
	    TLorentzVector tbar_aFSR; tbar_aFSR.SetPtEtaPhiM(PartonTree.MC_tbar_afterFSR_pt, PartonTree.MC_tbar_afterFSR_eta, PartonTree.MC_tbar_afterFSR_phi, PartonTree.MC_tbar_afterFSR_m);
	    TLorentzVector tbar_bFSR; tbar_bFSR.SetPtEtaPhiM(PartonTree.MC_tbar_beforeFSR_pt, PartonTree.MC_tbar_beforeFSR_eta, PartonTree.MC_tbar_beforeFSR_phi, PartonTree.MC_tbar_beforeFSR_m);

	    TLorentzVector t_W; t_W.SetPtEtaPhiM(PartonTree.MC_W_from_t_pt, PartonTree.MC_W_from_t_eta, PartonTree.MC_W_from_t_phi, PartonTree.MC_W_from_t_m);
	    TLorentzVector tbar_W; tbar_W.SetPtEtaPhiM(PartonTree.MC_W_from_tbar_pt, PartonTree.MC_W_from_tbar_eta, PartonTree.MC_W_from_tbar_phi, PartonTree.MC_W_from_tbar_m);
	    TLorentzVector t_b; t_b.SetPtEtaPhiM(PartonTree.MC_b_from_t_pt, PartonTree.MC_b_from_t_eta, PartonTree.MC_b_from_t_phi, PartonTree.MC_b_from_t_m);
	    TLorentzVector tbar_b; tbar_b.SetPtEtaPhiM(PartonTree.MC_b_from_tbar_pt, PartonTree.MC_b_from_tbar_eta, PartonTree.MC_b_from_tbar_phi, PartonTree.MC_b_from_tbar_m);
	    TLorentzVector ph; ph.SetPtEtaPhiM(PartonTree.MC_ph_pt, PartonTree.MC_ph_eta, PartonTree.MC_ph_phi, 0);

	    TLorentzVector t_W_decay1; t_W_decay1.SetPtEtaPhiM(PartonTree.MC_Wdecay1_from_t_pt, PartonTree.MC_Wdecay1_from_t_eta, PartonTree.MC_Wdecay1_from_t_phi, PartonTree.MC_Wdecay1_from_t_m);
	    TLorentzVector t_W_decay2; t_W_decay2.SetPtEtaPhiM(PartonTree.MC_Wdecay2_from_t_pt, PartonTree.MC_Wdecay2_from_t_eta, PartonTree.MC_Wdecay2_from_t_phi, PartonTree.MC_Wdecay2_from_t_m);
	    TLorentzVector tbar_W_decay1; tbar_W_decay1.SetPtEtaPhiM(PartonTree.MC_Wdecay1_from_tbar_pt, PartonTree.MC_Wdecay1_from_tbar_eta, PartonTree.MC_Wdecay1_from_tbar_phi, PartonTree.MC_Wdecay1_from_tbar_m);
	    TLorentzVector tbar_W_decay2; tbar_W_decay2.SetPtEtaPhiM(PartonTree.MC_Wdecay2_from_tbar_pt, PartonTree.MC_Wdecay2_from_tbar_eta, PartonTree.MC_Wdecay2_from_tbar_phi, PartonTree.MC_Wdecay2_from_tbar_m);

	    double t_diff1_Pt = t_aFSR.Pt() - (t_W_decay1+t_W_decay2+t_b).Pt(); double t_diff2_Pt = t_aFSR.Pt() - (t_W_decay1+t_W_decay2+t_b+ph).Pt();
	    double t_diff1_Eta = t_aFSR.Eta() - (t_W_decay1+t_W_decay2+t_b).Eta(); double t_diff2_Eta = t_aFSR.Eta() - (t_W_decay1+t_W_decay2+t_b+ph).Eta();
	    double t_diff1_Phi = t_aFSR.Phi() - (t_W_decay1+t_W_decay2+t_b).Phi(); double t_diff2_Phi = t_aFSR.Phi() - (t_W_decay1+t_W_decay2+t_b+ph).Phi();
	    double t_diff1_M = t_aFSR.M() - (t_W_decay1+t_W_decay2+t_b).M(); double t_diff2_M = t_aFSR.M() - (t_W_decay1+t_W_decay2+t_b+ph).M();
	    double tbar_diff1_Pt = tbar_aFSR.Pt() - (tbar_W_decay1+tbar_W_decay2+tbar_b).Pt(); double tbar_diff2_Pt = tbar_aFSR.Pt() - (tbar_W_decay1+tbar_W_decay2+tbar_b+ph).Pt();
	    double tbar_diff1_Eta = tbar_aFSR.Eta() - (tbar_W_decay1+tbar_W_decay2+tbar_b).Eta(); double tbar_diff2_Eta = tbar_aFSR.Eta() - (tbar_W_decay1+tbar_W_decay2+tbar_b+ph).Eta();
	    double tbar_diff1_Phi = tbar_aFSR.Phi() - (tbar_W_decay1+tbar_W_decay2+tbar_b).Phi(); double tbar_diff2_Phi = tbar_aFSR.Phi() - (tbar_W_decay1+tbar_W_decay2+tbar_b+ph).Phi();
	    double tbar_diff1_M = tbar_aFSR.M() - (tbar_W_decay1+tbar_W_decay2+tbar_b).M(); double tbar_diff2_M = tbar_aFSR.M() - (tbar_W_decay1+tbar_W_decay2+tbar_b+ph).M();

	    bool t_lep_decay = false;
	    if (abs(PartonTree.MC_Wdecay1_from_t_pdgId) == 11 ||
		abs(PartonTree.MC_Wdecay1_from_t_pdgId) == 12 ||
		abs(PartonTree.MC_Wdecay1_from_t_pdgId) == 13 ||
		abs(PartonTree.MC_Wdecay1_from_t_pdgId) == 14 ||
		abs(PartonTree.MC_Wdecay1_from_t_pdgId) == 15 ||
		abs(PartonTree.MC_Wdecay1_from_t_pdgId) == 16) {
		t_lep_decay = true;
	    }
	    bool tbar_lep_decay = false;
	    if (abs(PartonTree.MC_Wdecay1_from_tbar_pdgId) == 11 ||
		abs(PartonTree.MC_Wdecay1_from_tbar_pdgId) == 12 ||
		abs(PartonTree.MC_Wdecay1_from_tbar_pdgId) == 13 ||
		abs(PartonTree.MC_Wdecay1_from_tbar_pdgId) == 14 ||
		abs(PartonTree.MC_Wdecay1_from_tbar_pdgId) == 15 ||
		abs(PartonTree.MC_Wdecay1_from_tbar_pdgId) == 16) {
		tbar_lep_decay = true;
	    }
	    
	    // xcheck
	    h_Event_Category->Fill(PartonTree.MC_Event_Category, weight);
	    if (PartonTree.MC_Event_Category == 1) h_cat1_t_diff1_M->Fill(t_diff1_M, weight);
	    if (PartonTree.MC_Event_Category == 2) h_cat2_t_diff1_M->Fill(t_diff1_M, weight);
	    if (PartonTree.MC_Event_Category == 3 && t_lep_decay) h_cat3_t_diff2_M->Fill(t_diff2_M, weight);
	    if (PartonTree.MC_Event_Category == 4 && !t_lep_decay) h_cat4_tbar_diff2_M->Fill(tbar_diff2_M, weight);
	    if (PartonTree.MC_Event_Category == 5 && t_lep_decay) h_cat5_t_diff2_M->Fill(t_diff2_M, weight);
	    if (PartonTree.MC_Event_Category == 6 && !t_lep_decay) h_cat6_tbar_diff2_M->Fill(tbar_diff2_M, weight);
    	}
    }

    string savename;
    savename = "results/TTGOR/Histograms.root";
    TFile *f = new TFile(savename.c_str(), "recreate");
    f->cd();

    AddOverflow(h_Event_Category); h_Event_Category->Write();
    AddOverflow(h_cat1_t_diff1_M); h_cat1_t_diff1_M->Write();
    AddOverflow(h_cat2_t_diff1_M); h_cat2_t_diff1_M->Write();
    AddOverflow(h_cat3_t_diff2_M); h_cat3_t_diff2_M->Write();
    AddOverflow(h_cat4_tbar_diff2_M); h_cat4_tbar_diff2_M->Write();
    AddOverflow(h_cat5_t_diff2_M); h_cat5_t_diff2_M->Write();
    AddOverflow(h_cat6_tbar_diff2_M); h_cat6_tbar_diff2_M->Write();

    f->Close();
    delete f;
    cout << "Results save to: " << savename << endl;
}

void AddOverflow(TH1F *h) {  
    int nbin = 1;
    h->SetBinContent(nbin, h->GetBinContent(nbin) + h->GetBinContent(nbin-1));
    h->SetBinError(nbin, sqrt(pow(h->GetBinContent(nbin),2) + pow(h->GetBinContent(nbin-1),2)));
    nbin = h->GetNbinsX();
    h->SetBinContent(nbin, h->GetBinContent(nbin) + h->GetBinContent(nbin+1));
    h->SetBinError(nbin, sqrt(pow(h->GetBinContent(nbin),2) + pow(h->GetBinContent(nbin+1),2)));
    cout << h->GetName() << " " << h->Integral() << endl;
}
