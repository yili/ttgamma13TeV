#include <TFile.h>
#include <TLatex.h>
#include <TH1F.h>
#include <TCanvas.h>
#include <iostream>
#include <vector>

using namespace std;

void DrawELD (TString Process, TString Type, TString Channel);
vector<string> sys;

int main()
{
    sys.push_back("R1F2");
    sys.push_back("R2F1");
    sys.push_back("R2F05");
    sys.push_back("R05F2");
    sys.push_back("R05F1");
    sys.push_back("R05F05");
    sys.push_back("R1F05");
    sys.push_back("R2F2");
    sys.push_back("PDF25");
    sys.push_back("PDF24");
    sys.push_back("PDF27");
    sys.push_back("PDF26");
    sys.push_back("PDF21");
    sys.push_back("PDF20");
    sys.push_back("PDF23");
    sys.push_back("PDF22");
    sys.push_back("PDF29");
    sys.push_back("PDF28");
    sys.push_back("PDF36");
    sys.push_back("PDF34");
    sys.push_back("PDF35");
    sys.push_back("PDF32");
    sys.push_back("PDF33");
    sys.push_back("PDF30");
    sys.push_back("PDF31");
    sys.push_back("PDF38");
    sys.push_back("PDF39");
    sys.push_back("PDF83");
    sys.push_back("PDF82");
    sys.push_back("PDF81");
    sys.push_back("PDF80");
    sys.push_back("PDF87");
    sys.push_back("PDF100");
    sys.push_back("PDF85");
    sys.push_back("PDF84");
//    sys.push_back("PDF89");
    sys.push_back("PDF88");
    sys.push_back("PDF69");
    sys.push_back("PDF68");
    sys.push_back("PDF61");
    sys.push_back("PDF60");
//    sys.push_back("PDF63");
    sys.push_back("PDF62");
    sys.push_back("PDF65");
    sys.push_back("PDF64");
    sys.push_back("PDF67");
    sys.push_back("PDF66");
    sys.push_back("PDF8");
    sys.push_back("PDF9");
    sys.push_back("PDF6");
    sys.push_back("PDF7");
    sys.push_back("PDF4");
    sys.push_back("PDF5");
    sys.push_back("PDF2");
    sys.push_back("PDF3");
    sys.push_back("PDF1");
    sys.push_back("PDF94");
    sys.push_back("PDF95");
    sys.push_back("PDF96");
    sys.push_back("PDF97");
    sys.push_back("PDF90");
    sys.push_back("PDF91");
    sys.push_back("PDF92");
    sys.push_back("PDF93");
    sys.push_back("PDF98");
    sys.push_back("PDF99");
    sys.push_back("PDF18");
    sys.push_back("PDF19");
    sys.push_back("PDF14");
    sys.push_back("PDF15");
    sys.push_back("PDF16");
    sys.push_back("PDF17");
    sys.push_back("PDF10");
    sys.push_back("PDF11");
    sys.push_back("PDF12");
    sys.push_back("PDF13");
    sys.push_back("PDF78");
    sys.push_back("PDF79");
    sys.push_back("PDF72");
    sys.push_back("PDF73");
    sys.push_back("PDF70");
    sys.push_back("PDF71");
    sys.push_back("PDF76");
    sys.push_back("PDF77");
    sys.push_back("PDF74");
    sys.push_back("PDF75");
    sys.push_back("PDF86");
    sys.push_back("PDF37");
    sys.push_back("PDF49");
    sys.push_back("PDF48");
    sys.push_back("PDF47");
    sys.push_back("PDF46");
    sys.push_back("PDF45");
    sys.push_back("PDF44");
    sys.push_back("PDF43");
    sys.push_back("PDF42");
    sys.push_back("PDF41");
    sys.push_back("PDF40");
    sys.push_back("PDF50");
    sys.push_back("PDF51");
    sys.push_back("PDF52");
//    sys.push_back("PDF53");
    sys.push_back("PDF54");
    sys.push_back("PDF55");
    sys.push_back("PDF56");
    sys.push_back("PDF57");
    sys.push_back("PDF58");
    sys.push_back("PDF59");

    DrawELD ("Signal", "TruePh", "ejets");
    DrawELD ("Signal", "TruePh", "mujets");
    DrawELD ("WGammajetsElNLO", "TruePh", "ejets");
    DrawELD ("WGammajetsMuNLO", "TruePh", "mujets");
    DrawELD ("TTBar", "HFake", "ejets");
    DrawELD ("TTBar", "HFake", "mujets");
    DrawELD ("TTBar", "EFake", "ejets");
    DrawELD ("TTBar", "EFake", "mujets");

    return 0;
}

void DrawELD (TString Process, TString Type, TString Channel) {
    TString Variation = "Nominal";
    TFile* f_nom = new TFile("/afs/cern.ch/work/y/yili/private/MySVN/Code/TTG/run/results/"+Variation+"/13TeV_CutSR_PhMatch_"+Type+"_"+Process+"_Reco_SR1_"+Channel+"_"+Variation+"_Final03.root");
    TH1F* h_nom = (TH1F*)f_nom->Get("ELD_13TeV_CutSR_PhMatch_"+Type+"_"+Process+"_Reco_SR1_"+Channel+"_"+Variation);
    h_nom->Scale(1./h_nom->Integral());
    vector<TH1F*> h_sys;
    TH1F*h_sys_total = NULL;
    for (int i = 0; i < sys.size(); i++) {
	Variation = sys.at(i);
	TFile* f_tmp = new TFile("/afs/cern.ch/work/y/yili/private/MySVN/Code/TTG/run/results/"+Variation+"/13TeV_CutSR_PhMatch_"+Type+"_"+Process+"_Reco_SR1_"+Channel+"_"+Variation+"_Final03.root");
	TH1F* h_tmp = (TH1F*)f_tmp->Get("ELD_13TeV_CutSR_PhMatch_"+Type+"_"+Process+"_Reco_SR1_"+Channel+"_"+Variation);
	h_tmp->Scale(1./h_tmp->Integral());
	for (int j = 1; j <= h_nom->GetNbinsX(); j++) {
	    if (h_nom->GetBinContent(j) != 0) {
		double nom = h_nom->GetBinContent(j);
		double sys = h_tmp->GetBinContent(j);
		double relsys = sys/nom - 1;
		h_tmp->SetBinContent(j, relsys*100);
	    } else {
		h_tmp->SetBinContent(j, 0);
	    }
	}
	if (i == 0) h_sys_total = (TH1F*)h_tmp->Clone();
	else {
	    for (int j = 1; j <= h_sys_total->GetNbinsX(); j++) {
		double oldsys = h_sys_total->GetBinContent(j);
		double addsys = h_tmp->GetBinContent(j);
		double newsys = sqrt(pow(oldsys,2) + pow(addsys,2));
		h_sys_total->SetBinContent(j,newsys);
	    }
	}
	h_sys.push_back(h_tmp);
    }
    TH1F*h_sys_total_mir = (TH1F*)h_sys_total->Clone();
    for (int j = 1; j <= h_sys_total->GetNbinsX(); j++) {
        h_sys_total_mir->SetBinContent(j,-1*h_sys_total->GetBinContent(j));
    }

    TCanvas *c = new TCanvas("","",800,600);
    for (int i = 0; i < sys.size(); i++) {
	if (i == 0) {
	    h_sys.at(i)->SetTitle("ELD Sys");
	    h_sys.at(i)->GetXaxis()->SetTitle("ELD");
	    h_sys.at(i)->GetYaxis()->SetTitle("Rel. Sys. (%)");
	    h_sys.at(i)->GetYaxis()->SetRangeUser(-40,40);
	}
	h_sys.at(i)->Draw("same");
    }
    h_sys_total->SetLineColor(kRed); h_sys_total->Draw("same");
    h_sys_total_mir->SetLineColor(kRed); h_sys_total_mir->Draw("same");

    TLatex lt;
    lt.SetNDC();
    lt.DrawLatex(0.2, 0.8, Process+" "+Type+" "+Channel);

    c->SaveAs("plots/ELDShapeSys_"+Process+"_"+Type+"_"+Channel+".png");
    c->SaveAs("plots/ELDShapeSys_"+Process+"_"+Type+"_"+Channel+".pdf");
}
