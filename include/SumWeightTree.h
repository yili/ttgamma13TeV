//////////////////////////////////////////////////////////
// This class has been automatically generated on
// Mon Dec 18 21:26:27 2017 by ROOT version 6.04/14
// from TTree sumWeights/
// found on file: /eos/user/c/caudron2/TtGamma_PL/v010/410389.ttgamma_noallhad.p3152.PL5.001.root
//////////////////////////////////////////////////////////

#ifndef SumWeightTree_h
#define SumWeightTree_h

#include <TROOT.h>
#include <TChain.h>
#include <TFile.h>

// Header file for the classes stored in the TTree if any.
#include "vector"
#include "vector"

using namespace std;

class SumWeightTree {
public :
   TTree          *fChain;   //!pointer to the analyzed TTree or TChain
   Int_t           fCurrent; //!current Tree number in a TChain

// Fixed size dimensions of array or collections stored in the TTree if any.

   // Declaration of leaf types
   Int_t           dsid;
   Float_t         totalEventsWeighted;
   vector<float>   *totalEventsWeighted_mc_generator_weights;
   vector<string>  *names_mc_generator_weights;
   ULong64_t       totalEvents;

   // List of branches
   TBranch        *b_dsid;   //!
   TBranch        *b_totalEventsWeighted;   //!
   TBranch        *b_totalEventsWeighted_mc_generator_weights;   //!
   TBranch        *b_names_mc_generator_weights;   //!
   TBranch        *b_totalEvents;   //!

   SumWeightTree(TTree *tree=0);
   virtual ~SumWeightTree();
   virtual Int_t    Cut(Long64_t entry);
   virtual Int_t    GetTotalEntry();
   virtual Int_t    GetEntry(Long64_t entry);
   virtual Long64_t LoadTree(Long64_t entry);
   virtual void     Init(TTree *tree);
   virtual void     Loop();
   virtual Bool_t   Notify();
   virtual void     Show(Long64_t entry = -1);
};

#endif

#ifdef SumWeightTree_cxx
SumWeightTree::SumWeightTree(TTree *tree) : fChain(0) 
{
// if parameter tree is not specified (or zero), connect the file
// used to generate this class and read the Tree.
   if (tree == 0) {
      TFile *f = (TFile*)gROOT->GetListOfFiles()->FindObject("/eos/user/c/caudron2/TtGamma_PL/v010/410389.ttgamma_noallhad.p3152.PL5.001.root");
      if (!f || !f->IsOpen()) {
         f = new TFile("/eos/user/c/caudron2/TtGamma_PL/v010/410389.ttgamma_noallhad.p3152.PL5.001.root");
      }
      f->GetObject("sumWeights",tree);

   }
   Init(tree);
}

SumWeightTree::~SumWeightTree()
{
   if (!fChain) return;
   delete fChain->GetCurrentFile();
}

Int_t SumWeightTree::GetTotalEntry()
{
// Read contents of entry.
   if (!fChain) return 0;
   return fChain->GetEntriesFast();
}

Int_t SumWeightTree::GetEntry(Long64_t entry)
{
// Read contents of entry.
   if (!fChain) return 0;
   return fChain->GetEntry(entry);
}
Long64_t SumWeightTree::LoadTree(Long64_t entry)
{
// Set the environment to read one entry
   if (!fChain) return -5;
   Long64_t centry = fChain->LoadTree(entry);
   if (centry < 0) return centry;
   if (fChain->GetTreeNumber() != fCurrent) {
      fCurrent = fChain->GetTreeNumber();
      Notify();
   }
   return centry;
}

void SumWeightTree::Init(TTree *tree)
{
   // The Init() function is called when the selector needs to initialize
   // a new tree or chain. Typically here the branch addresses and branch
   // pointers of the tree will be set.
   // It is normally not necessary to make changes to the generated
   // code, but the routine can be extended by the user if needed.
   // Init() will be called many times when running on PROOF
   // (once per file to be processed).

   // Set object pointer
   totalEventsWeighted_mc_generator_weights = 0;
   names_mc_generator_weights = 0;
   // Set branch addresses and branch pointers
   if (!tree) return;
   fChain = tree;
   fCurrent = -1;
   fChain->SetMakeClass(1);

   fChain->SetBranchAddress("dsid", &dsid, &b_dsid);
   fChain->SetBranchAddress("totalEventsWeighted", &totalEventsWeighted, &b_totalEventsWeighted);
   fChain->SetBranchAddress("totalEventsWeighted_mc_generator_weights", &totalEventsWeighted_mc_generator_weights, &b_totalEventsWeighted_mc_generator_weights);
   fChain->SetBranchAddress("names_mc_generator_weights", &names_mc_generator_weights, &b_names_mc_generator_weights);
   fChain->SetBranchAddress("totalEvents", &totalEvents, &b_totalEvents);
   Notify();
}

Bool_t SumWeightTree::Notify()
{
   // The Notify() function is called when a new file is opened. This
   // can be either for a new TTree in a TChain or when when a new TTree
   // is started when using PROOF. It is normally not necessary to make changes
   // to the generated code, but the routine can be extended by the
   // user if needed. The return value is currently not used.

   return kTRUE;
}

void SumWeightTree::Show(Long64_t entry)
{
// Print contents of entry.
// If entry is not specified, print current entry
   if (!fChain) return;
   fChain->Show(entry);
}
Int_t SumWeightTree::Cut(Long64_t entry)
{
// This function may be called from Loop.
// returns  1 if entry is accepted.
// returns -1 otherwise.
   return 1;
}
#endif // #ifdef SumWeightTree_cxx
