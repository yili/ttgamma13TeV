#ifndef PlotComparor_h
#define PlotComparor_h

#include <TH1F.h>
#include <string>
#include <TPad.h>
#include "Logger.h"

using namespace std;

class PlotComparor {
    
private:

    // flags
    int atlas_flag;
    bool m_norm;
    bool m_logy;
    bool m_autorange;
    bool m_overflow;
    bool m_normbinwidth;
    bool m_cutbins;
    double m_cut_lo;
    double m_cut_hi;
    bool m_draw_dhline;
    bool m_do_ratio;
    bool m_fix_ratio_range;
    bool m_atlas_logo;
    bool m_is_data;
    bool m_is_simulation;
    bool m_is_13tev;
    bool m_is_14tev;
    bool m_showlumi;
    bool m_stack_reverse;
    bool m_stack_mc_only;
    bool m_save_ratio;
    bool m_order_bin;
    bool m_x_label_v;
    bool m_datamc_sf;
    bool m_normMCtodata;
    bool m_forced_lgpos;
    bool m_lgpos_manual;

    int m_line_width;

    // log
    float m_logy_min;
    bool  m_dby;
    float m_range_y_low;
    float m_range_y_high;
    string m_logo;
    string m_lhc_info;
    vector<double> s_cumi;

    // canvas
    float m_canvas_l;
    float m_canvas_h;
    float m_ypos_dhline;
    float m_logy_scale;
    int	  m_color_dhline;
    TPad* pad1;
    TPad* pad2;
    bool  m_c_square;
    double m_rightmargin;

    // legend
    bool m_no_lg;
    int m_startcolor;
    bool  m_lg_right;
    float m_lg_pos[4];
    int	  m_lg_column;
    double m_lg_scale_x;
    float m_lg_textsize;
    float m_logo_xshift;
    float m_lg_xshift;
    float m_lg_xshift2;
    float m_lg_yshift;
    string m_lg_basename;
    string m_lg_altername;
    vector<string> m_lg_alternames;
    int m_basecolor;
    vector<int> m_altercolors;

    // latex
    float m_lt_x;
    float m_lt_y;
    string m_channel;
    string m_type;

    string m_draw_option;
    string m_xtitle;
    string m_ytitle;
    double m_ytitle_offset;
    string m_ratio_ytitle;
    float m_xtitle_size;

    float m_yrange_ratio;

    string m_savename;
    string m_savedir;

    TH1F* h_base;
    TH1F* h_alter;
    vector<TH1F*> h_alters;

    Logger* m_lg;

    float m_max_ratio;
    float m_min_ratio;

public:

    PlotComparor();
    ~PlotComparor();

    void SetBaseH(TH1F*h);
    void SetAlterH(TH1F*h);
    void AddAlterH(TH1F*h);
    void ClearAlterHs();
    void ClearAlterColors();

    void SetLegendPos(float x1, float y1, float x2, float y2);
    void SetLegendNC(int nc);
    void SetLegendScaleX(double scale);
    void SetLegendTS(float size);
    void SetBaseHName(string name);
    void SetBaseColor(int i);
    void SetAlterHName(string name);
    void AddAlterColors(int i);
    void AddAlterHName(string name);

    string ParseXtitle(string name);
    string ParseYtitle(string name);

    void NormToUnit(bool norm);
    void UseLogy(bool logy, double min = 0.001, double scale = 20.);
    void LogyMin(double min);
    void ShowOverflow(bool of);
    void NormBinWidth(bool norm);
    void AutoRange(bool range);

    void Compare(bool debug = false);
    void DrawStack(bool debug = false);

    // config
    void SetChannel(string channel);
    void SetType(string type);
    void SetType(int type);
    void SetXtitle(string title);
    void SetXtitleSize(float titlesize);
    void SetYtitle(string title);
    void SetYtitleOffset(double offset);
    void SetRatioYtitle(string title);
    void SetDrawOption(string option);
    void SetSaveName(string name);
    void SetSaveDir(string dir);
    void DrawRatio(bool ratio);
    void FixRatioRange(bool fix);
    void SetMaxRatio(float max);
    void SetMinRatio(float min);
    void SetRangeY(float low, float high);
    void IsSimulation(bool issimu = true);
    void Is13TeV(bool is13tev = false);
    void Is14TeV(bool is14tev = false, bool showlumi = true);
    void DataLumi(double lumi);
    void HasData(bool hasdata = true);
    void ShiftLogoX(float shift);
    void ShiftLegendX(float shift);
    void ShiftLegendX2(float shift);
    void ShiftLegendY(float shift);
    void SetLogo(string logo);
    void SetYRangeRatio(float ratio);
    void SetLogyScale(float scale);
    void SetLineWidth(int width);
    void SquareCanvas(bool square = true);
    void ReverseStackOrder(bool reverse);
    void SetStartColor(int color);
    void DoCutbins(double cut_lo, double cut_hi);
    void NoLegend();
    void DoubleYRange(bool dby);
    void SaveRatio(bool save);
    void OrderBin(bool order);
    void VerticleXLabel(bool vx);
    void PrintDataMCSF(bool datamc);
    void NormMCtoData(bool norm);
    double Separation(TH1F* h2, TH1F* h1, int idx);
    void Print();
    void SetRightMargin(double margin);
    void SetLHCInfo(string lhcinfo);
    void ForcedLGPos(bool forced);

    // Tool
    void LegendPosAutoAdjust(bool debug = false);
    void SetATLASFlag(int flag);
    void DashHLine(float ypos, int color = 1);
    TH1F* AddOverflow(TH1F* h);
    TH1F* NormToBinWidth(TH1F* h);
    TH1F* Cutbins(TH1F* h);
};
#endif
