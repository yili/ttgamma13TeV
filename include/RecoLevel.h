//////////////////////////////////////////////////////////
// This class has been automatically generated on
// Mon Sep 11 15:12:02 2017 by ROOT version 6.04/14
// from TTree nominal/tree
// found on file: /eos/user/c/caudron2/TtGamma_PL/v009/410389.ttgamma_noallhad.p3152.PL4.001.root
//////////////////////////////////////////////////////////

#ifndef RecoLevel_h
#define RecoLevel_h

#include <TROOT.h>
#include <TChain.h>
#include <TFile.h>

// Header file for the classes stored in the TTree if any.
#include "vector"
#include "vector"
#include "vector"
#include "vector"

using namespace std;

class RecoLevel {
public :
   TTree          *fChain;   //!pointer to the analyzed TTree or TChain
   Int_t           fCurrent; //!current Tree number in a TChain
   bool m_upgrade;

// Fixed size dimensions of array or collections stored in the TTree if any.

   // Declaration of leaf types
   vector<float>   *mc_generator_weights;
   Float_t         weight_mc;
   Float_t         weight_pileup;
   Float_t         weight_leptonSF;
   Float_t         weight_photonSF;
   Float_t         weight_bTagSF_77;
   Float_t         weight_bTagSF_85;
   Float_t         weight_bTagSF_70;
   Float_t         weight_bTagSF_Continuous;
   Float_t         weight_jvt;
   Float_t         weight_pileup_UP;
   Float_t         weight_pileup_DOWN;
   Float_t         weight_leptonSF_EL_SF_Trigger_UP;
   Float_t         weight_leptonSF_EL_SF_Trigger_DOWN;
   Float_t         weight_leptonSF_EL_SF_Reco_UP;
   Float_t         weight_leptonSF_EL_SF_Reco_DOWN;
   Float_t         weight_leptonSF_EL_SF_ID_UP;
   Float_t         weight_leptonSF_EL_SF_ID_DOWN;
   Float_t         weight_leptonSF_EL_SF_Isol_UP;
   Float_t         weight_leptonSF_EL_SF_Isol_DOWN;
   Float_t         weight_leptonSF_MU_SF_Trigger_STAT_UP;
   Float_t         weight_leptonSF_MU_SF_Trigger_STAT_DOWN;
   Float_t         weight_leptonSF_MU_SF_Trigger_SYST_UP;
   Float_t         weight_leptonSF_MU_SF_Trigger_SYST_DOWN;
   Float_t         weight_leptonSF_MU_SF_ID_STAT_UP;
   Float_t         weight_leptonSF_MU_SF_ID_STAT_DOWN;
   Float_t         weight_leptonSF_MU_SF_ID_SYST_UP;
   Float_t         weight_leptonSF_MU_SF_ID_SYST_DOWN;
   Float_t         weight_leptonSF_MU_SF_ID_STAT_LOWPT_UP;
   Float_t         weight_leptonSF_MU_SF_ID_STAT_LOWPT_DOWN;
   Float_t         weight_leptonSF_MU_SF_ID_SYST_LOWPT_UP;
   Float_t         weight_leptonSF_MU_SF_ID_SYST_LOWPT_DOWN;
   Float_t         weight_leptonSF_MU_SF_Isol_STAT_UP;
   Float_t         weight_leptonSF_MU_SF_Isol_STAT_DOWN;
   Float_t         weight_leptonSF_MU_SF_Isol_SYST_UP;
   Float_t         weight_leptonSF_MU_SF_Isol_SYST_DOWN;
   Float_t         weight_leptonSF_MU_SF_TTVA_STAT_UP;
   Float_t         weight_leptonSF_MU_SF_TTVA_STAT_DOWN;
   Float_t         weight_leptonSF_MU_SF_TTVA_SYST_UP;
   Float_t         weight_leptonSF_MU_SF_TTVA_SYST_DOWN;
   Float_t         weight_indiv_SF_EL_Trigger;
   Float_t         weight_indiv_SF_EL_Trigger_UP;
   Float_t         weight_indiv_SF_EL_Trigger_DOWN;
   Float_t         weight_indiv_SF_EL_Reco;
   Float_t         weight_indiv_SF_EL_Reco_UP;
   Float_t         weight_indiv_SF_EL_Reco_DOWN;
   Float_t         weight_indiv_SF_EL_ID;
   Float_t         weight_indiv_SF_EL_ID_UP;
   Float_t         weight_indiv_SF_EL_ID_DOWN;
   Float_t         weight_indiv_SF_EL_Isol;
   Float_t         weight_indiv_SF_EL_Isol_UP;
   Float_t         weight_indiv_SF_EL_Isol_DOWN;
   Float_t         weight_indiv_SF_EL_ChargeID;
   Float_t         weight_indiv_SF_EL_ChargeID_UP;
   Float_t         weight_indiv_SF_EL_ChargeID_DOWN;
   Float_t         weight_indiv_SF_EL_ChargeMisID;
   Float_t         weight_indiv_SF_EL_ChargeMisID_STAT_UP;
   Float_t         weight_indiv_SF_EL_ChargeMisID_STAT_DOWN;
   Float_t         weight_indiv_SF_EL_ChargeMisID_SYST_UP;
   Float_t         weight_indiv_SF_EL_ChargeMisID_SYST_DOWN;
   Float_t         weight_indiv_SF_MU_Trigger;
   Float_t         weight_indiv_SF_MU_Trigger_STAT_UP;
   Float_t         weight_indiv_SF_MU_Trigger_STAT_DOWN;
   Float_t         weight_indiv_SF_MU_Trigger_SYST_UP;
   Float_t         weight_indiv_SF_MU_Trigger_SYST_DOWN;
   Float_t         weight_indiv_SF_MU_ID;
   Float_t         weight_indiv_SF_MU_ID_STAT_UP;
   Float_t         weight_indiv_SF_MU_ID_STAT_DOWN;
   Float_t         weight_indiv_SF_MU_ID_SYST_UP;
   Float_t         weight_indiv_SF_MU_ID_SYST_DOWN;
   Float_t         weight_indiv_SF_MU_ID_STAT_LOWPT_UP;
   Float_t         weight_indiv_SF_MU_ID_STAT_LOWPT_DOWN;
   Float_t         weight_indiv_SF_MU_ID_SYST_LOWPT_UP;
   Float_t         weight_indiv_SF_MU_ID_SYST_LOWPT_DOWN;
   Float_t         weight_indiv_SF_MU_Isol;
   Float_t         weight_indiv_SF_MU_Isol_STAT_UP;
   Float_t         weight_indiv_SF_MU_Isol_STAT_DOWN;
   Float_t         weight_indiv_SF_MU_Isol_SYST_UP;
   Float_t         weight_indiv_SF_MU_Isol_SYST_DOWN;
   Float_t         weight_indiv_SF_MU_TTVA;
   Float_t         weight_indiv_SF_MU_TTVA_STAT_UP;
   Float_t         weight_indiv_SF_MU_TTVA_STAT_DOWN;
   Float_t         weight_indiv_SF_MU_TTVA_SYST_UP;
   Float_t         weight_indiv_SF_MU_TTVA_SYST_DOWN;
   Float_t         weight_photonSF_ID_UP;
   Float_t         weight_photonSF_ID_DOWN;
   Float_t         weight_photonSF_effIso;
   Float_t         weight_photonSF_effLowPtIso_UP;
   Float_t         weight_photonSF_effLowPtIso_DOWN;
   Float_t         weight_photonSF_effTrkIso_UP;
   Float_t         weight_photonSF_effTrkIso_DOWN;
   Float_t         weight_jvt_UP;
   Float_t         weight_jvt_DOWN;
   vector<float>   *weight_bTagSF_77_eigenvars_B_up;
   vector<float>   *weight_bTagSF_77_eigenvars_C_up;
   vector<float>   *weight_bTagSF_77_eigenvars_Light_up;
   vector<float>   *weight_bTagSF_77_eigenvars_B_down;
   vector<float>   *weight_bTagSF_77_eigenvars_C_down;
   vector<float>   *weight_bTagSF_77_eigenvars_Light_down;
   Float_t         weight_bTagSF_77_extrapolation_up;
   Float_t         weight_bTagSF_77_extrapolation_down;
   Float_t         weight_bTagSF_77_extrapolation_from_charm_up;
   Float_t         weight_bTagSF_77_extrapolation_from_charm_down;
   vector<float>   *weight_bTagSF_85_eigenvars_B_up;
   vector<float>   *weight_bTagSF_85_eigenvars_C_up;
   vector<float>   *weight_bTagSF_85_eigenvars_Light_up;
   vector<float>   *weight_bTagSF_85_eigenvars_B_down;
   vector<float>   *weight_bTagSF_85_eigenvars_C_down;
   vector<float>   *weight_bTagSF_85_eigenvars_Light_down;
   Float_t         weight_bTagSF_85_extrapolation_up;
   Float_t         weight_bTagSF_85_extrapolation_down;
   Float_t         weight_bTagSF_85_extrapolation_from_charm_up;
   Float_t         weight_bTagSF_85_extrapolation_from_charm_down;
   vector<float>   *weight_bTagSF_70_eigenvars_B_up;
   vector<float>   *weight_bTagSF_70_eigenvars_C_up;
   vector<float>   *weight_bTagSF_70_eigenvars_Light_up;
   vector<float>   *weight_bTagSF_70_eigenvars_B_down;
   vector<float>   *weight_bTagSF_70_eigenvars_C_down;
   vector<float>   *weight_bTagSF_70_eigenvars_Light_down;
   Float_t         weight_bTagSF_70_extrapolation_up;
   Float_t         weight_bTagSF_70_extrapolation_down;
   Float_t         weight_bTagSF_70_extrapolation_from_charm_up;
   Float_t         weight_bTagSF_70_extrapolation_from_charm_down;
   vector<float>   *weight_bTagSF_Continuous_eigenvars_B_up;
   vector<float>   *weight_bTagSF_Continuous_eigenvars_C_up;
   vector<float>   *weight_bTagSF_Continuous_eigenvars_Light_up;
   vector<float>   *weight_bTagSF_Continuous_eigenvars_B_down;
   vector<float>   *weight_bTagSF_Continuous_eigenvars_C_down;
   vector<float>   *weight_bTagSF_Continuous_eigenvars_Light_down;
   Float_t         weight_bTagSF_Continuous_extrapolation_from_charm_up;
   Float_t         weight_bTagSF_Continuous_extrapolation_from_charm_down;
   ULong64_t       eventNumber;
   UInt_t          runNumber;
   UInt_t          randomRunNumber;
   UInt_t          mcChannelNumber;
   Float_t         mu;
   UInt_t          backgroundFlags;
   UInt_t          hasBadMuon;
   vector<float>   *el_pt;
   vector<float>   *el_eta;
   vector<float>   *el_cl_eta;
   vector<float>   *el_phi;
   vector<float>   *el_e;
   vector<float>   *el_charge;
   vector<float>   *el_topoetcone20;
   vector<float>   *el_ptvarcone20;
   vector<char>    *el_CF;
   vector<float>   *el_d0sig;
   vector<float>   *el_delta_z0_sintheta;
   vector<int>     *el_true_type;
   vector<int>     *el_true_origin;
   vector<int>     *el_true_typebkg;
   vector<int>     *el_true_originbkg;
   vector<float>     *el_faketype;
   vector<float>   *mu_pt;
   vector<float>   *mu_eta;
   vector<float>   *mu_phi;
   vector<float>   *mu_e;
   vector<float>   *mu_charge;
   vector<float>   *mu_topoetcone20;
   vector<float>   *mu_ptvarcone30;
   vector<float>   *mu_d0sig;
   vector<float>   *mu_delta_z0_sintheta;
   vector<int>     *mu_true_type;
   vector<int>     *mu_true_origin;
   vector<char>     *mu_true_isPrompt;
   vector<float>   *ph_pt;
   vector<float>   *ph_eta;
   vector<float>   *ph_phi;
   vector<float>   *ph_e;
   vector<float>   *ph_iso;
   vector<float>   *jet_pt;
   vector<float>   *jet_eta;
   vector<float>   *jet_phi;
   vector<float>   *jet_e;
   vector<float>   *jet_mv2c00;
   vector<float>   *jet_mv2c10;
   vector<float>   *jet_mv2c20;
   vector<float>   *jet_ip3dsv1;
   vector<float>   *jet_jvt;
   vector<char>    *jet_passfjvt;
   vector<int>     *jet_truthflav;
   vector<int>     *jet_truthPartonLabel;
   vector<char>    *jet_isTrueHS;
   vector<char>    *jet_isbtagged_77;
   vector<char>    *jet_isbtagged_85;
   vector<char>    *jet_isbtagged_70;
   vector<int>     *jet_tagWeightBin;
   Float_t         met_met;
   Float_t         met_phi;
   Int_t           ejets_gamma_basic;
   Int_t           mujets_gamma_basic;
   Int_t           ee_gamma_basic;
   Int_t           emu_gamma_basic;
   Int_t           mumu_gamma_basic;
   Int_t           ejets_2015;
   Int_t           ejets_2015_pl;
   Int_t           mujets_2015;
   Int_t           mujets_2015_pl;
   Int_t           emu_2015;
   Int_t           emu_2015_pl;
   Int_t           ee_2015;
   Int_t           ee_2015_pl;
   Int_t           mumu_2015;
   Int_t           mumu_2015_pl;
   Int_t           ejets_2016;
   Int_t           ejets_2016_pl;
   Int_t           mujets_2016;
   Int_t           mujets_2016_pl;
   Int_t           emu_2016;
   Int_t           emu_2016_pl;
   Int_t           ee_2016;
   Int_t           ee_2016_pl;
   Int_t           mumu_2016;
   Int_t           mumu_2016_pl;
   Char_t          HLT_e60_lhmedium_nod0;
   Char_t          HLT_mu26_ivarmedium;
   Char_t          HLT_e26_lhtight_nod0_ivarloose;
   Char_t          HLT_e140_lhloose_nod0;
   Char_t          HLT_mu20_iloose_L1MU15;
   Char_t          HLT_mu50;
   Char_t          HLT_e60_lhmedium;
   Char_t          HLT_e24_lhmedium_L1EM20VH;
   Char_t          HLT_e120_lhloose;
   vector<char>    *el_trigMatch_HLT_e60_lhmedium_nod0;
   vector<char>    *el_trigMatch_HLT_e26_lhtight_nod0_ivarloose;
   vector<char>    *el_trigMatch_HLT_e140_lhloose_nod0;
   vector<char>    *el_trigMatch_HLT_e60_lhmedium;
   vector<char>    *el_trigMatch_HLT_e24_lhmedium_L1EM20VH;
   vector<char>    *el_trigMatch_HLT_e120_lhloose;
   vector<char>    *mu_trigMatch_HLT_mu26_ivarmedium;
   vector<char>    *mu_trigMatch_HLT_mu50;
   vector<char>    *mu_trigMatch_HLT_mu20_iloose_L1MU15;
   vector<float>   *ph_topoetcone20;
   vector<float>   *ph_topoetcone30;
   vector<float>   *ph_topoetcone40;
   vector<float>   *ph_ptcone20;
   vector<float>   *ph_ptcone30;
   vector<float>   *ph_ptcone40;
   vector<float>   *ph_ptvarcone20;
   vector<float>   *ph_ptvarcone30;
   vector<float>   *ph_ptvarcone40;
   vector<char>    *ph_isoFCTCO;
   vector<char>    *ph_isoFCT;
   vector<char>    *ph_isoFCL;
   vector<char>    *ph_isTight;
   vector<char>    *ph_isLoose;
   vector<char>    *ph_isTight_daod;
   vector<char>    *ph_isHFake;
   vector<char>    *ph_isHadronFakeFailedDeltaE;
   vector<char>    *ph_isHadronFakeFailedFside;
   vector<char>    *ph_isHadronFakeFailedWs3;
   vector<char>    *ph_isHadronFakeFailedERatio;
   vector<float>   *ph_rhad1;
   vector<float>   *ph_rhad;
   vector<float>   *ph_reta;
   vector<float>   *ph_weta2;
   vector<float>   *ph_rphi;
   vector<float>   *ph_ws3;
   vector<float>   *ph_wstot;
   vector<float>   *ph_fracm;
   vector<float>   *ph_deltaE;
   vector<float>   *ph_eratio;
   vector<float>   *ph_emaxs1;
   vector<float>   *ph_f1;
   vector<float>   *ph_e277;
   vector<unsigned int> *ph_OQ;
   vector<unsigned int> *ph_author;
   vector<int>     *ph_conversionType;
   vector<float>   *ph_caloEta;
   vector<unsigned int> *ph_isEM;
   vector<int>     *ph_nVertices;
   vector<float>   *ph_SF_eff;
   vector<float>   *ph_SF_effUP;
   vector<float>   *ph_SF_effDO;
   vector<float>   *ph_SF_iso;
   vector<float>   *ph_SF_lowisoUP;
   vector<float>   *ph_SF_lowisoDO;
   vector<float>   *ph_SF_trkisoUP;
   vector<float>   *ph_SF_trkisoDO;
   vector<float>   *ph_drleadjet;
   vector<float>   *ph_dralljet;
   vector<float>   *ph_drsubljet;
   vector<float>   *ph_drlept;
   vector<float>   *ph_mgammalept;
   vector<float>   *ph_mgammaleptlept;
   Int_t           selph_index1;
   Int_t           selph_index2;
   Int_t           selhf_index1;
   vector<int>     *ph_truthType;
   vector<int>     *ph_truthOrigin;
   vector<float>     *ph_mc_Type;
   vector<float>     *ph_mc_Origin;
   vector<float>     *ph_faketype;
   vector<int>     *ph_truthAncestor;
   vector<int>     *ph_mc_pid;
   vector<int>     *ph_mc_barcode;
   vector<float>   *ph_mc_pt;
   vector<float>   *ph_mc_eta;
   vector<float>   *ph_mc_phi;
   vector<float>   *ph_mcel_dr;
   vector<float>   *ph_mcel_pt;
   vector<float>   *ph_mcel_eta;
   vector<float>   *ph_mcel_phi;
   vector<float>   *mcph_pt;
   vector<float>   *mcph_eta;
   vector<float>   *mcph_phi;
   vector<int>     *mcph_ancestor;
   vector<int>     *el_truthAncestor;
   vector<char>    *el_isoGradient;
   vector<int>     *el_mc_pid;
   vector<float>   *el_mc_charge;
   vector<float>   *el_mc_pt;
   vector<float>   *el_mc_eta;
   vector<float>   *el_mc_phi;
   vector<float>   *mu_mc_charge;
   vector<float>   *jet_mcdr_tW1;
   vector<float>   *jet_mcdr_tW2;
   vector<float>   *jet_mcdr_tB;
   vector<float>   *jet_mcdr_tbW1;
   vector<float>   *jet_mcdr_tbW2;
   vector<float>   *jet_mcdr_tbB;
   vector<int>     *lepton_type;
   vector<int>	   *jet_nGhosts_bHadron;
    vector<float>   *jet_mv1eff;
   Float_t         event_HT;
   Float_t         event_mll;
   Float_t         event_mwt;
   Int_t           event_njets;
   Int_t           event_nbjets70;
   Int_t           event_nbjets77;
   Int_t           event_nbjets85;
   Int_t           event_ngoodphotons;
   Int_t           event_photonorigin;
   Int_t           event_photonoriginTA;
   Int_t           event_nhadronfakes;
   vector<float>   *ph_HFT_MVA;
   Int_t           ph_nHFT_MVA;
   vector<float>   *event_ELD_MVA;
   Float_t         jet_pt_1st;
   Float_t         jet_pt_2nd;
   Float_t         jet_pt_3rd;
   Float_t         jet_pt_4th;
   Float_t         jet_pt_5th;
   Float_t         jet_pt_6th;
   Int_t           jet_tagWeightBin_leading;
   Int_t           jet_tagWeightBin_subleading;
   Int_t           jet_tagWeightBin_subsubleading;

   // List of branches
   TBranch        *b_weight_mc;   //!
   TBranch        *b_weight_pileup;   //!
   TBranch        *b_weight_leptonSF;   //!
   TBranch        *b_weight_photonSF;   //!
   TBranch        *b_weight_bTagSF_77;   //!
   TBranch        *b_weight_bTagSF_85;   //!
   TBranch        *b_weight_bTagSF_70;   //!
   TBranch        *b_weight_bTagSF_Continuous;   //!
   TBranch        *b_weight_jvt;   //!
   TBranch        *b_weight_pileup_UP;   //!
   TBranch        *b_weight_pileup_DOWN;   //!
   TBranch        *b_weight_leptonSF_EL_SF_Trigger_UP;   //!
   TBranch        *b_weight_leptonSF_EL_SF_Trigger_DOWN;   //!
   TBranch        *b_weight_leptonSF_EL_SF_Reco_UP;   //!
   TBranch        *b_weight_leptonSF_EL_SF_Reco_DOWN;   //!
   TBranch        *b_weight_leptonSF_EL_SF_ID_UP;   //!
   TBranch        *b_weight_leptonSF_EL_SF_ID_DOWN;   //!
   TBranch        *b_weight_leptonSF_EL_SF_Isol_UP;   //!
   TBranch        *b_weight_leptonSF_EL_SF_Isol_DOWN;   //!
   TBranch        *b_weight_leptonSF_MU_SF_Trigger_STAT_UP;   //!
   TBranch        *b_weight_leptonSF_MU_SF_Trigger_STAT_DOWN;   //!
   TBranch        *b_weight_leptonSF_MU_SF_Trigger_SYST_UP;   //!
   TBranch        *b_weight_leptonSF_MU_SF_Trigger_SYST_DOWN;   //!
   TBranch        *b_weight_leptonSF_MU_SF_ID_STAT_UP;   //!
   TBranch        *b_weight_leptonSF_MU_SF_ID_STAT_DOWN;   //!
   TBranch        *b_weight_leptonSF_MU_SF_ID_SYST_UP;   //!
   TBranch        *b_weight_leptonSF_MU_SF_ID_SYST_DOWN;   //!
   TBranch        *b_weight_leptonSF_MU_SF_ID_STAT_LOWPT_UP;   //!
   TBranch        *b_weight_leptonSF_MU_SF_ID_STAT_LOWPT_DOWN;   //!
   TBranch        *b_weight_leptonSF_MU_SF_ID_SYST_LOWPT_UP;   //!
   TBranch        *b_weight_leptonSF_MU_SF_ID_SYST_LOWPT_DOWN;   //!
   TBranch        *b_weight_leptonSF_MU_SF_Isol_STAT_UP;   //!
   TBranch        *b_weight_leptonSF_MU_SF_Isol_STAT_DOWN;   //!
   TBranch        *b_weight_leptonSF_MU_SF_Isol_SYST_UP;   //!
   TBranch        *b_weight_leptonSF_MU_SF_Isol_SYST_DOWN;   //!
   TBranch        *b_weight_leptonSF_MU_SF_TTVA_STAT_UP;   //!
   TBranch        *b_weight_leptonSF_MU_SF_TTVA_STAT_DOWN;   //!
   TBranch        *b_weight_leptonSF_MU_SF_TTVA_SYST_UP;   //!
   TBranch        *b_weight_leptonSF_MU_SF_TTVA_SYST_DOWN;   //!
   TBranch        *b_weight_indiv_SF_EL_Trigger;   //!
   TBranch        *b_weight_indiv_SF_EL_Trigger_UP;   //!
   TBranch        *b_weight_indiv_SF_EL_Trigger_DOWN;   //!
   TBranch        *b_weight_indiv_SF_EL_Reco;   //!
   TBranch        *b_weight_indiv_SF_EL_Reco_UP;   //!
   TBranch        *b_weight_indiv_SF_EL_Reco_DOWN;   //!
   TBranch        *b_weight_indiv_SF_EL_ID;   //!
   TBranch        *b_weight_indiv_SF_EL_ID_UP;   //!
   TBranch        *b_weight_indiv_SF_EL_ID_DOWN;   //!
   TBranch        *b_weight_indiv_SF_EL_Isol;   //!
   TBranch        *b_weight_indiv_SF_EL_Isol_UP;   //!
   TBranch        *b_weight_indiv_SF_EL_Isol_DOWN;   //!
   TBranch        *b_weight_indiv_SF_EL_ChargeID;   //!
   TBranch        *b_weight_indiv_SF_EL_ChargeID_UP;   //!
   TBranch        *b_weight_indiv_SF_EL_ChargeID_DOWN;   //!
   TBranch        *b_weight_indiv_SF_EL_ChargeMisID;   //!
   TBranch        *b_weight_indiv_SF_EL_ChargeMisID_STAT_UP;   //!
   TBranch        *b_weight_indiv_SF_EL_ChargeMisID_STAT_DOWN;   //!
   TBranch        *b_weight_indiv_SF_EL_ChargeMisID_SYST_UP;   //!
   TBranch        *b_weight_indiv_SF_EL_ChargeMisID_SYST_DOWN;   //!
   TBranch        *b_weight_indiv_SF_MU_Trigger;   //!
   TBranch        *b_weight_indiv_SF_MU_Trigger_STAT_UP;   //!
   TBranch        *b_weight_indiv_SF_MU_Trigger_STAT_DOWN;   //!
   TBranch        *b_weight_indiv_SF_MU_Trigger_SYST_UP;   //!
   TBranch        *b_weight_indiv_SF_MU_Trigger_SYST_DOWN;   //!
   TBranch        *b_weight_indiv_SF_MU_ID;   //!
   TBranch        *b_weight_indiv_SF_MU_ID_STAT_UP;   //!
   TBranch        *b_weight_indiv_SF_MU_ID_STAT_DOWN;   //!
   TBranch        *b_weight_indiv_SF_MU_ID_SYST_UP;   //!
   TBranch        *b_weight_indiv_SF_MU_ID_SYST_DOWN;   //!
   TBranch        *b_weight_indiv_SF_MU_ID_STAT_LOWPT_UP;   //!
   TBranch        *b_weight_indiv_SF_MU_ID_STAT_LOWPT_DOWN;   //!
   TBranch        *b_weight_indiv_SF_MU_ID_SYST_LOWPT_UP;   //!
   TBranch        *b_weight_indiv_SF_MU_ID_SYST_LOWPT_DOWN;   //!
   TBranch        *b_weight_indiv_SF_MU_Isol;   //!
   TBranch        *b_weight_indiv_SF_MU_Isol_STAT_UP;   //!
   TBranch        *b_weight_indiv_SF_MU_Isol_STAT_DOWN;   //!
   TBranch        *b_weight_indiv_SF_MU_Isol_SYST_UP;   //!
   TBranch        *b_weight_indiv_SF_MU_Isol_SYST_DOWN;   //!
   TBranch        *b_weight_indiv_SF_MU_TTVA;   //!
   TBranch        *b_weight_indiv_SF_MU_TTVA_STAT_UP;   //!
   TBranch        *b_weight_indiv_SF_MU_TTVA_STAT_DOWN;   //!
   TBranch        *b_weight_indiv_SF_MU_TTVA_SYST_UP;   //!
   TBranch        *b_weight_indiv_SF_MU_TTVA_SYST_DOWN;   //!
   TBranch        *b_weight_photonSF_ID_UP;   //!
   TBranch        *b_weight_photonSF_ID_DOWN;   //!
   TBranch        *b_weight_photonSF_effIso;   //!
   TBranch        *b_weight_photonSF_effLowPtIso_UP;   //!
   TBranch        *b_weight_photonSF_effLowPtIso_DOWN;   //!
   TBranch        *b_weight_photonSF_effTrkIso_UP;   //!
   TBranch        *b_weight_photonSF_effTrkIso_DOWN;   //!
   TBranch        *b_weight_jvt_UP;   //!
   TBranch        *b_weight_jvt_DOWN;   //!
   TBranch        *b_weight_bTagSF_77_eigenvars_B_up;   //!
   TBranch	  *b_mc_generator_weights;
   TBranch        *b_weight_bTagSF_77_eigenvars_C_up;   //!
   TBranch        *b_weight_bTagSF_77_eigenvars_Light_up;   //!
   TBranch        *b_weight_bTagSF_77_eigenvars_B_down;   //!
   TBranch        *b_weight_bTagSF_77_eigenvars_C_down;   //!
   TBranch        *b_weight_bTagSF_77_eigenvars_Light_down;   //!
   TBranch        *b_weight_bTagSF_77_extrapolation_up;   //!
   TBranch        *b_weight_bTagSF_77_extrapolation_down;   //!
   TBranch        *b_weight_bTagSF_77_extrapolation_from_charm_up;   //!
   TBranch        *b_weight_bTagSF_77_extrapolation_from_charm_down;   //!
   TBranch        *b_weight_bTagSF_85_eigenvars_B_up;   //!
   TBranch        *b_weight_bTagSF_85_eigenvars_C_up;   //!
   TBranch        *b_weight_bTagSF_85_eigenvars_Light_up;   //!
   TBranch        *b_weight_bTagSF_85_eigenvars_B_down;   //!
   TBranch        *b_weight_bTagSF_85_eigenvars_C_down;   //!
   TBranch        *b_weight_bTagSF_85_eigenvars_Light_down;   //!
   TBranch        *b_weight_bTagSF_85_extrapolation_up;   //!
   TBranch        *b_weight_bTagSF_85_extrapolation_down;   //!
   TBranch        *b_weight_bTagSF_85_extrapolation_from_charm_up;   //!
   TBranch        *b_weight_bTagSF_85_extrapolation_from_charm_down;   //!
   TBranch        *b_weight_bTagSF_70_eigenvars_B_up;   //!
   TBranch        *b_weight_bTagSF_70_eigenvars_C_up;   //!
   TBranch        *b_weight_bTagSF_70_eigenvars_Light_up;   //!
   TBranch        *b_weight_bTagSF_70_eigenvars_B_down;   //!
   TBranch        *b_weight_bTagSF_70_eigenvars_C_down;   //!
   TBranch        *b_weight_bTagSF_70_eigenvars_Light_down;   //!
   TBranch        *b_weight_bTagSF_70_extrapolation_up;   //!
   TBranch        *b_weight_bTagSF_70_extrapolation_down;   //!
   TBranch        *b_weight_bTagSF_70_extrapolation_from_charm_up;   //!
   TBranch        *b_weight_bTagSF_70_extrapolation_from_charm_down;   //!
   TBranch        *b_weight_bTagSF_Continuous_eigenvars_B_up;   //!
   TBranch        *b_weight_bTagSF_Continuous_eigenvars_C_up;   //!
   TBranch        *b_weight_bTagSF_Continuous_eigenvars_Light_up;   //!
   TBranch        *b_weight_bTagSF_Continuous_eigenvars_B_down;   //!
   TBranch        *b_weight_bTagSF_Continuous_eigenvars_C_down;   //!
   TBranch        *b_weight_bTagSF_Continuous_eigenvars_Light_down;   //!
   TBranch        *b_weight_bTagSF_Continuous_extrapolation_from_charm_up;   //!
   TBranch        *b_weight_bTagSF_Continuous_extrapolation_from_charm_down;   //!
   TBranch        *b_eventNumber;   //!
   TBranch        *b_runNumber;   //!
   TBranch        *b_randomRunNumber;   //!
   TBranch        *b_mcChannelNumber;   //!
   TBranch        *b_mu;   //!
   TBranch        *b_backgroundFlags;   //!
   TBranch        *b_hasBadMuon;   //!
   TBranch        *b_el_pt;   //!
   TBranch        *b_el_eta;   //!
   TBranch        *b_el_cl_eta;   //!
   TBranch        *b_el_phi;   //!
   TBranch        *b_el_e;   //!
   TBranch        *b_el_charge;   //!
   TBranch        *b_el_topoetcone20;   //!
   TBranch        *b_el_ptvarcone20;   //!
   TBranch        *b_el_CF;   //!
   TBranch        *b_el_d0sig;   //!
   TBranch        *b_el_delta_z0_sintheta;   //!
   TBranch        *b_el_true_type;   //!
   TBranch        *b_el_true_origin;   //!
   TBranch        *b_el_true_typebkg;   //!
   TBranch        *b_el_true_originbkg;   //!
   TBranch        *b_el_faketype;   //!
   TBranch        *b_mu_pt;   //!
   TBranch        *b_mu_eta;   //!
   TBranch        *b_mu_phi;   //!
   TBranch        *b_mu_e;   //!
   TBranch        *b_mu_charge;   //!
   TBranch        *b_mu_topoetcone20;   //!
   TBranch        *b_mu_ptvarcone30;   //!
   TBranch        *b_mu_d0sig;   //!
   TBranch        *b_mu_delta_z0_sintheta;   //!
   TBranch        *b_mu_true_type;   //!
   TBranch        *b_mu_true_origin;   //!
   TBranch        *b_mu_true_isPrompt;   //!
   TBranch        *b_ph_pt;   //!
   TBranch        *b_ph_eta;   //!
   TBranch        *b_ph_phi;   //!
   TBranch        *b_ph_e;   //!
   TBranch        *b_ph_iso;   //!
   TBranch        *b_jet_pt;   //!
   TBranch        *b_jet_eta;   //!
   TBranch        *b_jet_phi;   //!
   TBranch        *b_jet_e;   //!
   TBranch        *b_jet_mv2c00;   //!
   TBranch        *b_jet_mv2c10;   //!
   TBranch        *b_jet_mv2c20;   //!
   TBranch        *b_jet_ip3dsv1;   //!
   TBranch        *b_jet_jvt;   //!
   TBranch        *b_jet_passfjvt;   //!
   TBranch        *b_jet_truthflav;   //!
   TBranch        *b_jet_truthPartonLabel;   //!
   TBranch        *b_jet_isTrueHS;   //!
   TBranch        *b_jet_isbtagged_77;   //!
   TBranch        *b_jet_isbtagged_85;   //!
   TBranch        *b_jet_isbtagged_70;   //!
   TBranch        *b_jet_tagWeightBin;   //!
   TBranch        *b_met_met;   //!
   TBranch        *b_met_phi;   //!
   TBranch        *b_ejets_gamma_basic;   //!
   TBranch        *b_mujets_gamma_basic;   //!
   TBranch        *b_ee_gamma_basic;   //!
   TBranch        *b_emu_gamma_basic;   //!
   TBranch        *b_mumu_gamma_basic;   //!
   TBranch        *b_ejets_2015;   //!
   TBranch        *b_ejets_2015_pl;   //!
   TBranch        *b_mujets_2015;   //!
   TBranch        *b_mujets_2015_pl;   //!
   TBranch        *b_emu_2015;   //!
   TBranch        *b_emu_2015_pl;   //!
   TBranch        *b_ee_2015;   //!
   TBranch        *b_ee_2015_pl;   //!
   TBranch        *b_mumu_2015;   //!
   TBranch        *b_mumu_2015_pl;   //!
   TBranch        *b_ejets_2016;   //!
   TBranch        *b_ejets_2016_pl;   //!
   TBranch        *b_mujets_2016;   //!
   TBranch        *b_mujets_2016_pl;   //!
   TBranch        *b_emu_2016;   //!
   TBranch        *b_emu_2016_pl;   //!
   TBranch        *b_ee_2016;   //!
   TBranch        *b_ee_2016_pl;   //!
   TBranch        *b_mumu_2016;   //!
   TBranch        *b_mumu_2016_pl;   //!
   TBranch        *b_HLT_e60_lhmedium_nod0;   //!
   TBranch        *b_HLT_mu26_ivarmedium;   //!
   TBranch        *b_HLT_e26_lhtight_nod0_ivarloose;   //!
   TBranch        *b_HLT_e140_lhloose_nod0;   //!
   TBranch        *b_HLT_mu20_iloose_L1MU15;   //!
   TBranch        *b_HLT_mu50;   //!
   TBranch        *b_HLT_e60_lhmedium;   //!
   TBranch        *b_HLT_e24_lhmedium_L1EM20VH;   //!
   TBranch        *b_HLT_e120_lhloose;   //!
   TBranch        *b_el_trigMatch_HLT_e60_lhmedium_nod0;   //!
   TBranch        *b_el_trigMatch_HLT_e26_lhtight_nod0_ivarloose;   //!
   TBranch        *b_el_trigMatch_HLT_e140_lhloose_nod0;   //!
   TBranch        *b_el_trigMatch_HLT_e60_lhmedium;   //!
   TBranch        *b_el_trigMatch_HLT_e24_lhmedium_L1EM20VH;   //!
   TBranch        *b_el_trigMatch_HLT_e120_lhloose;   //!
   TBranch        *b_mu_trigMatch_HLT_mu26_ivarmedium;   //!
   TBranch        *b_mu_trigMatch_HLT_mu50;   //!
   TBranch        *b_mu_trigMatch_HLT_mu20_iloose_L1MU15;   //!
   TBranch        *b_ph_topoetcone20;   //!
   TBranch        *b_ph_topoetcone30;   //!
   TBranch        *b_ph_topoetcone40;   //!
   TBranch        *b_ph_ptcone20;   //!
   TBranch        *b_ph_ptcone30;   //!
   TBranch        *b_ph_ptcone40;   //!
   TBranch        *b_ph_ptvarcone20;   //!
   TBranch        *b_ph_ptvarcone30;   //!
   TBranch        *b_ph_ptvarcone40;   //!
   TBranch        *b_ph_isoFCTCO;   //!
   TBranch        *b_ph_isoFCT;   //!
   TBranch        *b_ph_isoFCL;   //!
   TBranch        *b_ph_isTight;   //!
   TBranch        *b_ph_isLoose;   //!
   TBranch        *b_ph_isTight_daod;   //!
   TBranch        *b_ph_isHFake;   //!
   TBranch        *b_ph_isHadronFakeFailedDeltaE;   //!
   TBranch        *b_ph_isHadronFakeFailedFside;   //!
   TBranch        *b_ph_isHadronFakeFailedWs3;   //!
   TBranch        *b_ph_isHadronFakeFailedERatio;   //!
   TBranch        *b_ph_rhad1;   //!
   TBranch        *b_ph_rhad;   //!
   TBranch        *b_ph_reta;   //!
   TBranch        *b_ph_weta2;   //!
   TBranch        *b_ph_rphi;   //!
   TBranch        *b_ph_ws3;   //!
   TBranch        *b_ph_wstot;   //!
   TBranch        *b_ph_fracm;   //!
   TBranch        *b_ph_deltaE;   //!
   TBranch        *b_ph_eratio;   //!
   TBranch        *b_ph_emaxs1;   //!
   TBranch        *b_ph_f1;   //!
   TBranch        *b_ph_e277;   //!
   TBranch        *b_ph_OQ;   //!
   TBranch        *b_ph_author;   //!
   TBranch        *b_ph_conversionType;   //!
   TBranch        *b_ph_caloEta;   //!
   TBranch        *b_ph_isEM;   //!
   TBranch        *b_ph_nVertices;   //!
   TBranch        *b_ph_SF_eff;   //!
   TBranch        *b_ph_SF_effUP;   //!
   TBranch        *b_ph_SF_effDO;   //!
   TBranch        *b_ph_SF_iso;   //!
   TBranch        *b_ph_SF_lowisoUP;   //!
   TBranch        *b_ph_SF_lowisoDO;   //!
   TBranch        *b_ph_SF_trkisoUP;   //!
   TBranch        *b_ph_SF_trkisoDO;   //!
   TBranch        *b_ph_drleadjet;   //!
   TBranch        *b_ph_dralljet;   //!
   TBranch        *b_ph_drsubljet;   //!
   TBranch        *b_ph_drlept;   //!
   TBranch        *b_ph_mgammalept;   //!
   TBranch        *b_ph_mgammaleptlept;   //!
   TBranch        *b_selph_index1;   //!
   TBranch        *b_selph_index2;   //!
   TBranch        *b_selhf_index1;   //!
   TBranch        *b_ph_truthType;   //!
   TBranch        *b_ph_truthOrigin;   //!
   TBranch        *b_ph_mc_Type;   //!
   TBranch        *b_ph_mc_Origin;   //!
   TBranch        *b_ph_faketype;   //!
   TBranch        *b_ph_truthAncestor;   //!
   TBranch        *b_ph_mc_pid;   //!
   TBranch        *b_ph_mc_barcode;   //!
   TBranch        *b_ph_mc_pt;   //!
   TBranch        *b_ph_mc_eta;   //!
   TBranch        *b_ph_mc_phi;   //!
   TBranch        *b_ph_mcel_dr;   //!
   TBranch        *b_ph_mcel_pt;   //!
   TBranch        *b_ph_mcel_eta;   //!
   TBranch        *b_ph_mcel_phi;   //!
   TBranch        *b_mcph_pt;   //!
   TBranch        *b_mcph_eta;   //!
   TBranch        *b_mcph_phi;   //!
   TBranch        *b_mcph_ancestor;   //!
   TBranch        *b_el_truthAncestor;   //!
   TBranch        *b_el_isoGradient;   //!
   TBranch        *b_el_mc_pid;   //!
   TBranch        *b_el_mc_charge;   //!
   TBranch        *b_el_mc_pt;   //!
   TBranch        *b_el_mc_eta;   //!
   TBranch        *b_el_mc_phi;   //!
   TBranch        *b_mu_mc_charge;   //!
   TBranch        *b_jet_mcdr_tW1;   //!
   TBranch        *b_jet_mcdr_tW2;   //!
   TBranch        *b_jet_mcdr_tB;   //!
   TBranch        *b_jet_mcdr_tbW1;   //!
   TBranch        *b_jet_mcdr_tbW2;   //!
   TBranch        *b_jet_mcdr_tbB;   //!
   TBranch        *b_jet_mv1eff;   //!
   TBranch        *b_lepton_type;   //!
   TBranch        *b_event_HT;   //!
   TBranch        *b_event_mll;   //!
   TBranch        *b_event_mwt;   //!
   TBranch        *b_event_njets;   //!
   TBranch        *b_event_nbjets70;   //!
   TBranch        *b_event_nbjets77;   //!
   TBranch        *b_event_nbjets85;   //!
   TBranch        *b_event_ngoodphotons;   //!
   TBranch        *b_event_photonorigin;   //!
   TBranch        *b_event_photonoriginTA;   //!
   TBranch        *b_event_nhadronfakes;   //!
   TBranch        *b_ph_HFT_MVA;   //!
   TBranch        *b_ph_nHFT_MVA;   //!
   TBranch        *b_event_ELD_MVA;   //!
   TBranch        *b_jet_pt_1st;   //!
   TBranch        *b_jet_pt_2nd;   //!
   TBranch        *b_jet_pt_3rd;   //!
   TBranch        *b_jet_pt_4th;   //!
   TBranch        *b_jet_pt_5th;   //!
   TBranch        *b_jet_pt_6th;   //!
   TBranch        *b_jet_tagWeightBin_leading;   //!
   TBranch        *b_jet_tagWeightBin_subleading;   //!
   TBranch        *b_jet_tagWeightBin_subsubleading;   //!
   TBranch        *b_jet_nGhosts_bHadron;   //!

   RecoLevel(TTree *tree=0, bool Upgrade = false);
   virtual ~RecoLevel();
   virtual Int_t    Cut(Long64_t entry);
   virtual Int_t    GetTotalEntry();
   virtual Int_t    GetEntry(Long64_t entry);
   virtual Long64_t LoadTree(Long64_t entry);
   virtual void     Init(TTree *tree);
   virtual void     Loop();
   virtual Bool_t   Notify();
   virtual void     Show(Long64_t entry = -1);
};

#endif

#ifdef RecoLevel_cxx
RecoLevel::RecoLevel(TTree *tree, bool Upgrade) : fChain(0) 
{
// if parameter tree is not specified (or zero), connect the file
// used to generate this class and read the Tree.
   if (tree == 0) {
      TFile *f = (TFile*)gROOT->GetListOfFiles()->FindObject("/eos/user/c/caudron2/TtGamma_PL/v009/410389.ttgamma_noallhad.p3152.PL4.001.root");
      if (!f || !f->IsOpen()) {
         f = new TFile("/eos/user/c/caudron2/TtGamma_PL/v009/410389.ttgamma_noallhad.p3152.PL4.001.root");
      }
      f->GetObject("nominal",tree);

   }
   m_upgrade = Upgrade;
   Init(tree);
}

RecoLevel::~RecoLevel()
{
   if (!fChain) return;
   delete fChain->GetCurrentFile();
}

Int_t RecoLevel::GetTotalEntry()
{
// Read contents of entry.
   if (!fChain) return 0;
   return fChain->GetEntriesFast();
}

Int_t RecoLevel::GetEntry(Long64_t entry)
{
// Read contents of entry.
   if (!fChain) return 0;
   return fChain->GetEntry(entry);
}
Long64_t RecoLevel::LoadTree(Long64_t entry)
{
// Set the environment to read one entry
   if (!fChain) return -5;
   Long64_t centry = fChain->LoadTree(entry);
   if (centry < 0) return centry;
   if (fChain->GetTreeNumber() != fCurrent) {
      fCurrent = fChain->GetTreeNumber();
      Notify();
   }
   return centry;
}

void RecoLevel::Init(TTree *tree)
{
   // The Init() function is called when the selector needs to initialize
   // a new tree or chain. Typically here the branch addresses and branch
   // pointers of the tree will be set.
   // It is normally not necessary to make changes to the generated
   // code, but the routine can be extended by the user if needed.
   // Init() will be called many times when running on PROOF
   // (once per file to be processed).

   // Set object pointer
   mc_generator_weights = 0;
   weight_bTagSF_77_eigenvars_B_up = 0;
   weight_bTagSF_77_eigenvars_C_up = 0;
   weight_bTagSF_77_eigenvars_Light_up = 0;
   weight_bTagSF_77_eigenvars_B_down = 0;
   weight_bTagSF_77_eigenvars_C_down = 0;
   weight_bTagSF_77_eigenvars_Light_down = 0;
   weight_bTagSF_85_eigenvars_B_up = 0;
   weight_bTagSF_85_eigenvars_C_up = 0;
   weight_bTagSF_85_eigenvars_Light_up = 0;
   weight_bTagSF_85_eigenvars_B_down = 0;
   weight_bTagSF_85_eigenvars_C_down = 0;
   weight_bTagSF_85_eigenvars_Light_down = 0;
   weight_bTagSF_70_eigenvars_B_up = 0;
   weight_bTagSF_70_eigenvars_C_up = 0;
   weight_bTagSF_70_eigenvars_Light_up = 0;
   weight_bTagSF_70_eigenvars_B_down = 0;
   weight_bTagSF_70_eigenvars_C_down = 0;
   weight_bTagSF_70_eigenvars_Light_down = 0;
   weight_bTagSF_Continuous_eigenvars_B_up = 0;
   weight_bTagSF_Continuous_eigenvars_C_up = 0;
   weight_bTagSF_Continuous_eigenvars_Light_up = 0;
   weight_bTagSF_Continuous_eigenvars_B_down = 0;
   weight_bTagSF_Continuous_eigenvars_C_down = 0;
   weight_bTagSF_Continuous_eigenvars_Light_down = 0;
   el_pt = 0;
   el_eta = 0;
   el_cl_eta = 0;
   el_phi = 0;
   el_e = 0;
   el_charge = 0;
   el_topoetcone20 = 0;
   el_ptvarcone20 = 0;
   el_CF = 0;
   el_d0sig = 0;
   el_delta_z0_sintheta = 0;
   el_true_type = 0;
   el_true_origin = 0;
   el_true_typebkg = 0;
   el_true_originbkg = 0;
   el_faketype = 0;
   mu_pt = 0;
   mu_eta = 0;
   mu_phi = 0;
   mu_e = 0;
   mu_charge = 0;
   mu_topoetcone20 = 0;
   mu_ptvarcone30 = 0;
   mu_d0sig = 0;
   mu_delta_z0_sintheta = 0;
   mu_true_type = 0;
   mu_true_origin = 0;
   mu_true_isPrompt = 0;
   ph_pt = 0;
   ph_eta = 0;
   ph_phi = 0;
   ph_e = 0;
   ph_iso = 0;
   jet_pt = 0;
   jet_eta = 0;
   jet_phi = 0;
   jet_e = 0;
   jet_mv2c00 = 0;
   jet_mv2c10 = 0;
   jet_mv2c20 = 0;
   jet_ip3dsv1 = 0;
   jet_jvt = 0;
   jet_passfjvt = 0;
   jet_truthflav = 0;
   jet_truthPartonLabel = 0;
   jet_isTrueHS = 0;
   jet_isbtagged_77 = 0;
   jet_isbtagged_85 = 0;
   jet_isbtagged_70 = 0;
   jet_tagWeightBin = 0;
   el_trigMatch_HLT_e60_lhmedium_nod0 = 0;
   el_trigMatch_HLT_e26_lhtight_nod0_ivarloose = 0;
   el_trigMatch_HLT_e140_lhloose_nod0 = 0;
   el_trigMatch_HLT_e60_lhmedium = 0;
   el_trigMatch_HLT_e24_lhmedium_L1EM20VH = 0;
   el_trigMatch_HLT_e120_lhloose = 0;
   mu_trigMatch_HLT_mu26_ivarmedium = 0;
   mu_trigMatch_HLT_mu50 = 0;
   mu_trigMatch_HLT_mu20_iloose_L1MU15 = 0;
   ph_topoetcone20 = 0;
   ph_topoetcone30 = 0;
   ph_topoetcone40 = 0;
   ph_ptcone20 = 0;
   ph_ptcone30 = 0;
   ph_ptcone40 = 0;
   ph_ptvarcone20 = 0;
   ph_ptvarcone30 = 0;
   ph_ptvarcone40 = 0;
   ph_isoFCTCO = 0;
   ph_isoFCT = 0;
   ph_isoFCL = 0;
   ph_isTight = 0;
   ph_isLoose = 0;
   ph_isTight_daod = 0;
   ph_isHFake = 0;
   ph_isHadronFakeFailedDeltaE = 0;
   ph_isHadronFakeFailedFside = 0;
   ph_isHadronFakeFailedWs3 = 0;
   ph_isHadronFakeFailedERatio = 0;
   ph_rhad1 = 0;
   ph_rhad = 0;
   ph_reta = 0;
   ph_weta2 = 0;
   ph_rphi = 0;
   ph_ws3 = 0;
   ph_wstot = 0;
   ph_fracm = 0;
   ph_deltaE = 0;
   ph_eratio = 0;
   ph_emaxs1 = 0;
   ph_f1 = 0;
   ph_e277 = 0;
   ph_OQ = 0;
   ph_author = 0;
   ph_conversionType = 0;
   ph_caloEta = 0;
   ph_isEM = 0;
   ph_nVertices = 0;
   ph_SF_eff = 0;
   ph_SF_effUP = 0;
   ph_SF_effDO = 0;
   ph_SF_iso = 0;
   ph_SF_lowisoUP = 0;
   ph_SF_lowisoDO = 0;
   ph_SF_trkisoUP = 0;
   ph_SF_trkisoDO = 0;
   ph_drleadjet = 0;
   ph_dralljet = 0;
   ph_drsubljet = 0;
   ph_drlept = 0;
   ph_mgammalept = 0;
   ph_mgammaleptlept = 0;
   ph_truthType = 0;
   ph_truthOrigin = 0;
   ph_mc_Type = 0;
   ph_mc_Origin = 0;
   ph_faketype = 0;
   ph_truthAncestor = 0;
   ph_mc_pid = 0;
   ph_mc_barcode = 0;
   ph_mc_pt = 0;
   ph_mc_eta = 0;
   ph_mc_phi = 0;
   ph_mcel_dr = 0;
   ph_mcel_pt = 0;
   ph_mcel_eta = 0;
   ph_mcel_phi = 0;
   mcph_pt = 0;
   mcph_eta = 0;
   mcph_phi = 0;
   mcph_ancestor = 0;
   el_truthAncestor = 0;
   el_isoGradient = 0;
   el_mc_pid = 0;
   el_mc_charge = 0;
   el_mc_pt = 0;
   el_mc_eta = 0;
   el_mc_phi = 0;
   mu_mc_charge = 0;
   jet_mcdr_tW1 = 0;
   jet_mcdr_tW2 = 0;
   jet_mcdr_tB = 0;
   jet_mcdr_tbW1 = 0;
   jet_mcdr_tbW2 = 0;
   jet_mcdr_tbB = 0;
   jet_nGhosts_bHadron = 0;
   jet_mv1eff = 0;
   lepton_type = 0;
   ph_HFT_MVA = 0;
   event_ELD_MVA = 0;
   // Set branch addresses and branch pointers
   if (!tree) return;
   fChain = tree;
   fCurrent = -1;
   fChain->SetMakeClass(1);

    fChain->SetBranchAddress("weight_mc", &weight_mc, &b_weight_mc);
    if (!m_upgrade) {
   	fChain->SetBranchAddress("weight_pileup", &weight_pileup, &b_weight_pileup);
   	fChain->SetBranchAddress("weight_leptonSF", &weight_leptonSF, &b_weight_leptonSF);
   	fChain->SetBranchAddress("weight_photonSF", &weight_photonSF, &b_weight_photonSF);
   	fChain->SetBranchAddress("weight_bTagSF_77", &weight_bTagSF_77, &b_weight_bTagSF_77);
   	fChain->SetBranchAddress("weight_bTagSF_85", &weight_bTagSF_85, &b_weight_bTagSF_85);
   	fChain->SetBranchAddress("weight_bTagSF_70", &weight_bTagSF_70, &b_weight_bTagSF_70);
   	fChain->SetBranchAddress("weight_bTagSF_Continuous", &weight_bTagSF_Continuous, &b_weight_bTagSF_Continuous);
   	fChain->SetBranchAddress("weight_jvt", &weight_jvt, &b_weight_jvt);
   	fChain->SetBranchAddress("weight_pileup_UP", &weight_pileup_UP, &b_weight_pileup_UP);
   	fChain->SetBranchAddress("weight_pileup_DOWN", &weight_pileup_DOWN, &b_weight_pileup_DOWN);
   	fChain->SetBranchAddress("weight_leptonSF_EL_SF_Trigger_UP", &weight_leptonSF_EL_SF_Trigger_UP, &b_weight_leptonSF_EL_SF_Trigger_UP);
   	fChain->SetBranchAddress("weight_leptonSF_EL_SF_Trigger_DOWN", &weight_leptonSF_EL_SF_Trigger_DOWN, &b_weight_leptonSF_EL_SF_Trigger_DOWN);
   	fChain->SetBranchAddress("weight_leptonSF_EL_SF_Reco_UP", &weight_leptonSF_EL_SF_Reco_UP, &b_weight_leptonSF_EL_SF_Reco_UP);
   	fChain->SetBranchAddress("weight_leptonSF_EL_SF_Reco_DOWN", &weight_leptonSF_EL_SF_Reco_DOWN, &b_weight_leptonSF_EL_SF_Reco_DOWN);
   	fChain->SetBranchAddress("weight_leptonSF_EL_SF_ID_UP", &weight_leptonSF_EL_SF_ID_UP, &b_weight_leptonSF_EL_SF_ID_UP);
   	fChain->SetBranchAddress("weight_leptonSF_EL_SF_ID_DOWN", &weight_leptonSF_EL_SF_ID_DOWN, &b_weight_leptonSF_EL_SF_ID_DOWN);
   	fChain->SetBranchAddress("weight_leptonSF_EL_SF_Isol_UP", &weight_leptonSF_EL_SF_Isol_UP, &b_weight_leptonSF_EL_SF_Isol_UP);
   	fChain->SetBranchAddress("weight_leptonSF_EL_SF_Isol_DOWN", &weight_leptonSF_EL_SF_Isol_DOWN, &b_weight_leptonSF_EL_SF_Isol_DOWN);
   	fChain->SetBranchAddress("weight_leptonSF_MU_SF_Trigger_STAT_UP", &weight_leptonSF_MU_SF_Trigger_STAT_UP, &b_weight_leptonSF_MU_SF_Trigger_STAT_UP);
   	fChain->SetBranchAddress("weight_leptonSF_MU_SF_Trigger_STAT_DOWN", &weight_leptonSF_MU_SF_Trigger_STAT_DOWN, &b_weight_leptonSF_MU_SF_Trigger_STAT_DOWN);
   	fChain->SetBranchAddress("weight_leptonSF_MU_SF_Trigger_SYST_UP", &weight_leptonSF_MU_SF_Trigger_SYST_UP, &b_weight_leptonSF_MU_SF_Trigger_SYST_UP);
   	fChain->SetBranchAddress("weight_leptonSF_MU_SF_Trigger_SYST_DOWN", &weight_leptonSF_MU_SF_Trigger_SYST_DOWN, &b_weight_leptonSF_MU_SF_Trigger_SYST_DOWN);
   	fChain->SetBranchAddress("weight_leptonSF_MU_SF_ID_STAT_UP", &weight_leptonSF_MU_SF_ID_STAT_UP, &b_weight_leptonSF_MU_SF_ID_STAT_UP);
   	fChain->SetBranchAddress("weight_leptonSF_MU_SF_ID_STAT_DOWN", &weight_leptonSF_MU_SF_ID_STAT_DOWN, &b_weight_leptonSF_MU_SF_ID_STAT_DOWN);
   	fChain->SetBranchAddress("weight_leptonSF_MU_SF_ID_SYST_UP", &weight_leptonSF_MU_SF_ID_SYST_UP, &b_weight_leptonSF_MU_SF_ID_SYST_UP);
   	fChain->SetBranchAddress("weight_leptonSF_MU_SF_ID_SYST_DOWN", &weight_leptonSF_MU_SF_ID_SYST_DOWN, &b_weight_leptonSF_MU_SF_ID_SYST_DOWN);
   	fChain->SetBranchAddress("weight_leptonSF_MU_SF_ID_STAT_LOWPT_UP", &weight_leptonSF_MU_SF_ID_STAT_LOWPT_UP, &b_weight_leptonSF_MU_SF_ID_STAT_LOWPT_UP);
   	fChain->SetBranchAddress("weight_leptonSF_MU_SF_ID_STAT_LOWPT_DOWN", &weight_leptonSF_MU_SF_ID_STAT_LOWPT_DOWN, &b_weight_leptonSF_MU_SF_ID_STAT_LOWPT_DOWN);
   	fChain->SetBranchAddress("weight_leptonSF_MU_SF_ID_SYST_LOWPT_UP", &weight_leptonSF_MU_SF_ID_SYST_LOWPT_UP, &b_weight_leptonSF_MU_SF_ID_SYST_LOWPT_UP);
   	fChain->SetBranchAddress("weight_leptonSF_MU_SF_ID_SYST_LOWPT_DOWN", &weight_leptonSF_MU_SF_ID_SYST_LOWPT_DOWN, &b_weight_leptonSF_MU_SF_ID_SYST_LOWPT_DOWN);
   	fChain->SetBranchAddress("weight_leptonSF_MU_SF_Isol_STAT_UP", &weight_leptonSF_MU_SF_Isol_STAT_UP, &b_weight_leptonSF_MU_SF_Isol_STAT_UP);
   	fChain->SetBranchAddress("weight_leptonSF_MU_SF_Isol_STAT_DOWN", &weight_leptonSF_MU_SF_Isol_STAT_DOWN, &b_weight_leptonSF_MU_SF_Isol_STAT_DOWN);
   	fChain->SetBranchAddress("weight_leptonSF_MU_SF_Isol_SYST_UP", &weight_leptonSF_MU_SF_Isol_SYST_UP, &b_weight_leptonSF_MU_SF_Isol_SYST_UP);
   	fChain->SetBranchAddress("weight_leptonSF_MU_SF_Isol_SYST_DOWN", &weight_leptonSF_MU_SF_Isol_SYST_DOWN, &b_weight_leptonSF_MU_SF_Isol_SYST_DOWN);
   	fChain->SetBranchAddress("weight_leptonSF_MU_SF_TTVA_STAT_UP", &weight_leptonSF_MU_SF_TTVA_STAT_UP, &b_weight_leptonSF_MU_SF_TTVA_STAT_UP);
   	fChain->SetBranchAddress("weight_leptonSF_MU_SF_TTVA_STAT_DOWN", &weight_leptonSF_MU_SF_TTVA_STAT_DOWN, &b_weight_leptonSF_MU_SF_TTVA_STAT_DOWN);
   	fChain->SetBranchAddress("weight_leptonSF_MU_SF_TTVA_SYST_UP", &weight_leptonSF_MU_SF_TTVA_SYST_UP, &b_weight_leptonSF_MU_SF_TTVA_SYST_UP);
   	fChain->SetBranchAddress("weight_leptonSF_MU_SF_TTVA_SYST_DOWN", &weight_leptonSF_MU_SF_TTVA_SYST_DOWN, &b_weight_leptonSF_MU_SF_TTVA_SYST_DOWN);
   	fChain->SetBranchAddress("weight_indiv_SF_EL_Trigger", &weight_indiv_SF_EL_Trigger, &b_weight_indiv_SF_EL_Trigger);
   	fChain->SetBranchAddress("weight_indiv_SF_EL_Trigger_UP", &weight_indiv_SF_EL_Trigger_UP, &b_weight_indiv_SF_EL_Trigger_UP);
   	fChain->SetBranchAddress("weight_indiv_SF_EL_Trigger_DOWN", &weight_indiv_SF_EL_Trigger_DOWN, &b_weight_indiv_SF_EL_Trigger_DOWN);
   	fChain->SetBranchAddress("weight_indiv_SF_EL_Reco", &weight_indiv_SF_EL_Reco, &b_weight_indiv_SF_EL_Reco);
   	fChain->SetBranchAddress("weight_indiv_SF_EL_Reco_UP", &weight_indiv_SF_EL_Reco_UP, &b_weight_indiv_SF_EL_Reco_UP);
   	fChain->SetBranchAddress("weight_indiv_SF_EL_Reco_DOWN", &weight_indiv_SF_EL_Reco_DOWN, &b_weight_indiv_SF_EL_Reco_DOWN);
   	fChain->SetBranchAddress("weight_indiv_SF_EL_ID", &weight_indiv_SF_EL_ID, &b_weight_indiv_SF_EL_ID);
   	fChain->SetBranchAddress("weight_indiv_SF_EL_ID_UP", &weight_indiv_SF_EL_ID_UP, &b_weight_indiv_SF_EL_ID_UP);
   	fChain->SetBranchAddress("weight_indiv_SF_EL_ID_DOWN", &weight_indiv_SF_EL_ID_DOWN, &b_weight_indiv_SF_EL_ID_DOWN);
   	fChain->SetBranchAddress("weight_indiv_SF_EL_Isol", &weight_indiv_SF_EL_Isol, &b_weight_indiv_SF_EL_Isol);
   	fChain->SetBranchAddress("weight_indiv_SF_EL_Isol_UP", &weight_indiv_SF_EL_Isol_UP, &b_weight_indiv_SF_EL_Isol_UP);
   	fChain->SetBranchAddress("weight_indiv_SF_EL_Isol_DOWN", &weight_indiv_SF_EL_Isol_DOWN, &b_weight_indiv_SF_EL_Isol_DOWN);
   	fChain->SetBranchAddress("weight_indiv_SF_EL_ChargeID", &weight_indiv_SF_EL_ChargeID, &b_weight_indiv_SF_EL_ChargeID);
   	fChain->SetBranchAddress("weight_indiv_SF_EL_ChargeID_UP", &weight_indiv_SF_EL_ChargeID_UP, &b_weight_indiv_SF_EL_ChargeID_UP);
   	fChain->SetBranchAddress("weight_indiv_SF_EL_ChargeID_DOWN", &weight_indiv_SF_EL_ChargeID_DOWN, &b_weight_indiv_SF_EL_ChargeID_DOWN);
   	fChain->SetBranchAddress("weight_indiv_SF_EL_ChargeMisID", &weight_indiv_SF_EL_ChargeMisID, &b_weight_indiv_SF_EL_ChargeMisID);
   	fChain->SetBranchAddress("weight_indiv_SF_EL_ChargeMisID_STAT_UP", &weight_indiv_SF_EL_ChargeMisID_STAT_UP, &b_weight_indiv_SF_EL_ChargeMisID_STAT_UP);
   	fChain->SetBranchAddress("weight_indiv_SF_EL_ChargeMisID_STAT_DOWN", &weight_indiv_SF_EL_ChargeMisID_STAT_DOWN, &b_weight_indiv_SF_EL_ChargeMisID_STAT_DOWN);
   	fChain->SetBranchAddress("weight_indiv_SF_EL_ChargeMisID_SYST_UP", &weight_indiv_SF_EL_ChargeMisID_SYST_UP, &b_weight_indiv_SF_EL_ChargeMisID_SYST_UP);
   	fChain->SetBranchAddress("weight_indiv_SF_EL_ChargeMisID_SYST_DOWN", &weight_indiv_SF_EL_ChargeMisID_SYST_DOWN, &b_weight_indiv_SF_EL_ChargeMisID_SYST_DOWN);
   	fChain->SetBranchAddress("weight_indiv_SF_MU_Trigger", &weight_indiv_SF_MU_Trigger, &b_weight_indiv_SF_MU_Trigger);
   	fChain->SetBranchAddress("weight_indiv_SF_MU_Trigger_STAT_UP", &weight_indiv_SF_MU_Trigger_STAT_UP, &b_weight_indiv_SF_MU_Trigger_STAT_UP);
   	fChain->SetBranchAddress("weight_indiv_SF_MU_Trigger_STAT_DOWN", &weight_indiv_SF_MU_Trigger_STAT_DOWN, &b_weight_indiv_SF_MU_Trigger_STAT_DOWN);
   	fChain->SetBranchAddress("weight_indiv_SF_MU_Trigger_SYST_UP", &weight_indiv_SF_MU_Trigger_SYST_UP, &b_weight_indiv_SF_MU_Trigger_SYST_UP);
   	fChain->SetBranchAddress("weight_indiv_SF_MU_Trigger_SYST_DOWN", &weight_indiv_SF_MU_Trigger_SYST_DOWN, &b_weight_indiv_SF_MU_Trigger_SYST_DOWN);
   	fChain->SetBranchAddress("weight_indiv_SF_MU_ID", &weight_indiv_SF_MU_ID, &b_weight_indiv_SF_MU_ID);
   	fChain->SetBranchAddress("weight_indiv_SF_MU_ID_STAT_UP", &weight_indiv_SF_MU_ID_STAT_UP, &b_weight_indiv_SF_MU_ID_STAT_UP);
   	fChain->SetBranchAddress("weight_indiv_SF_MU_ID_STAT_DOWN", &weight_indiv_SF_MU_ID_STAT_DOWN, &b_weight_indiv_SF_MU_ID_STAT_DOWN);
   	fChain->SetBranchAddress("weight_indiv_SF_MU_ID_SYST_UP", &weight_indiv_SF_MU_ID_SYST_UP, &b_weight_indiv_SF_MU_ID_SYST_UP);
   	fChain->SetBranchAddress("weight_indiv_SF_MU_ID_SYST_DOWN", &weight_indiv_SF_MU_ID_SYST_DOWN, &b_weight_indiv_SF_MU_ID_SYST_DOWN);
   	fChain->SetBranchAddress("weight_indiv_SF_MU_ID_STAT_LOWPT_UP", &weight_indiv_SF_MU_ID_STAT_LOWPT_UP, &b_weight_indiv_SF_MU_ID_STAT_LOWPT_UP);
   	fChain->SetBranchAddress("weight_indiv_SF_MU_ID_STAT_LOWPT_DOWN", &weight_indiv_SF_MU_ID_STAT_LOWPT_DOWN, &b_weight_indiv_SF_MU_ID_STAT_LOWPT_DOWN);
   	fChain->SetBranchAddress("weight_indiv_SF_MU_ID_SYST_LOWPT_UP", &weight_indiv_SF_MU_ID_SYST_LOWPT_UP, &b_weight_indiv_SF_MU_ID_SYST_LOWPT_UP);
   	fChain->SetBranchAddress("weight_indiv_SF_MU_ID_SYST_LOWPT_DOWN", &weight_indiv_SF_MU_ID_SYST_LOWPT_DOWN, &b_weight_indiv_SF_MU_ID_SYST_LOWPT_DOWN);
   	fChain->SetBranchAddress("weight_indiv_SF_MU_Isol", &weight_indiv_SF_MU_Isol, &b_weight_indiv_SF_MU_Isol);
   	fChain->SetBranchAddress("weight_indiv_SF_MU_Isol_STAT_UP", &weight_indiv_SF_MU_Isol_STAT_UP, &b_weight_indiv_SF_MU_Isol_STAT_UP);
   	fChain->SetBranchAddress("weight_indiv_SF_MU_Isol_STAT_DOWN", &weight_indiv_SF_MU_Isol_STAT_DOWN, &b_weight_indiv_SF_MU_Isol_STAT_DOWN);
   	fChain->SetBranchAddress("weight_indiv_SF_MU_Isol_SYST_UP", &weight_indiv_SF_MU_Isol_SYST_UP, &b_weight_indiv_SF_MU_Isol_SYST_UP);
   	fChain->SetBranchAddress("weight_indiv_SF_MU_Isol_SYST_DOWN", &weight_indiv_SF_MU_Isol_SYST_DOWN, &b_weight_indiv_SF_MU_Isol_SYST_DOWN);
   	fChain->SetBranchAddress("weight_indiv_SF_MU_TTVA", &weight_indiv_SF_MU_TTVA, &b_weight_indiv_SF_MU_TTVA);
   	fChain->SetBranchAddress("weight_indiv_SF_MU_TTVA_STAT_UP", &weight_indiv_SF_MU_TTVA_STAT_UP, &b_weight_indiv_SF_MU_TTVA_STAT_UP);
   	fChain->SetBranchAddress("weight_indiv_SF_MU_TTVA_STAT_DOWN", &weight_indiv_SF_MU_TTVA_STAT_DOWN, &b_weight_indiv_SF_MU_TTVA_STAT_DOWN);
   	fChain->SetBranchAddress("weight_indiv_SF_MU_TTVA_SYST_UP", &weight_indiv_SF_MU_TTVA_SYST_UP, &b_weight_indiv_SF_MU_TTVA_SYST_UP);
   	fChain->SetBranchAddress("weight_indiv_SF_MU_TTVA_SYST_DOWN", &weight_indiv_SF_MU_TTVA_SYST_DOWN, &b_weight_indiv_SF_MU_TTVA_SYST_DOWN);
   	fChain->SetBranchAddress("weight_photonSF_ID_UP", &weight_photonSF_ID_UP, &b_weight_photonSF_ID_UP);
   	fChain->SetBranchAddress("weight_photonSF_ID_DOWN", &weight_photonSF_ID_DOWN, &b_weight_photonSF_ID_DOWN);
   	fChain->SetBranchAddress("weight_photonSF_effIso", &weight_photonSF_effIso, &b_weight_photonSF_effIso);
   	fChain->SetBranchAddress("weight_photonSF_effLowPtIso_UP", &weight_photonSF_effLowPtIso_UP, &b_weight_photonSF_effLowPtIso_UP);
   	fChain->SetBranchAddress("weight_photonSF_effLowPtIso_DOWN", &weight_photonSF_effLowPtIso_DOWN, &b_weight_photonSF_effLowPtIso_DOWN);
   	fChain->SetBranchAddress("weight_photonSF_effTrkIso_UP", &weight_photonSF_effTrkIso_UP, &b_weight_photonSF_effTrkIso_UP);
   	fChain->SetBranchAddress("weight_photonSF_effTrkIso_DOWN", &weight_photonSF_effTrkIso_DOWN, &b_weight_photonSF_effTrkIso_DOWN);
   	fChain->SetBranchAddress("weight_jvt_UP", &weight_jvt_UP, &b_weight_jvt_UP);
   	fChain->SetBranchAddress("weight_jvt_DOWN", &weight_jvt_DOWN, &b_weight_jvt_DOWN);
   	fChain->SetBranchAddress("weight_bTagSF_77_eigenvars_B_up", &weight_bTagSF_77_eigenvars_B_up, &b_weight_bTagSF_77_eigenvars_B_up);
   	fChain->SetBranchAddress("mc_generator_weights", &mc_generator_weights, &b_mc_generator_weights);
   	fChain->SetBranchAddress("weight_bTagSF_77_eigenvars_C_up", &weight_bTagSF_77_eigenvars_C_up, &b_weight_bTagSF_77_eigenvars_C_up);
   	fChain->SetBranchAddress("weight_bTagSF_77_eigenvars_Light_up", &weight_bTagSF_77_eigenvars_Light_up, &b_weight_bTagSF_77_eigenvars_Light_up);
   	fChain->SetBranchAddress("weight_bTagSF_77_eigenvars_B_down", &weight_bTagSF_77_eigenvars_B_down, &b_weight_bTagSF_77_eigenvars_B_down);
   	fChain->SetBranchAddress("weight_bTagSF_77_eigenvars_C_down", &weight_bTagSF_77_eigenvars_C_down, &b_weight_bTagSF_77_eigenvars_C_down);
   	fChain->SetBranchAddress("weight_bTagSF_77_eigenvars_Light_down", &weight_bTagSF_77_eigenvars_Light_down, &b_weight_bTagSF_77_eigenvars_Light_down);
   	fChain->SetBranchAddress("weight_bTagSF_77_extrapolation_up", &weight_bTagSF_77_extrapolation_up, &b_weight_bTagSF_77_extrapolation_up);
   	fChain->SetBranchAddress("weight_bTagSF_77_extrapolation_down", &weight_bTagSF_77_extrapolation_down, &b_weight_bTagSF_77_extrapolation_down);
   	fChain->SetBranchAddress("weight_bTagSF_77_extrapolation_from_charm_up", &weight_bTagSF_77_extrapolation_from_charm_up, &b_weight_bTagSF_77_extrapolation_from_charm_up);
   	fChain->SetBranchAddress("weight_bTagSF_77_extrapolation_from_charm_down", &weight_bTagSF_77_extrapolation_from_charm_down, &b_weight_bTagSF_77_extrapolation_from_charm_down);
   	fChain->SetBranchAddress("weight_bTagSF_85_eigenvars_B_up", &weight_bTagSF_85_eigenvars_B_up, &b_weight_bTagSF_85_eigenvars_B_up);
   	fChain->SetBranchAddress("weight_bTagSF_85_eigenvars_C_up", &weight_bTagSF_85_eigenvars_C_up, &b_weight_bTagSF_85_eigenvars_C_up);
   	fChain->SetBranchAddress("weight_bTagSF_85_eigenvars_Light_up", &weight_bTagSF_85_eigenvars_Light_up, &b_weight_bTagSF_85_eigenvars_Light_up);
   	fChain->SetBranchAddress("weight_bTagSF_85_eigenvars_B_down", &weight_bTagSF_85_eigenvars_B_down, &b_weight_bTagSF_85_eigenvars_B_down);
   	fChain->SetBranchAddress("weight_bTagSF_85_eigenvars_C_down", &weight_bTagSF_85_eigenvars_C_down, &b_weight_bTagSF_85_eigenvars_C_down);
   	fChain->SetBranchAddress("weight_bTagSF_85_eigenvars_Light_down", &weight_bTagSF_85_eigenvars_Light_down, &b_weight_bTagSF_85_eigenvars_Light_down);
   	fChain->SetBranchAddress("weight_bTagSF_85_extrapolation_up", &weight_bTagSF_85_extrapolation_up, &b_weight_bTagSF_85_extrapolation_up);
   	fChain->SetBranchAddress("weight_bTagSF_85_extrapolation_down", &weight_bTagSF_85_extrapolation_down, &b_weight_bTagSF_85_extrapolation_down);
   	fChain->SetBranchAddress("weight_bTagSF_85_extrapolation_from_charm_up", &weight_bTagSF_85_extrapolation_from_charm_up, &b_weight_bTagSF_85_extrapolation_from_charm_up);
   	fChain->SetBranchAddress("weight_bTagSF_85_extrapolation_from_charm_down", &weight_bTagSF_85_extrapolation_from_charm_down, &b_weight_bTagSF_85_extrapolation_from_charm_down);
   	fChain->SetBranchAddress("weight_bTagSF_70_eigenvars_B_up", &weight_bTagSF_70_eigenvars_B_up, &b_weight_bTagSF_70_eigenvars_B_up);
   	fChain->SetBranchAddress("weight_bTagSF_70_eigenvars_C_up", &weight_bTagSF_70_eigenvars_C_up, &b_weight_bTagSF_70_eigenvars_C_up);
   	fChain->SetBranchAddress("weight_bTagSF_70_eigenvars_Light_up", &weight_bTagSF_70_eigenvars_Light_up, &b_weight_bTagSF_70_eigenvars_Light_up);
   	fChain->SetBranchAddress("weight_bTagSF_70_eigenvars_B_down", &weight_bTagSF_70_eigenvars_B_down, &b_weight_bTagSF_70_eigenvars_B_down);
   	fChain->SetBranchAddress("weight_bTagSF_70_eigenvars_C_down", &weight_bTagSF_70_eigenvars_C_down, &b_weight_bTagSF_70_eigenvars_C_down);
   	fChain->SetBranchAddress("weight_bTagSF_70_eigenvars_Light_down", &weight_bTagSF_70_eigenvars_Light_down, &b_weight_bTagSF_70_eigenvars_Light_down);
   	fChain->SetBranchAddress("weight_bTagSF_70_extrapolation_up", &weight_bTagSF_70_extrapolation_up, &b_weight_bTagSF_70_extrapolation_up);
   	fChain->SetBranchAddress("weight_bTagSF_70_extrapolation_down", &weight_bTagSF_70_extrapolation_down, &b_weight_bTagSF_70_extrapolation_down);
   	fChain->SetBranchAddress("weight_bTagSF_70_extrapolation_from_charm_up", &weight_bTagSF_70_extrapolation_from_charm_up, &b_weight_bTagSF_70_extrapolation_from_charm_up);
   	fChain->SetBranchAddress("weight_bTagSF_70_extrapolation_from_charm_down", &weight_bTagSF_70_extrapolation_from_charm_down, &b_weight_bTagSF_70_extrapolation_from_charm_down);
   	fChain->SetBranchAddress("weight_bTagSF_Continuous_eigenvars_B_up", &weight_bTagSF_Continuous_eigenvars_B_up, &b_weight_bTagSF_Continuous_eigenvars_B_up);
   	fChain->SetBranchAddress("weight_bTagSF_Continuous_eigenvars_C_up", &weight_bTagSF_Continuous_eigenvars_C_up, &b_weight_bTagSF_Continuous_eigenvars_C_up);
   	fChain->SetBranchAddress("weight_bTagSF_Continuous_eigenvars_Light_up", &weight_bTagSF_Continuous_eigenvars_Light_up, &b_weight_bTagSF_Continuous_eigenvars_Light_up);
   	fChain->SetBranchAddress("weight_bTagSF_Continuous_eigenvars_B_down", &weight_bTagSF_Continuous_eigenvars_B_down, &b_weight_bTagSF_Continuous_eigenvars_B_down);
   	fChain->SetBranchAddress("weight_bTagSF_Continuous_eigenvars_C_down", &weight_bTagSF_Continuous_eigenvars_C_down, &b_weight_bTagSF_Continuous_eigenvars_C_down);
   	fChain->SetBranchAddress("weight_bTagSF_Continuous_eigenvars_Light_down", &weight_bTagSF_Continuous_eigenvars_Light_down, &b_weight_bTagSF_Continuous_eigenvars_Light_down);
   	fChain->SetBranchAddress("weight_bTagSF_Continuous_extrapolation_from_charm_up", &weight_bTagSF_Continuous_extrapolation_from_charm_up, &b_weight_bTagSF_Continuous_extrapolation_from_charm_up);
   	fChain->SetBranchAddress("weight_bTagSF_Continuous_extrapolation_from_charm_down", &weight_bTagSF_Continuous_extrapolation_from_charm_down, &b_weight_bTagSF_Continuous_extrapolation_from_charm_down);
    }
   fChain->SetBranchAddress("eventNumber", &eventNumber, &b_eventNumber);
   fChain->SetBranchAddress("runNumber", &runNumber, &b_runNumber);
   if (!m_upgrade) {
    fChain->SetBranchAddress("randomRunNumber", &randomRunNumber, &b_randomRunNumber);
   }
   fChain->SetBranchAddress("mcChannelNumber", &mcChannelNumber, &b_mcChannelNumber);
   fChain->SetBranchAddress("mu", &mu, &b_mu);
   if (!m_upgrade) {
    fChain->SetBranchAddress("backgroundFlags", &backgroundFlags, &b_backgroundFlags);
    fChain->SetBranchAddress("hasBadMuon", &hasBadMuon, &b_hasBadMuon);
   }
   fChain->SetBranchAddress("el_pt", &el_pt, &b_el_pt);
   fChain->SetBranchAddress("el_eta", &el_eta, &b_el_eta);
   if (!m_upgrade) {
    fChain->SetBranchAddress("el_cl_eta", &el_cl_eta, &b_el_cl_eta);
   }
   fChain->SetBranchAddress("el_phi", &el_phi, &b_el_phi);
   fChain->SetBranchAddress("el_e", &el_e, &b_el_e);
   fChain->SetBranchAddress("el_charge", &el_charge, &b_el_charge);
   if (!m_upgrade) {
    fChain->SetBranchAddress("el_topoetcone20", &el_topoetcone20, &b_el_topoetcone20);
    fChain->SetBranchAddress("el_ptvarcone20", &el_ptvarcone20, &b_el_ptvarcone20);
    fChain->SetBranchAddress("el_CF", &el_CF, &b_el_CF);
    fChain->SetBranchAddress("el_d0sig", &el_d0sig, &b_el_d0sig);
    fChain->SetBranchAddress("el_delta_z0_sintheta", &el_delta_z0_sintheta, &b_el_delta_z0_sintheta);
    fChain->SetBranchAddress("el_true_typebkg", &el_true_typebkg, &b_el_true_typebkg);
    fChain->SetBranchAddress("el_true_originbkg", &el_true_originbkg, &b_el_true_originbkg);
   }
   fChain->SetBranchAddress("el_true_type", &el_true_type, &b_el_true_type);
   fChain->SetBranchAddress("el_true_origin", &el_true_origin, &b_el_true_origin);
   fChain->SetBranchAddress("mu_pt", &mu_pt, &b_mu_pt);
   fChain->SetBranchAddress("mu_eta", &mu_eta, &b_mu_eta);
   fChain->SetBranchAddress("mu_phi", &mu_phi, &b_mu_phi);
   fChain->SetBranchAddress("mu_e", &mu_e, &b_mu_e);
   fChain->SetBranchAddress("mu_charge", &mu_charge, &b_mu_charge);
    fChain->SetBranchAddress("mu_true_type", &mu_true_type, &b_mu_true_type);
    fChain->SetBranchAddress("mu_true_origin", &mu_true_origin, &b_mu_true_origin);
    if (m_upgrade) {
	fChain->SetBranchAddress("mu_true_isPrompt", &mu_true_isPrompt, &b_mu_true_isPrompt);
	fChain->SetBranchAddress("el_faketype", &el_faketype, &b_el_faketype);
  }
   if (!m_upgrade) {
    fChain->SetBranchAddress("mu_topoetcone20", &mu_topoetcone20, &b_mu_topoetcone20);
    fChain->SetBranchAddress("mu_ptvarcone30", &mu_ptvarcone30, &b_mu_ptvarcone30);
    fChain->SetBranchAddress("mu_d0sig", &mu_d0sig, &b_mu_d0sig);
    fChain->SetBranchAddress("mu_delta_z0_sintheta", &mu_delta_z0_sintheta, &b_mu_delta_z0_sintheta);
   }
   fChain->SetBranchAddress("ph_pt", &ph_pt, &b_ph_pt);
   fChain->SetBranchAddress("ph_eta", &ph_eta, &b_ph_eta);
   fChain->SetBranchAddress("ph_phi", &ph_phi, &b_ph_phi);
   fChain->SetBranchAddress("ph_e", &ph_e, &b_ph_e);
   if (!m_upgrade) {
    fChain->SetBranchAddress("ph_iso", &ph_iso, &b_ph_iso);
   }
   fChain->SetBranchAddress("jet_pt", &jet_pt, &b_jet_pt);
   fChain->SetBranchAddress("jet_eta", &jet_eta, &b_jet_eta);
   fChain->SetBranchAddress("jet_phi", &jet_phi, &b_jet_phi);
   fChain->SetBranchAddress("jet_e", &jet_e, &b_jet_e);
   if (!m_upgrade) {
    fChain->SetBranchAddress("jet_mv2c00", &jet_mv2c00, &b_jet_mv2c00);
    fChain->SetBranchAddress("jet_mv2c10", &jet_mv2c10, &b_jet_mv2c10);
    fChain->SetBranchAddress("jet_mv2c20", &jet_mv2c20, &b_jet_mv2c20);
    fChain->SetBranchAddress("jet_ip3dsv1", &jet_ip3dsv1, &b_jet_ip3dsv1);
    fChain->SetBranchAddress("jet_jvt", &jet_jvt, &b_jet_jvt);
    fChain->SetBranchAddress("jet_passfjvt", &jet_passfjvt, &b_jet_passfjvt);
    fChain->SetBranchAddress("jet_truthflav", &jet_truthflav, &b_jet_truthflav);
    fChain->SetBranchAddress("jet_truthPartonLabel", &jet_truthPartonLabel, &b_jet_truthPartonLabel);
    fChain->SetBranchAddress("jet_isTrueHS", &jet_isTrueHS, &b_jet_isTrueHS);
    fChain->SetBranchAddress("jet_isbtagged_77", &jet_isbtagged_77, &b_jet_isbtagged_77);
    fChain->SetBranchAddress("jet_isbtagged_85", &jet_isbtagged_85, &b_jet_isbtagged_85);
    fChain->SetBranchAddress("jet_isbtagged_70", &jet_isbtagged_70, &b_jet_isbtagged_70);
    fChain->SetBranchAddress("jet_tagWeightBin", &jet_tagWeightBin, &b_jet_tagWeightBin);
   }
   fChain->SetBranchAddress("met_met", &met_met, &b_met_met);
   fChain->SetBranchAddress("met_phi", &met_phi, &b_met_phi);
   fChain->SetBranchAddress("ejets_gamma_basic", &ejets_gamma_basic, &b_ejets_gamma_basic);
   fChain->SetBranchAddress("mujets_gamma_basic", &mujets_gamma_basic, &b_mujets_gamma_basic);
   fChain->SetBranchAddress("ee_gamma_basic", &ee_gamma_basic, &b_ee_gamma_basic);
   fChain->SetBranchAddress("emu_gamma_basic", &emu_gamma_basic, &b_emu_gamma_basic);
   fChain->SetBranchAddress("mumu_gamma_basic", &mumu_gamma_basic, &b_mumu_gamma_basic);
   if (!m_upgrade) {
    fChain->SetBranchAddress("ejets_2015", &ejets_2015, &b_ejets_2015);
    fChain->SetBranchAddress("ejets_2015_pl", &ejets_2015_pl, &b_ejets_2015_pl);
    fChain->SetBranchAddress("mujets_2015", &mujets_2015, &b_mujets_2015);
    fChain->SetBranchAddress("mujets_2015_pl", &mujets_2015_pl, &b_mujets_2015_pl);
    fChain->SetBranchAddress("emu_2015", &emu_2015, &b_emu_2015);
    fChain->SetBranchAddress("emu_2015_pl", &emu_2015_pl, &b_emu_2015_pl);
    fChain->SetBranchAddress("ee_2015", &ee_2015, &b_ee_2015);
    fChain->SetBranchAddress("ee_2015_pl", &ee_2015_pl, &b_ee_2015_pl);
    fChain->SetBranchAddress("mumu_2015", &mumu_2015, &b_mumu_2015);
    fChain->SetBranchAddress("mumu_2015_pl", &mumu_2015_pl, &b_mumu_2015_pl);
    fChain->SetBranchAddress("ejets_2016", &ejets_2016, &b_ejets_2016);
    fChain->SetBranchAddress("ejets_2016_pl", &ejets_2016_pl, &b_ejets_2016_pl);
    fChain->SetBranchAddress("mujets_2016", &mujets_2016, &b_mujets_2016);
    fChain->SetBranchAddress("mujets_2016_pl", &mujets_2016_pl, &b_mujets_2016_pl);
    fChain->SetBranchAddress("emu_2016", &emu_2016, &b_emu_2016);
    fChain->SetBranchAddress("emu_2016_pl", &emu_2016_pl, &b_emu_2016_pl);
    fChain->SetBranchAddress("ee_2016", &ee_2016, &b_ee_2016);
    fChain->SetBranchAddress("ee_2016_pl", &ee_2016_pl, &b_ee_2016_pl);
    fChain->SetBranchAddress("mumu_2016", &mumu_2016, &b_mumu_2016);
    fChain->SetBranchAddress("mumu_2016_pl", &mumu_2016_pl, &b_mumu_2016_pl);
    fChain->SetBranchAddress("HLT_e60_lhmedium_nod0", &HLT_e60_lhmedium_nod0, &b_HLT_e60_lhmedium_nod0);
    fChain->SetBranchAddress("HLT_mu26_ivarmedium", &HLT_mu26_ivarmedium, &b_HLT_mu26_ivarmedium);
    fChain->SetBranchAddress("HLT_e26_lhtight_nod0_ivarloose", &HLT_e26_lhtight_nod0_ivarloose, &b_HLT_e26_lhtight_nod0_ivarloose);
    fChain->SetBranchAddress("HLT_e140_lhloose_nod0", &HLT_e140_lhloose_nod0, &b_HLT_e140_lhloose_nod0);
    fChain->SetBranchAddress("HLT_mu20_iloose_L1MU15", &HLT_mu20_iloose_L1MU15, &b_HLT_mu20_iloose_L1MU15);
    fChain->SetBranchAddress("HLT_mu50", &HLT_mu50, &b_HLT_mu50);
    fChain->SetBranchAddress("HLT_e60_lhmedium", &HLT_e60_lhmedium, &b_HLT_e60_lhmedium);
    fChain->SetBranchAddress("HLT_e24_lhmedium_L1EM20VH", &HLT_e24_lhmedium_L1EM20VH, &b_HLT_e24_lhmedium_L1EM20VH);
    fChain->SetBranchAddress("HLT_e120_lhloose", &HLT_e120_lhloose, &b_HLT_e120_lhloose);
    fChain->SetBranchAddress("el_trigMatch_HLT_e60_lhmedium_nod0", &el_trigMatch_HLT_e60_lhmedium_nod0, &b_el_trigMatch_HLT_e60_lhmedium_nod0);
    fChain->SetBranchAddress("el_trigMatch_HLT_e26_lhtight_nod0_ivarloose", &el_trigMatch_HLT_e26_lhtight_nod0_ivarloose, &b_el_trigMatch_HLT_e26_lhtight_nod0_ivarloose);
    fChain->SetBranchAddress("el_trigMatch_HLT_e140_lhloose_nod0", &el_trigMatch_HLT_e140_lhloose_nod0, &b_el_trigMatch_HLT_e140_lhloose_nod0);
    fChain->SetBranchAddress("el_trigMatch_HLT_e60_lhmedium", &el_trigMatch_HLT_e60_lhmedium, &b_el_trigMatch_HLT_e60_lhmedium);
    fChain->SetBranchAddress("el_trigMatch_HLT_e24_lhmedium_L1EM20VH", &el_trigMatch_HLT_e24_lhmedium_L1EM20VH, &b_el_trigMatch_HLT_e24_lhmedium_L1EM20VH);
    fChain->SetBranchAddress("el_trigMatch_HLT_e120_lhloose", &el_trigMatch_HLT_e120_lhloose, &b_el_trigMatch_HLT_e120_lhloose);
    fChain->SetBranchAddress("mu_trigMatch_HLT_mu26_ivarmedium", &mu_trigMatch_HLT_mu26_ivarmedium, &b_mu_trigMatch_HLT_mu26_ivarmedium);
    fChain->SetBranchAddress("mu_trigMatch_HLT_mu50", &mu_trigMatch_HLT_mu50, &b_mu_trigMatch_HLT_mu50);
    fChain->SetBranchAddress("mu_trigMatch_HLT_mu20_iloose_L1MU15", &mu_trigMatch_HLT_mu20_iloose_L1MU15, &b_mu_trigMatch_HLT_mu20_iloose_L1MU15);
    fChain->SetBranchAddress("ph_topoetcone20", &ph_topoetcone20, &b_ph_topoetcone20);
    fChain->SetBranchAddress("ph_topoetcone30", &ph_topoetcone30, &b_ph_topoetcone30);
    fChain->SetBranchAddress("ph_topoetcone40", &ph_topoetcone40, &b_ph_topoetcone40);
    fChain->SetBranchAddress("ph_ptcone20", &ph_ptcone20, &b_ph_ptcone20);
    fChain->SetBranchAddress("ph_ptcone30", &ph_ptcone30, &b_ph_ptcone30);
    fChain->SetBranchAddress("ph_ptcone40", &ph_ptcone40, &b_ph_ptcone40);
    fChain->SetBranchAddress("ph_ptvarcone20", &ph_ptvarcone20, &b_ph_ptvarcone20);
    fChain->SetBranchAddress("ph_ptvarcone30", &ph_ptvarcone30, &b_ph_ptvarcone30);
    fChain->SetBranchAddress("ph_ptvarcone40", &ph_ptvarcone40, &b_ph_ptvarcone40);
    fChain->SetBranchAddress("ph_isoFCTCO", &ph_isoFCTCO, &b_ph_isoFCTCO);
    fChain->SetBranchAddress("ph_isoFCT", &ph_isoFCT, &b_ph_isoFCT);
    fChain->SetBranchAddress("ph_isoFCL", &ph_isoFCL, &b_ph_isoFCL);
    fChain->SetBranchAddress("ph_isTight", &ph_isTight, &b_ph_isTight);
    fChain->SetBranchAddress("ph_isLoose", &ph_isLoose, &b_ph_isLoose);
    fChain->SetBranchAddress("ph_isTight_daod", &ph_isTight_daod, &b_ph_isTight_daod);
    fChain->SetBranchAddress("ph_isHFake", &ph_isHFake, &b_ph_isHFake);
    fChain->SetBranchAddress("ph_isHadronFakeFailedDeltaE", &ph_isHadronFakeFailedDeltaE, &b_ph_isHadronFakeFailedDeltaE);
    fChain->SetBranchAddress("ph_isHadronFakeFailedFside", &ph_isHadronFakeFailedFside, &b_ph_isHadronFakeFailedFside);
    fChain->SetBranchAddress("ph_isHadronFakeFailedWs3", &ph_isHadronFakeFailedWs3, &b_ph_isHadronFakeFailedWs3);
    fChain->SetBranchAddress("ph_isHadronFakeFailedERatio", &ph_isHadronFakeFailedERatio, &b_ph_isHadronFakeFailedERatio);
    fChain->SetBranchAddress("ph_rhad1", &ph_rhad1, &b_ph_rhad1);
    fChain->SetBranchAddress("ph_rhad", &ph_rhad, &b_ph_rhad);
    fChain->SetBranchAddress("ph_reta", &ph_reta, &b_ph_reta);
    fChain->SetBranchAddress("ph_weta2", &ph_weta2, &b_ph_weta2);
    fChain->SetBranchAddress("ph_rphi", &ph_rphi, &b_ph_rphi);
    fChain->SetBranchAddress("ph_ws3", &ph_ws3, &b_ph_ws3);
    fChain->SetBranchAddress("ph_wstot", &ph_wstot, &b_ph_wstot);
    fChain->SetBranchAddress("ph_fracm", &ph_fracm, &b_ph_fracm);
    fChain->SetBranchAddress("ph_deltaE", &ph_deltaE, &b_ph_deltaE);
    fChain->SetBranchAddress("ph_eratio", &ph_eratio, &b_ph_eratio);
    fChain->SetBranchAddress("ph_emaxs1", &ph_emaxs1, &b_ph_emaxs1);
    fChain->SetBranchAddress("ph_f1", &ph_f1, &b_ph_f1);
    fChain->SetBranchAddress("ph_e277", &ph_e277, &b_ph_e277);
    fChain->SetBranchAddress("ph_OQ", &ph_OQ, &b_ph_OQ);
    fChain->SetBranchAddress("ph_author", &ph_author, &b_ph_author);
    fChain->SetBranchAddress("ph_conversionType", &ph_conversionType, &b_ph_conversionType);
    fChain->SetBranchAddress("ph_caloEta", &ph_caloEta, &b_ph_caloEta);
    fChain->SetBranchAddress("ph_isEM", &ph_isEM, &b_ph_isEM);
    fChain->SetBranchAddress("ph_nVertices", &ph_nVertices, &b_ph_nVertices);
    fChain->SetBranchAddress("ph_SF_eff", &ph_SF_eff, &b_ph_SF_eff);
    fChain->SetBranchAddress("ph_SF_effUP", &ph_SF_effUP, &b_ph_SF_effUP);
    fChain->SetBranchAddress("ph_SF_effDO", &ph_SF_effDO, &b_ph_SF_effDO);
    fChain->SetBranchAddress("ph_SF_iso", &ph_SF_iso, &b_ph_SF_iso);
    fChain->SetBranchAddress("ph_SF_lowisoUP", &ph_SF_lowisoUP, &b_ph_SF_lowisoUP);
    fChain->SetBranchAddress("ph_SF_lowisoDO", &ph_SF_lowisoDO, &b_ph_SF_lowisoDO);
    fChain->SetBranchAddress("ph_SF_trkisoUP", &ph_SF_trkisoUP, &b_ph_SF_trkisoUP);
    fChain->SetBranchAddress("ph_SF_trkisoDO", &ph_SF_trkisoDO, &b_ph_SF_trkisoDO);
    fChain->SetBranchAddress("ph_drleadjet", &ph_drleadjet, &b_ph_drleadjet);
    fChain->SetBranchAddress("ph_dralljet", &ph_dralljet, &b_ph_dralljet);
    fChain->SetBranchAddress("ph_drsubljet", &ph_drsubljet, &b_ph_drsubljet);
    fChain->SetBranchAddress("ph_drlept", &ph_drlept, &b_ph_drlept);
    fChain->SetBranchAddress("ph_mgammalept", &ph_mgammalept, &b_ph_mgammalept);
    fChain->SetBranchAddress("ph_mgammaleptlept", &ph_mgammaleptlept, &b_ph_mgammaleptlept);
   }
   if (!m_upgrade) {
    fChain->SetBranchAddress("selph_index1", &selph_index1, &b_selph_index1);
    fChain->SetBranchAddress("selph_index2", &selph_index2, &b_selph_index2);
    fChain->SetBranchAddress("selhf_index1", &selhf_index1, &b_selhf_index1);
   }
   if (!m_upgrade) {
    fChain->SetBranchAddress("ph_truthType", &ph_truthType, &b_ph_truthType);
    fChain->SetBranchAddress("ph_truthOrigin", &ph_truthOrigin, &b_ph_truthOrigin);
   } else {
    fChain->SetBranchAddress("ph_true_type", &ph_mc_Type, &b_ph_mc_Type);
    fChain->SetBranchAddress("ph_true_origin", &ph_mc_Origin, &b_ph_mc_Origin);
    fChain->SetBranchAddress("ph_faketype", &ph_faketype, &b_ph_faketype);
    fChain->SetBranchAddress("jet_mv1eff", &jet_mv1eff, &b_jet_mv1eff);
   }
   if (!m_upgrade) {
   fChain->SetBranchAddress("ph_truthAncestor", &ph_truthAncestor, &b_ph_truthAncestor);
   fChain->SetBranchAddress("ph_mc_pid", &ph_mc_pid, &b_ph_mc_pid);
   fChain->SetBranchAddress("ph_mc_barcode", &ph_mc_barcode, &b_ph_mc_barcode);
   fChain->SetBranchAddress("ph_mc_pt", &ph_mc_pt, &b_ph_mc_pt);
   fChain->SetBranchAddress("ph_mc_eta", &ph_mc_eta, &b_ph_mc_eta);
   fChain->SetBranchAddress("ph_mc_phi", &ph_mc_phi, &b_ph_mc_phi);
   fChain->SetBranchAddress("ph_mcel_dr", &ph_mcel_dr, &b_ph_mcel_dr);
   fChain->SetBranchAddress("ph_mcel_pt", &ph_mcel_pt, &b_ph_mcel_pt);
   fChain->SetBranchAddress("ph_mcel_eta", &ph_mcel_eta, &b_ph_mcel_eta);
   fChain->SetBranchAddress("ph_mcel_phi", &ph_mcel_phi, &b_ph_mcel_phi);
   fChain->SetBranchAddress("mcph_pt", &mcph_pt, &b_mcph_pt);
   fChain->SetBranchAddress("mcph_eta", &mcph_eta, &b_mcph_eta);
   fChain->SetBranchAddress("mcph_phi", &mcph_phi, &b_mcph_phi);
   fChain->SetBranchAddress("mcph_ancestor", &mcph_ancestor, &b_mcph_ancestor);
   fChain->SetBranchAddress("el_truthAncestor", &el_truthAncestor, &b_el_truthAncestor);
   fChain->SetBranchAddress("el_isoGradient", &el_isoGradient, &b_el_isoGradient);
   fChain->SetBranchAddress("el_mc_pid", &el_mc_pid, &b_el_mc_pid);
   fChain->SetBranchAddress("el_mc_charge", &el_mc_charge, &b_el_mc_charge);
   fChain->SetBranchAddress("el_mc_pt", &el_mc_pt, &b_el_mc_pt);
   fChain->SetBranchAddress("el_mc_eta", &el_mc_eta, &b_el_mc_eta);
   fChain->SetBranchAddress("el_mc_phi", &el_mc_phi, &b_el_mc_phi);
   fChain->SetBranchAddress("mu_mc_charge", &mu_mc_charge, &b_mu_mc_charge);
   fChain->SetBranchAddress("jet_mcdr_tW1", &jet_mcdr_tW1, &b_jet_mcdr_tW1);
   fChain->SetBranchAddress("jet_mcdr_tW2", &jet_mcdr_tW2, &b_jet_mcdr_tW2);
   fChain->SetBranchAddress("jet_mcdr_tB", &jet_mcdr_tB, &b_jet_mcdr_tB);
   fChain->SetBranchAddress("jet_mcdr_tbW1", &jet_mcdr_tbW1, &b_jet_mcdr_tbW1);
   fChain->SetBranchAddress("jet_mcdr_tbW2", &jet_mcdr_tbW2, &b_jet_mcdr_tbW2);
   fChain->SetBranchAddress("jet_mcdr_tbB", &jet_mcdr_tbB, &b_jet_mcdr_tbB);
   fChain->SetBranchAddress("lepton_type", &lepton_type, &b_lepton_type);
   fChain->SetBranchAddress("event_HT", &event_HT, &b_event_HT);
   fChain->SetBranchAddress("event_mll", &event_mll, &b_event_mll);
   fChain->SetBranchAddress("event_mwt", &event_mwt, &b_event_mwt);
   fChain->SetBranchAddress("event_njets", &event_njets, &b_event_njets);
   fChain->SetBranchAddress("event_nbjets70", &event_nbjets70, &b_event_nbjets70);
   fChain->SetBranchAddress("event_nbjets77", &event_nbjets77, &b_event_nbjets77);
   fChain->SetBranchAddress("event_nbjets85", &event_nbjets85, &b_event_nbjets85);
    fChain->SetBranchAddress("event_photonorigin", &event_photonorigin, &b_event_photonorigin);
    fChain->SetBranchAddress("event_photonoriginTA", &event_photonoriginTA, &b_event_photonoriginTA);
    fChain->SetBranchAddress("event_nhadronfakes", &event_nhadronfakes, &b_event_nhadronfakes);
    fChain->SetBranchAddress("ph_HFT_MVA", &ph_HFT_MVA, &b_ph_HFT_MVA);
    fChain->SetBranchAddress("ph_nHFT_MVA", &ph_nHFT_MVA, &b_ph_nHFT_MVA);
    fChain->SetBranchAddress("event_ELD_MVA", &event_ELD_MVA, &b_event_ELD_MVA);
    fChain->SetBranchAddress("jet_pt_1st", &jet_pt_1st, &b_jet_pt_1st);
    fChain->SetBranchAddress("jet_pt_2nd", &jet_pt_2nd, &b_jet_pt_2nd);
    fChain->SetBranchAddress("jet_pt_3rd", &jet_pt_3rd, &b_jet_pt_3rd);
    fChain->SetBranchAddress("jet_pt_4th", &jet_pt_4th, &b_jet_pt_4th);
    fChain->SetBranchAddress("jet_pt_5th", &jet_pt_5th, &b_jet_pt_5th);
    fChain->SetBranchAddress("jet_pt_6th", &jet_pt_6th, &b_jet_pt_6th);
    fChain->SetBranchAddress("jet_tagWeightBin_leading", &jet_tagWeightBin_leading, &b_jet_tagWeightBin_leading);
    fChain->SetBranchAddress("jet_tagWeightBin_subleading", &jet_tagWeightBin_subleading, &b_jet_tagWeightBin_subleading);
    fChain->SetBranchAddress("jet_tagWeightBin_subsubleading", &jet_tagWeightBin_subsubleading, &b_jet_tagWeightBin_subsubleading);
    fChain->SetBranchAddress("event_ngoodphotons", &event_ngoodphotons, &b_event_ngoodphotons);
   }
   fChain->SetBranchAddress("jet_nGhosts_bHadron", &jet_nGhosts_bHadron, &b_jet_nGhosts_bHadron);
   Notify();
}

Bool_t RecoLevel::Notify()
{
   // The Notify() function is called when a new file is opened. This
   // can be either for a new TTree in a TChain or when when a new TTree
   // is started when using PROOF. It is normally not necessary to make changes
   // to the generated code, but the routine can be extended by the
   // user if needed. The return value is currently not used.

   return kTRUE;
}

void RecoLevel::Show(Long64_t entry)
{
// Print contents of entry.
// If entry is not specified, print current entry
   if (!fChain) return;
   fChain->Show(entry);
}
Int_t RecoLevel::Cut(Long64_t entry)
{
// This function may be called from Loop.
// returns  1 if entry is accepted.
// returns -1 otherwise.
   return 1;
}
#endif // #ifdef RecoLevel_cxx
