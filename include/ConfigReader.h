#ifndef ConfigReader_h
#define ConfigReader_h
#include "Logger.h"
#include <cctype>
#include <iostream>
#include <sstream>
#include <fstream>
#include <iomanip>
#include <string>
#include <stdio.h>
#include <stdlib.h>
#include <map>
#include <vector>

using namespace std;

class ConfigReader {

private:

    vector<string> m_keyset;
    map<string,vector<string> > m_map;
    string m_tag;
    Logger *m_lg;
    string m_file;
    bool m_debug;

public:

    ConfigReader(string configfile, char tag = '-', bool debug = false);
    ~ConfigReader();
    
    void Init(bool debug = false);
    bool Contain(string key);
    int Size();
    
    string GetValue(string key, int pos = 0);
    int GetValueI(string key, int pos = 0);
    float GetValueF(string key, int pos = 0);
    bool GetValueB(string key, int pos = 0);
    void FillValueB(string key, string key2, bool &val);
    vector<string> GetValueAll(string key);
    vector<float> GetValueAllF(string key);
    vector<string> GetKeyAll(string ambikey);
    vector<vector<string> > GetAmbiValueAll(string ambikey);
    int CountWords(const char* str);
};
#endif
