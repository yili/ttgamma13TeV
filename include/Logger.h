#ifndef Logger_h
#define Logger_h
#include <iostream>
#include <iomanip>
#include <string>
#include <cstdarg>
#include <stdio.h>
#include <vector>

using namespace std;

namespace LOGGER{
    static int Green = 32;
    static int Red = 31;
    static int Gold = 33;
    static int Purple = 35;
}

class Logger {

private:

    string m_name;
    int m_name_width;
    int m_mark_number;
    string m_mark_type;
    int m_shift;
    int m_shift_step;
    bool m_simple_mode;
    string m_cout_dire;

    int m_cnt_warn;

    bool m_shutup;

public:
    
    Logger(string name = "Undefined", int name_width = 15, int mark_number = 15, string mark_type = "*");
    ~Logger();
    
    void SetModeSimple(bool simple = false);
    void SetName(string name);
    void SetCoutDire(string dire);
    void SetShift(int shift);
    void SetShiftStep(int shift_step);

    void ResetShift();
    void AddShift();
    void ReduShift();

    void SetPrecision(int precision);
    
    void PrintBase(string tag, int color);
    void NewLine();
    void Info();
    void Info(const char* fmt...);
    void InfoFixW(const char* fmt...);
    void Warn(const char* fmt...);
    void Err(const char* fmt...);
    void ShutUp();

    void Summary();
};
#endif
