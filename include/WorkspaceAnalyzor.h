#ifndef WORKSPACEANALYZOR_H
#define WORKSPACEANALYZOR_H

#include "AtlasStyle.h"
#include "Logger.h"

#include <TMultiGraph.h>
#include <TCanvas.h>
#include <TLatex.h>
#include <TBranch.h>
#include <TRandom3.h>
#include <THStack.h>
#include <TFile.h>
#include <TH1F.h>
#include <TH2.h>
#include <TF1.h>
#include <TCanvas.h>
#include <TLegend.h>
#include <TStyle.h>
#include <TConfidenceLevel.h>
#include <TLimit.h>
#include <TPaveStats.h>
#include <TLimitDataSource.h>
#include <TStopwatch.h>

#include <RooFit.h>
#include <RooRealVar.h>
#include <RooDataSet.h>
#include <RooGaussian.h>
#include <RooWorkspace.h>
#include <RooPlot.h>
#include <RooDataSet.h>
#include <RooRealVar.h>
#include <RooArgSet.h>
#include <RooWorkspace.h>
#include <RooNLLVar.h>
#include <RooMinuit.h>
#include <RooDataHist.h>
#include <RooHist.h>
#include <RooHistPdf.h>
#include <RooAddPdf.h>
#include <RooAbsPdf.h>
#include <RooFitResult.h>
#include <RooArgusBG.h>
#include <RooChi2Var.h>
#include <RooProfileLL.h>
#include <RooAddition.h>
#include <RooAddPdf.h>
#include <RooProdPdf.h>
#include <RooGaussModel.h>
#include <RooBinning.h>
#include <RooGenericPdf.h>
#include <RooExtendPdf.h>
#include <RooPoisson.h>
#include <RooLognormal.h>
#include <RooMCStudy.h>
#include <RooRandom.h>
#include <RooIntegralMorph.h>
#include <RooMomentMorph.h>
#include <RooCategory.h>
#include <RooSimultaneous.h>

//#include <RooStats/HistFactory/PiecewiseInterpolation.h>
#include <RooStats/HybridCalculatorOriginal.h>
#include <RooStats/HybridCalculator.h>
#include <RooStats/ProfileLikelihoodCalculator.h>
#include <RooStats/HypoTestResult.h>
#include <RooStats/MCMCCalculator.h>
#include <RooStats/UniformProposal.h>
#include <RooStats/FeldmanCousins.h>
#include <RooStats/NumberCountingPdfFactory.h>
#include <RooStats/ConfInterval.h>
#include <RooStats/PointSetInterval.h>
#include <RooStats/LikelihoodInterval.h>
#include <RooStats/LikelihoodIntervalPlot.h>
#include <RooStats/RooStatsUtils.h>
#include <RooStats/ModelConfig.h>
#include <RooStats/MCMCInterval.h>
#include <RooStats/MCMCIntervalPlot.h>
#include <RooStats/ProposalFunction.h>
#include <RooStats/ProposalHelper.h>
#include <RooStats/HybridPlot.h>
#include <RooStats/ToyMCSampler.h>
#include <RooStats/ProfileLikelihoodTestStat.h>
#include <RooStats/FrequentistCalculator.h>
#include <RooStats/HypoTestPlot.h>
#include <RooStats/HypoTestInverter.h>
#include <RooStats/HypoTestInverterResult.h>
#include <RooStats/HypoTestInverterPlot.h>
#include <RooStats/AsymptoticCalculator.h>
#include <RooStats/SequentialProposal.h>
#include <RooStats/RatioOfProfiledLikelihoodsTestStat.h>
#include <RooStats/ProofConfig.h>
#include <RooStats/NumberCountingUtils.h>

#include <iostream>
#include <sstream>
#include <string>
#include <iostream>
#include <iomanip>
#include <string>
#include <libgen.h>
#include <stdio.h>      
#include <stdlib.h> 
#include <map>

using namespace RooFit;
using namespace RooStats;
using namespace std;

class WorkspaceAnalyzor {

public:
    
    WorkspaceAnalyzor();
    ~WorkspaceAnalyzor();

    // Flags
    void SetInverseMode(bool inverse);
    void SetSilentMode(bool silent);

    // Config
    void SetFileName(string name);
    void SetWSName(string name);
    void SetMCName(string name);
    void SetDataName(string name);

    // Initialize
    void Init();
    void SetFuncFilter(vector<string> filt);
    void DetermineBinChanProc(RooFormulaVar* var, int &bin, int &chan, int &proc);

    // Mode
    void Print();
    void Fit(string option, bool verbose = true, bool save = true);
    void Plot(bool plc = false, bool stack = false, bool corr = false, bool pull = false, bool logy = false);
    void SysStudy(string type);

    // Save
    void SetSaveName(string savename);
    void SetSaveDir(string savedir);
    void SetSaveOption(string saveoption);
    void SetPlotDir(string plotdir);
    void Save();

    // Tool
    void Debug();
    void UsePullOrder(string order);
    void SetReferencePOI(int ref);
    void LinearityCheck();
    void ShutUp();
    void PrintDatas();
    void PrintParas(RooArgSet *set);
    void PrintFuncs(RooArgSet *set);
    void FixAllSys();
    void FloatSys(string tofloat);
    void FixSys(string tofix);
    void SortIndex(vector<float> inputs, vector<int> &outputs);
    int  FindRemoveMax(vector<float> &inputs);
    void UseAsimovData();
    vector<string>* GetSysNames();
    void PrintSysNames();
    pair<float,float> GetSysUpDn(RooFormulaVar* var, RooRealVar* sys, bool prefit = false);

    //void SetHistAppend(string appendname = "");

    //void ToyStudy(string sys = "");
    //void LoadResults(string name);

    //void BreakSysProf();
    //void Print(bool detail = false);
    //void PrintParaVals();

    //void SetNuisVal(string name, float val, bool verbose = false);
    //void SetNuisVal(vector<string> names, float val, bool verbose = false);
    //void SetNuisVal(vector<string> names, vector<float> vals, bool verbose = false);
    //void SetParaVal(vector<string> names, float val);

private:

    Logger* m_lg;

    // flag
    bool m_inverse_mode;
    bool m_silent_mode;
    bool m_use_asimov;
    int  m_ref_poi;
    bool m_debug;

    // config
    string m_filename;
    string m_wsname;
    string m_mcname;
    string m_dataname;
    string m_pull_order;
    
    // initialize
    TFile	    *m_infile;
    RooArgSet	    *m_obss;
    RooAbsPdf	    *m_model;
    RooAbsData	    *m_dataset;
    RooWorkspace    *m_workspace;
    ModelConfig	    *m_modelconfig;
    
    string  m_obs_title;
    string  m_obs_unit;
    float   m_obs_low;
    float   m_obs_high;
    int	    m_obs_nbin;
    RooRealVar* m_obs;

    vector<string>  *m_chan_names;
    vector<string>  *m_chan_titles;
    int		    m_chan_n;
    RooCategory	    *m_chans;
    RooCategory	    *m_chants;

    vector<int>			    *m_proc_n;
    vector<vector<string>*> 	    *m_proc_names;
    vector<vector<string>*> 	    *m_proc_titles;
    vector<RooCategory*>	    *m_procs;
    vector<RooCategory*>	    *m_procts;
    TH2F			    *h_proc_issigs;
    TH2F			    *h_proc_isfrees;

    RooRealVar			    *m_firstpoi;
    RooRealVar			    *m_secondpoi;
    string			    m_firstpoi_name;
    string			    m_firstpoi_title;
    bool			    m_multipoi;
    RooArgSet			    *m_pois;
    RooArgSet			    *m_nuiss;
    RooArgSet			    *m_nonsysnuiss;
    RooArgSet			    *m_sysnuiss;
    vector<string>		    *m_sys_names;
    vector<string>		    *m_sys_titles;
    int				    m_sys_n;
    RooArgSet			    *m_paras;
    RooArgSet			    *m_functions;
    RooArgSet			    *m_functions_new;
    vector<string>		    m_funcfilter;

    vector<vector<TH1F*>*>	    *m_proc_temps;
    vector<TH1F*>		    *m_datas;
    vector<float>		    *m_data_nums;

    // fit 
    float			    m_firstpoi_cen;
    float			    m_firstpoi_low;
    float			    m_firstpoi_high;
    float			    m_secondpoi_cen;
    float			    m_secondpoi_low;
    float			    m_secondpoi_high;
    map<string, float>		    m_prefit_val;
    map<string, float>		    m_prefit_err;
    map<string, float>		    m_postfit_err;
    RooFitResult		    *m_fit_results;
    ProfileLikelihoodCalculator	    *m_PLC;
    LikelihoodInterval		    *m_LI;
    
    // save
    string	m_savedir;
    string	m_savename;
    string	m_saveoption;
    string	m_plotdir;
    TH1F*	h_fit_results_plc;
    TH1F*	h_fit_results_poi;
    TH1F*	h_toy_study;
    vector<string> m_sys_order;
    vector<double> m_sys_order_val;
    TH1F*	h_sys_order;

    //HypoTestResult* m_hypo_results;
    //RooAbsReal* m_NLL;
    //RooAbsReal* m_NLLProf;

    //string m_savedir;
    //string m_savename;
    //string m_appendname;

    //map<string, float> ErrDecomp;
    //TH1F* h_err_decomp;
    //int m_toy_ntoy;
    //int m_toy_nevt;
    //float m_toy_fitrange;
    //int m_toy_fitbinning;
    //int m_toy_cnt;
    //TH1F* h_toy_results;

    //bool m_fit_stored;
    //bool m_dohypo_done;
    //bool m_doplc_done;
};
#endif


