cd ../

#######################################################################################################
## ejets channel
../bin/analysissample --Region CR1 --SubRegion ejets --Type Data --Selection CutSR --DoHist --SaveTag Final01 --Variation Nominal
#../bin/analysissample --Region CR1 --SubRegion ejets --Type QCD --QCDPara eta:mtw --Selection CutSR --DoHist --UseWeight --SaveTag Final01 --Variation Nominal

#../bin/analysissample --Region CR1 --SubRegion ejets --Type Reco --Process Signal --Selection CutSR --PhMatch TruePh --DoHist --UseWeight --LumiWeight --SaveTag Final01 --Variation Nominal
#../bin/analysissample --Region CR1 --SubRegion ejets --Type Reco --Process Signal --Selection CutSR --PhMatch Ancestor1 --DoHist --UseWeight --LumiWeight --SaveTag Final01 --Variation Nominal
#../bin/analysissample --Region CR1 --SubRegion ejets --Type Reco --Process Signal --Selection CutSR --PhMatch Ancestor2 --DoHist --UseWeight --LumiWeight --SaveTag Final01 --Variation Nominal
#../bin/analysissample --Region CR1 --SubRegion ejets --Type Reco --Process Signal --Selection CutSR --PhMatch Ancestor3 --DoHist --UseWeight --LumiWeight --SaveTag Final01 --Variation Nominal
#../bin/analysissample --Region CR1 --SubRegion ejets --Type Reco --Process TTBar --Selection CutSR --PhMatch TruePh --DoHist --UseWeight --LumiWeight --SaveTag Final01 --Variation Nominal
#../bin/analysissample --Region CR1 --SubRegion ejets --Type Reco --Process WGammajetsElNLO --Selection CutSR --PhMatch TruePh --DoHist --UseWeight --LumiWeight --SaveTag Final01 --Variation Nominal
#../bin/analysissample --Region CR1 --SubRegion ejets --Type Reco --Process WGammajetsMuNLO --Selection CutSR --PhMatch TruePh --DoHist --UseWeight --LumiWeight --SaveTag Final01 --Variation Nominal
#../bin/analysissample --Region CR1 --SubRegion ejets --Type Reco --Process WGammajetsTauNLO --Selection CutSR --PhMatch TruePh --DoHist --UseWeight --LumiWeight --SaveTag Final01 --Variation Nominal
#../bin/analysissample --Region CR1 --SubRegion ejets --Type Reco --Process WjetsEl --Selection CutSR --PhMatch TruePh --DoHist --UseWeight --LumiWeight --SaveTag Final01 --Variation Nominal
#../bin/analysissample --Region CR1 --SubRegion ejets --Type Reco --Process WjetsMu --Selection CutSR --PhMatch TruePh --DoHist --UseWeight --LumiWeight --SaveTag Final01 --Variation Nominal
#../bin/analysissample --Region CR1 --SubRegion ejets --Type Reco --Process WjetsTau --Selection CutSR --PhMatch TruePh --DoHist --UseWeight --LumiWeight --SaveTag Final01 --Variation Nominal
#../bin/analysissample --Region CR1 --SubRegion ejets --Type Reco --Process ZGammajetsElElNLO --Selection CutSR --PhMatch TruePh --DoHist --UseWeight --LumiWeight --SaveTag Final01 --Variation Nominal
#../bin/analysissample --Region CR1 --SubRegion ejets --Type Reco --Process ZGammajetsMuMuNLO --Selection CutSR --PhMatch TruePh --DoHist --UseWeight --LumiWeight --SaveTag Final01 --Variation Nominal
#../bin/analysissample --Region CR1 --SubRegion ejets --Type Reco --Process ZGammajetsTauTauNLO --Selection CutSR --PhMatch TruePh --DoHist --UseWeight --LumiWeight --SaveTag Final01 --Variation Nominal
#../bin/analysissample --Region CR1 --SubRegion ejets --Type Reco --Process ZjetsElEl --Selection CutSR --PhMatch TruePh --DoHist --UseWeight --LumiWeight --SaveTag Final01 --Variation Nominal
#../bin/analysissample --Region CR1 --SubRegion ejets --Type Reco --Process ZjetsMuMu --Selection CutSR --PhMatch TruePh --DoHist --UseWeight --LumiWeight --SaveTag Final01 --Variation Nominal
#../bin/analysissample --Region CR1 --SubRegion ejets --Type Reco --Process ZjetsTauTau --Selection CutSR --PhMatch TruePh --DoHist --UseWeight --LumiWeight --SaveTag Final01 --Variation Nominal
#../bin/analysissample --Region CR1 --SubRegion ejets --Type Reco --Process STOthers --Selection CutSR --PhMatch TruePh --DoHist --UseWeight --LumiWeight --SaveTag Final01 --Variation Nominal
#../bin/analysissample --Region CR1 --SubRegion ejets --Type Reco --Process STWT --Selection CutSR --PhMatch TruePh --DoHist --UseWeight --LumiWeight --SaveTag Final01 --Variation Nominal
#../bin/analysissample --Region CR1 --SubRegion ejets --Type Reco --Process Diboson --Selection CutSR --PhMatch TruePh --DoHist --UseWeight --LumiWeight --SaveTag Final01 --Variation Nominal
#
#../bin/analysissample --Region CR1 --SubRegion ejets --Type Reco --Process TTBar --Selection CutSR --PhMatch HFake --DoHist --UseWeight --LumiWeight --SaveTag Final01 --Variation Nominal
#../bin/analysissample --Region CR1 --SubRegion ejets --Type Reco --Process WjetsEl --Selection CutSR --PhMatch HFake --DoHist --UseWeight --LumiWeight --SaveTag Final01 --Variation Nominal
#../bin/analysissample --Region CR1 --SubRegion ejets --Type Reco --Process WjetsMu --Selection CutSR --PhMatch HFake --DoHist --UseWeight --LumiWeight --SaveTag Final01 --Variation Nominal
#../bin/analysissample --Region CR1 --SubRegion ejets --Type Reco --Process WjetsTau --Selection CutSR --PhMatch HFake --DoHist --UseWeight --LumiWeight --SaveTag Final01 --Variation Nominal
#../bin/analysissample --Region CR1 --SubRegion ejets --Type Reco --Process ZjetsElEl --Selection CutSR --PhMatch HFake --DoHist --UseWeight --LumiWeight --SaveTag Final01 --Variation Nominal
#../bin/analysissample --Region CR1 --SubRegion ejets --Type Reco --Process ZjetsMuMu --Selection CutSR --PhMatch HFake --DoHist --UseWeight --LumiWeight --SaveTag Final01 --Variation Nominal
#../bin/analysissample --Region CR1 --SubRegion ejets --Type Reco --Process ZjetsTauTau --Selection CutSR --PhMatch HFake --DoHist --UseWeight --LumiWeight --SaveTag Final01 --Variation Nominal
#../bin/analysissample --Region CR1 --SubRegion ejets --Type Reco --Process STOthers --Selection CutSR --PhMatch HFake --DoHist --UseWeight --LumiWeight --SaveTag Final01 --Variation Nominal
#../bin/analysissample --Region CR1 --SubRegion ejets --Type Reco --Process STWT --Selection CutSR --PhMatch HFake --DoHist --UseWeight --LumiWeight --SaveTag Final01 --Variation Nominal
#../bin/analysissample --Region CR1 --SubRegion ejets --Type Reco --Process Diboson --Selection CutSR --PhMatch HFake --DoHist --UseWeight --LumiWeight --SaveTag Final01 --Variation Nominal
#
#../bin/analysissample --Region CR1 --SubRegion ejets --Type Reco --Process TTBar --Selection CutSR --PhMatch EFake --DoHist --UseWeight --LumiWeight --SaveTag Final01 --Variation Nominal
#../bin/analysissample --Region CR1 --SubRegion ejets --Type Reco --Process WjetsEl --Selection CutSR --PhMatch EFake --DoHist --UseWeight --LumiWeight --SaveTag Final01 --Variation Nominal
#../bin/analysissample --Region CR1 --SubRegion ejets --Type Reco --Process WjetsMu --Selection CutSR --PhMatch EFake --DoHist --UseWeight --LumiWeight --SaveTag Final01 --Variation Nominal
#../bin/analysissample --Region CR1 --SubRegion ejets --Type Reco --Process WjetsTau --Selection CutSR --PhMatch EFake --DoHist --UseWeight --LumiWeight --SaveTag Final01 --Variation Nominal
#../bin/analysissample --Region CR1 --SubRegion ejets --Type Reco --Process ZjetsElEl --Selection CutSR --PhMatch EFake --DoHist --UseWeight --LumiWeight --SaveTag Final01 --Variation Nominal
#../bin/analysissample --Region CR1 --SubRegion ejets --Type Reco --Process ZjetsMuMu --Selection CutSR --PhMatch EFake --DoHist --UseWeight --LumiWeight --SaveTag Final01 --Variation Nominal
#../bin/analysissample --Region CR1 --SubRegion ejets --Type Reco --Process ZjetsTauTau --Selection CutSR --PhMatch EFake --DoHist --UseWeight --LumiWeight --SaveTag Final01 --Variation Nominal
#../bin/analysissample --Region CR1 --SubRegion ejets --Type Reco --Process STOthers --Selection CutSR --PhMatch EFake --DoHist --UseWeight --LumiWeight --SaveTag Final01 --Variation Nominal
#../bin/analysissample --Region CR1 --SubRegion ejets --Type Reco --Process STWT --Selection CutSR --PhMatch EFake --DoHist --UseWeight --LumiWeight --SaveTag Final01 --Variation Nominal
#../bin/analysissample --Region CR1 --SubRegion ejets --Type Reco --Process Diboson --Selection CutSR --PhMatch EFake --DoHist --UseWeight --LumiWeight --SaveTag Final01 --Variation Nominal
#
#######################################################################################################
## mujets channel
../bin/analysissample --Region CR1 --SubRegion mujets --Type Data --Selection CutSR --DoHist --SaveTag Final01 --Variation Nominal
#../bin/analysissample --Region CR1 --SubRegion mujets --Type QCD --UsePS --QCDPara pt:mtw --Selection CutSR --DoHist --UseWeight --SaveTag Final01 --Variation Nominal

#../bin/analysissample --Region CR1 --SubRegion mujets --Type Reco --Process Signal --Selection CutSR --PhMatch TruePh --DoHist --UseWeight --LumiWeight --SaveTag Final01 --Variation Nominal
#../bin/analysissample --Region CR1 --SubRegion mujets --Type Reco --Process Signal --Selection CutSR --PhMatch Ancestor1 --DoHist --UseWeight --LumiWeight --SaveTag Final01 --Variation Nominal
#../bin/analysissample --Region CR1 --SubRegion mujets --Type Reco --Process Signal --Selection CutSR --PhMatch Ancestor2 --DoHist --UseWeight --LumiWeight --SaveTag Final01 --Variation Nominal
#../bin/analysissample --Region CR1 --SubRegion mujets --Type Reco --Process Signal --Selection CutSR --PhMatch Ancestor3 --DoHist --UseWeight --LumiWeight --SaveTag Final01 --Variation Nominal
#../bin/analysissample --Region CR1 --SubRegion mujets --Type Reco --Process TTBar --Selection CutSR --PhMatch TruePh --DoHist --UseWeight --LumiWeight --SaveTag Final01 --Variation Nominal
#../bin/analysissample --Region CR1 --SubRegion mujets --Type Reco --Process WGammajetsElNLO --Selection CutSR --PhMatch TruePh --DoHist --UseWeight --LumiWeight --SaveTag Final01 --Variation Nominal
#../bin/analysissample --Region CR1 --SubRegion mujets --Type Reco --Process WGammajetsMuNLO --Selection CutSR --PhMatch TruePh --DoHist --UseWeight --LumiWeight --SaveTag Final01 --Variation Nominal
#../bin/analysissample --Region CR1 --SubRegion mujets --Type Reco --Process WGammajetsTauNLO --Selection CutSR --PhMatch TruePh --DoHist --UseWeight --LumiWeight --SaveTag Final01 --Variation Nominal
#../bin/analysissample --Region CR1 --SubRegion mujets --Type Reco --Process WjetsEl --Selection CutSR --PhMatch TruePh --DoHist --UseWeight --LumiWeight --SaveTag Final01 --Variation Nominal
#../bin/analysissample --Region CR1 --SubRegion mujets --Type Reco --Process WjetsMu --Selection CutSR --PhMatch TruePh --DoHist --UseWeight --LumiWeight --SaveTag Final01 --Variation Nominal
#../bin/analysissample --Region CR1 --SubRegion mujets --Type Reco --Process WjetsTau --Selection CutSR --PhMatch TruePh --DoHist --UseWeight --LumiWeight --SaveTag Final01 --Variation Nominal
#../bin/analysissample --Region CR1 --SubRegion mujets --Type Reco --Process ZGammajetsElElNLO --Selection CutSR --PhMatch TruePh --DoHist --UseWeight --LumiWeight --SaveTag Final01 --Variation Nominal
#../bin/analysissample --Region CR1 --SubRegion mujets --Type Reco --Process ZGammajetsMuMuNLO --Selection CutSR --PhMatch TruePh --DoHist --UseWeight --LumiWeight --SaveTag Final01 --Variation Nominal
#../bin/analysissample --Region CR1 --SubRegion mujets --Type Reco --Process ZGammajetsTauTauNLO --Selection CutSR --PhMatch TruePh --DoHist --UseWeight --LumiWeight --SaveTag Final01 --Variation Nominal
#../bin/analysissample --Region CR1 --SubRegion mujets --Type Reco --Process ZjetsElEl --Selection CutSR --PhMatch TruePh --DoHist --UseWeight --LumiWeight --SaveTag Final01 --Variation Nominal
#../bin/analysissample --Region CR1 --SubRegion mujets --Type Reco --Process ZjetsMuMu --Selection CutSR --PhMatch TruePh --DoHist --UseWeight --LumiWeight --SaveTag Final01 --Variation Nominal
#../bin/analysissample --Region CR1 --SubRegion mujets --Type Reco --Process ZjetsTauTau --Selection CutSR --PhMatch TruePh --DoHist --UseWeight --LumiWeight --SaveTag Final01 --Variation Nominal
#../bin/analysissample --Region CR1 --SubRegion mujets --Type Reco --Process STOthers --Selection CutSR --PhMatch TruePh --DoHist --UseWeight --LumiWeight --SaveTag Final01 --Variation Nominal
#../bin/analysissample --Region CR1 --SubRegion mujets --Type Reco --Process STWT --Selection CutSR --PhMatch TruePh --DoHist --UseWeight --LumiWeight --SaveTag Final01 --Variation Nominal
#../bin/analysissample --Region CR1 --SubRegion mujets --Type Reco --Process Diboson --Selection CutSR --PhMatch TruePh --DoHist --UseWeight --LumiWeight --SaveTag Final01 --Variation Nominal
#
#../bin/analysissample --Region CR1 --SubRegion mujets --Type Reco --Process TTBar --Selection CutSR --PhMatch HFake --DoHist --UseWeight --LumiWeight --SaveTag Final01 --Variation Nominal
#../bin/analysissample --Region CR1 --SubRegion mujets --Type Reco --Process WjetsEl --Selection CutSR --PhMatch HFake --DoHist --UseWeight --LumiWeight --SaveTag Final01 --Variation Nominal
#../bin/analysissample --Region CR1 --SubRegion mujets --Type Reco --Process WjetsMu --Selection CutSR --PhMatch HFake --DoHist --UseWeight --LumiWeight --SaveTag Final01 --Variation Nominal
#../bin/analysissample --Region CR1 --SubRegion mujets --Type Reco --Process WjetsTau --Selection CutSR --PhMatch HFake --DoHist --UseWeight --LumiWeight --SaveTag Final01 --Variation Nominal
#../bin/analysissample --Region CR1 --SubRegion mujets --Type Reco --Process ZjetsElEl --Selection CutSR --PhMatch HFake --DoHist --UseWeight --LumiWeight --SaveTag Final01 --Variation Nominal
#../bin/analysissample --Region CR1 --SubRegion mujets --Type Reco --Process ZjetsMuMu --Selection CutSR --PhMatch HFake --DoHist --UseWeight --LumiWeight --SaveTag Final01 --Variation Nominal
#../bin/analysissample --Region CR1 --SubRegion mujets --Type Reco --Process ZjetsTauTau --Selection CutSR --PhMatch HFake --DoHist --UseWeight --LumiWeight --SaveTag Final01 --Variation Nominal
#../bin/analysissample --Region CR1 --SubRegion mujets --Type Reco --Process STOthers --Selection CutSR --PhMatch HFake --DoHist --UseWeight --LumiWeight --SaveTag Final01 --Variation Nominal
#../bin/analysissample --Region CR1 --SubRegion mujets --Type Reco --Process STWT --Selection CutSR --PhMatch HFake --DoHist --UseWeight --LumiWeight --SaveTag Final01 --Variation Nominal
#../bin/analysissample --Region CR1 --SubRegion mujets --Type Reco --Process Diboson --Selection CutSR --PhMatch HFake --DoHist --UseWeight --LumiWeight --SaveTag Final01 --Variation Nominal
#
#../bin/analysissample --Region CR1 --SubRegion mujets --Type Reco --Process TTBar --Selection CutSR --PhMatch EFake --DoHist --UseWeight --LumiWeight --SaveTag Final01 --Variation Nominal
#../bin/analysissample --Region CR1 --SubRegion mujets --Type Reco --Process WjetsEl --Selection CutSR --PhMatch EFake --DoHist --UseWeight --LumiWeight --SaveTag Final01 --Variation Nominal
#../bin/analysissample --Region CR1 --SubRegion mujets --Type Reco --Process WjetsMu --Selection CutSR --PhMatch EFake --DoHist --UseWeight --LumiWeight --SaveTag Final01 --Variation Nominal
#../bin/analysissample --Region CR1 --SubRegion mujets --Type Reco --Process WjetsTau --Selection CutSR --PhMatch EFake --DoHist --UseWeight --LumiWeight --SaveTag Final01 --Variation Nominal
#../bin/analysissample --Region CR1 --SubRegion mujets --Type Reco --Process ZjetsElEl --Selection CutSR --PhMatch EFake --DoHist --UseWeight --LumiWeight --SaveTag Final01 --Variation Nominal
#../bin/analysissample --Region CR1 --SubRegion mujets --Type Reco --Process ZjetsMuMu --Selection CutSR --PhMatch EFake --DoHist --UseWeight --LumiWeight --SaveTag Final01 --Variation Nominal
#../bin/analysissample --Region CR1 --SubRegion mujets --Type Reco --Process ZjetsTauTau --Selection CutSR --PhMatch EFake --DoHist --UseWeight --LumiWeight --SaveTag Final01 --Variation Nominal
#../bin/analysissample --Region CR1 --SubRegion mujets --Type Reco --Process STOthers --Selection CutSR --PhMatch EFake --DoHist --UseWeight --LumiWeight --SaveTag Final01 --Variation Nominal
#../bin/analysissample --Region CR1 --SubRegion mujets --Type Reco --Process STWT --Selection CutSR --PhMatch EFake --DoHist --UseWeight --LumiWeight --SaveTag Final01 --Variation Nominal
#../bin/analysissample --Region CR1 --SubRegion mujets --Type Reco --Process Diboson --Selection CutSR --PhMatch EFake --DoHist --UseWeight --LumiWeight --SaveTag Final01 --Variation Nominal

#######################################################################################################
## emu channel
../bin/analysissample --Region CR1 --SubRegion emu --Type Data --Selection CutSR --DoHist --SaveTag Final01 --Variation Nominal
#
#../bin/analysissample --Region CR1 --SubRegion emu --Type Reco --Process Signal --Selection CutSR --PhMatch TruePh --DoHist --UseWeight --LumiWeight --SaveTag Final01 --Variation Nominal
#../bin/analysissample --Region CR1 --SubRegion emu --Type Reco --Process Signal --Selection CutSR --PhMatch Ancestor1 --DoHist --UseWeight --LumiWeight --SaveTag Final01 --Variation Nominal
#../bin/analysissample --Region CR1 --SubRegion emu --Type Reco --Process Signal --Selection CutSR --PhMatch Ancestor2 --DoHist --UseWeight --LumiWeight --SaveTag Final01 --Variation Nominal
#../bin/analysissample --Region CR1 --SubRegion emu --Type Reco --Process Signal --Selection CutSR --PhMatch Ancestor3 --DoHist --UseWeight --LumiWeight --SaveTag Final01 --Variation Nominal
#../bin/analysissample --Region CR1 --SubRegion emu --Type Reco --Process TTBar --Selection CutSR --PhMatch TruePh --DoHist --UseWeight --LumiWeight --SaveTag Final01 --Variation Nominal
#../bin/analysissample --Region CR1 --SubRegion emu --Type Reco --Process WGammajetsElNLO --Selection CutSR --PhMatch TruePh --DoHist --UseWeight --LumiWeight --SaveTag Final01 --Variation Nominal
#../bin/analysissample --Region CR1 --SubRegion emu --Type Reco --Process WGammajetsMuNLO --Selection CutSR --PhMatch TruePh --DoHist --UseWeight --LumiWeight --SaveTag Final01 --Variation Nominal
#../bin/analysissample --Region CR1 --SubRegion emu --Type Reco --Process WGammajetsTauNLO --Selection CutSR --PhMatch TruePh --DoHist --UseWeight --LumiWeight --SaveTag Final01 --Variation Nominal
#../bin/analysissample --Region CR1 --SubRegion emu --Type Reco --Process WjetsEl --Selection CutSR --PhMatch TruePh --DoHist --UseWeight --LumiWeight --SaveTag Final01 --Variation Nominal
#../bin/analysissample --Region CR1 --SubRegion emu --Type Reco --Process WjetsMu --Selection CutSR --PhMatch TruePh --DoHist --UseWeight --LumiWeight --SaveTag Final01 --Variation Nominal
#../bin/analysissample --Region CR1 --SubRegion emu --Type Reco --Process WjetsTau --Selection CutSR --PhMatch TruePh --DoHist --UseWeight --LumiWeight --SaveTag Final01 --Variation Nominal
#../bin/analysissample --Region CR1 --SubRegion emu --Type Reco --Process ZGammajetsElElNLO --Selection CutSR --PhMatch TruePh --DoHist --UseWeight --LumiWeight --SaveTag Final01 --Variation Nominal
#../bin/analysissample --Region CR1 --SubRegion emu --Type Reco --Process ZGammajetsMuMuNLO --Selection CutSR --PhMatch TruePh --DoHist --UseWeight --LumiWeight --SaveTag Final01 --Variation Nominal
#../bin/analysissample --Region CR1 --SubRegion emu --Type Reco --Process ZGammajetsTauTauNLO --Selection CutSR --PhMatch TruePh --DoHist --UseWeight --LumiWeight --SaveTag Final01 --Variation Nominal
#../bin/analysissample --Region CR1 --SubRegion emu --Type Reco --Process ZjetsElEl --Selection CutSR --PhMatch TruePh --DoHist --UseWeight --LumiWeight --SaveTag Final01 --Variation Nominal
#../bin/analysissample --Region CR1 --SubRegion emu --Type Reco --Process ZjetsMuMu --Selection CutSR --PhMatch TruePh --DoHist --UseWeight --LumiWeight --SaveTag Final01 --Variation Nominal
#../bin/analysissample --Region CR1 --SubRegion emu --Type Reco --Process ZjetsTauTau --Selection CutSR --PhMatch TruePh --DoHist --UseWeight --LumiWeight --SaveTag Final01 --Variation Nominal
#../bin/analysissample --Region CR1 --SubRegion emu --Type Reco --Process STOthers --Selection CutSR --PhMatch TruePh --DoHist --UseWeight --LumiWeight --SaveTag Final01 --Variation Nominal
#../bin/analysissample --Region CR1 --SubRegion emu --Type Reco --Process STWT --Selection CutSR --PhMatch TruePh --DoHist --UseWeight --LumiWeight --SaveTag Final01 --Variation Nominal
#../bin/analysissample --Region CR1 --SubRegion emu --Type Reco --Process Diboson --Selection CutSR --PhMatch TruePh --DoHist --UseWeight --LumiWeight --SaveTag Final01 --Variation Nominal
#
#../bin/analysissample --Region CR1 --SubRegion emu --Type Reco --Process TTBar --Selection CutSR --PhMatch HFake --DoHist --UseWeight --LumiWeight --SaveTag Final01 --Variation Nominal
#../bin/analysissample --Region CR1 --SubRegion emu --Type Reco --Process WjetsEl --Selection CutSR --PhMatch HFake --DoHist --UseWeight --LumiWeight --SaveTag Final01 --Variation Nominal
#../bin/analysissample --Region CR1 --SubRegion emu --Type Reco --Process WjetsMu --Selection CutSR --PhMatch HFake --DoHist --UseWeight --LumiWeight --SaveTag Final01 --Variation Nominal
#../bin/analysissample --Region CR1 --SubRegion emu --Type Reco --Process WjetsTau --Selection CutSR --PhMatch HFake --DoHist --UseWeight --LumiWeight --SaveTag Final01 --Variation Nominal
#../bin/analysissample --Region CR1 --SubRegion emu --Type Reco --Process ZjetsElEl --Selection CutSR --PhMatch HFake --DoHist --UseWeight --LumiWeight --SaveTag Final01 --Variation Nominal
#../bin/analysissample --Region CR1 --SubRegion emu --Type Reco --Process ZjetsMuMu --Selection CutSR --PhMatch HFake --DoHist --UseWeight --LumiWeight --SaveTag Final01 --Variation Nominal
#../bin/analysissample --Region CR1 --SubRegion emu --Type Reco --Process ZjetsTauTau --Selection CutSR --PhMatch HFake --DoHist --UseWeight --LumiWeight --SaveTag Final01 --Variation Nominal
#../bin/analysissample --Region CR1 --SubRegion emu --Type Reco --Process STOthers --Selection CutSR --PhMatch HFake --DoHist --UseWeight --LumiWeight --SaveTag Final01 --Variation Nominal
#../bin/analysissample --Region CR1 --SubRegion emu --Type Reco --Process STWT --Selection CutSR --PhMatch HFake --DoHist --UseWeight --LumiWeight --SaveTag Final01 --Variation Nominal
#../bin/analysissample --Region CR1 --SubRegion emu --Type Reco --Process Diboson --Selection CutSR --PhMatch HFake --DoHist --UseWeight --LumiWeight --SaveTag Final01 --Variation Nominal
#
#../bin/analysissample --Region CR1 --SubRegion emu --Type Reco --Process TTBar --Selection CutSR --PhMatch EFake --DoHist --UseWeight --LumiWeight --SaveTag Final01 --Variation Nominal
#../bin/analysissample --Region CR1 --SubRegion emu --Type Reco --Process WjetsEl --Selection CutSR --PhMatch EFake --DoHist --UseWeight --LumiWeight --SaveTag Final01 --Variation Nominal
#../bin/analysissample --Region CR1 --SubRegion emu --Type Reco --Process WjetsMu --Selection CutSR --PhMatch EFake --DoHist --UseWeight --LumiWeight --SaveTag Final01 --Variation Nominal
#../bin/analysissample --Region CR1 --SubRegion emu --Type Reco --Process WjetsTau --Selection CutSR --PhMatch EFake --DoHist --UseWeight --LumiWeight --SaveTag Final01 --Variation Nominal
#../bin/analysissample --Region CR1 --SubRegion emu --Type Reco --Process ZjetsElEl --Selection CutSR --PhMatch EFake --DoHist --UseWeight --LumiWeight --SaveTag Final01 --Variation Nominal
#../bin/analysissample --Region CR1 --SubRegion emu --Type Reco --Process ZjetsMuMu --Selection CutSR --PhMatch EFake --DoHist --UseWeight --LumiWeight --SaveTag Final01 --Variation Nominal
#../bin/analysissample --Region CR1 --SubRegion emu --Type Reco --Process ZjetsTauTau --Selection CutSR --PhMatch EFake --DoHist --UseWeight --LumiWeight --SaveTag Final01 --Variation Nominal
#../bin/analysissample --Region CR1 --SubRegion emu --Type Reco --Process STOthers --Selection CutSR --PhMatch EFake --DoHist --UseWeight --LumiWeight --SaveTag Final01 --Variation Nominal
#../bin/analysissample --Region CR1 --SubRegion emu --Type Reco --Process STWT --Selection CutSR --PhMatch EFake --DoHist --UseWeight --LumiWeight --SaveTag Final01 --Variation Nominal
#../bin/analysissample --Region CR1 --SubRegion emu --Type Reco --Process Diboson --Selection CutSR --PhMatch EFake --DoHist --UseWeight --LumiWeight --SaveTag Final01 --Variation Nominal
#
#######################################################################################################
## ee channel
../bin/analysissample --Region CR1 --SubRegion ee --Type Data --Selection CutSR --DoHist --SaveTag Final01 --Variation Nominal
#
#../bin/analysissample --Region CR1 --SubRegion ee --Type Reco --Process Signal --Selection CutSR --PhMatch TruePh --DoHist --UseWeight --LumiWeight --SaveTag Final01 --Variation Nominal
#../bin/analysissample --Region CR1 --SubRegion ee --Type Reco --Process Signal --Selection CutSR --PhMatch Ancestor1 --DoHist --UseWeight --LumiWeight --SaveTag Final01 --Variation Nominal
#../bin/analysissample --Region CR1 --SubRegion ee --Type Reco --Process Signal --Selection CutSR --PhMatch Ancestor2 --DoHist --UseWeight --LumiWeight --SaveTag Final01 --Variation Nominal
#../bin/analysissample --Region CR1 --SubRegion ee --Type Reco --Process Signal --Selection CutSR --PhMatch Ancestor3 --DoHist --UseWeight --LumiWeight --SaveTag Final01 --Variation Nominal
#../bin/analysissample --Region CR1 --SubRegion ee --Type Reco --Process TTBar --Selection CutSR --PhMatch TruePh --DoHist --UseWeight --LumiWeight --SaveTag Final01 --Variation Nominal
#../bin/analysissample --Region CR1 --SubRegion ee --Type Reco --Process WGammajetsElNLO --Selection CutSR --PhMatch TruePh --DoHist --UseWeight --LumiWeight --SaveTag Final01 --Variation Nominal
#../bin/analysissample --Region CR1 --SubRegion ee --Type Reco --Process WGammajetsMuNLO --Selection CutSR --PhMatch TruePh --DoHist --UseWeight --LumiWeight --SaveTag Final01 --Variation Nominal
#../bin/analysissample --Region CR1 --SubRegion ee --Type Reco --Process WGammajetsTauNLO --Selection CutSR --PhMatch TruePh --DoHist --UseWeight --LumiWeight --SaveTag Final01 --Variation Nominal
#../bin/analysissample --Region CR1 --SubRegion ee --Type Reco --Process WjetsEl --Selection CutSR --PhMatch TruePh --DoHist --UseWeight --LumiWeight --SaveTag Final01 --Variation Nominal
#../bin/analysissample --Region CR1 --SubRegion ee --Type Reco --Process WjetsMu --Selection CutSR --PhMatch TruePh --DoHist --UseWeight --LumiWeight --SaveTag Final01 --Variation Nominal
#../bin/analysissample --Region CR1 --SubRegion ee --Type Reco --Process WjetsTau --Selection CutSR --PhMatch TruePh --DoHist --UseWeight --LumiWeight --SaveTag Final01 --Variation Nominal
#../bin/analysissample --Region CR1 --SubRegion ee --Type Reco --Process ZGammajetsElElNLO --Selection CutSR --PhMatch TruePh --DoHist --UseWeight --LumiWeight --SaveTag Final01 --Variation Nominal
#../bin/analysissample --Region CR1 --SubRegion ee --Type Reco --Process ZGammajetsMuMuNLO --Selection CutSR --PhMatch TruePh --DoHist --UseWeight --LumiWeight --SaveTag Final01 --Variation Nominal
#../bin/analysissample --Region CR1 --SubRegion ee --Type Reco --Process ZGammajetsTauTauNLO --Selection CutSR --PhMatch TruePh --DoHist --UseWeight --LumiWeight --SaveTag Final01 --Variation Nominal
#../bin/analysissample --Region CR1 --SubRegion ee --Type Reco --Process ZjetsElEl --Selection CutSR --PhMatch TruePh --DoHist --UseWeight --LumiWeight --SaveTag Final01 --Variation Nominal
#../bin/analysissample --Region CR1 --SubRegion ee --Type Reco --Process ZjetsMuMu --Selection CutSR --PhMatch TruePh --DoHist --UseWeight --LumiWeight --SaveTag Final01 --Variation Nominal
#../bin/analysissample --Region CR1 --SubRegion ee --Type Reco --Process ZjetsTauTau --Selection CutSR --PhMatch TruePh --DoHist --UseWeight --LumiWeight --SaveTag Final01 --Variation Nominal
#../bin/analysissample --Region CR1 --SubRegion ee --Type Reco --Process STOthers --Selection CutSR --PhMatch TruePh --DoHist --UseWeight --LumiWeight --SaveTag Final01 --Variation Nominal
#../bin/analysissample --Region CR1 --SubRegion ee --Type Reco --Process STWT --Selection CutSR --PhMatch TruePh --DoHist --UseWeight --LumiWeight --SaveTag Final01 --Variation Nominal
#../bin/analysissample --Region CR1 --SubRegion ee --Type Reco --Process Diboson --Selection CutSR --PhMatch TruePh --DoHist --UseWeight --LumiWeight --SaveTag Final01 --Variation Nominal
#
#../bin/analysissample --Region CR1 --SubRegion ee --Type Reco --Process TTBar --Selection CutSR --PhMatch HFake --DoHist --UseWeight --LumiWeight --SaveTag Final01 --Variation Nominal
#../bin/analysissample --Region CR1 --SubRegion ee --Type Reco --Process WjetsEl --Selection CutSR --PhMatch HFake --DoHist --UseWeight --LumiWeight --SaveTag Final01 --Variation Nominal
#../bin/analysissample --Region CR1 --SubRegion ee --Type Reco --Process WjetsMu --Selection CutSR --PhMatch HFake --DoHist --UseWeight --LumiWeight --SaveTag Final01 --Variation Nominal
#../bin/analysissample --Region CR1 --SubRegion ee --Type Reco --Process WjetsTau --Selection CutSR --PhMatch HFake --DoHist --UseWeight --LumiWeight --SaveTag Final01 --Variation Nominal
#../bin/analysissample --Region CR1 --SubRegion ee --Type Reco --Process ZjetsElEl --Selection CutSR --PhMatch HFake --DoHist --UseWeight --LumiWeight --SaveTag Final01 --Variation Nominal
#../bin/analysissample --Region CR1 --SubRegion ee --Type Reco --Process ZjetsMuMu --Selection CutSR --PhMatch HFake --DoHist --UseWeight --LumiWeight --SaveTag Final01 --Variation Nominal
#../bin/analysissample --Region CR1 --SubRegion ee --Type Reco --Process ZjetsTauTau --Selection CutSR --PhMatch HFake --DoHist --UseWeight --LumiWeight --SaveTag Final01 --Variation Nominal
#../bin/analysissample --Region CR1 --SubRegion ee --Type Reco --Process STOthers --Selection CutSR --PhMatch HFake --DoHist --UseWeight --LumiWeight --SaveTag Final01 --Variation Nominal
#../bin/analysissample --Region CR1 --SubRegion ee --Type Reco --Process STWT --Selection CutSR --PhMatch HFake --DoHist --UseWeight --LumiWeight --SaveTag Final01 --Variation Nominal
#../bin/analysissample --Region CR1 --SubRegion ee --Type Reco --Process Diboson --Selection CutSR --PhMatch HFake --DoHist --UseWeight --LumiWeight --SaveTag Final01 --Variation Nominal
#
#../bin/analysissample --Region CR1 --SubRegion ee --Type Reco --Process TTBar --Selection CutSR --PhMatch EFake --DoHist --UseWeight --LumiWeight --SaveTag Final01 --Variation Nominal
#../bin/analysissample --Region CR1 --SubRegion ee --Type Reco --Process WjetsEl --Selection CutSR --PhMatch EFake --DoHist --UseWeight --LumiWeight --SaveTag Final01 --Variation Nominal
#../bin/analysissample --Region CR1 --SubRegion ee --Type Reco --Process WjetsMu --Selection CutSR --PhMatch EFake --DoHist --UseWeight --LumiWeight --SaveTag Final01 --Variation Nominal
#../bin/analysissample --Region CR1 --SubRegion ee --Type Reco --Process WjetsTau --Selection CutSR --PhMatch EFake --DoHist --UseWeight --LumiWeight --SaveTag Final01 --Variation Nominal
#../bin/analysissample --Region CR1 --SubRegion ee --Type Reco --Process ZjetsElEl --Selection CutSR --PhMatch EFake --DoHist --UseWeight --LumiWeight --SaveTag Final01 --Variation Nominal
#../bin/analysissample --Region CR1 --SubRegion ee --Type Reco --Process ZjetsMuMu --Selection CutSR --PhMatch EFake --DoHist --UseWeight --LumiWeight --SaveTag Final01 --Variation Nominal
#../bin/analysissample --Region CR1 --SubRegion ee --Type Reco --Process ZjetsTauTau --Selection CutSR --PhMatch EFake --DoHist --UseWeight --LumiWeight --SaveTag Final01 --Variation Nominal
#../bin/analysissample --Region CR1 --SubRegion ee --Type Reco --Process STOthers --Selection CutSR --PhMatch EFake --DoHist --UseWeight --LumiWeight --SaveTag Final01 --Variation Nominal
#../bin/analysissample --Region CR1 --SubRegion ee --Type Reco --Process STWT --Selection CutSR --PhMatch EFake --DoHist --UseWeight --LumiWeight --SaveTag Final01 --Variation Nominal
#../bin/analysissample --Region CR1 --SubRegion ee --Type Reco --Process Diboson --Selection CutSR --PhMatch EFake --DoHist --UseWeight --LumiWeight --SaveTag Final01 --Variation Nominal
#
#######################################################################################################
## mumu channel
../bin/analysissample --Region CR1 --SubRegion mumu --Type Data --Selection CutSR --DoHist --SaveTag Final01 --Variation Nominal
#
#../bin/analysissample --Region CR1 --SubRegion mumu --Type Reco --Process Signal --Selection CutSR --PhMatch TruePh --DoHist --UseWeight --LumiWeight --SaveTag Final01 --Variation Nominal
#../bin/analysissample --Region CR1 --SubRegion mumu --Type Reco --Process Signal --Selection CutSR --PhMatch Ancestor1 --DoHist --UseWeight --LumiWeight --SaveTag Final01 --Variation Nominal
#../bin/analysissample --Region CR1 --SubRegion mumu --Type Reco --Process Signal --Selection CutSR --PhMatch Ancestor2 --DoHist --UseWeight --LumiWeight --SaveTag Final01 --Variation Nominal
#../bin/analysissample --Region CR1 --SubRegion mumu --Type Reco --Process Signal --Selection CutSR --PhMatch Ancestor3 --DoHist --UseWeight --LumiWeight --SaveTag Final01 --Variation Nominal
#../bin/analysissample --Region CR1 --SubRegion mumu --Type Reco --Process TTBar --Selection CutSR --PhMatch TruePh --DoHist --UseWeight --LumiWeight --SaveTag Final01 --Variation Nominal
#../bin/analysissample --Region CR1 --SubRegion mumu --Type Reco --Process WGammajetsElNLO --Selection CutSR --PhMatch TruePh --DoHist --UseWeight --LumiWeight --SaveTag Final01 --Variation Nominal
#../bin/analysissample --Region CR1 --SubRegion mumu --Type Reco --Process WGammajetsMuNLO --Selection CutSR --PhMatch TruePh --DoHist --UseWeight --LumiWeight --SaveTag Final01 --Variation Nominal
#../bin/analysissample --Region CR1 --SubRegion mumu --Type Reco --Process WGammajetsTauNLO --Selection CutSR --PhMatch TruePh --DoHist --UseWeight --LumiWeight --SaveTag Final01 --Variation Nominal
#../bin/analysissample --Region CR1 --SubRegion mumu --Type Reco --Process WjetsEl --Selection CutSR --PhMatch TruePh --DoHist --UseWeight --LumiWeight --SaveTag Final01 --Variation Nominal
#../bin/analysissample --Region CR1 --SubRegion mumu --Type Reco --Process WjetsMu --Selection CutSR --PhMatch TruePh --DoHist --UseWeight --LumiWeight --SaveTag Final01 --Variation Nominal
#../bin/analysissample --Region CR1 --SubRegion mumu --Type Reco --Process WjetsTau --Selection CutSR --PhMatch TruePh --DoHist --UseWeight --LumiWeight --SaveTag Final01 --Variation Nominal
#../bin/analysissample --Region CR1 --SubRegion mumu --Type Reco --Process ZGammajetsElElNLO --Selection CutSR --PhMatch TruePh --DoHist --UseWeight --LumiWeight --SaveTag Final01 --Variation Nominal
#../bin/analysissample --Region CR1 --SubRegion mumu --Type Reco --Process ZGammajetsMuMuNLO --Selection CutSR --PhMatch TruePh --DoHist --UseWeight --LumiWeight --SaveTag Final01 --Variation Nominal
#../bin/analysissample --Region CR1 --SubRegion mumu --Type Reco --Process ZGammajetsTauTauNLO --Selection CutSR --PhMatch TruePh --DoHist --UseWeight --LumiWeight --SaveTag Final01 --Variation Nominal
#../bin/analysissample --Region CR1 --SubRegion mumu --Type Reco --Process ZjetsElEl --Selection CutSR --PhMatch TruePh --DoHist --UseWeight --LumiWeight --SaveTag Final01 --Variation Nominal
#../bin/analysissample --Region CR1 --SubRegion mumu --Type Reco --Process ZjetsMuMu --Selection CutSR --PhMatch TruePh --DoHist --UseWeight --LumiWeight --SaveTag Final01 --Variation Nominal
#../bin/analysissample --Region CR1 --SubRegion mumu --Type Reco --Process ZjetsTauTau --Selection CutSR --PhMatch TruePh --DoHist --UseWeight --LumiWeight --SaveTag Final01 --Variation Nominal
#../bin/analysissample --Region CR1 --SubRegion mumu --Type Reco --Process STOthers --Selection CutSR --PhMatch TruePh --DoHist --UseWeight --LumiWeight --SaveTag Final01 --Variation Nominal
#../bin/analysissample --Region CR1 --SubRegion mumu --Type Reco --Process STWT --Selection CutSR --PhMatch TruePh --DoHist --UseWeight --LumiWeight --SaveTag Final01 --Variation Nominal
#../bin/analysissample --Region CR1 --SubRegion mumu --Type Reco --Process Diboson --Selection CutSR --PhMatch TruePh --DoHist --UseWeight --LumiWeight --SaveTag Final01 --Variation Nominal
#
#../bin/analysissample --Region CR1 --SubRegion mumu --Type Reco --Process TTBar --Selection CutSR --PhMatch HFake --DoHist --UseWeight --LumiWeight --SaveTag Final01 --Variation Nominal
#../bin/analysissample --Region CR1 --SubRegion mumu --Type Reco --Process WjetsEl --Selection CutSR --PhMatch HFake --DoHist --UseWeight --LumiWeight --SaveTag Final01 --Variation Nominal
#../bin/analysissample --Region CR1 --SubRegion mumu --Type Reco --Process WjetsMu --Selection CutSR --PhMatch HFake --DoHist --UseWeight --LumiWeight --SaveTag Final01 --Variation Nominal
#../bin/analysissample --Region CR1 --SubRegion mumu --Type Reco --Process WjetsTau --Selection CutSR --PhMatch HFake --DoHist --UseWeight --LumiWeight --SaveTag Final01 --Variation Nominal
#../bin/analysissample --Region CR1 --SubRegion mumu --Type Reco --Process ZjetsElEl --Selection CutSR --PhMatch HFake --DoHist --UseWeight --LumiWeight --SaveTag Final01 --Variation Nominal
#../bin/analysissample --Region CR1 --SubRegion mumu --Type Reco --Process ZjetsMuMu --Selection CutSR --PhMatch HFake --DoHist --UseWeight --LumiWeight --SaveTag Final01 --Variation Nominal
#../bin/analysissample --Region CR1 --SubRegion mumu --Type Reco --Process ZjetsTauTau --Selection CutSR --PhMatch HFake --DoHist --UseWeight --LumiWeight --SaveTag Final01 --Variation Nominal
#../bin/analysissample --Region CR1 --SubRegion mumu --Type Reco --Process STOthers --Selection CutSR --PhMatch HFake --DoHist --UseWeight --LumiWeight --SaveTag Final01 --Variation Nominal
#../bin/analysissample --Region CR1 --SubRegion mumu --Type Reco --Process STWT --Selection CutSR --PhMatch HFake --DoHist --UseWeight --LumiWeight --SaveTag Final01 --Variation Nominal
#../bin/analysissample --Region CR1 --SubRegion mumu --Type Reco --Process Diboson --Selection CutSR --PhMatch HFake --DoHist --UseWeight --LumiWeight --SaveTag Final01 --Variation Nominal
#
#../bin/analysissample --Region CR1 --SubRegion mumu --Type Reco --Process TTBar --Selection CutSR --PhMatch EFake --DoHist --UseWeight --LumiWeight --SaveTag Final01 --Variation Nominal
#../bin/analysissample --Region CR1 --SubRegion mumu --Type Reco --Process WjetsEl --Selection CutSR --PhMatch EFake --DoHist --UseWeight --LumiWeight --SaveTag Final01 --Variation Nominal
#../bin/analysissample --Region CR1 --SubRegion mumu --Type Reco --Process WjetsMu --Selection CutSR --PhMatch EFake --DoHist --UseWeight --LumiWeight --SaveTag Final01 --Variation Nominal
#../bin/analysissample --Region CR1 --SubRegion mumu --Type Reco --Process WjetsTau --Selection CutSR --PhMatch EFake --DoHist --UseWeight --LumiWeight --SaveTag Final01 --Variation Nominal
#../bin/analysissample --Region CR1 --SubRegion mumu --Type Reco --Process ZjetsElEl --Selection CutSR --PhMatch EFake --DoHist --UseWeight --LumiWeight --SaveTag Final01 --Variation Nominal
#../bin/analysissample --Region CR1 --SubRegion mumu --Type Reco --Process ZjetsMuMu --Selection CutSR --PhMatch EFake --DoHist --UseWeight --LumiWeight --SaveTag Final01 --Variation Nominal
#../bin/analysissample --Region CR1 --SubRegion mumu --Type Reco --Process ZjetsTauTau --Selection CutSR --PhMatch EFake --DoHist --UseWeight --LumiWeight --SaveTag Final01 --Variation Nominal
#../bin/analysissample --Region CR1 --SubRegion mumu --Type Reco --Process STOthers --Selection CutSR --PhMatch EFake --DoHist --UseWeight --LumiWeight --SaveTag Final01 --Variation Nominal
#../bin/analysissample --Region CR1 --SubRegion mumu --Type Reco --Process STWT --Selection CutSR --PhMatch EFake --DoHist --UseWeight --LumiWeight --SaveTag Final01 --Variation Nominal
#../bin/analysissample --Region CR1 --SubRegion mumu --Type Reco --Process Diboson --Selection CutSR --PhMatch EFake --DoHist --UseWeight --LumiWeight --SaveTag Final01 --Variation Nominal
