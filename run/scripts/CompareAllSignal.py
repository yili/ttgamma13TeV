import os

ToCompare = [ 

	['ErrorBar DrawRatio DoNorm', 
	 'LeadPhPtCone', 
	 'LeadPhPtCone_CR1_emu_Reco_Signal_Nominal LeadPhPtCone_CR1_emu_Reco_Signal_Nominal LeadPhPtCone_CR1_emu_Reco_TTBar_Nominal',
	 'results/Results_Signal_Reco_FULL_13TeV_CR1_emu_CutSRRawNoIsoPt2030_PhMatch_TruePh_Lumiweighted_V010.02.root results/Results_Signal_Reco_Old_FULL_13TeV_CR1_emu_CutSRRawNoIsoPt2030_PhMatch_TruePh_Lumiweighted_NoKfac_V010.02.root results/Results_TTBar_Reco_FULL_13TeV_CR1_emu_CutSRRawNoIsoPt2030_PhMatch_TruePh_Lumiweighted_V010.02.root',
	 'tty(new) tty(old) tt', 
	 'SR/emu', 
	 'Signal_LeadPhPtCone_TruePh', 
	 '0.5', '1.5'], 

	['ErrorBar DrawRatio DoNorm', 
	 'LeadPhMCElDr', 
	 'LeadPhMCElDr_CR1_emu_Reco_Signal_Nominal LeadPhMCElDr_CR1_emu_Reco_Signal_Nominal LeadPhMCElDr_CR1_emu_Reco_TTBar_Nominal',
	 'results/Results_Signal_Reco_FULL_13TeV_CR1_emu_CutSRRawNoIsoPt2030_PhMatch_TruePh_Lumiweighted_V010.02.root results/Results_Signal_Reco_Old_FULL_13TeV_CR1_emu_CutSRRawNoIsoPt2030_PhMatch_TruePh_Lumiweighted_NoKfac_V010.02.root results/Results_TTBar_Reco_FULL_13TeV_CR1_emu_CutSRRawNoIsoPt2030_PhMatch_TruePh_Lumiweighted_V010.02.root',
	 'tty(new) tty(old) tt', 
	 'SR/emu', 
	 'Signal_LeadPhMCElDr_TruePh', 
	 '0.5', '1.5'], 

	['ErrorBar DrawRatio DoNorm', 
	 'LeadPhMinDrPhLepCoarse2', 
	 'LeadPhMinDrPhLepCoarse2_CR1_emu_Reco_Signal_Nominal LeadPhMinDrPhLepCoarse2_CR1_emu_Reco_Signal_Nominal LeadPhMinDrPhLepCoarse2_CR1_emu_Reco_TTBar_Nominal',
	 'results/Results_Signal_Reco_FULL_13TeV_CR1_emu_CutSRRawNoIsoPt2030_PhMatch_TruePh_Lumiweighted_V010.02.root results/Results_Signal_Reco_Old_FULL_13TeV_CR1_emu_CutSRRawNoIsoPt2030_PhMatch_TruePh_Lumiweighted_NoKfac_V010.02.root results/Results_TTBar_Reco_FULL_13TeV_CR1_emu_CutSRRawNoIsoPt2030_PhMatch_TruePh_Lumiweighted_V010.02.root',
	 'tty(new) tty(old) tt', 
	 'SR/emu', 
	 'Signal_LeadPhMinDrPhLepCoarse2_TruePh', 
	 '0.5', '1.5'], 

	['ErrorBar DrawRatio DoNorm', 
	 'LeadPhType', 
	 'LeadPhType_CR1_emu_Reco_Signal_Nominal LeadPhType_CR1_emu_Reco_Signal_Nominal LeadPhType_CR1_emu_Reco_TTBar_Nominal',
	 'results/Results_Signal_Reco_FULL_13TeV_CR1_emu_CutSRRawNoIsoPt2030_PhMatch_TruePh_Lumiweighted_V010.02.root results/Results_Signal_Reco_Old_FULL_13TeV_CR1_emu_CutSRRawNoIsoPt2030_PhMatch_TruePh_Lumiweighted_NoKfac_V010.02.root results/Results_TTBar_Reco_FULL_13TeV_CR1_emu_CutSRRawNoIsoPt2030_PhMatch_TruePh_Lumiweighted_V010.02.root',
	 'tty(new) tty(old) tt', 
	 'SR/emu', 
	 'Signal_LeadPhType_TruePh', 
	 '0.5', '1.5'], 

	['ErrorBar DrawRatio DoNorm', 
	 'LeadPhOrigin', 
	 'LeadPhOrigin_CR1_emu_Reco_Signal_Nominal LeadPhOrigin_CR1_emu_Reco_Signal_Nominal LeadPhOrigin_CR1_emu_Reco_TTBar_Nominal',
	 'results/Results_Signal_Reco_FULL_13TeV_CR1_emu_CutSRRawNoIsoPt2030_PhMatch_TruePh_Lumiweighted_V010.02.root results/Results_Signal_Reco_Old_FULL_13TeV_CR1_emu_CutSRRawNoIsoPt2030_PhMatch_TruePh_Lumiweighted_NoKfac_V010.02.root results/Results_TTBar_Reco_FULL_13TeV_CR1_emu_CutSRRawNoIsoPt2030_PhMatch_TruePh_Lumiweighted_V010.02.root',
	 'tty(new) tty(old) tt', 
	 'SR/emu', 
	 'Signal_LeadPhOrigin_TruePh', 
	 '0.5', '1.5'], 

	['ErrorBar DrawRatio DoNorm', 
	 'PileUpMu', 
	 'PileUpMu_CR1_emu_Reco_Signal_Nominal PileUpMu_CR1_emu_Reco_Signal_Nominal PileUpMu_CR1_emu_Reco_TTBar_Nominal',
	 'results/Results_Signal_Reco_FULL_13TeV_CR1_emu_CutSRRawNoIsoPt2030_PhMatch_TruePh_Lumiweighted_V010.02.root results/Results_Signal_Reco_Old_FULL_13TeV_CR1_emu_CutSRRawNoIsoPt2030_PhMatch_TruePh_Lumiweighted_NoKfac_V010.02.root results/Results_TTBar_Reco_FULL_13TeV_CR1_emu_CutSRRawNoIsoPt2030_PhMatch_TruePh_Lumiweighted_V010.02.root',
	 'tty(new) tty(old) tt', 
	 'SR/emu', 
	 'Signal_PileUpMu_TruePh', 
	 '0.5', '1.5'], 

	['ErrorBar DrawRatio DoNorm', 
	 'MLepLep2', 
	 'MLepLep2_CR1_emu_Reco_Signal_Nominal MLepLep2_CR1_emu_Reco_Signal_Nominal MLepLep2_CR1_emu_Reco_TTBar_Nominal',
	 'results/Results_Signal_Reco_FULL_13TeV_CR1_emu_CutSRRawNoIsoPt2030_PhMatch_TruePh_Lumiweighted_V010.02.root results/Results_Signal_Reco_Old_FULL_13TeV_CR1_emu_CutSRRawNoIsoPt2030_PhMatch_TruePh_Lumiweighted_NoKfac_V010.02.root results/Results_TTBar_Reco_FULL_13TeV_CR1_emu_CutSRRawNoIsoPt2030_PhMatch_TruePh_Lumiweighted_V010.02.root',
	 'tty(new) tty(old) tt', 
	 'SR/emu', 
	 'Signal_MLepLep2_TruePh', 
	 '0.5', '1.5'], 

	['ErrorBar DrawRatio DoNorm', 
	 'MPhLepLep2', 
	 'MPhLepLep2_CR1_emu_Reco_Signal_Nominal MPhLepLep2_CR1_emu_Reco_Signal_Nominal MPhLepLep2_CR1_emu_Reco_TTBar_Nominal',
	 'results/Results_Signal_Reco_FULL_13TeV_CR1_emu_CutSRRawNoIsoPt2030_PhMatch_TruePh_Lumiweighted_V010.02.root results/Results_Signal_Reco_Old_FULL_13TeV_CR1_emu_CutSRRawNoIsoPt2030_PhMatch_TruePh_Lumiweighted_NoKfac_V010.02.root results/Results_TTBar_Reco_FULL_13TeV_CR1_emu_CutSRRawNoIsoPt2030_PhMatch_TruePh_Lumiweighted_V010.02.root',
	 'tty(new) tty(old) tt', 
	 'SR/emu', 
	 'Signal_MPhLepLep2_TruePh', 
	 '0.5', '1.5'], 

	['ErrorBar DrawRatio DoNorm', 
	 'MWT2', 
	 'MWT2_CR1_emu_Reco_Signal_Nominal MWT2_CR1_emu_Reco_Signal_Nominal MWT2_CR1_emu_Reco_TTBar_Nominal',
	 'results/Results_Signal_Reco_FULL_13TeV_CR1_emu_CutSRRawNoIsoPt2030_PhMatch_TruePh_Lumiweighted_V010.02.root results/Results_Signal_Reco_Old_FULL_13TeV_CR1_emu_CutSRRawNoIsoPt2030_PhMatch_TruePh_Lumiweighted_NoKfac_V010.02.root results/Results_TTBar_Reco_FULL_13TeV_CR1_emu_CutSRRawNoIsoPt2030_PhMatch_TruePh_Lumiweighted_V010.02.root',
	 'tty(new) tty(old) tt', 
	 'SR/emu', 
	 'Signal_MWT2_TruePh', 
	 '0.5', '1.5'], 

	['ErrorBar DrawRatio DoNorm', 
	 'dPhiLepMET2', 
	 'dPhiLepMET2_CR1_emu_Reco_Signal_Nominal dPhiLepMET2_CR1_emu_Reco_Signal_Nominal dPhiLepMET2_CR1_emu_Reco_TTBar_Nominal',
	 'results/Results_Signal_Reco_FULL_13TeV_CR1_emu_CutSRRawNoIsoPt2030_PhMatch_TruePh_Lumiweighted_V010.02.root results/Results_Signal_Reco_Old_FULL_13TeV_CR1_emu_CutSRRawNoIsoPt2030_PhMatch_TruePh_Lumiweighted_NoKfac_V010.02.root results/Results_TTBar_Reco_FULL_13TeV_CR1_emu_CutSRRawNoIsoPt2030_PhMatch_TruePh_Lumiweighted_V010.02.root',
	 'tty(new) tty(old) tt', 
	 'SR/emu', 
	 'Signal_dPhiLepMET2_TruePh', 
	 '0.5', '1.5'], 

	['ErrorBar DrawRatio DoNorm', 
	 'Nbjet', 
	 'Nbjet_CR1_emu_Reco_Signal_Nominal Nbjet_CR1_emu_Reco_Signal_Nominal Nbjet_CR1_emu_Reco_TTBar_Nominal',
	 'results/Results_Signal_Reco_FULL_13TeV_CR1_emu_CutSRRawNoIsoPt2030_PhMatch_TruePh_Lumiweighted_V010.02.root results/Results_Signal_Reco_Old_FULL_13TeV_CR1_emu_CutSRRawNoIsoPt2030_PhMatch_TruePh_Lumiweighted_NoKfac_V010.02.root results/Results_TTBar_Reco_FULL_13TeV_CR1_emu_CutSRRawNoIsoPt2030_PhMatch_TruePh_Lumiweighted_V010.02.root',
	 'tty(new) tty(old) tt', 
	 'SR/emu', 
	 'Signal_Nbjet_TruePh', 
	 '0.5', '1.5'], 

	['ErrorBar DrawRatio DoNorm', 
	 'Njet', 
	 'Njet_CR1_emu_Reco_Signal_Nominal Njet_CR1_emu_Reco_Signal_Nominal Njet_CR1_emu_Reco_TTBar_Nominal',
	 'results/Results_Signal_Reco_FULL_13TeV_CR1_emu_CutSRRawNoIsoPt2030_PhMatch_TruePh_Lumiweighted_V010.02.root results/Results_Signal_Reco_Old_FULL_13TeV_CR1_emu_CutSRRawNoIsoPt2030_PhMatch_TruePh_Lumiweighted_NoKfac_V010.02.root results/Results_TTBar_Reco_FULL_13TeV_CR1_emu_CutSRRawNoIsoPt2030_PhMatch_TruePh_Lumiweighted_V010.02.root',
	 'tty(new) tty(old) tt', 
	 'SR/emu', 
	 'Signal_Njet_TruePh', 
	 '0.5', '1.5'], 

	['ErrorBar DrawRatio DoNorm', 
	 'MET2', 
	 'MET2_CR1_emu_Reco_Signal_Nominal MET2_CR1_emu_Reco_Signal_Nominal MET2_CR1_emu_Reco_TTBar_Nominal',
	 'results/Results_Signal_Reco_FULL_13TeV_CR1_emu_CutSRRawNoIsoPt2030_PhMatch_TruePh_Lumiweighted_V010.02.root results/Results_Signal_Reco_Old_FULL_13TeV_CR1_emu_CutSRRawNoIsoPt2030_PhMatch_TruePh_Lumiweighted_NoKfac_V010.02.root results/Results_TTBar_Reco_FULL_13TeV_CR1_emu_CutSRRawNoIsoPt2030_PhMatch_TruePh_Lumiweighted_V010.02.root',
	 'tty(new) tty(old) tt', 
	 'SR/emu', 
	 'Signal_MET2_TruePh', 
	 '0.5', '1.5'], 

	['ErrorBar DrawRatio DoNorm', 
	 'MPhLep2', 
	 'MPhLep2_CR1_emu_Reco_Signal_Nominal MPhLep2_CR1_emu_Reco_Signal_Nominal MPhLep2_CR1_emu_Reco_TTBar_Nominal',
	 'results/Results_Signal_Reco_FULL_13TeV_CR1_emu_CutSRRawNoIsoPt2030_PhMatch_TruePh_Lumiweighted_V010.02.root results/Results_Signal_Reco_Old_FULL_13TeV_CR1_emu_CutSRRawNoIsoPt2030_PhMatch_TruePh_Lumiweighted_NoKfac_V010.02.root results/Results_TTBar_Reco_FULL_13TeV_CR1_emu_CutSRRawNoIsoPt2030_PhMatch_TruePh_Lumiweighted_V010.02.root',
	 'tty(new) tty(old) tt', 
	 'SR/emu', 
	 'Signal_MPhLep2_TruePh', 
	 '0.5', '1.5'], 

	['ErrorBar DrawRatio DoNorm', 
	 'LeadPhPt2', 
	 'LeadPhPt2_CR1_emu_Reco_Signal_Nominal LeadPhPt2_CR1_emu_Reco_Signal_Nominal LeadPhPt2_CR1_emu_Reco_TTBar_Nominal',
	 'results/Results_Signal_Reco_FULL_13TeV_CR1_emu_CutSRRawNoIsoPt2030_PhMatch_TruePh_Lumiweighted_V010.02.root results/Results_Signal_Reco_Old_FULL_13TeV_CR1_emu_CutSRRawNoIsoPt2030_PhMatch_TruePh_Lumiweighted_NoKfac_V010.02.root results/Results_TTBar_Reco_FULL_13TeV_CR1_emu_CutSRRawNoIsoPt2030_PhMatch_TruePh_Lumiweighted_V010.02.root',
	 'tty(new) tty(old) tt', 
	 'SR/emu', 
	 'Signal_LeadPhPt2_TruePh', 
	 '0.5', '1.5'], 

	['ErrorBar DrawRatio DoNorm', 
	 'M2J2', 
	 'M2J2_CR1_emu_Reco_Signal_Nominal M2J2_CR1_emu_Reco_Signal_Nominal M2J2_CR1_emu_Reco_TTBar_Nominal',
	 'results/Results_Signal_Reco_FULL_13TeV_CR1_emu_CutSRRawNoIsoPt2030_PhMatch_TruePh_Lumiweighted_V010.02.root results/Results_Signal_Reco_Old_FULL_13TeV_CR1_emu_CutSRRawNoIsoPt2030_PhMatch_TruePh_Lumiweighted_NoKfac_V010.02.root results/Results_TTBar_Reco_FULL_13TeV_CR1_emu_CutSRRawNoIsoPt2030_PhMatch_TruePh_Lumiweighted_V010.02.root',
	 'tty(new) tty(old) tt', 
	 'SR/emu', 
	 'Signal_M2J2_TruePh', 
	 '0.5', '1.5'], 


	]

for tocompare in ToCompare:
    option = "_Options " 
    option += tocompare[0]
    title = "_XTitle "
    title += tocompare[1]
    hists = "_HistList "
    hists += tocompare[2]
    files = "_FileList "
    files += tocompare[3]
    lgs = "_Titles "
    lgs += tocompare[4]
    channel = "_Channel "
    channel += tocompare[5]
    save = "_Save "
    save += tocompare[6]
    ratiolo = "_RatioLo "
    ratiolo += tocompare[7]
    ratiohi = "_RatioHi "
    ratiohi += tocompare[8]
    
    run = "cd ../../config;"
    run += "echo \"" + option + "\" > 13TeV_compare_shape_tmp.cfg;"
    run += "echo \"" + title + "\" >> 13TeV_compare_shape_tmp.cfg;"
    run += "echo \"" + hists + "\" >> 13TeV_compare_shape_tmp.cfg;"
    run += "echo \"" + files + "\" >> 13TeV_compare_shape_tmp.cfg;"
    run += "echo \"" + lgs + "\" >> 13TeV_compare_shape_tmp.cfg;"
    run += "echo \"" + channel + "\" >> 13TeV_compare_shape_tmp.cfg;"
    run += "echo \"" + save + "\" >> 13TeV_compare_shape_tmp.cfg;"
    run += "echo \"" + ratiolo + "\" >> 13TeV_compare_shape_tmp.cfg;"
    run += "echo \"" + ratiohi + "\" >> 13TeV_compare_shape_tmp.cfg;"
    run += "cd ../run; ../bin/compareshapes ../config/13TeV_compare_shape_tmp.cfg;"
    success = os.system(run)

    if success !=  0:
        break

    run = "rm ../../config/13TeV_compare_shape_tmp.cfg;"
    os.system(run)
