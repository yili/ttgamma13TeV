Channel="EJ"
Region="EF2"
SubRegion="zeg"
Processes="ZjetsElEl ZGammajetsElEl WGammajetsEl TTBar Signal"
#Processes="TTBar"
Type="Reco"
Cut="CutZeg"
Save="Nominal"
PhMatch="1 2 3"

echo "general configuration"
echo "---------------------"
echo "Channel: "$Channel
echo "Region: "$Region
echo "SubRegion: "$SubRegion
echo "Cut: "$Cut
echo "Save Tag: "$Save
echo "---------------------"
echo ""

for proc in $Processes
do
    if [ "$proc" == "-" ]; then
	echo running on data

	cd /afs/cern.ch/work/y/yili/private/MySVN/Code/TTG/config/
	cp 13TeV_analysis_sample_proto.cfg 13TeV_analysis_sample_data.cfg
	sed -i "s/#KEY_DOPHMATCH//g" 13TeV_analysis_sample_data.cfg
	sed -i "s/#KEY_PHMATCH//g" 13TeV_analysis_sample_data.cfg
	sed -i "s/#KEY_CHANNEL/$Channel/g" 13TeV_analysis_sample_data.cfg
	sed -i "s/#KEY_REGION/$Region/g" 13TeV_analysis_sample_data.cfg
	sed -i "s/#KEY_SUBREGION/$SubRegion/g" 13TeV_analysis_sample_data.cfg
	sed -i "s/#KEY_PROCESS//g" 13TeV_analysis_sample_data.cfg
	sed -i "s/#KEY_TYPE/Data/g" 13TeV_analysis_sample_data.cfg
	sed -i "s/#KEY_SIMULATION//g" 13TeV_analysis_sample_data.cfg
	sed -i "s/#KEY_CUT/$Cut/g" 13TeV_analysis_sample_data.cfg
	sed -i "s/#KEY_SAVE/$Save/g" 13TeV_analysis_sample_data.cfg

	cd /afs/cern.ch/work/y/yili/private/MySVN/Code/TTG/run/
	../bin/analysissample ../config/13TeV_analysis_sample_data.cfg
	succeed=`echo $?`
	
	if [ "$succeed" != "0" ]; then
	   echo analysis of data failed!
	   exit
	fi
	
	rm ../config/13TeV_analysis_sample_data.cfg
    else
	echo running on $proc

	Simulation="FULL"
	if [ "$proc" == "Signal" ]; then
	    Simulation="AFII"
	fi

	if [ "$PhMatch" == "" ]; then
	    echo without photon match 
	    
	    cd /afs/cern.ch/work/y/yili/private/MySVN/Code/TTG/config/
	    cp 13TeV_analysis_sample_proto.cfg 13TeV_analysis_sample_"$proc"_nophmatch.cfg
	    sed -i "s/#KEY_DOPHMATCH//g" 13TeV_analysis_sample_"$proc"_nophmatch.cfg
	    sed -i "s/#KEY_PHMATCH//g" 13TeV_analysis_sample_"$proc"_nophmatch.cfg
	    sed -i "s/#KEY_CHANNEL/$Channel/g" 13TeV_analysis_sample_"$proc"_nophmatch.cfg
	    sed -i "s/#KEY_REGION/$Region/g" 13TeV_analysis_sample_"$proc"_nophmatch.cfg
	    sed -i "s/#KEY_SUBREGION/$SubRegion/g" 13TeV_analysis_sample_"$proc"_nophmatch.cfg
	    sed -i "s/#KEY_PROCESS/$proc/g" 13TeV_analysis_sample_"$proc"_nophmatch.cfg
	    sed -i "s/#KEY_TYPE/$Type/g" 13TeV_analysis_sample_"$proc"_nophmatch.cfg
	    sed -i "s/#KEY_SIMULATION/$Simulation/g" 13TeV_analysis_sample_"$proc"_nophmatch.cfg
	    sed -i "s/#KEY_CUT/$Cut/g" 13TeV_analysis_sample_"$proc"_nophmatch.cfg
	    sed -i "s/#KEY_SAVE/$Save/g" 13TeV_analysis_sample_"$proc"_nophmatch.cfg
	    
	    cd /afs/cern.ch/work/y/yili/private/MySVN/Code/TTG/run/
	    ../bin/analysissample ../config/13TeV_analysis_sample_"$proc"_nophmatch.cfg
	    succeed=`echo $?`
	    
	    if [ "$succeed" != "0" ]; then
	       echo analysis of $proc with phmatch $phmatch failed!
	       exit
	    fi
	    
	    rm ../config/13TeV_analysis_sample_"$proc"_nophmatch.cfg
	else
	    for phmatch in $PhMatch
	    do
	        echo with photon match type $phmatch

	        cd /afs/cern.ch/work/y/yili/private/MySVN/Code/TTG/config/
	        cp 13TeV_analysis_sample_proto.cfg 13TeV_analysis_sample_"$proc"_phmatch"$phmatch".cfg
	        sed -i "s/#KEY_DOPHMATCH/PhMatch/g" 13TeV_analysis_sample_"$proc"_phmatch"$phmatch".cfg
	        sed -i "s/#KEY_PHMATCH/$phmatch/g" 13TeV_analysis_sample_"$proc"_phmatch"$phmatch".cfg
	        sed -i "s/#KEY_CHANNEL/$Channel/g" 13TeV_analysis_sample_"$proc"_phmatch"$phmatch".cfg
	        sed -i "s/#KEY_REGION/$Region/g" 13TeV_analysis_sample_"$proc"_phmatch"$phmatch".cfg
	        sed -i "s/#KEY_SUBREGION/$SubRegion/g" 13TeV_analysis_sample_"$proc"_phmatch"$phmatch".cfg
	        sed -i "s/#KEY_PROCESS/$proc/g" 13TeV_analysis_sample_"$proc"_phmatch"$phmatch".cfg
	        sed -i "s/#KEY_TYPE/$Type/g" 13TeV_analysis_sample_"$proc"_phmatch"$phmatch".cfg
	        sed -i "s/#KEY_SIMULATION/$Simulation/g" 13TeV_analysis_sample_"$proc"_phmatch"$phmatch".cfg
	        sed -i "s/#KEY_CUT/$Cut/g" 13TeV_analysis_sample_"$proc"_phmatch"$phmatch".cfg
	        sed -i "s/#KEY_SAVE/$Save/g" 13TeV_analysis_sample_"$proc"_phmatch"$phmatch".cfg

	        cd /afs/cern.ch/work/y/yili/private/MySVN/Code/TTG/run/
	        ../bin/analysissample ../config/13TeV_analysis_sample_"$proc"_phmatch"$phmatch".cfg
	        succeed=`echo $?`
	        
	        if [ "$succeed" != "0" ]; then
	           echo analysis of $proc with phmatch $phmatch failed!
	           exit
	        fi
	        
	        rm ../config/13TeV_analysis_sample_"$proc"_phmatch"$phmatch".cfg
	    done
	fi
    fi
done
