import os

# v1 nominal

Save = "V007.04"
gen_option = "DoHist UseWeight LumiWeight"
ToRun = [ 

	['', 'CR1','ejets','CutCR','','','QCD',''], 
	['', 'CR1','mujets','CutCR','','','QCD',''], 
	['UsePS', 'CR1','mujets','CutCR','','','QCD',''], 

	['', 'CR1','ejets','CutCR','','','Data',''], 
        ['PhMatch', 'CR1','ejets','CutCR','TruePh','Signal','Reco','FULL'], 
        ['PhMatch', 'CR1','ejets','CutCR','HFake','TTBar','Reco','FULL'], 
        ['PhMatch', 'CR1','ejets','CutCR','EFake','TTBar','Reco','FULL'], 
        ['PhMatch', 'CR1','ejets','CutCR','TruePh','WGammajetsElNLO','Reco','FULL'], 
        ['PhMatch', 'CR1','ejets','CutCR','TruePh','WGammajetsMuNLO','Reco','FULL'], 
        ['PhMatch', 'CR1','ejets','CutCR','TruePh','WGammajetsTauNLO','Reco','FULL'], 
        ['PhMatch', 'CR1','ejets','CutCR','HFake','WjetsEl','Reco','FULL'], 
        ['PhMatch', 'CR1','ejets','CutCR','HFake','WjetsMu','Reco','FULL'], 
        ['PhMatch', 'CR1','ejets','CutCR','HFake','WjetsTau','Reco','FULL'], 
        ['PhMatch', 'CR1','ejets','CutCR','TruePh','ZGammajetsElElNLO','Reco','FULL'], 
        ['PhMatch', 'CR1','ejets','CutCR','TruePh','ZGammajetsMuMuNLO','Reco','FULL'], 
        ['PhMatch', 'CR1','ejets','CutCR','TruePh','ZGammajetsTauTau','Reco','FULL'], 
        ['PhMatch', 'CR1','ejets','CutCR','HFake','ZjetsElEl','Reco','FULL'], 
        ['PhMatch', 'CR1','ejets','CutCR','HFake','ZjetsMuMu','Reco','FULL'], 
        ['PhMatch', 'CR1','ejets','CutCR','HFake','ZjetsTauTau','Reco','FULL'], 
        ['PhMatch', 'CR1','ejets','CutCR','EFake','ZjetsElEl','Reco','FULL'], 
        ['PhMatch', 'CR1','ejets','CutCR','EFake','ZjetsMuMu','Reco','FULL'], 
        ['PhMatch', 'CR1','ejets','CutCR','EFake','ZjetsTauTau','Reco','FULL'], 
	['', 'CR1','ejets','CutCR','','Diboson','Reco','FULL'], 
	['', 'CR1','ejets','CutCR','','STOthers','Reco','FULL'], 
	['', 'CR1','ejets','CutCR','','STWT','Reco','FULL'], 

	['', 'CR1','mujets','CutCR','','','Data',''], 
        ['PhMatch', 'CR1','mujets','CutCR','TruePh','Signal','Reco','FULL'], 
        ['PhMatch', 'CR1','mujets','CutCR','HFake','TTBar','Reco','FULL'], 
        ['PhMatch', 'CR1','mujets','CutCR','EFake','TTBar','Reco','FULL'], 
        ['PhMatch', 'CR1','mujets','CutCR','TruePh','WGammajetsElNLO','Reco','FULL'], 
        ['PhMatch', 'CR1','mujets','CutCR','TruePh','WGammajetsMuNLO','Reco','FULL'], 
        ['PhMatch', 'CR1','mujets','CutCR','TruePh','WGammajetsTauNLO','Reco','FULL'], 
        ['PhMatch', 'CR1','mujets','CutCR','HFake','WjetsEl','Reco','FULL'], 
        ['PhMatch', 'CR1','mujets','CutCR','HFake','WjetsMu','Reco','FULL'], 
        ['PhMatch', 'CR1','mujets','CutCR','HFake','WjetsTau','Reco','FULL'], 
        ['PhMatch', 'CR1','mujets','CutCR','TruePh','ZGammajetsElElNLO','Reco','FULL'], 
        ['PhMatch', 'CR1','mujets','CutCR','TruePh','ZGammajetsMuMuNLO','Reco','FULL'], 
        ['PhMatch', 'CR1','mujets','CutCR','TruePh','ZGammajetsTauTau','Reco','FULL'], 
        ['PhMatch', 'CR1','mujets','CutCR','HFake','ZjetsElEl','Reco','FULL'], 
        ['PhMatch', 'CR1','mujets','CutCR','HFake','ZjetsMuMu','Reco','FULL'], 
        ['PhMatch', 'CR1','mujets','CutCR','HFake','ZjetsTauTau','Reco','FULL'], 
        ['PhMatch', 'CR1','mujets','CutCR','EFake','ZjetsElEl','Reco','FULL'], 
        ['PhMatch', 'CR1','mujets','CutCR','EFake','ZjetsMuMu','Reco','FULL'], 
        ['PhMatch', 'CR1','mujets','CutCR','EFake','ZjetsTauTau','Reco','FULL'], 
	['', 'CR1','mujets','CutCR','','Diboson','Reco','FULL'], 
	['', 'CR1','mujets','CutCR','','STOthers','Reco','FULL'], 
	['', 'CR1','mujets','CutCR','','STWT','Reco','FULL'], 

	]

for torun in ToRun:
    option = gen_option
    option += torun[0]
    region = torun[1]
    subregion = torun[2]
    cut = torun[3]
    phmatch = torun[4] 
    process = torun[5]
    Type = torun[6]
    simu = torun[7]

    run = "cd /afs/cern.ch/work/y/yili/private/MySVN/Code/TTG/config/;"
    run += "cp 13TeV_analysis_sample_proto.cfg 13TeV_analysis_sample_tmp.cfg;"
    run += "sed -i \"s/#KEY_OPTION/" + option + "/g\" 13TeV_analysis_sample_tmp.cfg;"
    run += "sed -i \"s/#KEY_PHMATCH/" + phmatch + "/g\" 13TeV_analysis_sample_tmp.cfg;"
    run += "sed -i \"s/#KEY_REGION/" + region + "/g\" 13TeV_analysis_sample_tmp.cfg;"
    run += "sed -i \"s/#KEY_SUBREGION/" + subregion + "/g\" 13TeV_analysis_sample_tmp.cfg;"
    run += "sed -i \"s/#KEY_PROCESS/" + process + "/g\" 13TeV_analysis_sample_tmp.cfg;"
    run += "sed -i \"s/#KEY_TYPE/" + Type + "/g\" 13TeV_analysis_sample_tmp.cfg;"
    run += "sed -i \"s/#KEY_SIMULATION/" + simu + "/g\" 13TeV_analysis_sample_tmp.cfg;"
    run += "sed -i \"s/#KEY_CUT/" + cut + "/g\" 13TeV_analysis_sample_tmp.cfg;"
    run += "sed -i \"s/#KEY_ADDTAG//g\" 13TeV_analysis_sample_tmp.cfg;"
    run += "sed -i \"s/#KEY_SAVE/" + Save + "/g\" 13TeV_analysis_sample_tmp.cfg;"
    #run += "sed -i \"s/#KEY_TEST/" + test + "/g\" 13TeV_analysis_sample_tmp.cfg;"
    #run += "sed -i \"s/#KEY_DEBUG/" + debug + "/g\" 13TeV_analysis_sample_tmp.cfg;"
    run += "cd /afs/cern.ch/work/y/yili/private/MySVN/Code/TTG/run/;"
    run += "../bin/analysissample ../config/13TeV_analysis_sample_tmp.cfg;"
    success = os.system(run)

    if success !=  0:
        break

    run = "rm ../../config/13TeV_analysis_sample_tmp.cfg;"
    os.system(run)
