import os

ToCompare = [ 

	['ErrorBar DrawRatio', 
	 'Ph Type', 
	 'LeadPhType_CR1_ee_Reco_ZjetsElEl_Nominal LeadPhType_CR1_ee_Reco_ZjetsElEl_Nominal',
	 'results/Results_ZjetsElEl_Reco_FULL_13TeV_CR1_ee_CutZGammaVR_Lumiweighted_V010.01.root results/Results_ZjetsElEl_Reco_FULL_13TeV_CR1_ee_CutZGammaVR_PhMatch_HFake_Lumiweighted_V010.01.root',
	 'All HFake', 
	 'ZGamma_Type', 
	 'ZGamma_LeadPhType_HFake_All', 
	 '0.5', '1.5'], 

	['ErrorBar DrawRatio', 
	 'Ph Origin', 
	 'LeadPhOrigin_CR1_ee_Reco_ZjetsElEl_Nominal LeadPhOrigin_CR1_ee_Reco_ZjetsElEl_Nominal',
	 'results/Results_ZjetsElEl_Reco_FULL_13TeV_CR1_ee_CutZGammaVR_Lumiweighted_V010.01.root results/Results_ZjetsElEl_Reco_FULL_13TeV_CR1_ee_CutZGammaVR_PhMatch_HFake_Lumiweighted_V010.01.root',
	 'All HFake', 
	 'ZGamma_Origin', 
	 'ZGamma_LeadPhOrigin_HFake_All', 
	 '0.5', '1.5'], 

	]

for tocompare in ToCompare:
    option = "_Options " 
    option += tocompare[0]
    title = "_XTitle "
    title += tocompare[1]
    hists = "_HistList "
    hists += tocompare[2]
    files = "_FileList "
    files += tocompare[3]
    lgs = "_Titles "
    lgs += tocompare[4]
    channel = "_Channel "
    channel += tocompare[5]
    save = "_Save "
    save += tocompare[6]
    ratiolo = "_RatioLo "
    ratiolo += tocompare[7]
    ratiohi = "_RatioHi "
    ratiohi += tocompare[8]
    
    run = "cd ../../config;"
    run += "echo \"" + option + "\" > 13TeV_compare_shape_tmp.cfg;"
    run += "echo \"" + title + "\" >> 13TeV_compare_shape_tmp.cfg;"
    run += "echo \"" + hists + "\" >> 13TeV_compare_shape_tmp.cfg;"
    run += "echo \"" + files + "\" >> 13TeV_compare_shape_tmp.cfg;"
    run += "echo \"" + lgs + "\" >> 13TeV_compare_shape_tmp.cfg;"
    run += "echo \"" + channel + "\" >> 13TeV_compare_shape_tmp.cfg;"
    run += "echo \"" + save + "\" >> 13TeV_compare_shape_tmp.cfg;"
    run += "echo \"" + ratiolo + "\" >> 13TeV_compare_shape_tmp.cfg;"
    run += "echo \"" + ratiohi + "\" >> 13TeV_compare_shape_tmp.cfg;"
    run += "cd ../run; ../bin/compareshapes ../config/13TeV_compare_shape_tmp.cfg;"
    success = os.system(run)

    if success !=  0:
        break

    run = "rm ../../config/13TeV_compare_shape_tmp.cfg;"
    os.system(run)
