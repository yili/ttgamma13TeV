cd /afs/cern.ch/work/y/yili/private/MySVN/Code/TTG/run/

../bin/egammafakestudy_sf --config ../config/13TeV_egammafake_study_proto.cfg --gaus --pt --eta
#../bin/egammafakestudy_sf --config ../config/13TeV_egammafake_study_proto.cfg --gaus --pt --eta --diffmean
../bin/egammafakestudy_sf --config ../config/13TeV_egammafake_study_proto.cfg --gaus --pt --eta --zgamma
../bin/egammafakestudy_sf --config ../config/13TeV_egammafake_study_proto.cfg --gaus --pt --eta --mctemp
../bin/egammafakestudy_sf --config ../config/13TeV_egammafake_study_proto.cfg --gaus --pt --eta --expand
../bin/egammafakestudy_sf --config ../config/13TeV_egammafake_study_proto.cfg --gaus --pt --eta --shrink

../bin/egammafakestudy_sf --config ../config/13TeV_egammafake_study_proto.cfg --gaus --reverse --pt --eta
#../bin/egammafakestudy_sf --config ../config/13TeV_egammafake_study_proto.cfg --gaus --reverse --pt --eta --diffmean
../bin/egammafakestudy_sf --config ../config/13TeV_egammafake_study_proto.cfg --gaus --reverse --pt --eta --zgamma
../bin/egammafakestudy_sf --config ../config/13TeV_egammafake_study_proto.cfg --gaus --reverse --pt --eta --mctemp
../bin/egammafakestudy_sf --config ../config/13TeV_egammafake_study_proto.cfg --gaus --reverse --pt --eta --expand
../bin/egammafakestudy_sf --config ../config/13TeV_egammafake_study_proto.cfg --gaus --reverse --pt --eta --shrink
