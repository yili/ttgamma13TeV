### before do all, run
#   batch/Nominal/ForHLLHCZGamma.py 
#   batch/Nominal/ForHLLHCQCD.py
#   batch/Upgrade/All.py 
#   batch/Upgrade/All_HFake.py 
#   batch/Upgrade/Unfolding.py 
#   to prepare all the inputs

set -e

##HFweight="--HFweight075"
#
cd /afs/cern.ch/work/y/yili/private/Analysis/Siegen13TeVWork/MainCode/run
rm scripts/upgrade/repo/*
#
#### pre-analysis
#../bin/comparecutflow1314 $HFweight
#mv log_cf1 scripts/upgrade/repo/
#mv log_cf11 scripts/upgrade/repo/
#mv log_cf2 scripts/upgrade/repo/
#cp plots/Upgrade/Proc_ratio.pdf scripts/upgrade/repo/
#cp plots/Upgrade/Signal_ratio.pdf scripts/upgrade/repo/
#
#../bin/comparecutflowlargeeta 
#mv log_cf3 scripts/upgrade/repo/
#
####../bin/compareshapes1314signal
#cp plots/Upgrade/CompareShape1314Siganl_NGoodPh_ejets.pdf scripts/upgrade/repo/
#cp plots/Upgrade/CompareShape1314Siganl_Njet_ejets.pdf scripts/upgrade/repo/
#cp plots/Upgrade/CompareSigShapeTruth1314_ph_pt_ljets.pdf scripts/upgrade/repo/
#cp plots/Upgrade/CompareSigShapeTruth1314_ph_pt_ll.pdf scripts/upgrade/repo/
#cp plots/Upgrade/CompareSigEff1314_ph_pt_ljets.pdf scripts/upgrade/repo/
#cp plots/Upgrade/CompareSigEff1314_ph_pt_ll.pdf scripts/upgrade/repo/
#
#../bin/makeinputs $HFweight --pt1000
#../bin/makeinputs $HFweight --emu --pt1000
#
##../bin/derivehfakecorr 
#cp plots/Upgrade/ForHFakeCorr_*.pdf scripts/upgrade/repo/
##
#cd /afs/cern.ch/work/y/yili/private/Analysis/Siegen13TeVWork/MainCode/run
#../bin/drawstack ../config/14TeV_draw_stack_sr_ejets.cfg $HFweight
#../bin/drawstack ../config/14TeV_draw_stack_sr_mujets.cfg $HFweight
#../bin/drawstack ../config/14TeV_draw_stack_sr_ee.cfg $HFweight
#../bin/drawstack ../config/14TeV_draw_stack_sr_emu.cfg $HFweight
#../bin/drawstack ../config/14TeV_draw_stack_sr_mumu.cfg $HFweight
#cp plots/14TeV_Stack_LeadPhPtUnfold2_CutSRUpgrade_UR_ejets_NoData.pdf scripts/upgrade/repo/
#cp plots/14TeV_Stack_LeadPhPtUnfold2_CutSRUpgrade_UR_mujets_NoData.pdf scripts/upgrade/repo/
#cp plots/14TeV_Stack_LeadPhPtUnfold2_CutSRUpgrade_UR_ee_NoData.pdf scripts/upgrade/repo/
#cp plots/14TeV_Stack_LeadPhPtUnfold2_CutSRUpgrade_UR_emu_NoData.pdf scripts/upgrade/repo/
#cp plots/14TeV_Stack_LeadPhPtUnfold2_CutSRUpgrade_UR_mumu_NoData.pdf scripts/upgrade/repo/
#cp plots/14TeV_Stack_LeadPhPtUnfold_CutSRUpgrade_UR_ejets_NoData.pdf scripts/upgrade/repo/
#cp plots/14TeV_Stack_LeadPhPtUnfold_CutSRUpgrade_UR_mujets_NoData.pdf scripts/upgrade/repo/
#cp plots/14TeV_Stack_LeadPhPtUnfold_CutSRUpgrade_UR_ee_NoData.pdf scripts/upgrade/repo/
#cp plots/14TeV_Stack_LeadPhPtUnfold_CutSRUpgrade_UR_emu_NoData.pdf scripts/upgrade/repo/
#cp plots/14TeV_Stack_LeadPhPtUnfold_CutSRUpgrade_UR_mumu_NoData.pdf scripts/upgrade/repo/
#cp plots/14TeV_Stack_LeadPhAbsEtaUnfold_CutSRUpgrade_UR_ejets_NoData.pdf scripts/upgrade/repo/
#cp plots/14TeV_Stack_LeadPhAbsEtaUnfold_CutSRUpgrade_UR_mujets_NoData.pdf scripts/upgrade/repo/
#cp plots/14TeV_Stack_LeadPhAbsEtaUnfold_CutSRUpgrade_UR_ee_NoData.pdf scripts/upgrade/repo/
#cp plots/14TeV_Stack_LeadPhAbsEtaUnfold_CutSRUpgrade_UR_emu_NoData.pdf scripts/upgrade/repo/
#cp plots/14TeV_Stack_LeadPhAbsEtaUnfold_CutSRUpgrade_UR_mumu_NoData.pdf scripts/upgrade/repo/
#cp plots/14TeV_Stack_LeadPhMinDrPhLepUnfold_CutSRUpgrade_UR_ejets_NoData.pdf scripts/upgrade/repo/
#cp plots/14TeV_Stack_LeadPhMinDrPhLepUnfold_CutSRUpgrade_UR_mujets_NoData.pdf scripts/upgrade/repo/
#cp plots/14TeV_Stack_LeadPhMinDrPhLepUnfold_CutSRUpgrade_UR_ee_NoData.pdf scripts/upgrade/repo/
#cp plots/14TeV_Stack_LeadPhMinDrPhLepUnfold_CutSRUpgrade_UR_emu_NoData.pdf scripts/upgrade/repo/
#cp plots/14TeV_Stack_LeadPhMinDrPhLepUnfold_CutSRUpgrade_UR_mumu_NoData.pdf scripts/upgrade/repo/
#cp plots/14TeV_Stack_DEtaLepLepUnfold_CutSRUpgrade_UR_ee_NoData.pdf scripts/upgrade/repo/
#cp plots/14TeV_Stack_DEtaLepLepUnfold_CutSRUpgrade_UR_emu_NoData.pdf scripts/upgrade/repo/
#cp plots/14TeV_Stack_DEtaLepLepUnfold_CutSRUpgrade_UR_mumu_NoData.pdf scripts/upgrade/repo/
#cp plots/14TeV_Stack_DPhiLepLepUnfold_CutSRUpgrade_UR_ee_NoData.pdf scripts/upgrade/repo/
#cp plots/14TeV_Stack_DPhiLepLepUnfold_CutSRUpgrade_UR_emu_NoData.pdf scripts/upgrade/repo/
#cp plots/14TeV_Stack_DPhiLepLepUnfold_CutSRUpgrade_UR_mumu_NoData.pdf scripts/upgrade/repo/

#../bin/calcuefficiency
#mv log_eff scripts/upgrade/repo/
#
#### run TRexFitter 
##
##cd /afs/cern.ch/work/y/yili/private/Analysis/TRexFitter/
##sh run_all.sh
cd /afs/cern.ch/work/y/yili/private/Analysis/Siegen13TeVWork/MainCode/run
#
../bin/comparefiduprecision
##mv log_fidu scripts/upgrade/repo/
cp plots/Upgrade/Uncert_comparison.pdf scripts/upgrade/repo/
##
##cd /afs/cern.ch/work/y/yili/private/Analysis/Siegen13TeVWork/MainCode/run
##../bin/rankingsys
##
##mv log_sysrank_ejets scripts/upgrade/repo/
##mv log_sysrank_mujets scripts/upgrade/repo/
##mv log_sysrank_ee scripts/upgrade/repo/
##mv log_sysrank_emu scripts/upgrade/repo/
##mv log_sysrank_mumu scripts/upgrade/repo/
##mv log_sysrank scripts/upgrade/repo/
#
###### run unfolding code before continue
##
#cd /afs/cern.ch/work/y/yili/private/Analysis/Unfolding_Upgrade
##sh run_all.sh
#cd /afs/cern.ch/work/y/yili/private/Analysis/Siegen13TeVWork/MainCode/run
#
#cp /afs/cern.ch/work/y/yili/private/Analysis/Unfolding_Upgrade/Results_Step2_pt1000/sysplot*.eps scripts/upgrade/repo/
#cp /afs/cern.ch/work/y/yili/private/Analysis/Unfolding_Upgrade/Results_Step2_emuonly_pt1000/sysplot*.eps scripts/upgrade/repo/
#cp /afs/cern.ch/work/y/yili/private/Analysis/Unfolding_Upgrade/Results_Step5/particle_*.eps scripts/upgrade/repo/
#
##### post-analysis
#cd /afs/cern.ch/work/y/yili/private/Analysis/Siegen13TeVWork/MainCode/run
#../bin/comparefiduprecision --diff --chan sinlepton --var ph_pt
#../bin/comparefiduprecision --diff --chan sinlepton --var ph_eta
#../bin/comparefiduprecision --diff --chan sinlepton --var dR_lep_ph
#../bin/comparefiduprecision --diff --chan dilepton --var ph_pt
#../bin/comparefiduprecision --diff --chan dilepton --var ph_eta
#../bin/comparefiduprecision --diff --chan dilepton --var dR_lep_ph
#../bin/comparefiduprecision --diff --chan dilepton --var dEta_lep
#../bin/comparefiduprecision --diff --chan dilepton --var dPhi_lep
#../bin/comparefiduprecision --1414 --diff --chan dilepton --var ph_pt
#../bin/comparefiduprecision --1414 --diff --chan dilepton --var ph_eta
#../bin/comparefiduprecision --1414 --diff --chan dilepton --var dR_lep_ph
#../bin/comparefiduprecision --1414 --diff --chan dilepton --var dEta_lep
#../bin/comparefiduprecision --1414 --diff --chan dilepton --var dPhi_lep
#../bin/comparefiduprecision --1414 --swap --diff --chan dilepton --var ph_pt
#../bin/comparefiduprecision --1414 --swap --diff --chan dilepton --var ph_eta
#../bin/comparefiduprecision --1414 --swap --diff --chan dilepton --var dR_lep_ph
#../bin/comparefiduprecision --1414 --swap --diff --chan dilepton --var dEta_lep
#../bin/comparefiduprecision --1414 --swap --diff --chan dilepton --var dPhi_lep
#../bin/comparefiduprecision --abs --diff --chan sinlepton --var ph_pt
#../bin/comparefiduprecision --abs --diff --chan dilepton --var ph_pt
#../bin/comparefiduprecision --1414 --abs --diff --chan dilepton --var ph_pt
#mv log_500emu scripts/upgrade/repo/
#mv log_500ljets scripts/upgrade/repo/
#cp plots/Upgrade/Uncert_comparison*.pdf scripts/upgrade/repo/
