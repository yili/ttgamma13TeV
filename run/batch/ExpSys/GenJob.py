#!/bin/env python

import os,sys,string,argparse,commands
import shlex
from subprocess import Popen, PIPE

List=[
#"EG_RESOLUTION_ALL__1down",
#"EG_RESOLUTION_ALL__1up",
#"EG_SCALE_ALL__1down",
#"EG_SCALE_ALL__1up",
#"JET_21NP_JET_BJES_Response__1down",
#"JET_21NP_JET_BJES_Response__1up",
#"JET_21NP_JET_EffectiveNP_1__1down",
#"JET_21NP_JET_EffectiveNP_1__1up",
#"JET_21NP_JET_EffectiveNP_2__1down",
#"JET_21NP_JET_EffectiveNP_2__1up",
#"JET_21NP_JET_EffectiveNP_3__1down",
#"JET_21NP_JET_EffectiveNP_3__1up",
#"JET_21NP_JET_EffectiveNP_4__1down",
#"JET_21NP_JET_EffectiveNP_4__1up",
#"JET_21NP_JET_EffectiveNP_5__1down",
#"JET_21NP_JET_EffectiveNP_5__1up",
#"JET_21NP_JET_EffectiveNP_6__1down",
#"JET_21NP_JET_EffectiveNP_6__1up",
#"JET_21NP_JET_EffectiveNP_7__1down",
#"JET_21NP_JET_EffectiveNP_7__1up",
#"JET_21NP_JET_EffectiveNP_8restTerm__1down",
#"JET_21NP_JET_EffectiveNP_8restTerm__1up",
#"JET_21NP_JET_EtaIntercalibration_Modelling__1down",
#"JET_21NP_JET_EtaIntercalibration_Modelling__1up",
#"JET_21NP_JET_EtaIntercalibration_NonClosure__1down",
#"JET_21NP_JET_EtaIntercalibration_NonClosure__1up",
#"JET_21NP_JET_EtaIntercalibration_TotalStat__1down",
#"JET_21NP_JET_EtaIntercalibration_TotalStat__1up",
#"JET_21NP_JET_Flavor_Composition__1down",
#"JET_21NP_JET_Flavor_Composition__1up",
#"JET_21NP_JET_Flavor_Response__1down",
#"JET_21NP_JET_Flavor_Response__1up",
#"JET_21NP_JET_Pileup_OffsetMu__1down",
#"JET_21NP_JET_Pileup_OffsetMu__1up",
#"JET_21NP_JET_Pileup_OffsetNPV__1down",
#"JET_21NP_JET_Pileup_OffsetNPV__1up",
#"JET_21NP_JET_Pileup_PtTerm__1down",
#"JET_21NP_JET_Pileup_PtTerm__1up",
#"JET_21NP_JET_Pileup_RhoTopology__1down",
#"JET_21NP_JET_Pileup_RhoTopology__1up",
#"JET_21NP_JET_PunchThrough_MC15__1down",
#"JET_21NP_JET_PunchThrough_MC15__1up",
#"JET_21NP_JET_SingleParticle_HighPt__1down",
#"JET_21NP_JET_SingleParticle_HighPt__1up",
#"JET_JER_SINGLE_NP__1up",
#"MET_SoftTrk_ResoPara",
#"MET_SoftTrk_ResoPerp",
#"MET_SoftTrk_ScaleDown",
#"MET_SoftTrk_ScaleUp",
#"MUON_ID__1down",
#"MUON_ID__1up",
#"MUON_MS__1down",
#"MUON_MS__1up",
#"MUON_SAGITTA_RESBIAS__1down",
#"MUON_SAGITTA_RESBIAS__1up",
#"MUON_SAGITTA_RHO__1down",
#"MUON_SAGITTA_RHO__1up",
#"MUON_SCALE__1down",
#"MUON_SCALE__1up",
#"PUUp",
#"PUDn",
#"BTagCExtUp",
#"BTagCExtDn",
#"BTagB0Up",
#"BTagB0Dn",
#"BTagB1Up",
#"BTagB1Dn",
#"BTagB2Up",
#"BTagB2Dn",
#"BTagB3Up",
#"BTagB3Dn",
#"BTagB4Up",
#"BTagB4Dn",
#"BTagB5Up",
#"BTagB5Dn",
#"BTagB6Up",
#"BTagB6Dn",
#"BTagB7Up",
#"BTagB7Dn",
#"BTagB8Up",
#"BTagB8Dn",
#"BTagB9Up",
#"BTagB9Dn",
#"BTagB10Up",
#"BTagB10Dn",
"BTagB11Up",
"BTagB11Dn",
"BTagB12Up",
"BTagB12Dn",
"BTagB13Up",
"BTagB13Dn",
"BTagB14Up",
"BTagB14Dn",
"BTagB15Up",
"BTagB15Dn",
"BTagB16Up",
"BTagB16Dn",
"BTagB17Up",
"BTagB17Dn",
"BTagB18Up",
"BTagB18Dn",
"BTagB19Up",
"BTagB19Dn",
"BTagB20Up",
"BTagB20Dn",
"BTagB21Up",
"BTagB21Dn",
"BTagB22Up",
"BTagB22Dn",
"BTagB23Up",
"BTagB23Dn",
"BTagB24Up",
"BTagB24Dn",
"BTagB25Up",
"BTagB25Dn",
"BTagB26Up",
"BTagB26Dn",
"BTagB27Up",
"BTagB27Dn",
"BTagB28Up",
"BTagB28Dn",
"BTagB29Up",
"BTagB29Dn",
#"BTagC0Up",
#"BTagC0Dn",
#"BTagC1Up",
#"BTagC1Dn",
#"BTagC2Up",
#"BTagC2Dn",
#"BTagC3Up",
#"BTagC3Dn",
#"BTagC4Up",
#"BTagC4Dn",
#"BTagC5Up",
#"BTagC5Dn",
#"BTagC6Up",
#"BTagC6Dn",
#"BTagC7Up",
#"BTagC7Dn",
#"BTagC8Up",
#"BTagC8Dn",
#"BTagC9Up",
#"BTagC9Dn",
#"BTagC10Up",
#"BTagC10Dn",
"BTagC11Up",
"BTagC11Dn",
"BTagC12Up",
"BTagC12Dn",
"BTagC13Up",
"BTagC13Dn",
"BTagC14Up",
"BTagC14Dn",
#"BTagL0Up",
#"BTagL0Dn",
#"BTagL1Up",
#"BTagL1Dn",
#"BTagL2Up",
#"BTagL2Dn",
#"BTagL3Up",
#"BTagL3Dn",
#"BTagL4Up",
#"BTagL4Dn",
#"BTagL5Up",
#"BTagL5Dn",
#"BTagL6Up",
#"BTagL6Dn",
#"BTagL7Up",
#"BTagL7Dn",
#"BTagL8Up",
#"BTagL8Dn",
#"BTagL9Up",
#"BTagL9Dn",
#"BTagL10Up",
#"BTagL10Dn",
"BTagL11Up",
"BTagL11Dn",
"BTagL12Up",
"BTagL12Dn",
"BTagL13Up",
"BTagL13Dn",
"BTagL14Up",
"BTagL14Dn",
"BTagL15Up",
"BTagL15Dn",
"BTagL16Up",
"BTagL16Dn",
"BTagL17Up",
"BTagL17Dn",
"BTagL18Up",
"BTagL18Dn",
"BTagL19Up",
"BTagL19Dn",
"BTagL20Up",
"BTagL20Dn",
"BTagL21Up",
"BTagL21Dn",
"BTagL22Up",
"BTagL22Dn",
"BTagL23Up",
"BTagL23Dn",
"BTagL24Up",
"BTagL24Dn",
"BTagL25Up",
"BTagL25Dn",
"BTagL26Up",
"BTagL26Dn",
"BTagL27Up",
"BTagL27Dn",
"BTagL28Up",
"BTagL28Dn",
"BTagL29Up",
"BTagL29Dn",
#"EL_Trigger_Up",
#"EL_Trigger_Dn",
#"EL_Reco_Up",
#"EL_Reco_Dn",
#"EL_ID_Up",
#"EL_ID_Dn",
#"EL_Isol_Up",
#"EL_Isol_Dn",
#"MU_Trigger_STAT_Up",
#"MU_Trigger_STAT_Dn",
#"MU_Trigger_SYST_Up",
#"MU_Trigger_SYST_Dn",
#"MU_ID_STAT_Up",
#"MU_ID_STAT_Dn",
#"MU_ID_SYST_Up",
#"MU_ID_SYST_Dn",
#"MU_ID_STAT_LOWPT_Up",
#"MU_ID_STAT_LOWPT_Dn",
#"MU_ID_SYST_LOWPT_Up",
#"MU_ID_SYST_LOWPT_Dn",
#"MU_Isol_STAT_Up",
#"MU_Isol_STAT_Dn",
#"MU_Isol_SYST_Up",
#"MU_Isol_SYST_Dn",
#"MU_TTVA_STAT_Up",
#"MU_TTVA_STAT_Dn",
#"MU_TTVA_SYST_Up",
#"MU_TTVA_SYST_Dn",
#"JVT_Up",
#"JVT_Dn",
#"PH_ID_Up",
#"PH_ID_Dn",
#"PH_Isol_Up",
#"PH_Isol_Dn",
#"PH_TrkIsol_Up",
#"PH_TrkIsol_Dn",
]     

def getJobDef(name):    

    text = """
#!/bin/bash

#BSUB -J %s
#BSUB -o stdout_%s.out
#BSUB -e stderr_%s.out
#BSUB -q 8nh
#BSUB -u $USER@cern.ch

stagein()
{
  echo "Start";
}

runcode()
{
  echo "Running the code";
  cd /tmp/yili;
  cp /afs/cern.ch/work/y/yili/private/MySVN/Code/TTG/bin/analysissample ./;
  set -x
  sys=%s
  mkdir /afs/cern.ch/work/y/yili/private/MySVN/Code/TTG/run/results/$sys 
  ./analysissample --SaveDir /afs/cern.ch/work/y/yili/private/MySVN/Code/TTG/run/results/$sys/ --Region CR1 --SubRegion ejets --Type Reco --Process Signal --Selection CutSR --PhMatch TruePh --DoHist --UseWeight --LumiWeight --SaveTag Final02 --Variation $sys
  ./analysissample --SaveDir /afs/cern.ch/work/y/yili/private/MySVN/Code/TTG/run/results/$sys/ --Region CR1 --SubRegion ejets --Type Reco --Process WGammajetsElNLO --Selection CutSR --PhMatch TruePh --DoHist --UseWeight --LumiWeight --SaveTag Final02 --Variation $sys
  ./analysissample --SaveDir /afs/cern.ch/work/y/yili/private/MySVN/Code/TTG/run/results/$sys/ --Region CR1 --SubRegion ejets --Type Reco --Process WGammajetsMuNLO --Selection CutSR --PhMatch TruePh --DoHist --UseWeight --LumiWeight --SaveTag Final02 --Variation $sys
  ./analysissample --SaveDir /afs/cern.ch/work/y/yili/private/MySVN/Code/TTG/run/results/$sys/ --Region CR1 --SubRegion ejets --Type Reco --Process WGammajetsTauNLO --Selection CutSR --PhMatch TruePh --DoHist --UseWeight --LumiWeight --SaveTag Final02 --Variation $sys
  ./analysissample --SaveDir /afs/cern.ch/work/y/yili/private/MySVN/Code/TTG/run/results/$sys/ --Region CR1 --SubRegion ejets --Type Reco --Process ZGammajetsElElNLO --Selection CutSR --PhMatch TruePh --DoHist --UseWeight --LumiWeight --SaveTag Final02 --Variation $sys
  ./analysissample --SaveDir /afs/cern.ch/work/y/yili/private/MySVN/Code/TTG/run/results/$sys/ --Region CR1 --SubRegion ejets --Type Reco --Process ZGammajetsMuMuNLO --Selection CutSR --PhMatch TruePh --DoHist --UseWeight --LumiWeight --SaveTag Final02 --Variation $sys
  ./analysissample --SaveDir /afs/cern.ch/work/y/yili/private/MySVN/Code/TTG/run/results/$sys/ --Region CR1 --SubRegion ejets --Type Reco --Process ZGammajetsTauTauNLO --Selection CutSR --PhMatch TruePh --DoHist --UseWeight --LumiWeight --SaveTag Final02 --Variation $sys
  ./analysissample --SaveDir /afs/cern.ch/work/y/yili/private/MySVN/Code/TTG/run/results/$sys/ --Region CR1 --SubRegion ejets --Type Reco --Process STOthers --Selection CutSR --PhMatch TruePh --DoHist --UseWeight --LumiWeight --SaveTag Final02 --Variation $sys
  ./analysissample --SaveDir /afs/cern.ch/work/y/yili/private/MySVN/Code/TTG/run/results/$sys/ --Region CR1 --SubRegion ejets --Type Reco --Process STWT --Selection CutSR --PhMatch TruePh --DoHist --UseWeight --LumiWeight --SaveTag Final02 --Variation $sys
  ./analysissample --SaveDir /afs/cern.ch/work/y/yili/private/MySVN/Code/TTG/run/results/$sys/ --Region CR1 --SubRegion ejets --Type Reco --Process Diboson --Selection CutSR --PhMatch TruePh --DoHist --UseWeight --LumiWeight --SaveTag Final02 --Variation $sys
  
  ./analysissample --SaveDir /afs/cern.ch/work/y/yili/private/MySVN/Code/TTG/run/results/$sys/ --Region CR1 --SubRegion ejets --Type Reco --Process TTBar --Selection CutSR --PhMatch HFake --DoHist --UseWeight --LumiWeight --SaveTag Final02 --Variation $sys
  ./analysissample --SaveDir /afs/cern.ch/work/y/yili/private/MySVN/Code/TTG/run/results/$sys/ --Region CR1 --SubRegion ejets --Type Reco --Process STOthers --Selection CutSR --PhMatch HFake --DoHist --UseWeight --LumiWeight --SaveTag Final02 --Variation $sys
  ./analysissample --SaveDir /afs/cern.ch/work/y/yili/private/MySVN/Code/TTG/run/results/$sys/ --Region CR1 --SubRegion ejets --Type Reco --Process STWT --Selection CutSR --PhMatch HFake --DoHist --UseWeight --LumiWeight --SaveTag Final02 --Variation $sys
  ./analysissample --SaveDir /afs/cern.ch/work/y/yili/private/MySVN/Code/TTG/run/results/$sys/ --Region CR1 --SubRegion ejets --Type Reco --Process Diboson --Selection CutSR --PhMatch HFake --DoHist --UseWeight --LumiWeight --SaveTag Final02 --Variation $sys
  
  ./analysissample --SaveDir /afs/cern.ch/work/y/yili/private/MySVN/Code/TTG/run/results/$sys/ --Region CR1 --SubRegion ejets --Type Reco --Process TTBar --Selection CutSR --PhMatch EFake --DoHist --UseWeight --LumiWeight --SaveTag Final02 --Variation $sys
  ./analysissample --SaveDir /afs/cern.ch/work/y/yili/private/MySVN/Code/TTG/run/results/$sys/ --Region CR1 --SubRegion ejets --Type Reco --Process STOthers --Selection CutSR --PhMatch EFake --DoHist --UseWeight --LumiWeight --SaveTag Final02 --Variation $sys
  ./analysissample --SaveDir /afs/cern.ch/work/y/yili/private/MySVN/Code/TTG/run/results/$sys/ --Region CR1 --SubRegion ejets --Type Reco --Process STWT --Selection CutSR --PhMatch EFake --DoHist --UseWeight --LumiWeight --SaveTag Final02 --Variation $sys
  ./analysissample --SaveDir /afs/cern.ch/work/y/yili/private/MySVN/Code/TTG/run/results/$sys/ --Region CR1 --SubRegion ejets --Type Reco --Process Diboson --Selection CutSR --PhMatch EFake --DoHist --UseWeight --LumiWeight --SaveTag Final02 --Variation $sys

  ./analysissample --SaveDir /afs/cern.ch/work/y/yili/private/MySVN/Code/TTG/run/results/$sys/ --Region CR1 --SubRegion mujets --Type Reco --Process Signal --Selection CutSR --PhMatch TruePh --DoHist --UseWeight --LumiWeight --SaveTag Final02 --Variation $sys
  ./analysissample --SaveDir /afs/cern.ch/work/y/yili/private/MySVN/Code/TTG/run/results/$sys/ --Region CR1 --SubRegion mujets --Type Reco --Process WGammajetsElNLO --Selection CutSR --PhMatch TruePh --DoHist --UseWeight --LumiWeight --SaveTag Final02 --Variation $sys
  ./analysissample --SaveDir /afs/cern.ch/work/y/yili/private/MySVN/Code/TTG/run/results/$sys/ --Region CR1 --SubRegion mujets --Type Reco --Process WGammajetsMuNLO --Selection CutSR --PhMatch TruePh --DoHist --UseWeight --LumiWeight --SaveTag Final02 --Variation $sys
  ./analysissample --SaveDir /afs/cern.ch/work/y/yili/private/MySVN/Code/TTG/run/results/$sys/ --Region CR1 --SubRegion mujets --Type Reco --Process WGammajetsTauNLO --Selection CutSR --PhMatch TruePh --DoHist --UseWeight --LumiWeight --SaveTag Final02 --Variation $sys
  ./analysissample --SaveDir /afs/cern.ch/work/y/yili/private/MySVN/Code/TTG/run/results/$sys/ --Region CR1 --SubRegion mujets --Type Reco --Process ZGammajetsElElNLO --Selection CutSR --PhMatch TruePh --DoHist --UseWeight --LumiWeight --SaveTag Final02 --Variation $sys
  ./analysissample --SaveDir /afs/cern.ch/work/y/yili/private/MySVN/Code/TTG/run/results/$sys/ --Region CR1 --SubRegion mujets --Type Reco --Process ZGammajetsMuMuNLO --Selection CutSR --PhMatch TruePh --DoHist --UseWeight --LumiWeight --SaveTag Final02 --Variation $sys
  ./analysissample --SaveDir /afs/cern.ch/work/y/yili/private/MySVN/Code/TTG/run/results/$sys/ --Region CR1 --SubRegion mujets --Type Reco --Process ZGammajetsTauTauNLO --Selection CutSR --PhMatch TruePh --DoHist --UseWeight --LumiWeight --SaveTag Final02 --Variation $sys
  ./analysissample --SaveDir /afs/cern.ch/work/y/yili/private/MySVN/Code/TTG/run/results/$sys/ --Region CR1 --SubRegion mujets --Type Reco --Process STOthers --Selection CutSR --PhMatch TruePh --DoHist --UseWeight --LumiWeight --SaveTag Final02 --Variation $sys
  ./analysissample --SaveDir /afs/cern.ch/work/y/yili/private/MySVN/Code/TTG/run/results/$sys/ --Region CR1 --SubRegion mujets --Type Reco --Process STWT --Selection CutSR --PhMatch TruePh --DoHist --UseWeight --LumiWeight --SaveTag Final02 --Variation $sys
  ./analysissample --SaveDir /afs/cern.ch/work/y/yili/private/MySVN/Code/TTG/run/results/$sys/ --Region CR1 --SubRegion mujets --Type Reco --Process Diboson --Selection CutSR --PhMatch TruePh --DoHist --UseWeight --LumiWeight --SaveTag Final02 --Variation $sys
  
  ./analysissample --SaveDir /afs/cern.ch/work/y/yili/private/MySVN/Code/TTG/run/results/$sys/ --Region CR1 --SubRegion mujets --Type Reco --Process TTBar --Selection CutSR --PhMatch HFake --DoHist --UseWeight --LumiWeight --SaveTag Final02 --Variation $sys
  ./analysissample --SaveDir /afs/cern.ch/work/y/yili/private/MySVN/Code/TTG/run/results/$sys/ --Region CR1 --SubRegion mujets --Type Reco --Process STOthers --Selection CutSR --PhMatch HFake --DoHist --UseWeight --LumiWeight --SaveTag Final02 --Variation $sys
  ./analysissample --SaveDir /afs/cern.ch/work/y/yili/private/MySVN/Code/TTG/run/results/$sys/ --Region CR1 --SubRegion mujets --Type Reco --Process STWT --Selection CutSR --PhMatch HFake --DoHist --UseWeight --LumiWeight --SaveTag Final02 --Variation $sys
  ./analysissample --SaveDir /afs/cern.ch/work/y/yili/private/MySVN/Code/TTG/run/results/$sys/ --Region CR1 --SubRegion mujets --Type Reco --Process Diboson --Selection CutSR --PhMatch HFake --DoHist --UseWeight --LumiWeight --SaveTag Final02 --Variation $sys
  
  ./analysissample --SaveDir /afs/cern.ch/work/y/yili/private/MySVN/Code/TTG/run/results/$sys/ --Region CR1 --SubRegion mujets --Type Reco --Process TTBar --Selection CutSR --PhMatch EFake --DoHist --UseWeight --LumiWeight --SaveTag Final02 --Variation $sys
  ./analysissample --SaveDir /afs/cern.ch/work/y/yili/private/MySVN/Code/TTG/run/results/$sys/ --Region CR1 --SubRegion mujets --Type Reco --Process STOthers --Selection CutSR --PhMatch EFake --DoHist --UseWeight --LumiWeight --SaveTag Final02 --Variation $sys
  ./analysissample --SaveDir /afs/cern.ch/work/y/yili/private/MySVN/Code/TTG/run/results/$sys/ --Region CR1 --SubRegion mujets --Type Reco --Process STWT --Selection CutSR --PhMatch EFake --DoHist --UseWeight --LumiWeight --SaveTag Final02 --Variation $sys
  ./analysissample --SaveDir /afs/cern.ch/work/y/yili/private/MySVN/Code/TTG/run/results/$sys/ --Region CR1 --SubRegion mujets --Type Reco --Process Diboson --Selection CutSR --PhMatch EFake --DoHist --UseWeight --LumiWeight --SaveTag Final02 --Variation $sys

  ./analysissample --SaveDir /afs/cern.ch/work/y/yili/private/MySVN/Code/TTG/run/results/$sys/ --Region CR1 --SubRegion ee --Type Reco --Process Signal --Selection CutSR --PhMatch TruePh --DoHist --UseWeight --LumiWeight --SaveTag Final02 --Variation $sys
  ./analysissample --SaveDir /afs/cern.ch/work/y/yili/private/MySVN/Code/TTG/run/results/$sys/ --Region CR1 --SubRegion ee --Type Reco --Process WGammajetsElNLO --Selection CutSR --PhMatch TruePh --DoHist --UseWeight --LumiWeight --SaveTag Final02 --Variation $sys
  ./analysissample --SaveDir /afs/cern.ch/work/y/yili/private/MySVN/Code/TTG/run/results/$sys/ --Region CR1 --SubRegion ee --Type Reco --Process WGammajetsMuNLO --Selection CutSR --PhMatch TruePh --DoHist --UseWeight --LumiWeight --SaveTag Final02 --Variation $sys
  ./analysissample --SaveDir /afs/cern.ch/work/y/yili/private/MySVN/Code/TTG/run/results/$sys/ --Region CR1 --SubRegion ee --Type Reco --Process WGammajetsTauNLO --Selection CutSR --PhMatch TruePh --DoHist --UseWeight --LumiWeight --SaveTag Final02 --Variation $sys
  ./analysissample --SaveDir /afs/cern.ch/work/y/yili/private/MySVN/Code/TTG/run/results/$sys/ --Region CR1 --SubRegion ee --Type Reco --Process ZGammajetsElElNLO --Selection CutSR --PhMatch TruePh --DoHist --UseWeight --LumiWeight --SaveTag Final02 --Variation $sys
  ./analysissample --SaveDir /afs/cern.ch/work/y/yili/private/MySVN/Code/TTG/run/results/$sys/ --Region CR1 --SubRegion ee --Type Reco --Process ZGammajetsMuMuNLO --Selection CutSR --PhMatch TruePh --DoHist --UseWeight --LumiWeight --SaveTag Final02 --Variation $sys
  ./analysissample --SaveDir /afs/cern.ch/work/y/yili/private/MySVN/Code/TTG/run/results/$sys/ --Region CR1 --SubRegion ee --Type Reco --Process ZGammajetsTauTauNLO --Selection CutSR --PhMatch TruePh --DoHist --UseWeight --LumiWeight --SaveTag Final02 --Variation $sys
  ./analysissample --SaveDir /afs/cern.ch/work/y/yili/private/MySVN/Code/TTG/run/results/$sys/ --Region CR1 --SubRegion ee --Type Reco --Process STOthers --Selection CutSR --PhMatch TruePh --DoHist --UseWeight --LumiWeight --SaveTag Final02 --Variation $sys
  ./analysissample --SaveDir /afs/cern.ch/work/y/yili/private/MySVN/Code/TTG/run/results/$sys/ --Region CR1 --SubRegion ee --Type Reco --Process STWT --Selection CutSR --PhMatch TruePh --DoHist --UseWeight --LumiWeight --SaveTag Final02 --Variation $sys
  ./analysissample --SaveDir /afs/cern.ch/work/y/yili/private/MySVN/Code/TTG/run/results/$sys/ --Region CR1 --SubRegion ee --Type Reco --Process Diboson --Selection CutSR --PhMatch TruePh --DoHist --UseWeight --LumiWeight --SaveTag Final02 --Variation $sys
  
  ./analysissample --SaveDir /afs/cern.ch/work/y/yili/private/MySVN/Code/TTG/run/results/$sys/ --Region CR1 --SubRegion ee --Type Reco --Process TTBar --Selection CutSR --PhMatch HFake --DoHist --UseWeight --LumiWeight --SaveTag Final02 --Variation $sys
  ./analysissample --SaveDir /afs/cern.ch/work/y/yili/private/MySVN/Code/TTG/run/results/$sys/ --Region CR1 --SubRegion ee --Type Reco --Process STOthers --Selection CutSR --PhMatch HFake --DoHist --UseWeight --LumiWeight --SaveTag Final02 --Variation $sys
  ./analysissample --SaveDir /afs/cern.ch/work/y/yili/private/MySVN/Code/TTG/run/results/$sys/ --Region CR1 --SubRegion ee --Type Reco --Process STWT --Selection CutSR --PhMatch HFake --DoHist --UseWeight --LumiWeight --SaveTag Final02 --Variation $sys
  ./analysissample --SaveDir /afs/cern.ch/work/y/yili/private/MySVN/Code/TTG/run/results/$sys/ --Region CR1 --SubRegion ee --Type Reco --Process Diboson --Selection CutSR --PhMatch HFake --DoHist --UseWeight --LumiWeight --SaveTag Final02 --Variation $sys
  
  ./analysissample --SaveDir /afs/cern.ch/work/y/yili/private/MySVN/Code/TTG/run/results/$sys/ --Region CR1 --SubRegion ee --Type Reco --Process TTBar --Selection CutSR --PhMatch EFake --DoHist --UseWeight --LumiWeight --SaveTag Final02 --Variation $sys
  ./analysissample --SaveDir /afs/cern.ch/work/y/yili/private/MySVN/Code/TTG/run/results/$sys/ --Region CR1 --SubRegion ee --Type Reco --Process STOthers --Selection CutSR --PhMatch EFake --DoHist --UseWeight --LumiWeight --SaveTag Final02 --Variation $sys
  ./analysissample --SaveDir /afs/cern.ch/work/y/yili/private/MySVN/Code/TTG/run/results/$sys/ --Region CR1 --SubRegion ee --Type Reco --Process STWT --Selection CutSR --PhMatch EFake --DoHist --UseWeight --LumiWeight --SaveTag Final02 --Variation $sys
  ./analysissample --SaveDir /afs/cern.ch/work/y/yili/private/MySVN/Code/TTG/run/results/$sys/ --Region CR1 --SubRegion ee --Type Reco --Process Diboson --Selection CutSR --PhMatch EFake --DoHist --UseWeight --LumiWeight --SaveTag Final02 --Variation $sys

  ./analysissample --SaveDir /afs/cern.ch/work/y/yili/private/MySVN/Code/TTG/run/results/$sys/ --Region CR1 --SubRegion mumu --Type Reco --Process Signal --Selection CutSR --PhMatch TruePh --DoHist --UseWeight --LumiWeight --SaveTag Final02 --Variation $sys
  ./analysissample --SaveDir /afs/cern.ch/work/y/yili/private/MySVN/Code/TTG/run/results/$sys/ --Region CR1 --SubRegion mumu --Type Reco --Process WGammajetsElNLO --Selection CutSR --PhMatch TruePh --DoHist --UseWeight --LumiWeight --SaveTag Final02 --Variation $sys
  ./analysissample --SaveDir /afs/cern.ch/work/y/yili/private/MySVN/Code/TTG/run/results/$sys/ --Region CR1 --SubRegion mumu --Type Reco --Process WGammajetsMuNLO --Selection CutSR --PhMatch TruePh --DoHist --UseWeight --LumiWeight --SaveTag Final02 --Variation $sys
  ./analysissample --SaveDir /afs/cern.ch/work/y/yili/private/MySVN/Code/TTG/run/results/$sys/ --Region CR1 --SubRegion mumu --Type Reco --Process WGammajetsTauNLO --Selection CutSR --PhMatch TruePh --DoHist --UseWeight --LumiWeight --SaveTag Final02 --Variation $sys
  ./analysissample --SaveDir /afs/cern.ch/work/y/yili/private/MySVN/Code/TTG/run/results/$sys/ --Region CR1 --SubRegion mumu --Type Reco --Process ZGammajetsElElNLO --Selection CutSR --PhMatch TruePh --DoHist --UseWeight --LumiWeight --SaveTag Final02 --Variation $sys
  ./analysissample --SaveDir /afs/cern.ch/work/y/yili/private/MySVN/Code/TTG/run/results/$sys/ --Region CR1 --SubRegion mumu --Type Reco --Process ZGammajetsMuMuNLO --Selection CutSR --PhMatch TruePh --DoHist --UseWeight --LumiWeight --SaveTag Final02 --Variation $sys
  ./analysissample --SaveDir /afs/cern.ch/work/y/yili/private/MySVN/Code/TTG/run/results/$sys/ --Region CR1 --SubRegion mumu --Type Reco --Process ZGammajetsTauTauNLO --Selection CutSR --PhMatch TruePh --DoHist --UseWeight --LumiWeight --SaveTag Final02 --Variation $sys
  ./analysissample --SaveDir /afs/cern.ch/work/y/yili/private/MySVN/Code/TTG/run/results/$sys/ --Region CR1 --SubRegion mumu --Type Reco --Process STOthers --Selection CutSR --PhMatch TruePh --DoHist --UseWeight --LumiWeight --SaveTag Final02 --Variation $sys
  ./analysissample --SaveDir /afs/cern.ch/work/y/yili/private/MySVN/Code/TTG/run/results/$sys/ --Region CR1 --SubRegion mumu --Type Reco --Process STWT --Selection CutSR --PhMatch TruePh --DoHist --UseWeight --LumiWeight --SaveTag Final02 --Variation $sys
  ./analysissample --SaveDir /afs/cern.ch/work/y/yili/private/MySVN/Code/TTG/run/results/$sys/ --Region CR1 --SubRegion mumu --Type Reco --Process Diboson --Selection CutSR --PhMatch TruePh --DoHist --UseWeight --LumiWeight --SaveTag Final02 --Variation $sys
  
  ./analysissample --SaveDir /afs/cern.ch/work/y/yili/private/MySVN/Code/TTG/run/results/$sys/ --Region CR1 --SubRegion mumu --Type Reco --Process TTBar --Selection CutSR --PhMatch HFake --DoHist --UseWeight --LumiWeight --SaveTag Final02 --Variation $sys
  ./analysissample --SaveDir /afs/cern.ch/work/y/yili/private/MySVN/Code/TTG/run/results/$sys/ --Region CR1 --SubRegion mumu --Type Reco --Process STOthers --Selection CutSR --PhMatch HFake --DoHist --UseWeight --LumiWeight --SaveTag Final02 --Variation $sys
  ./analysissample --SaveDir /afs/cern.ch/work/y/yili/private/MySVN/Code/TTG/run/results/$sys/ --Region CR1 --SubRegion mumu --Type Reco --Process STWT --Selection CutSR --PhMatch HFake --DoHist --UseWeight --LumiWeight --SaveTag Final02 --Variation $sys
  ./analysissample --SaveDir /afs/cern.ch/work/y/yili/private/MySVN/Code/TTG/run/results/$sys/ --Region CR1 --SubRegion mumu --Type Reco --Process Diboson --Selection CutSR --PhMatch HFake --DoHist --UseWeight --LumiWeight --SaveTag Final02 --Variation $sys
  
  ./analysissample --SaveDir /afs/cern.ch/work/y/yili/private/MySVN/Code/TTG/run/results/$sys/ --Region CR1 --SubRegion mumu --Type Reco --Process TTBar --Selection CutSR --PhMatch EFake --DoHist --UseWeight --LumiWeight --SaveTag Final02 --Variation $sys
  ./analysissample --SaveDir /afs/cern.ch/work/y/yili/private/MySVN/Code/TTG/run/results/$sys/ --Region CR1 --SubRegion mumu --Type Reco --Process STOthers --Selection CutSR --PhMatch EFake --DoHist --UseWeight --LumiWeight --SaveTag Final02 --Variation $sys
  ./analysissample --SaveDir /afs/cern.ch/work/y/yili/private/MySVN/Code/TTG/run/results/$sys/ --Region CR1 --SubRegion mumu --Type Reco --Process STWT --Selection CutSR --PhMatch EFake --DoHist --UseWeight --LumiWeight --SaveTag Final02 --Variation $sys
  ./analysissample --SaveDir /afs/cern.ch/work/y/yili/private/MySVN/Code/TTG/run/results/$sys/ --Region CR1 --SubRegion mumu --Type Reco --Process Diboson --Selection CutSR --PhMatch EFake --DoHist --UseWeight --LumiWeight --SaveTag Final02 --Variation $sys

  ./analysissample --SaveDir /afs/cern.ch/work/y/yili/private/MySVN/Code/TTG/run/results/$sys/ --Region CR1 --SubRegion emu --Type Reco --Process Signal --Selection CutSR --PhMatch TruePh --DoHist --UseWeight --LumiWeight --SaveTag Final02 --Variation $sys
  ./analysissample --SaveDir /afs/cern.ch/work/y/yili/private/MySVN/Code/TTG/run/results/$sys/ --Region CR1 --SubRegion emu --Type Reco --Process WGammajetsElNLO --Selection CutSR --PhMatch TruePh --DoHist --UseWeight --LumiWeight --SaveTag Final02 --Variation $sys
  ./analysissample --SaveDir /afs/cern.ch/work/y/yili/private/MySVN/Code/TTG/run/results/$sys/ --Region CR1 --SubRegion emu --Type Reco --Process WGammajetsMuNLO --Selection CutSR --PhMatch TruePh --DoHist --UseWeight --LumiWeight --SaveTag Final02 --Variation $sys
  ./analysissample --SaveDir /afs/cern.ch/work/y/yili/private/MySVN/Code/TTG/run/results/$sys/ --Region CR1 --SubRegion emu --Type Reco --Process WGammajetsTauNLO --Selection CutSR --PhMatch TruePh --DoHist --UseWeight --LumiWeight --SaveTag Final02 --Variation $sys
  ./analysissample --SaveDir /afs/cern.ch/work/y/yili/private/MySVN/Code/TTG/run/results/$sys/ --Region CR1 --SubRegion emu --Type Reco --Process ZGammajetsElElNLO --Selection CutSR --PhMatch TruePh --DoHist --UseWeight --LumiWeight --SaveTag Final02 --Variation $sys
  ./analysissample --SaveDir /afs/cern.ch/work/y/yili/private/MySVN/Code/TTG/run/results/$sys/ --Region CR1 --SubRegion emu --Type Reco --Process ZGammajetsMuMuNLO --Selection CutSR --PhMatch TruePh --DoHist --UseWeight --LumiWeight --SaveTag Final02 --Variation $sys
  ./analysissample --SaveDir /afs/cern.ch/work/y/yili/private/MySVN/Code/TTG/run/results/$sys/ --Region CR1 --SubRegion emu --Type Reco --Process ZGammajetsTauTauNLO --Selection CutSR --PhMatch TruePh --DoHist --UseWeight --LumiWeight --SaveTag Final02 --Variation $sys
  ./analysissample --SaveDir /afs/cern.ch/work/y/yili/private/MySVN/Code/TTG/run/results/$sys/ --Region CR1 --SubRegion emu --Type Reco --Process STOthers --Selection CutSR --PhMatch TruePh --DoHist --UseWeight --LumiWeight --SaveTag Final02 --Variation $sys
  ./analysissample --SaveDir /afs/cern.ch/work/y/yili/private/MySVN/Code/TTG/run/results/$sys/ --Region CR1 --SubRegion emu --Type Reco --Process STWT --Selection CutSR --PhMatch TruePh --DoHist --UseWeight --LumiWeight --SaveTag Final02 --Variation $sys
  ./analysissample --SaveDir /afs/cern.ch/work/y/yili/private/MySVN/Code/TTG/run/results/$sys/ --Region CR1 --SubRegion emu --Type Reco --Process Diboson --Selection CutSR --PhMatch TruePh --DoHist --UseWeight --LumiWeight --SaveTag Final02 --Variation $sys
  
  ./analysissample --SaveDir /afs/cern.ch/work/y/yili/private/MySVN/Code/TTG/run/results/$sys/ --Region CR1 --SubRegion emu --Type Reco --Process TTBar --Selection CutSR --PhMatch HFake --DoHist --UseWeight --LumiWeight --SaveTag Final02 --Variation $sys
  ./analysissample --SaveDir /afs/cern.ch/work/y/yili/private/MySVN/Code/TTG/run/results/$sys/ --Region CR1 --SubRegion emu --Type Reco --Process STOthers --Selection CutSR --PhMatch HFake --DoHist --UseWeight --LumiWeight --SaveTag Final02 --Variation $sys
  ./analysissample --SaveDir /afs/cern.ch/work/y/yili/private/MySVN/Code/TTG/run/results/$sys/ --Region CR1 --SubRegion emu --Type Reco --Process STWT --Selection CutSR --PhMatch HFake --DoHist --UseWeight --LumiWeight --SaveTag Final02 --Variation $sys
  ./analysissample --SaveDir /afs/cern.ch/work/y/yili/private/MySVN/Code/TTG/run/results/$sys/ --Region CR1 --SubRegion emu --Type Reco --Process Diboson --Selection CutSR --PhMatch HFake --DoHist --UseWeight --LumiWeight --SaveTag Final02 --Variation $sys
  
  ./analysissample --SaveDir /afs/cern.ch/work/y/yili/private/MySVN/Code/TTG/run/results/$sys/ --Region CR1 --SubRegion emu --Type Reco --Process TTBar --Selection CutSR --PhMatch EFake --DoHist --UseWeight --LumiWeight --SaveTag Final02 --Variation $sys
  ./analysissample --SaveDir /afs/cern.ch/work/y/yili/private/MySVN/Code/TTG/run/results/$sys/ --Region CR1 --SubRegion emu --Type Reco --Process STOthers --Selection CutSR --PhMatch EFake --DoHist --UseWeight --LumiWeight --SaveTag Final02 --Variation $sys
  ./analysissample --SaveDir /afs/cern.ch/work/y/yili/private/MySVN/Code/TTG/run/results/$sys/ --Region CR1 --SubRegion emu --Type Reco --Process STWT --Selection CutSR --PhMatch EFake --DoHist --UseWeight --LumiWeight --SaveTag Final02 --Variation $sys
  ./analysissample --SaveDir /afs/cern.ch/work/y/yili/private/MySVN/Code/TTG/run/results/$sys/ --Region CR1 --SubRegion emu --Type Reco --Process Diboson --Selection CutSR --PhMatch EFake --DoHist --UseWeight --LumiWeight --SaveTag Final02 --Variation $sys
}

stageout()
{
  echo "Finished";
}

stagein
runcode
stageout

exit
""" % (name, name, name, name) 

    return text

def submitJob(name):

    bsubFile = open( name + ".bsub", "w")          
    text = getJobDef(name)            
    bsubFile.write(text)
    bsubFile.close()            
    os.system("chmod 775 -R *")            

for term in List:
    submitJob(term)
    command="bsub -q 8nh "+ term + ".bsub"
    print command
    os.system(command)

