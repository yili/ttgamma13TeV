#!/bin/env python

import os,sys,string,argparse,commands
import shlex
from subprocess import Popen, PIPE

List=[
["TTBar", "ejets", "HFake", "TrueLp", "--HFakeReweight"],
["Zjets", "ejets", "HFake", "TrueLp", "--HFakeReweight"],
["Wjets", "ejets", "HFake", "TrueLp", "--HFakeReweight"],
["ST", "ejets", "HFake", "TrueLp", "--HFakeReweight"],
["Diboson", "ejets", "HFake", "TrueLp", "--HFakeReweight"],

["TTBar", "mujets", "HFake", "TrueLp", "--HFakeReweight"],
["Zjets", "mujets", "HFake", "TrueLp", "--HFakeReweight"],
["Wjets", "mujets", "HFake", "TrueLp", "--HFakeReweight"],
["ST", "mujets", "HFake", "TrueLp", "--HFakeReweight"],
["Diboson", "mujets", "HFake", "TrueLp", "--HFakeReweight"],

["TTBar", "ee", "HFake", "TrueLp", "--HFakeReweight"],
["Zjets", "ee", "HFake", "TrueLp", "--HFakeReweight"],
["ST", "ee", "HFake", "TrueLp", "--HFakeReweight"],
["Diboson", "ee", "HFake", "TrueLp", "--HFakeReweight"],

["TTBar", "mumu", "HFake", "TrueLp", "--HFakeReweight"],
["Zjets", "mumu", "HFake", "TrueLp", "--HFakeReweight"],
["ST", "mumu", "HFake", "TrueLp", "--HFakeReweight"],
["Diboson", "mumu", "HFake", "TrueLp", "--HFakeReweight"],

["TTBar", "emu", "HFake", "TrueLp", "--HFakeReweight"],
["ST", "emu", "HFake", "TrueLp", "--HFakeReweight"],
["Diboson", "emu", "HFake", "TrueLp", "--HFakeReweight"],

#["TTBar", "ejets", "HFake", "TrueLp", "--HFakeReweight025"],
#["Zjets", "ejets", "HFake", "TrueLp", "--HFakeReweight025"],
#["Wjets", "ejets", "HFake", "TrueLp", "--HFakeReweight025"],
#["ST", "ejets", "HFake", "TrueLp", "--HFakeReweight025"],
#["Diboson", "ejets", "HFake", "TrueLp", "--HFakeReweight025"],
#
#["TTBar", "mujets", "HFake", "TrueLp", "--HFakeReweight025"],
#["Zjets", "mujets", "HFake", "TrueLp", "--HFakeReweight025"],
#["Wjets", "mujets", "HFake", "TrueLp", "--HFakeReweight025"],
#["ST", "mujets", "HFake", "TrueLp", "--HFakeReweight025"],
#["Diboson", "mujets", "HFake", "TrueLp", "--HFakeReweight025"],
#
#["TTBar", "ee", "HFake", "TrueLp", "--HFakeReweight025"],
#["Zjets", "ee", "HFake", "TrueLp", "--HFakeReweight025"],
#["ST", "ee", "HFake", "TrueLp", "--HFakeReweight025"],
#["Diboson", "ee", "HFake", "TrueLp", "--HFakeReweight025"],
#
#["TTBar", "mumu", "HFake", "TrueLp", "--HFakeReweight025"],
#["Zjets", "mumu", "HFake", "TrueLp", "--HFakeReweight025"],
#["ST", "mumu", "HFake", "TrueLp", "--HFakeReweight025"],
#["Diboson", "mumu", "HFake", "TrueLp", "--HFakeReweight025"],
#
#["TTBar", "emu", "HFake", "TrueLp", "--HFakeReweight025"],
#["ST", "emu", "HFake", "TrueLp", "--HFakeReweight025"],
#["Diboson", "emu", "HFake", "TrueLp", "--HFakeReweight025"],
#
#["TTBar", "ejets", "HFake", "TrueLp", "--HFakeReweight05"],
#["Zjets", "ejets", "HFake", "TrueLp", "--HFakeReweight05"],
#["Wjets", "ejets", "HFake", "TrueLp", "--HFakeReweight05"],
#["ST", "ejets", "HFake", "TrueLp", "--HFakeReweight05"],
#["Diboson", "ejets", "HFake", "TrueLp", "--HFakeReweight05"],
#
#["TTBar", "mujets", "HFake", "TrueLp", "--HFakeReweight05"],
#["Zjets", "mujets", "HFake", "TrueLp", "--HFakeReweight05"],
#["Wjets", "mujets", "HFake", "TrueLp", "--HFakeReweight05"],
#["ST", "mujets", "HFake", "TrueLp", "--HFakeReweight05"],
#["Diboson", "mujets", "HFake", "TrueLp", "--HFakeReweight05"],
#
#["TTBar", "ee", "HFake", "TrueLp", "--HFakeReweight05"],
#["Zjets", "ee", "HFake", "TrueLp", "--HFakeReweight05"],
#["ST", "ee", "HFake", "TrueLp", "--HFakeReweight05"],
#["Diboson", "ee", "HFake", "TrueLp", "--HFakeReweight05"],
#
#["TTBar", "mumu", "HFake", "TrueLp", "--HFakeReweight05"],
#["Zjets", "mumu", "HFake", "TrueLp", "--HFakeReweight05"],
#["ST", "mumu", "HFake", "TrueLp", "--HFakeReweight05"],
#["Diboson", "mumu", "HFake", "TrueLp", "--HFakeReweight05"],
#
#["TTBar", "emu", "HFake", "TrueLp", "--HFakeReweight05"],
#["ST", "emu", "HFake", "TrueLp", "--HFakeReweight05"],
#["Diboson", "emu", "HFake", "TrueLp", "--HFakeReweight05"],
#
#["TTBar", "ejets", "HFake", "TrueLp", "--HFakeReweight075"],
#["Zjets", "ejets", "HFake", "TrueLp", "--HFakeReweight075"],
#["Wjets", "ejets", "HFake", "TrueLp", "--HFakeReweight075"],
#["ST", "ejets", "HFake", "TrueLp", "--HFakeReweight075"],
#["Diboson", "ejets", "HFake", "TrueLp", "--HFakeReweight075"],
#
#["TTBar", "mujets", "HFake", "TrueLp", "--HFakeReweight075"],
#["Zjets", "mujets", "HFake", "TrueLp", "--HFakeReweight075"],
#["Wjets", "mujets", "HFake", "TrueLp", "--HFakeReweight075"],
#["ST", "mujets", "HFake", "TrueLp", "--HFakeReweight075"],
#["Diboson", "mujets", "HFake", "TrueLp", "--HFakeReweight075"],
#
#["TTBar", "ee", "HFake", "TrueLp", "--HFakeReweight075"],
#["Zjets", "ee", "HFake", "TrueLp", "--HFakeReweight075"],
#["ST", "ee", "HFake", "TrueLp", "--HFakeReweight075"],
#["Diboson", "ee", "HFake", "TrueLp", "--HFakeReweight075"],
#
#["TTBar", "mumu", "HFake", "TrueLp", "--HFakeReweight075"],
#["Zjets", "mumu", "HFake", "TrueLp", "--HFakeReweight075"],
#["ST", "mumu", "HFake", "TrueLp", "--HFakeReweight075"],
#["Diboson", "mumu", "HFake", "TrueLp", "--HFakeReweight075"],
#
#["TTBar", "emu", "HFake", "TrueLp", "--HFakeReweight075"],
#["ST", "emu", "HFake", "TrueLp", "--HFakeReweight075"],
#["Diboson", "emu", "HFake", "TrueLp", "--HFakeReweight075"],
]     

def getJobDef(sample, channel, phtype, lptype, hfakeweight):    

    text = """
#!/bin/bash

stagein()
{
  echo "Start";
}

runcode()
{
  echo "Running the code";
  cd /afs/cern.ch/work/y/yili/private/Analysis/Siegen13TeVWork/MainCode/run;
  set -x

  ../bin/analysissample --SaveDir results/Upgrade/ --Region UR --Process %s --Type Upgrade --SubRegion %s --Selection CutSRUpgrade --DoHist --UseWeight --LumiWeight --SaveTag U08 --Variation Nominal --PhMatch %s --LpMatch %s --CME 14TeV %s
}

stageout()
{
  echo "Finished";
}

stagein
runcode
stageout

exit
""" % (sample, channel, phtype, lptype, hfakeweight) 
#""" % (sample, channel, phtype, lptype, sample, channel, phtype, lptype, sample, channel, phtype, lptype) 

    return text

def submitJob(sample, channel, phtype, lptype, hfakeweight):

    bsubFile = open( sample +"." + channel + "." + phtype + "." + lptype + "." + hfakeweight + ".bsub", "w")          
    text = getJobDef(sample, channel, phtype, lptype, hfakeweight)            
    bsubFile.write(text)
    bsubFile.close()            
    os.system("chmod 775 -R *")            

for term in List:
    submitJob(term[0], term[1], term[2], term[3], term[4])
    command="bsub -q 8nh "+ term[0]+"." + term[1] + "." + term[2] + "." + term[3] + "." + term[4] + ".bsub"
    print command
    os.system(command)

