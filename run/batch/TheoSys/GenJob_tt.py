#!/bin/env python

import os,sys,string,argparse,commands
import shlex
from subprocess import Popen, PIPE

List=[
"Nominal",
"R1F2",
"R2F1",
"R2F05",
"R05F2",
"R05F1",
"R05F05",
"R1F05",
"R2F2",
"PDF25",
"PDF24",
"PDF27",
"PDF26",
"PDF21",
"PDF20",
"PDF23",
"PDF22",
"PDF29",
"PDF28",
"PDF36",
"PDF34",
"PDF35",
"PDF32",
"PDF33",
"PDF30",
"PDF31",
"PDF38",
"PDF39",
"PDF83",
"PDF82",
"PDF81",
"PDF80",
"PDF87",
"PDF100",
"PDF85",
"PDF84",
"PDF89",
"PDF88",
"PDF69",
"PDF68",
"PDF61",
"PDF60",
"PDF63",
"PDF62",
"PDF65",
"PDF64",
"PDF67",
"PDF66",
"PDF8",
"PDF9",
"PDF6",
"PDF7",
"PDF4",
"PDF5",
"PDF2",
"PDF3",
"PDF1",
"PDF94",
"PDF95",
"PDF96",
"PDF97",
"PDF90",
"PDF91",
"PDF92",
"PDF93",
"PDF98",
"PDF99",
"PDF18",
"PDF19",
"PDF14",
"PDF15",
"PDF16",
"PDF17",
"PDF10",
"PDF11",
"PDF12",
"PDF13",
"PDF78",
"PDF79",
"PDF72",
"PDF73",
"PDF70",
"PDF71",
"PDF76",
"PDF77",
"PDF74",
"PDF75",
"PDF86",
"PDF37",
"PDF49",
"PDF48",
"PDF47",
"PDF46",
"PDF45",
"PDF44",
"PDF43",
"PDF42",
"PDF41",
"PDF40",
"PDF50",
"PDF51",
"PDF52",
"PDF53",
"PDF54",
"PDF55",
"PDF56",
"PDF57",
"PDF58",
"PDF59",
]     

def getJobDef(name):    

    text = """
#!/bin/bash

#BSUB -J %s
#BSUB -o stdout_%s.out
#BSUB -e stderr_%s.out
#BSUB -q 8nh
#BSUB -u $USER@cern.ch

stagein()
{
  echo "Start";
}

runcode()
{
  echo "Running the code";
  cd /tmp/yili;
  cp /afs/cern.ch/work/y/yili/private/MySVN/Code/TTG/bin/analysissample ./;
  set -x
  variation=%s
  mkdir /afs/cern.ch/work/y/yili/private/MySVN/Code/TTG/run/results/$variation
  ./analysissample --SaveDir /afs/cern.ch/work/y/yili/private/MySVN/Code/TTG/run/results/$variation --Region SR1 --SubRegion ejets --Type Reco --Process TTBar --Selection CutSR --PhMatch HFake --DoHist --UseWeight --LumiWeight --SaveTag Final03 --Variation $variation
  ./analysissample --SaveDir /afs/cern.ch/work/y/yili/private/MySVN/Code/TTG/run/results/$variation --Region SR1 --SubRegion ejets --Type Reco --Process TTBar --Selection CutSR --PhMatch EFake --DoHist --UseWeight --LumiWeight --SaveTag Final03 --Variation $variation
  ./analysissample --SaveDir /afs/cern.ch/work/y/yili/private/MySVN/Code/TTG/run/results/$variation --Region SR1 --SubRegion mujets --Type Reco --Process TTBar --Selection CutSR --PhMatch HFake --DoHist --UseWeight --LumiWeight --SaveTag Final03 --Variation $variation
  ./analysissample --SaveDir /afs/cern.ch/work/y/yili/private/MySVN/Code/TTG/run/results/$variation --Region SR1 --SubRegion mujets --Type Reco --Process TTBar --Selection CutSR --PhMatch EFake --DoHist --UseWeight --LumiWeight --SaveTag Final03 --Variation $variation
}

stageout()
{
  echo "Finished";
}

stagein
runcode
stageout

exit
""" % (name, name, name, name) 

    return text

def submitJob(name):

    bsubFile = open( name + ".bsub", "w")          
    text = getJobDef(name)            
    bsubFile.write(text)
    bsubFile.close()            
    os.system("chmod 775 -R *")            

for term in List:
    submitJob(term)
    command="bsub -q 8nh "+ term + ".bsub"
    print command
    os.system(command)

