#!/bin/env python

import os,sys,string,argparse,commands
import shlex
from subprocess import Popen, PIPE

List=[
"TTBarAFNorm",
"TTBarAFME",
"TTBarAFPS",
"TTBarAFQCDUp",
"TTBarAFQCDDn"
]     

def getJobDef(name):    

    text = """
#!/bin/bash

#BSUB -J %s
#BSUB -o stdout_%s.out
#BSUB -e stderr_%s.out
#BSUB -q 8nh
#BSUB -u $USER@cern.ch

stagein()
{
  echo "Start";
}

runcode()
{
  echo "Running the code";
  cd /afs/cern.ch/work/y/yili/private/MySVN/Code/TTG/run;
  sampletag=%s
  mkdir results/$sampletag
  set -x
  ../bin/analysissample --SaveDir results/$sampletag/ --Region CR1 --SubRegion ejets --Type Reco --Process $sampletag --Selection CutSR --PhMatch HFake --DoHist --UseWeight --LumiWeight --SaveTag Final01 --Variation Nominal
  ../bin/analysissample --SaveDir results/$sampletag/ --Region CR1 --SubRegion ejets --Type Reco --Process $sampletag --Selection CutSR --PhMatch EFake --DoHist --UseWeight --LumiWeight --SaveTag Final01 --Variation Nominal
  ../bin/analysissample --SaveDir results/$sampletag/ --Region CR1 --SubRegion mujets --Type Reco --Process $sampletag --Selection CutSR --PhMatch HFake --DoHist --UseWeight --LumiWeight --SaveTag Final01 --Variation Nominal
  ../bin/analysissample --SaveDir results/$sampletag/ --Region CR1 --SubRegion mujets --Type Reco --Process $sampletag --Selection CutSR --PhMatch EFake --DoHist --UseWeight --LumiWeight --SaveTag Final01 --Variation Nominal
  ../bin/analysissample --SaveDir results/$sampletag/ --Region CR1 --SubRegion ee --Type Reco --Process $sampletag --Selection CutSR --PhMatch HFake --DoHist --UseWeight --LumiWeight --SaveTag Final01 --Variation Nominal
  ../bin/analysissample --SaveDir results/$sampletag/ --Region CR1 --SubRegion ee --Type Reco --Process $sampletag --Selection CutSR --PhMatch EFake --DoHist --UseWeight --LumiWeight --SaveTag Final01 --Variation Nominal
  ../bin/analysissample --SaveDir results/$sampletag/ --Region CR1 --SubRegion mumu --Type Reco --Process $sampletag --Selection CutSR --PhMatch HFake --DoHist --UseWeight --LumiWeight --SaveTag Final01 --Variation Nominal
  ../bin/analysissample --SaveDir results/$sampletag/ --Region CR1 --SubRegion mumu --Type Reco --Process $sampletag --Selection CutSR --PhMatch EFake --DoHist --UseWeight --LumiWeight --SaveTag Final01 --Variation Nominal
  ../bin/analysissample --SaveDir results/$sampletag/ --Region CR1 --SubRegion emu --Type Reco --Process $sampletag --Selection CutSR --PhMatch HFake --DoHist --UseWeight --LumiWeight --SaveTag Final01 --Variation Nominal
  ../bin/analysissample --SaveDir results/$sampletag/ --Region CR1 --SubRegion emu --Type Reco --Process $sampletag --Selection CutSR --PhMatch EFake --DoHist --UseWeight --LumiWeight --SaveTag Final01 --Variation Nominal
}

stageout()
{
  echo "Finished";
}

stagein
runcode
stageout

exit
""" % (name, name, name, name) 

    return text

def submitJob(name):

    bsubFile = open( name + ".bsub", "w")          
    text = getJobDef(name)            
    bsubFile.write(text)
    bsubFile.close()            
    os.system("chmod 775 -R *")            

for term in List:
    submitJob(term)
    command="bsub -q 8nh "+ term + ".bsub"
    print command
    os.system(command)

