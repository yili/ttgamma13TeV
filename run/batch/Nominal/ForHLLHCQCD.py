#!/bin/env python

import os,sys,string,argparse,commands
import shlex
from subprocess import Popen, PIPE

List=[
"CutSR",
]     

def getJobDef(name):    

    text = """
#!/bin/bash

#BSUB -J %s
#BSUB -o stdout_%s.out
#BSUB -e stderr_%s.out
#BSUB -q 8nh
#BSUB -u $USER@cern.ch

stagein()
{
  echo "Start";
}

runcode()
{
  echo "Running the code";
  cd /afs/cern.ch/work/y/yili/private/Analysis/Siegen13TeVWork/MainCode/run;
  set -x

  selection=%s
  
  ../bin/analysissample --SaveDir results/Nominal/ --Region CR1 --SubRegion ejets --Type QCD --QCDPara eta:mtw --Selection $selection --DoHist --UseWeight --SaveTag Final08 --Variation Nominal --ZGammaReweight

  ../bin/analysissample --SaveDir results/Nominal/ --Region CR1 --SubRegion mujets --Type QCD --UsePS --QCDPara pt:mtw --Selection $selection --DoHist --UseWeight --SaveTag Final08 --Variation Nominal

}

stageout()
{
  echo "Finished";
}

stagein
runcode
stageout

exit
""" % (name,name,name,name) 

    return text

def submitJob(name):

    bsubFile = open( name + "_ejets.bsub", "w")          
    text = getJobDef(name)            
    bsubFile.write(text)
    bsubFile.close()            
    os.system("chmod 775 -R *")            

for term in List:
    submitJob(term)
    command="bsub -q 8nh "+ term + "_ejets.bsub"
    print command
    os.system(command)

