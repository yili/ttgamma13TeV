#!/bin/env python

import os,sys,string,argparse,commands
import shlex
from subprocess import Popen, PIPE

List=[
"CutSR",
#"CutZGammaVR",
#"CutSRNoIDNoIso",
#"CutSRNoIso",
#"CutHFake",
#"CutSRNph",
#"CutSRNoID",
#"CutSRDLMPhLepLep170",
#"CutSRCvt",
#"CutSRNoIsoCvt",
#"CutHFakeCvt",
#"CutSRNphCvt",
#"CutSRNoIDCvt",
#"CutSRDLMPhLepLep170Cvt",
#"CutSRUnCvt",
#"CutSRNoIsoUnCvt",
#"CutHFakeUnCvt",
#"CutSRNphUnCvt",
#"CutSRNoIDUnCvt",
#"CutSRDLMPhLepLep170UnCvt",
#"CutZGammaVR",
#"CutZGammaVRNoIso",
]     

def getJobDef(name):    

    text = """
#!/bin/bash

#BSUB -J %s
#BSUB -o stdout_%s.out
#BSUB -e stderr_%s.out
#BSUB -q 8nh
#BSUB -u $USER@cern.ch

stagein()
{
  echo "Start";
}

runcode()
{
  echo "Running the code";
  cd /afs/cern.ch/work/y/yili/private/Analysis/Siegen13TeVWork/MainCode/run;
  set -x

  selection=%s
  #../bin/analysissample --SaveDir results/Nominal/ --Region CR1 --SubRegion emu --Type Data --Selection $selection --DoHist --SaveTag Final06 --Variation Nominal
  
  #../bin/analysissample --SaveDir results/Nominal/ --Region CR1 --SubRegion emu --Type Reco --Process Signal --Selection $selection --PhMatch TruePh --DoHist --UseWeight --LumiWeight --SaveTag Final06 --Variation Nominal
  #../bin/analysissample --SaveDir results/Nominal/ --Region CR1 --SubRegion emu --Type Reco --Process Signal --Selection $selection --PhMatch Ancestor1 --DoHist --UseWeight --LumiWeight --SaveTag Final06 --Variation Nominal
  #../bin/analysissample --SaveDir results/Nominal/ --Region CR1 --SubRegion emu --Type Reco --Process Signal --Selection $selection --PhMatch Ancestor2 --DoHist --UseWeight --LumiWeight --SaveTag Final06 --Variation Nominal
  #../bin/analysissample --SaveDir results/Nominal/ --Region CR1 --SubRegion emu --Type Reco --Process Signal --Selection $selection --PhMatch Ancestor3 --DoHist --UseWeight --LumiWeight --SaveTag Final06 --Variation Nominal
  #../bin/analysissample --SaveDir results/Nominal/ --Region CR1 --SubRegion emu --Type Reco --Process TTBar --Selection $selection --PhMatch TruePh --DoHist --UseWeight --LumiWeight --SaveTag Final06 --Variation Nominal
  #../bin/analysissample --SaveDir results/Nominal/ --Region CR1 --SubRegion emu --Type Reco --Process WGammajetsElNLO --Selection $selection --PhMatch TruePh --DoHist --UseWeight --LumiWeight --SaveTag Final06 --Variation Nominal
  #../bin/analysissample --SaveDir results/Nominal/ --Region CR1 --SubRegion emu --Type Reco --Process WGammajetsMuNLO --Selection $selection --PhMatch TruePh --DoHist --UseWeight --LumiWeight --SaveTag Final06 --Variation Nominal
  #../bin/analysissample --SaveDir results/Nominal/ --Region CR1 --SubRegion emu --Type Reco --Process WGammajetsTauNLO --Selection $selection --PhMatch TruePh --DoHist --UseWeight --LumiWeight --SaveTag Final06 --Variation Nominal
  #../bin/analysissample --SaveDir results/Nominal/ --Region CR1 --SubRegion emu --Type Reco --Process WjetsEl --Selection $selection --PhMatch TruePh --DoHist --UseWeight --LumiWeight --SaveTag Final06 --Variation Nominal
  #../bin/analysissample --SaveDir results/Nominal/ --Region CR1 --SubRegion emu --Type Reco --Process WjetsMu --Selection $selection --PhMatch TruePh --DoHist --UseWeight --LumiWeight --SaveTag Final06 --Variation Nominal
  #../bin/analysissample --SaveDir results/Nominal/ --Region CR1 --SubRegion emu --Type Reco --Process WjetsTau --Selection $selection --PhMatch TruePh --DoHist --UseWeight --LumiWeight --SaveTag Final06 --Variation Nominal
  #../bin/analysissample --SaveDir results/Nominal/ --Region CR1 --SubRegion emu --Type Reco --Process ZGammajetsElElNLO --Selection $selection --PhMatch TruePh --DoHist --UseWeight --LumiWeight --SaveTag Final06 --Variation Nominal
  #../bin/analysissample --SaveDir results/Nominal/ --Region CR1 --SubRegion emu --Type Reco --Process ZGammajetsMuMuNLO --Selection $selection --PhMatch TruePh --DoHist --UseWeight --LumiWeight --SaveTag Final06 --Variation Nominal
  #../bin/analysissample --SaveDir results/Nominal/ --Region CR1 --SubRegion emu --Type Reco --Process ZGammajetsTauTauNLO --Selection $selection --PhMatch TruePh --DoHist --UseWeight --LumiWeight --SaveTag Final06 --Variation Nominal
  #../bin/analysissample --SaveDir results/Nominal/ --Region CR1 --SubRegion emu --Type Reco --Process ZjetsElEl --Selection $selection --PhMatch TruePh --DoHist --UseWeight --LumiWeight --SaveTag Final06 --Variation Nominal
  #../bin/analysissample --SaveDir results/Nominal/ --Region CR1 --SubRegion emu --Type Reco --Process ZjetsMuMu --Selection $selection --PhMatch TruePh --DoHist --UseWeight --LumiWeight --SaveTag Final06 --Variation Nominal
  #../bin/analysissample --SaveDir results/Nominal/ --Region CR1 --SubRegion emu --Type Reco --Process ZjetsTauTau --Selection $selection --PhMatch TruePh --DoHist --UseWeight --LumiWeight --SaveTag Final06 --Variation Nominal
  #../bin/analysissample --SaveDir results/Nominal/ --Region CR1 --SubRegion emu --Type Reco --Process STOthers --Selection $selection --PhMatch TruePh --DoHist --UseWeight --LumiWeight --SaveTag Final06 --Variation Nominal
  #../bin/analysissample --SaveDir results/Nominal/ --Region CR1 --SubRegion emu --Type Reco --Process STWT --Selection $selection --PhMatch TruePh --DoHist --UseWeight --LumiWeight --SaveTag Final06 --Variation Nominal
  #../bin/analysissample --SaveDir results/Nominal/ --Region CR1 --SubRegion emu --Type Reco --Process Diboson --Selection $selection --PhMatch TruePh --DoHist --UseWeight --LumiWeight --SaveTag Final06 --Variation Nominal
  
  #../bin/analysissample --SaveDir results/Nominal/ --Region CR1 --SubRegion emu --Type Reco --Process TTBar --Selection $selection --PhMatch HFake --DoHist --UseWeight --LumiWeight --SaveTag Final06 --Variation Nominal
  #../bin/analysissample --SaveDir results/Nominal/ --Region CR1 --SubRegion emu --Type Reco --Process WjetsEl --Selection $selection --PhMatch HFake --DoHist --UseWeight --LumiWeight --SaveTag Final06 --Variation Nominal
  #../bin/analysissample --SaveDir results/Nominal/ --Region CR1 --SubRegion emu --Type Reco --Process WjetsMu --Selection $selection --PhMatch HFake --DoHist --UseWeight --LumiWeight --SaveTag Final06 --Variation Nominal
  #../bin/analysissample --SaveDir results/Nominal/ --Region CR1 --SubRegion emu --Type Reco --Process WjetsTau --Selection $selection --PhMatch HFake --DoHist --UseWeight --LumiWeight --SaveTag Final06 --Variation Nominal
  #../bin/analysissample --SaveDir results/Nominal/ --Region CR1 --SubRegion emu --Type Reco --Process ZjetsElEl --Selection $selection --PhMatch HFake --DoHist --UseWeight --LumiWeight --SaveTag Final06 --Variation Nominal
  #../bin/analysissample --SaveDir results/Nominal/ --Region CR1 --SubRegion emu --Type Reco --Process ZjetsMuMu --Selection $selection --PhMatch HFake --DoHist --UseWeight --LumiWeight --SaveTag Final06 --Variation Nominal
  #../bin/analysissample --SaveDir results/Nominal/ --Region CR1 --SubRegion emu --Type Reco --Process ZjetsTauTau --Selection $selection --PhMatch HFake --DoHist --UseWeight --LumiWeight --SaveTag Final06 --Variation Nominal
  #../bin/analysissample --SaveDir results/Nominal/ --Region CR1 --SubRegion emu --Type Reco --Process STOthers --Selection $selection --PhMatch HFake --DoHist --UseWeight --LumiWeight --SaveTag Final06 --Variation Nominal
  #../bin/analysissample --SaveDir results/Nominal/ --Region CR1 --SubRegion emu --Type Reco --Process STWT --Selection $selection --PhMatch HFake --DoHist --UseWeight --LumiWeight --SaveTag Final06 --Variation Nominal
  #../bin/analysissample --SaveDir results/Nominal/ --Region CR1 --SubRegion emu --Type Reco --Process Diboson --Selection $selection --PhMatch HFake --DoHist --UseWeight --LumiWeight --SaveTag Final06 --Variation Nominal
  #
  #../bin/analysissample --SaveDir results/Nominal/ --Region CR1 --SubRegion emu --Type Reco --Process TTBar --Selection $selection --PhMatch EFake --DoHist --UseWeight --LumiWeight --SaveTag Final06 --Variation Nominal
  #../bin/analysissample --SaveDir results/Nominal/ --Region CR1 --SubRegion emu --Type Reco --Process WjetsEl --Selection $selection --PhMatch EFake --DoHist --UseWeight --LumiWeight --SaveTag Final06 --Variation Nominal
  #../bin/analysissample --SaveDir results/Nominal/ --Region CR1 --SubRegion emu --Type Reco --Process WjetsMu --Selection $selection --PhMatch EFake --DoHist --UseWeight --LumiWeight --SaveTag Final06 --Variation Nominal
  #../bin/analysissample --SaveDir results/Nominal/ --Region CR1 --SubRegion emu --Type Reco --Process WjetsTau --Selection $selection --PhMatch EFake --DoHist --UseWeight --LumiWeight --SaveTag Final06 --Variation Nominal
  #../bin/analysissample --SaveDir results/Nominal/ --Region CR1 --SubRegion emu --Type Reco --Process ZjetsElEl --Selection $selection --PhMatch EFake --DoHist --UseWeight --LumiWeight --SaveTag Final06 --Variation Nominal
  #../bin/analysissample --SaveDir results/Nominal/ --Region CR1 --SubRegion emu --Type Reco --Process ZjetsMuMu --Selection $selection --PhMatch EFake --DoHist --UseWeight --LumiWeight --SaveTag Final06 --Variation Nominal
  #../bin/analysissample --SaveDir results/Nominal/ --Region CR1 --SubRegion emu --Type Reco --Process ZjetsTauTau --Selection $selection --PhMatch EFake --DoHist --UseWeight --LumiWeight --SaveTag Final06 --Variation Nominal
  #../bin/analysissample --SaveDir results/Nominal/ --Region CR1 --SubRegion emu --Type Reco --Process STOthers --Selection $selection --PhMatch EFake --DoHist --UseWeight --LumiWeight --SaveTag Final06 --Variation Nominal
  #../bin/analysissample --SaveDir results/Nominal/ --Region CR1 --SubRegion emu --Type Reco --Process STWT --Selection $selection --PhMatch EFake --DoHist --UseWeight --LumiWeight --SaveTag Final06 --Variation Nominal
  #../bin/analysissample --SaveDir results/Nominal/ --Region CR1 --SubRegion emu --Type Reco --Process Diboson --Selection $selection --PhMatch EFake --DoHist --UseWeight --LumiWeight --SaveTag Final06 --Variation Nominal
  
 #../bin/analysissample --SaveDir results/Nominal/ --Region CR1 --SubRegion ee --Type Data --Selection $selection --DoHist --SaveTag Final06 --Variation Nominal
  
  #../bin/analysissample --SaveDir results/Nominal/ --Region CR1 --SubRegion ee --Type Reco --Process Signal --Selection $selection --PhMatch TruePh --DoHist --UseWeight --LumiWeight --SaveTag Final06 --Variation Nominal
  #../bin/analysissample --SaveDir results/Nominal/ --Region CR1 --SubRegion ee --Type Reco --Process Signal --Selection $selection --PhMatch Ancestor1 --DoHist --UseWeight --LumiWeight --SaveTag Final06 --Variation Nominal
  #../bin/analysissample --SaveDir results/Nominal/ --Region CR1 --SubRegion ee --Type Reco --Process Signal --Selection $selection --PhMatch Ancestor2 --DoHist --UseWeight --LumiWeight --SaveTag Final06 --Variation Nominal
  #../bin/analysissample --SaveDir results/Nominal/ --Region CR1 --SubRegion ee --Type Reco --Process Signal --Selection $selection --PhMatch Ancestor3 --DoHist --UseWeight --LumiWeight --SaveTag Final06 --Variation Nominal
  #../bin/analysissample --SaveDir results/Nominal/ --Region CR1 --SubRegion ee --Type Reco --Process TTBar --Selection $selection --PhMatch TruePh --DoHist --UseWeight --LumiWeight --SaveTag Final06 --Variation Nominal
  ../bin/analysissample --SaveDir results/Nominal/ --Region CR1 --SubRegion ee --Type Reco --Process WGammajetsElNLO --Selection $selection --PhMatch TruePh --DoHist --UseWeight --LumiWeight --SaveTag Final06 --Variation Nominal
  ../bin/analysissample --SaveDir results/Nominal/ --Region CR1 --SubRegion ee --Type Reco --Process WGammajetsMuNLO --Selection $selection --PhMatch TruePh --DoHist --UseWeight --LumiWeight --SaveTag Final06 --Variation Nominal
  ../bin/analysissample --SaveDir results/Nominal/ --Region CR1 --SubRegion ee --Type Reco --Process WGammajetsTauNLO --Selection $selection --PhMatch TruePh --DoHist --UseWeight --LumiWeight --SaveTag Final06 --Variation Nominal
  #../bin/analysissample --SaveDir results/Nominal/ --Region CR1 --SubRegion ee --Type Reco --Process WjetsEl --Selection $selection --PhMatch TruePh --DoHist --UseWeight --LumiWeight --SaveTag Final06 --Variation Nominal
  #../bin/analysissample --SaveDir results/Nominal/ --Region CR1 --SubRegion ee --Type Reco --Process WjetsMu --Selection $selection --PhMatch TruePh --DoHist --UseWeight --LumiWeight --SaveTag Final06 --Variation Nominal
  #../bin/analysissample --SaveDir results/Nominal/ --Region CR1 --SubRegion ee --Type Reco --Process WjetsTau --Selection $selection --PhMatch TruePh --DoHist --UseWeight --LumiWeight --SaveTag Final06 --Variation Nominal
  ../bin/analysissample --SaveDir results/Nominal/ --Region CR1 --SubRegion ee --Type Reco --Process ZGammajetsElElNLO --Selection $selection --PhMatch TruePh --DoHist --UseWeight --LumiWeight --SaveTag Final06 --Variation Nominal
  ../bin/analysissample --SaveDir results/Nominal/ --Region CR1 --SubRegion ee --Type Reco --Process ZGammajetsMuMuNLO --Selection $selection --PhMatch TruePh --DoHist --UseWeight --LumiWeight --SaveTag Final06 --Variation Nominal
  ../bin/analysissample --SaveDir results/Nominal/ --Region CR1 --SubRegion ee --Type Reco --Process ZGammajetsTauTauNLO --Selection $selection --PhMatch TruePh --DoHist --UseWeight --LumiWeight --SaveTag Final06 --Variation Nominal
  #../bin/analysissample --SaveDir results/Nominal/ --Region CR1 --SubRegion ee --Type Reco --Process ZjetsElEl --Selection $selection --PhMatch TruePh --DoHist --UseWeight --LumiWeight --SaveTag Final06 --Variation Nominal
  #../bin/analysissample --SaveDir results/Nominal/ --Region CR1 --SubRegion ee --Type Reco --Process ZjetsMuMu --Selection $selection --PhMatch TruePh --DoHist --UseWeight --LumiWeight --SaveTag Final06 --Variation Nominal
  #../bin/analysissample --SaveDir results/Nominal/ --Region CR1 --SubRegion ee --Type Reco --Process ZjetsTauTau --Selection $selection --PhMatch TruePh --DoHist --UseWeight --LumiWeight --SaveTag Final06 --Variation Nominal
  ../bin/analysissample --SaveDir results/Nominal/ --Region CR1 --SubRegion ee --Type Reco --Process STOthers --Selection $selection --PhMatch TruePh --DoHist --UseWeight --LumiWeight --SaveTag Final06 --Variation Nominal
  ../bin/analysissample --SaveDir results/Nominal/ --Region CR1 --SubRegion ee --Type Reco --Process STWT --Selection $selection --PhMatch TruePh --DoHist --UseWeight --LumiWeight --SaveTag Final06 --Variation Nominal
  ../bin/analysissample --SaveDir results/Nominal/ --Region CR1 --SubRegion ee --Type Reco --Process Diboson --Selection $selection --PhMatch TruePh --DoHist --UseWeight --LumiWeight --SaveTag Final06 --Variation Nominal
  
  #../bin/analysissample --SaveDir results/Nominal/ --Region CR1 --SubRegion ee --Type Reco --Process TTBar --Selection $selection --PhMatch HFake --DoHist --UseWeight --LumiWeight --SaveTag Final06 --Variation Nominal
  #../bin/analysissample --SaveDir results/Nominal/ --Region CR1 --SubRegion ee --Type Reco --Process WjetsEl --Selection $selection --PhMatch HFake --DoHist --UseWeight --LumiWeight --SaveTag Final06 --Variation Nominal
  #../bin/analysissample --SaveDir results/Nominal/ --Region CR1 --SubRegion ee --Type Reco --Process WjetsMu --Selection $selection --PhMatch HFake --DoHist --UseWeight --LumiWeight --SaveTag Final06 --Variation Nominal
  #../bin/analysissample --SaveDir results/Nominal/ --Region CR1 --SubRegion ee --Type Reco --Process WjetsTau --Selection $selection --PhMatch HFake --DoHist --UseWeight --LumiWeight --SaveTag Final06 --Variation Nominal
  #../bin/analysissample --SaveDir results/Nominal/ --Region CR1 --SubRegion ee --Type Reco --Process ZjetsElEl --Selection $selection --PhMatch HFake --DoHist --UseWeight --LumiWeight --SaveTag Final06 --Variation Nominal
  #../bin/analysissample --SaveDir results/Nominal/ --Region CR1 --SubRegion ee --Type Reco --Process ZjetsMuMu --Selection $selection --PhMatch HFake --DoHist --UseWeight --LumiWeight --SaveTag Final06 --Variation Nominal
  #../bin/analysissample --SaveDir results/Nominal/ --Region CR1 --SubRegion ee --Type Reco --Process ZjetsTauTau --Selection $selection --PhMatch HFake --DoHist --UseWeight --LumiWeight --SaveTag Final06 --Variation Nominal
  #../bin/analysissample --SaveDir results/Nominal/ --Region CR1 --SubRegion ee --Type Reco --Process STOthers --Selection $selection --PhMatch HFake --DoHist --UseWeight --LumiWeight --SaveTag Final06 --Variation Nominal
  #../bin/analysissample --SaveDir results/Nominal/ --Region CR1 --SubRegion ee --Type Reco --Process STWT --Selection $selection --PhMatch HFake --DoHist --UseWeight --LumiWeight --SaveTag Final06 --Variation Nominal
  #../bin/analysissample --SaveDir results/Nominal/ --Region CR1 --SubRegion ee --Type Reco --Process Diboson --Selection $selection --PhMatch HFake --DoHist --UseWeight --LumiWeight --SaveTag Final06 --Variation Nominal
  #
  #../bin/analysissample --SaveDir results/Nominal/ --Region CR1 --SubRegion ee --Type Reco --Process TTBar --Selection $selection --PhMatch EFake --DoHist --UseWeight --LumiWeight --SaveTag Final06 --Variation Nominal
  #../bin/analysissample --SaveDir results/Nominal/ --Region CR1 --SubRegion ee --Type Reco --Process WjetsEl --Selection $selection --PhMatch EFake --DoHist --UseWeight --LumiWeight --SaveTag Final06 --Variation Nominal
  #../bin/analysissample --SaveDir results/Nominal/ --Region CR1 --SubRegion ee --Type Reco --Process WjetsMu --Selection $selection --PhMatch EFake --DoHist --UseWeight --LumiWeight --SaveTag Final06 --Variation Nominal
  #../bin/analysissample --SaveDir results/Nominal/ --Region CR1 --SubRegion ee --Type Reco --Process WjetsTau --Selection $selection --PhMatch EFake --DoHist --UseWeight --LumiWeight --SaveTag Final06 --Variation Nominal
  #../bin/analysissample --SaveDir results/Nominal/ --Region CR1 --SubRegion ee --Type Reco --Process ZjetsElEl --Selection $selection --PhMatch EFake --DoHist --UseWeight --LumiWeight --SaveTag Final06 --Variation Nominal
  #../bin/analysissample --SaveDir results/Nominal/ --Region CR1 --SubRegion ee --Type Reco --Process ZjetsMuMu --Selection $selection --PhMatch EFake --DoHist --UseWeight --LumiWeight --SaveTag Final06 --Variation Nominal
  #../bin/analysissample --SaveDir results/Nominal/ --Region CR1 --SubRegion ee --Type Reco --Process ZjetsTauTau --Selection $selection --PhMatch EFake --DoHist --UseWeight --LumiWeight --SaveTag Final06 --Variation Nominal
  #../bin/analysissample --SaveDir results/Nominal/ --Region CR1 --SubRegion ee --Type Reco --Process STOthers --Selection $selection --PhMatch EFake --DoHist --UseWeight --LumiWeight --SaveTag Final06 --Variation Nominal
  #../bin/analysissample --SaveDir results/Nominal/ --Region CR1 --SubRegion ee --Type Reco --Process STWT --Selection $selection --PhMatch EFake --DoHist --UseWeight --LumiWeight --SaveTag Final06 --Variation Nominal
  #../bin/analysissample --SaveDir results/Nominal/ --Region CR1 --SubRegion ee --Type Reco --Process Diboson --Selection $selection --PhMatch EFake --DoHist --UseWeight --LumiWeight --SaveTag Final06 --Variation Nominal
  
 #../bin/analysissample --SaveDir results/Nominal/ --Region CR1 --SubRegion mumu --Type Data --Selection $selection --DoHist --SaveTag Final06 --Variation Nominal
  
  #../bin/analysissample --SaveDir results/Nominal/ --Region CR1 --SubRegion mumu --Type Reco --Process Signal --Selection $selection --PhMatch TruePh --DoHist --UseWeight --LumiWeight --SaveTag Final06 --Variation Nominal
  #../bin/analysissample --SaveDir results/Nominal/ --Region CR1 --SubRegion mumu --Type Reco --Process Signal --Selection $selection --PhMatch Ancestor1 --DoHist --UseWeight --LumiWeight --SaveTag Final06 --Variation Nominal
  #../bin/analysissample --SaveDir results/Nominal/ --Region CR1 --SubRegion mumu --Type Reco --Process Signal --Selection $selection --PhMatch Ancestor2 --DoHist --UseWeight --LumiWeight --SaveTag Final06 --Variation Nominal
  #../bin/analysissample --SaveDir results/Nominal/ --Region CR1 --SubRegion mumu --Type Reco --Process Signal --Selection $selection --PhMatch Ancestor3 --DoHist --UseWeight --LumiWeight --SaveTag Final06 --Variation Nominal
  #../bin/analysissample --SaveDir results/Nominal/ --Region CR1 --SubRegion mumu --Type Reco --Process TTBar --Selection $selection --PhMatch TruePh --DoHist --UseWeight --LumiWeight --SaveTag Final06 --Variation Nominal
  ../bin/analysissample --SaveDir results/Nominal/ --Region CR1 --SubRegion mumu --Type Reco --Process WGammajetsElNLO --Selection $selection --PhMatch TruePh --DoHist --UseWeight --LumiWeight --SaveTag Final06 --Variation Nominal
  ../bin/analysissample --SaveDir results/Nominal/ --Region CR1 --SubRegion mumu --Type Reco --Process WGammajetsMuNLO --Selection $selection --PhMatch TruePh --DoHist --UseWeight --LumiWeight --SaveTag Final06 --Variation Nominal
  ../bin/analysissample --SaveDir results/Nominal/ --Region CR1 --SubRegion mumu --Type Reco --Process WGammajetsTauNLO --Selection $selection --PhMatch TruePh --DoHist --UseWeight --LumiWeight --SaveTag Final06 --Variation Nominal
  #../bin/analysissample --SaveDir results/Nominal/ --Region CR1 --SubRegion mumu --Type Reco --Process WjetsEl --Selection $selection --PhMatch TruePh --DoHist --UseWeight --LumiWeight --SaveTag Final06 --Variation Nominal
  #../bin/analysissample --SaveDir results/Nominal/ --Region CR1 --SubRegion mumu --Type Reco --Process WjetsMu --Selection $selection --PhMatch TruePh --DoHist --UseWeight --LumiWeight --SaveTag Final06 --Variation Nominal
  #../bin/analysissample --SaveDir results/Nominal/ --Region CR1 --SubRegion mumu --Type Reco --Process WjetsTau --Selection $selection --PhMatch TruePh --DoHist --UseWeight --LumiWeight --SaveTag Final06 --Variation Nominal
  ../bin/analysissample --SaveDir results/Nominal/ --Region CR1 --SubRegion mumu --Type Reco --Process ZGammajetsElElNLO --Selection $selection --PhMatch TruePh --DoHist --UseWeight --LumiWeight --SaveTag Final06 --Variation Nominal
  ../bin/analysissample --SaveDir results/Nominal/ --Region CR1 --SubRegion mumu --Type Reco --Process ZGammajetsMuMuNLO --Selection $selection --PhMatch TruePh --DoHist --UseWeight --LumiWeight --SaveTag Final06 --Variation Nominal
  ../bin/analysissample --SaveDir results/Nominal/ --Region CR1 --SubRegion mumu --Type Reco --Process ZGammajetsTauTauNLO --Selection $selection --PhMatch TruePh --DoHist --UseWeight --LumiWeight --SaveTag Final06 --Variation Nominal
  #../bin/analysissample --SaveDir results/Nominal/ --Region CR1 --SubRegion mumu --Type Reco --Process ZjetsElEl --Selection $selection --PhMatch TruePh --DoHist --UseWeight --LumiWeight --SaveTag Final06 --Variation Nominal
  #../bin/analysissample --SaveDir results/Nominal/ --Region CR1 --SubRegion mumu --Type Reco --Process ZjetsMuMu --Selection $selection --PhMatch TruePh --DoHist --UseWeight --LumiWeight --SaveTag Final06 --Variation Nominal
  #../bin/analysissample --SaveDir results/Nominal/ --Region CR1 --SubRegion mumu --Type Reco --Process ZjetsTauTau --Selection $selection --PhMatch TruePh --DoHist --UseWeight --LumiWeight --SaveTag Final06 --Variation Nominal
  ../bin/analysissample --SaveDir results/Nominal/ --Region CR1 --SubRegion mumu --Type Reco --Process STOthers --Selection $selection --PhMatch TruePh --DoHist --UseWeight --LumiWeight --SaveTag Final06 --Variation Nominal
  ../bin/analysissample --SaveDir results/Nominal/ --Region CR1 --SubRegion mumu --Type Reco --Process STWT --Selection $selection --PhMatch TruePh --DoHist --UseWeight --LumiWeight --SaveTag Final06 --Variation Nominal
  ../bin/analysissample --SaveDir results/Nominal/ --Region CR1 --SubRegion mumu --Type Reco --Process Diboson --Selection $selection --PhMatch TruePh --DoHist --UseWeight --LumiWeight --SaveTag Final06 --Variation Nominal
  
  #../bin/analysissample --SaveDir results/Nominal/ --Region CR1 --SubRegion mumu --Type Reco --Process TTBar --Selection $selection --PhMatch HFake --DoHist --UseWeight --LumiWeight --SaveTag Final06 --Variation Nominal
  #../bin/analysissample --SaveDir results/Nominal/ --Region CR1 --SubRegion mumu --Type Reco --Process WjetsEl --Selection $selection --PhMatch HFake --DoHist --UseWeight --LumiWeight --SaveTag Final06 --Variation Nominal
  #../bin/analysissample --SaveDir results/Nominal/ --Region CR1 --SubRegion mumu --Type Reco --Process WjetsMu --Selection $selection --PhMatch HFake --DoHist --UseWeight --LumiWeight --SaveTag Final06 --Variation Nominal
  #../bin/analysissample --SaveDir results/Nominal/ --Region CR1 --SubRegion mumu --Type Reco --Process WjetsTau --Selection $selection --PhMatch HFake --DoHist --UseWeight --LumiWeight --SaveTag Final06 --Variation Nominal
  #../bin/analysissample --SaveDir results/Nominal/ --Region CR1 --SubRegion mumu --Type Reco --Process ZjetsElEl --Selection $selection --PhMatch HFake --DoHist --UseWeight --LumiWeight --SaveTag Final06 --Variation Nominal
  #../bin/analysissample --SaveDir results/Nominal/ --Region CR1 --SubRegion mumu --Type Reco --Process ZjetsMuMu --Selection $selection --PhMatch HFake --DoHist --UseWeight --LumiWeight --SaveTag Final06 --Variation Nominal
  #../bin/analysissample --SaveDir results/Nominal/ --Region CR1 --SubRegion mumu --Type Reco --Process ZjetsTauTau --Selection $selection --PhMatch HFake --DoHist --UseWeight --LumiWeight --SaveTag Final06 --Variation Nominal
  #../bin/analysissample --SaveDir results/Nominal/ --Region CR1 --SubRegion mumu --Type Reco --Process STOthers --Selection $selection --PhMatch HFake --DoHist --UseWeight --LumiWeight --SaveTag Final06 --Variation Nominal
  #../bin/analysissample --SaveDir results/Nominal/ --Region CR1 --SubRegion mumu --Type Reco --Process STWT --Selection $selection --PhMatch HFake --DoHist --UseWeight --LumiWeight --SaveTag Final06 --Variation Nominal
  #../bin/analysissample --SaveDir results/Nominal/ --Region CR1 --SubRegion mumu --Type Reco --Process Diboson --Selection $selection --PhMatch HFake --DoHist --UseWeight --LumiWeight --SaveTag Final06 --Variation Nominal
  #
  #../bin/analysissample --SaveDir results/Nominal/ --Region CR1 --SubRegion mumu --Type Reco --Process TTBar --Selection $selection --PhMatch EFake --DoHist --UseWeight --LumiWeight --SaveTag Final06 --Variation Nominal
  #../bin/analysissample --SaveDir results/Nominal/ --Region CR1 --SubRegion mumu --Type Reco --Process WjetsEl --Selection $selection --PhMatch EFake --DoHist --UseWeight --LumiWeight --SaveTag Final06 --Variation Nominal
  #../bin/analysissample --SaveDir results/Nominal/ --Region CR1 --SubRegion mumu --Type Reco --Process WjetsMu --Selection $selection --PhMatch EFake --DoHist --UseWeight --LumiWeight --SaveTag Final06 --Variation Nominal
  #../bin/analysissample --SaveDir results/Nominal/ --Region CR1 --SubRegion mumu --Type Reco --Process WjetsTau --Selection $selection --PhMatch EFake --DoHist --UseWeight --LumiWeight --SaveTag Final06 --Variation Nominal
  #../bin/analysissample --SaveDir results/Nominal/ --Region CR1 --SubRegion mumu --Type Reco --Process ZjetsElEl --Selection $selection --PhMatch EFake --DoHist --UseWeight --LumiWeight --SaveTag Final06 --Variation Nominal
  #../bin/analysissample --SaveDir results/Nominal/ --Region CR1 --SubRegion mumu --Type Reco --Process ZjetsMuMu --Selection $selection --PhMatch EFake --DoHist --UseWeight --LumiWeight --SaveTag Final06 --Variation Nominal
  #../bin/analysissample --SaveDir results/Nominal/ --Region CR1 --SubRegion mumu --Type Reco --Process ZjetsTauTau --Selection $selection --PhMatch EFake --DoHist --UseWeight --LumiWeight --SaveTag Final06 --Variation Nominal
  #../bin/analysissample --SaveDir results/Nominal/ --Region CR1 --SubRegion mumu --Type Reco --Process STOthers --Selection $selection --PhMatch EFake --DoHist --UseWeight --LumiWeight --SaveTag Final06 --Variation Nominal
  #../bin/analysissample --SaveDir results/Nominal/ --Region CR1 --SubRegion mumu --Type Reco --Process STWT --Selection $selection --PhMatch EFake --DoHist --UseWeight --LumiWeight --SaveTag Final06 --Variation Nominal
  #../bin/analysissample --SaveDir results/Nominal/ --Region CR1 --SubRegion mumu --Type Reco --Process Diboson --Selection $selection --PhMatch EFake --DoHist --UseWeight --LumiWeight --SaveTag Final06 --Variation Nominal
}

stageout()
{
  echo "Finished";
}

stagein
runcode
stageout

exit
""" % (name, name, name, name) 

    return text

def submitJob(name):

    bsubFile = open( name + "_ll.bsub", "w")          
    text = getJobDef(name)            
    bsubFile.write(text)
    bsubFile.close()            
    os.system("chmod 775 -R *")            

for term in List:
    submitJob(term)
    command="bsub -q 8nh "+ term + "_ll.bsub"
    print command
    os.system(command)

